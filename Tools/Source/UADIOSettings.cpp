#include "UADIOSettings.h"

#include "UADMaster.h"

namespace ung
{
	UadIOSettings::UadIOSettings()
	{
		auto fbxManager = UadMaster::getInstance().getFbxManager();

		//创建IO settings对象
		mSettings = FbxIOSettings::Create(fbxManager, IOSROOT);

		BOOST_ASSERT(mSettings);
	}

	UadIOSettings::~UadIOSettings()
	{
		if (mSettings)
		{
			mSettings->Destroy();
		}
	}

	FbxIOSettings* UadIOSettings::getFbxIOSettings() const
	{
		BOOST_ASSERT(mSettings);

		return mSettings;
	}

	void UadIOSettings::setBoolProp(String const & name, bool on_off)
	{
		BOOST_ASSERT(mSettings);

		/*
		IMP_FBX_MATERIAL
		IMP_FBX_TEXTURE
		IMP_FBX_LINK
		IMP_FBX_SHAPE
		IMP_FBX_GOBO
		IMP_FBX_ANIMATION
		IMP_FBX_GLOBAL_SETTINGS

		EXP_FBX_MATERIAL
		EXP_FBX_TEXTURE
		EXP_FBX_EMBEDDED
		EXP_FBX_SHAPE
		EXP_FBX_GOBO
		EXP_FBX_ANIMATION
		EXP_FBX_GLOBAL_SETTINGS
		*/

		/*
		The FbxIOSettings object's EXP_FBX_EMBEDDED property specifies how the FBX SDK
		should export any media (textures, sound, movies, etc).
		If the EXP_FBX_EMBEDDED property is set to:
		true:the media is embedded within the exported file. This option is available only when 
		exporting to binary FBX files.
		false:the media is not embedded within the exported file. Instead, the FBX file contains 
		relative references to the media files.
		When the FBX SDK imports a binary FBX file which contains embedded media,it will 
		extract the embedded media files into the binary FBX file's myExportFile.fbm/subdirectory.
		If this subdirectory does not exist, it is automatically created.
		*/

		mSettings->SetBoolProp(name.data(), on_off);
	}

	bool UadIOSettings::getBoolProp(String const & name) const
	{
		BOOST_ASSERT(mSettings);

		//如果没有找到对应的属性的话，则返回false
		bool notFoundReturn{ false };

		return mSettings->GetBoolProp(name.data(), notFoundReturn);
	}
}//namespace ung