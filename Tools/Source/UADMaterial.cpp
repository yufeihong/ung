#include "UADMaterial.h"

#include "UADMaster.h"
#include "UADScene.h"

namespace ung
{
	UadMaterial::UadMaterial(String const& name) :
		mPhong(nullptr),
		mName(name)
	{
		auto& master = UadMaster::getInstance();
		auto scene = master.getScene();

		//mPhong = FbxSurfacePhong::Create(scene->getFbxScene(), mName.data());

		//BOOST_ASSERT(mPhong);
	}

	UadMaterial::~UadMaterial()
	{
		if (mPhong)
		{
			mPhong->Destroy();
		}
	}

	int UadMaterial::getDiffuseTextureCount() const
	{
		BOOST_ASSERT(mPhong);

		auto tc = mPhong->Diffuse.GetSrcObjectCount<FbxFileTexture>();

		return tc;
	}
}//namespace ung