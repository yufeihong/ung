#include "UADMaster.h"

#include "UADImporter.h"
#include "UADNode.h"

namespace ung
{
	void UadMaster::_createFbxManager()
	{
		UNG_PRINT_ONE_LINE("构造UadMaster");

		//创建memory管理器
		//mFbxManager = FbxManager::Create();
		//BOOST_ASSERT(mFbxManager);
	}

	void UadMaster::_destroyFbxManager()
	{
		//BOOST_ASSERT(mFbxManager);

		/*
		销毁memory管理器，同时也销毁了它所handling的其它对象
		All the objects that
		1,have been allocated by the memory manager, and that
		2,have not been explicitly destroyed
		will be automatically destroyed.
		*/
		//mFbxManager->Destroy();

		UNG_PRINT_ONE_LINE("销毁FbxManager。");
	}

	void UadMaster::_getSDKVersion()
	{
		BOOST_ASSERT(mFbxManager);

		//获取FBX SDK的版本号，包括修订编号以及发布日期
		auto version = mFbxManager->GetVersion();
		mSDKVersion = String(version);

		BOOST_ASSERT(!mSDKVersion.empty());
	}

	void UadMaster::_loadDLL()
	{
		BOOST_ASSERT(mFbxManager);

		FbxString dllPath = FbxGetApplicationDirectory();
		mFbxManager->LoadPluginsDirectory(dllPath.Buffer());
	}
}//namespace ung