#include "UADMesh.h"

#include "UADSubMesh.h"
#include "UFCSTLWrapper.h"

namespace ung
{
	UadMesh::UadMesh()
	{
	}

	UadMesh::~UadMesh()
	{
		ungContainerClear(mSubMeshs);
	}

	UadSubMesh* UadMesh::createSubMesh(FbxMesh* fbxMesh)
	{
		auto subMesh = new UadSubMesh(fbxMesh);

		BOOST_ASSERT(subMesh);

		mSubMeshs.push_back(subMesh);

		return subMesh;
	}
}//namespace ung