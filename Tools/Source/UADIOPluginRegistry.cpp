#include "UADIOPluginRegistry.h"

#include "UADMaster.h"

namespace ung
{
	UadIOPluginRegistry::UadIOPluginRegistry()
	{
		auto& master = UadMaster::getInstance();
		auto fbxManager = master.getFbxManager();

		/*
		FbxIOPluginRegistry* GetIOPluginRegistry()	const
		Access to the unique FbxIOPluginRegistry object.
		Returns:The pointer to the user FbxIOPluginRegistry
		*/
		mRegistry = fbxManager->GetIOPluginRegistry();

		BOOST_ASSERT(mRegistry);
	}

	UadIOPluginRegistry::~UadIOPluginRegistry()
	{
	}

	int UadIOPluginRegistry::getWriterFormatCount() const
	{
		BOOST_ASSERT(mRegistry);

		/*
		int GetWriterFormatCount() const
		Returns:The number of file formats that can be exported.
		Remarks:Multiple identifiers for the same format count as different file formats. For 
		example,eFBX_BINARY, eFBX_ASCII and eFBX_ENCRYPTED(����) are counted as three 
		separate file formats.
		*/
		return mRegistry->GetWriterFormatCount();
	}

	bool UadIOPluginRegistry::writerIsFBX(int writerIndex) const
	{
		BOOST_ASSERT(mRegistry);

		/*
		bool WriterIsFBX(int pFileFormat) const
		Verifies if the file format of the Writer is FBX.
		pFileFormat:
		The file format identifier.
		*/
		return mRegistry->WriterIsFBX(writerIndex);
	}

	String UadIOPluginRegistry::getWriterFormatDescription(int writerIndex) const
	{
		BOOST_ASSERT(mRegistry);

		/*
		const char* GetWriterFormatDescription(int pFileFormat) const
		Returns the description of an exportable file format.
		pFileFormat:
		The file format identifier.
		Returns:
		A pointer to the character representation of the description.
		*/
		auto desc = mRegistry->GetWriterFormatDescription(writerIndex);

		return String(desc);
	}
}//namespace ung