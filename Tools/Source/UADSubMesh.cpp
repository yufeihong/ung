#include "UADSubMesh.h"

#include "UFCSTLWrapper.h"
#include "UFCNumericCast.h"
#include "UFCNumBounds.h"

namespace ung
{
	UadSubMesh::UadSubMesh(FbxMesh* fbxMesh) :
		mFbxMesh(fbxMesh)
	{
		mTriangleCount = numCast<unsigned short, int>(mFbxMesh->GetPolygonCount());

		/*
		int GetControlPointsCount() const
		Returns the number of control points.
		*/
		mVertexCount = numCast<unsigned short, int>(mFbxMesh->GetControlPointsCount());

		//顶点数量一定要容许unsigned short来表达
		BOOST_ASSERT(mVertexCount <= NumBounds<unsigned short>::highest() + 1);

		mPositionBuffer = new FbxVector4[mVertexCount];

		readFbxMesh();
	}

	UadSubMesh::~UadSubMesh()
	{
		if (mPositionBuffer)
		{
			delete[] mPositionBuffer;
		}

		ungContainerClear(mIndices);
	}

	void UadSubMesh::readFbxMesh()
	{
		_readPositionData();

		_readIndexData();
	}

	//-----------------------------------------------------------------------------------------

	void UadSubMesh::_readPositionData()
	{
		/*
		FbxVector4* GetControlPoints(FbxStatus* pStatus = ((void*)0)) const
		Returns a pointer to the array of control points.
		pStatus,Not used in the implementation of this class.
		Returns:
		Pointer to the array of control points, or NULL if the array has not been allocated.
		*/
		memcpy(mPositionBuffer, mFbxMesh->GetControlPoints(), sizeof(FbxVector4) * mVertexCount);
	}

	void UadSubMesh::_readIndexData()
	{
		for (unsigned int i = 0; i < mTriangleCount; ++i)
		{
			/*
			int GetPolygonSize(int pPolygonIndex) const
			Get the number of polygon vertices in a polygon.
			pPolygonIndex,Index of the polygon.
			Returns:
			The number of polygon vertices in the indexed polygon. If the polygon index is out of 
			bounds, return -1.
			*/
			auto polygonSize = mFbxMesh->GetPolygonSize(i);
			BOOST_ASSERT(polygonSize == 3);

			for (int j = 0; j < polygonSize; ++j)
			{
				/*
				当前顶点在顶点缓冲区中的索引值。

				int GetPolygonVertex(int pPolygonIndex,int pPositionInPolygon) const
				Get a polygon vertex (i.e: an index to a control point).
				Parameters:
				pPolygonIndex,Index of queried polygon. The valid range for this parameter is 0 to 
				FbxMesh::GetPolygonCount().
				pPositionInPolygon,Position of polygon vertex in indexed polygon. The valid range 
				for this parameter is 0 to FbxMesh::GetPolygonSize(pPolygonIndex).
				Returns:
				Return the polygon vertex indexed or -1 if the requested vertex does not exists or 
				the indices arguments have an invalid range.
				*/
				int controlPointIndex = mFbxMesh->GetPolygonVertex(i, j);

				mIndices.push_back(controlPointIndex);
			}
		}
	}
}//namespace ung