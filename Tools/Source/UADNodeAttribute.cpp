#include "UADNodeAttribute.h"

namespace
{
	using namespace ung;

	String getAttributeTypeName(FbxNodeAttribute::EType type)
	{
		switch (type)
		{
		case FbxNodeAttribute::eUnknown: return "unidentified";
		case FbxNodeAttribute::eNull: return "null";
		case FbxNodeAttribute::eMarker: return "marker";
		case FbxNodeAttribute::eSkeleton: return "skeleton";
		case FbxNodeAttribute::eMesh: return "mesh";
		case FbxNodeAttribute::eNurbs: return "nurbs";
		case FbxNodeAttribute::ePatch: return "patch";
		case FbxNodeAttribute::eCamera: return "camera";
		case FbxNodeAttribute::eCameraStereo: return "stereo";
		case FbxNodeAttribute::eCameraSwitcher: return "camera switcher";
		case FbxNodeAttribute::eLight: return "light";
		case FbxNodeAttribute::eOpticalReference: return "optical reference";
		case FbxNodeAttribute::eOpticalMarker: return "marker";
		case FbxNodeAttribute::eNurbsCurve: return "nurbs curve";
		case FbxNodeAttribute::eTrimNurbsSurface: return "trim nurbs surface";
		case FbxNodeAttribute::eBoundary: return "boundary";
		case FbxNodeAttribute::eNurbsSurface: return "nurbs surface";
		case FbxNodeAttribute::eShape: return "shape";
		case FbxNodeAttribute::eLODGroup: return "lodgroup";
		case FbxNodeAttribute::eSubDiv: return "subdiv";
		default: return "unknown";
		}
	}
}//namespace

namespace ung
{
	UadNodeAttribute::UadNodeAttribute(FbxNodeAttribute* ab) :
		mAttribute(ab)
	{
		BOOST_ASSERT(mAttribute);

		mName = mAttribute->GetName();
		mType = mAttribute->GetAttributeType();
		mTypeName = getAttributeTypeName(mType);
	}

	UadNodeAttribute::~UadNodeAttribute()
	{
	}

	String const & UadNodeAttribute::getName() const
	{
		return mName;
	}

	FbxNodeAttribute::EType UadNodeAttribute::getType() const
	{
		return mType;
	}

	String const & UadNodeAttribute::getTypeName() const
	{
		return mTypeName;
	}

	FbxNodeAttribute * UadNodeAttribute::getFbxNodeAttribute() const
	{
		BOOST_ASSERT(mAttribute);

		return mAttribute;
	}
}//namespace ung