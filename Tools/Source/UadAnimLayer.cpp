#include "UADAnimLayer.h"

#include "UADMaster.h"
#include "UADScene.h"

namespace ung
{
	UadAnimLayer::UadAnimLayer(String * pName) :
		mName(!pName ? "" : *pName)
	{
		auto& master = UadMaster::getInstance();
		auto scene = master.getScene();

		//mAnimLayer = FbxAnimLayer::Create(scene->getFbxScene(), mName.data());

		//BOOST_ASSERT(mAnimLayer);
	}

	UadAnimLayer::UadAnimLayer(FbxAnimLayer * animLayer, String * pName) :
		mAnimLayer(animLayer),
		mName(!pName ? "" : *pName)
	{
		BOOST_ASSERT(mAnimLayer);
	}

	UadAnimLayer::~UadAnimLayer()
	{
		if (mAnimLayer)
		{
			mAnimLayer->Destroy();
		}
	}

	FbxAnimLayer * UadAnimLayer::getFbxAnimLayer() const
	{
		BOOST_ASSERT(mAnimLayer);

		return mAnimLayer;
	}
}//namespace ung