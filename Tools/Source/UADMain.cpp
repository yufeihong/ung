#include "UAD.h"

using namespace ung;

int main()
{
	auto manager = createManager();

	auto scene = createScene(manager, "");

	auto importer = createImporter(manager, "", "../../../Resource/Media/Models/fbx/box.fbx");

	populateScene(manager,scene, importer);

	//processNode(scene->GetRootNode());

	auto exporter = createExporter(manager, "", "../../../Resource/Media/Models/fbx/out.fbx");

	return 0;
}