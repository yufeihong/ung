/*
FbxManager:
SDK中的几乎所有类，都是通过该管理器进行内存空间的分配和释放的。

The FbxManager used to instantiate a FbxObject can be retrieved by calling 
FbxObject::GetFbxManager().
//Assume pImporter is an instance of FbxImporter.
FbxManager* lSdkManager = pImporter->GetFbxManager();
*/

/*
FbxIOSettings:
FbxIOSettings是一个树状结构，相当于一个properties(属性)的集合。
该树状结构用于导入/导出FBX文件时的一些选项。
可以做成一个UI面板，其中有一些导入/导出的选项。
树状结构中的选项可以通过下列函数来读取自于一个XML文件，或者存储至一个XML文件：
ReadXMLFile(),WriteXMLFile(),WriteXmlPropToFile()
该类负责指定一个场景中的那些elements可以导入或导出，这些elements包括：
cameras, lights, meshes, textures,materials, animations, custom properties, etc.
一个FbxIOSettings对象必须在传递给FbxImporter或FbxExporter对象之前进行实例化，并且配置。
一个FbxIOSettings对象被创建后，其所有的选项都被赋予了一个默认值。
*/

/*
FbxImporter:
要导入一个FBX文件，就必须创建FbxIOSettings对象和FbxImporter对象。
The FBX SDK does not directly support exporting and importing to/from a memory stream.
When a binary FBX file containing embedded media is imported, the media will be extracted 
from the file and copied into a subdirectory. This subdirectory will be created at the location of 
the FBX file with the same name as the file, with the extension .fbm.
*/

/*
FbxExporter:
During an export operation, the root node of a scene is not exported to the file. Only the 
children of the root node are exported to the file. As such, it is not recommended to associate 
anything to the root node that should be saved to a file.
*/

/*
FbxIOPluginRegistry:
This class serves as the registrar for file formats.
A file format must be registered when it is used by the FBX SDK.
This class also lets you create and read formats other than FBX SDK native formats. Users of 
FBX SDK can write their own plug-ins to read or write arbitrary file formats. Once their plug-ins 
are registered in this class, FBX SDK is able to read or write these file formats.
Each FbxManager has a unique FbxIOPluginRegistry. To get an instance of this class:
FbxIOPluginRegistry* registry = manager->GetIOPluginRegistry();
*/

/*
FbxScene:
FbxScene对象相当于一个容器，里面存放的是场景中的elements。
每一个导入/导出文件对应唯一一个FbxScene对象。
一个FbxScene对象可以容纳各种elements，包括meshes, lights, cameras, skeletons,NURBS and
animation curves。
场景中elements的组织结构为一个层次树，树的节点为FbxNodes。
可以通过函数FbxScene::GetRootNode()来得到树的根节点。
把一个场景导出至一个文件中时，根节点会被保存，在这之前仅其子节点保存了。
子节点可以通过函数FbxNode::GetChild()来访问。
父节点可以通过函数FbxNode::GetParent()来访问。
一个FbxNode节点也相当于一个容器，可以容纳场景中的一个或多个elements。
场景中的elements都是继承自FbxNodeAttribute类。所以一个FbxNode节点可能容纳一个或多个
FbxNodeAttribute对象。
根节点不包含任何attributes(属性)。
场景中的element也是node attribute。
In FBX,a scene can have one or more "animation stack".An animation stack is a container for 
animation data.You can access a file's animation stack information without the overhead of 
loading the entire file into the scene.(你可以访问文件的动画堆栈信息，而无需将整个文件加载到场景中。)
*/

/*
FbxNode:
An FbxNode provides access to its local translation (FbxNode::LclTranslation), rotation
(FbxNode::LclRotation), and scaling (FbxNode::LclScaling) properties. These properties
represent the transformations applied to the parent node's position, orientation, and scale
to obtain the current node's position, orientation, and scale.
*/

/*
FbxNodeAttribute:
This class is the base class to all types of node attributes.
A node attribute is the content of a node.
*/

/*
FbxTakeInfo:
This class contains take information from an imported file or exported to an output file.
A "take" is in fact a group of animation data grouped by name, so the FBX file format can 
support many "animation takes" in an FBX file to mimic(模仿) how a movie is produced by 
making many takes of the same scene.
The most used data is the "take name", other data are rarely used. Example of use: to get the 
list of all animation take names of FBX file without loading all the scene content. When a 
FbxImporter is initialized, the take information can be read and can be available before the long 
Import() step, this way, we can get the take info data very fast since we don't need to load all 
the animation scene data.
*/

/*
Objects:
Most of the classes in the FBX SDK are derived from FbxObject.

Object Properties:
An FBX object such as FbxNode uses FBX properties (FbxProperty) rather than conventional 
C++ member variables(而不是传统的C++成员变量). FBX properties allow for strongly typed data
(强类型数据)to be dynamically added to FBX objects.

Connections:
The concept of connections is relevant to understanding the FBX SDK's object model.
Connections can bind objects to one another, properties to one another, objects to properties,
and vice versa(连接可以将对象绑定到另一个对象，属性相互连接，对象与属性绑定，反之亦然). Note 
that there is no Connection class available in the API. Rather, connections can only be 
manipulated with the FbxObject and FbxProperty connection-related member functions such as 
FbxObject::GetSrcObject().
*/

/*
Creating Objects within a Scene:
A FbxScene object can contain a variety of scene elements such as meshes, lights, animations,
characters, etc. These elements should be created with a reference to the scene in which they 
exist. As such, when the scene is exported, so are all of its elements. When the scene is 
destroyed, the memory allocated to all of its objects is also released.
//Create a node object
FbxNode* lNode = FbxNode::Create(lScene, "node");
//Create a mesh object
FbxMesh* lMesh = FbxMesh::Create(lScene, "");
NOTE:It is possible to create scene elements (FbxNode, FbxNodeAttribute) using only a 
reference to the FbxManager (instead of FbxScene), however when the scene is destroyed,
those scene elements will not be destroyed with it; they will only be destroyed either explicitly 
or when the FbxManager is destroyed.

Destroying objects:
An FBX SDK object should be explicitly destroyed by calling its Destroy() member function. The 
FbxManager will automatically free the memory allocated for that object, and will update all the 
internal connections between that object (FbxObject), its properties (FbxProperty), and other 
FbxObject to remove any inconsistencies(消除任何的不一致之处).
*/

/*
Properties:
The FBX SDK uses the FbxProperty class to enforce strongly typed, static and/or dynamic 
associations of properties to instances of FbxObject.
*/

/*
Collections:
Container classes such as, FbxAnimLayer, FbxAnimStack, and FbxScene inherit from the 
FbxCollection class. This class provides an interface to:
Add members,FbxCollection::AddMember()
Remove members,FbxCollection::RemoveMember()
Count members,FbxCollection::GetMemberCount()
Get a member,FbxCollection::GetMember()
Search its members,FbxCollection::FindMember()
*/

/*
Copying an FBX Object:
An FBX object can be copied by calling its Copy() member function. The assignment operator 
(operator=) cannot be used to copy FBX objects; it is a private member function. The following 
code sample illustrates how to copy a mesh object.
//Assume that lScene is a pointer to a valid scene object.
FbxMesh* lSourceMesh = FbxMesh::Create (lScene, "");
//Define control points, etc. for lSourceMesh.
//This mesh will be overwritten
FbxMesh* lTargetMesh = FbxMesh::Create (lScene, "");
//Copy the data from lSourceMesh into lTargetMesh.Note that the source object and the target 
//object must be instances of the same class (FbxMesh in this case).
lTargetMesh->Copy(lSourceMesh);
NOTE:Copying an FbxObject will also copy all of its associated FbxProperty instances, and their 
values.
NOTE:Copying an FbxObject does not copy any of its inter-object connections (for example,
parent-child relationships). These connections must be set explicitly on the copy.
*/

/*
FbxPatch:
A patch is a type of node attribute with parametric surface(参数曲面).
A patch object is useful for creating gently(轻微的) curved surfaces, and provides very detailed 
control for manipulating complex geometry.
*/

/*
FbxSkeleton:
是FbxNodeAttribute的子类。
This class specializes(专门处理) a node attribute to represent the elements forming "bone" chains.
The FbxSkeleton name of the class comes from the analogy(类比) with the human body skeletal 
structure. In fact, an object of this type is nothing more than a transform node with special 
properties that are useful for its graphical representation and during IK/FK and skin deformation 
computations. Typically, a scene will contain chains of FbxSkeleton node attributes that,together,
form a skeleton segment.For instance,the representation of the leg of a character can be 
achieved using one parent node with the attribute eRoot,followed by one child (femur) of type 
eLimb, this child having a child also (tibia) of the same type. Finally, terminated with a last 
node attribute of type eEffector (ankle).
*/

/*
FbxSkin:
A skin deformer(变形) contains clusters (FbxCluster). Each cluster acts on a subset of the 
geometry's control points, with different weights. For example, a mesh of humanoid shape can 
have a skin attached, that describes the way the humanoid mesh is deformed by bones. When 
the bones are animated, the clusters act on the geometry to animate it too.
*/

/*
FbxCluster:
A cluster, or link, is an entity acting on a geometry (FbxGeometry). More precisely(更确切地说),
the cluster acts on a subset of the geometry's control points. For each control point that the 
cluster acts on, the intensity(强度) of the cluster's action is modulated by a weight(由权重来调制).
The link mode (ELinkMode) specifies how the weights are taken into account.
The cluster's link node specifies the node (FbxNode) that influences the control points of the 
cluster. If the node is animated, the control points will move accordingly.
A cluster is usually part of a skin.
For example, imagine a mesh representing a humanoid, and imagine a skeleton made of bones.
Each bone is represented by a node in FBX. To bind the geometry to the nodes, we create a 
skin (FbxSkin). The skin has many clusters, each one corresponding to a bone. Each node 
influences some control points of the mesh. A node has a high influence on some of the points
(high weight) and lower influence on some other points (low weight). Some points of the mesh 
are not affected at all by the bone, so they would not be part of the corresponding cluster.
*/