#include "UADExporter.h"

#include "UADMaster.h"
#include "UADIOPluginRegistry.h"
#include "UADIOSettings.h"
#include "UADScene.h"

#include "UFCFilesystem.h"
#include "UFCStringUtilities.h"

namespace
{
	using namespace ung;

	void checkFile(String const & fileName)
	{
		auto& fs = Filesystem::getInstance();

		const char* nameCStr = fileName.c_str();

		auto extRet = fs.hasExtension(nameCStr);
		BOOST_ASSERT(extRet && "The output fbx file does not have extension.");

		auto ext = fs.getFileExtension(nameCStr);
		BOOST_ASSERT(StringUtilities::equalInSensitive(ext, String("fbx")));
	}
}//namespace

namespace ung
{
	UadExporter::UadExporter(UadIOSettings * pSettings, String const & fileName,bool embedMedia, String * pName) :
		mName(!pName ? "" : *pName),
		mFormat(-1)
	{
		BOOST_ASSERT(pSettings);
		mIOSettings = pSettings;

		checkFile(fileName);

		auto& master = UadMaster::getInstance();
		auto fbxManager = master.getFbxManager();

		mExporter = FbxExporter::Create(fbxManager, mName.data());
		BOOST_ASSERT(mExporter);

		auto& IOPluginRegistry = master.getIOPluginRegistry();

		if (!embedMedia)
		{
			//Try to export in ASCII if possible
			int formatCount = IOPluginRegistry.getWriterFormatCount();

			for (int index = 0; index < formatCount; ++index)
			{
				if (IOPluginRegistry.writerIsFBX(index))
				{
					auto desc = IOPluginRegistry.getWriterFormatDescription(index);
					if (StringUtilities::containInSensitive(desc,"ascii"))
					{
						mFormat = index;

						break;
					}
				}
			}
		}

		bool initRet = mExporter->Initialize(fileName.data(),mFormat,mIOSettings->getFbxIOSettings());

		if (!initRet)
		{
			UNG_PRINT_ONE_LINE("Call to FbxExporter::Initialize() failed.");

			auto errorMsg = mExporter->GetStatus().GetErrorString();
			UNG_EXCEPTION(errorMsg);
		}
	}

	UadExporter::~UadExporter()
	{
		if (mExporter)
		{
			mExporter->Destroy();
		}
	}

	void UadExporter::getFileVersion(int & major, int & minor, int & revision) const
	{
		FbxManager::GetFileFormatVersion(major,minor,revision);
	}

	int UadExporter::getFormat() const
	{
		return mFormat;
	}

	void UadExporter::doExport(UadScene* pScene)
	{
		BOOST_ASSERT(pScene);

		BOOST_ASSERT(mExporter);

		//mExporter->Export(pScene->getFbxScene());

		//mExporter->Destroy();
		//mExporter = nullptr;
	}
}//namespace ung