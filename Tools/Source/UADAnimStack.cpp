#include "UADAnimStack.h"

#include "UADMaster.h"
#include "UADScene.h"
#include "UADAnimLayer.h"
#include "UFCSTLWrapper.h"

namespace ung
{
	UadAnimStack::UadAnimStack(String* pName) :
		mName(!pName ? "" : *pName)
	{
		auto& master = UadMaster::getInstance();
		auto scene = master.getScene();

		//mAnimStack = FbxAnimStack::Create(scene->getFbxScene(), mName.data());
		//BOOST_ASSERT(mAnimStack);

		_buildAnimLayers();
	}

	UadAnimStack::UadAnimStack(FbxAnimStack* animStack, String* pName) :
		mAnimStack(animStack),
		mName(!pName ? "" : *pName)
	{
		BOOST_ASSERT(mAnimStack);

		_buildAnimLayers();
	}

	UadAnimStack::~UadAnimStack()
	{
		ungContainerClear(mAnimLayers);

		if (mAnimStack)
		{
			mAnimStack->Destroy();
		}
	}

	FbxAnimStack* UadAnimStack::getFbxAnimStack() const
	{
		BOOST_ASSERT(mAnimStack);

		return mAnimStack;
	}

	String const& UadAnimStack::getName() const
	{
		return mName;
	}

	void UadAnimStack::addAnimLayer(UadAnimLayer* animLayer)
	{
		BOOST_ASSERT(animLayer);
		BOOST_ASSERT(mAnimStack);

		mAnimStack->AddMember(animLayer->getFbxAnimLayer());

		mAnimLayers.push_back(animLayer);
	}

	int UadAnimStack::getAnimLayerCount() const
	{
		BOOST_ASSERT(mAnimStack);

		/*
		int GetMemberCount() const
		Returns the number of class T objects contained within the collection.
		*/
		return mAnimStack->GetMemberCount();
	}

	UadAnimLayer* UadAnimStack::getAnimLayerByIndex(int index) const
	{
		BOOST_ASSERT(index >= 0 && index < (int)(mAnimLayers.size()));

		return mAnimLayers[index];
	}

	void UadAnimStack::_buildAnimLayers()
	{
		auto animLayerCount = getAnimLayerCount();

		for (int i = 0; i < animLayerCount; ++i)
		{
			/*
			T* GetMember(int pIndex = 0) const
			Returns the member of class T at the given index in the collection.
			*/
			FbxAnimLayer* animLayer = mAnimStack->GetMember<FbxAnimLayer>(i);

			BOOST_ASSERT(animLayer);

			mAnimLayers.push_back(new UadAnimLayer(animLayer));
		}
	}
}//namespace ung