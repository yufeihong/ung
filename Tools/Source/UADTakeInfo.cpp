#include "UADTakeInfo.h"

namespace ung
{
	UadTakeInfo::UadTakeInfo(FbxTakeInfo* takeInfo) :
		mTakeInfo(takeInfo)
	{
		BOOST_ASSERT(mTakeInfo);

		mName = String(mTakeInfo->mName.Buffer());
	}

	UadTakeInfo::~UadTakeInfo()
	{
	}

	FbxTakeInfo* UadTakeInfo::getFbxTakeInfo() const
	{
		BOOST_ASSERT(mTakeInfo);

		return mTakeInfo;
	}

	String const & UadTakeInfo::getName() const
	{
		BOOST_ASSERT(mTakeInfo);

		return mName;
	}

	String UadTakeInfo::getDescription() const
	{
		BOOST_ASSERT(mTakeInfo);

		return String(mTakeInfo->mDescription.Buffer());
	}

	bool UadTakeInfo::isSelected() const
	{
		BOOST_ASSERT(mTakeInfo);

		return mTakeInfo->mSelect;
	}
}//namespace ung