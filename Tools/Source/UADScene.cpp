#include "UADScene.h"

#include "UADMesh.h"
#include "UFCSTLWrapper.h"

namespace ung
{
	UadScene::UadScene() :
		mMesh(nullptr),
		mAnimStackCount(0)
	{
	}

	UadScene::~UadScene()
	{
		//ungContainerClear(takeInfo);
	}

	void UadScene::setAnimStackCount(int c)
	{
		mAnimStackCount = c;
	}

	int UadScene::getAnimStackCount() const
	{
		return mAnimStackCount;
	}

	void UadScene::setActiveAnimStackName(FbxString const& name)
	{
		mActiveAnimStackName = name;
	}

	FbxString const& UadScene::getActiveAnimStackName() const
	{
		return mActiveAnimStackName;
	}

	void UadScene::addTakeInfo(FbxTakeInfo* takeInfo)
	{
		BOOST_ASSERT(takeInfo);

		mTakeInfos.push_back(takeInfo);
	}

	UadMesh* UadScene::createMesh()
	{
		BOOST_ASSERT(!mMesh);

		mMesh = new UadMesh();

		BOOST_ASSERT(mMesh);

		return mMesh;
	}

	UadMesh* UadScene::getMesh() const
	{
		BOOST_ASSERT(mMesh);

		return mMesh;
	}
}//namespace ung