#include "UADImporter.h"

#include "UADIOSettings.h"
#include "UADMaster.h"
#include "UADScene.h"
#include "UADTakeInfo.h"

#include "UFCFilesystem.h"
#include "UFCStringUtilities.h"

namespace ung
{
	UadImporter::UadImporter(UadIOSettings* pSettings,String const& fbxFileName,String* pName) :
		mName(pName == nullptr ? "" : *pName)
	{
		BOOST_ASSERT(pSettings);
		mIOSettings = pSettings;

		_checkFile(fbxFileName);

		auto fbxManager = UadMaster::getInstance().getFbxManager();

		mImporter = FbxImporter::Create(fbxManager, mName.data());
		BOOST_ASSERT(mImporter);

		/*
		bool Initialize(const char* pFileName,int pFileFormat = -1,FbxIOSettings* pIOSettings = ((void*) 0))
		pFileName Name of file to access.
		pFileFormat file format identifier User does not need to specify it by default.if not specified,
		plugin will detect the file format according to file suffix automatically.
		pIOSettings client IOSettings,if not specified,a default IOSettings will be created
		*/
		auto initRet = mImporter->Initialize(mFileFullName.data(), -1, mIOSettings->getFbxIOSettings());
		if (!initRet)
		{
			UNG_PRINT_ONE_LINE("Call to FbxImporter::Initialize() failed.");

			/*
			Get the status object containing the success or failure state.
			FbxStatus& GetStatus()
			*/
			auto errorMsg = mImporter->GetStatus().GetErrorString();
			UNG_EXCEPTION(errorMsg);
		}

		BOOST_ASSERT(mImporter->IsFBX());

		auto AnimStackCount = getAnimStackCount();
		for (int i = 0; i < AnimStackCount; ++i)
		{
			FbxTakeInfo* takeInfo = mImporter->GetTakeInfo(i);
			BOOST_ASSERT(takeInfo);

			mTakeInfos.put(new UadTakeInfo(takeInfo));
		}
	}

	UadImporter::~UadImporter()
	{
		mTakeInfos.deleteAll();
		mTakeInfos.clear();
	}

	UadIOSettings * UadImporter::getIOSettings() const
	{
		BOOST_ASSERT(mIOSettings);

		return mIOSettings;
	}

	String const& UadImporter::getFileFullName() const
	{
		BOOST_ASSERT(!mFileFullName.empty());

		return mFileFullName;
	}

	String const& UadImporter::getFileName() const
	{
		BOOST_ASSERT(!mFileName.empty());

		return mFileName;
	}

	void UadImporter::getFileVersion(int & major, int & minor, int & revision) const
	{
		BOOST_ASSERT(mImporter);

		mImporter->GetFileVersion(major, minor, revision);
	}

	int UadImporter::getAnimStackCount() const
	{
		//This function must be called after FbxImporter::Initialize()

		return mImporter->GetAnimStackCount();
	}

	String UadImporter::getActiveAnimStackName() const
	{
		//This function must be called after FbxImporter::Initialize().

		auto name = mImporter->GetActiveAnimStackName();

		return String(name.Buffer());
	}

	void UadImporter::populateScene()
	{
		BOOST_ASSERT(mImporter);

		auto scene = UadMaster::getInstance().getScene();

		//scene->clear();

		/*
		FbxImporter对象将FBX文件中的elements填充到一个FbxScene对象。

		Import the currently opened file into a scene.
		bool Import(FbxDocument* pDocument,bool pNonBlocking = false)
		pDocument Document to fill with file content.
		pNonBlocking	If true,the import process will be executed in a new thread,allowing it to be
		non-blocking.To determine if the import finished,refer to the function IsImporting().
		*/
		//auto importRet = mImporter->Import(scene->getFbxScene());
		//if (!importRet)
		//{
		//	UNG_EXCEPTION("Failed to import fbx file to a scene.\n");
		//}

		//FbxScene对象被填充后，FbxImporter对象就可以销毁了。
		mImporter->Destroy();
		mImporter = nullptr;
	}

	//---------------------------------------------------------------------------

	void UadImporter::_checkFile(String const & fileName)
	{
		auto& fs = Filesystem::getInstance();

		String fullName = String(FBX_PATH) + fileName;

		const char* fullNameCStr = fullName.c_str();

		auto isExist = fs.isExists(fullNameCStr);
		if (!isExist)
		{
			UNG_EXCEPTION("The input fbx file does not exist.");
		}

		auto validRet = fs.isRegularFile(fullNameCStr);
		BOOST_ASSERT(validRet && "The input fbx file is not a regular file.");

		auto extRet = fs.hasExtension(fullNameCStr);
		BOOST_ASSERT(extRet && "The input fbx file does not have extension.");

		auto ext = fs.getFileExtension(fullNameCStr);
		BOOST_ASSERT(StringUtilities::equalInSensitive(ext, String("fbx")));

		mFileFullName = fullName;
		mFileName = fs.getFileName(fullNameCStr);
	}
}//namespace ung