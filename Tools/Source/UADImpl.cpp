#include "UADImpl.h"

#include "UADScene.h"
#include "UADMesh.h"
#include "UADSubMesh.h"
#include "UFCFilesystem.h"

namespace ung
{
	void checkFile(const char* fileFullName)
	{
		auto& fs = Filesystem::getInstance();

		auto isExist = fs.isExists(fileFullName);
		if (!isExist)
		{
			UNG_EXCEPTION("The input fbx file does not exist.");
		}

		auto validRet = fs.isRegularFile(fileFullName);
		BOOST_ASSERT(validRet && "The input fbx file is not a regular file.");

		auto extRet = fs.hasExtension(fileFullName);
		BOOST_ASSERT(extRet && "The input fbx file does not have extension.");

		auto ext = fs.getFileExtension(fileFullName);
		BOOST_ASSERT(StringUtilities::equalInSensitive(ext, String("fbx")));
	}

	void checkNode(FbxNode* node)
	{
		std::vector<int> slots;
		int slotsSize{ 1 };
		for (int i = 0; i < slotsSize; ++i)
		{
			slots.push_back(0);
		}

		auto attributeCount = node->GetNodeAttributeCount();

		for (int i = 0; i < attributeCount; ++i)
		{
			switch (node->GetNodeAttributeByIndex(i)->GetAttributeType())
			{
			case FbxNodeAttribute::eMesh:
				++slots[0];
				break;
			}
		}

		for (int i = 0; i < slotsSize; ++i)
		{
			BOOST_ASSERT(slots[i] < 2);
		}

		slots.clear();
	}

	void processFbxMesh(FbxNode* node)
	{
		/*
		FbxMesh* GetMesh()
		Get the node attribute casted to a FbxMesh pointer.
		Returns:
		Pointer to the mesh object.
		Remarks:
		This method will try to process the default node attribute first. If it cannot find it, it will 
		scan the list of connected node attributes and get the first object that is a 
		FbxNodeAttribute::eMesh.
		(如果一个节点上面关联了两个mesh的话，那么第二个mesh永远也处理不到)
		If the above search failed to get a valid pointer or it cannot be successfully casted, this 
		method will return NULL.
		*/
		auto mesh = node->GetMesh();
		BOOST_ASSERT(mesh);

		/*
		bool IsTriangleMesh() const
		Determines if the mesh is composed entirely of triangles.
		*/
		auto isTriangleMesh = mesh->IsTriangleMesh();
		BOOST_ASSERT(isTriangleMesh);

		//创建一个sub mesh
		auto subMesh = UadScene::getInstance().getMesh()->createSubMesh(mesh);
	}
}//namespace ung