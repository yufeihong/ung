#include "UADTexture.h"

#include "UADMaster.h"
#include "UADScene.h"

namespace ung
{
	UadTexture::UadTexture(String* pName) :
		mName(!pName ? "" : *pName)
	{
		auto& master = UadMaster::getInstance();
		auto scene = master.getScene();

		//mTexture = FbxFileTexture::Create(scene->getFbxScene(), mName.data());

		//BOOST_ASSERT(mTexture);
	}

	UadTexture::~UadTexture()
	{
		if (mTexture)
		{
			mTexture->Destroy();
		}
	}
}//namespace ung