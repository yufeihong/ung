#include "UAD.h"

#include "UADImpl.h"
#include "UADScene.h"
#include "UADMesh.h"
#include "UADSubMesh.h"

namespace ung
{
	FbxManager* createManager()
	{
		//创建memory管理器
		auto manager = FbxManager::Create();

		BOOST_ASSERT(manager);

		//获取FBX SDK的版本号，包括修订编号以及发布日期
		auto version = manager->GetVersion();

		FbxString dllPath = FbxGetApplicationDirectory();
		manager->LoadPluginsDirectory(dllPath.Buffer());

		return manager;
	}

	void buildIOSettings(FbxManager* manager)
	{
		auto settings = FbxIOSettings::Create(manager, IOSROOT);

		BOOST_ASSERT(settings);

		manager->SetIOSettings(settings);
	}

	FbxScene* createScene(FbxManager* manager, const char* name)
	{
		auto scene = FbxScene::Create(manager,name);

		BOOST_ASSERT(scene);

		/*
		void Clear()
		Clear the scene content by deleting the node tree below the root node and restoring 
		default settings.
		*/
		scene->Clear();

		return scene;
	}

	FbxImporter* createImporter(FbxManager* manager, const char* name,const char* fileFullName)
	{
		auto importer = FbxImporter::Create(manager,name);

		BOOST_ASSERT(importer);

		checkFile(fileFullName);

		/*
		bool Initialize(const char* pFileName,int pFileFormat = -1,FbxIOSettings* pIOSettings = ((void*) 0))
		pFileName Name of file to access.
		pFileFormat file format identifier User does not need to specify it by default.if not specified,
		plugin will detect the file format according to file suffix automatically.
		pIOSettings client IOSettings,if not specified,a default IOSettings will be created
		*/
		auto ret = importer->Initialize(fileFullName, -1,manager->GetIOSettings());
		if (!ret)
		{
			UNG_PRINT_ONE_LINE("Failed to initialize FbxImporter.");

			/*
			Get the status object containing the success or failure state.
			FbxStatus& GetStatus()
			*/
			auto errorMsg = importer->GetStatus().GetErrorString();

			if (importer->GetStatus().GetCode() == FbxStatus::eInvalidFileVersion)
			{
				int sdkMajor, sdkMinor, sdkRevision;
				//Get the current default FBX file format version number for this version of the FBX SDK.
				FbxManager::GetFileFormatVersion(sdkMajor, sdkMinor, sdkRevision);
				int fileMajor, fileMinor, fileRevision;
				//获取输入的FBX文件版本信息
				importer->GetFileVersion(fileMajor, fileMinor, fileRevision);

				//抛出异常
			}

			UNG_EXCEPTION(errorMsg);
		}

		BOOST_ASSERT(importer->IsFBX());

		return importer;
	}

	void populateScene(FbxManager* manager,FbxScene* scene, FbxImporter* importer)
	{
		if (importer->IsFBX())
		{
			auto& myScene = UadScene::getInstance();

			auto animStackCount = importer->GetAnimStackCount();

			myScene.setAnimStackCount(animStackCount);

			auto activeAnimStackName = importer->GetActiveAnimStackName();

			myScene.setActiveAnimStackName(activeAnimStackName);

			for (int i = 0; i < animStackCount;++i)
			{
				auto takeInfo = importer->GetTakeInfo(i);

				myScene.addTakeInfo(takeInfo);
			}
		}

		/*
		FbxImporter对象将FBX文件中的elements填充到一个FbxScene对象。

		Import the currently opened file into a scene.
		bool Import(FbxDocument* pDocument,bool pNonBlocking = false)
		pDocument Document to fill with file content.
		pNonBlocking	If true,the import process will be executed in a new thread,allowing it to be
		non-blocking.To determine if the import finished,refer to the function IsImporting().
		*/
		auto ret = importer->Import(scene);
		if (!ret)
		{
			UNG_EXCEPTION("Failed to import fbx file to a scene.\n");
		}

		//FbxScene对象被填充后，FbxImporter对象就可以销毁了。
		importer->Destroy();
		importer = nullptr;

		FbxGeometryConverter geometryConverter(manager);

		/*
		bool Triangulate(FbxScene* pScene,bool pReplace,bool pLegacy = false)
		Triangulate all node attributes in the scene that can be triangulated.
		Parameters:
		pScene,The scene to iterate through to triangulate meshes.
		pReplace,If true, replace the original meshes with the new triangulated meshes on all the 
		nodes, and delete the original meshes. Otherwise, original meshes are left untouched.
		pLegacy,If true, use legacy triangulation method that does not support holes in geometry.
		Provided for backward compatibility.
		Returns:
		true if all node attributes that can be triangulated were triangulated successfully.
		Remarks:
		The function will still iterate through all meshes regardless if one fails to triangulate, but 
		will return false in that case. This function currently only supports node attribute of type 
		eMesh, ePatch, eNurbs or eNurbsSurface.
		*/
		ret = geometryConverter.Triangulate(scene,true);
		BOOST_ASSERT(ret);

		//创建mesh
		UadScene::getInstance().createMesh();
	}

	void processNode(FbxNode* node)
	{
#if UAD_DEBUGMODE
		checkNode(node);
#endif

		auto attributeCount = node->GetNodeAttributeCount();

		for (int i = 0; i < attributeCount; ++i)
		{
			auto attribute = node->GetNodeAttributeByIndex(i);

			BOOST_ASSERT(attribute);

			auto attributeType = attribute->GetAttributeType();

			switch (attributeType)
			{
			case FbxNodeAttribute::eMesh:
				processFbxMesh(node);
				break;
			}
		}

		auto childCount = node->GetChildCount();

		for (int i = 0; i < childCount; ++ i)
		{
			auto childNode = node->GetChild(i);

			BOOST_ASSERT(childNode);

			processNode(childNode);
		}
	}

	FbxExporter* createExporter(FbxManager* manager, const char* name, const char* fileFullName)
	{
		auto exporter = FbxExporter::Create(manager,name);
		BOOST_ASSERT(exporter);

		int format = -1;

		/*
		FbxIOPluginRegistry* GetIOPluginRegistry()	const
		Access to the unique FbxIOPluginRegistry object.
		Returns:The pointer to the user FbxIOPluginRegistry
		*/
		auto IORegistry = manager->GetIOPluginRegistry();

		/*
		int GetWriterFormatCount() const
		Returns:The number of file formats that can be exported.
		Remarks:Multiple identifiers for the same format count as different file formats. For 
		example,eFBX_BINARY, eFBX_ASCII and eFBX_ENCRYPTED(加密) are counted as three 
		separate file formats.
		*/
		int formatCount = IORegistry->GetWriterFormatCount();

		if (format < 0 || format >= formatCount)
		{
			/*
			int GetNativeWriterFormat()
			Returns the ID of the native file format.
			*/
			format = IORegistry->GetNativeWriterFormat();
		}

		for (int i = 0; i < formatCount; ++i)
		{
			/*
			bool WriterIsFBX(int pFileFormat) const
			Verifies if the file format of the Writer is FBX.
			pFileFormat:
			The file format identifier.
			*/
			if (IORegistry->WriterIsFBX(i))
			{
				/*
				const char* GetWriterFormatDescription(int pFileFormat) const
				Returns the description of an exportable file format.
				pFileFormat:
				The file format identifier.
				Returns:
				A pointer to the character representation of the description.
				*/
				auto desc = IORegistry->GetWriterFormatDescription(i);
				if (StringUtilities::containInSensitive(desc, "ascii"))
				{
					format = i;

					break;
				}
			}
		}

		bool ret = exporter->Initialize(fileFullName,format,manager->GetIOSettings());

		if (!ret)
		{
			UNG_PRINT_ONE_LINE("Failed to initialize FbxExporter.");

			auto errorMsg = exporter->GetStatus().GetErrorString();
			UNG_EXCEPTION(errorMsg);
		}

		return exporter;
	}

	void destroyManager(FbxManager* manager)
	{
		BOOST_ASSERT(manager);

		/*
		销毁memory管理器，同时也销毁了它所handling的其它对象
		All the objects that
		1,have been allocated by the memory manager, and that
		2,have not been explicitly destroyed
		will be automatically destroyed.
		*/
		manager->Destroy();

		manager = nullptr;
	}
}//namespace ung