#include "UADNode.h"

#include "UADNodeAttribute.h"
#include "UFCSTLWrapper.h"

namespace ung
{
	UadNode::UadNode(FbxNode* pNode) :
		mNode(pNode),
		mDefaultAttribute(nullptr)
	{
		BOOST_ASSERT(mNode);

		mName = mNode->GetName();

		/*
		FbxNodeAttribute* GetNodeAttribute()
		Get the default node attribute.
		The default node attribute is the attribute that has been set by the call to 
		SetNodeAttribute().
		Returns:
		Pointer to the default node attribute or NULL if the node doesn't have a node attribute.
		*/
		FbxNodeAttribute* defaultAttribute = mNode->GetNodeAttribute();
		if (defaultAttribute)
		{
			mDefaultAttribute = new UadNodeAttribute(defaultAttribute);
		}

		auto attributeCount = mNode->GetNodeAttributeCount();

		for (int i = 0; i < attributeCount; ++i)
		{
			auto ab = mNode->GetNodeAttributeByIndex(i);

			BOOST_ASSERT(ab);

			mAttributes.push_back(new UadNodeAttribute(ab));
		}
	}

	UadNode::~UadNode()
	{
		delete mDefaultAttribute;

		ungContainerClear(mAttributes);
		
		if (mNode)
		{
			mNode->Destroy();
		}
	}

	FbxNode* UadNode::getFbxNode() const
	{
		BOOST_ASSERT(mNode);

		return mNode;
	}

	String const& UadNode::getName() const
	{
		return mName;
	}

	bool UadNode::isVisibility() const
	{
		BOOST_ASSERT(mNode);

		/*
		bool GetVisibility() const
		Get the current value of the Visibility property.
		Returns:
		false if the Visibility property value is 0.0 and true for any other value.
		Remarks:
		This method expects the Visibility property to exist and to be valid. If this condition is not 
		met, the returned value will be false.
		*/
		return mNode->GetVisibility();
	}

	int UadNode::getChildNodeCount(bool isRecursive) const
	{
		BOOST_ASSERT(mNode);

		auto count = mNode->GetChildCount(isRecursive);

		return count;
	}

	FbxNode* UadNode::getChildByIndex(int index) const
	{
		BOOST_ASSERT(mNode);

		BOOST_ASSERT(index >= 0 && index < getChildNodeCount());

		auto child = mNode->GetChild(index);
		BOOST_ASSERT(child);

		return child;
	}

	FbxDouble3 UadNode::getLocalTranslation() const
	{
		/*
		FbxPropertyT<FbxDouble3> LclTranslation:
		This property contains the translation information of the node.
		Default value is 0.,0.,0.
		*/
		return mNode->LclTranslation.Get();
	}

	FbxDouble3 UadNode::getLocalRotation() const
	{
		/*
		FbxPropertyT<FbxDouble3> LclRotation
		This property contains the rotation information of the node.
		Default value is 0.,0.,0.
		*/
		return mNode->LclRotation.Get();
	}

	FbxDouble3 UadNode::getLocalScaling() const
	{
		/*
		FbxPropertyT<FbxDouble3> LclScaling
		This property contains the scaling information of the node.
		Default value is 1.,1.,1.
		*/
		return mNode->LclScaling.Get();
	}

	int UadNode::getAttributeCount() const
	{
		return mNode->GetNodeAttributeCount();
	}

	void UadNode::setDefaultAttribute(UadNodeAttribute * ab)
	{
		BOOST_ASSERT(ab);
		BOOST_ASSERT(mNode);

		/*
		FbxNodeAttribute* SetNodeAttribute(FbxNodeAttribute* pNodeAttribute)
		Set the node attribute.
		Parameters:
		pNodeAttribute,Node attribute object
		Returns:
		Pointer to previous node attribute object. NULL if the node didn't have a node attribute or 
		if the new node attribute is equal to the one currently set.
		Remarks:
		A node attribute can be shared between nodes.
		If this node has more than one attribute (added via the AddAttribute() method), this call 
		will destroy all, but the default node attribute.
		*/
		auto ret = mNode->SetNodeAttribute(ab->getFbxNodeAttribute());

		BOOST_ASSERT(ret);
	}

	UadNodeAttribute * UadNode::getDefaultAttribute() const
	{
		return mDefaultAttribute;
	}

	UadNodeAttribute* UadNode::getAttributeByIndex(int index) const
	{
		return mAttributes[index];
	}

	int UadNode::getMaterialCount() const
	{
		/*
		int GetMaterialCount() const
		Returns:
		The number of materials applied to this node.
		Remarks:
		If this node has an instanced node attribute, it is possible to have a material applied to 
		this node more than once. The material count may not reflect the distinct material count.
		*/
		auto mc = mNode->GetMaterialCount();

		return mc;
	}

	FbxSurfaceMaterial * UadNode::getMaterialByIndex(int index) const
	{
		BOOST_ASSERT(index >= 0 && index < getMaterialCount());

		/*
		FbxSurfaceMaterial* GetMaterial(int pIndex) const
		Access a material on this node.
		Parameters:
		pIndex	Valid range is [0, GetMaterialCount() - 1]
		Returns:
		The pIndex-th material, or NULL if pIndex is invalid.
		*/
		auto material = mNode->GetMaterial(index);

		BOOST_ASSERT(material);

		return material;
	}

	void UadNode::addChild(UadNode* node)
	{
		BOOST_ASSERT(node);

		/*
		bool AddChild(FbxNode* pNode)
		Add a child node and its underlying node tree.
		pNode,Node we want to make child of this.
		Returns:
		true on success, false if pNode is NULL or the system is unable to make the connection.
		Remarks:
		If pNode already has a parent, first it is removed from current parent and then added to 
		this one.
		*/
		auto ret = mNode->AddChild(node->getFbxNode());

		BOOST_ASSERT(ret);
	}
}//namespace ung