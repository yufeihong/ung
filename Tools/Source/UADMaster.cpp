#include "UADMaster.h"

#include "UADIOPluginRegistry.h"
#include "UADScene.h"
#include "UADNode.h"
#include "UADNodeAttribute.h"
#include "UADIOSettings.h"
#include "UADImporter.h"
#include "UADExporter.h"

namespace ung
{
	UadMaster::UadMaster() :
		mScene(nullptr),
		mImporter(nullptr),
		mExporter(nullptr)
	{
		_createFbxManager();

		_getSDKVersion();

		_loadDLL();
	}

	UadMaster::~UadMaster()
	{
		if (mImporter)
		{
			delete mImporter;
		}

		if (mExporter)
		{
			delete mExporter;
		}

		if (mScene)
		{
			delete mScene;
		}

		_destroyFbxManager();
	}

	void UadMaster::createScene(String* pName)
	{
		//mScene = new UadScene(pName);

		//BOOST_ASSERT(mScene);
	}

	void UadMaster::populateScene()
	{
		BOOST_ASSERT(mScene);
		BOOST_ASSERT(mImporter);

		mImporter->populateScene();
	}

	//void UadMaster::traverse(UadNode* node)
	//{
	//	auto attributeCount = node->getAttributeCount();

	//	if (attributeCount > 0)
	//	{
	//	}

	//	auto childCount = node->getChildNodeCount();

	//	for (int i = 0; i < childCount; ++i)
	//	{
	//		auto child = node->getChildByIndex(i);

	//		//traverse(child);
	//	}
	//}

	UadIOSettings* UadMaster::createIOSettings()
	{
		auto ios = new UadIOSettings();

		BOOST_ASSERT(ios);

		return ios;
	}

	UadImporter* UadMaster::createImporter(UadIOSettings* pIOS,String const& fbxFileName,String* pName)
	{
		mImporter = new UadImporter(pIOS,fbxFileName,pName);

		BOOST_ASSERT(mImporter);

		return mImporter;
	}

	UadExporter* UadMaster::createExporter(UadIOSettings* pSettings, String const& fileName,bool embedMedia, String* pName)
	{
		mExporter = new UadExporter(pSettings, fileName, embedMedia,pName);

		BOOST_ASSERT(mExporter);

		return mExporter;
	}

	FbxManager* UadMaster::getFbxManager() const
	{
		BOOST_ASSERT(mFbxManager);

		return mFbxManager;
	}

	String const& UadMaster::getSDKVersion() const
	{
		BOOST_ASSERT(!mSDKVersion.empty());

		return mSDKVersion;
	}

	UadIOPluginRegistry& UadMaster::getIOPluginRegistry() const
	{
		return UadIOPluginRegistry::getInstance();
	}

	UadScene* UadMaster::getScene() const
	{
		BOOST_ASSERT(mScene);

		return mScene;
	}

	UadImporter* UadMaster::getImporter() const
	{
		BOOST_ASSERT(mImporter);

		return mImporter;
	}

	UadExporter* UadMaster::getExporter() const
	{
		BOOST_ASSERT(mExporter);

		return mExporter;
	}
}//namespace ung