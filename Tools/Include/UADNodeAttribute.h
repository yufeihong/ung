/*!
 * \brief
 * 节点Attribute
 * \file UADNodeAttribute.h
 *
 * \author Su Yang
 *
 * \date 2017/06/23
 */
#ifndef _UAD_NODEATTRIBUTE_H_
#define _UAD_NODEATTRIBUTE_H_

#ifndef _UAD_CONFIG_H_
#include "UADConfig.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 节点Attribute
	 * \class UadNodeAttribute
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/23
	 *
	 * \todo
	 */
	class UadNodeAttribute
	{
	public:
		/*!
		 * \remarks 构造函数
		 * \return 
		*/
		explicit UadNodeAttribute(FbxNodeAttribute* ab);

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~UadNodeAttribute();

		/*!
		 * \remarks 获取名字
		 * \return 
		*/
		String const& getName() const;

		/*!
		 * \remarks 获取类型
		 * \return 
		*/
		FbxNodeAttribute::EType getType() const;

		/*!
		 * \remarks 获取类型名字
		 * \return 
		*/
		String const& getTypeName() const;

		/*!
		 * \remarks 获取FbxNodeAttribute
		 * \return 
		*/
		FbxNodeAttribute* getFbxNodeAttribute() const;

	private:
		FbxNodeAttribute* mAttribute;
		String mName;
		FbxNodeAttribute::EType mType;
		String mTypeName;
	};
}//namespace ung

#endif//_UAD_NODEATTRIBUTE_H_