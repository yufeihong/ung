/*!
 * \brief
 * 动画层
 * \file UADAnimLayer.h
 *
 * \author Su Yang
 *
 * \date 2017/06/30
 */
#ifndef _UAD_ANIMLAYER_H_
#define _UAD_ANIMLAYER_H_

#ifndef _UAD_CONFIG_H_
#include "UADConfig.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 动画层
	 * \class UadAnimLayer
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/30
	 *
	 * \todo
	 */
	class UadAnimLayer
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		 * \param String * pName
		*/
		UadAnimLayer(String* pName = nullptr);

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param FbxAnimLayer * animLayer
		 * \param String * pName
		*/
		UadAnimLayer(FbxAnimLayer* animLayer,String* pName = nullptr);

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~UadAnimLayer();

		/*!
		 * \remarks 获取FbxAnimLayer
		 * \return 
		*/
		FbxAnimLayer* getFbxAnimLayer() const;

	private:
		FbxAnimLayer* mAnimLayer;
		String mName;
	};
}//namespace ung

#endif//_UAD_ANIMLAYER_H_