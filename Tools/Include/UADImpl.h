/*!
 * \brief
 * impl
 * \file UADImpl.h
 *
 * \author Su Yang
 *
 * \date 2017/07/03
 */
#ifndef _UAD_IMPL_H_
#define _UAD_IMPL_H_

#ifndef _UAD_CONFIG_H_
#include "UADConfig.h"
#endif

namespace ung
{
	/*!
	 * \remarks 检查文件是否合法
	 * \return 
	 * \param const char * fileFullName
	*/
	void checkFile(const char* fileFullName);

	/*!
	 * \remarks 用于检查一个节点是否关联了两个或多个相同类型的attribute
	 * \return 
	 * \param FbxNode* node
	*/
	void checkNode(FbxNode* node);

	void readPosition();

	/*!
	 * \remarks 处理FbxMesh
	 * \return 
	 * \param FbxNode* node
	*/
	void processFbxMesh(FbxNode* node);
}//namespace ung

#endif//_UAD_IMPL_H_