/*!
 * \brief
 * sub mesh
 * \file UADSubMesh.h
 *
 * \author Su Yang
 *
 * \date 2017/07/03
 */
#ifndef _UAD_SUBMESH_H_
#define _UAD_SUBMESH_H_

#ifndef _UAD_CONFIG_H_
#include "UADConfig.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * sub mesh
	 * \class UadSubMesh
	 *
	 * \author Su Yang
	 *
	 * \date 2017/07/03
	 *
	 * \todo
	 */
	class UadSubMesh
	{
	public:
		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param FbxMesh* fbxMesh
		*/
		UadSubMesh(FbxMesh* fbxMesh);

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~UadSubMesh();

		/*!
		 * \remarks 读取FbxMesh数据
		 * \return 
		*/
		void readFbxMesh();

	private:
		/*!
		 * \remarks 读取position数据
		 * \return 
		*/
		void _readPositionData();

		/*!
		 * \remarks 读取index数据
		 * \return 
		*/
		void _readIndexData();

	private:
		FbxMesh* mFbxMesh;
		unsigned short mTriangleCount;
		unsigned short mVertexCount;
		FbxVector4* mPositionBuffer;
		STL_VECTOR(unsigned short) mIndices;
	};
}//namespace ung

#endif//_UAD_SUBMESH_H_