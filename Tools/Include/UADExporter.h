/*!
 * \brief
 * 导出器
 * \file UADExporter.h
 *
 * \author Su Yang
 *
 * \date 2017/06/20
 */
#ifndef _UAD_EXPORTER_H_
#define _UAD_EXPORTER_H_

#ifndef _UAD_CONFIG_H_
#include "UADConfig.h"
#endif

namespace ung
{
	class UadIOSettings;
	class UadScene;

	/*!
	 * \brief
	 * 导出器
	 * \class UadExporter
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/23
	 *
	 * \todo
	 */
	class UadExporter
	{
	public:
		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param UadIOSettings * pSettings
		 * \param String const & fileName
		 * \param bool embedMedia
		 * \param String * pName
		*/
		UadExporter(UadIOSettings* pSettings,String const& fileName,bool embedMedia = false,String* pName = nullptr);

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~UadExporter();

		/*!
		 * \remarks 获取输出的FBX文件版本信息
		 * \return 
		 * \param int & major
		 * \param int & minor
		 * \param int & revision
		*/
		void getFileVersion(int& major,int& minor,int& revision) const;

		/*!
		 * \remarks 获取format
		 * \return 
		*/
		int getFormat() const;

		/*!
		 * \remarks 导出
		 * \return 
		 * \param UadScene * pScene
		*/
		void doExport(UadScene* pScene);

	private:
		FbxExporter* mExporter;
		String mName;
		UadIOSettings* mIOSettings;
		int mFormat;
	};
}//namespace ung

#endif//_UAD_EXPORTER_H_