/*!
 * \brief
 * take info
 * \file UADTakeInfo.h
 *
 * \author Su Yang
 *
 * \date 2017/06/23
 */
#ifndef _UAD_TAKEINFO_H_
#define _UAD_TAKEINFO_H_

#ifndef _UAD_CONFIG_H_
#include "UADConfig.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * take info
	 * \class UadTakeInfo
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/23
	 *
	 * \todo
	 */
	class UadTakeInfo
	{
	public:
		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param FbxTakeInfo * takeInfo
		*/
		explicit UadTakeInfo(FbxTakeInfo* takeInfo);

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~UadTakeInfo();

		/*!
		 * \remarks 获取FbxTakeInfo
		 * \return 
		*/
		FbxTakeInfo* getFbxTakeInfo() const;

		/*!
		 * \remarks 获取名字
		 * \return 
		*/
		String const& getName() const;

		/*!
		 * \remarks 获取描述
		 * \return 
		*/
		String getDescription() const;

		/*!
		 * \remarks 是否被选
		 * \return 
		*/
		bool isSelected() const;

	private:
		FbxTakeInfo* mTakeInfo;
		String mName;
	};
}//namespace ung

#endif//_UAD_TAKEINFO_H_