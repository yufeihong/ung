/*!
 * \brief
 * 声明
 * \file UAD.h
 *
 * \author Su Yang
 *
 * \date 2017/07/02
 */
#ifndef _UAD_H_
#define _UAD_H_

#ifndef _UAD_CONFIG_H_
#include "UADConfig.h"
#endif

namespace ung
{
	/*!
	 * \remarks 创建管理器，载入插件
	 * \return 
	*/
	FbxManager* createManager();

	/*!
	 * \remarks 构建IO过滤器
	 * \return 
	 * \param FbxManager * manager
	*/
	void buildIOSettings(FbxManager* manager);

	/*!
	 * \remarks 创建场景
	 * \return 
	 * \param FbxManager * manager
	 * \param const char * name
	*/
	FbxScene* createScene(FbxManager* manager, const char* name);

	/*!
	 * \remarks 创建导入器
	 * \return 
	 * \param FbxManager * manager
	 * \param const char * name
	 * \param const char * fileFullName
	*/
	FbxImporter* createImporter(FbxManager* manager, const char* name,const char* fileFullName);

	/*!
	 * \remarks 填充场景，并且对整个场景进行三角化
	 * \return 
	 * \param FbxManager * manager
	 * \param FbxScene * scene
	 * \param FbxImporter * importer
	*/
	void populateScene(FbxManager* manager,FbxScene* scene,FbxImporter* importer);

	/*!
	 * \remarks 递归处理节点
	 * \return 
	 * \param FbxNode * node
	*/
	void processNode(FbxNode* node);

	/*!
	 * \remarks 创建导出器
	 * \return 
	 * \param FbxManager * manager
	 * \param const char * name
	 * \param const char * fileFullName
	*/
	FbxExporter* createExporter(FbxManager* manager, const char* name,const char* fileFullName);

	/*!
	 * \remarks 销毁管理器
	 * \return 
	 * \param FbxManager * manager
	*/
	void destroyManager(FbxManager* manager);
}//namespace ung

#endif//_UAD_H_