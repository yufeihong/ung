/*!
 * \brief
 * 材质
 * \file UADMaterial.h
 *
 * \author Su Yang
 *
 * \date 2017/06/30
 */
#ifndef _UAD_MATERIAL_H_
#define _UAD_MATERIAL_H_

#ifndef _UAD_CONFIG_H_
#include "UADConfig.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 材质
	 * \class UadMaterial
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/30
	 *
	 * \todo
	 */
	class UadMaterial
	{
	public:
		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param String const & name
		*/
		UadMaterial(String const& name);

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~UadMaterial();

		/*!
		 * \remarks 获取Diffuse通道的纹理数量
		 * \return 
		*/
		int getDiffuseTextureCount() const;

	private:
		FbxSurfacePhong* mPhong;
		String mName;
	};
}//namespace ung

#endif//_UAD_MATERIAL_H_