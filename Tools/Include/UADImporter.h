/*!
 * \brief
 * 导入器
 * \file UADImporter.h
 *
 * \author Su Yang
 *
 * \date 2017/06/20
 */
#ifndef _UAD_IMPORTER_H_
#define _UAD_IMPORTER_H_

#ifndef _UAD_CONFIG_H_
#include "UADConfig.h"
#endif

#ifndef _UFC_STLWRAPPER_H_
#include "UFCSTLWrapper.h"
#endif

namespace ung
{
	class UadIOSettings;
	class UadTakeInfo;

	/*!
	 * \brief
	 * 导入器
	 * \class UadImporter
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/20
	 *
	 * \todo
	 */
	class UadImporter
	{
	public:
		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param UadIOSettings* pSettings
		 * \param String const & fbxFileName fbx文件名(不包含路径)
		 * \param String* pName 导入器名字
		*/
		UadImporter(UadIOSettings* pSettings,String const& fbxFileName,String* pName = nullptr);

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~UadImporter();

		/*!
		 * \remarks 获取UadIOSettings
		 * \return 
		*/
		UadIOSettings* getIOSettings() const;

		/*!
		 * \remarks 获取输入的FBX文件的全名
		 * \return 
		*/
		String const& getFileFullName() const;

		/*!
		 * \remarks 输入的FBX文件名
		 * \return 
		*/
		String const& getFileName() const;

		/*!
		 * \remarks 获取输入的FBX文件版本信息
		 * \return 
		 * \param int & major
		 * \param int & minor
		 * \param int & revision
		*/
		void getFileVersion(int& major,int& minor,int& revision) const;

		/*!
		 * \remarks Get the number of available animation stacks in the file.
		 * \return 
		*/
		int getAnimStackCount() const;

		/*!
		 * \remarks Return the active animation stack name.
		 * \return Active animation stack name if there is one, otherwise returns an empty string.
		*/
		String getActiveAnimStackName() const;

		/*!
		 * \remarks 填充场景
		 * \return 
		*/
		void populateScene();

	private:
		/*!
		 * \remarks 检查输入的FBX文件
		 * \return 
		 * \param String const& fileName fbx文件名(不包含路径)
		*/
		void _checkFile(String const& fileName);

	private:
		FbxImporter* mImporter;
		String mName;
		String mFileFullName;
		String mFileName;
		UadIOSettings* mIOSettings;
		SetWrapper<UadTakeInfo*> mTakeInfos;
	};
}//namespace ung

#endif//_UAD_IMPORTER_H_