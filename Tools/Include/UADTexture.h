/*!
 * \brief
 * 纹理
 * \file UADTexture.h
 *
 * \author Su Yang
 *
 * \date 2017/06/30
 */
#ifndef _UAD_TEXTURE_H_
#define _UAD_TEXTURE_H_

#ifndef _UAD_CONFIG_H_
#include "UADConfig.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 纹理
	 * \class UadTexture
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/30
	 *
	 * \todo
	 */
	class UadTexture
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		 * \param String * pName
		*/
		UadTexture(String* pName = nullptr);

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~UadTexture();

	private:
		FbxFileTexture* mTexture;
		String mName;
	};
}//namespace ung

#endif//_UAD_TEXTURE_H_