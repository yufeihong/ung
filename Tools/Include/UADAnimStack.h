/*!
 * \brief
 * 动画栈
 * \file UADAnimStack.h
 *
 * \author Su Yang
 *
 * \date 2017/06/30
 */
#ifndef _UAD_ANIMSTACK_H_
#define _UAD_ANIMSTACK_H_

#ifndef _UAD_CONFIG_H_
#include "UADConfig.h"
#endif

namespace ung
{
	class UadAnimLayer;

	/*!
	 * \brief
	 * 动画栈
	 * \class UadAnimStack
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/30
	 *
	 * \todo
	 */
	class UadAnimStack
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		 * \param String * pName
		*/
		UadAnimStack(String* pName = nullptr);

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param FbxAnimStack* animStack
		 * \param String* pName
		*/
		UadAnimStack(FbxAnimStack* animStack,String* pName = nullptr);

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~UadAnimStack();

		/*!
		 * \remarks 获取FbxAnimStack
		 * \return 
		*/
		FbxAnimStack* getFbxAnimStack() const;

		/*!
		 * \remarks 获取名字
		 * \return 
		*/
		String const& getName() const;

		/*!
		 * \remarks 添加动画层
		 * \return 
		 * \param UadAnimLayer * animLayer
		*/
		void addAnimLayer(UadAnimLayer* animLayer);

		/*!
		 * \remarks 获取anim layer的数量
		 * \return 
		*/
		int getAnimLayerCount() const;

		/*!
		 * \remarks 获取索引所指定的anim layer
		 * \return 
		 * \param int index
		*/
		UadAnimLayer* getAnimLayerByIndex(int index) const;

	private:
		/*!
		 * \remarks 获取，构建该stack里面的layers
		 * \return 
		*/
		void _buildAnimLayers();

	private:
		FbxAnimStack* mAnimStack;
		String mName;
		STL_VECTOR(UadAnimLayer*) mAnimLayers;
	};
}//namespace ung

#endif//_UAD_ANIMSTACK_H_