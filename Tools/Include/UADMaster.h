/*!
 * \brief
 * 外观
 * \file UADMaster.h
 *
 * \author Su Yang
 *
 * \date 2017/06/20
 */
#ifndef _UAD_MASTER_H_
#define _UAD_MASTER_H_

#ifndef _UAD_CONFIG_H_
#include "UADConfig.h"
#endif

#ifndef _UBS_SINGLETON_H_
#include "UBSSingleton.h"
#endif

namespace ung
{
	class UadIOPluginRegistry;
	class UadScene;
	class UadIOSettings;
	class UadImporter;
	class UadExporter;

	/*!
	 * \brief
	 * 外观
	 * \class UadMaster
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/20
	 *
	 * \todo
	 */
	class UadMaster : public Singleton<UadMaster>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		UadMaster();

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~UadMaster();

		/*!
		 * \remarks 创建场景
		 * \return 
		 * \param String* pName 场景名字
		*/
		void createScene(String* pName = nullptr);

		/*!
		 * \remarks 填充场景
		 * \return 
		*/
		void populateScene();

		/*!
		 * \remarks 遍历场景
		 * \return 
		 * \param UadNode* node
		*/
		//void traverse(UadNode* node);

		/*!
		 * \remarks 创建IOSettings对象
		 * \return 
		*/
		UadIOSettings* createIOSettings();

		/*!
		 * \remarks 创建导入器对象
		 * \return 
		 * \param UadIOSettings* pIOS 已经配置好的IOSettings
		 * \param String const& fbxFileName 输入fbx文件名(不包含路径)
		 * \param String* pName 导入器名
		*/
		UadImporter* createImporter(UadIOSettings* pIOS,String const& fbxFileName,String* pName = nullptr);

		/*!
		 * \remarks 创建导入器对象
		 * \return 
		 * \param UadIOSettings* pSettings 已经配置好的IOSettings
		 * \param String const& fileName 输出fbx文件名(不包含路径)
		 * \param bool embedMedia
		 * \param String* pName 导出器名
		*/
		UadExporter* createExporter(UadIOSettings* pSettings,String const& fileName,bool embedMedia = false,String* pName = nullptr);

		/*!
		 * \remarks 获取FBX内存管理器
		 * \return 
		*/
		FbxManager* getFbxManager() const;

		/*!
		 * \remarks 获取FBX SDK版本信息
		 * \return 
		*/
		String const& getSDKVersion() const;

		/*!
		 * \remarks 获取IOPluginRegistry对象
		 * \return 
		*/
		UadIOPluginRegistry& getIOPluginRegistry() const;

		/*!
		 * \remarks 获取场景
		 * \return 
		*/
		UadScene* getScene() const;

		/*!
		 * \remarks 获取导入器对象
		 * \return 
		*/
		UadImporter* getImporter() const;

		/*!
		 * \remarks 获取导出器对象
		 * \return 
		*/
		UadExporter* getExporter() const;

	private:
		/*!
		 * \remarks 创建FBX内存管理器
		 * \return 
		*/
		void _createFbxManager();

		/*!
		 * \remarks 销毁FBX内存管理器
		 * \return 
		*/
		void _destroyFbxManager();

		/*!
		 * \remarks 获取FBX SDK版本信息
		 * \return 
		*/
		void _getSDKVersion();

		/*!
		 * \remarks load dll
		 * \return 
		*/
		void _loadDLL();

	private:
		FbxManager* mFbxManager;
		String mSDKVersion;
		UadScene* mScene;
		UadImporter* mImporter;
		UadExporter* mExporter;
	};
}

#endif//_UAD_MASTER_H_