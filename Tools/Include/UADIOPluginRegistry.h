/*!
 * \brief
 * IO插件注册
 * \file UADIOPluginRegistry.h
 *
 * \author Su Yang
 *
 * \date 2017/06/24
 */
#ifndef _UAD_IOPLUGINREGISTRY_H_
#define _UAD_IOPLUGINREGISTRY_H_

#ifndef _UAD_CONFIG_H_
#include "UADConfig.h"
#endif

#ifndef _UBS_SINGLETON_H_
#include "UBSSingleton.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 
	 * \class UadIOPluginRegistry
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/24
	 *
	 * \todo
	 */
	class UadIOPluginRegistry : public Singleton<UadIOPluginRegistry>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		UadIOPluginRegistry();

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~UadIOPluginRegistry();

		/*!
		 * \remarks 获取可以导出的格式的数量
		 * \return 
		*/
		int getWriterFormatCount() const;

		/*!
		 * \remarks 判断索引所指定的writer是否为FBX
		 * \return 
		 * \param int writerIndex
		*/
		bool writerIsFBX(int writerIndex) const;

		/*!
		 * \remarks 获取索引所指定的writer的描述
		 * \return 
		 * \param int writerIndex
		*/
		String getWriterFormatDescription(int writerIndex) const;

	private:
		FbxIOPluginRegistry* mRegistry;
	};
}//namespace ung

#endif//_UAD_IOPLUGINREGISTRY_H_