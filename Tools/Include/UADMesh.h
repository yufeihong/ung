/*!
 * \brief
 * mesh
 * \file UADMesh.h
 *
 * \author Su Yang
 *
 * \date 2017/07/01
 */
#ifndef _UAD_MESH_H_
#define _UAD_MESH_H_

#ifndef _UAD_CONFIG_H_
#include "UADConfig.h"
#endif

namespace ung
{
	class UadSubMesh;

	/*!
	 * \brief
	 * mesh
	 * \class UadMesh
	 *
	 * \author Su Yang
	 *
	 * \date 2017/07/01
	 *
	 * \todo
	 */
	class UadMesh
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		UadMesh();

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~UadMesh();

		/*!
		 * \remarks 创建sub mesh
		 * \return 
		 * \param FbxMesh * fbxMesh
		*/
		UadSubMesh* createSubMesh(FbxMesh* fbxMesh);

	private:
		STL_VECTOR(UadSubMesh*) mSubMeshs;
	};
}//namespace ung

#endif//_UAD_MESH_H_