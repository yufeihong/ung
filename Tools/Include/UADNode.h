/*!
 * \brief
 * 场景节点
 * \file UADNode.h
 *
 * \author Su Yang
 *
 * \date 2017/06/20
 */
#ifndef _UAD_NODE_H_
#define _UAD_NODE_H_

#ifndef _UAD_CONFIG_H_
#include "UADConfig.h"
#endif

namespace ung
{
	class UadNodeAttribute;

	/*!
	 * \brief
	 * 场景节点
	 * \class UadNode
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/20
	 *
	 * \todo
	 */
	class UadNode
	{
	public:
		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param FbxNode* pNode
		*/
		explicit UadNode(FbxNode* pNode);

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~UadNode();

		/*!
		 * \remarks 获取FbxNode
		 * \return 
		*/
		FbxNode* getFbxNode() const;

		/*!
		 * \remarks 获取名字
		 * \return 
		*/
		String const& getName() const;

		/*!
		 * \remarks 是否可见
		 * \return 
		*/
		bool isVisibility() const;

		/*!
		 * \remarks 获取孩子节点的数量
		 * \return 
		 * \param bool isRecursive
		*/
		int getChildNodeCount(bool isRecursive = false) const;

		/*!
		 * \remarks 获取索引所指定的孩子节点
		 * \return 
		 * \param int index
		*/
		FbxNode* getChildByIndex(int index) const;

		/*!
		 * \remarks Get the contents of the node
		 * \return 
		*/
		FbxDouble3 getLocalTranslation() const;

		/*!
		 * \remarks Get the contents of the node
		 * \return 
		*/
		FbxDouble3 getLocalRotation() const;

		/*!
		 * \remarks Get the contents of the node
		 * \return 
		*/
		FbxDouble3 getLocalScaling() const;

		/*!
		 * \remarks Get the number of node attribute(s) connected to this node.
		 * \return 
		*/
		int getAttributeCount() const;

		/*!
		 * \remarks 设置默认的attribute
		 * \return 
		 * \param UadNodeAttribute* ab
		*/
		void setDefaultAttribute(UadNodeAttribute* ab);

		/*!
		 * \remarks 获取默认的attribute或nullptr
		 * \return 
		*/
		UadNodeAttribute* getDefaultAttribute() const;

		/*!
		 * \remarks Get the connected node attribute by specifying its index in the connection list.
		 * \return 
		 * \param int index The connection number of the node.
		*/
		UadNodeAttribute* getAttributeByIndex(int index) const;

		/*!
		 * \remarks 获取应用到节点的材质数量
		 * \return 
		*/
		int getMaterialCount() const;

		/*!
		 * \remarks 获取索引所指定的材质
		 * \return 
		 * \param int index
		*/
		FbxSurfaceMaterial* getMaterialByIndex(int index) const;

		/*!
		 * \remarks 
		 * \return 
		 * \param UadNode * node
		*/
		void addChild(UadNode* node);

	private:
		FbxNode* mNode;
		String mName;
		UadNodeAttribute* mDefaultAttribute;
		STL_VECTOR(UadNodeAttribute*) mAttributes;
	};
}//namespace ung

#endif//_UAD_NODE_H_