/*!
 * \brief
 * 场景
 * \file UADScene.h
 *
 * \author Su Yang
 *
 * \date 2017/06/20
 */
#ifndef _UAD_SCENE_H_
#define _UAD_SCENE_H_

#ifndef _UAD_CONFIG_H_
#include "UADConfig.h"
#endif

#ifndef _UBS_SINGLETON_H_
#include "UBSSingleton.h"
#endif

namespace ung
{
	class UadMesh;

	/*!
	 * \brief
	 * 场景
	 * \class UadScene
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/20
	 *
	 * \todo
	 */
	class UadScene : public Singleton<UadScene>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		UadScene();

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~UadScene();

		/*!
		 * \remarks 设置anim stack的数量
		 * \return 
		 * \param int c
		*/
		void setAnimStackCount(int c);

		/*!
		 * \remarks 获取anim stack的数量
		 * \return 
		*/
		int getAnimStackCount() const;

		/*!
		 * \remarks 设置active anim stack的名字
		 * \return 
		 * \param FbxString const& name
		*/
		void setActiveAnimStackName(FbxString const& name);

		/*!
		 * \remarks 获取active anim stack的名字
		 * \return 
		*/
		FbxString const& getActiveAnimStackName() const;

		/*!
		 * \remarks 增加take info
		 * \return 
		 * \param FbxTakeInfo* takeInfo
		*/
		void addTakeInfo(FbxTakeInfo* takeInfo);

		/*!
		 * \remarks 创建mesh
		 * \return 
		*/
		UadMesh* createMesh();

		/*!
		 * \remarks 获取mesh
		 * \return 
		*/
		UadMesh* getMesh() const;

	private:
		UadMesh* mMesh;
		int mAnimStackCount;
		FbxString mActiveAnimStackName;
		STL_VECTOR(FbxTakeInfo*) mTakeInfos;
	};
}//namespace ung

#endif//_UAD_SCENE_H_