/*!
 * \brief
 * 配置文件。
 * \file UADConfig.h
 *
 * \author Su Yang
 *
 * \date 2017/06/20
 */
#ifndef _UAD_CONFIG_H_
#define _UAD_CONFIG_H_

#ifndef _INCLUDE_FBX_SDK_H_
#include "fbxsdk.h"
#define _INCLUDE_FBX_SDK_H_
#endif

#ifndef _INCLUDE_STLIB_IOSTREAM_
#include <iostream>
#define _INCLUDE_STLIB_IOSTREAM_
#endif

#ifndef _UBS_STRING_H_
#include "UBSString.h"
#endif

#ifndef _UFC_EXCEPTION_H_
#include "UFCException.h"
#endif

//#ifndef _INCLUDE_TINYXML2_H_
// //该头文件必须位于D3D头文件之后
//#include "tinyxml2.h"
//#define _INCLUDE_TINYXML2_H_
//#endif
//using namespace tinyxml2;

#ifdef NDEBUG
#define UAD_DEBUGMODE 0
#else
#define UAD_DEBUGMODE 1
#endif

#define FBX_PATH "../../../Resource/Media/Models/fbx/"

#endif//_UAD_CONFIG_H_