/*!
 * \brief
 * IOSettings
 * \file UADIOSettings.h
 *
 * \author Su Yang
 *
 * \date 2017/06/23
 */
#ifndef _UAD_IOSETTINGS_H_
#define _UAD_IOSETTINGS_H_

#ifndef _UAD_CONFIG_H_
#include "UADConfig.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * IOSettings
	 * \class UadIOSettings
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/23
	 *
	 * \todo
	 */
	class UadIOSettings
	{
	public:
		/*!
		 * \remarks 构造函数
		 * \return 
		*/
		UadIOSettings();

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~UadIOSettings();

		/*!
		 * \remarks 获取FbxIOSettings
		 * \return 
		*/
		FbxIOSettings* getFbxIOSettings() const;

		/*!
		 * \remarks 设置FbxIOSettings中的开关
		 * \return 
		 * \param String const & name
		 * \param bool on_off
		*/
		void setBoolProp(String const& name, bool on_off);

		/*!
		 * \remarks 获取FbxIOSettings中的开关
		 * \return 
		 * \param String const & name
		*/
		bool getBoolProp(String const& name) const;

	private:
		FbxIOSettings* mSettings;
	};
}//namespace ung

#endif//_UAD_IOSETTINGS_H_