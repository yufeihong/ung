#include "UME.h"
#include "ClassDesc.h"
#include "Impl.h"

#define STRING2(x) #x
#define STRING(x) STRING2(x)
#define TODO(x) __FILE__ "(" STRING(__LINE__) "): TODO: "x

//This returns the class instance of our exporter information
ClassDesc2* GetUMExportDesc()
{ 
	static UMExportClassDesc UMExportDesc;

	return &UMExportDesc; 
}

INT_PTR CALLBACK UMExportOptionsDlgProc(HWND hWnd,UINT message,WPARAM wParam,LPARAM lParam)
{
	static UMExport* imp = NULL;
	
	switch(message)
	{
		case WM_INITDIALOG:
			imp = (UMExport*)lParam;
			CenterWindow(hWnd,GetParent(hWnd));
			imp->mImpl->hWnd = hWnd;

			//设置默认状态
			CheckDlgButton(hWnd, IDC_MESH, imp->mImpl->mExportMesh);
			CheckDlgButton(hWnd, IDC_MATERIAL, imp->mImpl->mExportMaterial);
			CheckDlgButton(hWnd, IDC_LIGHT, imp->mImpl->mExportLight);
			CheckDlgButton(hWnd, IDC_CAMERA, imp->mImpl->mExportCamera);

			CheckDlgButton(hWnd, IDC_VERTEX, imp->mImpl->mExportMesh && imp->mImpl->mExportMeshPosition);
			CheckDlgButton(hWnd, IDC_TANGENT, imp->mImpl->mExportMesh && imp->mImpl->mExportMeshTangent);
			CheckDlgButton(hWnd, IDC_BITANGENT, imp->mImpl->mExportMesh && imp->mImpl->mExportMeshBitangent);
			CheckDlgButton(hWnd, IDC_NORMAL, imp->mImpl->mExportMesh && imp->mImpl->mExportMeshNormal);
			CheckDlgButton(hWnd, IDC_INDEX, imp->mImpl->mExportMesh && imp->mImpl->mExportMeshIndex);
			CheckDlgButton(hWnd, IDC_UV, imp->mImpl->mExportMesh && imp->mImpl->mExportMeshUV);

			EnableWindow(GetDlgItem(hWnd, IDC_VERTEX), imp->mImpl->mExportMesh);
			EnableWindow(GetDlgItem(hWnd, IDC_TANGENT), imp->mImpl->mExportMesh);
			EnableWindow(GetDlgItem(hWnd, IDC_BITANGENT), imp->mImpl->mExportMesh);
			EnableWindow(GetDlgItem(hWnd, IDC_NORMAL), imp->mImpl->mExportMesh);
			EnableWindow(GetDlgItem(hWnd, IDC_INDEX), imp->mImpl->mExportMesh);
			EnableWindow(GetDlgItem(hWnd, IDC_UV), imp->mImpl->mExportMesh);
			
			CheckDlgButton(hWnd,IDC_OPENGL,1);
			CheckDlgButton(hWnd, IDC_BINARY,1);
			CheckDlgButton(hWnd,IDC_STRIP,1);

			return TRUE;

		case WM_COMMAND:
			switch (LOWORD(wParam))
			{
			case IDC_MESH:
				//Enable/disable mesh options
				EnableWindow(GetDlgItem(hWnd, IDC_VERTEX), IsDlgButtonChecked(hWnd,IDC_MESH));
				EnableWindow(GetDlgItem(hWnd, IDC_TANGENT), IsDlgButtonChecked(hWnd,IDC_MESH));
				EnableWindow(GetDlgItem(hWnd, IDC_BITANGENT), IsDlgButtonChecked(hWnd,IDC_MESH));
				EnableWindow(GetDlgItem(hWnd, IDC_NORMAL), IsDlgButtonChecked(hWnd,IDC_MESH));
				EnableWindow(GetDlgItem(hWnd, IDC_INDEX), IsDlgButtonChecked(hWnd,IDC_MESH));
				EnableWindow(GetDlgItem(hWnd, IDC_UV), IsDlgButtonChecked(hWnd,IDC_MESH));
				break;

			case IDC_EXPORT:
				imp->mImpl->mExportMesh = IsDlgButtonChecked(hWnd, IDC_MESH);
				imp->mImpl->mExportMaterial = IsDlgButtonChecked(hWnd, IDC_MATERIAL);
				imp->mImpl->mExportLight = IsDlgButtonChecked(hWnd, IDC_LIGHT);
				imp->mImpl->mExportCamera = IsDlgButtonChecked(hWnd, IDC_CAMERA);

				imp->mImpl->mExportMeshPosition = IsDlgButtonChecked(hWnd, IDC_VERTEX);
				imp->mImpl->mExportMeshTangent = IsDlgButtonChecked(hWnd, IDC_TANGENT);
				imp->mImpl->mExportMeshBitangent = IsDlgButtonChecked(hWnd, IDC_BITANGENT);
				imp->mImpl->mExportMeshNormal = IsDlgButtonChecked(hWnd, IDC_NORMAL); 
				imp->mImpl->mExportMeshIndex = IsDlgButtonChecked(hWnd, IDC_INDEX);
				imp->mImpl->mExportMeshUV = IsDlgButtonChecked(hWnd, IDC_UV);
				
				if (IsDlgButtonChecked(hWnd,IDC_OPENGL))
				{
					imp->mImpl->mCoordSystem = 0;
				}
				else if (IsDlgButtonChecked(hWnd,IDC_D3D))
				{
					imp->mImpl->mCoordSystem = 1;
				}

				if(IsDlgButtonChecked(hWnd, IDC_BINARY))
				{
					imp->mImpl->mFormat = 0;
				}
				else if(IsDlgButtonChecked(hWnd, IDC_XML))
				{
					imp->mImpl->mFormat = 1;
				}
				else if(IsDlgButtonChecked(hWnd, IDC_JSON))
				{
					imp->mImpl->mFormat = 2;
				}
				else if(IsDlgButtonChecked(hWnd, IDC_INFO))
				{
					imp->mImpl->mFormat = 3;
				}

				if(IsDlgButtonChecked(hWnd, IDC_STRIP))
				{
					imp->mImpl->mStripList = 0;
				}
				else if(IsDlgButtonChecked(hWnd, IDC_LIST))
				{
					imp->mImpl->mStripList = 1;
				}
				
				EndDialog(hWnd, 1);
				break;

		case IDCANCEL:
				EndDialog(hWnd, 0);
				break;
			}
			break;

		default:
			return FALSE;
	}//switch(message)

	return 0;
}

UMExport::UMExport() :
	mImpl(new Impl)
{
}

UMExport::~UMExport() 
{
	if(mImpl)
	{
		delete mImpl;
		mImpl = NULL;
	}
}

int UMExport::ExtCount()
{
	return 1;
}

const TCHAR* UMExport::Ext(int /*n*/)
{		
	return _T("UMD");
}

const TCHAR* UMExport::LongDesc()
{
	return _T("UME Exporter");
}
	
const TCHAR* UMExport::ShortDesc() 
{
	return _T("UNG");
}

const TCHAR* UMExport::AuthorName()
{
	return _T("Su Yang");
}

const TCHAR* UMExport::CopyrightMessage() 
{
	return _T("Copyright");
}

const TCHAR* UMExport::OtherMessage1() 
{
	return _T("");
}

const TCHAR* UMExport::OtherMessage2() 
{
	return _T("");
}

unsigned int UMExport::Version()
{
	return 100;
}

void UMExport::ShowAbout(HWND /*hWnd*/)
{			
}

BOOL UMExport::SupportsOptions(int ext, DWORD options)
{
	//只支持一个扩展名
	assert(ext == 0);

	return(options == SCENE_EXPORT_SELECTED) ? TRUE : FALSE;
}