#ifndef _UPS_H_
#define _UPS_H_

#ifndef NULL
#define NULL 0
#endif

#ifdef _DEBUG
#pragma comment(lib, "UPS_d")
#else
#pragma comment(lib, "UPS")
#endif

#define CACHE_SIZE 128

enum PrimType
{
	PT_LIST,
	PT_STRIP,
	PT_FAN
};

class PrimitiveGroup
{
public:
	PrimitiveGroup() :
		type(PT_STRIP),
		numIndices(0),
		indices(NULL)
	{
	}

	~PrimitiveGroup()
	{
		if (indices)
		{
			delete[] indices;
			indices = NULL;
		}
	}

	PrimType type;
	unsigned int numIndices;
	unsigned short* indices;
};

/*
For GPUs that support primitive restart, this sets a value as the restart index
Restart is meaningless if strips are not being stitched together, so enabling restart makes PSTriStrip forcing stitching.  So, you'll get back one strip.

Default value: disabled
*/
void enableRestart(const unsigned int restartVal);

/*
For GPUs that support primitive restart, this disables using primitive restart
*/
void disableRestart();

/*
Sets the cache size which the stripfier uses to optimize the data.
Controls the length of the generated individual strips.
This is the "actual" cache size, so 24 for GeForce3 and 16 for GeForce1/2
You may want to play around with this number to tweak performance.

Default value:128
*/
void setCacheSize(const unsigned int cacheSize);

/*
bool to indicate whether to stitch together strips into one huge strip or not.
If set to true, you'll get back one huge strip stitched together using degenerate triangles.
If set to false, you'll get back a large number of separate strips.

Default value:true
*/
void setStitchStrips(const bool bStitchStrips);

/*
Sets the minimum acceptable size for a strip, in triangles.
All strips generated which are shorter than this will be thrown into one big, separate list.

Default value:0
*/
void setMinStripSize(const unsigned int minSize);

/*
If set to true, will return an optimized list, with no strips at all.

Default value:false
*/
void setListsOnly(const bool bListsOnly);

/*
in_indices: input index list, the indices you would use to render
in_numIndices: number of entries in in_indices
primGroups: array of optimized/stripified PrimitiveGroups
numGroups: number of groups returned

Be sure to call delete[] on the returned primGroups to avoid leaking mem
*/
bool generateStrips(const unsigned short* in_indices, const unsigned int in_numIndices,
					PrimitiveGroup** primGroups, unsigned short* numGroups, bool validateEnabled = false);

/*
Function to remap your indices to improve spatial locality in your vertex buffer.

in_primGroups: array of PrimitiveGroups you want remapped
numGroups: number of entries in in_primGroups
numVerts: number of vertices in your vertex buffer, also can be thought of as the range of acceptable values for indices in your primitive groups.
remappedGroups: array of remapped PrimitiveGroups

Note that, according to the remapping handed back to you, you must reorder your vertex buffer.

Credit goes to the MS Xbox crew for the idea for this interface.
*/
void remapIndices(const PrimitiveGroup* in_primGroups, const unsigned short numGroups, 
				  const unsigned short numVerts, PrimitiveGroup** remappedGroups);

#endif//_UPS_H_