#include "Impl.h"
#include <triobj.h>

BOOL Impl::isMiscGeometry(INode* node, ObjectState* objectState)
{
	//Skip this object if it's just a target
	if (!objectState->obj || (objectState->obj->ClassID() == Class_ID(TARGET_CLASS_ID, 0)))
	{
		return true;
	}

	String nodeName = node->GetName();

	//Skip the bone information about the biped
	if(nodeName.compare(0,3,L"Bip") == 0 || nodeName.compare(0,3,L"bip") == 0)
	{
		return true;
	}

	//Skip unneeded tag information about our model
	if (nodeName.compare(0,4,L"tag_") == 0)
	{
		return true;
	}

	return false;
}

void Impl::exportMesh(INode* node)
{
	/*
	With the current child node that was passed in, we can access the state of the current object/mesh.  We pass in 0 because we want the current state of the 
	mesh at frame 0 of animation.
	We then receive an instance of the ObjectState class, which stores a pointer to the current object in the node.
	*/
	ObjectState objectState = node->EvalWorldState(0);

	if(isMiscGeometry(node, &objectState))
	{
		return;
	}

	//===================================================================

	/*
	There are many different way to create 3D models in 3DS Max, like NURBS and such that aren't stored in 
	triangle formats.We want our data stored in triangles so we need to convert the data to a "Tri Object",
	which is an object that is comprised of triangles.  We then have to remember to free it.
	*/
	//This boolean tells us if we converted a tri object that needs to be freed
	BOOL needDelete;
	//This stores the pointer to our converted tri object
	TriObject* triObject = NULL;
	triObject = getTriObjectFromNode(node,&objectState,needDelete);
	if (!triObject)
	{
		return;
	}

	//===================================================================

	/*
	From our tri object we can access a pointer to a "Mesh" class that holds the face information we will be exporting.

	Mesh& GetMesh()
	Remarks:
	This method is available in release 3.0 and later only.
	Returns:
	a reference to the mesh data member of this TriObject.
	*/
	Mesh* mesh = &triObject->GetMesh();

	//===================================================================

	//定义一个我们的mesh对象
	UMEMesh umeMesh;
	umeMesh.build(this,node,mesh,mCoordSystem);
	mMeshs.push_back(umeMesh);

	//认为一次只能导出一个mesh
	assert(mMeshs.size() == 1);

	//===================================================================

	/*
	virtual Mtl* GetMtl()
	Returns:
	a pointer to the renderer material for the node. If the value returned is NULL the user has not assigned a 
	material yet. See Class Mtl, Materials, Textures and Maps.
	*/
	//Mtl* nodeMaterial = node->GetMtl();
	//String materialName = getMaterialName(nodeMaterial);

	////顶点颜色
	//int vertextColorCount = mesh->numCVerts;
	//if (vertextColorCount)
	//{
	//	for (int i=0; i<vertextColorCount; i++)
	//	{
	//		/*
	//		VertColor* vertCol
	//		Array of color vertices.
	//		*/
	//		Point3 vertexColor = mesh->vertCol[i];
	//	}
	//}

	//纹理坐标

	//===================================================================

	if (needDelete)
	{
		delete triObject;
		triObject = NULL;
	}
}

////如果当前传入的顶点与容器中的某个顶点重复，那么pos取得容器中相同的顶点的位置
//BOOL Impl::isVertexUnique(UMEVector3 const& vertex,int& pos)
//{
//	size_t currentVertexCount = mUniqueVertices.size();
//
//	for (int i = 0; i < currentVertexCount; i++)
//	{
//		if (mUniqueVertices[i] == vertex)
//		{
//			pos = i;
//			return FALSE;
//		}
//	}
//
//	return TRUE;
//}