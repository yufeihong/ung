//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 UME.rc 使用
//
#define IDS_LIBDESCRIPTION              1
#define IDS_CATEGORY                    2
#define IDS_CLASS_NAME                  3
#define IDS_PARAMS                      4
#define IDS_SPIN                        5
#define IDD_PANEL                       101
#define IDC_CLOSEBUTTON                 1000
#define IDC_OPENGL                      1013
#define IDC_D3D                         1014
#define IDC_BINARY                      1015
#define IDC_XML                         1016
#define IDC_JSON                        1017
#define IDC_INFO                        1018
#define IDC_LIGHT                       1019
#define IDC_CAMERA                      1020
#define IDC_UV                          1021
#define IDC_NORMAL                      1022
#define IDC_TANGENT                     1023
#define IDC_BITANGENT                   1024
#define IDC_STRIP                       1025
#define IDC_RADIO2                      1026
#define IDC_LIST                        1026
#define IDC_MESH                        1101
#define IDC_MATERIAL                    1102
#define IDC_VERTEX                      1201
#define IDC_INDEX                       1202
#define IDC_EXPORT                      1501
#define IDC_CANCEL                      1502

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1027
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
