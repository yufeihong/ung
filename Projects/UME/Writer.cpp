#include "Writer.h"
#include "Impl.h"
#include "boost/algorithm/string.hpp"
#include "boost/lexical_cast.hpp"
using namespace boost;
using namespace boost::algorithm::detail;

Writer::Writer()
{
}

Writer::~Writer()
{
}

void Writer::setImpl(Impl* impl)
{
	mImpl = impl;
}

void Writer::writeSceneHeader()
{
	addKeyValue(mSceneNode,"Scene.MaxVersion","2016-64");

	addKeyValue(mSceneNode,"Scene.Author","UNG");

	if (mImpl->mCoordSystem == 0)
	{
		addKeyValue(mSceneNode,"Scene.CoordSystem","OpenGL");
	}
	else if (mImpl->mCoordSystem == 1)
	{
		addKeyValue(mSceneNode,"Scene.CoordSystem","Direct3D");
	}

	if (mImpl->mFormat == 0)
	{
		addKeyValue(mSceneNode,"Scene.Format","BINARY");
	}
	else if (mImpl->mFormat == 1)
	{
		addKeyValue(mSceneNode,"Scene.Format","XML");
	}
	else if (mImpl->mFormat == 2)
	{
		addKeyValue(mSceneNode,"Scene.Format","JSON");
	}
	else if (mImpl->mFormat == 3)
	{
		addKeyValue(mSceneNode,"Scene.Format","INFO");
	}

	addKeyValue(mSceneNode,"Scene.TicksPerFrame",GetTicksPerFrame());

	addKeyValue(mSceneNode,"Scene.FrameRate",GetFrameRate());

	//addKeyValue(mSceneNode,"Scene.MeshCount",mImpl->mMeshs.size());

	mSceneNode.add("Scene.MaterialCount",mImpl->mMaterialsCount);
}

void Writer::writeMaterials()
{
	if (!mImpl->mExportMaterial)
	{
		return;
	}

	////当没有材质的时候，为了XML的标签
	//if (mImpl->mMaterialsCount == 0)
	//{
	//	ptree matContent;

	//	addKeyValue(matContent,"Name","nil");										//借用下Lua中的nil，应用程序需要就此作出判断

	//	addNode(mMaterialNode, "Material", matContent);
	//}

	for (uint16 i = 0; i < mImpl->mMaterialsCount; ++i)
	{
		mMaterialNodes.push_back(ptree());
	}

	for (uint16 i = 0; i < mImpl->mMaterialsCount; ++i)
	{
		ptree matContent;

		UMEMaterial const& currentMaterial = mImpl->mUMEMaterials[i];

		mMaterialNames.push_back(currentMaterial.mName);

		addKeyValue(matContent,"Name",currentMaterial.mName);
		addKeyValue(matContent, "Ambient", currentMaterial.mAmbient);
		addKeyValue(matContent, "Diffuse", currentMaterial.mDiffuse);
		addKeyValue(matContent, "Specular", currentMaterial.mSpecular);
		addKeyValue(matContent, "Shininess", currentMaterial.mShininess);
		addKeyValue(matContent, "ShininessStrength", currentMaterial.mShininessStrength);
		addKeyValue(matContent, "Transparency", currentMaterial.mTransparency);
		addKeyValue(matContent, "BlendMode", currentMaterial.mBlendMode);
		addKeyValue(matContent, "TwoSide", currentMaterial.mTwoSide);
		uint16 textureCount = currentMaterial.mTextures.size();
		addKeyValue(matContent, "TextureCount", textureCount);

		ptree textureNode;
		for (int j = 0; j < textureCount; j++)
		{
			addKeyValue(textureNode,"Texture",currentMaterial.mTextures[j]);
		}
		addNode(matContent, "Textures", textureNode);

		addNode(mMaterialNodes[i], "Material", matContent);
	}

	//addNode(mSceneNode, "Scene.Materials", mMaterialNode);
}

void Writer::writeMeshs()
{
	if (!mImpl->mExportMesh)
	{
		return;
	}

	ptree meshNode;
	//遍历所有的mesh
	for (int i = 0; i < mImpl->mMeshs.size(); i++)
	{
		ptree meshContent;
		
		//当前mesh
		UMEMesh const& mesh = mImpl->mMeshs[i];

		//当前mesh的总face数量
		addKeyValue(meshContent,"FaceCount",mesh.getFaceCount());

		//当前mesh的总顶点数量
		usize vertexCount = mesh.getOriginalVertexCount();
		addKeyValue(meshContent,"VertexCount",vertexCount);

		//当前mesh的Position
		if (mImpl->mExportMeshPosition)
		{
			ptree positionContent;
			for (int j = 0; j < vertexCount; j++)
			{
				addKeyValue(positionContent,"Position",mesh.getOriginalVertexAtIndex(j));
			}

			addNode(meshContent,"Positions",positionContent);
		}

		//当前mesh的Tangent
		if (mImpl->mExportMeshTangent)
		{
			ptree tangentContent;
			for (int j = 0; j < vertexCount; j++)
			{
				addKeyValue(tangentContent,"Tangent",mesh.getOriginalTangentAtIndex(j));
			}

			addNode(meshContent,"Tangents",tangentContent);
		}

		//当前mesh的Bitangent
		if (mImpl->mExportMeshBitangent)
		{
			ptree bitangentContent;
			for (int j = 0; j < vertexCount; j++)
			{
				addKeyValue(bitangentContent,"Bitangent",mesh.getOriginalBitangentAtIndex(j));
			}

			addNode(meshContent,"Bitangents",bitangentContent);
		}

		//当前mesh的Normal
		if (mImpl->mExportMeshNormal)
		{
			ptree normalContent;
			for (int j = 0; j < vertexCount; j++)
			{
				addKeyValue(normalContent,"Normal",mesh.getOriginalNormalAtIndex(j));
			}

			addNode(meshContent,"Normals",normalContent);
		}

		//当前mesh的UV
		if (mImpl->mExportMeshUV)
		{
			ptree uvContent;
			for (int j = 0; j < vertexCount; j++)
			{
				addKeyValue(uvContent,"UV",mesh.getOriginalUVAtIndex(j));
			}

			addNode(meshContent,"UVs",uvContent);
		}

		//遍历当前mesh所有的subMesh
		ptree subMeshsNode;
		ptree subMeshNode;
		int subMeshCount = mesh.getSubMeshs().size();
		for (int j = 0; j < subMeshCount; j++)
		{
			ptree subMeshContent;

			//当前subMesh
			UMESubMesh const& subMesh = (mesh.getSubMeshs())[j];

			if (subMesh.mMaterialName.empty())
			{
				addKeyValue(subMeshContent,"MaterialName","nil");						//应用程序需要就此作出判断
			}
			else
			{
				std::string materialName = subMesh.mMaterialName;
				if (mImpl->mFormat == 0)
				{
				}
				else if (mImpl->mFormat == 1)
				{
					materialName += std::string(".xml");
				}

				to_lower(materialName);

				addKeyValue(subMeshContent,"MaterialName",materialName);
			}
			addKeyValue(subMeshContent,"TrianglesMode",subMesh.mTrianglesMode);
			addKeyValue(subMeshContent,"FaceCount",subMesh.mFaceCount);
			addKeyValue(subMeshContent,"VertexCount",subMesh.mVertexCount);
			usize indexCount = subMesh.mListIndices.size();
			if (subMesh.mTrianglesMode == "Strip")
			{
				indexCount = subMesh.mStripIndices.size();
			}
			addKeyValue(subMeshContent,"IndexCount",indexCount);

			//索引
			if (mImpl->mExportMeshIndex)
			{
				ptree indexContent;
				for(int k = 0; k < indexCount; ++k)
				{
					uint16 indexValue = 0;
					if (subMesh.mTrianglesMode == "Strip")
					{
						indexValue = subMesh.mStripIndices[k];
					}
					else
					{
						indexValue = subMesh.mListIndices[k];
					}

					addKeyValue(indexContent,"Index",indexValue);
				}

				addNode(subMeshContent,"Indices",indexContent);
			}

			addNode(subMeshNode, "SubMesh", subMeshContent);
		}//subMesh

		addNode(meshContent,"SubMeshs",subMeshNode);

		//addNode(meshNode,"Mesh",meshContent);
		addNode(mSceneNode,"Scene.Mesh",meshContent);
	}//遍历所有的mesh

	//addNode(mSceneNode,"Scene.Meshs",meshNode);
}

void Writer::doWriteToFile(ptree& tree,std::string& fileName)
{
	to_lower(fileName);

	if (mImpl->mFormat == 0)
	{

	}
	else if (mImpl->mFormat == 1)
	{
		ireplace_last(fileName,".umd",".xml");
		std::ofstream ofs(fileName);
		xml_writer_settings<std::string> settings('\t', 1);
		write_xml(ofs, tree,settings);
	}
}

void Writer::doWriteMesh()
{
	writeSceneHeader();

	writeMeshs();

	doWriteToFile(mSceneNode,mImpl->mFileName);
}

void Writer::doWriteMaterial()
{
	writeMaterials();

	for (int i = 0; i < mMaterialNames.size(); i++)
	{
		std::string materialFileName = mMaterialNames[i];
		materialFileName += std::string(".UMD");
		doWriteToFile(mMaterialNodes[i],materialFileName);
	}
}

//------------------------------------------------------------------------------------------------------------------------------------------

void Writer::addKeyValue(ptree& parent, std::string const& key, std::string const& value)
{
	parent.add(key,value);
}

void Writer::addKeyValue(ptree& parent, std::string const& key, float const& value)
{
	parent.add(key,value);
}

void Writer::addKeyValue(ptree& parent, std::string const& key, uint16 const& value)
{
	parent.add(key,value);
}

void Writer::addKeyValue(ptree& parent, std::string const& key, int const& value)
{
	parent.add(key,value);
}

void Writer::addKeyValue(ptree& parent, std::string const& key, usize const& value)
{
	parent.add(key,value);
}

void Writer::addKeyValue(ptree& parent, std::string const& key, size_t const& value)
{
	parent.add(key,value);
}

void Writer::addKeyValue(ptree& parent, std::string const& key, UMEVector2 const& value)
{
	char buffer[128];
	sprintf(buffer, "%f,%f", value.x,value.y);

	parent.add(key,buffer);
}

void Writer::addKeyValue(ptree& parent, std::string const& key, UMEVector3 const& value)
{
	char buffer[128];
	sprintf(buffer, "%f,%f,%f", value.x,value.y,value.z);

	parent.add(key,buffer);
}

void Writer::addKeyValue(ptree& parent, std::string const& key, UMEVector4 const& value)
{
	char buffer[128];
	sprintf(buffer, "%f,%f,%f,%f", value.x,value.y,value.z,value.w);

	parent.add(key,buffer);
}

void Writer::addNode(ptree& parent,std::string const& parentName,ptree const& childNode)
{
	parent.add_child(parentName, childNode);
}