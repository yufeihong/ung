#pragma once

#include <string>

class UMEVector2;
class UMEVector3;

extern std::string wstring2string(std::wstring sToMatch);

//从一个完整路径中得到文件名
extern std::string getFileName(std::string fileFullName);

//v2是单位向量标杆
extern void correctPerpendicular(UMEVector3& v1,UMEVector3 const& v2);

extern bool isPerpendicular(UMEVector3 const& v1,UMEVector3 const& v2);

extern bool isEqual(float value1,float value2);

extern bool isEqual(UMEVector2 const& value1,UMEVector2 const& value2);