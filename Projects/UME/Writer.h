#pragma once

#include "UMEVector.h"
#include "boost/property_tree/ptree.hpp"
#include "boost/property_tree/xml_parser.hpp"
#include "boost/property_tree/json_parser.hpp"
#include "boost/property_tree/info_parser.hpp"
#include "boost/property_tree/ini_parser.hpp"
using namespace boost::property_tree;

#define SEPARATE_MATERIAL_FILE

class Impl;

class Writer
{
public:
	Writer();

	~Writer();

	void setImpl(Impl* impl);

	void doWriteMesh();

	void doWriteMaterial();

private:
	void writeSceneHeader();
	void writeMaterials();
	void writeMeshs();
	void doWriteToFile(ptree& tree,std::string& fileName);

	void addKeyValue(ptree& parent, std::string const& key, std::string const& value);
	void addKeyValue(ptree& parent, std::string const& key, float const& value);
	void addKeyValue(ptree& parent, std::string const& key, uint16 const& value);
	void addKeyValue(ptree& parent, std::string const& key, int const& value);
	void addKeyValue(ptree& parent, std::string const& key, usize const& value);
	void addKeyValue(ptree& parent, std::string const& key, size_t const& value);
	void addKeyValue(ptree& parent, std::string const& key, UMEVector2 const& value);
	void addKeyValue(ptree& parent, std::string const& key, UMEVector3 const& value);
	void addKeyValue(ptree& parent, std::string const& key, UMEVector4 const& value);
	void addNode(ptree& parent,std::string const& parentName,ptree const& childNode);

private:
	Impl* mImpl;

	ptree mSceneNode;
	std::vector<ptree> mMaterialNodes;

	//当把材质和其它数据分开，单独写入一个或几个文件中时，需要记录材质的名字来作为材质文件名
	std::vector<std::string> mMaterialNames;
};