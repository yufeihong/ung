#pragma once

#include <string>
#include <unordered_map>

typedef std::wstring String;

//未引用的形参
#pragma warning(disable : 4100)

//局部变量已初始化但不引用
#pragma warning(disable : 4189)

//从“size_t”转换到“int”
#pragma warning(disable : 4267)