#pragma once

#include "Include.h"
#include "resource.h"
#include <istdplug.h>
#include <iparamb2.h>
#include <iparamm2.h>
#include <maxtypes.h>
#include <impexp.h>

#define UMExport_CLASS_ID Class_ID(0x4ee98252, 0x8c7dd6e2)

extern TCHAR *GetString(int id);
//The global hInstance for our DLL
extern HINSTANCE hInstance;

class Impl;

/*
SceneExport:
#include <impexp.h>
Description:
This is a base class for creating file export plug-ins. The plug-in implements methods of this class to describe the properties of the export plug-in and 
a method that handles the actual export process.
*/
class UMExport : public SceneExport 
{
	friend INT_PTR CALLBACK UMExportOptionsDlgProc(HWND hWnd,UINT message,WPARAM wParam,LPARAM lParam);

public:
	UMExport();

	virtual ~UMExport();

	//Returns the number of file name extensions supported by the plug-in.
	virtual int ExtCount();

	//Returns the 'i-th' file name extension.
	virtual const TCHAR* Ext(int n);

	//Returns a long ASCII description of the file type being exported.
	virtual const TCHAR* LongDesc();

	//Returns a short ASCII description of the file type being exported.
	virtual const TCHAR* ShortDesc();

	//Returns the ASCII Author name.
	virtual const TCHAR* AuthorName();

	//Returns the ASCII Copyright message for the plug-in.
	virtual const TCHAR* CopyrightMessage();

	//Returns the first message string that is displayed.
	virtual const TCHAR* OtherMessage1();

	//Returns the second message string that is displayed.
	virtual const TCHAR* OtherMessage2();

	//Returns the version number of the export plug-in. The format is the version number * 100.
	virtual unsigned int Version();

	/*
	virtual void ShowAbout(HWND hWnd)
	Remarks:
	This method is called to have the plug-in display its "About..." box.
	Parameters:
	HWND hWnd,The parent window handle for the dialog.
	*/
	virtual void ShowAbout(HWND hWnd);

	/*
	virtual BOOL SupportsOptions(int ext,DWORD options)
	Remarks:
	This method is called by 3ds Max to determine if one or more export options are supported by a plug-in for a given extension.It should return TRUE 
	if all option bits set are supported for this extension;otherwise FALSE.Note that the method has a default implementation defined in order to provide 
	easy backward compatibility.It returns FALSE,indicating that no options are supported.
	Parameters:
	int ext,This parameter indicates which extension the options are being queried for,based on the number of extensions returned by the 
	SceneExport::ExtCount() method.This index is zero based.
	DWORD options,This parameter specifies which options are being queried,and may have more than one option specified.At present,the only export 
	option is SCENE_EXPORT_SELECTED, but this may change in the future.If more than one option is specified in this field, the plugin should only 
	return TRUE if all of the options are supported. If one or more of the options are not supported, the plugin should return FALSE. Default 
	Implementation:{return FALSE;}
	*/
	virtual BOOL SupportsOptions(int ext, DWORD options);

	/*
	virtual int DoExport(const MCHAR* name,ExpInterface* ei,Interface* i,BOOL suppressPrompts = FALSE,DWORD options = 0)
	Remarks:
	This method is called for the plug-in to perform its file export.
	Parameters:
	const MCHAR* name,The export file name.
	ExpInterface* ei,A pointer the plug-in may use to call methods to enumerate the scene.
	Interface* i,An interface pointer the plug-in may use to call methods of 3ds Max.
	BOOL suppressPrompts=FALSE,This parameter is available in release 2.0 and later only.When TRUE, the plug-in must not display any dialogs 
	requiring user input. It is up to the plug-in as to how to handle error conditions or situations requiring user input. This is an option set up for the 
	3ds Max API in order for plug-in developers to create batch export plugins which operate unattended. See Interface::ExportToFile().
	DWORD options=0,This parameter is available in release 3.0 and later only.In order to support export of selected objects (as well as future 
	enhancements), this method now has this additional parameter. The only currently defined option is:SCENE_EXPORT_SELECTED,When this bit is set 
	the export module should only export the selected nodes.
	Returns:
	One of the following three values should be returned:
	#define IMPEXP_FAIL 0
	#define IMPEXP_SUCCESS 1
	#define IMPEXP_CANCEL 2
	*/
	virtual int DoExport(const TCHAR* fileName,ExpInterface* ei,Interface* i, BOOL suppressPrompts = FALSE, DWORD options=0);

//protected:
//	int GetSceneNodes(INodeTab& i_nodeTab, INode* i_currentNode = NULL);

private:
	Impl* mImpl;
};