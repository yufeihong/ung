#include "UME.h"
#include "Impl.h"
#include "UMETools.h"

extern INT_PTR CALLBACK UMExportOptionsDlgProc(HWND hWnd,UINT message,WPARAM wParam,LPARAM lParam);

int UMExport::DoExport(const TCHAR* fileName, ExpInterface* /*ei*/, Interface* ip, BOOL suppressPrompts, DWORD options)
{
	mImpl->mFileName = wstring2string(fileName);

	mImpl->mInterface = ip;
	mImpl->mShowPrompts = suppressPrompts ? FALSE : TRUE;
	mImpl->mExportSelected = (options & SCENE_EXPORT_SELECTED) ? TRUE : FALSE;

	//读取配置文件

	if(mImpl->mShowPrompts)
	{
		/*
		Creates a modal dialog box from a dialog box template resource.
		Before displaying the dialog box, the function passes an application-defined value to the dialog box procedure as the 
		lParam parameter of the WM_INITDIALOG message. An application can use this value to initialize dialog box controls.
		*/
		long long dlgRet = DialogBoxParam(hInstance, MAKEINTRESOURCE(IDD_PANEL), GetActiveWindow(), UMExportOptionsDlgProc, (LPARAM)this);
		assert(dlgRet != -1);
	}

	//获取根节点
	mImpl->mRootNode = mImpl->mInterface->GetRootNode();
	/*
	virtual int IsRootNode()
	Determines if this node is the root node (does not have a parent node).
	Returns:
	Non-zero if the node is the root node; otherwise 0.
	*/
	int isRootRet = mImpl->mRootNode->IsRootNode();
	assert(isRootRet);

	size_t rootChildCount = mImpl->mRootNode->NumberOfChildren();

	//遍历节点
	for (int i=0; i < rootChildCount; ++i)
	{
		if (mImpl->mInterface->GetCancel())
		{
			break;
		}

		mImpl->enumNode(mImpl->mRootNode->GetChildNode(i));
	}

	//写
	mImpl->write();

	//写入配置文件

	//end
	mImpl->end();

	return TRUE;
}