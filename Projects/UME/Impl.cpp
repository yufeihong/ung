#include "Impl.h"
#include "Writer.h"
#include "UMETools.h"
#include "stdmat.h"

Impl::Impl() :
	mInterface(NULL),
	mShowPrompts(FALSE),
	mExportSelected(FALSE),

	mExportMesh(TRUE),
	mExportMaterial(TRUE),
	mExportLight(FALSE),
	mExportCamera(FALSE),

	mExportMeshPosition(TRUE),
	mExportMeshIndex(TRUE),
	mExportMeshColor(FALSE),
	mExportMeshUV(TRUE),
	mExportMeshNormal(TRUE),
	mExportMeshTangent(TRUE),
	mExportMeshBitangent(FALSE),
	mFormat(0),
	mCoordSystem(0),
	mStripList(0),

	mTotalNodeCount(0),
	mTotalMaterialCount(0),
	mTicksPerFrame(0),
	mFrameRate(0),
	mFirstFrame(0),
	mLastFrame(0),
	mStaticFrame(0),

	mRootNode(NULL),

	mMaterialsCount(0),

	mWriter(new Writer)
{
}

Impl::~Impl()
{
	if (mWriter)
	{
		delete mWriter;
		mWriter = NULL;
	}
}

void Impl::enumNode(INode* node)
{
	String nodeName = node->GetName();

	if(mExportSelected && node->Selected() == FALSE)
	{
		return;
	}

	/*
	If this node is a group head, all children are members of this group. The node will be a dummy node and the node name 
	is the actualy group name.

	virtual BOOL IsGroupHead()
	Returns TRUE if this node is the head of a group; otherwise FALSE.
	*/
	BOOL isGroupHead = node->IsGroupHead();

	if (!isGroupHead)
	{
		collectMaterials(node);
		buildMaterial();

		/*
		The ObjectState is a 'thing' that flows down the pipeline containing all information about the object.
		By calling EvalWorldState() we tell max to eveluate the object at end of the pipeline.

		virtual const ObjectState& EvalWorldState(TimeValue time,BOOL evalHidden = TRUE)
		Remarks:
		This method should be called when a developer needs to work with an object that is the result of the node's pipeline.
		This is the object that the appears in the scene.
		This may not be an object that anyone has a reference to - it may just be an object that has flowed down the pipeline.
		For example, if there is a Sphere in the scene that has a Bend and Taper applied, EvalWorldState() would return an 
		ObjectState containing a TriObject. This is the result of the sphere turning into a TriObject and being bent and tapered 
		(just as it appeared in the scene).
		If a developer needs to access the object that the node in the scene references, then the method 
		INode::GetObjectRef() should be used instead.
		Parameters:
		time Specifies the time to retrieve the object state.
		evalHidden If FALSE and the node is hidden, the pipeline will not actually be evaluated (however the TM will).
		Returns:
		The ObjectState that is the result of the pipeline. See Class ObjectState.
		*/
		ObjectState os = node->EvalWorldState(0);
		//The obj member of ObjectState is the actual object we will export.
		if (os.obj)
		{
			Class_ID cID = os.obj->ClassID();

			//We look at the super class ID to determine the type of the object.
			switch(os.obj->SuperClassID())
			{
			case GEOMOBJECT_CLASS_ID: 
				if (mExportMesh && cID != Class_ID(TARGET_CLASS_ID, 0))
				{
					exportMesh(node);
				}
				break;
			case CAMERA_CLASS_ID:
				if (mExportCamera)
				{
					//exportCamera(node);
				}
				break;
			case LIGHT_CLASS_ID:
				if (mExportLight)
				{
					//exportLight(node);
				}
				break;
			}
		}
	}

	int childCount = node->NumberOfChildren();
	for (int i = 0; i < childCount; i++)
	{
		enumNode(node->GetChildNode(i));
	}
}

void Impl::collectMaterials(INode* node)
{
	/*
	virtual Mtl* GetMtl()
	Returns a pointer to the renderer material for the node.If the value returned is NULL the user has not assigned a material yet.See Class Mtl,
	Materials, Textures and Maps.
	*/
	Mtl* mtl = node->GetMtl();

	if (mtl)
	{
		/*
		virtual BOOL IsMultiMtl()
		Remarks:
		This method returns TRUE for materials or textures that select sub-materials based on submaterial number (for example a mesh faceMtlIndex).
		The only materials for which this method should return TRUE are those that choose to use one of their sub-maps or sub-mtls based on the 
		submaterial number. For the majority of materials and maps, they should return FALSE, and in that case all of the submaterials and maps are 
		enumerated by MtlBase::Requirements().
		*/
		if (mtl->IsMultiMtl())
		{
			/*
			virtual int NumSubMtls()
			Returns the number of sub-materials managed by this material.
			*/
			int subMtlCount = mtl->NumSubMtls();
			assert(subMtlCount > 0);
			
			for (int i = 0; i < subMtlCount; ++i)
			{
				/*
				virtual Mtl* GetSubMtl(int i)
				Returns a pointer to the 'i-th' sub-material of this material.
				Parameters:
				i The index of the material to return.
				*/
				Mtl* subMtl = mtl->GetSubMtl(i);
				assert(subMtl);
				std::wstring subMtlWName = subMtl->GetName();
				std::string subMtlName = wstring2string(subMtlWName);
				assert(!subMtlName.empty());
				addUMEMaterial(subMtlName,subMtl);
			}
		}//if (mtl->IsMultiMtl())
		else
		{
			std::wstring mtlWName = mtl->GetName();
			std::string mtlName = wstring2string(mtlWName);
			assert(!mtlName.empty());
			addUMEMaterial(mtlName,mtl);
		}//if (mtl->IsMultiMtl())
	}//if (mtl)
}

void Impl::buildMaterial()
{
	mMaterialsCount = mUMEMaterials.size();

	for (usize i = 0; i < mMaterialsCount; i++)
	{
		UMEMaterial& umeMaterial = mUMEMaterials[i];

		Mtl* currentMtl = umeMaterial.mMtl;

		if (currentMtl->ClassID() == Class_ID(DMTL_CLASS_ID, 0))
		{
			StdMat* stdMtl = (StdMat*)currentMtl;
			assert(stdMtl);

			/*
			virtual float GetOpacity(TimeValue t)
			Returns the opacity setting at the specified time.
			*/
			float opacity = stdMtl->GetOpacity(0);
			if (opacity < 1.0)
			{
				/*
				virtual int GetTransparencyType()
				Returns the transparency type.
				Returns:One of the following values:
				TRANSP_SUBTRACTIVE
				TRANSP_ADDITIVE
				TRANSP_FILTER
				*/
				switch (stdMtl->GetTransparencyType()) 
				{
				case TRANSP_FILTER: 
					umeMaterial.mBlendMode = "blend";
					break;
				case TRANSP_SUBTRACTIVE: 
					umeMaterial.mBlendMode = "subtract";
					break;
				case TRANSP_ADDITIVE: 
					umeMaterial.mBlendMode = "add";
					break;
				default: 
					assert(0);
				}	
			}

			/*
			virtual BOOL GetTwoSided()
			Returns TRUE if two sided is on; otherwise FALSE.
			*/
			umeMaterial.mTwoSide = stdMtl->GetTwoSided();
		}

		umeMaterial.mAmbient = currentMtl->GetAmbient();
		umeMaterial.mDiffuse = currentMtl->GetDiffuse();
		umeMaterial.mSpecular = currentMtl->GetSpecular();
		umeMaterial.mShininess = currentMtl->GetShininess();
		umeMaterial.mShininessStrength = currentMtl->GetShinStr();
		umeMaterial.mTransparency = currentMtl->GetXParency();

		/*
		If we have a pointer to a Mtl class, we can get a pointer to a Texmap class.
		The flag ID_DI stands for Diffuse.
		In Max, the texture maps that are files are stored in the diffuse portion of the material.
		*/
		//Texmap* diffuseTexture = currentMtl->GetSubTexmap(ID_DI);

		usize textureCount = currentMtl->NumSubTexmaps();
		for (uint16 j = 0; j < textureCount; j++)
		{
			/*
			virtual Texmap* GetSubTexmap(int i)
			Returns a pointer to the 'i-th' sub-texmap managed by the material or texture.
			Note: For the 3ds Max Standard material, the sub-texmap indexes used with this method are listed in Texture Map 
			Indices.
			Parameters:
			int i
			Specifies the texmap to return.
			Default Implementation:
			{ return NULL; }
			*/
			Texmap* texture = currentMtl->GetSubTexmap(j);
			if (texture)
			{
				if (texture->ClassID() == Class_ID(BMTEX_CLASS_ID, 0x00))
				{
					//if (!((StdMat*)currentMtl)->MapEnabled(i))
					//{
					//	continue;
					//}

					/*
					virtual const MCHAR* GetMapName()
					Returns the name of the bitmap file.
					*/
					std::wstring textureWName = ((BitmapTex *)texture)->GetMapName();
					std::string textureName = wstring2string(textureWName);
					assert(!textureName.empty());
					addTexture(umeMaterial,textureName);
				}
			}//if (texture)
		}//for (uint16 j = 0; j < textureCount; j++)
	}
}

void Impl::addUMEMaterial(std::string name,Mtl* mtl)
{
	std::vector<UMEMaterial>::iterator beg = mUMEMaterials.begin();
	std::vector<UMEMaterial>::iterator end = mUMEMaterials.end();

	while (beg != end)
	{
		if (name == beg->mName)
		{
			return;
		}

		++beg;
	}

	UMEMaterial umeMaterial(name,mtl);
	mUMEMaterials.push_back(umeMaterial);
}

void Impl::addTexture(UMEMaterial& umeMaterial,std::string textureFileName)
{
	std::string justFileName = getFileName(textureFileName);
	uint16 textureCount = umeMaterial.mTextures.size();

	for(uint16 i = 0; i < textureCount; ++i)
	{
		if (justFileName == umeMaterial.mTextures[i])
		{
			return;
		}
	}

	umeMaterial.mTextures.push_back(justFileName);
}

TriObject* Impl::getTriObjectFromNode(INode* node,ObjectState* objectState,int& needDelete)
{
	needDelete = FALSE;

	Object* obj = objectState->obj;

	assert(obj);

	//Here we check to see if we can convert our mesh to a tri object
	if (obj->CanConvertToType(Class_ID(TRIOBJ_CLASS_ID, 0)))
	{
		//If the object isn't already a tri object this will create a new object
		TriObject* triObject = (TriObject*)obj->ConvertToType(0,Class_ID(TRIOBJ_CLASS_ID, 0));

		assert(triObject);

		//If the tri object isn't the same pointer as the original object,we need to be sure to free the pointer after we are done exporting.
		if ((TriObject*)obj != triObject)
		{
			needDelete = TRUE;
		}

		return triObject;
	}
	else
	{
		return NULL;
	}
}

void Impl::write()
{
	mWriter->setImpl(this);

	mWriter->doWriteMesh();

	mWriter->doWriteMaterial();
}

void Impl::end()
{
	Beep(523, 500);
	Beep(587, 500);
	Beep(659, 500);
	Beep(698, 500);
	Beep(784, 500);
	Beep(880, 500);
	Beep(980, 500);
	Beep(1060, 500);

	//����д����Ϣ��
	MessageBox(0,L"Has been exported to complete.",L"UNG",MB_OK);
}

/*
//���ַ���ת���ɿ��ַ���  
wstring string2Wstring(string sToMatch)  
{
	//����ת������ַ����ĳ���(�������ַ���������)
	int iWLen = MultiByteToWideChar(CP_ACP, 0, sToMatch.c_str(), sToMatch.size(), 0, 0);
	wchar_t *lpwsz = new wchar_t [iWLen + 1];
	MultiByteToWideChar( CP_ACP, 0, sToMatch.c_str(), sToMatch.size(), lpwsz, iWLen );
	lpwsz[iWLen] = L'/0';
	wstring wsToMatch(lpwsz);
	delete []lpwsz;

	return wsToMatch;
}
*/

/*
mTicksPerFrame = GetTicksPerFrame();

mFrameRate = GetFrameRate();

Interval range = mInterface->GetAnimRange();
mFirstFrame = range.Start() / mTicksPerFrame;
mLastFrame = range.End() / mTicksPerFrame;
*/