#include "UMEObjects.h"
#include "UMETools.h"
#include "Impl.h"
#include <inode.h>
#include <stdmat.h>

//从“size_t”转换到“uint16”，可能丢失数据
#pragma warning(disable : 4267)

UMEMesh::UMEMesh() :
	mNode(NULL),
	mMesh(NULL),
	mCoordSystem(-1),
	mOriginalVertexCount(0),
	mOriginalUVCount(0),
	mFaceCount(0),
	mMatIDCount(0)
{
	for (int i = 0; i < 3; i++)
	{
		mIndexOrder[i] = -1;
	}
}

void UMEMesh::build(Impl* impl,INode* node,Mesh* mesh,int coordSystem)
{
	setImpl(impl);

	setNode(node);
	setMesh(mesh);

	//------------------------------------------------------

	setCoordSystem(coordSystem);

	//------------------------------------------------------

	buildNodeTransformAtFirstFrame();
	buildIndexOrderOfFace();

	//------------------------------------------------------

	getOriginalVertices();

	//------------------------------------------------------

	getOriginalUVs();

	//------------------------------------------------------

	buildUMEFaceFace();
	buildUMEFaceUVFace();

	//------------------------------------------------------

	collectMatID();

	//------------------------------------------------------

	buildUMEFaceOI();

	//------------------------------------------------------

	buildUMEFaceMatID();
	buildUMEFaceSGID();
	buildUMEFaceArea();
	buildUMEFaceAngle();
	buildUMEFaceTI();

	//------------------------------------------------------

	buildUMEFaceNor();
	buildUMEFaceTanAndBitan();

	//------------------------------------------------------

	duplicateOriginalVerticesBasedOnSG();
	duplicateOriginalVerticesBasedOnUV();

	//------------------------------------------------------

	CorrespondingUVToVertex();

	//------------------------------------------------------

	adaptCoordinateSystem();

	//------------------------------------------------------

	buildOriginalTBNs();

	//------------------------------------------------------

	buildSubMeshs();

	//------------------------------------------------------

	tryStrip();
}

void UMEMesh::setImpl(Impl* impl)
{
	mImpl = impl;
}

void UMEMesh::setNode(INode* node)
{
	assert(node);

	mNode = node;
}

void UMEMesh::setMesh(Mesh* mesh)
{
	assert(mesh);

	mMesh = mesh;
}

void UMEMesh::setCoordSystem(int value)
{
	mCoordSystem = value;
}

void UMEMesh::buildNodeTransformAtFirstFrame()
{
	/*
	INode::GetObjTMAfterWSM() - This method explicitly gets the full node transformation matrix and object-offset 
	transformation and world space modifier effect unless the points of the object have already been transformed 
	into world space in which case it will return the identity matrix.
	*/
	mTransform = mNode->GetObjTMAfterWSM(0);
}

void UMEMesh::buildIndexOrderOfFace()
{
	BOOL isNegScale = isNegativeScale(mTransform);

	/*
	Order of the vertices. Get them counter clockwise if the objects is negatively scaled.
	If the polygon order is not normal, reverse it.
	*/
	if(isNegScale)
	{
		mIndexOrder[0] = 2;
		mIndexOrder[1] = 1;
		mIndexOrder[2] = 0;
	}
	else
	{
		mIndexOrder[0] = 0;
		mIndexOrder[1] = 1;
		mIndexOrder[2] = 2;
	}
}

void UMEMesh::getOriginalVertices()
{
	//原始顶点数量
	mOriginalVertexCount = mMesh->getNumVerts();

	for (usize i = 0; i < mOriginalVertexCount; i++)
	{
		//把对象空间的Point3顶点先变换到世界空间，然后再用Point3来构造一个UMEVector3顶点
		UMEVector3 v(mTransform * mMesh->verts[i]);
		//保存位于世界空间的原始顶点
		mOriginalVertices.push_back(v);
	}
}

void UMEMesh::getOriginalUVs()
{
	//原始UV数量
	/*
	int getNumTVerts()const
	Returns the number of texture vertices (in mapping channel 1).
	{ return numTVerts; }
	*/
	mOriginalUVCount = mMesh->getNumTVerts();
	for (usize i = 0; i < mOriginalUVCount; i++)
	{
		/*
		UVVert* tVerts
		The array of texture vertices.
		This stores the UVW coordinates. For a 2D mapping only two of them are used, i.e. UV, VW, or WU.
		This just provides greater flexibility so the user can choose to use UV, VW, or WU. Note: typedef Point3 UVVert;
		*/
		UVVert uv = mMesh->tVerts[i];

		mOriginalUVs.push_back(uv);
	}
}

void UMEMesh::buildUMEFaceFace()
{
	mFaceCount = mMesh->getNumFaces();

	for (usize i = 0; i < mFaceCount; ++i)
	{
		Face const& face = mMesh->faces[i];

		if (isTriangle(face))
		{
			UMEFace umeFace;

			umeFace.mFace = face;

			mUMEFaces.push_back(umeFace);
		}
	}

	//真正三角形的数量
	mFaceCount = mUMEFaces.size();
	assert(mFaceCount <= mMesh->getNumFaces());
}

void UMEMesh::buildUMEFaceUVFace()
{
	assert(mFaceCount);
	for (usize i = 0; i < mFaceCount; ++i)
	{
		if (mOriginalUVCount > 0)
		{
			mUMEFaces[i].mUVFace = mMesh->tvFace[i];
		}
	}
}

void UMEMesh::collectMatID()
{
	if (mOriginalUVCount == 0)
	{
		return;
	}

	assert(mFaceCount);
	for (int i = 0; i < mFaceCount; ++i)
	{
		Face face = mUMEFaces[i].mFace;
		//当前face的材质ID
		int matID = face.getMatID();
		assert(matID >= 0);
		//把当前mesh的所有faces的材质ID给存储到一个map中，键值为材质ID，second为一个容器，用于存储材质ID为first的所有face在mesh中的index
		mMatIDPool[matID].push_back(i);
	}

	mMatIDCount = mMatIDPool.size();
	assert(mMatIDCount >= 1);

	//检查
	std::map<int,std::vector<int>>::const_iterator cbeg = mMatIDPool.cbegin();
	std::map<int,std::vector<int>>::const_iterator cend = mMatIDPool.cend();

	usize faceCount = 0;

	while (cbeg != cend)
	{
		faceCount += cbeg->second.size();

		++cbeg;
	}

	assert(faceCount == mFaceCount);
}

void UMEMesh::buildUMEFaceOI()
{
	umefaces_iterator beg = getUMEFacesBegin();
	umefaces_iterator end = getUMEFacesEnd();

	while (beg != end)
	{
		for (int i = 0; i < 3; i++)
		{
			//当前顶点在原始顶点数据中的索引，对应顶点容器为mOriginalVertices
			uint16 index = beg->mFace.v[mIndexOrder[i]];
			beg->mOI[i] = index;
			
			assert(index < mOriginalVertexCount);
		}

		++beg;
	}
}

void UMEMesh::buildUMEFaceMatID()
{
	if (mOriginalUVCount == 0)
	{
		return;
	}

	umefaces_iterator beg = getUMEFacesBegin();
	umefaces_iterator end = getUMEFacesEnd();

	while (beg != end)
	{
		beg->mMatID = beg->mFace.getMatID();

		assert(beg->mMatID >= 0);

		++beg;
	}
}

void UMEMesh::buildUMEFaceSGID()
{
	umefaces_iterator beg = getUMEFacesBegin();
	umefaces_iterator end = getUMEFacesEnd();
	
	while (beg != end)
	{
		beg->mSGID = beg->mFace.smGroup;

		assert(beg->mSGID != 0xFFFFFFFF);

		++beg;
	}
}

void UMEMesh::buildUMEFaceArea()
{
	umefaces_iterator beg = getUMEFacesBegin();
	umefaces_iterator end = getUMEFacesEnd();

	while (beg != end)
	{
		UMEVector3 edge1_0 = getUMEFaceEdge(*beg,1,0);
		UMEVector3 edge2_0 = getUMEFaceEdge(*beg,2,0);
		beg->mArea = (edge1_0.crossProduct(edge2_0)).length() * 0.5;

		assert(beg->mArea > 0.0);

		++beg;
	}
}

void UMEMesh::buildUMEFaceAngle()
{
	umefaces_iterator beg = getUMEFacesBegin();
	umefaces_iterator end = getUMEFacesEnd();

	while (beg != end)
	{
		UMEVector3 edge1_0 = getUMEFaceEdge(*beg,1,0);
		UMEVector3 edge2_0 = getUMEFaceEdge(*beg,2,0);

		UMEVector3 edge0_1 = getUMEFaceEdge(*beg,0,1);
		UMEVector3 edge2_1 = getUMEFaceEdge(*beg,2,1);

		UMEVector3 edge0_2 = getUMEFaceEdge(*beg,0,2);
		UMEVector3 edge1_2 = getUMEFaceEdge(*beg,1,2);

		beg->mAngle[0] = edge1_0.angleBetween(edge2_0);
		beg->mAngle[1] = edge0_1.angleBetween(edge2_1);
		beg->mAngle[2] = edge0_2.angleBetween(edge1_2);

		assert(beg->mAngle[0] > 0.0);
		assert(beg->mAngle[1] > 0.0);
		assert(beg->mAngle[2] > 0.0);

		++beg;
	}
}

void UMEMesh::buildUMEFaceTI()
{
	if (mOriginalUVCount == 0)
	{
		return;
	}

	umefaces_iterator beg = getUMEFacesBegin();
	umefaces_iterator end = getUMEFacesEnd();

	while (beg != end)
	{
		for (uint16 i = 0; i < 3; ++i)
		{
			uint16 vertexIndex = mIndexOrder[i];
			beg->mTI[i] = beg->mUVFace.t[vertexIndex];
		}

		++beg;
	}
}

void UMEMesh::buildUMEFaceNor()
{
	umefaces_iterator beg = getUMEFacesBegin();
	umefaces_iterator end = getUMEFacesEnd();

	while (beg != end)
	{
		UMEVector3 edge1_0 = getUMEFaceEdge(*beg,1,0);
		UMEVector3 edge2_0 = getUMEFaceEdge(*beg,2,0);
		beg->mNor = (edge1_0.crossProduct(edge2_0)).normalize();

		correctPerpendicular(beg->mNor,edge1_0.normalize());
		correctPerpendicular(beg->mNor,edge2_0.normalize());

		++beg;
	}
}

void UMEMesh::buildUMEFaceTanAndBitan()
{
	umefaces_iterator beg = getUMEFacesBegin();
	umefaces_iterator end = getUMEFacesEnd();

	while (beg != end)
	{
		UMEVector3 edge1_0 = getUMEFaceEdge(*beg,1,0);
		UMEVector3 edge2_0 = getUMEFaceEdge(*beg,2,0);

		UMEVector2 uvEdge1_0 = getUMEFaceUVEdge(*beg,1,0);
		UMEVector2 uvEdge2_0 = getUMEFaceUVEdge(*beg,2,0);

		float r = 1.0 / (uvEdge1_0.x * uvEdge2_0.y - uvEdge2_0.x * uvEdge1_0.y);
		//s方向
		beg->mTan = UMEVector3((uvEdge2_0.y * edge1_0.x - uvEdge1_0.y * edge2_0.x) * r, (uvEdge2_0.y * edge1_0.y - uvEdge1_0.y * edge2_0.y) * r,
			(uvEdge2_0.y * edge1_0.z - uvEdge1_0.y * edge2_0.z) * r);
		//t方向
		beg->mBitan = UMEVector3((uvEdge1_0.x * edge2_0.x - uvEdge2_0.x * edge1_0.x) * r, (uvEdge1_0.x * edge2_0.y - uvEdge2_0.x * edge1_0.y) * r,
			(uvEdge1_0.x * edge2_0.z - uvEdge2_0.x * edge1_0.z) * r);


		//handedness
		UMEVector3 NxT = beg->mNor.crossProduct(beg->mTan);
		float NxT_dot_B = NxT.dotProduct(beg->mBitan);
		float handedness = (NxT_dot_B < 0.0) ? -1.0 : 1.0;

		beg->mBitan = NxT * handedness;
		//正交化
		applyGramSchmidt(beg->mNor,beg->mTan,beg->mBitan);

		++beg;
	}
}

void UMEMesh::duplicateOriginalVerticesBasedOnSG()
{
	assert(mBooks.size() == 0);

	groupingVerticesBasedOnSG();

	duplicateOriginalVerticesImpl();
}

void UMEMesh::duplicateOriginalVerticesBasedOnUV()
{
	assert(mBooks.size() == 0);

	groupingVerticesBasedOnUV();

	duplicateOriginalVerticesImpl();
}

void UMEMesh::CorrespondingUVToVertex()
{
	//先把之前的uv容器给暂存
	std::vector<UMEVector2> tempUVs = mOriginalUVs;

	//清空mOriginalUVs
	mOriginalUVs.clear();

	//让mOriginalUVs的大小等于mOriginalVertexCount
	mOriginalUVs.resize(mOriginalVertexCount);

	//用于检查是否还存在同一个顶点具有不同uv值的情况
	std::vector<bool> flags;
	for (int i = 0; i < mOriginalVertexCount; i++)
	{
		flags.push_back(false);
	}

	//遍历face
	for (int i = 0; i < mFaceCount; i++)
	{
		//当前UMEFace
		UMEFace const& umeFace = mUMEFaces[i];

		for (uint16 j = 0; j < 3; j++)
		{
			//当前顶点在mOriginalVertices中的pos
			uint16 vPos = umeFace.mOI[j];

			//当前顶点在mOriginalUVs中的pos
			uint16 uvPos = umeFace.mTI[j];
			//当前顶点的uv值
			UMEVector2 uvValue = tempUVs[uvPos];

			//保存当前顶点的uv值到mOriginalUVs容器中的vPos位置
			//检查vPos这个位置是否已经有值了
			bool ret = flags[vPos];
			if (ret)
			{
				//如果已经有值了，那么检查已经存在的值和我们将要保存的值是否不相等
				bool isEqualRet = isEqual(uvValue,mOriginalUVs[vPos]);
				if (!isEqualRet)
				{
					//不相等，说明我们前面基于uv不同而对顶点分裂的代码有错误
					assert(0 && "same pos in mOriginalUVs has different uv values.");
				}
			}
			else
			{
				mOriginalUVs[vPos] = uvValue;

				//标记
				flags[vPos] = true;
			}
		}
	}

	//更新mOriginalUVCount
	mOriginalUVCount = mOriginalUVs.size();

	assert(mOriginalUVCount == mOriginalVertexCount);
}

void UMEMesh::adaptCoordinateSystem()
{
	assert(mCoordSystem == 0 || mCoordSystem == 1);

	if (mCoordSystem == 0)
	{
		originalVerticesToOpenGL();
	}
	else
	{
		originalVerticesToDirect3D();
	}
}

void UMEMesh::buildOriginalTBNs()
{
	//初始化
	UMEVector3 zero;
	for (usize i = 0; i < mOriginalVertexCount; i++)
	{
		mOriginalTangents.push_back(zero);
		mOriginalBitangents.push_back(zero);
		mOriginalNormals.push_back(zero);
	}

	assert(mBooks.size() == 0);

	//map只会有一个条目，因为顶点已经根据光滑组分裂过了
	groupingVerticesBasedOnSG();

	buildOriginalTBNsImpl();
}

void UMEMesh::buildSubMeshs()
{
	buildSubMeshMaterial();

	buildSubMeshFace();

	buildSubMeshVertexCount();

	//检查
	usize totalVertexCount = 0;
	for (uint16 i = 0; i < mSubMeshs.size(); i++)
	{
		//当前子网格
		UMESubMesh& subMesh = mSubMeshs[i];

		totalVertexCount += subMesh.mVertexCount;

		assert(subMesh.mListIndices.size() == subMesh.mFaceCount * 3);
	}

	assert(totalVertexCount == mOriginalVertexCount);
}

void UMEMesh::tryStrip()
{
	uint16 subMeshCount = mSubMeshs.size();

	//遍历所有的子网格
	for (uint16 i = 0; i < subMeshCount; i++)
	{
		//当前子网格
		UMESubMesh& subMesh = mSubMeshs[i];

		if (subMesh.mTrianglesMode == "List")
		{
			return;
		}

		BOOL ret = doStrip(subMesh.mListIndices,subMesh.mStripIndices);

		if (!ret)
		{
			subMesh.mTrianglesMode = "List";
		}
	}
}