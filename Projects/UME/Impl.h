#pragma once

#include "Include.h"
#include "UMEObjects.h"
#include <istdplug.h>

class Writer;

class Impl
{
public:
	Impl();

	~Impl();

	std::string mFileName;
	Interface* mInterface;
	HWND hWnd;
	BOOL mShowPrompts;
	BOOL mExportSelected;

	BOOL mExportMesh;
	BOOL mExportMaterial;
	BOOL mExportLight;
	BOOL mExportCamera;

	BOOL mExportMeshPosition;
	BOOL mExportMeshIndex;
	BOOL mExportMeshColor;
	BOOL mExportMeshUV;
	BOOL mExportMeshNormal;
	BOOL mExportMeshTangent;
	BOOL mExportMeshBitangent;

	int mCoordSystem;
	int mFormat;
	int mStripList;

	UMEStatistics mStatistics;

	//场景--------------------------------------------------------------------------------------------------------------------------------

	size_t mTotalNodeCount;
	size_t mTotalMaterialCount;
	int mTicksPerFrame;
	int mFrameRate;
	int mFirstFrame;
	int mLastFrame;
	TimeValue mStaticFrame;

	//节点--------------------------------------------------------------------------------------------------------------------------------

	INode* mRootNode;
	void enumNode(INode* node);
	TriObject* getTriObjectFromNode(INode* node,ObjectState* objectState,int& needDelete);

	//材质
	std::vector<UMEMaterial> mUMEMaterials;
	usize mMaterialsCount;
	void collectMaterials(INode* node);
	void buildMaterial();
	void addUMEMaterial(std::string name,Mtl* mtl);
	void addTexture(UMEMaterial& umeMaterial,std::string textureFileName);

	//网格--------------------------------------------------------------------------------------------------------------------------------
	
	BOOL isMiscGeometry(INode* node, ObjectState* objectState);

	void exportMesh(INode* node);

	std::vector<UMEMesh> mMeshs;

	void write();

	void end();

	Writer* mWriter;
};