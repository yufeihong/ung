#pragma once

#include <imtl.h>

typedef unsigned short uint16;
typedef unsigned int usize;

class UMEVector2
{
public:
	UMEVector2() :
		x(0.0),
		y(0.0)
	{
	}

	UMEVector2(float xx,float yy) :
		x(xx),
		y(yy)
	{
	}

	UMEVector2(Point3 const& p3) :
		x(p3.x),
		y(p3.y)
	{
	}

	UMEVector2 operator-(UMEVector2 const& v) const
	{
		return UMEVector2(x - v.x,y - v.y);
	}

	BOOL operator==(UMEVector2 const& v) const
	{
		return x == v.x && y == v.y;
	}

	BOOL operator!=(UMEVector2 const& v) const
	{
		return !(operator==(v));
	}

	float x,y;
};

class UMEVector3
{
public:
	UMEVector3() :
		x(0.0),
		y(0.0),
		z(0.0)
	{
	}

	UMEVector3(float xx,float yy,float zz) :
		x(xx),
		y(yy),
		z(zz)
	{
	}

	UMEVector3(Point3 const& p3) :
		x(p3.x),
		y(p3.y),
		z(p3.z)
	{
	}

	UMEVector3(Color const& c) :
		x(c.r),
		y(c.g),
		z(c.b)
	{
	}

	UMEVector3(UMEVector3 const& v) :
		x(v.x),
		y(v.y),
		z(v.z)
	{
	}

	UMEVector3 operator-(UMEVector3 const& v) const
	{
		return UMEVector3(x - v.x,y - v.y,z - v.z);
	}

	UMEVector3 operator*(float s)
	{
		return UMEVector3(x * s,y * s,z * s);
	}

	UMEVector3 operator/(float s)
	{
		assert(s != 0.0);

		float invS = 1.0 / s ;

		return this->operator*(invS);
	}

	UMEVector3& operator+=(UMEVector3 const& v)
	{
		x += v.x;
		y += v.y;
		z += v.z;

		return *this;
	}

	BOOL operator==(UMEVector3 const& v) const
	{
		return x == v.x && y == v.y && z == v.z;
	}

	UMEVector3 crossProduct(const UMEVector3& v) const
	{
		return UMEVector3(y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x);
	}

	float length() const
	{
		return std::sqrt(x * x + y * y + z * z);
	}

	UMEVector3 normalize()
	{
		float len = length();
		assert(len > 0.0f);

		float invLen = 1.0 / len;
		x *= invLen;
		y *= invLen;
		z *= invLen;

		len = length();
		assert(fabs(len - 1.0) < 1.0e-6);

		return *this;
	}

	float dotProduct(const UMEVector3& v) const
	{
		return (x * v.x + y * v.y + z * v.z);
	}

	float angleBetween(const UMEVector3& v) const
	{
		UMEVector3 v1 = *this;
		v1.normalize();
		UMEVector3 v2 = v;
		v2.normalize();

		float dotProd = v1.dotProduct(v2);

		if (dotProd < -1.0)
		{
			dotProd = -1.0;
		}
		if (dotProd > 1.0)
		{
			dotProd = 1.0;
		}

		return std::acos(dotProd);
	}

	float x,y,z;
};

class UMEVector4
{
public:
	UMEVector4() :
		x(0.0),
		y(0.0),
		z(0.0),
		w(0.0)
	{
	}

	UMEVector4(UMEVector3 const& v3) :
		x(v3.x),
		y(v3.y),
		z(v3.z),
		w(0.0)
	{
	}

	float x,y,z,w;
};

class UMEMatrix4
{
public:
	UMEMatrix4()
	{
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				m[i][j] = 0.0;
			}
		}
	}

	float m[4][4];
};