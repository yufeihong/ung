#include "UMEObjects.h"
#include "Impl.h"
#include "UMETools.h"
#include "UPS.h"
#include "boost/lexical_cast.hpp"

#pragma warning(disable : 4267)

/*
This returns a boolean telling us if our polygon order is normal or not.
This comes in handy when artists create characters that has mirroring.This way normals and polygons are rendered backwards when they shouldn't be.

Determine is the node has negative scaling.
This is used for mirrored objects for example. They have a negative scale factor so when calculating the normal we should 
take the vertices counter clockwise.
If we don't compensate(抵消，补偿) for this the objects will be 'inverted'.
*/
BOOL UMEMesh::isNegativeScale(Matrix3& mat)
{
	/*
	We take our transformation matrix for the current mesh and then take the cross product between the first and second row.We then take the dot product of 
	that result with the third row in the matrix.
	By checking the result of the dot product we can tell if the object's transform matrix is inside out, which might be caused by mirroring.
	*/
	return (DotProd(CrossProd(mat.GetRow(0),mat.GetRow(1)),mat.GetRow(2)) < 0.0) ? 1:0;
}

UMEMesh::umefaces_iterator UMEMesh::getUMEFacesBegin()
{
	return mUMEFaces.begin();
}

UMEMesh::umefaces_iterator UMEMesh::getUMEFacesEnd()
{
	return mUMEFaces.end();
}

BOOL UMEMesh::isTriangle(Face const& face)
{
	uint16 vIndex0 = face.v[mIndexOrder[0]];
	uint16 vIndex1 = face.v[mIndexOrder[1]];
	uint16 vIndex2 = face.v[mIndexOrder[2]];

	if (vIndex0 == vIndex1 || vIndex0 == vIndex2 || vIndex1 == vIndex2)
	{
		return FALSE;
	}

	return TRUE;
}

UMEVector3 UMEMesh::getUMEFaceEdge(UMEFace umeFace,uint16 fromVertexIndex,uint16 toVertexIndex) const
{
	assert(fromVertexIndex < 3 && toVertexIndex < 3);

	return getOriginalVertexAtIndex(umeFace.mOI[fromVertexIndex]) - getOriginalVertexAtIndex(umeFace.mOI[toVertexIndex]);
}

UMEVector2 UMEMesh::getUMEFaceUVEdge(UMEFace umeFace,uint16 fromVertexIndex,uint16 toVertexIndex) const
{
	assert(fromVertexIndex < 3 && toVertexIndex < 3);

	return getOriginalUVAtIndex(umeFace.mTI[fromVertexIndex]) - getOriginalUVAtIndex(umeFace.mTI[toVertexIndex]);
}

std::pair<BOOL,uint16> UMEMesh::isTwoFacesShareVertex(UMEFace const& A,UMEFace const& B,uint16 vertexIndex)
{
	assert(vertexIndex >= 0 && vertexIndex < mOriginalVertexCount);

	std::pair<BOOL,uint16> p = std::make_pair(FALSE,0xFFFF);

	if (A.mOI[vertexIndex] == B.mOI[0])
	{
		p.first = TRUE;
		p.second = 0;
	}
	else if (A.mOI[vertexIndex] == B.mOI[1])
	{
		p.first = TRUE;
		p.second = 1;
	}
	else if (A.mOI[vertexIndex] == B.mOI[2])
	{
		p.first = TRUE;
		p.second = 2;
	}

	return p;
}

void UMEMesh::groupingVerticesBasedOnSG()
{
	//遍历所有的顶点
	for (int vertexIndex = 0; vertexIndex < mOriginalVertexCount; vertexIndex++)
	{
		BookValue book;
		book.mVertexPos = vertexIndex;

		//遍历face
		for (usize faceIndex = 0; faceIndex < mFaceCount; faceIndex++)
		{
			//当前face
			UMEFace umeFace = mUMEFaces[faceIndex];

			//当前face是否有使用当前顶点
			bool isUse = false;

			//遍历face的三个顶点
			for(uint16 i = 0; i < 3; ++i)
			{
				//当前face的当前顶点的索引是否与vertexIndex相等
				if (umeFace.mOI[i] == vertexIndex)
				{
					isUse = true;

					break;
				}
			}//遍历face的三个顶点

			//如果当前face有使用vertexIndex这个顶点
			if (isUse)
			{
				//当前face的SG
				DWORD sgID = umeFace.mSGID;

				//那么把当前face的信息，给挂到这个顶点下面
				book.mDirtyFaces[sgID].push_back(faceIndex);
			}
		}//遍历face

		//保存这个顶点的信息
		mBooks.push_back(book);
	}//遍历所有的顶点
}

void UMEMesh::groupingVerticesBasedOnUV()
{
	//遍历所有的顶点
	for (int vertexIndex = 0; vertexIndex < mOriginalVertexCount; vertexIndex++)
	{
		BookValue book;
		book.mVertexPos = vertexIndex;

		DWORD uvPos;

		//遍历face
		for (usize faceIndex = 0; faceIndex < mFaceCount; faceIndex++)
		{
			//当前face
			UMEFace umeFace = mUMEFaces[faceIndex];

			//当前face是否有使用当前顶点
			bool isUse = false;

			//遍历face的三个顶点
			for(uint16 i = 0; i < 3; ++i)
			{
				//当前face的当前顶点的索引是否与vertexIndex相等
				if (umeFace.mOI[i] == vertexIndex)
				{
					isUse = true;

					//当前顶点的uvPos
					uvPos = umeFace.mTI[i];

					break;
				}
			}//遍历face的三个顶点

			//如果当前face有使用vertexIndex这个顶点
			if (isUse)
			{
				//那么把当前face的信息，给挂到这个顶点下面
				book.mDirtyFaces[uvPos].push_back(faceIndex);
			}
		}//遍历face

		//保存这个顶点的信息
		mBooks.push_back(book);
	}//遍历所有的顶点
}

void UMEMesh::duplicateOriginalVerticesImpl()
{
	usize bookSize = mBooks.size();

	//遍历被book的所有顶点
	for (int i = 0; i < bookSize; i++)
	{
		//当前顶点的信息
		BookValue const& vertexInfo = mBooks[i];

		std::map<DWORD,std::vector<int>>::const_iterator cbeg = vertexInfo.mDirtyFaces.cbegin();
		std::map<DWORD,std::vector<int>>::const_iterator cend = vertexInfo.mDirtyFaces.cend();

		//遍历根据光滑组或者uvPos分类的faces，第一组不动
		++cbeg;

		while (cbeg != cend)
		{
			//得到faces容器
			std::vector<int> const& faces = cbeg->second;
			usize faceSize = faces.size();
			//遍历faces容器
			for (int j = 0; j < faceSize; j++)
			{
				//当前face的位置
				int facePos = faces[j];
				//当前face
				UMEFace& face = mUMEFaces[facePos];
				//找到当前face的0,1,2
				int vIndex = -1;
				for (int k = 0; k < 3; k++)
				{
					if (vertexInfo.mVertexPos == face.mOI[k])
					{
						vIndex = k;
						break;
					}
				}
				assert(vIndex != -1);

				//修改当前face的vIndex的索引
				face.mOI[vIndex] = mOriginalVertices.size();
			}

			//要复制的顶点
			UMEVector3 v = mOriginalVertices[vertexInfo.mVertexPos];
			mOriginalVertices.push_back(v);
			mOriginalVertexCount = mOriginalVertices.size();

			++cbeg;
		}
	}

	mBooks.clear();
}

void UMEMesh::buildOriginalTBNsImpl()
{
	usize bookSize = mBooks.size();

	//遍历被book的所有顶点
	for (int i = 0; i < bookSize; i++)
	{
		//当前顶点的信息
		BookValue const& vertexInfo = mBooks[i];

		assert(vertexInfo.mDirtyFaces.size() == 1);

		//用于累加
		UMEVector3 T,B,N;

		//得到faces容器
		std::vector<int> const& faces = (vertexInfo.mDirtyFaces.cbegin())->second;
		usize faceSize = faces.size();
		//遍历faces容器
		for (int j = 0; j < faceSize; j++)
		{
			//当前face的位置
			int facePos = faces[j];
			//当前face
			UMEFace& face = mUMEFaces[facePos];
			float area = face.mArea;
			UMEVector3 facetT = face.mTan;
			UMEVector3 facetB = face.mBitan;
			UMEVector3 facetN = face.mNor;
			//找到当前face的哪个顶点使用了当前顶点，并得到那个顶点的夹角
			float angle = 361.0;
			for (int k = 0; k < 3; k++)
			{
				if (vertexInfo.mVertexPos == face.mOI[k])
				{
					//当前顶点的夹角
					angle = face.mAngle[k];

					break;
				}
			}
			assert(!isEqual(angle,361.0));

			//给当前顶点的TBNs来累加当前face的TBN
			T += facetT * area * angle;
			B += facetB * area * angle;
			N += facetN * area * angle;
		}//遍历faces容器

		//handedness
		UMEVector3 NxT = N.crossProduct(T);
		float NxT_dot_B = NxT.dotProduct(B);
		float handedness = (NxT_dot_B < 0.0) ? -1.0 : 1.0;
		B = NxT * handedness;

		//正交化
		applyGramSchmidt(N,T,B);

		mOriginalTangents[vertexInfo.mVertexPos] = T;
		mOriginalTangents[vertexInfo.mVertexPos].w = handedness;
		mOriginalBitangents[vertexInfo.mVertexPos] = B;
		mOriginalNormals[vertexInfo.mVertexPos] = N;
	}//遍历被book的所有顶点

	mBooks.clear();
}

void UMEMesh::applyGramSchmidt(UMEVector3& alpha1,UMEVector3& alpha2,UMEVector3& alpha3)
{
	/*
	UMEVector3 beta1 = alpha1;

	float b1dotb1 = beta1.dotProduct(beta1);

	UMEVector3 beta2 = alpha2 - beta1 * (alpha2.dotProduct(beta1) / b1dotb1);

	float b2dotb2 = beta2.dotProduct(beta2);

	UMEVector3 beta3 = alpha3 - beta1 * (alpha3.dotProduct(beta1) / b1dotb1) - beta2 * (alpha3.dotProduct(beta2) / b2dotb2);
	*/

	//再修正
	UMEVector3 N = alpha1.normalize();
	UMEVector3 T = alpha2.normalize();
	UMEVector3 B = alpha3.normalize();
	correctPerpendicular(T,N);
	correctPerpendicular(B,N);
	correctPerpendicular(B,T);

	//返回
	alpha1 = N;
	alpha2 = T;
	alpha3 = B;
}

/*
3dx Max采用右手坐标系
OpenGL采用右手坐标系
Direct3D采用左手坐标系

3dx Max中，垂直轴定义为z轴
OpenGL中，垂直轴定义为y轴
*/
void UMEMesh::originalVerticesToOpenGL()
{
	for(size_t i = 0; i < mOriginalVertexCount; ++i)
	{
		UMEVector3 v = mOriginalVertices[i];

		mOriginalVertices[i].y = v.z;
		mOriginalVertices[i].z = -v.y;
	}
}

void UMEMesh::originalVerticesToDirect3D()
{
	for(size_t i = 0; i < mOriginalVertexCount; ++i)
	{
		UMEVector3 v = mOriginalVertices[i];

		mOriginalVertices[i].y = v.z;
		mOriginalVertices[i].z = v.y;
	}
}

void UMEMesh::indicesToDirect3D(std::vector<uint16>& indicesVector)
{
	size_t indexCount = indicesVector.size();

	for (size_t i = 0; i < indexCount; i += 3)
	{
	std::swap(indicesVector[i + 1],indicesVector[i + 2]);
	}
}

void UMEMesh::buildSubMeshMaterial()
{
	Mtl* mtl = mNode->GetMtl();

	//如果没有材质的话，那么就push一个子网格，并且标明这个子网格需要应用程序给其一个默认材质
	if (!mtl)
	{
		UMESubMesh umeSubMesh;

		/*
		一个立方体，可能没有材质，或者可能有一个材质，但是可能会存在6个面，每个面具有不同的matID。
		所以，这里处理没有材质的情况，直接忽略掉matID这个信息。
		*/
		assert(umeSubMesh.mMaterialName.empty());

		mSubMeshs.push_back(umeSubMesh);

		return;
	}

	uint16 mtlCount = mtl->NumSubMtls();

	if (mtlCount == 0)
	{
		assert(mMatIDCount == 1);

		UMESubMesh umeSubMesh;

		/*
		一个立方体，可能有一个材质，但是可能会存在6个面，每个面具有不同的matID。
		所以，这里处理一个材质的情况，直接忽略掉matID这个信息。
		*/
		umeSubMesh.mMaterialName = mImpl->mUMEMaterials[0].mName;
		assert(!umeSubMesh.mMaterialName.empty());

		mSubMeshs.push_back(umeSubMesh);

		return;
	}

	assert(mImpl->mMaterialsCount == mMatIDCount);

	std::map<int,std::vector<int>>::const_iterator cbeg = mMatIDPool.cbegin();
	std::map<int,std::vector<int>>::const_iterator cend = mMatIDPool.cend();

	while (cbeg != cend)
	{
		UMESubMesh umeSubMesh;

		umeSubMesh.mMatID = cbeg->first;
		Mtl* subMaterial = mtl->GetSubMtl(umeSubMesh.mMatID % mtlCount);

		int pos = -1;
		for (uint16 i = 0; i < mImpl->mMaterialsCount; i++)
		{
			if (subMaterial == mImpl->mUMEMaterials[i].mMtl)
			{
				pos = i;
			}
		}

		assert(pos != -1);

		umeSubMesh.mMaterialName = mImpl->mUMEMaterials[pos].mName;
		assert(!umeSubMesh.mMaterialName.empty());

		mSubMeshs.push_back(umeSubMesh);

		++cbeg;
	}
}

void UMEMesh::buildSubMeshFace()
{
	uint16 subMeshCount = mSubMeshs.size();

	//遍历所有的子网格
	for (uint16 i = 0; i < subMeshCount; i++)
	{
		//当前子网格
		UMESubMesh& subMesh = mSubMeshs[i];

		//设置三角形模式
		if (mImpl->mStripList == 0)
		{
			subMesh.mTrianglesMode = "Strip";
		}

		//如果当前子网格没有材质信息
		if (subMesh.mMaterialName.empty())
		{
			//那么当前子网格的所有face就是容器mUMEFaces中的所有face

			subMesh.mFaceCount = mUMEFaces.size();

			for (usize j = 0; j < subMesh.mFaceCount; j++)
			{
				//当前UMEFace
				UMEFace const& umeFace = mUMEFaces[j];

				for (uint16 k = 0; k < 3; k++)
				{
					//当前顶点的索引
					uint16 vertexIndex = umeFace.mOI[k];

					assert(vertexIndex < mOriginalVertexCount);

					//保存当前顶点的索引
					subMesh.mListIndices.push_back(vertexIndex);
				}
			}
		}
		else
		{
			//存储当前子网格的所有face的容器
			std::vector<int>& facesOfSubMesh = mMatIDPool[subMesh.mMatID];
			//当前子网格的face数量
			subMesh.mFaceCount = facesOfSubMesh.size();

			//遍历当前子网格的所有face
			std::vector<int>::iterator beg = facesOfSubMesh.begin();
			std::vector<int>::iterator end = facesOfSubMesh.end();
			while (beg != end)
			{
				//当前UMEFace在mUMEFaces中的pos
				int pos = *beg;
				//当前UMEFace
				UMEFace const& umeFace = mUMEFaces[pos];

				for (uint16 k = 0; k < 3; k++)
				{
					//当前顶点的索引
					uint16 vertexIndex = umeFace.mOI[k];

					assert(vertexIndex < mOriginalVertexCount);

					//保存当前顶点的索引
					subMesh.mListIndices.push_back(vertexIndex);
				}

				++beg;
			}
		}
		

		//如果输出为Direct3D的话，则需要调整三角形顺序
		assert(mCoordSystem == 0 || mCoordSystem == 1);
		if (mCoordSystem == 1)
		{
			indicesToDirect3D(subMesh.mListIndices);
		}
	}//for (uint16 i = 0; i < subMeshCount; i++)
}

void UMEMesh::buildSubMeshVertexCount()
{
	uint16 subMeshCount = mSubMeshs.size();

	//遍历所有的子网格
	for (uint16 i = 0; i < subMeshCount; i++)
	{
		//当前子网格
		UMESubMesh& subMesh = mSubMeshs[i];

		//统计当前子网格的顶点数量
		std::vector<BOOL> flags;
		for (int m = 0; m < mOriginalVertexCount; m++)
		{
			flags.push_back(FALSE);
		}
		for(uint16 k = 0; k < subMesh.mListIndices.size(); ++k)
		{
			//当前索引值
			uint16 index = subMesh.mListIndices[k];
			flags[index] = TRUE;
		}
		assert(subMesh.mVertexCount == 0);
		for (int p = 0; p < flags.size(); p++)
		{
			if (flags[p])
			{
				++subMesh.mVertexCount;
			}
		}
	}
}

BOOL UMEMesh::doStrip(std::vector<uint16> const& indicies,std::vector<uint16>& out)
{
	BOOL ret = FALSE;

	unsigned short indexCount = indicies.size();

	if (indexCount > 65536)
	{
		return ret;
	}

	PrimitiveGroup* primitiveGroup;
	unsigned short numprims;
	BOOL originalStripRet = generateStrips(indicies.data(), indexCount, &primitiveGroup, &numprims, true);

	if (originalStripRet && numprims == 1)
	{
		PrimitiveGroup& primitiveItem = primitiveGroup[0];
		PrimType trianglesType = primitiveItem.type;
		unsigned stripIndexCount = primitiveItem.numIndices;

		if (trianglesType == 1 && stripIndexCount <= indexCount)
		{
			unsigned short* stripIndices = primitiveItem.indices;
			for (unsigned i = 0; i < stripIndexCount; ++i)
			{
				out.push_back(stripIndices[i]);
			}

			ret = TRUE;
		}
	}

	delete[] primitiveGroup;

	return ret;
}

std::vector<UMESubMesh> const& UMEMesh::getSubMeshs() const
{
	return mSubMeshs;
}

std::map<int,std::vector<int>> const& UMEMesh::getMatIDPool() const
{
	return mMatIDPool;
}

usize UMEMesh::getFaceCount() const
{
	return mFaceCount;
}

usize UMEMesh::getOriginalVertexCount() const
{
	return mOriginalVertexCount;
}

UMEVector3 UMEMesh::getOriginalVertexAtIndex(uint16 vertexIndex) const
{
	assert(vertexIndex >= 0 && vertexIndex < mOriginalVertexCount);

	return mOriginalVertices[vertexIndex];
}

UMEVector4 UMEMesh::getOriginalTangentAtIndex(uint16 vertexIndex) const
{
	assert(vertexIndex >= 0 && vertexIndex < mOriginalVertexCount);

	return mOriginalTangents[vertexIndex];
}

UMEVector3 UMEMesh::getOriginalBitangentAtIndex(uint16 vertexIndex) const
{
	assert(vertexIndex >= 0 && vertexIndex < mOriginalVertexCount);

	return mOriginalBitangents[vertexIndex];
}

UMEVector3 UMEMesh::getOriginalNormalAtIndex(uint16 vertexIndex) const
{
	assert(vertexIndex >= 0 && vertexIndex < mOriginalVertexCount);

	return mOriginalNormals[vertexIndex];
}

UMEVector2 UMEMesh::getOriginalUVAtIndex(uint16 uvIndex) const
{
	assert(uvIndex >= 0 && uvIndex < mOriginalUVCount);

	return mOriginalUVs[uvIndex];
}