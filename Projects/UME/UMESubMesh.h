#pragma once

#include <vector>
#include <string>
#include "UMEVector.h"

class UMESubMesh
{
public:
	UMESubMesh() :
		mMatID(0xFFFF),
		mFaceCount(0),
		mVertexCount(0),
		mTrianglesMode("List")
	{
	}

	uint16 mMatID;
	std::string mMaterialName;

	usize mFaceCount;
	uint16 mVertexCount;

	std::vector<uint16> mListIndices;
	std::vector<uint16> mStripIndices;

	std::string mTrianglesMode;
};