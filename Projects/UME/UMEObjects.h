#pragma once

#include <vector>
#include <map>
#include <string>
#include "UMEVector.h"
#include "UMEFace.h"
#include "UMEMaterial.h"
#include "UMESubMesh.h"

//要进行整理
class Impl;

class UMEStatistics
{
public:
	UMEStatistics() :
		mMaxVersion("2016-64"),
		mAuthor("UNG"),
		mMeshCount(0)
	{
	}

	std::string mMaxVersion;
	std::string mAuthor;
	usize mMeshCount;
};

class UMEMesh
{
public:
	UMEMesh();

	void build(Impl* impl,INode* node,Mesh* mesh,int coordSystem);

private:
	void setImpl(Impl* impl);

	//设置持有当前mesh的node
	void setNode(INode* node);

	//设置持有真正网格数据的mesh
	void setMesh(Mesh* mesh);

	//设置导出至OpenGL或Direct3D
	void setCoordSystem(int value);

	//获取第一帧时用于把node从对象空间变换到世界空间的矩阵
	void buildNodeTransformAtFirstFrame();

	//判断一个face上的三个顶点的index order
	void buildIndexOrderOfFace();

	//获取原始顶点
	void getOriginalVertices();

	//原始UV
	void getOriginalUVs();

	//初始化UMEFace的数据:Face
	void buildUMEFaceFace();

	//初始化UMEFace的数据:UVFace
	void buildUMEFaceUVFace();

	//获取所有真正三角形的材质ID，存储到mMatIDPool
	void collectMatID();

	//填充UMEFace的数据:face的三个顶点在原始顶点容器mOriginalVertices中的位置
	void buildUMEFaceOI();

	//填充UMEFace的数据:face的matID
	void buildUMEFaceMatID();

	//填充UMEFace的数据:face的SGID
	void buildUMEFaceSGID();

	//填充UMEFace的数据:face的area
	void buildUMEFaceArea();

	//填充UMEFace的数据:face的angle
	void buildUMEFaceAngle();

	//填充UMEFace的数据:face的三个顶点纹理坐标在原始顶点纹理容器mOriginalUVs中的位置
	void buildUMEFaceTI();

	//填充UMEFace的数据:face的normal
	void buildUMEFaceNor();

	//填充UMEFace的数据:face的tangent和bitangent
	void buildUMEFaceTanAndBitan();

	void duplicateOriginalVerticesBasedOnSG();

	//依据UV不同来分裂顶点
	void duplicateOriginalVerticesBasedOnUV();

	//让UV容器中的数据和分裂后的顶点容器中的数据一一对应
	void CorrespondingUVToVertex();

	//使顶点适应坐标系
	void adaptCoordinateSystem();

	//填充原始顶点的normal,tangent,bitangent
	void buildOriginalTBNs();

	//填充batch
	void buildSubMeshs();

	//build当前mesh为strip
	void tryStrip();

private:
	void originalVerticesToOpenGL();
	void originalVerticesToDirect3D();

	//判断建模时是否使用了镜像
	BOOL isNegativeScale(Matrix3& mat);

	//遍历mUMEFaces的helper
	typedef std::vector<UMEFace>::iterator umefaces_iterator;
	umefaces_iterator getUMEFacesBegin();
	umefaces_iterator getUMEFacesEnd();

	//判断图元是不是一个三角形
	BOOL isTriangle(Face const& face);

	//边:fromVertexIndex - toVertexIndex，向量指向toVertexIndex。参数2和3取值范围为:0,1,2来表示一个face中的顶点的索引
	UMEVector3 getUMEFaceEdge(UMEFace umeFace,uint16 fromVertexIndex,uint16 toVertexIndex) const;

	//uv边:fromVertexIndex - toVertexIndex，向量指向toVertexIndex。参数2和3取值范围为:0,1,2来表示一个face中的顶点的索引
	UMEVector2 getUMEFaceUVEdge(UMEFace umeFace,uint16 fromVertexIndex,uint16 toVertexIndex) const;

	//两个面是否共享某个顶点，参数3取值范围为:0,1,2来表示一个face中的顶点的索引，返回值中的索引标明faceB中的哪个顶点和face A中的vertexIndex为同一个顶点
	std::pair<BOOL,uint16> isTwoFacesShareVertex(UMEFace const& A,UMEFace const& B,uint16 vertexIndex);

	//根据光滑组，来对顶点进行分组
	void groupingVerticesBasedOnSG();

	//根据uvPos，来对顶点进行分组
	void groupingVerticesBasedOnUV();

	//分裂原始顶点
	void duplicateOriginalVerticesImpl();

	//计算顶点的TBNs
	void buildOriginalTBNsImpl();

	//Gram-Schmidt正交化
	void applyGramSchmidt(UMEVector3& alpha1,UMEVector3& alpha2,UMEVector3& alpha3);

	//把传入的索引容器中的索引给逆序，以使用Direct3D
	void indicesToDirect3D(std::vector<uint16>& indicesVector);

	//填充子网格的材质ID和材质名称
	void buildSubMeshMaterial();

	//统计子网格的顶点数量
	void buildSubMeshVertexCount();

	//填充子网格的索引
	void buildSubMeshFace();

	BOOL doStrip(std::vector<uint16> const& indicies,std::vector<uint16>& out);

public:
	//get mSubMeshs
	std::vector<UMESubMesh> const& getSubMeshs() const;

	//get mMatIDPool
	std::map<int,std::vector<int>> const& getMatIDPool() const;

	usize getFaceCount() const;

	usize getOriginalVertexCount() const;

	//获取给定原始索引所对应的原始顶点数据
	UMEVector3 getOriginalVertexAtIndex(uint16 vertexIndex) const;
	UMEVector4 getOriginalTangentAtIndex(uint16 vertexIndex) const;
	UMEVector3 getOriginalBitangentAtIndex(uint16 vertexIndex) const;
	UMEVector3 getOriginalNormalAtIndex(uint16 vertexIndex) const;

	//获取给定原始UV索引所对应的原始顶点UV数据
	UMEVector2 getOriginalUVAtIndex(uint16 uvIndex) const;

private:
	Impl* mImpl;
	INode* mNode;
	Mesh* mMesh;
	int mCoordSystem;

	//键值为材质ID，second为一个容器，用于存储材质ID为first的所有face在mesh中的index
	std::map<int,std::vector<int>> mMatIDPool;
	usize mMatIDCount;

	//把顶点变换到世界空间的矩阵(0时刻)
	Matrix3 mTransform;
	//face中三个顶点的索引
	int mIndexOrder[3];

	//原始顶点的数量
	usize mOriginalVertexCount;
	//原始UV的数量
	usize mOriginalUVCount;
	//face的数量
	usize mFaceCount;

	//原始顶点
	std::vector<UMEVector3> mOriginalVertices;
	//与原始顶点相匹配的tangent,bitangnt,normal
	std::vector<UMEVector4> mOriginalTangents;
	std::vector<UMEVector3> mOriginalBitangents;
	std::vector<UMEVector3> mOriginalNormals;
	//与original顶点容器一一对应的uv容器
	std::vector<UMEVector2> mOriginalUVs;

	//依据光滑组来分裂顶点
	struct BookValue
	{
		BookValue() :
			mVertexPos(-1)
		{
		}

		//在原始顶点容器中的位置
		int mVertexPos;
		//DWORD，光滑组或者uvPos,std::vector<int>int为UMEFace在mUMEFaces中的位置
		std::map<DWORD,std::vector<int>> mDirtyFaces;
	};
	std::vector<BookValue> mBooks;

	//处理阶段真正存储数据的地方
	std::vector<UMEFace> mUMEFaces;

	//batch
	std::vector<UMESubMesh> mSubMeshs;
};