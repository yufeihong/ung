#pragma once

#include "UMEVector.h"
#include <vector>
#include <map>
#include <string>

class INode;
class Mtl;
class Texmap;

class UMEMaterial
{
public:
	UMEMaterial() :
		mMtl(NULL),
		mShininess(0.0),
		mShininessStrength(0.0),
		mTransparency(0.0)
	{
	}

	UMEMaterial(std::string const& name,Mtl* mtl) :
		mMtl(mtl),
		mName(name),
		mShininess(0.0),
		mShininessStrength(0.0),
		mTransparency(0.0),
		mBlendMode("replace"),
		mTwoSide(FALSE)
	{
	}

	//真正的材质
	Mtl* mMtl;

	//材质名字
	std::string mName;

	//ambient color
	UMEVector3 mAmbient;
	//diffuse color
	UMEVector3 mDiffuse;
	//specular color
	UMEVector3 mSpecular;
	//shininess value
	float mShininess;
	//shininess strength
	float mShininessStrength;
	//transparency value
	float mTransparency;
	std::string mBlendMode;
	BOOL mTwoSide;
	//纹理名字
	std::vector<std::string> mTextures;
};