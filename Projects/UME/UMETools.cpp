#include "UMETools.h"
#include "UMEVector.h"
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

/*
The difference between 1 and the smallest value greater than 1 for float objects is: 1.19209e-007
The difference between 1 and the smallest value greater than 1 for double objects is: 2.22045e-016
The difference between 1 and the smallest value greater than 1 for long double objects is: 2.22045e-016
*/
float zeroFloat = 1.0e-5;

std::string wstring2string(std::wstring sToMatch)
{
	std::string sResult;
	//计算转换后字符串的长度(包含字符串结束符)
	int iLen = WideCharToMultiByte(CP_ACP, NULL, sToMatch.c_str(), -1, NULL, 0, NULL, FALSE);
	char* lpsz = new char[iLen];
	WideCharToMultiByte(CP_OEMCP, NULL, sToMatch.c_str(), -1, lpsz, iLen, NULL, FALSE);
	sResult.assign(lpsz, iLen - 1);
	delete []lpsz;

	return sResult;
}

std::string getFileName(std::string fileFullName)
{
	char name[128];
	char ext[16];

	_splitpath(fileFullName.c_str(), NULL, NULL, name, ext);

	std::string ret(name);
	ret += ext;

	return ret;
}

void correctPerpendicular(UMEVector3& v1,UMEVector3 const& v2)
{
	assert(isEqual(v2.length(),1.0));

	UMEVector3 v(v1);

	//v1在v2上的投影的长度
	float len = v.dotProduct(v2);
	UMEVector3 proj = UMEVector3(v2.x * len,v2.y * len,v2.z * len);

	v1 = (v - proj).normalize();

	assert(isPerpendicular(v1,v2));
}

bool isPerpendicular(UMEVector3 const& v1,UMEVector3 const& v2)
{
	float dotRet = v1.dotProduct(v2);

	return isEqual(dotRet,0.0);
}

bool isEqual(float value1,float value2)
{
	float absDiff = fabs(value1 - value2);

	bool ret = absDiff < zeroFloat;

	return ret;
}

bool isEqual(UMEVector2 const& value1,UMEVector2 const& value2)
{
	float xDiff = fabs(value1.x - value2.x);
	float yDiff = fabs(value1.y - value2.y);

	bool ret = xDiff < zeroFloat && yDiff < zeroFloat;

	return ret;
}