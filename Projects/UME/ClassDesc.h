#pragma once

#include "UME.h"

class UMExportClassDesc : public ClassDesc2 
{
public:
	virtual int IsPublic()
	{
		return TRUE;
	}

	virtual void* Create(BOOL /*loading = FALSE*/)
	{
		return new UMExport();
	}

	virtual const TCHAR* ClassName()
	{
		return GetString(IDS_CLASS_NAME);
	}

	virtual SClass_ID SuperClassID()
	{
		return SCENE_EXPORT_CLASS_ID;
	}

	virtual Class_ID ClassID()
	{
		return UMExport_CLASS_ID;
	}

	virtual const TCHAR* Category()
	{
		return GetString(IDS_CATEGORY);
	}

	virtual const TCHAR* InternalName()
	{
		return _T("UMExport");
	}

	virtual HINSTANCE HInstance()
	{
		return hInstance;
	}
};