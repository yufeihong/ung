#pragma once

#include "UMEVector.h"
#include <mesh.h>

class UMEFace
{
public:
	UMEFace()
	{
		for (int i = 0; i < 3; i++)
		{
			mOI[i] = 0xFFFF;
			mAngle[i] = -1.0f;
		}

		mMatID = 0xFFFF;
		mSGID = 0xFFFFFFFF;

		mArea = -1.0f;
	}

	//Face
	Face mFace;

	//UVFace
	TVFace mUVFace;

	//original indices
	uint16 mOI[3];

	//material id
	uint16 mMatID;

	//smooth group id
	DWORD mSGID;

	//face area
	float mArea;
	//radians or degrees,the choice of units does not matter.
	float mAngle[3];

	//unique uv indices
	uint16 mTI[3];

	//face normal
	UMEVector3 mNor;
	//face tangent
	UMEVector3 mTan;
	//face bitangent
	UMEVector3 mBitan;
};