#include "UD9Program.h"

#ifdef UD9_HIERARCHICAL_COMPILE

namespace ung
{
	D9Program::D9Program()
	{
	}

	D9Program::~D9Program()
	{
	}

	void D9Program::attach(Shader * shader)
	{
	}

	void D9Program::detach(Shader * shader)
	{
	}

	void D9Program::detachAll()
	{
	}

	void D9Program::link()
	{
	}

	void D9Program::use()
	{
	}
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE