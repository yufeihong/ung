#include "UD9Adapter.h"

#ifdef UD9_HIERARCHICAL_COMPILE

#include "UD9RenderSystem.h"
#include "UD9DisplayMode.h"

namespace ung
{
	D9Adapter::D9Adapter() :
		mDisplayModeCollection(nullptr)
	{
		/*
		PVOID SecureZeroMemory(PVOID ptr,SIZE_T cnt);
		Fills a block of memory with zeros. It is designed to be a more secure version of ZeroMemory.
		ptr:A pointer to the starting address of the block of memory to fill with zeros.
		cnt:The size of the block of memory to fill with zeros,in bytes.
		Return value:
		This function returns a pointer to the block of memory.
		Use this function instead of ZeroMemory when you want to ensure that your data will be 
		overwritten promptly(迅速地，立即地，毫不迟疑地),as some C++ compilers can optimize a 
		call to ZeroMemory by removing it entirely.
		*/
		SecureZeroMemory(&mAdapterIdentifier,sizeof(mAdapterIdentifier));
		SecureZeroMemory(&mDesktopDisplayMode,sizeof(mDesktopDisplayMode));
		SecureZeroMemory(&mCaps,sizeof(mCaps));
	}

	D9Adapter::D9Adapter(uint32 ordinalNumber, D3DADAPTER_IDENTIFIER9 const & identifier,
		D3DDISPLAYMODE const& desktopDisplayMode,D3DCAPS9 const& caps) :
		mAdapterOrdinalNumber(ordinalNumber),
		mAdapterIdentifier(identifier),
		mDesktopDisplayMode(desktopDisplayMode),
		mCaps(caps),
		mDisplayModeCollection(nullptr)
	{
	}

	D9Adapter::~D9Adapter()
	{
		UNG_DEL(mDisplayModeCollection);
	}

	String D9Adapter::getName() const
	{
		return String(mAdapterIdentifier.Driver);
	}

	String D9Adapter::getDescription() const
	{
		std::ostringstream iss;
		iss << "Adapter:" << mAdapterOrdinalNumber << "-" << mAdapterIdentifier.Description;

		return String(iss.str());
	}

	uint32 D9Adapter::getOridinalNumber() const
	{
		return mAdapterOrdinalNumber;
	}

	D3DADAPTER_IDENTIFIER9 const & D9Adapter::getIdentifier() const
	{
		return mAdapterIdentifier;
	}

	D3DDISPLAYMODE const & D9Adapter::getDesktopDisplayMode() const
	{
		return mDesktopDisplayMode;
	}

	D3DCAPS9 const & D9Adapter::getCaps() const
	{
		return mCaps;
	}

	uint32 D9Adapter::getDisplayModeCount(D3DFORMAT fm) const
	{
		auto pD3D = D9RenderSystem::getIDirect3D9();

		/*
		UINT GetAdapterModeCount(UINT Adapter,D3DFORMAT Format);
		Returns the number of display modes available on this adapter.
		Adapter:
		Type:UINT
		Ordinal number that denotes the display adapter.D3DADAPTER_DEFAULT is always the 
		primary display adapter.
		Format:
		Type:D3DFORMAT
		Identifies the format of the surface type using D3DFORMAT.Use EnumAdapterModes to 
		see the valid formats.
		*/
		return pD3D->GetAdapterModeCount(mAdapterOrdinalNumber,fm);
	}

	void D9Adapter::getD3DDisplayMode(D3DFORMAT fm,uint32 modeIndex, D3DDISPLAYMODE* pOutMode)
	{
		auto pD3D = D9RenderSystem::getIDirect3D9();

		/*
		Direct3D渲染设备总是支持渲染图形到函数EnumAdapterModes()所枚举出来的显示模式中指定格式
		的表面。如果需要渲染图形到其它格式的表面，需要调用函数IDirect3D9::CheckDeviceFormat()来
		检查当前设备是否支持。

		HRESULT EnumAdapterModes(UINT Adapter,D3DFORMAT Format,UINT Mode,D3DDISPLAYMODE *pMode);
		Queries the device to determine whether the specified adapter supports the requested 
		format and display mode.This method could be used in a loop to enumerate all the 
		available adapter modes.
		Adapter:
		Ordinal number denoting the display adapter to enumerate. D3DADAPTER_DEFAULT is 
		always the primary display adapter. This method returns D3DERR_INVALIDCALL when this 
		value equals or exceeds the number of display adapters in the system.
		Format:
		Allowable pixel formats. See Remarks.
		Mode:
		Represents the display-mode index which is an unsigned integer between zero and the 
		value returned by GetAdapterModeCount minus one.
		pMode:
		A pointer to the available display mode of type D3DDISPLAYMODE.
		If the device can be used on this adapter, D3D_OK is returned.
		If the Adapter equals or exceeds the number of display adapters in the system,
		D3DERR_INVALIDCALL is returned.
		If either surface format is not supported or if hardware acceleration is not available for the 
		specified formats, D3DERR_NOTAVAILABLE is returned.
		The application specifies a format and the enumeration is restricted to those display 
		modes that exactly match the format (alpha is ignored). 
		Allowed formats (which are members of D3DFORMAT) are as follows:
		D3DFMT_A1R5G5B5
		D3DFMT_A2R10G10B10
		D3DFMT_A8R8G8B8
		D3DFMT_R5G6B5
		D3DFMT_X1R5G5B5
		D3DFMT_X8R8G8B8
		In addition, EnumAdapterModes treats pixel formats 565 and 555 as equivalent, and 
		returns the correct version. The difference comes into play only when the application locks 
		the back buffer and there is an explicit flag that the application must set in order to 
		accomplish this.
		*/
		pD3D->EnumAdapterModes(mAdapterOrdinalNumber,fm,modeIndex,pOutMode);
	}

	D9DisplayModeCollection* D9Adapter::getDisplayModeCollection()
	{
		if (!mDisplayModeCollection)
		{
			mDisplayModeCollection = UNG_NEW D9DisplayModeCollection(this);

			//enumerates the supported display modes.
			mDisplayModeCollection->enumerateDisplayMode();
		}

		return mDisplayModeCollection;
	}
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE

/*
typedef struct D3DADAPTER_IDENTIFIER9
{
	char							Driver[MAX_DEVICE_IDENTIFIER_STRING];
	char							Description[MAX_DEVICE_IDENTIFIER_STRING];
	char							DeviceName[32];
	LARGE_INTEGER		DriverVersion;
	DWORD						DriverVersionLowPart;
	DWORD						DriverVersionHighPart;
	DWORD						VendorId;
	DWORD						DeviceId;
	DWORD						SubSysId;
	DWORD						Revision;
	GUID							DeviceIdentifier;
	DWORD						WHQLLevel;
}D3DADAPTER_IDENTIFIER9,*LPD3DADAPTER_IDENTIFIER9;
Contains information identifying the adapter.
Driver:
Type:char
Used for presentation to the user. This should not be used to identify particular drivers, because 
many different strings might be associated with the same device and driver from different 
vendors.
Description:
Type:char
Used for presentation to the user.
DeviceName:
Type:char
Device name for GDI.
DriverVersion:
Type:LARGE_INTEGER
Identify the version of the Direct3D driver. It is legal to do less than and greater than 
comparisons on the 64-bit signed integer value. However, exercise caution if you use this 
element to identify problematic drivers. Instead, you should use DeviceIdentifier.
VendorId:
Type:DWORD
Can be used to help identify a particular chip set. Query this member to identify the 
manufacturer. The value can be zero if unknown.
DeviceId:
Type:DWORD
Can be used to help identify a particular chip set. Query this member to identify the type of 
chip set. The value can be zero if unknown.
SubSysId:
Type:DWORD
Can be used to help identify a particular chip set. Query this member to identify the subsystem,
typically the particular board. The value can be zero if unknown.
Revision:
Type:DWORD
Can be used to help identify a particular chip set. Query this member to identify the revision 
level of the chip set. The value can be zero if unknown.
DeviceIdentifier:
Type:GUID
Can be queried to check changes in the driver and chip set. This GUID is a unique identifier for 
the driver and chip set pair. Query this member to track changes to the driver and chip set in 
order to generate a new profile for the graphics subsystem. DeviceIdentifier can also be used to 
identify particular problematic drivers.
WHQLLevel:
Type:DWORD
Used to determine the Windows Hardware Quality Labs (WHQL) validation level for this driver 
and device pair.The DWORD is a packed date structure defining the date of the release of the 
most recent WHQL test passed by the driver.It is legal to perform < and > operations on this 
value.The following illustrates the date format:
Bits
31-16		The year, a decimal number from 1999 upwards.
15-8			The month, a decimal number from 1 to 12.
7-0			The day, a decimal number from 1 to 31.
The following values are also used:
0				Not certified.
1				WHQL validated, but no date information is available.
Differences between Direct3D 9 and Direct3D 9Ex:
For Direct3D9Ex running on Windows Vista, Windows Server 2008, Windows 7, and Windows 
Server 2008 R2 (or more current operating system), IDirect3D9::GetAdapterIdentifier returns 1 
for the WHQL level without checking the status of the driver.
The following pseudocode example illustrates the version format encoded in the DriverVersion,
DriverVersionLowPart,and DriverVersionHighPart members:
Product = HIWORD(DriverVersion.HighPart)
Version = LOWORD(DriverVersion.HighPart)
SubVersion = HIWORD(DriverVersion.LowPart)
Build = LOWORD(DriverVersion.LowPart)
MAX_DEVICE_IDENTIFIER_STRING is a constant with the following definition:
#define MAX_DEVICE_IDENTIFIER_STRING 512
The VendorId,DeviceId,SubSysId,and Revision members can be used in tandem to identify 
particular chip sets.However,use these members with caution.
*/