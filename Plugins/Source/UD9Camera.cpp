#include "UD9Camera.h"

#ifdef UD9_HIERARCHICAL_COMPILE

#include "UD9Viewport.h"

namespace ung
{
	D9Camera::D9Camera(String const& name,Vector3 const & pos, Vector3 const & lookat, real_type aspect, Radian fovY, real_type nDis, real_type fDis) :
		Camera(name,pos,lookat,aspect,fovY,nDis,fDis)
	{
	}

	D9Camera::~D9Camera()
	{
	}

	const Matrix4& D9Camera::getProjectionMatrix() const
	{
		return mFrustum.getProjectionMatrixD3D();
	}

	AABB D9Camera::getClipSpaceAABB() const
	{
		return AABB(Vector3(-1.0,-1.0,0.0),Vector3(1.0,1.0,1.0));
	}

	Viewport* D9Camera::createViewport(uint32 left, uint32 top, uint32 width, uint32 height, int32 zOrder)
	{
		mViewport = UNG_NEW D9Viewport(left, top, width, height, zOrder);

		//让视口和当前摄像机进行关联
		mViewport->attachCamera(this);

		return mViewport;
	}
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE