#include "UD9VertexBuffer.h"

#ifdef UD9_HIERARCHICAL_COMPILE

#include "UD9RenderSystem.h"
#include "UD9Device.h"
#include "UD9Utilities.h"

namespace
{
	using namespace ung;

	/*!
	 * \remarks 为顶点缓冲区选择合适的usage和pool
	 * \return 
	 * \param uint32& usage
	 * \param ResourcePoolType & pool
	*/
	void chooseUsageAndPoolOfVertexBuffer(uint32& usage,ResourcePoolType& pool)
	{
		using type = IVideoBuffer::Usage;

		auto STATIC = static_cast<uint32>(type::VB_USAGE_STATIC);
		auto DYNAMIC = static_cast<uint32>(type::VB_USAGE_DYNAMIC);
		auto WRITE_ONLY = static_cast<uint32>(type::VB_USAGE_WRITE_ONLY);

#define HAVE_STATIC						ungEnumCombinationHave(usage, STATIC)
#define HAVE_DYNAMIC					ungEnumCombinationHave(usage, DYNAMIC)
#define HAVE_WRITE_ONLY			ungEnumCombinationHave(usage,WRITE_ONLY)

		/*
		如果没有STATIC，也没有DYNAMIC，那么使其拥有STATIC。
		*/
		if (!HAVE_STATIC && !HAVE_DYNAMIC)
		{
			ungEnumCombinationJoin(usage, STATIC);
		}

		/*
		如果同时拥有STATIC和DYNAMIC的话，那么选择DYNAMIC，移除掉STATIC
		*/
		if (HAVE_STATIC && HAVE_DYNAMIC)
		{
			ungEnumCombinationRemove(usage, STATIC);
		}

		/*
		如果想要读取buffer的话，那么说明系统内存中有一个镜像，这个时候buffer应该拥有WRITE_ONLY，
		因为此时不可能会从buffer进行读取了。
		如果不想读取buffer的话，那么buffer应该是只可写的，应该拥有WRITE_ONLY。
		综上所述，无论是否想要进行读取，buffer都应该是只可写的，应该拥有WRITE_ONLY。
		*/
		if (!HAVE_WRITE_ONLY)
		{
			ungEnumCombinationJoin(usage, WRITE_ONLY);
		}

		/*
		如果没有使用VB_USAGE_DONOTCLIP标记，那么得到的是一个a clipping-capable vertex buffer。
		Set to indicate that the vertex buffer content will never require clipping. When rendering 
		with buffers that have this flag set, the D3DRS_CLIPPING render state must be set to false.
		(如果使用了VB_USAGE_DONOTCLIP标记，那么渲染状态里面的D3DRS_CLIPPING状态必须设置为
		false)
		*/

		//---------------------------------------------------------------------------

		pool = ResourcePoolType::RPT_DEFAULT;

		/*
		Applications should use RPT_MANAGED for most static resources
		*/
		if (HAVE_STATIC)
		{
			/*
			RPT_MANAGED会抹杀掉WRITE_ONLY所带来的效率提升。
			*/
			pool = ResourcePoolType::RPT_MANAGED;
		}
	}
}//namespace

namespace ung
{
	D9VertexBuffer::D9VertexBuffer() :
		mVB(nullptr)
	{
	}

	D9VertexBuffer::D9VertexBuffer(uint32 vertexSize, uint32 numVertices, uint32 usage, bool useShadowBuffer) :
		VertexBuffer(vertexSize,numVertices,usage,useShadowBuffer)
	{
		auto pRS = D9RenderSystem::getInstance();

		auto pActiveDevice = pRS->getActiveDevice();

		ResourcePoolType pool;
		chooseUsageAndPoolOfVertexBuffer(mUsage, pool);

		/*
		HRESULT IDirect3DDevice9::CreateVertexBuffer(UINT Length,DWORD Usage,DWORD FVF,
		D3DPOOL PoolIDirectSDVertexBuffer9** ppVertexBuffer,HANDLE* pSharedHandle);
		Length:
		Size of the vertex buffer,in bytes.For FVF vertex buffers,Length must be large enough to 
		contain at least one vertex,but it need not be a multiple of the vertex size.Length is not 
		validated for non-FVF buffers.
		Usage:
		Usage can be 0,which indicates no usage value.However,if usage is desired,use a 
		combination of one or more D3DUSAGE constants.It is good practice to match the usage 
		parameter in CreateVertexBuffer with the behavior flags in IDirect3D9::CreateDevice.
		FVF:
		Combination of D3DFVF,a usage specifier that describes the vertex format of the vertices 
		in this buffer.If this parameter is set to a valid FVF code,the created vertex buffer is an 
		FVF vertex buffer.Otherwise,if this parameter is set to zero,the vertex buffer is a non-FVF 
		vertex buffer.
		Pool:
		Member of the D3DPOOL enumerated type,describing a valid memory class into which to 
		place the resource.Do not set to D3DPOOL_SCRATCH.
		In general,specify D3DPOOL_DEFAULT for dynamic buffers and D3DPOOL_MANAGED for 
		static buffers.
		ppVertexBuffer:
		Address of a pointer to an IDirect3DVertexBuffer9 interface,representing the created 
		vertex buffer resource.
		pSharedHandle:
		Reserved.Set this parameter to NULL.This parameter can be used in Direct3D 9 for 
		Windows Vista to share resources.
		If the method succeeds,the return value is D3D_OK.If the method fails,the return value 
		can be one of the following:D3DERR_INVALIDCALL,D3DERR_OUTOFVIDEOMEMORY,
		E_OUTOFMEMORY.
		*/
		auto ret = pActiveDevice->CreateVertexBuffer(vertexSize * numVertices,
			ud9ToD3D9Usage(mUsage), 0, ud9ToD3D9ResourcePool(pool), &mVB, NULL);

		BOOST_ASSERT(ret == D3D_OK);
	}

	D9VertexBuffer::~D9VertexBuffer()
	{
		BOOST_ASSERT(D9RenderSystem::getD9VertexBufferManager());

		UD9_RELEASE(mVB);
	}

	void D9VertexBuffer::writeData(uint32 offset, uint32 length, const void * pSource, bool discardWholeBuffer)
	{
		void* pDst = lock(offset,length,
			discardWholeBuffer ? LockOptions::VB_LOCK_DISCARD : LockOptions::VB_LOCK_NORMAL);
		
		memcpy(pDst, pSource, length);

		unlock();
	}

	void D9VertexBuffer::getDesc(D3DVERTEXBUFFER_DESC & desc)
	{
		BOOST_ASSERT(mVB);

		mVB->GetDesc(&desc);
	}

	void* D9VertexBuffer::lockImpl(uint32 offset, uint32 length, LockOptions options)
	{
		BOOST_ASSERT(mVB);

		void* pData{ nullptr };

		/*
		Locks a range of vertex data and obtains a pointer to the vertex buffer memory.
		HRESULT Lock(UINT OffsetToLock,UINT SizeToLock,VOID** ppbData,DWORD Flags);
		OffsetToLock:
		Offset into the vertex data to lock,in bytes.To lock the entire vertex buffer,specify 0 for 
		both parameters,SizeToLock and OffsetToLock.
		SizeToLock:
		Size of the vertex data to lock,in bytes.To lock the entire vertex buffer,specify 0 for both 
		parameters,SizeToLock and OffsetToLock.
		ppbData:
		VOID* pointer to a memory buffer containing the returned vertex data.
		Flags:
		Combination of zero or more locking flags that describe the type of lock to perform.For 
		this method,the valid flags are:
		D3DLOCK_DISCARD
		D3DLOCK_NO_DIRTY_UPDATE
		D3DLOCK_NOSYSLOCK
		D3DLOCK_READONLY
		D3DLOCK_NOOVERWRITE
		If the method succeeds,the return value is D3D_OK.If the method fails, the return value 
		can be D3DERR_INVALIDCALL.
		As a general rule,do not hold a lock across more than one frame.When working with 
		vertex buffers,you are allowed to make multiple lock calls;however,you must ensure that 
		the number of lock calls match the number of unlock calls.
		*/
		auto ret = mVB->Lock(offset,length,&pData,options);

		BOOST_ASSERT(ret == D3D_OK);
		BOOST_ASSERT(pData);

		return pData;
	}

	void D9VertexBuffer::unlockImpl()
	{
		BOOST_ASSERT(mVB);

		auto ret = mVB->Unlock();

		BOOST_ASSERT(ret == D3D_OK);
	}
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE

/*
vertex and index buffers are the Direct3D resources for storing geometry data,which can be 
placed in video memory.

Resource management is not recommended for resources which change with high frequency.
For example,vertex and index buffers which are used to traverse a scene graph every frame 
rendering only geometry visible to the user changes every frame.Since managed resources are 
backed by system memory,the constant changes must be updated in system memory,which 
can cause a severe degradation in performance.For this particular scenario,D3DPOOL_DEFAULT 
along with D3DUSAGE_DYNAMIC should be used.
*/

/*
Vertex Buffers(Direct3D 9):
Vertex buffers,represented by the IDirect3DVertexBuffer9 interface,are memory buffers that 
contain vertex data.Vertex buffers can contain any vertex type - transformed or untransformed,
lit or unlit - that can be rendered through the use of the rendering methods in the 
IDirect3DDevice9 interface.You can process the vertices in a vertex buffer to perform 
operations such as transformation,lighting,or generating clipping flags.Transformation is always 
performed.
The flexibility of vertex buffers make them ideal staging points for reusing transformed 
geometry.You could create a single vertex buffer,transform,light,and clip the vertices in it,and 
render the model in the scene as many times as needed without re-transforming it,even with 
interleaved render state changes.This is useful when rendering models that use multiple 
textures:the geometry is transformed only once,and then portions of it can be rendered as 
needed,interleaved with the required texture changes.Render state changes made after vertices 
are processed take effect the next time the vertices are processed.
Description:
A vertex buffer is described in terms of its capabilities:if it can exist only in system memory,if it 
is only used for write operations,and the type and number of vertices it can contain.All these 
traits are held in a D3DVERTEXBUFFER_DESC structure.
The Format member is set to D3DFMT_VERTEXDATA to indicate that this is a vertex buffer.The 
Type identifies the resource type of the vertex buffer.The Usage structure member contains 
general capability flags.The D3DUSAGE_SOFTWAREPROCESSING flag indicates that the vertex 
buffer is to be used with software vertex processing.The presence of the 
D3DUSAGE_WRITEONLY flag in Usage indicates that the vertex buffer memory is used only for 
write operations.This frees the driver to place the vertex data in the best memory location to 
enable fast processing and rendering.If the D3DUSAGE_WRITEONLY flag is not used,the driver 
is less likely(是不太可能的) to put the data in a location that is inefficient for read operations.This 
sacrifices(牺牲) some processing and rendering speed.If this flag is not specified,it is assumed 
that applications perform read and write operations on the data within the vertex buffer.
Pool specifies the memory class that is allocated for the vertex buffer.The 
D3DPOOL_SYSTEMMEM flag indicates that the system created the vertex buffer in system 
memory.
The Size member stores the size, in bytes, of the vertex buffer data.The FVF member contains 
a combination of D3DFVF that identify the type of vertices that the buffer contains.
Memory Pool and Usage:
You can create vertex buffers with the IDirect3DDevice9::CreateVertexBuffer method,which 
takes pool (memory class) and usage parameters.IDirect3DDevice9::CreateVertexBuffer can 
also be created with a specified FVF code for use in fixed function vertex processing, or as the 
output of process vertices.
The D3DUSAGE_SOFTWAREPROCESSING flag can be set when mixed-mode or software vertex 
processing 
(D3DCREATE_MIXED_VERTEXPROCESSING / D3DCREATE_SOFTWARE_VERTEXPROCESSING) is 
enabled for that device. D3DUSAGE_SOFTWAREPROCESSING must be set for buffers to be 
used with software vertex processing in mixed mode, but it should not be set for the best 
possible performance when using hardware vertex processing in mixed mode.
(D3DCREATE_HARDWARE_VERTEXPROCESSING). However, setting 
D3DUSAGE_SOFTWAREPROCESSING is the only option when a single buffer is to be used with 
both hardware and software vertex processing. D3DUSAGE_SOFTWAREPROCESSING is allowed 
for mixed as well as for software devices.
It is possible to force vertex and index buffers into system memory by specifying 
D3DPOOL_SYSTEMMEM, even when the vertex processing is done in hardware. This is a way to 
avoid overly large amounts of page-locked memory when a driver is putting these buffers into 
AGP memory.

Creating a Vertex Buffer(Direct3D 9):
You create a vertex buffer object by calling the IDirect3DDevice9::CreateVertexBuffer method,
which accepts five parameters. The first parameter specifies the vertex buffer length, in bytes.
Use the sizeof operator to determine the size of a vertex format, in bytes.Consider the following 
custom vertex format.
struct CUSTOMVERTEX
{
	FLOAT x, y, z;
	FLOAT rhw;
	DWORD color;
	FLOAT tu, tv;			//Texture coordinates
};
//Custom flexible vertex format (FVF) describing the custom vertex structure
#define D3DFVF_CUSTOMVERTEX (D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1)
To create a vertex buffer to hold four CUSTOMVERTEX structures, specify 
[4*sizeof(CUSTOMVERTEX)] for the Length parameter.
The second parameter is a set of usage controls. Among other things, its value determines 
whether the vertex buffer is capable of containing clipping information - in the form of clip 
flags - for vertices that exist outside the viewing area. To create a vertex buffer that cannot 
contain clip flags, include the D3DUSAGE_DONOTCLIP(用于标记一个不能包含clipping information的
顶点缓冲区) flag for the Usage parameter. The D3DUSAGE_DONOTCLIP(仅当你指出顶点缓冲区里面的
数据是transformed，那么可以使用该标记) flag is applied only if you also indicate that the vertex 
buffer will contain transformed vertices - the D3DFVF_XYZRHW flag is included in the FVF 
parameter. The IDirect3DDevice9::CreateVertexBuffer method ignores the 
D3DUSAGE_DONOTCLIP(如果顶点缓冲区里面的数据是untransformed，那么即使你使用了该标记，该标
记也会被忽略掉) flag if you indicate that the buffer will contain untransformed vertices (the 
D3DFVF_XYZ flag). Clipping flags occupy additional memory(使用D3DUSAGE_DONOTCLIP标记可
以略少占用memory), making a clipping-capable vertex buffer slightly larger than a vertex buffer 
incapable of containing clipping flags. Because these resources are allocated when the vertex 
buffer is created, you must request a clipping-capable vertex buffer ahead of time(提前).
The third parameter, FVF, is a combination of D3DFVF that describe the vertex format of the 
vertex buffer. If you specify 0 for this parameter, then the vertex buffer is a non-FVF vertex 
buffer.
The fourth parameter describes the memory class into which to place the vertex buffer.
The final parameter that IDirect3DDevice9::CreateVertexBuffer accepts is the address of a 
variable that will be filled with a pointer to the new IDirect3DVertexBuffer9 interface of the 
vertex buffer object, if the call succeeds.
You cannot produce clip flags for a vertex buffer that was created without support for them.
The following C++ code example shows what creating a vertex buffer might look like in code.
//d3dDevice contains the address of an IDirect3DDevice9 interface
//g_pVB is a variable of type LPDIRECT3DVERTEXBUFFER9
//The custom vertex type
struct CUSTOMVERTEX
{
	FLOAT x, y, z;
	FLOAT rhw;
	DWORD color;
	FLOAT tu, tv;			//The texture coordinates
};
#define D3DFVF_CUSTOMVERTEX (D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1)
// Create a clipping-capable vertex buffer.Allocate enough memory in the default memory pool 
//to hold three CUSTOMVERTEX structures
if( FAILED( d3dDevice->CreateVertexBuffer( 3*sizeof(CUSTOMVERTEX),
		0(Usage), D3DFVF_CUSTOMVERTEX, D3DPOOL_DEFAULT, &g_pVB, NULL ) ) )
	return E_FAIL;

Accessing the Contents of a Vertex Buffer(Direct3D 9):
Vertex buffer objects enable applications to directly access the memory allocated for vertex 
data. You can retrieve a pointer to vertex buffer memory by calling the 
IDirect3DVertexBuffer9::Lock method, and then accessing the memory as needed to fill the 
buffer with new vertex data or to read any data it already contains.

Rendering from a Vertex Buffer(Direct3D 9):
Rendering vertex data from a vertex buffer requires a few steps. First, you need to set the 
stream source by calling the IDirect3DDevice9::SetStreamSource method, as shown in the 
following example.
d3dDevice->SetStreamSource( 0, g_pVB, 0, sizeof(CUSTOMVERTEX) );
The first parameter of IDirect3DDevice9::SetStreamSource tells Direct3D the source of the 
device data stream. The second parameter is the vertex buffer to bind to the data stream. The 
third parameter is the offset from the beginning of the stream to the beginning of the vertex 
data, in bytes. The fourth parameter is the stride of the component, in bytes. In the sample 
code above, the size of a CUSTOMVERTEX is used for the stride of the component.
The next step is to inform Direct3D which vertex shader to use by calling the 
IDirect3DDevice9::SetVertexShader method. The following sample code sets an FVF code for 
the vertex shader. This informs Direct3D of the types of vertices it is dealing with.
d3dDevice->SetFVF( D3DFVF_CUSTOMVERTEX );
After setting the stream source and vertex shader, any draw methods will use the vertex buffer.
The code example below shows how to render vertices from a vertex buffer with the 
IDirect3DDevice9::DrawPrimitive method.
d3dDevice->DrawPrimitive( D3DPT_TRIANGLELIST, 0, 1 );
The second parameter that IDirect3DDevice9::DrawPrimitive accepts is the index of the first 
vector in the vertex buffer to load.

FVF Vertex Buffers(Direct3D 9):
Setting the FVF parameter of the IDirect3DDevice9::CreateVertexBuffer method to a nonzero 
value, which must be a valid FVF code, indicates that the buffer content is to be characterized 
by an FVF code. A vertex buffer that is created with an FVF code is referred to as an FVF vertex 
buffer. Some methods or uses of IDirect3DDevice9 require FVF vertex buffers, and others 
require non-FVF vertex buffers. FVF vertex buffers are required as the destination vertex buffer 
argument for IDirect3DDevice9::ProcessVertices.
FVF vertex buffers can be bound to a source data stream for any stream number.
The presence of the D3DFVF_XYZRHW component on FVF vertex buffers indicates that the 
vertices in that buffer have been processed. Vertex buffers used for 
IDirect3DDevice9::ProcessVertices destination vertex buffers must be post-processed. Vertex 
buffers used for fixed function shader inputs can be either preprocessed or postprocessed. If 
the vertex buffer is post-processed, then the shader is effectively bypassed and the data is 
passed directly to the primitive clipping and setup module.
FVF vertex buffers can be used with vertex shaders. Also, vertex streams can represent the 
same vertex formats that non-FVF vertex buffers can. They do not have to be used to input 
data from separate vertex buffers. The additional flexibility of the new vertex streams enables 
applications that need to keep their data separate to work better, but it is not required. If the 
application can maintain interleaved data in advance, then that is a performance boost. If the 
application will only interleave the data before every rendering call, then it should enable the 
API or hardware to do this with multiple streams.
The most important things with vertex performance is to use a 32-byte vertex, and to maintain 
good cache ordering.
*/

/*
Vertex Data Streams(Direct3D 9):
The rendering interfaces for Direct3D consist of methods that render primitives from vertex 
data stored in one or more data buffers. Vertex data consists of vertex elements combined to 
form vertex components. Vertex elements, the smallest unit of a vertex, represent entities such 
as position, normal, or color.
Vertex components are one or more vertex elements stored contiguously (interleaved per 
vertex) in a single memory buffer. A complete vertex consists of one or more components,
where each component is in a separate memory buffer(顶点的每一个component“对应”一个buffer).
To render a primitive, multiple vertex components are read and assembled so that complete 
vertices are available for vertex processing. The following diagram shows the process of 
rendering primitives using vertex components.
如图：Knowledge/DX/003.png所示。
Rendering primitives consists of two steps. First, set up one or more vertex component 
streams; second, invoke a IDirect3DDevice9::DrawPrimitive method to render from those 
streams. Identification of vertex elements within these component streams is specified by the 
vertex shader.
The IDirect3DDevice9::DrawPrimitive methods specify an offset in the vertex data streams so 
that an arbitrary contiguous subset of the primitives within one set of vertex data can be 
rendered with each draw invocation. This enables you to change the device rendering state 
between groups of primitives that are rendered from the same vertex buffers.

Setting the Stream Source(Direct3D 9):
The IDirect3DDevice9::SetStreamSource method binds a vertex buffer to a device data stream,
creating an association between the vertex data and one of several data stream ports that feed 
the primitive processing functions. The actual references to the stream data do not occur until 
a drawing method, such as IDirect3DDevice9::DrawPrimitive, is called.
A stream is defined as a uniform array of component data, where each component consists of 
one or more elements(每个component包含一个或多个elements) representing a single entity such 
as position, normal, color, and so on. The Stride parameter specifies the size of the component,
in bytes(stride指定了一个component的大小，单位为字节).
*/