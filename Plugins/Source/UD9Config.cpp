#include "UD9Config.h"

#ifdef UD9_HIERARCHICAL_COMPILE

#if UNG_DEBUGMODE
#pragma comment(lib,"UNG_d.lib")
#else
#pragma comment(lib,"UNG.lib")
#endif

#pragma comment(lib,"d3d9.lib")
#if UNG_DEBUGMODE
#pragma comment(lib,"d3dx9d.lib")
#else
#pragma comment(lib,"d3dx9.lib")
#endif

#endif//UD9_HIERARCHICAL_COMPILE

/*
DirectX 9.0版本中VS和PS都是2.0版本。
*/

/*
Direct3D程序基本结构：
1，创建一个窗口。
2，创建Direct3D对象，设备对象。
3，消息循环。
4，渲染。
5，清除COM对象。
*/