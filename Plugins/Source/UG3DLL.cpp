#include "UG3DLL.h"

#ifdef UG3_HIERARCHICAL_COMPILE

#include "UG3Plugin.h"
#include "UG3RenderSystem.h"

namespace ung
{
	static G3Plugin* plugin{};

	void dllCreatePlugin() noexcept
	{
		plugin = UNG_NEW G3Plugin;

		UNG_PRINT_ONE_LINE("GL3插件已经创建。");

		PluginManager::getInstance().install(plugin);
	}

	void dllDestroyPlugin()
	{
		PluginManager::getInstance().uninstall(plugin);
		UNG_DEL(plugin);

		UNG_PRINT_ONE_LINE("GL3插件已经销毁。");
	}
}//namespace ung

#endif//UG3_HIERARCHICAL_COMPILE