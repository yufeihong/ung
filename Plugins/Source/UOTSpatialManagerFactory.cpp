#include "UOTSpatialManagerFactory.h"

#ifdef UOT_HIERARCHICAL_COMPILE

#include "UOTSpatialManager.h"

namespace ung
{
	OctreeSpatialManagerFactory::OctreeSpatialManagerFactory() :
		mIdentify("OctreeSpatialManagerFactory")
	{
	}

	OctreeSpatialManagerFactory::~OctreeSpatialManagerFactory()
	{
		//确保在场景管理器中创建的具体的空间管理器已经销毁了
		BOOST_ASSERT(!Root::getInstance().getSpatialManagerEnumerator().hasSpatialManager(this));
	}

	String const & OctreeSpatialManagerFactory::getIdentity() const
	{
		return mIdentify;
	}

	SceneTypeMask OctreeSpatialManagerFactory::getMask() const
	{
		return ST_GENERIC | ST_EXTERIOR;
	}

	ISpatialManager * OctreeSpatialManagerFactory::createInstance(String const & instanceName)
	{
		return UNG_NEW OctreeSpatialManager(this,instanceName);
	}

	void OctreeSpatialManagerFactory::destroyInstance(ISpatialManager * instancePtr)
	{
		UNG_DEL(instancePtr);
	}
}//namespace ung

#endif//UOT_HIERARCHICAL_COMPILE