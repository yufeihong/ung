#include "UD9Utilities.h"

#ifdef UD9_HIERARCHICAL_COMPILE

namespace ung
{
	PixelFormat UNG_STDCALL ud9ToUniformPixelFormat(D3DFORMAT d9fm)
	{
		PixelFormat ret;

		switch (d9fm)
		{
		case D3DFMT_UNKNOWN:
		{
			ret = PixelFormat::PF_UNKNOWN;
			break;
		}
		case D3DFMT_X1R5G5B5:
		{
			ret = PixelFormat::PF_16BITS_X1R5G5B5;
			break;
		}
		case D3DFMT_R5G6B5:
		{
			ret = PixelFormat::PF_16BITS_R5G6B5;
			break;
		}
		case D3DFMT_R8G8B8:
		{
			ret = PixelFormat::PF_24BITS_R8G8B8;
			break;
		}
		case D3DFMT_X8R8G8B8:
		{
			ret = PixelFormat::PF_32BITS_X8R8G8B8;
			break;
		}
		case D3DFMT_A8R8G8B8:
		{
			ret = PixelFormat::PF_32BITS_A8R8G8B8;
			break;
		}
		case D3DFMT_A8:
		{
			ret = PixelFormat::PF_8BITS_A8;
			break;
		}
		case D3DFMT_R16F:
		{
			ret = PixelFormat::PF_16BITS_FLOAT_R16;
			break;
		}
		case D3DFMT_A16B16G16R16F:
		{
			ret = PixelFormat::PF_64BITS_FLOAT_A16B16G16R16;
			break;
		}
		case D3DFMT_R32F:
		{
			ret = PixelFormat::PF_32BITS_FLOAT_R32;
			break;
		}
		case D3DFMT_A32B32G32R32F:
		{
			ret = PixelFormat::PF_128BITS_FLOAT_A32B32G32R32;
			break;
		}
		}

		return ret;
	}

	D3DFORMAT UNG_STDCALL ud9ToD3D9PixelFormat(PixelFormat uniformpf)
	{
		switch (uniformpf)
		{
		case PixelFormat::PF_UNKNOWN:
			return D3DFMT_UNKNOWN;
		case PixelFormat::PF_16BITS_X1R5G5B5:
			return D3DFMT_X1R5G5B5;
		case PixelFormat::PF_16BITS_R5G6B5:
			return D3DFMT_R5G6B5;
		case PixelFormat::PF_24BITS_R8G8B8:
			return D3DFMT_R8G8B8;
		case PixelFormat::PF_32BITS_X8R8G8B8:
			return D3DFMT_X8R8G8B8;
		case PixelFormat::PF_32BITS_A8R8G8B8:
			return D3DFMT_A8R8G8B8;
		case PixelFormat::PF_8BITS_A8:
			return D3DFMT_A8;
		case PixelFormat::PF_16BITS_FLOAT_R16:
			return D3DFMT_R16F;
		case PixelFormat::PF_64BITS_FLOAT_A16B16G16R16:
			return D3DFMT_A16B16G16R16F;
		case PixelFormat::PF_32BITS_FLOAT_R32:
			return D3DFMT_R32F;
		case PixelFormat::PF_128BITS_FLOAT_A32B32G32R32:
			return D3DFMT_A32B32G32R32F;
		}
	}

	D3DPOOL UNG_STDCALL ud9ToD3D9ResourcePool(ResourcePoolType rpt)
	{
		D3DPOOL ret;

		switch (rpt)
		{
		case ResourcePoolType::RPT_DEFAULT:
			ret = D3DPOOL_DEFAULT;
			break;
		case ResourcePoolType::RPT_MANAGED:
			ret = D3DPOOL_MANAGED;
			break;
		case ResourcePoolType::RPT_SYSTEM:
			ret = D3DPOOL_SYSTEMMEM;
			break;
		case ResourcePoolType::RPT_SCRATCH:
			ret = D3DPOOL_SCRATCH;
			break;
		}

		return ret;
	}

	ResourcePoolType UNG_STDCALL ud9ToUniformResourcePool(D3DPOOL rpt)
	{
		ResourcePoolType ret;

		switch (rpt)
		{
		case D3DPOOL_DEFAULT:
			ret = ResourcePoolType::RPT_DEFAULT;
			break;
		case D3DPOOL_MANAGED:
			ret = ResourcePoolType::RPT_MANAGED;
			break;
		case D3DPOOL_SYSTEMMEM:
			ret = ResourcePoolType::RPT_SYSTEM;
			break;
		case D3DPOOL_SCRATCH:
			ret = ResourcePoolType::RPT_SCRATCH;
			break;
		}

		return ret;
	}

	void UNG_STDCALL ud9ToD3D9Rect(PixelRectangle const & pr,SeparatedPointer<RECT>& out)
	{
		if (!(pr.isEntire()))
		{
			out.create();

			out->left = pr.getMinX();
			out->right = pr.getMaxX();
			out->top = pr.getMinY();
			out->bottom = pr.getMaxY();
		}
	}

	void UNG_STDCALL ud9ToUniformRect(RECT* const rect, PixelRectangle& out)
	{
		if (rect)
		{
			out.setMinX(rect->left);
			out.setMaxX(rect->right);
			out.setMinY(rect->top);
			out.setMaxY(rect->bottom);
		}
	}

	uint32 UNG_STDCALL ud9ToD3D9LockSurfaceFlag(LockSurfaceFlag const & lsf)
	{
		return static_cast<uint32>(lsf);
	}

	LockSurfaceFlag UNG_STDCALL ud9ToUniformLockSurfaceFlag(uint32 lsf)
	{
		switch (lsf)
		{
		case D3DLOCK_DISCARD:
			return LockSurfaceFlag::LSF_DISCARD;
		case D3DLOCK_DONOTWAIT:
			return LockSurfaceFlag::LSF_DONOTWAIT;
		case D3DLOCK_NO_DIRTY_UPDATE:
			return LockSurfaceFlag::LSF_NO_DIRTY_UPDATE;
		case D3DLOCK_NOOVERWRITE:
			return LockSurfaceFlag::LSF_NOOVERWRITE;
		case D3DLOCK_NOSYSLOCK:
			return LockSurfaceFlag::LSF_NOSYSLOCK;
		case D3DLOCK_READONLY:
			return LockSurfaceFlag::LSF_READONLY;
		default:
			return LockSurfaceFlag::LSF_NONE;
		}
	}

	uint32 UNG_STDCALL ud9ToD3D9ElementType(VertexElementType const & vet)
	{
		switch (vet)
		{
		case VertexElementType::VET_FLOAT1:
			return D3DDECLTYPE_FLOAT1;
		case VertexElementType::VET_FLOAT2:
			return D3DDECLTYPE_FLOAT2;
		case VertexElementType::VET_FLOAT3:
			return D3DDECLTYPE_FLOAT3;
		case VertexElementType::VET_FLOAT4:
			return D3DDECLTYPE_FLOAT4;
		case VertexElementType::VET_COLOR:
		case VertexElementType::VET_COLOR_ARGB:
			return D3DDECLTYPE_D3DCOLOR;
		default:
			BOOST_ASSERT(0 && "Vertex element type convert failed.");
		}
	}

	VertexElementType UNG_STDCALL ud9ToUniformElementType(uint32 vet)
	{
		switch (vet)
		{
		case D3DDECLTYPE_FLOAT1:
			return VertexElementType::VET_FLOAT1;
		case D3DDECLTYPE_FLOAT2:
			return VertexElementType::VET_FLOAT2;
		case D3DDECLTYPE_FLOAT3:
			return VertexElementType::VET_FLOAT3;
		case D3DDECLTYPE_FLOAT4:
			return VertexElementType::VET_FLOAT4;
		case D3DDECLTYPE_D3DCOLOR:
			return VertexElementType::VET_COLOR;
		default:
			BOOST_ASSERT(0 && "Vertex element type convert failed.");
		}
	}

	uint32 UNG_STDCALL ud9ToD3D9ElementSemantic(VertexElementSemantic const & ves)
	{
		switch (ves)
		{
		case VertexElementSemantic::VES_POSITION:
			return D3DDECLUSAGE_POSITION;
		case VertexElementSemantic::VES_BLEND_WEIGHTS:
			return D3DDECLUSAGE_BLENDWEIGHT;
		case VertexElementSemantic::VES_BLEND_INDICES:
			return D3DDECLUSAGE_BLENDINDICES;
		case VertexElementSemantic::VES_NORMAL:
			return D3DDECLUSAGE_NORMAL;
		case VertexElementSemantic::VES_DIFFUSE:
			return D3DDECLUSAGE_COLOR;
		case VertexElementSemantic::VES_TEXTURE_COORDINATES:
			return D3DDECLUSAGE_TEXCOORD;
		case VertexElementSemantic::VES_BINORMAL:
			return D3DDECLUSAGE_BINORMAL;
		case VertexElementSemantic::VES_TANGENT:
			return D3DDECLUSAGE_TANGENT;
		case VertexElementSemantic::VES_POSITION_HCS:
			return D3DDECLUSAGE_POSITIONT;
		default:
			BOOST_ASSERT("Failed to convert vertex element semantic.");
		}
	}

	VertexElementSemantic UNG_STDCALL ud9ToUniformElementSemantic(uint32 ves)
	{
		switch (ves)
		{
		case D3DDECLUSAGE_POSITION:
			return VertexElementSemantic::VES_POSITION;
		case D3DDECLUSAGE_BLENDWEIGHT:
			return VertexElementSemantic::VES_BLEND_WEIGHTS;
		case D3DDECLUSAGE_BLENDINDICES:
			return VertexElementSemantic::VES_BLEND_INDICES;
		case D3DDECLUSAGE_NORMAL:
			return VertexElementSemantic::VES_NORMAL;
		case D3DDECLUSAGE_COLOR:
			return VertexElementSemantic::VES_DIFFUSE;
		case D3DDECLUSAGE_TEXCOORD:
			return VertexElementSemantic::VES_TEXTURE_COORDINATES;
		case D3DDECLUSAGE_BINORMAL:
			return VertexElementSemantic::VES_BINORMAL;
		case D3DDECLUSAGE_TANGENT:
			return VertexElementSemantic::VES_TANGENT;
		case D3DDECLUSAGE_POSITIONT:
			return VertexElementSemantic::VES_POSITION_HCS;
		default:
			BOOST_ASSERT("Failed to convert vertex element semantic.");
		}
	}

	uint32 UNG_STDCALL ud9ToD3D9Usage(uint32 usage)
	{
		uint32 ret{ 0 };

		auto DYNAMIC = static_cast<uint32>(IVideoBuffer::Usage::VB_USAGE_DYNAMIC);
		auto WRITE_ONLY = static_cast<uint32>(IVideoBuffer::Usage::VB_USAGE_WRITE_ONLY);

		if (ungEnumCombinationHave(usage,DYNAMIC))
		{
			ungEnumCombinationJoin(ret, D3DUSAGE_DYNAMIC);
		}

		if (ungEnumCombinationHave(usage, WRITE_ONLY))
		{
			ungEnumCombinationJoin(ret, D3DUSAGE_WRITEONLY);
		}

		return ret;
	}

	uint32 UNG_STDCALL ud9ToUniformUsage(uint32 us)
	{
		uint32 ret{ 0 };

		if (!ungEnumCombinationHave(us, D3DUSAGE_DYNAMIC))
		{
			ungEnumCombinationJoin(ret, IVideoBuffer::Usage::VB_USAGE_STATIC);
		}

		if (ungEnumCombinationHave(us, D3DUSAGE_DYNAMIC))
		{
			ungEnumCombinationJoin(ret, IVideoBuffer::Usage::VB_USAGE_DYNAMIC);
		}

		if (ungEnumCombinationHave(us, D3DUSAGE_WRITEONLY))
		{
			ungEnumCombinationJoin(ret, IVideoBuffer::Usage::VB_USAGE_WRITE_ONLY);
		}

		return ret;
	}

	uint32 UNG_STDCALL ud9ToD3D9CullMode(CullMode cm)
	{
		if (cm == CullMode::CM_CW)
		{
			return D3DCULL_CW;
		}
		else if (cm == CullMode::CM_CCW)
		{
			return D3DCULL_CCW;
		}
	}

	CullMode UNG_STDCALL ud9ToUniformCullMode(uint32 cm)
	{
		if (cm == D3DCULL_CW)
		{
			return CullMode::CM_CW;
		}
		else if (cm == D3DCULL_CCW)
		{
			return CullMode::CM_CCW;
		}
	}
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE