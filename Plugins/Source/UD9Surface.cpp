#include "UD9Surface.h"

#ifdef UD9_HIERARCHICAL_COMPILE

#include "UD9RenderSystem.h"
#include "UD9Device.h"
#include "UD9Utilities.h"

namespace ung
{
	D9OffscreenPlainSurface::D9OffscreenPlainSurface(uint32 width, uint32 height, PixelFormat pf, ResourcePoolType pt) :
		mWidth(width),
		mHeight(height),
		mFormat(pf),
		mPoolType(pt),
		mSurface(nullptr)
	{
		auto pActiveDevice = D9RenderSystem::getInstance()->getActiveDevice();

		if (mPoolType == ResourcePoolType::RPT_MANAGED)
		{
			mPoolType = ResourcePoolType::RPT_DEFAULT;
		}

		/*
		Create an off-screen surface.
		HRESULT CreateOffscreenPlainSurface(UINT Width,UINT Height,D3DFORMAT Format,
		D3DPOOL Pool,IDirect3DSurface9** ppSurface,HANDLE* pSharedHandle);
		Width:
		Width of the surface.
		Height:
		Height of the surface.
		Format:
		Format of the surface.
		Pool:
		Surface pool type.
		ppSurface:
		Pointer to the IDirect3DSurface9 interface created.
		pSharedHandle:
		Reserved.Set this parameter to NULL.This parameter can be used in Direct3D 9 for 
		Windows Vista to share resources.
		If the method succeeds,the return value is D3D_OK.If the method fails,the return value 
		can be the following:D3DERR_INVALIDCALL.
		D3DPOOL_SCRATCH will return a surface that has identical characteristics to a surface 
		created by the DirectX 8.x method CreateImageSurface.
		D3DPOOL_DEFAULT is the appropriate pool for use with the IDirect3DDevice9::StretchRect 
		and IDirect3DDevice9::ColorFill.
		D3DPOOL_MANAGED is not allowed(不允许的) when creating an offscreen plain surface.
		Off-screen plain surfaces are always lockable,regardless of their pool types.
		*/
		auto ret = pActiveDevice->CreateOffscreenPlainSurface(mWidth, mHeight, ud9ToD3D9PixelFormat(mFormat), ud9ToD3D9ResourcePool(mPoolType), &mSurface,nullptr);

		BOOST_ASSERT(ret == D3D_OK);

		/*
		Get the surface description.

		typedef struct D3DSURFACE_DESC
		{
			D3DFORMAT Format;
			D3DRESOURCETYPE Type;
			DWORD Usage;
			D3DPOOL Pool;
			D3DMULTISAMPLE_TYPE MultiSampleType;
			DWORD MultiSampleQuality;
			UINT Width;
			UINT Height;
		}D3DSURFACE_DESC,*LPD3DSURFACE_DESC;
		Format:
		Member of the D3DFORMAT enumerated type,describing the surface format.
		Type:
		Member of the D3DRESOURCETYPE enumerated type,identifying this resource as a surface.
		Usage:
		Either the D3DUSAGE_DEPTHSTENCIL or D3DUSAGE_RENDERTARGET values.
		Pool:
		Member of the D3DPOOL enumerated type,specifying the class of memory allocated for 
		this surface.
		MultiSampleType:
		Member of the D3DMULTISAMPLE_TYPE enumerated type,specifying the levels of 
		full-scene multisampling supported by the surface.
		MultiSampleQuality:
		Quality level.The valid range is between zero and one less than the level returned by 
		pQualityLevels used by CheckDeviceMultiSampleType.Passing a larger value returns the 
		error,D3DERR_INVALIDCALL.The MultisampleQuality values of paired render targets,depth 
		stencil surfaces and the MultiSample type must all match.
		Width:
		Width of the surface,in pixels.
		Height:
		Height of the surface,in pixels.
		*/
		ret = mSurface->GetDesc(&mDesc);

		BOOST_ASSERT(ret == D3D_OK);
	}

	D9OffscreenPlainSurface::~D9OffscreenPlainSurface()
	{
		UD9_RELEASE(mSurface);
	}

	uint32 D9OffscreenPlainSurface::getWidth() const
	{
		return mDesc.Width;
	}

	uint32 D9OffscreenPlainSurface::getHeight() const
	{
		return mDesc.Height;
	}

	PixelFormat D9OffscreenPlainSurface::getFormat() const
	{
		return ud9ToUniformPixelFormat(mDesc.Format);
	}

	uint32 D9OffscreenPlainSurface::getColorDepth() const
	{
		return uint32();
	}

	uint32* D9OffscreenPlainSurface::lock(PixelRectangle const & rect, LockSurfaceFlag lsFlag,uint32& pitch)
	{
		BOOST_ASSERT(mSurface);

		SeparatedPointer<RECT> rt;
		ud9ToD3D9Rect(rect, rt);

		RECT* pRECT = rt.get();

		/*
		Describes a locked rectangular region.
		typedef struct D3DLOCKED_RECT
		{
			INT Pitch;
			void* pBits;
		}D3DLOCKED_RECT,*LPD3DLOCKED_RECT;
		Pitch:
		Number of bytes in one row of the surface.
		pBits:
		Pointer to the locked bits.If a RECT was provided to the LockRect call,pBits will be 
		appropriately offset from the start of the surface.
		*/
		D3DLOCKED_RECT lockedRect;

		/*
		Locks a rectangle on a surface.
		HRESULT LockRect(D3DLOCKED_RECT* pLockedRect,const RECT* pRect,DWORD Flags);
		pLockedRect:
		Pointer to a D3DLOCKED_RECT structure that describes the locked region.
		pRect:
		Pointer to a rectangle to lock.Specified by a pointer to a RECT structure.Specifying NULL 
		for this parameter expands the dirty region to cover the entire surface.
		Flags:
		Combination of zero or more locking flags that describe the type of lock to perform.For 
		this method,the valid flags are:
		D3DLOCK_DISCARD
		D3DLOCK_DONOTWAIT
		D3DLOCK_NO_DIRTY_UPDATE
		D3DLOCK_NOSYSLOCK
		D3DLOCK_READONLY
		If the method succeeds, the return value is D3D_OK.
		If the method fails, the return value can be D3DERR_INVALIDCALL or D3DERR_WASSTILLDRAWING.
		You may not specify a subrect when using D3DLOCK_DISCARD.
		(一个子矩形不能和D3DLOCK_DISCARD搭配)
		If the D3DLOCK_DONOTWAIT flag is specified and the driver cannot lock the surface 
		immediately,IDirect3DSurface9::LockRect will return D3DERR_WASSTILLDRAWING so 
		that an application can use the CPU cycles while waiting for the driver to lock the surface.
		The only lockable format for a depth-stencil surface is D3DFMT_D16_LOCKABLE.
		(对于深度-模板表面，只有当其格式为D3DFMT_D16_LOCKABLE时，才可对其进行锁操作)
		For performance reasons,dirty regions are recorded only for level zero of a texture.Dirty 
		regions are automatically recorded when IDirect3DSurface9::LockRect is called without 
		D3DLOCK_NO_DIRTY_UPDATE or D3DLOCK_READONLY.
		See IDirect3DDevice9::UpdateTexture for more information.
		A multisample back buffer cannot be locked.
		This method cannot retrieve data from a surface that is contained by a texture resource 
		created with D3DUSAGE_RENDERTARGET because such a texture must be assigned to 
		D3DPOOL_DEFAULT memory and is therefore not lockable.In this case,use instead 
		IDirect3DDevice9::GetRenderTargetData to copy texture data from device memory to 
		system memory.
		*/
		auto ret = mSurface->LockRect(&lockedRect,pRECT,ud9ToD3D9LockSurfaceFlag(lsFlag));
		BOOST_ASSERT(ret == D3D_OK);

		//销毁
		rt.destroy();

		pitch = lockedRect.Pitch;

		return static_cast<uint32*>(lockedRect.pBits);

		/*
		//Iterate through each pixel in the surface and set it to red.
		DWORD* imageData = (DWORD*)lockedRect.pBits;				//DWORD:unsigned long
		for(int i = 0; i < surfaceDesc.Height; i++)
		{
			for(int j = 0; j < surfaceDesc.Width; j++)
			{
				//Index into texture,note we use the pitch and divide by four since the pitch is given 
				//in bytes and there are 4 bytes per DWORD.
				int index = i * lockedRect.Pitch / 4 + j;
				imageData[index] = 0xffff0000;									//red

				//The 32-bit pixel format assumption is important since we cast the bits to DWORDs,
				//which are 32 bits.This lets us treat every DWORD as representing a pixel in the 
				//surface.
			}
		}
		*/
	}

	void D9OffscreenPlainSurface::unlock()
	{
		BOOST_ASSERT(mSurface);

		/*
		Unlocks a rectangle on a surface.
		HRESULT UnlockRect();
		If the method succeeds, the return value is D3D_OK.If the method fails, the return value 
		can be D3DERR_INVALIDCALL.
		*/
		auto ret = mSurface->UnlockRect();

		BOOST_ASSERT(ret == D3D_OK);
	}
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE

/*
在Direct3D中，表面（Surface）代表显示内存中的一块线性区域，尽管表面可以位于系统内存，但是通常
情况下表面位于显卡的显存中。
与表面相关的各种操作以及对表面的管理由IDirect3DSurface9接口完成。
前台缓冲区，后台缓冲区
表面翻转（flipping surface）：
将后台缓冲区的内容提交到前台缓冲区从而在屏幕上显示的过程。
交换链（swap chain）：
一个或多个后台缓冲区的集合，这些后台缓冲区可以按顺序逐个提交到前台缓冲区。

可以通过调用下面函数中的任意一个来创建一个表面：
IDirect3DDevice9::CreateDepthStencilSurface()
IDirect3DDevice9::CreateOffscreenPlainSurface()
IDirect3DDevice9::CreateRenderTarget()

可以通过调用IDirect3DSurface9::GetDesc()函数来获得一个已存在表面的格式(D3DSURFACE_DESC)。

一个表面一经创建，就可以通过下面的任何一个函数来获取指向该表面的指针：
IDirect3DCubeTexture9::GetCubeMapSurface()
IDirect3DDevice9::GetBackBuffer()
IDirect3DDevice9::GetDepthStencilSurface()
IDirect3DDevice9::GetFrontBufferData()
IDirect3DDevice9::GetRenderTarget()
IDirect3DSwapChain9::GetBackBuffer()
IDirect3DTexture9::GetSurfaceLevel()
利用IDirect3DSurface9接口，可以通过函数IDirect3DDevice9::UpdateSurface()直接访问表面内存。
IDirect3DDevice9::UpdateSurface()函数允许从一个IDirect3DSurface9接口表示的表面复制一块矩形
像素区域到另一个IDirect3DSurface9接口表示的表面。IDirect3DSurface9接口还允许直接访问显示内存，
例如可以使用IDirect3DSurface9::LockRect()函数锁定显存中的一块区域，对锁定的区域操作结束后，还
必须调用IDirect3DSurface9::UnlockRect()函数进行解锁。

表面翻转(交换指针)：
图形适配器保存一个表面的指针，该表面代表即将在显示器上显示的图像，这个表面称作前台缓冲区。当显示
器刷新时，显卡将前台缓冲区的内容发送到显示器去显示。通常，刷新频率的范围是60~100Hz，当应用程序
在显示器的一个刷新周期中间更新前台缓冲区时，显示器显示的上半部分图像是更新前的旧图像，而下半部分
显示的是更新后的新图像，这种现象叫做“撕裂(tearing)”。
Direct3D通过两种途径来避免撕裂现象的发生。
1，仅在显示垂直回扫(vertical retrace)时允许更新前台缓冲区。显示器更新显示图像通常是通过光头(light pin)
自上至下，自左至右逐行扫描完成的。当光头到达右下角时，显示器将光头重新定位到左上部，从而可以进行
新一轮的显示图像更新，这个重新定位光头的过程就是垂直回扫。在垂直回扫期间，显示器不显示任何内容，
所以对前台缓冲区的任何更新都不会显示，直至显示器进行新一轮的图形扫描显示。尽管垂直回扫相对较慢，
但是，对于渲染一个复杂场景来说，更新前台缓冲区的时间还是会超过垂直回扫的时间，所以，如果在渲染复
杂场景时想要避免撕裂现象，就需要一个后台缓存（back buffer）过程。
2，使用后台缓存技术。表面翻转，在一个显示器刷新周期内可以进行一系列的提交后台缓冲区请求，这就是
Direct3D设备提交间隔（presentation interval）的概念。

交换链：
交换链是按顺序逐个提交到前台显示的多个后台缓冲区的集合。
在Direct3D中创建的每一个渲染设备至少有一个交换链。
可以通过调用IDirect3DDevice9::CreateAdditionalSwapChain()函数，为Direct3D设备创建一个交换链。
一个应用程序可以为每个视图创建一个交换链，并将每一个交换链和一个特定的窗口相关联。需要注意的是：
一个Direct3D设备只能有一个全屏幕交换链。
可以通过调用IDirect3DDevice9::GetBackBuffer()或IDirect3DSwapChain9::GetBackBuffer()函数，
获取特定的后台缓冲区，这两个函数都返回指向IDirect3DSurface9接口的指针。注意：调用这两个函数会
增加对表面对象的引用次数，所以在使用完该表面接口指针后要记得调用Release()函数，否则会造成内存泄
漏。
“翻转”一词更多地用于描述对任一用D3DSWAPEFFECT_FLIP标志创建的交换链的后台缓冲区的提交，而不是
由显卡驱动程序执行的表面翻转操作（flipping operation）。
注意：对于提交操作，当交换链是全屏交换链时，几乎总是由翻转操作实现的。而对于窗口交换链，提交操作
则必须由复制操作实现。

直接访问表面内存：
可以通过使用IDirect3DSurface9::LockRect()函数直接访问表面内存。该函数的pRect参数是一个指向RECT
结构的指针，该参数表示要直接访问的表面内存矩形。将该参数设置为NULL，表示直接操作整个表面内存。同
样，可以用一个矩形表示表面内存的一部分。当多个线程锁定多个表面上的矩形区域时，它们不能重叠。另外，
多重采样后台缓冲区不能锁定。
IDirect3DSurface9::LockRect()函数将所有正确访问表面内存的信息填充到一个D3DLOCKED_RECT结构
中，该结构包含一个整形成员变量pitch和一个锁定内存块的指针。当对表面内存的访问结束后，调用函数
IDirect3DSurface9::UnlockRect()，解锁该表面。
当锁定一个表面时，就可以直接操作其中的内容。
1，永远不要假定显示间距（pitch）是固定不变的。总是要检查IDirect3DSurface9::LockRect()返回的间距
信息，有多种情况会造成间距变化，包括表面内存的位置，显卡类型以及Direct3D的驱动版本。
2，确定复制的目标是一个锁定的表面。Direct3D的复制方法对一个未锁定的表面进行操作会失败。
3，当一个表面锁定时，限制程序的操作。

私有表面数据：
可以使用表面保存任何类型的应用程序的特定数据。
一个表面可以有多个私有数据缓冲区。每个缓冲区有一个GUID标识。
要存储私有数据，需要调用IDirect3DSurface9::SetPrivateData()函数，并传递一个指向源缓冲区的指针和
一个在程序中为要保存的数据指定的GUID。准备保存的源数据可以COM对象的形式存在，这时，需要传递一
个该对象的IUnknown接口指针，并且需要将最后一个参数Flags设置为D3DSPD_IUNKNOWNPOINTER。

伽玛控制：
通过Gamma控制可以改变如何显示表面内容，而不影响表面内容，可以将这些控制看做在渲染到屏幕之前
Direct3对数据的简单过滤。
Gamma控制是交换链的一个属性。Gamma控制可以动态改变如何将表面的红，绿，蓝颜色级别映射到实际
系统显示的颜色级别。通过设置Gamma级别，可以使用用户屏幕闪现颜色，例如当用户控制的角色射击时闪
现红色，当用户角色选择新的对象时闪现绿色等等。如果不使用Gamma控制，只有通过应用颜色偏差调整后
台缓冲区中的颜色级别来实现上面的效果。
Gamma控制设置会立即起作用，而不用等待垂直直线同步操作。
通过调用IDirect3DDevice9::SetGammaRamp()函数和IDirect3DDevice9::GetGammaRamp()函数，
在将表面每个像素传送至数模转换器(DAC)之前，可以控制影响表面每个像素红，绿，蓝各颜色组分的映射
级别。
Gamma Ramp级别：
Gamma Ramp描述的是数值集合，这些数值指的是帧缓冲区中每个像素特定颜色组分（红，绿，蓝）映射到
显示器数模转换器对应颜色组分的级别。这种重新映射操作是通过查表实现的，每个颜色组分有一张表。
这种映射的工作过程是：Direct3D从帧缓冲区取一个像素，并计算它的红，绿，蓝3个颜色分量值。每个颜色
组分由0~65535之间的一个数值表示。Direct3D取出每个颜色组分的原数值并用它来索引256个元素的数组
（ramp），每个数组元素包含一个值，用来代替原颜色值。因为Direct3D为帧缓冲区中每个像素的每个颜色
组分别执行查表和替换操作，所以改变了所有最终显示到屏幕上的每个像素的颜色。
设置和获取Gamma Ramp级别：
通过调用IDirect3DDevice9::SetGammaRamp()函数可以为主表面设置Gamma Ramp级别，调用
IDirect3DDevice9::GetGammaRamp()函数可以获取主表面的Gamma Ramp级别。
*/

/*
Depth Buffers:
The depth buffer is a surface that does not contain image data but rather depth information 
about a particular pixel.The possible depth values range from 0.0 to 1.0,where 0.0 denotes 
the closest an object can be to the viewer and 1.0 denotes the farthest an object can be from 
the viewer.There is an entry in the depth buffer that corresponds to each pixel in the back 
buffer (i.e., the ijth element in the back buffer corresponds to the ijth element in the depth 
buffer).So if the back buffer had a resolution of 1280 × 1024,there would be 1280 × 1024 
depth entries.
Note Actually,the requirement is that the depth buffer dimensions be greater than or equal to 
the render target (back buffer).

Multisampling:
Generates the final color of a pixel by sampling its neighboring pixels.
Shrinking the pixel sizes by increasing the monitor resolution can alleviate(减轻，缓和) the 
problem significantly to where the stairstep effect goes largely unnoticed.
When increasing the monitor resolution is not possible or not enough,we can apply antialiasing 
techniques.Direct3D supports an antialiasing technique called multisampling,which works by 
taking the neighboring pixels into consideration when computing the final color of a pixel.Thus,
the technique is called multisampling because it uses multiple pixel samples to compute the 
final color of a pixel.
The D3DMULTISAMPLE_TYPE enumerated type consists of values that allow us to specify the 
number of samples to use in multisampling:
D3DMULTISAMPLE_NONE:Specifies no multisampling.
D3DMULTISAMPLE_2_SAMPLES,..., D3DMULTISAMPLE_16_SAMPLES:Specifies to use 2, …, 16 
samples.
There is also a quality level associated with the multisampling type,this is described as a 
DWORD.We actually specify the multisampling type and quality level for the back buffer during 
device creation.
Before using a particular multisampling type (other than D3DMULTISAMPLE_NONE),we need to 
check whether the hardware supports it.We do this with the 
IDirect3D9::CheckDeviceMultiSampleType method.

Memory Pools:
A Direct3D resource,such as a surface,can exist in system memory(RAM),video memory(VRAM),
or AGP memory.Device accessible memory refers to either VRAM or AGP memory since the 
graphics device can directly access memory of these types;the device cannot access system 
memory.The Direct3D memory pool determines the memory type in which a resource is placed.
A memory pool is specified by one of the members of the D3DPOOL enumerated type.The three 
main members of this type are:
D3DPOOL_MANAGED:
Resources placed in the managed pool are backed up in system memory(自动在系统内存中保存了
一个备份),and are automatically moved between system memory and device accessible memory 
as needed(自动在显存和系统内存之间移动),based on the Direct3D Runtime's own management 
system.When resources in this pool are accessed and changed by the application,they work 
with the system memory backup copy,and then Direct3D automatically updates them to device
accessible memory as needed.Managed resources do not need to be destroyed prior to an 
IDirect3DDevice9::Reset call,and do not need to be reinitialized after the reset call(无需在
IDirect3DDevice9::Reset()之前销毁，也无需在其后重新初始化).
D3DPOOL_DEFAULT:
The default memory pool instructs Direct3D to place the resource in the memory that is best 
suited for the resource type and usage --- usually VRAM or AGP memory.Resources in the 
default pool are not backed up in system memory by Direct3D,and must be destroyed(released) 
prior to an IDirectSDDevice9::Reset call and reinitialized after the reset call(必须在
IDirect3DDevice9::Reset()之前销毁，也必须在其后重新初始化).
D3DPOOL_SYSTEMMEM:
Specifies that the resource be placed in system memory.Note that the graphics device cannot 
use a resource placed in this pool.This pool is usually used when the application needs to do 
some work on the resource before copying it into device accessible memory,or when the 
application just wants its own local backup copy of a resource.System resources do not need to 
be destroyed prior to an IDirect3DDevice9::Reset call,and do not need to be reinitialized after 
the reset call.

Vertex Processing and Pure Devices:
Vertices are the building blocks for 3D geometry and they can be processed in two different 
ways:in software (software vertex processing) or in hardware (hardware vertex processing).
Software vertex processing is always supported and can always be used.
Hardware vertex processing can only be used if the graphics card supports vertex processing in 
hardware.
Hardware vertex processing is always preferred since dedicated hardware is faster than 
software.Furthermore,performing vertex processing in hardware unloads calculations from the 
CPU,which implies the CPU is free to perform other calculations(也就是说，硬件顶点处理，是在GPU
中对顶点进行处理).
The IDirect3DDevice9 provides many Get style methods,which return information about the 
device.A pure device refers to a device in which some of these Get methods (e.g.,render state,
transform state,lights,and texture Get methods) are disabled;moreover,a pure device can only 
be specified with hardware vertex processing.The benefit,however,for using a pure device is 
that it may lead to a speed increase.
There is also a mixed vertex processing mode,which allows you to mix both software and 
hardware vertex processing.For example,you may want to do some vertex processing in 
software and some in hardware.The typical reason for doing this is if the hardware doesn't 
support a certain vertex processing feature (e.g.,some vertex shader),then you emulate(仿真) 
that particular feature by using software vertex processing,and use hardware vertex processing 
wherever you can.
Another way of saying a graphics card supports hardware vertex processing in hardware is to 
say that the graphics card supports transformation and lighting calculations in hardware(支持硬
件顶点处理的另外一个说法是，硬件支持顶点变换和光照计算).

Device Capabilities:
Every feature Direct3D exposes has a corresponding data member or bit in the D3DCAPS9 
structure.
*/