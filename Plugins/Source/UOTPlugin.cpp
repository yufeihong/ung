#include "UOTPlugin.h"

#ifdef UOT_HIERARCHICAL_COMPILE

#ifndef WIN32_LEAN_AND_MEAN
#if UNG_PLATFORM == UNG_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#if !defined(NOMINMAX) && defined(_MSC_VER)
#define NOMINMAX																					//required to stop windows.h messing up std::min
#endif
#include <windows.h>
#endif
#endif

#include "UOTSpatialManagerFactory.h"

namespace ung
{
	namespace
	{
		const char* pluginName = "plugin_UOT";
	}//namespace

	OCTPlugin::OCTPlugin()
	{
	}

	OCTPlugin::~OCTPlugin()
	{
	}

	const char* OCTPlugin::getName() const
	{
		return pluginName;
	}

	/*
	HINSTANCE:A handle to an instance. This is the base address of the module in memory.
	*/
	void OCTPlugin::install()
	{
#if UNG_DEBUGMODE
		HINSTANCE hInst = GetModuleHandle(L"UOT_d.dll");
#else
		HINSTANCE hInst = GetModuleHandle(L"UOT.dll");
#endif

		//UNG_PRINT_ONE_LINE("八叉树插件已经安装。");

		//创建八叉树管理器工厂
		mOctreeSpatialManagerFactory = UNG_NEW OctreeSpatialManagerFactory;

		Root::getInstance().getSpatialManagerEnumerator().addFactory(mOctreeSpatialManagerFactory);
	}

	void OCTPlugin::uninstall()
	{
		//卸载插件之前做清理工作
		Root::getInstance().getSpatialManagerEnumerator().removeFactory(mOctreeSpatialManagerFactory);
		UNG_DEL(mOctreeSpatialManagerFactory);
		BOOST_ASSERT(!mOctreeSpatialManagerFactory);

		//UNG_PRINT_ONE_LINE("八叉树插件已经卸载。");
	}
}//namespace ung

#endif//UOT_HIERARCHICAL_COMPILE