#include "UOTNode.h"

#ifdef UOT_HIERARCHICAL_COMPILE

namespace
{
	using namespace ung;

	/*!
	 * \remarks 计算给定depth两个节点的中心间距
	 * \return 
	 * \param int32 depth
	*/
	real_type nodeDistance(int32 depth)
	{
		return global_world_size / (Math::calPow(2, depth));
	}

	/*!
	 * \remarks 计算给定depth的立方体边长(已经loose了)
	 * \return 
	 * \param int32 depth
	*/
	real_type cubeLength(int32 depth)
	{
		return 2.0 * nodeDistance(depth);
	}

	/*!
	 * \remarks 计算给定depth的立方体边长(没有包含loose)
	 * \return 
	 * \param int32 depth
	*/
	real_type cubeLengthWithoutLoose(int32 depth)
	{
		return nodeDistance(depth);
	}

	/*!
	 * \remarks 计算在给定深度，能够随意放置的对象的最大半径。
	 * 在松散八叉树中一个给定的层次能够容纳任何其半径小于或等于包围立方体边长1 / 4的物体，而不管其位置如何
	 * \return 
	 * \param int32 depth
	*/
	real_type arbitraryPositionR(int32 depth)
	{
		return 0.5 * nodeDistance(depth);
	}

	/*!
	 * \remarks 获取第一个能够容纳一个半径为radius的物体的树的层次
	 * 比这个层次更低的层次是肯定无法放置参数所给半径的对象的
	 * 但是这个层次不一定就能放置下特定的对象，因为还与对象的放置位置有关
	 * 实际的深度值小于等于该值
	 * \return 
	 * \param real_type radius
	*/
	int32 probablyDepth(real_type radius)
	{
		auto d = Math::calFloor(Math::calLog2(global_world_size / radius));

		//如果可能的深度值大于最大深度值
		if (d > global_octree_max_depth)
		{
			d = global_octree_max_depth;
		}

		return d;
	}
}//namespace

namespace ung
{
	OctreeNode::OctreeNode(OctreeNode* parent) :
		mParent(parent),
		mDepth(parent ? parent->getDepth() + 1 : 0)
	{
		//初始化8个子节点
		for (uint8 i = 0; i < 8; ++i)
		{
			mChildren[i] = nullptr;
		}

		//把根节点的6个面的邻居都设置为nullptr
		if (!mParent)
		{
			for (uint8 i = 0; i < 6; ++i)
			{
				mNeighbors[i] = nullptr;
			}
		}
	}

	OctreeNode::~OctreeNode()
	{
	}

	OctreeNode * OctreeNode::getParent() const
	{
		return mParent;
	}

	int32 OctreeNode::getDepth() const
	{
		return mDepth;
	}

	void OctreeNode::attachObject(StrongIObjectPtr objectPtr)
	{
		BOOST_ASSERT(objectPtr);

#if UNG_DEBUGMODE
		auto findRet = std::find(mObjects.begin(),mObjects.end(),objectPtr);
		BOOST_ASSERT(findRet == mObjects.end());
#endif

		mObjects.push_back(objectPtr);
	}

	void OctreeNode::detachObject(StrongIObjectPtr objectPtr)
	{
		BOOST_ASSERT(objectPtr);

#if UNG_DEBUGMODE
		auto findRet = std::find(mObjects.begin(), mObjects.end(), objectPtr);
		BOOST_ASSERT(findRet != mObjects.end());
#endif

		mObjects.remove(objectPtr);
	}

	OctreeNode * OctreeNode::createChild()
	{
		return nullptr;
	}

	void OctreeNode::createChildren()
	{
		for (uint8 i = 0; i < 8; i++)
		{
			mChildren[i] = UNG_NEW OctreeNode(this);

			//计算该孩子节点的中心坐标
			mChildren[i]->calCenterPosition(i);
		}

		//填充上面创建的8个孩子节点的每个节点的6个面的邻居
		calChildrenNeighbor();
	}

	void OctreeNode::destroyChildren()
	{
		for (uint8 i = 0; i < 8; ++i)
		{
			//当前孩子节点(这里auto必须显式引用)
			auto& child = mChildren[i];

			//只能从下往上销毁
			BOOST_ASSERT(!child->hasChildren());

			//必须没有持有任何对象
			BOOST_ASSERT(child->mObjects.empty());

			//遍历当前孩子节点的6个面的邻居
			for (uint8 j = 0; j < 6; ++j)
			{
				//当前邻居
				auto neighbor = child->mNeighbors[j];

				if (!neighbor)
				{
					continue;
				}

				//当前邻居的深度
				auto neighborDepth = neighbor->mDepth;
				//如果邻居的深度等于当前孩子节点的深度
				if (neighborDepth == child->mDepth)
				{
					//设置该邻居节点的某个面的邻居为当前孩子节点的父节点
					uint8 m{ 6 };
					for (uint8 k = 0; k < 6; ++k)
					{
						//该孩子节点的某个面的邻居，必然是当前的孩子节点
						if (neighbor->mNeighbors[k] == child)
						{
							m = k;
							break;
						}
					}
					BOOST_ASSERT(m < 6);
					neighbor->mNeighbors[m] = this;
				}
				
				child->mNeighbors[j] = nullptr;
			}

			//释放当前孩子节点
			child->mParent = nullptr;
			child->mDepth = -1;
			child->mCenter = Vector3{};
			UNG_DEL(child);
		}//end for (uint8 i = 0; i < 8; ++i)
	}

	OctreeNode * OctreeNode::getNeighbor(uint32 index) const
	{
		return mNeighbors[index];
	}

	OctreeNode * OctreeNode::getChild(uint32 index) const
	{
		return mChildren[index];
	}

	bool OctreeNode::hasChildren() const
	{
		if (mChildren[0])
		{
			return true;
		}

		return false;
	}

	Vector3 const & OctreeNode::getCenterPosition() const
	{
		return mCenter;
	}

	AABB OctreeNode::getAABB() const
	{
		//基于当前节点的边长来构建一个节点的AABB
		auto nodeLen = cubeLength(mDepth);

		return AABB(Vector3(-nodeLen),Vector3(nodeLen));
	}

	OctreeNode * OctreeNode::push(StrongIObjectPtr objectPtr)
	{
		//目前设计，该函数仅允许根节点来调用
		BOOST_ASSERT(!mParent);
		
		//根节点调用这个函数
		auto theNode = pushImpl(objectPtr);

		return theNode;
	}

	void OctreeNode::pop(StrongIObjectPtr objectPtr)
	{
		detachObject(objectPtr);

		//尝试消融节点
		OctreeNode* upper = getParent();
		while (upper)
		{
			bool canDestroy{ true };
			for (uint8 i = 0; i < 8; ++i)
			{
				auto child = upper->mChildren[i];

				if (child->hasAttachedObject())
				{
					canDestroy = false;
				}
			}

			//如果当前节点的8个兄弟节点都再没有关联任何对象了，那么可以销毁这8个兄弟节点
			if (canDestroy)
			{
				upper->destroyChildren();

				/*
				继续向上，这样可以把当时插入对象时“路途”上专门为该对象创建的“细分”都给消融掉。
				这样做，虽然实时地释放了内存空间，但是如果一个对象在某个附近区域徘徊来徘徊去，
				就会导致不断地创建和销毁行为，从而影响效率。
				认为，可以做成延迟一定时间再销毁。
				*/
				upper = upper->getParent();
			}
			else
			{
				break;
			}
		}
	}

	uint32 OctreeNode::getAttachedObjectCount() const
	{
		return mObjects.size();
	}

	bool OctreeNode::hasAttachedObject() const
	{
		if (mObjects.empty())
		{
			return false;
		}

		return true;
	}

	void OctreeNode::postTraverseDepthFirstForDestroy(std::function<void(OctreeNode*)> fn)
	{
		if (hasChildren())
		{
			for (uint8 i = 0; i < 8; i++)
			{
				OctreeNode* child = mChildren[i];

				child->postTraverseDepthFirstForDestroy(fn);

				//到第8个节点的时候，才通过父节点来销毁这8个兄弟节点
				if (i == 7)
				{
					fn(child->mParent);
				}
			}
		}
	}

	void OctreeNode::preTraverseDepthFirst(std::function<void(OctreeNode*)> fn)
	{
		fn(this);

		if (hasChildren())
		{
			for (uint8 i = 0; i < 8; i++)
			{
				OctreeNode* child = mChildren[i];

				child->preTraverseDepthFirst(fn);
			}
		}
	}

	OctreeNode * OctreeNode::findSuitableNode(Vector3 const & worldPosition)
	{
		//目前设计，该函数仅允许根节点来调用
		BOOST_ASSERT(!mParent);

		return findProbablyNode(worldPosition,global_octree_max_depth);
	}

	OctreeNode * OctreeNode::findSuitableNode(StrongIObjectPtr objectPtr)
	{
		//目前设计，该函数仅允许根节点来调用
		BOOST_ASSERT(!mParent);

		//对象的世界坐标系位置
		auto objectWorldPosition = objectPtr->getTransformComponent()->getInWorldPosition();

		//根据对象半径找到一个有可能的最深层
		int32 proDepth = probablyDepth(objectPtr->getRadius());

		auto proNode = findProbablyNode(objectWorldPosition.toVector3(),proDepth);

		//测试这个找到的节点能否容纳对象
		auto conRet = proNode->canContain(objectPtr);

		if (conRet)
		{
			return proNode;
		}

		BOOST_ASSERT(proNode->mParent);

		return proNode->mParent;
	}

	OctreeNode * OctreeNode::findSuitableNode(Sphere const & sphere)
	{
		//目前设计，该函数仅允许根节点来调用
		BOOST_ASSERT(!mParent);

		//球心的世界坐标系位置
		auto centerWorldPosition = sphere.getCenter();

		//根据球体半径找到一个有可能的最深层
		int32 proDepth = probablyDepth(sphere.getRadius());

		auto proNode = findProbablyNode(centerWorldPosition, proDepth);

		//测试这个找到的节点能否容纳球体
		auto conRet = proNode->canContain(sphere);

		if (conRet)
		{
			return proNode;
		}

		BOOST_ASSERT(proNode->mParent);

		return proNode->mParent;
	}

	OctreeNode * OctreeNode::findSuitableNode(AABB const & box)
	{
		//目前设计，该函数仅允许根节点来调用
		BOOST_ASSERT(!mParent);

		//AABB的世界坐标系位置
		auto centerWorldPosition = box.getCenter();

		//根据AABB的半径找到一个有可能的最深层
		int32 proDepth = probablyDepth(box.getRadius());

		auto proNode = findProbablyNode(centerWorldPosition, proDepth);

		//测试这个找到的节点能否容纳AABB
		auto conRet = proNode->canContain(box);

		if (conRet)
		{
			return proNode;
		}

		BOOST_ASSERT(proNode->mParent);

		return proNode->mParent;
	}

	bool OctreeNode::canContain(StrongIObjectPtr objectPtr)
	{
		//先看看当前深度可以任意放置对象的半径是多少
		auto arbitraryRadius = arbitraryPositionR(mDepth);
		//如果对象的半径比这个arbitraryRadius还小，那么当前深度不用检查就没问题
		if (objectPtr->getRadius() <= arbitraryRadius)
		{
			return true;
		}

		//对象的模型空间的AABB
		auto objectLocalAABB = objectPtr->getAABB();
		//对象到世界空间的变换的逆变换
		auto toModelSpaceOfObject = objectPtr->getTransformComponent()->getToWorldMatrixInverse();

		//当前节点位于世界空间的AABB
		auto nodeBox = getAABB();

		/*
		把nodeBox给变换到对象的模型空间。
		*/
		auto nodeBoxInObjectModelSpace = nodeBox * toModelSpaceOfObject;

		/*
		构建一个位于节点坐标系的对象的AABB
		相当于把对象的模型空间的AABB给临时平移到节点的坐标系中来进行一次包含测试。
		*/
		auto containRet = nodeBoxInObjectModelSpace.contain(objectLocalAABB);

		return containRet;
	}

	bool OctreeNode::canContain(Sphere const & sphere)
	{
		//球体的半径
		auto sphereRadius = sphere.getRadius();

		//先看看当前深度可以任意放置对象的半径是多少
		auto arbitraryRadius = arbitraryPositionR(mDepth);
		//如果球体的半径比这个arbitraryRadius还小，那么当前深度不用检查就没问题
		if (sphereRadius <= arbitraryRadius)
		{
			return true;
		}

		//球体的世界空间的AABB
		auto sphereWorldBox = sphere.getWorldAABB();

		//当前节点位于世界空间的AABB
		auto nodeBox = getAABB();

		auto containRet = nodeBox.contain(sphereWorldBox);

		return containRet;
	}

	bool OctreeNode::canContain(AABB const & box)
	{
		//AABB的半径
		auto boxRadius = box.getRadius();

		//先看看当前深度可以任意放置对象的半径是多少
		auto arbitraryRadius = arbitraryPositionR(mDepth);
		//如果AABB的半径比这个arbitraryRadius还小，那么当前深度不用检查就没问题
		if (boxRadius <= arbitraryRadius)
		{
			return true;
		}

		//当前节点位于世界空间的AABB
		auto nodeBox = getAABB();

		auto containRet = nodeBox.contain(box);

		return containRet;
	}

	typename ListConstIterators<StrongIObjectPtr>::type OctreeNode::getObjectsConstIterators() const
	{
		return std::make_pair(mObjects.cbegin(), mObjects.cend());
	}

	void OctreeNode::calCenterPosition(uint8 index)
	{
		//父节点的中心坐标
		auto parentPos = mParent->mCenter;

		//当前节点的中心和其父节点中心在某个轴上的偏移量
		auto halfLen = cubeLengthWithoutLoose(mDepth) * 0.5;

		switch (index)
		{
		case 0:
			mCenter = Vector3(parentPos.x + halfLen,parentPos.y + halfLen,parentPos.z + halfLen);
			break;
		case 1:
			mCenter = Vector3(parentPos.x + halfLen,parentPos.y + halfLen,parentPos.z - halfLen);
			break;
		case 2:
			mCenter = Vector3(parentPos.x + halfLen,parentPos.y - halfLen,parentPos.z + halfLen);
			break;
		case 3:
			mCenter = Vector3(parentPos.x + halfLen,parentPos.y - halfLen,parentPos.z - halfLen);
			break;
		case 4:
			mCenter = Vector3(parentPos.x - halfLen,parentPos.y + halfLen,parentPos.z + halfLen);
			break;
		case 5:
			mCenter = Vector3(parentPos.x - halfLen,parentPos.y + halfLen,parentPos.z - halfLen);
			break;
		case 6:
			mCenter = Vector3(parentPos.x - halfLen,parentPos.y - halfLen,parentPos.z + halfLen);
			break;
		case 7:
			mCenter = Vector3(parentPos.x - halfLen,parentPos.y - halfLen,parentPos.z - halfLen);
			break;
		default:
			UNG_EXCEPTION("The index range specified by the parameter is out of bounds.");
		}
	}

	void OctreeNode::calChildrenNeighbor()
	{
		for (uint8 i = 0; i < 8; ++i)
		{
			//当前孩子节点
			auto child = mChildren[i];

			switch (i)
			{
				case 0:
				{
					child->mNeighbors[3] = mChildren[4];
					child->mNeighbors[4] = mChildren[2];
					child->mNeighbors[5] = mChildren[1];

					child->calOuterSurfaceNeighbor(0);
					child->calOuterSurfaceNeighbor(1);
					child->calOuterSurfaceNeighbor(2);

					break;
				}
				case 1:
				{
					child->mNeighbors[2] = mChildren[0];
					child->mNeighbors[3] = mChildren[5];
					child->mNeighbors[4] = mChildren[3];

					child->calOuterSurfaceNeighbor(0);
					child->calOuterSurfaceNeighbor(1);
					child->calOuterSurfaceNeighbor(5);

					break;
				}
				case 2:
				{
					child->mNeighbors[1] = mChildren[0];
					child->mNeighbors[3] = mChildren[6];
					child->mNeighbors[5] = mChildren[3];

					child->calOuterSurfaceNeighbor(0);
					child->calOuterSurfaceNeighbor(2);
					child->calOuterSurfaceNeighbor(4);

					break;
				}
				case 3:
					child->mNeighbors[1] = mChildren[1];
					child->mNeighbors[2] = mChildren[2];
					child->mNeighbors[3] = mChildren[7];

					child->calOuterSurfaceNeighbor(0);
					child->calOuterSurfaceNeighbor(4);
					child->calOuterSurfaceNeighbor(5);
					break;
				case 4:
				{
					child->mNeighbors[0] = mChildren[0];
					child->mNeighbors[4] = mChildren[6];
					child->mNeighbors[5] = mChildren[5];

					child->calOuterSurfaceNeighbor(1);
					child->calOuterSurfaceNeighbor(2);
					child->calOuterSurfaceNeighbor(3);
					break;
				}
				case 5:
				{
					child->mNeighbors[0] = mChildren[1];
					child->mNeighbors[2] = mChildren[4];
					child->mNeighbors[4] = mChildren[7];

					child->calOuterSurfaceNeighbor(1);
					child->calOuterSurfaceNeighbor(3);
					child->calOuterSurfaceNeighbor(5);
					break;
				}
				case 6:
				{
					child->mNeighbors[0] = mChildren[2];
					child->mNeighbors[1] = mChildren[4];
					child->mNeighbors[5] = mChildren[7];

					child->calOuterSurfaceNeighbor(2);
					child->calOuterSurfaceNeighbor(3);
					child->calOuterSurfaceNeighbor(4);
					break;
				}
				case 7:
				{
					child->mNeighbors[0] = mChildren[3];
					child->mNeighbors[1] = mChildren[5];
					child->mNeighbors[2] = mChildren[6];

					child->calOuterSurfaceNeighbor(3);
					child->calOuterSurfaceNeighbor(4);
					child->calOuterSurfaceNeighbor(5);
					break;
				}
			}//end switch
		}//end for
	}

	void OctreeNode::calOuterSurfaceNeighbor(uint8 faceIndex)
	{
		/*
		当前节点外表面的邻居，默认设置为当前节点父节点的外表面邻居。
		neighbor_p中的p后缀表示，相对于这个邻居节点的子节点来说，它是父。
		注意，当前节点父节点的邻居，有可能并不是和父节点深度相等的，可能会更高于父节点
		*/
		auto neighbor_p = mParent->mNeighbors[faceIndex];
		mNeighbors[faceIndex] = neighbor_p;

		if (!neighbor_p)
		{
			return;
		}

		//判断neighbor_p是否有孩子节点
		auto has_children = neighbor_p->hasChildren();
		if (has_children)
		{
			/*
			neighbor_p有孩子节点，那么其8个孩子节点中，能够和当前节点互为邻居的子节点就是距离当前
			节点最近的那个子节点。
			*/

			//计算当前节点的中心和neighbor_p的8个孩子节点的中心的距离的平方
			real_type dis2 = nodeDistance(0) * nodeDistance(0);			//初始化为一个最大值

			uint8 j = 8;
			for (uint8 i = 0; i < 8; ++i)
			{
				//当前节点的中心和neighbor_p->mChildren[i]节点的中心连接后的一个向量
				auto v = mCenter - neighbor_p->mChildren[i]->mCenter;
				//距离的平方
				auto len2 = v.dotProduct(v);
				//找到最小距离所对应的那个neighbor_p的孩子节点的索引
				if (dis2 > len2)
				{
					dis2 = len2;
					j = i;
				}
			}
			BOOST_ASSERT(j < 8);

			//这里无需递归

			/*
			找到邻居节点的那个距离最短的孩子节点了，接下来再判定该孩子节点的哪个面。
			neighbor_c是neighbor_p的某个孩子节点。
			*/
			auto neighbor_c = neighbor_p->mChildren[j];

			uint8 k{ 6 };
			for (uint8 i = 0; i < 6; ++i)
			{
				//该孩子节点的某个面的邻居，必然是当前节点的父节点
				if (neighbor_c->mNeighbors[i] == mParent)
				{
					k = i;
					break;
				}
			}
			BOOST_ASSERT(k < 6);

			//两边都进行更新
			mNeighbors[faceIndex] = neighbor_c;
			neighbor_c->mNeighbors[k] = this;
		}
	}

	OctreeNode * OctreeNode::findProbablyNode(Vector3 const & worldPosition, int32 proDepth)
	{
		//如果可能的深度值大于当前节点的深度值
		if (proDepth > mDepth)
		{
			//判断坐标应该位于当前节点的哪个孩子节点

			//让对象位于当前节点的坐标系中
			auto objectCurrentNodePosition = Vector3(worldPosition.x - mCenter.x,
				worldPosition.y - mCenter.y,
				worldPosition.z - mCenter.z);

			uint8 x{}, y{}, z{};
			if (objectCurrentNodePosition.x < 0.0)
			{
				x = 1;
			}
			if (objectCurrentNodePosition.y < 0.0)
			{
				y = 1;
			}
			if (objectCurrentNodePosition.z < 0.0)
			{
				z = 1;
			}
			uint8 childIndex = 4 * x + 2 * y + z;

			//如果当前节点没有孩子节点
			if (!hasChildren())
			{
				createChildren();
			}
			mChildren[childIndex]->findProbablyNode(worldPosition, proDepth);
		}

		//如果可能的深度值等于当前节点的深度值
		//一个点，肯定可以放置在当前节点
		return this;
	}

	OctreeNode * OctreeNode::pushImpl(StrongIObjectPtr objectPtr)
	{
		//找到合适的节点
		return findSuitableNode(objectPtr);
	}
}//namespace ung

#endif//UOT_HIERARCHICAL_COMPILE

/*
究竟让根节点是一个box呢？还是一个AABB呢？
box计算简单。
AABB看上去更舒服。
比如，有一个狭长型的地图，长宽比例为8:1，用AABB的话，就可以构建一个很合适的根节点，很好看。而，
用box的话，就会构建一个8:8的根节点，看上去很难看。但是用box构建的这个8:8的空间，到底浪费了什么？
浪费了内存！浪费了多少内存？没浪费多少，因为8:8，从中间劈1次就是4了，劈2次就是2了，劈3次就是1了。
劈了3次，每次创建8个孩子节点，那么就是浪费了24个孩子节点而已。
*/

/*
8个子节点编号：
0:右，上，前		索引：0	0	0
1:右，上，后		索引：0	0	1
2:右，下，前		索引：0	1	0
3:右，下，后		索引：0	1	1

4:左，上，前		索引：1	0	0
5:左，上，后		索引：1	0	1
6:左，下，前		索引：1	1	0
7:左，下，后		索引：1	1	1
*/

/*
计算8个子节点的中心坐标：
P:父节点的中心坐标
L:子节点的边长
HL:子节点边长的一半
plus = P + HL
minus = P - HL
0：P为左，后，下角	P.x + HL		P.y + HL		P.z + HL
1：P为左，前，下角	P.x + HL		P.y + HL		P.z - HL
2：P为左，后，上角	P.x + HL		P.y - HL		P.z + HL
3：P为左，前，上角	P.x + HL		P.y - HL		P.z - HL

4：P为右，后，下角	P.x - HL		P.y + HL		P.z + HL
5：P为右，前，下角	P.x - HL		P.y + HL		P.z - HL
6：P为右，后，上角	P.x - HL		P.y - HL		P.z + HL
7：P为右，前，上角	P.x - HL		P.y - HL		P.z - HL
*/

/*
一个box的6个面的索引：
0:右面
1:上面
2:前面
3:左面
4:下面
5:后面
*/