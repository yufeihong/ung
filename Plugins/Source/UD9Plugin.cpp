#include "UD9Plugin.h"

#ifdef UD9_HIERARCHICAL_COMPILE

#include "UD9RenderSystem.h"

#ifndef WIN32_LEAN_AND_MEAN
#if UNG_PLATFORM == UNG_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#if !defined(NOMINMAX) && defined(_MSC_VER)
#define NOMINMAX																					//required to stop windows.h messing up std::min
#endif
#include <windows.h>
#endif
#endif

namespace ung
{
	namespace
	{
		const char* pluginName = "plugin_UD9";
	}//namespace

	D9Plugin::D9Plugin() :
		mRenderSystem(nullptr)
	{
	}

	D9Plugin::~D9Plugin()
	{
		UNG_DEL(mRenderSystem);
	}

	const char* D9Plugin::getName() const
	{
		return pluginName;
	}

	/*
	HINSTANCE:A handle to an instance. This is the base address of the module in memory.
	*/
	void D9Plugin::install()
	{
		//hInst：为了创建窗口用
#if UNG_DEBUGMODE
		HINSTANCE hInst = GetModuleHandle(L"UD9_d.dll");
#else
		HINSTANCE hInst = GetModuleHandle(L"UD9.dll");
#endif

		//UNG_PRINT_ONE_LINE("D3D9插件已经安装。");

		//创建D3D9渲染系统
		UNG_LOG("Create D3D9 render system.");

		mRenderSystem = UNG_NEW D9RenderSystem(hInst);

		Root::getInstance().addRenderSystem(pluginName, mRenderSystem);
	}

	void D9Plugin::uninstall()
	{
		Root::getInstance().removeRenderSystem(pluginName);

		//UNG_PRINT_ONE_LINE("D3D9插件已经卸载。");
	}
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE