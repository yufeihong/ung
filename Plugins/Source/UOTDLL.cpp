#include "UOTDLL.h"

#ifdef UOT_HIERARCHICAL_COMPILE

#include "UOTPlugin.h"
#include "UFCPluginManager.h"

namespace ung
{
	static OCTPlugin* plugin{};

	void dllCreatePlugin() noexcept
	{
		plugin = UNG_NEW OCTPlugin;

		//UNG_PRINT_ONE_LINE("八叉树插件已经创建。");

		PluginManager::getInstance().install(plugin);
	}

	void dllDestroyPlugin()
	{
		PluginManager::getInstance().uninstall(plugin);
		UNG_DEL(plugin);

		//UNG_PRINT_ONE_LINE("八叉树插件已经销毁。");
	}
}//namespace ung

#endif//UOT_HIERARCHICAL_COMPILE