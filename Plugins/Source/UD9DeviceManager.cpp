#include "UD9DeviceManager.h"

#ifdef UD9_HIERARCHICAL_COMPILE

#include "UD9RenderSystem.h"
#include "UD9Adapter.h"
#include "UD9AdapterManager.h"
#include "UD9Device.h"
#include "UD9Window.h"

namespace ung
{
	D9DeviceManager::D9DeviceManager() :
		mActiveDevice(nullptr)
	{
	}

	D9DeviceManager::~D9DeviceManager()
	{
		BOOST_ASSERT(D9RenderSystem::getInstance()->getIDirect3D9());

		BOOST_ASSERT(D9RenderSystem::getInstance()->getD9DeviceManager());

		mDevices.deleteAll();
		mDevices.clear();
	}

	D9Device* D9DeviceManager::selectDevice(D9Window* window)
	{
		D9Device* device = nullptr;
		D3DDEVTYPE deviceType = D3DDEVTYPE_HAL;

		//渲染系统
		auto pRS = D9RenderSystem::getInstance();

		//适配器管理器
		auto pAdapterManager = pRS->getD9AdapterManager();

		//参数所指定的窗口所归属的显示器句柄
		HMONITOR windowMonitor = window->getMonitor();

		//找到与显示器句柄所匹配的适配器
		D9Adapter* pAdapter = pAdapterManager->getAdapterFromMonitor(windowMonitor);

		//适配器的序号
		auto oridinalNumber = pAdapter->getOridinalNumber();

		//在已经创建的抽象设备中进行匹配
		auto beg = mDevices.begin();
		auto end = mDevices.end();
		while (beg != end)
		{
			auto pDevice = *beg;
			auto oridinalNum = pDevice->getAdapterOridinalNumber();
			auto type = pDevice->getType();
			auto isFullScreen = pDevice->isUsedInFullScreen();

			if (oridinalNum == oridinalNumber && type == deviceType && isFullScreen == window->isFullScreen())
			{
				device = pDevice;

				break;
			}

			++beg;
		}

		//没有找到
		if (!device)
		{
			device = UNG_NEW D9Device(deviceType,oridinalNumber,windowMonitor);
			mDevices.put(device);

			if (!mActiveDevice)
			{
				mActiveDevice = device;
			}
		}

		//互相关联
		window->setDevice(device);

		device->attachRenderWindow(window);

		//Update shared focus window handle.
		auto sfw = device->getSharedFocusWindow();
		if (window->isFullScreen() && oridinalNumber == D3DADAPTER_DEFAULT && !sfw)
		{
			device->setSharedFocusWindow(window->getHandle());
		}

		device->buildPresentParameters(window);

		device->create();

		pRS->setActiveDevice(mActiveDevice->getDevice());

		pRS->setDefaultRenderStates();

		return device;
	}

	D9Device* D9DeviceManager::getActiveDevice() const
	{
		return mActiveDevice;
	}

	uint32 D9DeviceManager::getDevicesCount() const
	{
		return mDevices.size();
	}

	typename D9DeviceManager::devices_type const & D9DeviceManager::getAllDevices() const
	{
		return mDevices;
	}

	typename D9DeviceManager::devices_const_its D9DeviceManager::getDevicesIterators() const
	{
		return std::make_pair(mDevices.cbegin(), mDevices.cend());
	}
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE

/*
Selecting a Device (Direct3D 9):
Applications can query hardware to detect the supported Direct3D device types. This section 
contains information about the primary tasks involved in enumerating display adapters and 
selecting Direct3D devices.
An application must perform a series of tasks to select an appropriate Direct3D device. Note 
that the following steps are intended for a full-screen application and that, in most cases, a 
windowed application can skip most of these steps.
1,Initially, the application must enumerate the display adapters on the system. An adapter is a 
physical piece of hardware. Note that the graphics card might contain more than a single 
adapter, as is the case with a dual-headed display. Applications that are not concerned with 
multi-monitor support can disregard this step, and pass D3DADAPTER_DEFAULT to the 
IDirect3D9::EnumAdapterModes method in step 2.
2,For each adapter, the application enumerates the supported display modes by calling 
IDirect3D9::EnumAdapterModes.
3,If required, the application checks for the presence of hardware acceleration in each 
enumerated display mode by calling IDirect3D9::CheckDeviceType, as shown in the following 
code example. Note that this is only one of the possible uses for IDirect3D9::CheckDeviceType.
4,The application checks for the desired level of functionality for the device on this adapter by 
calling the IDirect3D9::GetDeviceCaps method. The capability returned by this method is 
guaranteed to be constant for a device across all display modes, verified by 
IDirect3D9::CheckDeviceType.
5,Devices can always render to surfaces of the format of an enumerated display mode that is 
supported by the device. If the application is required to render to a surface of a different 
format, it can call IDirect3D9::CheckDeviceFormat. If the device can render to the format, then 
it is guaranteed that all capabilities returned by IDirect3D9::GetDeviceCaps are applicable.
6,Lastly, the application can determine if multisampling techniques, such as full-scene 
antialiasing, are supported for a render format by using the 
IDirect3D9::CheckDeviceMultiSampleType method.
After completing the preceding steps, the application should have a list of display modes in 
which it can operate. The final step is to verify that enough device-accessible memory is 
available to accommodate the required number of buffers and antialiasing. This test is 
necessary because the memory consumption for the mode and multisample combination is 
impossible to predict without verifying it. Moreover, some display adapter architectures might 
not have a constant amount of device-accessible memory. This means that an application 
should be able to report out-of-video-memory failures when going into full-screen mode.Typically,
an application should remove full-screen mode from the list of modes that it offers to a user, or 
it should attempt to consume less memory by reducing the number of back buffers or by using 
a less complex multisampling technique.

A windowed application performs a similar set of tasks.
1,It determines the desktop rectangle covered by the client area of the window.
2,It enumerates adapters, looking for the adapter whose monitor covers the client area. If the 
client area is owned by more than one adapter, then the application can choose to drive each 
adapter independently, or to drive a single adapter and have Direct3D transfer pixels from one 
device to another at presentation. The application can also disregard two preceding steps and 
use the D3DADAPTER_DEFAULT adapter. Note that this might result in slower operation when 
the window is placed on a secondary monitor.
3,The application should call IDirect3D9::CheckDeviceType to determine if the device can 
support rendering to a back buffer of the specified format while in desktop mode.
IDirect3D9::GetAdapterDisplayMode can be used to determine the desktop display format.

Determining Hardware Support (Direct3D 9):
Direct3D provides the following functions to determine hardware support.
IDirect3D9::CheckDeviceFormat:
Used to verify whether a surface format can be used as a texture, whether a surface format can 
be used as a texture and a render target, or whether a surface format can be used as a 
depth-stencil buffer. In addition, this method is used to verify depth buffer format support and 
depth-stencil buffer format support.
IDirect3D9::CheckDeviceType:
Used to verify a device's ability to perform hardware acceleration, a device's ability to build a 
swap chain for presentation, or a device's ability to render to the current display format.
IDirect3D9::CheckDepthStencilMatch:
Used to verify whether a depth-stencil buffer format is compatible with a render-target format.
Note that before calling this method, the application should call IDirect3D9::CheckDeviceFormat 
on both the depth-stencil and render-target formats.
*/