#include "UD9DisplayMode.h"

#ifdef UD9_HIERARCHICAL_COMPILE

#include "UFCSTLWrapper.h"
#include "UD9RenderSystem.h"
#include "UD9Adapter.h"

namespace ung
{
	D9DisplayMode::D9DisplayMode()
	{
		SecureZeroMemory(&mDisplayMode, sizeof(mDisplayMode));
	}

	D9DisplayMode::D9DisplayMode(D3DDISPLAYMODE const & dm) :
		mDisplayMode(dm)
	{
	}

	D9DisplayMode::~D9DisplayMode()
	{
	}

	uint32 D9DisplayMode::getWidth() const
	{
		return mDisplayMode.Width;
	}

	uint32 D9DisplayMode::getHeight() const
	{
		return mDisplayMode.Height;
	}

	D3DFORMAT D9DisplayMode::getPixelFormat() const
	{
		return mDisplayMode.Format;
	}

	uint32 D9DisplayMode::getRefreshRate() const
	{
		return mDisplayMode.RefreshRate;
	}

	uint32 D9DisplayMode::getColorDepth() const
	{
		uint32 colorDepth = 16;
		if (mDisplayMode.Format == D3DFMT_X8R8G8B8 || mDisplayMode.Format == D3DFMT_A8R8G8B8 || 
			mDisplayMode.Format == D3DFMT_R8G8B8)
		{
			colorDepth = 32;								//����colorDepth�͵���16
		}

		return colorDepth;
	}

	D3DDISPLAYMODE const & D9DisplayMode::getD3DDisplayMode() const
	{
		return mDisplayMode;
	}

	void D9DisplayMode::promoteRefreshRate(uint32 rr)
	{
		mDisplayMode.RefreshRate = rr;
	}

	//-------------------------------------------------------------------------------

	D9DisplayModeCollection::D9DisplayModeCollection(D9Adapter * ap) :
		mAdapter(ap)
	{
		BOOST_ASSERT(ap);
	}

	D9DisplayModeCollection::~D9DisplayModeCollection()
	{
		mAdapter = nullptr;

		auto lambdaExpress = [](D9DisplayMode*& pDisplayMode)
		{
			UNG_DEL(pDisplayMode);
		};
		ungContainerLoop(mCollection, lambdaExpress);

		ungContainerClear(mCollection);
	}

	bool D9DisplayModeCollection::enumerateDisplayMode()
	{
		auto pD9 = D9RenderSystem::getInstance()->getIDirect3D9();

		UINT modeIndex;
		D3DFORMAT fm = D3DFMT_X8R8G8B8;

		UINT modeCount = mAdapter->getDisplayModeCount(fm);
		
		for (modeIndex = 0; modeIndex < modeCount; modeIndex++)
		{
			D3DDISPLAYMODE displayMode;
			mAdapter->getD3DDisplayMode(fm,modeIndex,&displayMode);

			//Filter out low-resolutions
			if (displayMode.Width < 800 || displayMode.Height < 600)
			{
				continue;
			}

			//Check to see if it is already in the list (to filter out refresh rates)
			bool found = false;

			auto beg = mCollection.begin();
			auto end = mCollection.end();
			while (beg != end)
			{
				D3DDISPLAYMODE oldDisp = (*beg)->getD3DDisplayMode();

				if (oldDisp.Width == displayMode.Width && oldDisp.Height == displayMode.Height && 
						oldDisp.Format == displayMode.Format)
				{
					//Check refresh rate and favour higher if poss
					if (oldDisp.RefreshRate < displayMode.RefreshRate)
					{
						(*beg)->promoteRefreshRate(displayMode.RefreshRate);
					}

					found = true;
					break;
				}

				++beg;
			}

			if (!found)
			{
				mCollection.push_back(UNG_NEW D9DisplayMode(displayMode));
			}
		}

		return true;
	}

	D9DisplayMode* D9DisplayModeCollection::getDisplayMode(uint32 width, uint32 height, D3DFORMAT pixelFormat)
	{
		D9DisplayMode* ret = nullptr;

		for (auto const& dm : mCollection)
		{
			auto w = dm->getWidth();
			auto h = dm->getHeight();
			auto fm = dm->getPixelFormat();

			if (w == width && h == height && fm == pixelFormat)
			{
				ret = dm;

				break;
			}
		}

		return ret;
	}

	uint32 D9DisplayModeCollection::getCount() const
	{
		return mCollection.size();
	}

	STL_VECTOR(D9DisplayMode *) const& D9DisplayModeCollection::getCollection() const
	{
		return mCollection;
	}
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE

/*
typedef struct D3DDISPLAYMODE
{
	UINT					Width;
	UINT					Height;
	UINT					RefreshRate;
	D3DFORMAT		Format;
}D3DDISPLAYMODE,*LPD3DDISPLAYMODE;
Describes the display mode.
Width:
Screen width,in pixels.
Height:
Screen height,in pixels.
RefreshRate:
Refresh rate.The value of 0 indicates an adapter default.
Format:
Member of the D3DFORMAT enumerated type,describing the surface format of the display mode.
*/