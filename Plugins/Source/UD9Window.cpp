#include "UD9Window.h"

#ifdef UD9_HIERARCHICAL_COMPILE

#include "URMWindowEventUtilities.h"
#include "UD9RenderSystem.h"
#include "UD9Adapter.h"
#include "UD9AdapterManager.h"
#include "UD9Device.h"
#include "UD9DisplayMode.h"

#pragma warning(push)
#pragma warning(disable : 4800)

namespace ung
{
	D9Window::D9Window(String const & title, bool isFullScreen, uint32 width, uint32 height,bool primary,HINSTANCE inst) :
		mInstance(inst),
		mClassName(nullptr),
		mTitle(title),
		mIsFullScreen(isFullScreen),
		mWidth(isFullScreen ? 0 : width),
		mHeight(isFullScreen ? 0 : height),
		mLastWidth(width),
		mLastHeight(height),
		mDepthBuffer(nullptr),
		mBackBuffer(nullptr),
		mSwapChain(nullptr),
		mIsPrimary(primary),
		mDevice(nullptr),
		//D3D9在窗口模式下开启垂直同步，否则帧速率会受限
		mVerticalSync(!isFullScreen ? true : false),
		mFullScreenDisplayMode(nullptr),
		/*
		D3DFMT_UNKNOWN can be specified for the BackBufferFormat while in windowed mode.
		This tells the runtime to use the current desktop display-mode format and eliminates the 
		need to call GetDisplayMode.
		For windowed applications, the back buffer format no longer needs to match the 
		display-mode format because color conversion can now be done by the hardware (if the 
		hardware supports color conversion).The set of possible back buffer formats is constrained,
		but the runtime will allow any valid back buffer format to be presented to any desktop 
		format.(There is the additional requirement that the device be operable in the desktop;
		devices typically do not operate in 8 bits per pixel modes.)
		Full-screen applications cannot do color conversion.
		D3DFMT_UNKNOWN,This means the application does not have to query the current 
		desktop format before calling CreateDevice for windowed mode.For full-screen mode,the 
		back buffer format must be specified.
		*/
		mPixelFormat(!isFullScreen ? D3DFMT_UNKNOWN : D3DFMT_R5G6B5),
		mDepthStencilFormat(D3DFMT_D16),
		//全屏模式下，也初始化为boost::logic::indeterminate
		mState(boost::logic::indeterminate)
	{
		String str = UniqueID().toString();
		auto ret = LexicalCast::convert(str,mClassName);
		BOOST_ASSERT(ret);

		buildStyle();

		//保持下面三个函数的调用顺序
		_selectDepthStencilFormat();

		_selectWindowedFormat();

		_selectFullScreenDisplayMode();

		if (mIsFullScreen)
		{
			mWidth = mFullScreenDisplayMode->getWidth();
			mHeight = mFullScreenDisplayMode->getHeight();
		}
	}

	D9Window::~D9Window()
	{
		delete[] mClassName;

		UD9_RELEASE(mDepthBuffer);
		UD9_RELEASE(mBackBuffer);
		UD9_RELEASE(mSwapChain);
	}

	wchar_t const * D9Window::getClassName() const
	{
		return mClassName;
	}

	HWND D9Window::getHandle() const
	{
		return mHWND;
	}

	String const& D9Window::getTitle() const
	{
		return mTitle;
	}

	bool D9Window::isFullScreen() const
	{
		return mIsFullScreen;
	}

	bool D9Window::isVerticalSync() const
	{
		return mVerticalSync;
	}

	uint32 D9Window::getWidth() const
	{
		return mWidth;
	}

	uint32 D9Window::getHeight() const
	{
		return mHeight;
	}

	void D9Window::setWidth(uint32 newWidth)
	{
		//缩小窗口时，通过解析消息参数所得到的宽和高都是0。
		if (newWidth > 0)
		{
			mWidth = newWidth;
		}
	}

	void D9Window::setHeight(uint32 newHeight)
	{
		if (newHeight > 0)
		{
			mHeight = newHeight;
		}
	}

	uint32 D9Window::getLastWidth() const
	{
		return mLastWidth;
	}

	uint32 D9Window::getLastHeight() const
	{
		return mLastHeight;
	}

	void D9Window::updateLastWH()
	{
		mLastWidth = mWidth;
		mLastHeight = mHeight;
	}

	void D9Window::buildStyle()
	{
		mFullScreenStyle = WS_CLIPCHILDREN | WS_POPUP | WS_VISIBLE;
		mWindowedStyle = WS_CLIPCHILDREN | WS_OVERLAPPEDWINDOW | WS_VISIBLE;
	}

	uint32 D9Window::getFullScreenStyle() const
	{
		return mFullScreenStyle;
	}

	uint32 D9Window::getWindowedStyle() const
	{
		return mWindowedStyle;
	}

	uint32 D9Window::getStyle() const
	{
		return mIsFullScreen ? mFullScreenStyle : mWindowedStyle;
	}

	HMONITOR D9Window::getMonitor() const
	{
		BOOST_ASSERT(mMonitorToWhichWindowBelongs);

		return mMonitorToWhichWindowBelongs;
	}

	bool D9Window::isPrimary() const
	{
		return mIsPrimary;
	}

	bool D9Window::create()
	{
		//显示器句柄
		HMONITOR hMonitor = _findMonitorFromDefaultAdapter();

		//显示器信息
		MONITORINFO monitorInfo = _getMonitorInfo(hMonitor);

		//左上角
		uint32 left = INT_MAX;
		uint32 top = INT_MAX;

		//期望的窗口宽高(窗口的整体大小)
		uint32 desiredWinWidth = 0;
		uint32 desiredWinHeight = 0;

		//计算窗口的锚点和大小
		_calWindowPositionAndSize(monitorInfo.rcWork,left,top,desiredWinWidth,desiredWinHeight);

		//注册窗口类
		_registerWindowClass();

		//创建窗口
		_createWindow(left,top,desiredWinWidth,desiredWinHeight);

		//从已经创建的窗口来获取该窗口所归属的显示器句柄
		mMonitorToWhichWindowBelongs = _findMonitorFromWindow();
		BOOST_ASSERT(mMonitorToWhichWindowBelongs == hMonitor);

		return true;
	}

	void D9Window::destroy()
	{
		if (mHWND)
		{
			//不要在这里注销窗口类。

			mHWND = 0;
		}
	}

	void D9Window::setMinMaxState(boost::tribool state)
	{
		BOOST_ASSERT(!mIsFullScreen);

		mState = state;
	}

	boost::tribool D9Window::getMinMaxState() const
	{
		BOOST_ASSERT(!mIsFullScreen);

		return mState;
	}

	bool D9Window::isNormalState() const
	{
		return boost::indeterminate(mState);
	}

	void D9Window::setDevice(D9Device* dv)
	{
		BOOST_ASSERT(dv);

		mDevice = dv;
	}

	void D9Window::present()
	{
		/*
		Presents the contents of the next buffer in the sequence of back buffers owned by the 
		swap chain.
		HRESULT Present(const RECT *pSourceRect,const RECT *pDestRect,
		HWND hDestWindowOverride,const RGNDATA *pDirtyRegion,DWORD dwFlags);
		pSourceRect:
		Pointer to the source rectangle.Use NULL to present the entire surface.This value must be 
		NULL unless the swap chain was created with D3DSWAPEFFECT_COPY.If the rectangle 
		exceeds the source surface, the rectangle is clipped to the source surface.
		pDestRect:
		Pointer to the destination rectangle in client coordinates.This value must be NULL unless 
		the swap chain was created with D3DSWAPEFFECT_COPY.Use NULL to fill the entire client 
		area.If the rectangle exceeds the destination client area,the rectangle is clipped to the 
		destination client area.
		hDestWindowOverride:
		Destination window whose client area is taken as the target for this presentation.If this 
		value is NULL,the runtime uses the hDeviceWindow member of 
		D3DPRESENT_PARAMETERS for the presentation.
		pDirtyRegion:
		This value must be NULL unless the swap chain was created with D3DSWAPEFFECT_COPY.
		See Flipping Surfaces (Direct3D 9).If this value is non-NULL,the contained region is 
		expressed in back buffer coordinates.The rectangles within the region are the minimal set 
		of pixels that need to be updated.This method takes these rectangles into account when 
		optimizing the presentation by copying only the pixels within the region,or some suitably 
		expanded set of rectangles.This is an aid to optimization only,and the application should 
		not rely on the region being copied exactly.The implementation may choose to copy the 
		whole source rectangle.
		dwFlags:
		Allows the application to request that the method return immediately when the driver 
		reports that it cannot schedule a presentation.Valid values are 0,or any combination of 
		D3DPRESENT_DONOTWAIT or D3DPRESENT_LINEAR_CONTENT.
		If dwFlags = 0,this method behaves as it did prior to Direct3D 9.Present will spin until the 
		hardware is free,without returning an error.
		If dwFlags = D3DPRESENT_DONOTWAIT,and the hardware is busy processing or waiting 
		for a vertical sync interval,the method will return D3DERR_WASSTILLDRAWING.
		If dwFlags = D3DPRESENT_LINEAR_CONTENT,gamma correction is performed from linear 
		space to sRGB for windowed swap chains.This flag will take effect only when the driver 
		exposes D3DCAPS3_LINEAR_TO_SRGB_PRESENTATION (see Gamma (Direct3D 9)).
		Appliations should specify this flag if the backbuffer format is 16-bit floating point in order 
		to match windowed mode present to fullscreen gamma behavior.
		Present will fail if called between BeginScene and EndScene pairs unless the render target 
		is not the current render target (such as the back buffer you get from creating an 
		additional swap chain). This is a new behavior for Direct3D 9.
		*/
		auto presentRet = mSwapChain->Present(NULL, NULL, NULL, NULL,0);

		BOOST_ASSERT(presentRet == D3D_OK);
	}

	D9DisplayMode const * D9Window::getFullScreenDisplayMode() const
	{
		BOOST_ASSERT(mIsFullScreen);
		BOOST_ASSERT(mFullScreenDisplayMode);

		return mFullScreenDisplayMode;
	}

	D3DFORMAT D9Window::getPixelFormat() const
	{
		return mPixelFormat;
	}

	D3DFORMAT D9Window::getDepthStencilFormat() const
	{
		return mDepthStencilFormat;
	}

	HMONITOR D9Window::_findMonitorFromDefaultAdapter()
	{
		HMONITOR hMonitor = 0;

		//D3D9接口对象
		auto pD9 = D9RenderSystem::getIDirect3D9();

		/*
		显示器句柄。typedef HANDLE HMONITOR
		HMONITOR GetAdapterMonitor(UINT Adapter);
		Returns the handle of the monitor associated with the Direct3D object.
		Adapter:
		Type:UINT
		Ordinal number that denotes the display adapter.D3DADAPTER_DEFAULT is always the 
		primary display adapter.
		*/
		hMonitor = pD9->GetAdapterMonitor(D3DADAPTER_DEFAULT);
		BOOST_ASSERT(hMonitor);

		return hMonitor;
	}

	HMONITOR D9Window::_findMonitorFromWindow()
	{
		/*
		HMONITOR MonitorFromWindow(HWND hwnd,DWORD dwFlags);
		The MonitorFromWindow function retrieves a handle to the display monitor that has the 
		largest area of intersection with the bounding rectangle of a specified window.
		If the window intersects one or more display monitor rectangles, the return value is an 
		HMONITOR handle to the display monitor that has the largest area of intersection with the 
		window.
		If the window does not intersect a display monitor, the return value depends on the value 
		of dwFlags.
		If the window is currently minimized, MonitorFromWindow uses the rectangle of the 
		window before it was minimized.
		hwnd:
		A handle to the window of interest.
		dwFlags:
		Determines the function's return value if the window does not intersect any display 
		monitor.
		This parameter can be one of the following values.
		MONITOR_DEFAULTTONEAREST
		Returns a handle to the display monitor that is nearest to the window.
		MONITOR_DEFAULTTONULL
		Returns NULL.
		MONITOR_DEFAULTTOPRIMARY
		Returns a handle to the primary display monitor.
		*/
		mMonitorToWhichWindowBelongs = MonitorFromWindow(mHWND,MONITOR_DEFAULTTONEAREST);

		return mMonitorToWhichWindowBelongs;
	}

	MONITORINFO D9Window::_getMonitorInfo(HMONITOR hMonitor)
	{
		BOOST_ASSERT(hMonitor);

		/*
		typedef struct tagMONITORINFO
		{
			DWORD			cbSize;
			RECT				rcMonitor;
			RECT				rcWork;
			DWORD			dwFlags;
		}MONITORINFO,*LPMONITORINFO;
		The MONITORINFO structure contains information about a display monitor.
		cbSize:
		The size of the structure,in bytes.
		Set this member to sizeof ( MONITORINFO ) before calling the GetMonitorInfo function.
		Doing so lets the function determine the type of structure you are passing to it.
		rcMonitor:
		A RECT structure that specifies the display monitor rectangle, expressed in virtual-screen 
		coordinates. Note that if the monitor is not the primary display monitor, some of the 
		rectangle's coordinates may be negative values.
		rcWork:
		A RECT structure that specifies the work area rectangle of the display monitor, expressed 
		in virtual-screen coordinates. Note that if the monitor is not the primary display monitor,
		some of the rectangle's coordinates may be negative values.
		dwFlags:
		A set of flags that represent attributes of the display monitor.
		The following flag is defined.
		Value										Meaning
		MONITORINFOF_PRIMARY		This is the primary display monitor.
		*/
		MONITORINFO monitorInfo;
		SecureZeroMemory(&monitorInfo, sizeof(monitorInfo));
		monitorInfo.cbSize = sizeof(monitorInfo);
		monitorInfo.dwFlags = MONITORINFOF_PRIMARY;

		/*
		BOOL GetMonitorInfo(HMONITOR hMonitor,LPMONITORINFO lpmi);
		Retrieves information about a display monitor.
		hMonitor:
		A handle to the display monitor of interest.
		lpmi:
		A pointer to a MONITORINFO or MONITORINFOEX structure that receives information 
		about the specified display monitor.
		*/
		auto monitorInfoRet = GetMonitorInfo(hMonitor,&monitorInfo);
		BOOST_ASSERT(monitorInfoRet);

		return monitorInfo;
	}

	uint32 D9Window::_getAdapterOridinalNumberFromMonitor()
	{
		//渲染系统
		auto pRS = D9RenderSystem::getInstance();

		//适配器管理器
		auto pAM = pRS->getD9AdapterManager();

		//显示器句柄
		auto hMonitor = _findMonitorFromDefaultAdapter();

		//适配器
		auto pAdapter = pAM->getAdapterFromMonitor(hMonitor);

		//适配器序号
		auto oridinalNumber = pAdapter->getOridinalNumber();

		return oridinalNumber;
	}

	void D9Window::_calWindowPositionAndSize(RECT workAreaRectangle, uint32 & left, uint32 & top, uint32 & desiredWidth, uint32 & desiredHeight)
	{
		if (mIsFullScreen)
		{
			left = 0;
			top = 0;

			desiredWidth = mWidth;
			desiredHeight = mHeight;

			return;
		}

		//工作区域矩形
		auto rcwLeft = workAreaRectangle.left;
		auto rcwRight = workAreaRectangle.right;
		auto rcwTop = workAreaRectangle.top;
		auto rcwBottom = workAreaRectangle.bottom;

		//屏幕宽高(工作区域，高比屏幕的分辨率数值略小)
		uint32 screenWidth = rcwRight - rcwLeft;
		uint32 screenHeight = rcwBottom - rcwTop;

		//客户区域宽高
		uint32 clientWidth = std::min(mWidth,screenWidth);
		uint32 clientHeight = std::min(mHeight,screenHeight);

		left = rcwLeft + (screenWidth - clientWidth) / 2;
		top = rcwTop + (screenHeight - clientHeight) / 2;

		RECT rc;
		SetRect(&rc, 0, 0, clientWidth, clientHeight);
		/*
		BOOL WINAPI AdjustWindowRect(LPRECT lpRect,DWORD dwStyle,BOOL bMenu);
		Calculates the required size of the window rectangle, based on the desired client-rectangle 
		size. The window rectangle can then be passed to the CreateWindow function to create a 
		window whose client area is the desired size.
		To specify an extended window style, use the AdjustWindowRectEx function.
		A client rectangle is the smallest rectangle that completely encloses a client area. A 
		window rectangle is the smallest rectangle that completely encloses the window, which 
		includes the client area and the nonclient area.
		The AdjustWindowRect function does not add extra space when a menu bar wraps to two 
		or more rows.
		The AdjustWindowRect function does not take the WS_VSCROLL or WS_HSCROLL styles 
		into account. To account for the scroll bars, call the GetSystemMetrics function with 
		SM_CXVSCROLL or SM_CYHSCROLL.
		lpRect:
		Type:LPRECT
		A pointer to a RECT structure that contains the coordinates of the top-left and 
		bottom-right corners of the desired client area. When the function returns, the structure 
		contains the coordinates of the top-left and bottom-right corners of the window to 
		accommodate(容纳) the desired client area.
		dwStyle:
		Type:DWORD
		The window style of the window whose required size is to be calculated. Note that you 
		cannot specify the WS_OVERLAPPED style.(可以用WS_OVERLAPPEDWINDOW)
		bMenu:
		Type:BOOL
		Indicates whether the window has a menu.
		*/
		AdjustWindowRect(&rc, getStyle(), false);
		//期望的窗口宽高(窗口整体宽高)
		desiredWidth = rc.right - rc.left;
		desiredHeight = rc.bottom - rc.top;
	}

	void D9Window::_registerWindowClass()
	{
		HINSTANCE hInst = mInstance;

		/*
		CS_OWNDC:
		Allocates a unique device context for each window in the class.
		CS_HREDRAW | CS_VREDRAW:
		当水平方向或垂直方向尺寸改变时，要完全刷新窗口。
		*/
		UINT windowClassStyle = 0;

		/*
		窗口是在窗口类的基础上创建的，可以在单个窗口类的基础上创建多个窗口。
		窗口类定义了窗口过程和基于此类的窗口的其它一些特征。
		*/
		WNDCLASS windowClass;
		memset(&windowClass, 0, sizeof(WNDCLASS));

		windowClass.style = windowClassStyle;
		/*
		Associating a Window Procedure with a Window Class
		这个窗口过程将处理基于此类创建的所有窗口的全部消息。
		*/
		windowClass.lpfnWndProc = WindowEventUtilities::windowProc;
		//获得目前运行的程序
		windowClass.hInstance = hInst;									//GetModuleHandle(NULL);
		//使用默认的鼠标
		windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
		//图标
		windowClass.hIcon = LoadIcon(NULL, IDI_WINLOGO);
		/*
		窗口的背景画刷都无关紧要，因为在DirectX接管之后，它将不起作用。
		*/
		windowClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
		windowClass.lpszClassName = mClassName;
		windowClass.lpszMenuName = NULL;
		/*
		在类结构和Windows内部保存的窗口结构中不预留额外空间。
		*/
		windowClass.cbClsExtra = 0;
		windowClass.cbWndExtra = 0;

		/*
		在创建窗口之前，必须首先向操作系统注册一个窗口类
		Registers a window class for subsequent(后来的) use in calls to the CreateWindow or 
		CreateWindowEx function.
		The RegisterClass function has been superseded(取代) by the RegisterClassEx function.
		You can still use RegisterClass, however, if you do not need to set the class small icon.
		ATOM WINAPI RegisterClass(const WNDCLASS *lpWndClass);
		If the function succeeds, the return value is a class atom that uniquely identifies the class 
		being registered. This atom can only be used by the CreateWindow,CreateWindowEx,
		GetClassInfo,GetClassInfoEx,FindWindow,FindWindowEx,and UnregisterClass functions 
		and the IActiveIMMap::FilterClientWindows method.
		If the function fails, the return value is zero.
		If you register the window class by using RegisterClassA, the application tells the system 
		that the windows of the created class expect messages with text or character parameters 
		to use the ANSI character set; if you register it by using RegisterClassW, the application 
		requests that the system pass text parameters of messages as Unicode.The 
		IsWindowUnicode function enables applications to query the nature of each window.For 
		more information on ANSI and Unicode functions,see Conventions for Function Prototypes.
		All window classes that an application registers are unregistered when it terminates.
		No window classes registered by a DLL are unregistered when the DLL is unloaded.A DLL 
		must explicitly unregister its classes when it is unloaded.
		*/
		if (RegisterClass(&windowClass) == 0)
		{
			UNG_EXCEPTION("Window registration failed.");
		}

		/*
		Displays or hides the cursor.
		int WINAPI ShowCursor(BOOL bShow);
		bShow:
		If bShow is TRUE, the display count is incremented by one. If bShow is FALSE, the display 
		count is decremented by one.
		The return value specifies the new display counter.
		This function sets an internal display counter that determines whether the cursor should 
		be displayed.The cursor is displayed only if the display count is greater than or equal to 0.
		If a mouse is installed, the initial display count is 0.If no mouse is installed,the display 
		count is –1.
		*/
		ShowCursor(true);
	}

	void D9Window::_createWindow(uint32 left,uint32 top,uint32 desiredWidth,uint32 desiredHeight)
	{
		wchar_t* pTitle = nullptr;
		LexicalCast::convert(mTitle, pTitle);
		wchar_t windowName[64];
		auto copyRet = wcscpy_s(windowName, pTitle);
		delete[] pTitle;
		pTitle = nullptr;
		BOOST_ASSERT(copyRet == 0);

		HINSTANCE hInst = mInstance;

		//Window Extended Style
		DWORD extStyle = 0;
		if (mIsFullScreen)
		{
			/*
			WS_EX_TOPMOST:
			The window should be placed above all non-topmost windows and should stay above 
			them, even when the window is deactivated. To add or remove this style, use the 
			SetWindowPos function.
			*/
			extStyle |= WS_EX_TOPMOST;
		}

		/*
		创建窗口
		The CreateWindowEx function sends WM_NCCREATE, WM_NCCALCSIZE, and WM_CREATE 
		messages to the window being created.
		If the created window is a child window, its default position is at the bottom of the Z-order.
		If the created window is a top-level window, its default position is at the top of the Z-order 
		(but beneath all topmost windows unless the created window is itself topmost).

		mClassName:把正在创建的窗口与一个窗口类相连接。

		Creates an overlapped,pop-up,or child window with an extended window style;otherwise,
		this function is identical to the CreateWindow function.For more information about 
		creating a window and for full descriptions of the other parameters of CreateWindowEx,
		see CreateWindow.
		HWND WINAPI CreateWindowEx(DWORD dwExStyle,LPCTSTR lpClassName,
		LPCTSTR lpWindowName,DWORD dwStyle,int x,int y,int nWidth,int nHeight,
		HWND hWndParent,HMENU hMenu,HINSTANCE hInstance,LPVOID lpParam);
		dwExStyle:
		The extended window style of the window being created.For a list of possible values,see 
		Extended Window Styles.
		lpClassName:
		A null-terminated string or a class atom created by a previous call to the RegisterClass or 
		RegisterClassEx function.The atom must be in the low-order word of lpClassName;the 
		high-order word must be zero.If lpClassName is a string,it specifies the window class 
		name.The class name can be any name registered with RegisterClass or RegisterClassEx,
		provided that the module that registers the class is also the module that creates the 
		window.The class name can also be any of the predefined system class names.
		lpWindowName:
		The window name.If the window style specifies a title bar,the window title pointed to by 
		lpWindowName is displayed in the title bar.When using CreateWindow to create controls,
		such as buttons,check boxes,and static controls,use lpWindowName to specify the text of 
		the control.When creating a static control with the SS_ICON style,use lpWindowName to 
		specify the icon name or identifier.To specify an identifier,use the syntax "#num".
		dwStyle:
		The style of the window being created.This parameter can be a combination of the window 
		style values,plus the control styles indicated in the Remarks section.
		x:
		The initial horizontal position of the window.For an overlapped or pop-up window,the x 
		parameter is the initial x-coordinate of the window's upper-left corner,in screen 
		coordinates.For a child window,x is the x-coordinate of the upper-left corner of the 
		window relative to the upper-left corner of the parent window's client area.If x is set to 
		CW_USEDEFAULT,the system selects the default position for the window's upper-left 
		corner and ignores the y parameter.CW_USEDEFAULT is valid only for overlapped 
		windows;if it is specified for a pop-up or child window,the x and y parameters are set to 
		zero.
		y:
		The initial vertical position of the window.For an overlapped or pop-up window,the y 
		parameter is the initial y-coordinate of the window's upper-left corner,in screen 
		coordinates.For a child window,y is the initial y-coordinate of the upper-left corner of the 
		child window relative to the upper-left corner of the parent window's client area.For a list 
		box y is the initial y-coordinate of the upper-left corner of the list box's client area relative 
		to the upper-left corner of the parent window's client area.
		If an overlapped window is created with the WS_VISIBLE style bit set and the x 
		parameter is set to CW_USEDEFAULT,then the y parameter determines how the window is 
		shown.If the y parameter is CW_USEDEFAULT,then the window manager calls 
		ShowWindow with the SW_SHOW flag after the window has been created.If the y 
		parameter is some other value,then the window manager calls ShowWindow with that 
		value as the nCmdShow parameter.
		nWidth:
		The width,in device units,of the window.For overlapped windows,nWidth is the window's 
		width,in screen coordinates,or CW_USEDEFAULT.If nWidth is CW_USEDEFAULT,the system 
		selects a default width and height for the window;the default width extends from the 
		initial x-coordinates to the right edge of the screen;the default height extends from the 
		initial y-coordinate to the top of the icon area.CW_USEDEFAULT is valid only for 
		overlapped windows;if CW_USEDEFAULT is specified for a pop-up or child window,the 
		nWidth and nHeight parameter are set to zero.
		nHeight:
		The height,in device units,of the window.For overlapped windows,nHeight is the window's 
		height,in screen coordinates.If the nWidth parameter is set to CW_USEDEFAULT,the 
		system ignores nHeight.
		hWndParent:
		A handle to the parent or owner window of the window being created.To create a child 
		window or an owned window,supply a valid window handle.This parameter is optional for 
		pop-up windows.
		To create a message-only window,supply HWND_MESSAGE or a handle to an existing 
		message-only window.
		hMenu:
		A handle to a menu,or specifies a child-window identifier,depending on the window style.
		For an overlapped or pop-up window,hMenu identifies the menu to be used with the 
		window;it can be NULL if the class menu is to be used.For a child window,hMenu specifies 
		the child-window identifier,an integer value used by a dialog box control to notify its 
		parent about events.The application determines the child-window identifier;it must be 
		unique for all child windows with the same parent window.
		hInstance:
		A handle to the instance of the module to be associated with the window.
		lpParam:
		Pointer to a value to be passed to the window through the CREATESTRUCT structure 
		(lpCreateParams member) pointed to by the lParam param of the WM_CREATE message.
		This message is sent to the created window by this function before it returns.
		If an application calls CreateWindow to create a MDI client window,lpParam should point 
		to a CLIENTCREATESTRUCT structure.If an MDI client window calls CreateWindow to 
		create an MDI child window,lpParam should point to a MDICREATESTRUCT structure.
		lpParam may be NULL if no additional data is needed.

		Defines the initialization parameters passed to the window procedure of an application.
		These members are identical to the parameters of the CreateWindowEx function.
		typedef struct tagCREATESTRUCT
		{
			LPVOID lpCreateParams;
			HINSTANCE hInstance;
			HMENU hMenu;
			HWND hwndParent;
			int cy;
			int cx;
			int y;
			int x;
			LONG style;
			LPCTSTR lpszName;
			LPCTSTR lpszClass;
			DWORD dwExStyle;
		}CREATESTRUCT,*LPCREATESTRUCT;
		lpCreateParams:
		Contains additional data which may be used to create the window.If the window is being 
		created as a result of a call to the CreateWindow or CreateWindowEx function,this 
		member contains the value of the lpParam parameter specified in the function call.
		cy:
		The height of the new window,in pixels.
		y:
		The y-coordinate of the upper left corner of the new window.If the new window is a child 
		window,coordinates are relative to the parent window.Otherwise,the coordinates are 
		relative to the screen origin.
		lpszClass:
		A pointer to a null-terminated string or an atom that specifies the class name of the new 
		window.
		Because the lpszClass member can contain a pointer to a local (and thus inaccessable) 
		atom,do not obtain the class name by using this member.Use the GetClassName function 
		instead.
		*/
		mHWND = CreateWindowEx(extStyle, mClassName, windowName,
			getStyle(),
			left, top,
			desiredWidth,
			desiredHeight,
			NULL, NULL,
			hInst,
			this);

		/*
		HWND-HANDLE-PVOID-void*
		If the function fails, the return value is NULL.
		*/
		if (mHWND == NULL)
		{
			UNG_EXCEPTION("Create Window failed.");
		}

#if UNG_DEBUGMODE
		bool ret{ false };
		RECT rc;

		/*
		Retrieves the dimensions of the bounding rectangle of the specified window(包围窗口的矩形).
		The dimensions are given in screen coordinates that are relative to the upper-left corner 
		of the screen.
		BOOL WINAPI GetWindowRect(HWND hWnd,LPRECT lpRect);
		The pixel at (right, bottom) lies immediately outside the rectangle.
		hWnd:
		A handle to the window.
		lpRect:
		A pointer to a RECT structure that receives the screen coordinates of the upper-left and 
		lower-right corners of the window.
		*/
		ret = GetWindowRect(mHWND, &rc);
		BOOST_ASSERT(ret);
		auto validateLeft = rc.left;
		auto validateTop = rc.top;
		BOOST_ASSERT(validateLeft == left);
		BOOST_ASSERT(validateTop == top);

		/*
		Retrieves the coordinates of a window's client area(客户区域).The client coordinates specify 
		the upper-left and lower-right corners of the client area.Because client coordinates are 
		relative to the upper-left corner of a window's client area,the coordinates of the upper-left 
		corner are (0,0).
		BOOL WINAPI GetClientRect(HWND hWnd,LPRECT lpRect);
		The pixel at (right, bottom) lies immediately outside the rectangle.
		hWnd:
		A handle to the window whose client coordinates are to be retrieved.
		lpRect:
		A pointer to a RECT structure that receives the client coordinates.The left and top 
		members are zero.The right and bottom members contain the width and height of the 
		window.
		*/
		ret = GetClientRect(mHWND, &rc);
		BOOST_ASSERT(ret);
		BOOST_ASSERT(rc.left == 0);
		BOOST_ASSERT(rc.top == 0);
		auto drawableAreaWidth = rc.right;
		auto drawableAreaHeight = rc.bottom;
		BOOST_ASSERT(drawableAreaWidth == mWidth);
		BOOST_ASSERT(drawableAreaHeight == mHeight);
#endif

		/*
		此时，Windows内部已经创建了窗口，但它还未在显示器上显示。
		需要调用ShowWindow()和UpdateWindow()
		UpdateWindow():
		强制Windows更新窗口的内容，并产生一个WM_PAINT消息。
		*/
		ShowWindow(mHWND,SW_SHOWNORMAL);
		SetActiveWindow(mHWND);
		SetForegroundWindow(mHWND);
		SetFocus(mHWND);
		UpdateWindow(mHWND);

		/*
		调用UpdateWindow()后，窗口出现在显示器上。
		*/
	}

	void D9Window::_selectDepthStencilFormat()
	{
		//渲染系统
		auto pRS = D9RenderSystem::getInstance();

		//D3D9接口对象
		auto pD9 = pRS->getIDirect3D9();

		auto adapterManager = pRS->getD9AdapterManager();
		auto oridinalNumber = _getAdapterOridinalNumberFromMonitor();

		mPixelFormat = D3DFMT_A8R8G8B8;

		//深度模板格式
		auto deviceType = D3DDEVTYPE_HAL;

		/*
		Verify that the depth format exists.

		HRESULT CheckDeviceFormat(UINT Adapter,D3DDEVTYPE DeviceType,D3DFORMAT AdapterFormat,
				DWORD Usage,D3DRESOURCETYPE RType,D3DFORMAT CheckFormat);
		Determines whether a surface format is available as a specified resource type and can 
		be used as a texture, depth-stencil buffer, or render target, or any combination of the 
		three, on a device representing this adapter.
		该函数用于检查一个表面格式是否能用作纹理或渲染目标，或者用于检查一个表面格式是否能够用于
		深度缓冲区。此外，该函数还可用于检查是否支持指定的深度缓冲区格式和模板缓冲区格式。
		Adapter:
		Ordinal number denoting the display adapter to query. D3DADAPTER_DEFAULT is 
		always the primary display adapter. This method returns D3DERR_INVALIDCALL when 
		this value equals or exceeds the number of display adapters in the system.
		DeviceType:
		Member of the D3DDEVTYPE enumerated type, identifying the device type.
		AdapterFormat:
		Member of the D3DFORMAT enumerated type, identifying the format of the display 
		mode into which the adapter will be placed.
		Usage:
		Requested usage options for the surface. Usage options are any combination of 
		D3DUSAGE and D3DUSAGE_QUERY constants (only a subset of the D3DUSAGE 
		constants are valid for CheckDeviceFormat).
		RType:
		Resource type requested for use with the queried format.
		CheckFormat:
		Format of the surfaces which may be used, as defined by Usage.
		Examples:
		An off-screen plain surface format - Specify Usage = 0 and RType = 
		D3DRTYPE_SURFACE.
		Alpha blending in a pixel shader - Set Usage to 
		D3DUSAGE_QUERY_POSTPIXELSHADER_BLENDING. Expect this to fail for all 
		floating-point render targets.
		Autogeneration of mipmaps - Set Usage to D3DUSAGE_AUTOGENMIPMAP. If the 
		mipmap automatic generation fails, the application will get a non-mipmapped texture.
		Calling this method is considered a hint, so this method can return 
		D3DOK_NOAUTOGEN (a valid success code) if the only thing that fails is the mipmap 
		generation. For more information about mipmap generation
		*/
		if (FAILED(pD9->CheckDeviceFormat(oridinalNumber, deviceType, mPixelFormat,
				D3DUSAGE_DEPTHSTENCIL, D3DRTYPE_SURFACE, D3DFMT_D24S8)) || 
			FAILED(pD9->CheckDeviceFormat(oridinalNumber, deviceType, mPixelFormat,
				D3DUSAGE_DEPTHSTENCIL, D3DRTYPE_SURFACE, D3DFMT_D24X8)))
		{
			mPixelFormat = D3DFMT_X8R8G8B8;
		}

		/*
		HRESULT CheckDepthStencilMatch(UINT Adapter,D3DDEVTYPE DeviceType,
		D3DFORMAT AdapterFormat,D3DFORMAT RenderTargetFormat,D3DFORMAT DepthStencilFormat);
		Determines whether a depth-stencil format is compatible with a render-target format in
		a particular display mode.
		Adapter:
		Ordinal number denoting the display adapter to query.D3DADAPTER_DEFAULT is always
		the primary display adapter.
		DeviceType:
		Member of the D3DDEVTYPE enumerated type, identifying the device type.
		AdapterFormat:
		Member of the D3DFORMAT enumerated type, identifying the format of the display
		mode into which the adapter will be placed.
		RenderTargetFormat:
		Member of the D3DFORMAT enumerated type, identifying the format of the
		render-target surface to be tested.
		DepthStencilFormat:
		Member of the D3DFORMAT enumerated type, identifying the format of the
		depth-stencil surface to be tested.
		*/
		if (SUCCEEDED(pD9->CheckDepthStencilMatch(oridinalNumber, deviceType,
			mPixelFormat, mPixelFormat, D3DFMT_D24S8)))
		{
			mDepthStencilFormat = D3DFMT_D24S8;
		}
		else
		{
			mDepthStencilFormat = D3DFMT_D24X8;
		}
	}

	void D9Window::_selectWindowedFormat()
	{
		if (mIsFullScreen)
		{
			return;
		}

		if (mColorDepth > 16)
		{
			//渲染系统
			auto pRS = D9RenderSystem::getInstance();

			//D3D9接口对象
			auto pD9 = pRS->getIDirect3D9();

			//适配器管理器
			auto pAM = pRS->getD9AdapterManager();

			//适配器序号
			auto oridinalNumber = _getAdapterOridinalNumberFromMonitor();

			/*
			桌面像素格式
			Use the current display mode for windowed mode.
			*/
			auto desktopPixelFormat = pAM->getAdapterFromOridinalNumber(oridinalNumber)->getDesktopDisplayMode().Format;
				
			/*
			For windowed applications,the back buffer format need not match the display format.
			However,you should call IDirect3D9::CheckDeviceFormatConversion to see if the 
			hardware supports a conversion between the two formats you want to use.
			*/
			/*
			Tests the device to see if it supports conversion from one display format to another.
			HRESULT CheckDeviceFormatConversion(UINT Adapter,D3DDEVTYPE DeviceType,
			D3DFORMAT SourceFormat,D3DFORMAT TargetFormat);
			*/
			if (FAILED(pD9->CheckDeviceFormatConversion(oridinalNumber, D3DDEVTYPE_HAL,
				desktopPixelFormat, mPixelFormat)))
			{
				mPixelFormat = desktopPixelFormat;
			}
		}
	}

	void D9Window::_selectFullScreenDisplayMode()
	{
		if (!mIsFullScreen || mFullScreenDisplayMode)
		{
			return;
		}

		//渲染系统
		auto pRS = D9RenderSystem::getInstance();

		//D3D9接口对象
		auto pD9 = pRS->getIDirect3D9();

		//build display mode
		auto adapterManager = pRS->getD9AdapterManager();
		auto oridinalNumber = _getAdapterOridinalNumberFromMonitor();
		auto pAdapter = adapterManager->getAdapterFromOridinalNumber(oridinalNumber);
		auto displayModes = pAdapter->getDisplayModeCollection();

		auto collection = displayModes->getCollection();

		D9DisplayMode* ret = nullptr;

		//临时转存到list中
		STL_LIST(D9DisplayMode*) tempList { collection.cbegin(), collection.cend() };

		//排序
		auto lambdaExpress = [](D9DisplayMode* a, D9DisplayMode* b) -> bool
		{
			auto aw = a->getWidth();
			auto ah = a->getHeight();
			auto arr = a->getRefreshRate();
			auto bw = b->getWidth();
			auto bh = b->getHeight();
			auto brr = b->getRefreshRate();

			if (aw > bw)
			{
				return true;
			}
			else if (aw == bw)
			{
				if (ah > bh)
				{
					return true;
				}
				else if (ah == bh)
				{
					if (arr > brr)
					{
						return true;
					}
				}
			}

			return false;
		};

		tempList.sort(lambdaExpress);

		for (auto const& dm : tempList)
		{
			auto displayFormat = dm->getPixelFormat();

			/*
			Verifies whether a hardware accelerated device type can be used on this adapter.
			HRESULT CheckDeviceType(UINT Adapter,D3DDEVTYPE DeviceType,
			D3DFORMAT DisplayFormat,D3DFORMAT BackBufferFormat,BOOL Windowed);
			该函数用于检查设备的硬件加速能力，为表面提交创建交换链的能力以及渲染到当前显示格式的能力。
			A hal device type requires hardware acceleration. Applications can use CheckDeviceType
			to determine if the needed hardware and drivers are present to support a hal device.
			Full-screen applications should not specify a DisplayFormat that contains an alpha
			channel.This will result in a failed call. Note that an alpha channel can be present in the
			back buffer but the two display formats must be identical in all other respects.For
			example,if DisplayFormat = D3DFMT_X1R5G5B5,valid values for BackBufferFormat
			include D3DFMT_X1R5G5B5 and D3DFMT_A1R5G5B5 but exclude D3DFMT_R5G6B5.
			Using CheckDeviceType to test for compatibility between a back buffer that differs from
			the display format will return appropriate values.This means that the call will reflect
			device capabilities.If the device cannot render to the requested back-buffer format,the
			call will still return D3DERR_NOTAVAILABLE.If the device can render to the format,but
			cannot perform the color-converting presentation,the return value will also be
			D3DERR_NOTAVAILABLE.Applications can discover hardware support for the
			presentation itself by calling CheckDeviceFormatConversion.No software emulation for
			the color-converting presentation itself will be offered.
			Adapter:
			Ordinal number denoting the display adapter to enumerate.D3DADAPTER_DEFAULT is
			always the primary display adapter.This method returns D3DERR_INVALIDCALL when
			this value equals or exceeds the number of display adapters in the system.
			DeviceType:
			Member of the D3DDEVTYPE enumerated type,indicating the device type to check.
			DisplayFormat:
			Member of the D3DFORMAT enumerated type,indicating the format of the adapter
			display mode for which the device type is to be checked.For example,some devices will
			operate only in 16-bits-per-pixel modes.
			BackBufferFormat:
			Back buffer format.For more information about formats,see D3DFORMAT.This value
			must be one of the render-target formats.You can use GetAdapterDisplayMode to
			obtain the current format.For windowed applications,the back buffer format does not
			need to match the display mode format if the hardware supports color conversion.The
			set of possible back buffer formats is constrained,but the runtime will allow any valid
			back buffer format to be presented to any desktop format.There is the additional
			requirement that the device be operable in the desktop because devices typically do
			not operate in 8 bits per pixel modes.Full-screen applications cannot do color
			conversion.D3DFMT_UNKNOWN is allowed for windowed mode.
			Windowed:
			Value indicating whether the device type will be used in full-screen or windowed mode.
			If set to TRUE,the query is performed for windowed applications;otherwise,this value
			should be set FALSE.(a certain display and back buffer format configuration may
			work in windowed mode but not full-screen mode,or conversely(反之).)
			If the device can be used on this adapter,D3D_OK is returned.D3DERR_INVALIDCALL is
			returned if Adapter equals or exceeds the number of display adapters in the system.
			D3DERR_INVALIDCALL is also returned if CheckDeviceType specified a device that does
			not exist.D3DERR_NOTAVAILABLE is returned if the requested back buffer format is not
			supported,or if hardware acceleration is not available for the specified formats.
			*/
			if (SUCCEEDED(pD9->CheckDeviceType(oridinalNumber, D3DDEVTYPE_HAL,
				displayFormat, mPixelFormat,false)))
			{
				mFullScreenDisplayMode = dm;

				return;
			}
			else
			{
				//Use the commonly supported format D3DFMT_X8R8G8B8 for full-screen mode.
				mPixelFormat = D3DFMT_X8R8G8B8;

				if (SUCCEEDED(pD9->CheckDeviceType(oridinalNumber, D3DDEVTYPE_HAL,
					displayFormat, mPixelFormat, false)))
				{
					mFullScreenDisplayMode = dm;

					return;
				}
				else
				{
					mPixelFormat = displayFormat;

					mFullScreenDisplayMode = dm;

					return;
				}
			}
		}

		if (!mFullScreenDisplayMode)
		{
			UNG_EXCEPTION("Failed to find a display mode suitable for full screen mode.");
		}
	}

	void D9Window::changeWindowedFullScreenFlag()
	{
		mIsFullScreen = !mIsFullScreen;
	}
}//namespace ung

#pragma warning(pop)

#endif//UD9_HIERARCHICAL_COMPILE

/*
Window Styles:
The following are the window styles. After the window has been created, these styles cannot be 
modified, except as noted.
WS_BORDER
0x00800000L:
The window has a thin-line border.
WS_CAPTION
0x00C00000L:
The window has a title bar (includes the WS_BORDER style).
WS_CHILD
0x40000000L:
The window is a child window.A window with this style cannot have a menu bar.This style cannot 
be used with the WS_POPUP style.
WS_CHILDWINDOW
0x40000000L:
Same as the WS_CHILD style.
WS_CLIPCHILDREN
0x02000000L:
Excludes the area occupied by child windows when drawing occurs within the parent window.
This style is used when creating the parent window.
WS_CLIPSIBLINGS
0x04000000L:
Clips child windows relative to each other; that is, when a particular child window receives a 
WM_PAINT message, the WS_CLIPSIBLINGS style clips all other overlapping child windows out 
of the region of the child window to be updated. If WS_CLIPSIBLINGS is not specified and child 
windows overlap, it is possible, when drawing within the client area of a child window, to draw 
within the client area of a neighboring child window.
WS_DISABLED
0x08000000L:
The window is initially disabled. A disabled window cannot receive input from the user. To 
change this after a window has been created, use the EnableWindow function.
WS_DLGFRAME
0x00400000L:
The window has a border of a style typically used with dialog boxes. A window with this style 
cannot have a title bar.
WS_GROUP
0x00020000L:
The window is the first control of a group of controls. The group consists of this first control and 
all controls defined after it, up to the next control with the WS_GROUP style. The first control in 
each group usually has the WS_TABSTOP style so that the user can move from group to group.
The user can subsequently change the keyboard focus from one control in the group to the next 
control in the group by using the direction keys.
You can turn this style on and off to change dialog box navigation. To change this style after a 
window has been created, use the SetWindowLong function.
WS_HSCROLL
0x00100000L:
The window has a horizontal scroll bar.
WS_ICONIC
0x20000000L:
The window is initially minimized. Same as the WS_MINIMIZE style.
WS_MAXIMIZE
0x01000000L:
The window is initially maximized.
WS_MAXIMIZEBOX
0x00010000L:
The window has a maximize button. Cannot be combined with the WS_EX_CONTEXTHELP style.
The WS_SYSMENU style must also be specified.
WS_MINIMIZE
0x20000000L:
The window is initially minimized. Same as the WS_ICONIC style.
WS_MINIMIZEBOX
0x00020000L:
The window has a minimize button. Cannot be combined with the WS_EX_CONTEXTHELP style.
The WS_SYSMENU style must also be specified.
WS_OVERLAPPED
0x00000000L:
The window is an overlapped window. An overlapped window has a title bar and a border. Same 
as the WS_TILED style.
WS_OVERLAPPEDWINDOW:
(WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX)
The window is an overlapped window. Same as the WS_TILEDWINDOW style.
WS_POPUP
0x80000000L:
The windows is a pop-up window.This style cannot be used with the WS_CHILD style.
WS_POPUPWINDOW
(WS_POPUP | WS_BORDER | WS_SYSMENU)
The window is a pop-up window. The WS_CAPTION and WS_POPUPWINDOW styles must be 
combined to make the window menu visible.
WS_SIZEBOX
0x00040000L:
The window has a sizing border. Same as the WS_THICKFRAME style.
WS_SYSMENU
0x00080000L:
The window has a window menu on its title bar. The WS_CAPTION style must also be specified.
WS_TABSTOP
0x00010000L:
The window is a control that can receive the keyboard focus when the user presses the TAB key.
Pressing the TAB key changes the keyboard focus to the next control with the WS_TABSTOP 
style.
You can turn this style on and off to change dialog box navigation. To change this style after a 
window has been created, use the SetWindowLong function. For user-created windows and 
modeless dialogs to work with tab stops, alter the message loop to call the IsDialogMessage 
function.
WS_THICKFRAME
0x00040000L:
The window has a sizing border. Same as the WS_SIZEBOX style.
WS_TILED
0x00000000L:
The window is an overlapped window. An overlapped window has a title bar and a border. Same 
as the WS_OVERLAPPED style.
WS_TILEDWINDOW
(WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX)
The window is an overlapped window. Same as the WS_OVERLAPPEDWINDOW style.
WS_VISIBLE
0x10000000L:
The window is initially visible.
This style can be turned on and off by using the ShowWindow or SetWindowPos function.
WS_VSCROLL
0x00200000L:
The window has a vertical scroll bar.
*/

/*
HMONITOR MonitorFromPoint(POINT pt,DWORD dwFlags);
retrieves a handle to the display monitor that contains a specified point.
pt:
A POINT structure that specifies the point of interest in virtual-screen coordinates.
dwFlags:
Determines the function's return value if the point is not contained within any display 
monitor.
This parameter can be one of the following values.
MONITOR_DEFAULTTONEAREST
Returns a handle to the display monitor that is nearest to the point.
MONITOR_DEFAULTTONULL
Returns NULL.
MONITOR_DEFAULTTOPRIMARY
Returns a handle to the primary display monitor.
If the point is contained by a display monitor, the return value is an HMONITOR handle to 
that display monitor.
If the point is not contained by a display monitor, the return value depends on the 
value of dwFlags.

POINT windowAnchorPoint;
//Fill in anchor point.
windowAnchorPoint.x = 0;
windowAnchorPoint.y = 0;
hMonitor = MonitorFromPoint(windowAnchorPoint, MONITOR_DEFAULTTONEAREST);

The MonitorFromRect function retrieves a handle to the display monitor that has the largest 
area of intersection with a specified rectangle.
HMONITOR MonitorFromRect(LPCRECT lprc,DWORD dwFlags);
lprc:
A pointer to a RECT structure that specifies the rectangle of interest in virtual-screen 
coordinates.
dwFlags:
Determines the function's return value if the rectangle does not intersect any display monitor.
This parameter can be one of the following values.
MONITOR_DEFAULTTONEAREST:
Returns a handle to the display monitor that is nearest to the rectangle.
MONITOR_DEFAULTTONULL:
Returns NULL.
MONITOR_DEFAULTTOPRIMARY:
Returns a handle to the primary display monitor.
If the rectangle intersects one or more display monitor rectangles, the return value is an 
HMONITOR handle to the display monitor that has the largest area of intersection with the 
rectangle.
If the rectangle does not intersect a display monitor, the return value depends on the value of 
dwFlags.
*/

/*
tribool位于名字空间boost::logic，但为了方便使用被using语句引入了boost名字空间。
使用无参的默认构造函数，tribool的默认值是false。
tribool的逻辑运算符和比较运算符可以任意混合bool和一起运算。
不确定状态indeterminate是一个特殊的tribool值，它与bool值true，false的运算遵循三态布尔逻辑：
1，任何与indeterminate的比较操作结果都是indeterminate。
2，与indeterminate的逻辑或运算(||)只有与true运算结果为true，其它均为indeterminate。
3，与indeterminate的逻辑与运算(&&)只有与false运算结果为false，其它均为indeterminate。
4，indeterminate的逻辑非操作(!)结果仍为indeterminate。
自由函数indeterminate()可以判断一个tribool是否处于不确定状态。或者可以与indeterminate进行比较。
如果仅使用true和false两个值，那么tribool与bool的用法是完全相同的。
使用indeterminate进行条件判断永远都不会成立。例如：
tribool tb = boost::logic::indeterminate;
if(tb)																	//永远不成立
{
	...
}
if(!tb)																//永远不成立
{
	...
}
if(indeterminate(tb))											//成立
{
	...
}
tribool可以如bool类型一样进行流操作，但需要包含另外一个头文件：
boost/logic/tribool_io.hpp
如果设置了流的boolalpha标志，则对应字符串，否则对应false:0,true:1,indeterminate:2
*/