#include "UG3Window.h"

#ifdef UG3_HIERARCHICAL_COMPILE

#include "boost/algorithm/minmax.hpp"
#include "URMWindowEventUtilities.h"

#pragma warning(push)
#pragma warning(disable : 4800)

namespace ung
{
	G3Window::G3Window(String const & title, bool isFullScreen, uint32 width, uint32 height,bool primary,HINSTANCE inst) :
		mInstance(inst),
		mClassName(nullptr),
		mTitle(title),
		mIsFullScreen(isFullScreen),
		mWidth(isFullScreen ? 0 : width),
		mHeight(isFullScreen ? 0 : height),
		mLastWidth(width),
		mLastHeight(height),
		mIsPrimary(primary),
		mVerticalSync(!isFullScreen ? true : false),
		//全屏模式下，也初始化为boost::logic::indeterminate
		mState(boost::logic::indeterminate)
	{
		String str = UniqueID().toString();
		auto ret = LexicalCast::convert(str,mClassName);
		BOOST_ASSERT(ret);

		buildStyle();

		//保持下面三个函数的调用顺序
		_selectDepthStencilFormat();

		_selectWindowedFormat();

		_selectFullScreenDisplayMode();

		if (mIsFullScreen)
		{
			//mWidth = mFullScreenDisplayMode->getWidth();
			//mHeight = mFullScreenDisplayMode->getHeight();
		}
	}

	G3Window::~G3Window()
	{
		delete[] mClassName;
	}

	wchar_t const * G3Window::getClassName() const
	{
		return mClassName;
	}

	HWND G3Window::getHandle() const
	{
		return mHWND;
	}

	String const& G3Window::getTitle() const
	{
		return mTitle;
	}

	bool G3Window::isFullScreen() const
	{
		return mIsFullScreen;
	}

	bool G3Window::isVerticalSync() const
	{
		return mVerticalSync;
	}

	uint32 G3Window::getWidth() const
	{
		return mWidth;
	}

	uint32 G3Window::getHeight() const
	{
		return mHeight;
	}

	void G3Window::setWidth(uint32 newWidth)
	{
		//缩小窗口时，通过解析消息参数所得到的宽和高都是0。
		if (newWidth > 0)
		{
			mWidth = newWidth;
		}
	}

	void G3Window::setHeight(uint32 newHeight)
	{
		if (newHeight > 0)
		{
			mHeight = newHeight;
		}
	}

	uint32 G3Window::getLastWidth() const
	{
		return mLastWidth;
	}

	uint32 G3Window::getLastHeight() const
	{
		return mLastHeight;
	}

	void G3Window::updateLastWH()
	{
		mLastWidth = mWidth;
		mLastHeight = mHeight;
	}

	void G3Window::buildStyle()
	{
		mFullScreenStyle = WS_CLIPCHILDREN | WS_POPUP | WS_VISIBLE;
		mWindowedStyle = WS_CLIPCHILDREN | WS_OVERLAPPEDWINDOW | WS_VISIBLE;
	}

	uint32 G3Window::getFullScreenStyle() const
	{
		return mFullScreenStyle;
	}

	uint32 G3Window::getWindowedStyle() const
	{
		return mWindowedStyle;
	}

	uint32 G3Window::getStyle() const
	{
		return mIsFullScreen ? mFullScreenStyle : mWindowedStyle;
	}

	HMONITOR G3Window::getMonitor() const
	{
		BOOST_ASSERT(mMonitorToWhichWindowBelongs);

		return mMonitorToWhichWindowBelongs;
	}

	bool G3Window::isPrimary() const
	{
		return mIsPrimary;
	}

	bool G3Window::create()
	{
		////显示器句柄
		//HMONITOR hMonitor = _findMonitorFromDefaultAdapter();

		////显示器信息
		//MONITORINFO monitorInfo = _getMonitorInfo(hMonitor);

		//左上角
		uint32 left = INT_MAX;
		uint32 top = INT_MAX;

		//期望的窗口宽高(窗口的整体大小)
		uint32 desiredWinWidth = 0;
		uint32 desiredWinHeight = 0;

		////计算窗口的锚点和大小
		//_calWindowPositionAndSize(monitorInfo.rcWork,left,top,desiredWinWidth,desiredWinHeight);

		////注册窗口类
		//_registerWindowClass();

		////创建窗口
		//_createWindow(left,top,desiredWinWidth,desiredWinHeight);

		////从已经创建的窗口来获取该窗口所归属的显示器句柄
		//mMonitorToWhichWindowBelongs = _findMonitorFromWindow();
		//BOOST_ASSERT(mMonitorToWhichWindowBelongs == hMonitor);

		return true;
	}

	void G3Window::destroy()
	{
		if (mHWND)
		{
			mHWND = 0;
		}
	}

	void G3Window::setMinMaxState(boost::tribool state)
	{
		BOOST_ASSERT(!mIsFullScreen);

		mState = state;
	}

	boost::tribool G3Window::getMinMaxState() const
	{
		BOOST_ASSERT(!mIsFullScreen);

		return mState;
	}

	bool G3Window::isNormalState() const
	{
		return boost::indeterminate(mState);
	}

	void G3Window::present()
	{
	}

	HMONITOR G3Window::_findMonitorFromWindow()
	{
		mMonitorToWhichWindowBelongs = MonitorFromWindow(mHWND,MONITOR_DEFAULTTONEAREST);

		return mMonitorToWhichWindowBelongs;
	}

	MONITORINFO G3Window::_getMonitorInfo(HMONITOR hMonitor)
	{
		BOOST_ASSERT(hMonitor);

		MONITORINFO monitorInfo;
		SecureZeroMemory(&monitorInfo, sizeof(monitorInfo));
		monitorInfo.cbSize = sizeof(monitorInfo);
		monitorInfo.dwFlags = MONITORINFOF_PRIMARY;

		auto monitorInfoRet = GetMonitorInfo(hMonitor,&monitorInfo);
		BOOST_ASSERT(monitorInfoRet);

		return monitorInfo;
	}

	void G3Window::_calWindowPositionAndSize(RECT workAreaRectangle, uint32 & left, uint32 & top, uint32 & desiredWidth, uint32 & desiredHeight)
	{
		if (mIsFullScreen)
		{
			left = 0;
			top = 0;

			desiredWidth = mWidth;
			desiredHeight = mHeight;

			return;
		}

		//工作区域矩形
		auto rcwLeft = workAreaRectangle.left;
		auto rcwRight = workAreaRectangle.right;
		auto rcwTop = workAreaRectangle.top;
		auto rcwBottom = workAreaRectangle.bottom;

		//屏幕宽高(工作区域，高比屏幕的分辨率数值略小)
		uint32 screenWidth = rcwRight - rcwLeft;
		uint32 screenHeight = rcwBottom - rcwTop;

		//客户区域宽高
		uint32 clientWidth = boost::minmax(mWidth, screenWidth).get<0>();
		uint32 clientHeight = boost::minmax(mHeight, screenHeight).get<0>();

		left = rcwLeft + (screenWidth - clientWidth) / 2;
		top = rcwTop + (screenHeight - clientHeight) / 2;

		RECT rc;
		SetRect(&rc, 0, 0, clientWidth, clientHeight);

		AdjustWindowRect(&rc, getStyle(), false);
		//期望的窗口宽高(窗口整体宽高)
		desiredWidth = rc.right - rc.left;
		desiredHeight = rc.bottom - rc.top;
	}

	void G3Window::_registerWindowClass()
	{
		HINSTANCE hInst = mInstance;

		/*
		CS_OWNDC:
		Allocates a unique device context for each window in the class.
		CS_HREDRAW | CS_VREDRAW:
		当水平方向或垂直方向尺寸改变时，要完全刷新窗口。
		*/
		UINT windowClassStyle = 0;

		/*
		窗口是在窗口类的基础上创建的，可以在单个窗口类的基础上创建多个窗口。
		窗口类定义了窗口过程和基于此类的窗口的其它一些特征。
		*/
		WNDCLASS windowClass;
		memset(&windowClass, 0, sizeof(WNDCLASS));

		windowClass.style = windowClassStyle;
		/*
		Associating a Window Procedure with a Window Class
		这个窗口过程将处理基于此类创建的所有窗口的全部消息。
		*/
		windowClass.lpfnWndProc = WindowEventUtilities::windowProc;
		//获得目前运行的程序
		windowClass.hInstance = hInst;									//GetModuleHandle(NULL);
		//使用默认的鼠标
		windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
		//图标
		windowClass.hIcon = LoadIcon(NULL, IDI_WINLOGO);
		/*
		窗口的背景画刷都无关紧要，因为在DirectX接管之后，它将不起作用。
		*/
		windowClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
		windowClass.lpszClassName = mClassName;
		windowClass.lpszMenuName = NULL;
		/*
		在类结构和Windows内部保存的窗口结构中不预留额外空间。
		*/
		windowClass.cbClsExtra = 0;
		windowClass.cbWndExtra = 0;

		if (RegisterClass(&windowClass) == 0)
		{
			UNG_EXCEPTION("Window registration failed.");
		}

		ShowCursor(true);
	}

	void G3Window::_createWindow(uint32 left,uint32 top,uint32 desiredWidth,uint32 desiredHeight)
	{
		wchar_t* pTitle = nullptr;
		LexicalCast::convert(mTitle, pTitle);
		wchar_t windowName[64];
		auto copyRet = wcscpy_s(windowName, pTitle);
		delete[] pTitle;
		pTitle = nullptr;
		BOOST_ASSERT(copyRet == 0);

		HINSTANCE hInst = mInstance;

		//Window Extended Style
		DWORD extStyle = 0;
		if (mIsFullScreen)
		{
			extStyle |= WS_EX_TOPMOST;
		}

		mHWND = CreateWindowEx(extStyle, mClassName, windowName,
			getStyle(),
			left, top,
			desiredWidth,
			desiredHeight,
			NULL, NULL,
			hInst,
			this);

		if (mHWND == NULL)
		{
			UNG_EXCEPTION("Create Window failed.");
		}

#if UNG_DEBUGMODE
		bool ret{ false };
		RECT rc;

		ret = GetWindowRect(mHWND, &rc);
		BOOST_ASSERT(ret);
		auto validateLeft = rc.left;
		auto validateTop = rc.top;
		BOOST_ASSERT(validateLeft == left);
		BOOST_ASSERT(validateTop == top);

		ret = GetClientRect(mHWND, &rc);
		BOOST_ASSERT(ret);
		BOOST_ASSERT(rc.left == 0);
		BOOST_ASSERT(rc.top == 0);
		auto drawableAreaWidth = rc.right;
		auto drawableAreaHeight = rc.bottom;
		BOOST_ASSERT(drawableAreaWidth == mWidth);
		BOOST_ASSERT(drawableAreaHeight == mHeight);
#endif

		/*
		此时，Windows内部已经创建了窗口，但它还未在显示器上显示。
		需要调用ShowWindow()和UpdateWindow()
		UpdateWindow():
		强制Windows更新窗口的内容，并产生一个WM_PAINT消息。
		*/
		ShowWindow(mHWND,SW_SHOWNORMAL);
		SetActiveWindow(mHWND);
		SetForegroundWindow(mHWND);
		SetFocus(mHWND);
		UpdateWindow(mHWND);

		/*
		调用UpdateWindow()后，窗口出现在显示器上。
		*/
	}

	void G3Window::_selectDepthStencilFormat()
	{
	}

	void G3Window::_selectWindowedFormat()
	{
	}

	void G3Window::_selectFullScreenDisplayMode()
	{
	}

	void G3Window::changeWindowedFullScreenFlag()
	{
		mIsFullScreen = !mIsFullScreen;
	}
}//namespace ung

#pragma warning(pop)

#endif//UG3_HIERARCHICAL_COMPILE