#include "UG3Monitor.h"

#ifdef UG3_HIERARCHICAL_COMPILE

namespace ung
{
	G3Monitor::G3Monitor(HMONITOR hMonitor) :
		mHMonitor(hMonitor)
	{
		BOOST_ASSERT(mHMonitor);

		SecureZeroMemory(&mInfo,sizeof(MONITORINFOEX));

		mInfo.cbSize = sizeof(MONITORINFOEX);

		GetMonitorInfo(hMonitor, &mInfo);
	}

	G3Monitor::~G3Monitor()
	{
	}
}//namespace ung

#endif//UG3_HIERARCHICAL_COMPILE