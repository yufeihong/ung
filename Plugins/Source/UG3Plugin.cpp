#include "UG3Plugin.h"

#ifdef UG3_HIERARCHICAL_COMPILE

#include "UG3RenderSystem.h"

namespace ung
{
	namespace
	{
		const char* pluginName = "plugin_UG3";
	}//namespace

	G3Plugin::G3Plugin() :
		mRenderSystem(nullptr)
	{
	}

	G3Plugin::~G3Plugin()
	{
		UNG_DEL(mRenderSystem);
	}

	const char* G3Plugin::getName() const
	{
		return pluginName;
	}

	/*
	HINSTANCE:A handle to an instance. This is the base address of the module in memory.
	*/
	void G3Plugin::install()
	{
		//hInst：为了创建窗口用
#if UNG_DEBUGMODE
		HINSTANCE hInst = GetModuleHandle(L"UG3_d.dll");
#else
		HINSTANCE hInst = GetModuleHandle(L"UG3.dll");
#endif

		UNG_PRINT_ONE_LINE("GL3插件已经安装。");

		//创建GL3渲染系统
		UNG_LOG("Create GL3 render system.");

		mRenderSystem = UNG_NEW G3RenderSystem(hInst);

		Root::getInstance().addRenderSystem(pluginName, mRenderSystem);
	}

	void G3Plugin::uninstall()
	{
		Root::getInstance().removeRenderSystem(pluginName);

		UNG_PRINT_ONE_LINE("GL3插件已经卸载。");
	}
}//namespace ung

#endif//UG3_HIERARCHICAL_COMPILE