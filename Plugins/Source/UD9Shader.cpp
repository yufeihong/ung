#include "UD9Shader.h"

#ifdef UD9_HIERARCHICAL_COMPILE

#include "UD9RenderSystem.h"
#include "UD9Device.h"

namespace ung
{
	D9Shader::D9Shader(String const& fullName,ShaderType type) :
		Shader(fullName,type),
		mConstantTable(nullptr)
	{
	}

	D9Shader::~D9Shader()
	{
		UD9_RELEASE(mConstantTable);
	}

	Shader* D9Shader::clone() const
	{
		return nullptr;
	}

	void D9Shader::setConstant(const char* name, void* pBuffer, uint32 bytes)
	{
		BOOST_ASSERT(mConstantTable);

		auto pActiveDevice = D9RenderSystem::getInstance()->getActiveDevice();

		auto handle = mConstantTable->GetConstantByName(NULL, name);

		//被编译器优化掉的常量，这里得到的句柄为空
		if (!handle)
		{
			return;
		}

		auto ret = mConstantTable->SetValue(pActiveDevice, handle, pBuffer, bytes);

		BOOST_ASSERT(ret == D3D_OK);
	}

	//-------------------------------------------------------------------------

	D9VertexShader::D9VertexShader(String const& fullName) :
		D9Shader(fullName,ShaderType::ST_VERT),
		mShader(nullptr)
	{
		auto fileExt = mParser->getExtension();

		BOOST_ASSERT(fileExt == String("vert"));
	}

	D9VertexShader::~D9VertexShader()
	{
		/*
		可以先Release IDirect3DVertexShader9，然后再Release ID3DXConstantTable
		*/

		UD9_RELEASE(mShader);
	}

	Shader* D9VertexShader::clone() const
	{
		//Shader::clone();

		return nullptr;
	}

	void D9VertexShader::compile()
	{
		auto pActiveDevice = D9RenderSystem::getInstance()->getActiveDevice();

		ID3DXBuffer* pShader = nullptr;
		ID3DXBuffer* pErrorMsg = nullptr;

		auto source = mParser->getSource();
		auto size = mParser->getSize();

		/*
		vs_3_0
		Instruction Slots:
		512 minimum, and up to the number of slots in 
		D3DCAPS9.MaxVertexShader30InstructionSlots.See D3DCAPS9.

		Returns the name of the highest high-level shader language (HLSL) profile supported by 
		a given device.
		LPCSTR D3DXGetVertexShaderProfile(LPDIRECT3DDEVICE9 pDevice);
		Return value:
		The HLSL profile name.
		If the device does not support vertex shaders then the function returns NULL.
		A shader profile specifies the assembly shader version to use and the capabilities available 
		to the HLSL compiler when compiling a shader.
		*/
		auto profile = D3DXGetVertexShaderProfile(pActiveDevice);
		BOOST_ASSERT(profile);

		uint32 flags{ 0 };

#if UNG_DEBUGMODE
		flags |= D3DXSHADER_DEBUG;
#endif

		flags |= D3DXSHADER_PACKMATRIX_ROWMAJOR;

		/*
		Compile a shader file.
		Note:Instead of using this legacy(遗弃) function,we recommend that you compile offline 
		by using the Fxc.exe command-line compiler or use the D3DCompile API.
		HRESULT D3DXCompileShader(LPCSTR pSrcData,UINT srcDataLen,
		const D3DXMACRO *pDefines,LPD3DXINCLUDE pInclude,LPCSTR pFunctionName,
		LPCSTR pProfile,DWORD Flags,LPD3DXBUFFER *ppShader,LPD3DXBUFFER *ppErrorMsgs,
		LPD3DXCONSTANTTABLE *ppConstantTable);
		pSrcData:
		Pointer to a string that contains the shader.
		srcDataLen:
		Length of the data in bytes.
		pDefines:
		An optional NULL terminated array of D3DXMACRO structures.This value may be NULL.
		pInclude:
		Optional interface pointer,ID3DXInclude,to use for handling #include directives.If this 
		value is NULL,#includes will either be honored when compiling from a file or will cause an 
		error when compiled from a resource or memory.
		pFunctionName:
		Pointer to a string that contains the name of the shader entry point function where 
		execution begins.
		pProfile:
		Pointer to a shader profile which determines the shader instruction set.See 
		D3DXGetVertexShaderProfile or D3DXGetPixelShaderProfile for a list of the profiles 
		available.
		Flags:
		Compile options identified by various flags.The Direct3D 10 HLSL compiler is now the 
		default.See D3DXSHADER Flags for details.
		ppShader:
		Returns a buffer containing the created shader.This buffer contains the compiled shader 
		code,as well as any embedded debug and symbol table information.
		ppErrorMsgs:
		Returns a buffer containing a listing of errors and warnings that were encountered during 
		the compile.These are the same messages the debugger displays when running in debug 
		mode.This value may be NULL.
		ppConstantTable:
		Returns an ID3DXConstantTable interface,which can be used to access shader constants.
		This value can be NULL.If you compile your application as large address aware (that is, 
		you use the /LARGEADDRESSAWARE linker option to handle addresses larger than 2 GB),
		you cannot use this parameter and must set it to NULL.Instead,you must use the 
		D3DXGetShaderConstantTableEx function to retrieve the shader-constant table that is 
		embedded inside the shader.In this D3DXGetShaderConstantTableEx call,you must pass 
		the D3DXCONSTTABLE_LARGEADDRESSAWARE flag to the Flags parameter to specify to 
		access up to 4 GB of virtual address space.
		*/
		D3DXCompileShader(source,size,NULL,NULL,"mainVS",profile,flags,&pShader,&pErrorMsg,
			&mConstantTable);

		if (pErrorMsg)
		{
			/*
			Retrieves a pointer to the data in the buffer.
			LPVOID GetBufferPointer();
			*/
			char* msg = (char*)pErrorMsg->GetBufferPointer();
			UD9_RELEASE(pErrorMsg);

			UNG_EXCEPTION(msg);
		}

		auto ret = pActiveDevice->CreateVertexShader((DWORD*)pShader->GetBufferPointer(), &mShader);

		BOOST_ASSERT(ret == D3D_OK);

		UD9_RELEASE(pShader);
	}

	IDirect3DVertexShader9* D9VertexShader::getShader() const
	{
		BOOST_ASSERT(mShader);

		return mShader;
	}

	//-------------------------------------------------------------------------

	D9PixelShader::D9PixelShader(String const & fullName) :
		D9Shader(fullName,ShaderType::ST_FRAG),
		mShader(nullptr)
	{
		auto fileExt = mParser->getExtension();

		BOOST_ASSERT(fileExt == String("frag"));
	}

	D9PixelShader::~D9PixelShader()
	{
		UD9_RELEASE(mShader);
	}

	Shader* D9PixelShader::clone() const
	{
		return nullptr;
	}

	void D9PixelShader::compile()
	{
		auto pActiveDevice = D9RenderSystem::getInstance()->getActiveDevice();

		ID3DXBuffer* pShader = nullptr;
		ID3DXBuffer* pErrorMsg = nullptr;

		auto source = mParser->getSource();
		auto size = mParser->getSize();

		/*
		ps_3_0
		*/
		auto profile = D3DXGetPixelShaderProfile(pActiveDevice);
		BOOST_ASSERT(profile);

		uint32 flags{ 0 };

#if UNG_DEBUGMODE
		flags |= D3DXSHADER_DEBUG;
#endif

		flags |= D3DXSHADER_PACKMATRIX_ROWMAJOR;

		D3DXCompileShader(source,size,NULL,NULL,"mainPS",profile,flags,&pShader,&pErrorMsg,&mConstantTable);

		if (pErrorMsg)
		{
			char* msg = (char*)pErrorMsg->GetBufferPointer();
			UD9_RELEASE(pErrorMsg);

			UNG_EXCEPTION(msg);
		}

		auto ret = pActiveDevice->CreatePixelShader((DWORD*)pShader->GetBufferPointer(), &mShader);

		BOOST_ASSERT(ret == D3D_OK);

		UD9_RELEASE(pShader);
	}

	IDirect3DPixelShader9* D9PixelShader::getShader() const
	{
		BOOST_ASSERT(mShader);

		return mShader;
	}
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE