#include "UOTSpatialManager.h"

#ifdef UOT_HIERARCHICAL_COMPILE

#include "UOTSceneQuery.h"

namespace ung
{
	OctreeSpatialManager::OctreeSpatialManager(ISpatialManagerFactory* creator,String const & managerName) :
		mCreator(creator),
		mName(managerName.empty() ? UniqueID().toString(): managerName),
		mMaxDepth(-1),
		mLooseK(2.0),
		mRootNode(UNG_NEW OctreeNode(nullptr))
	{
	}

	OctreeSpatialManager::~OctreeSpatialManager()
	{
		auto co = std::mem_fn(&OctreeNode::destroyChildren);
		mRootNode->postTraverseDepthFirstForDestroy(co);
		UNG_DEL(mRootNode);
	}

	ISpatialManagerFactory * OctreeSpatialManager::getCreator() const
	{
		return mCreator;
	}

	String const& OctreeSpatialManager::getFactoryIdentify() const
	{
		return mCreator->getIdentity();
	}

	SceneTypeMask OctreeSpatialManager::getFactoryMask() const
	{
		return mCreator->getMask();
	}

	String const & OctreeSpatialManager::getManagerName() const
	{
		return mName;
	}

	OctreeNode * OctreeSpatialManager::createNode(ISpatialNode * parent)
	{
		return nullptr;
	}

	void OctreeSpatialManager::destroyNode(ISpatialNode * node)
	{
	}

	OctreeNode * OctreeSpatialManager::getRootNode() const
	{
		return mRootNode;
	}

	OctreeNode * OctreeSpatialManager::placeObject(StrongIObjectPtr objectPtr)
	{
		auto suitableNode = mRootNode->push(objectPtr);

		suitableNode->attachObject(objectPtr);

		return suitableNode;
	}

	void OctreeSpatialManager::takeAwayObject(StrongIObjectPtr objectPtr)
	{
		auto attachedNode = objectPtr->getAttachedSpatialNode();
		if (!attachedNode)
		{
			return;
		}

		OctreeNode* attachedOctNode = static_cast<OctreeNode*>(attachedNode);
		attachedOctNode->pop(objectPtr);
	}

	StrongISceneQueryPtr OctreeSpatialManager::createRaySceneQuery(Ray const & ray)
	{
		std::shared_ptr<UotRaySceneQuery> rayQuery(UNG_NEW_SMART UotRaySceneQuery(ray,this));

		return rayQuery;
	}

	StrongISceneQueryPtr OctreeSpatialManager::createSphereSceneQuery(Sphere const & sphere)
	{
		std::shared_ptr<UotSphereSceneQuery> sphereQuery(UNG_NEW_SMART UotSphereSceneQuery(sphere, this));

		return sphereQuery;
	}

	StrongISceneQueryPtr OctreeSpatialManager::createAABBSceneQuery(AABB const & box)
	{
		std::shared_ptr<UotAABBSceneQuery> boxQuery(UNG_NEW_SMART UotAABBSceneQuery(box, this));

		return boxQuery;
	}

	StrongISceneQueryPtr OctreeSpatialManager::createCameraSceneQuery(Camera const & camera)
	{
		return StrongISceneQueryPtr();
	}

	void OctreeSpatialManager::init(AABB const & worldBox, int32 maxDepth)
	{
		mBox = worldBox;
		mMaxDepth = maxDepth;
	}
}//namespace ung

#endif//UOT_HIERARCHICAL_COMPILE