#include "UOTConfig.h"

#ifdef UOT_HIERARCHICAL_COMPILE

ung::real_type global_world_size{ 32.0 };
ung::int32 global_octree_max_depth{5};

#if UNG_DEBUGMODE
#pragma comment(lib,"UNG_d.lib")
#else
#pragma comment(lib,"UNG.lib")
#endif

#endif//UOT_HIERARCHICAL_COMPILE

/*
插件和UNG之间的关系：
插件依赖UNG的头文件，dll。
UNG不依赖插件，UNG和插件之间约定一个getSymbol()函数所需要的名字，UNG需要知道插件dll文件的
名字，以便在如下函数中以字符串的形式来使用：
PluginManager::getInstance().load("UOT.dll");
*/