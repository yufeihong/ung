#include "UD9VertexDeclaration.h"

#ifdef UD9_HIERARCHICAL_COMPILE

#include "UD9RenderSystem.h"
#include "UD9Device.h"
#include "UD9Utilities.h"

namespace ung
{
	D9VertexDeclaration::D9VertexDeclaration() :
		mD3D9Declaration(nullptr)
	{
	}

	D9VertexDeclaration::~D9VertexDeclaration()
	{
		UD9_RELEASE(mD3D9Declaration);
	}

	void D9VertexDeclaration::addElementFinish()
	{
		BOOST_ASSERT(!mAddVertexElementFlag);
		BOOST_ASSERT(!mD3D9Declaration);

		//结束标记
		mAddVertexElementFlag = true;

		auto pRS = D9RenderSystem::getInstance();

		auto pActiveDevice = pRS->getActiveDevice();

		//element个数
		auto elementCount = VertexDeclaration::getElementCount();

		//创建D3DVERTEXELEMENT9数组
#if UNG_DEBUGMODE
		D3DVERTEXELEMENT9* elemArray =
			AdapterAllocator::allocate<D3DVERTEXELEMENT9>(elementCount + 1, ALLOCATE_INFO);
#else
		D3DVERTEXELEMENT9* elemArray =
			AdapterAllocator::allocate<D3DVERTEXELEMENT9>(elementCount + 1);
#endif

		//填充数组
		auto its = mElementList.getIterators();
		auto i{ 0 };
		while (its.first != its.second)
		{
			auto& elem = elemArray[i];

			elem.Stream = its.first->getStream();
			elem.Offset = its.first->getOffset();
			elem.Type = ud9ToD3D9ElementType(its.first->getType());
			elem.Method = D3DDECLMETHOD_DEFAULT;
			elem.Usage = ud9ToD3D9ElementSemantic(its.first->getSemantic());
			elem.UsageIndex = its.first->getUsageIndex();
			
			++i;
			++its.first;
		}
		elemArray[elementCount] = D3DDECL_END();

		//获取IDirect3DVertexDeclaration9接口
		auto ret = pActiveDevice->CreateVertexDeclaration(elemArray, &mD3D9Declaration);
		BOOST_ASSERT(ret == D3D_OK);

		//销毁数组
		AdapterAllocator::deallocate<D3DVERTEXELEMENT9>(elemArray,elementCount);
	}

	/*const VertexElement & D9VertexDeclaration::addElement(uint32 source, uint32 offset, VertexElementSemantic semantic, VertexElementType theType, uint32 index)
	{
		return VertexDeclaration::addElement(source,offset,semantic,theType,index);
	}

	const VertexElement & D9VertexDeclaration::insertElement(uint32 atPosition, uint32 source, uint32 offset, VertexElementSemantic semantic, VertexElementType theType, uint32 index)
	{
		return VertexDeclaration::insertElement(atPosition, source, offset, semantic, theType, index);
	}

	void D9VertexDeclaration::removeElement(uint32 elem_index)
	{
		VertexDeclaration::removeElement(elem_index);
	}

	void D9VertexDeclaration::removeElement(VertexElementSemantic semantic, uint32 index)
	{
		VertexDeclaration::removeElement(semantic, index);
	}

	void D9VertexDeclaration::removeAllElements()
	{
		VertexDeclaration::removeAllElements();
	}

	void D9VertexDeclaration::modifyElement(uint32 elem_index, uint32 source, uint32 offset, VertexElementSemantic semantic, VertexElementType theType, uint32 index)
	{
		VertexDeclaration::modifyElement(elem_index, source, offset, semantic, theType, index);
	}*/

	void D9VertexDeclaration::set()
	{
		BOOST_ASSERT(mAddVertexElementFlag);

		BOOST_ASSERT(mD3D9Declaration);

		auto pRS = D9RenderSystem::getInstance();

		auto pActiveDevice = pRS->getActiveDevice();

		auto ret = pActiveDevice->SetVertexDeclaration(mD3D9Declaration);
		BOOST_ASSERT(ret == D3D_OK);
	}
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE