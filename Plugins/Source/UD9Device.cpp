#include "UD9Device.h"

#ifdef UD9_HIERARCHICAL_COMPILE

#include "UD9RenderSystem.h"
#include "UD9Adapter.h"
#include "UD9AdapterManager.h"
#include "UD9Window.h"
#include "UD9DisplayMode.h"

namespace ung
{
	HWND D9Device::mSharedFocusWindow = nullptr;

	D9Device::D9Device(D3DDEVTYPE devType, UINT adapterOridinalNumber,HMONITOR hMonitor) :
		mDeviceType(devType),
		mDevice(nullptr),
		mAdapterOridinalNumberWhichDeviceBelongTo(adapterOridinalNumber),
		mMonitorWhichDeviceBelongTo(hMonitor),
		mIsLost(false),
		mFocusWindow(nullptr)
	{
		SecureZeroMemory(&mDescOfDeviceFeatures, sizeof(mDescOfDeviceFeatures));
		SecureZeroMemory(&mCaps, sizeof(mCaps));
		SecureZeroMemory(&mCreationParameters, sizeof(mCreationParameters));
	}

	D9Device::~D9Device()
	{
		BOOST_ASSERT(D9RenderSystem::getInstance()->getIDirect3D9());

		/*
		mWindows不拥有指针，所以，这里不调用deleteAll()函数进行析构。
		*/
		mWindows.clear();

		UD9_RELEASE(mDevice);
	}

	uint32 D9Device::getAdapterOridinalNumber() const
	{
		return mAdapterOridinalNumberWhichDeviceBelongTo;
	}

	D3DDEVTYPE D9Device::getType() const
	{
		BOOST_ASSERT(mDeviceType == D3DDEVTYPE_HAL);

		return mDeviceType;
	}

	IDirect3DDevice9 * D9Device::getDevice() const
	{
		BOOST_ASSERT(mDevice);

		return mDevice;
	}

	void D9Device::attachRenderWindow(D9Window* window)
	{
		BOOST_ASSERT(window);

		mWindows.put(window);
	}

	void D9Device::detachRenderWindow(D9Window * window)
	{
		BOOST_ASSERT(window);

		mWindows.take(window);
	}

	D9Window* D9Device::getPrimaryWindow() const
	{
		auto pRS = D9RenderSystem::getInstance();
		auto pAdapterManager = pRS->getD9AdapterManager();

		auto beg = mWindows.begin();
		auto end = mWindows.end();

		while (beg != end)
		{
			auto win = *beg;
			HMONITOR monitor = win->getMonitor();
			D9Adapter* pAdapter = pAdapterManager->getAdapterFromMonitor(monitor);
			auto oridinalNumber = pAdapter->getOridinalNumber();

			if (oridinalNumber == D3DADAPTER_DEFAULT)
			{
				return win;
			}

			++beg;
		}
	}

	bool D9Device::isLost()
	{
		HRESULT hr;

		/*
		Device有没有丢失,就是通过这个函数来判定
		HRESULT TestCooperativeLevel();
		Reports the current cooperative-level status of the Direct3D device for a windowed or 
		full-screen application.
		(Get the state of the graphics device.)
		If the method succeeds,the return value is D3D_OK,indicating that the device is 
		operational(可使用的) and the calling application can continue.
		If the method fails,the return value can be one of the following values:
		D3DERR_DEVICELOST:
		The device is lost and cannot be reset yet.
		D3DERR_DEVICENOTRESET:
		The device is lost but can be restored by calling IDirect3DDevice9::Reset.
		D3DERR_DRIVERINTERNALERROR:
		An internal driver error has occurred,in this case,the application should terminate.
		If the device is lost but cannot be restored at the current time,
		IDirect3DDevice9::TestCooperativeLevel returns the D3DERR_DEVICELOST return code.
		This would be the case,for example,when a full-screen device has lost focus.If an 
		application detects a lost device,it should pause and periodically(周期性的) call 
		IDirect3DDevice9::TestCooperativeLevel until it receives a return value of 
		D3DERR_DEVICENOTRESET.The application may then attempt to reset the device by 
		calling IDirect3DDevice9::Reset and,if this succeeds,restore the necessary resources and 
		resume normal operation.Note that IDirect3DDevice9::Present will return 
		D3DERR_DEVICELOST if the device is either "lost" or "not reset".
		A call to IDirect3DDevice9::TestCooperativeLevel will fail if called on a different thread 
		than that used to create the device being reset.

		Calling IDirect3DDevice9::Reset causes all texture memory surfaces to be lost,managed 
		textures to be flushed from video memory,and all state information to be lost.Before 
		calling the IDirect3DDevice9::Reset method for a device,an application should release any 
		explicit render targets,depth stencil surfaces,additional swap chains,state blocks,and 
		D3DPOOL_DEFAULT resources associated with the device.
		*/
		hr = mDevice->TestCooperativeLevel();

		if (hr == D3DERR_DEVICELOST)
		{
			return true;
		}
		//The device is lost but we can reset and restore it.
		else if (hr == D3DERR_DEVICENOTRESET)
		{
			/*
			Resources in the D3DPOOL_DEFAULT memory pool need to be released prior to 
			invoking the Reset method,and need to be restored after invoking the Reset method.
			For resources in D3DPOOL_DEFAULT,we put the resource release code in onLostDevice 
			and the resource restoration code in onResetDevice.
			*/
			onLostDevice();

			/*
			Resets the type,size,and format of the swap chain.
			HRESULT Reset(D3DPRESENT_PARAMETERS* pPresentationParameters);
			Possible return values include: D3D_OK,D3DERR_DEVICELOST,
			D3DERR_DEVICEREMOVED,D3DERR_DRIVERINTERNALERROR,or 
			D3DERR_OUTOFVIDEOMEMORY.
			*/
			auto resetRet = reset();

			if (resetRet == D3D_OK)
			{
				return false;
			}
			else if (resetRet == D3DERR_DEVICELOST)
			{
				return true;
			}
			else if (resetRet == D3DERR_DRIVERINTERNALERROR)
			{
				UNG_EXCEPTION("Internal driver error.");

				PostQuitMessage(0);

				return true;
			}

			onResetDevice();

			//Not lost anymore.
			return false;
		}
		//Driver error,exit.
		else if (hr == D3DERR_DRIVERINTERNALERROR)
		{
			UNG_EXCEPTION("Internal driver error.");

			PostQuitMessage(0);

			return true;
		}

		return false;
	}

	void D9Device::onLostDevice()
	{
	}

	void D9Device::onResetDevice()
	{
	}

	HRESULT D9Device::reset()
	{
		auto pRS = D9RenderSystem::getInstance();

		auto activeWindow = dynamic_cast<D9Window*>(pRS->getActiveWindow());

		BOOST_ASSERT(activeWindow);

		buildPresentParameters(activeWindow);

		return mDevice->Reset(&mDescOfDeviceFeatures);
	}

	void D9Device::create()
	{
		auto pRS = D9RenderSystem::getInstance();
		auto pD9 = pRS->getIDirect3D9();

		auto primaryWindow = getPrimaryWindow();

		//Case we have to share the same focus window.
		if (mSharedFocusWindow)
		{
			mFocusWindow = mSharedFocusWindow;
		}
		else
		{
			mFocusWindow = primaryWindow->getHandle();
		}

		//The behavior of this device.
		DWORD behaviorFlags = 0;
		/*
		D3DCREATE_FPU_PRESERVE:
		Set the precision for Direct3D floating-point calculations to the precision used by the 
		calling thread. If you do not specify this flag, Direct3D defaults to single-precision 
		round-to-nearest mode for two reasons:
		Double-precision mode will reduce Direct3D performance.
		Portions of Direct3D assume floating-point unit exceptions are masked; unmasking these 
		exceptions may result in undefined behavior.
		D3DCREATE_ADAPTERGROUP_DEVICE:
		Application asks the device to drive all the heads that this master adapter owns. The flag 
		is illegal on nonmaster adapters. If this flag is set, the presentation parameters passed to 
		CreateDevice should point to an array of D3DPRESENT_PARAMETERS. The number of 
		elements in D3DPRESENT_PARAMETERS should equal the number of adapters defined by 
		the NumberOfAdaptersInGroup member of the D3DCAPS9 structure. The DirectX runtime 
		will assign each element to each head in the numerical order specified by the 
		AdapterOrdinalInGroup member of D3DCAPS9.
		D3DCREATE_MIXED_VERTEXPROCESSING:
		Specifies mixed (both software and hardware) vertex processing. For Windows 10, version 
		1607 and later, use of this setting is not recommended.
		D3DCREATE_PUREDEVICE:
		Specifies that Direct3D does not support Get* calls for anything that can be stored in 
		state blocks. It also tells Direct3D not to provide any emulation services for vertex 
		processing. This means that if the device does not support vertex processing, then the 
		application can use only post-transformed vertices.
		*/
		behaviorFlags &= ~D3DCREATE_ADAPTERGROUP_DEVICE;
		behaviorFlags |= D3DCREATE_HARDWARE_VERTEXPROCESSING;
		behaviorFlags |= D3DCREATE_PUREDEVICE;

		HRESULT hr;

		/*
		Creates a device to represent the display adapter.
		HRESULT CreateDevice(UINT Adapter,D3DDEVTYPE DeviceType,HWND hFocusWindow,
		DWORD BehaviorFlags,[in,out] D3DPRESENT_PARAMETERS* pPresentationParameters,
		IDirect3DDevice9** ppReturnedDeviceInterface);
		This method returns a fully working device interface,set to the required display mode 
		(or windowed),and allocated with the appropriate back buffers.To begin rendering, the 
		application needs only to create and set a depth buffer (assuming EnableAutoDepthStencil 
		is FALSE in D3DPRESENT_PARAMETERS).
		When you create a Direct3D device, you supply two different window parameters:
		a focus window (hFocusWindow) and a device window (the hDeviceWindow in 
		D3DPRESENT_PARAMETERS). The purpose of each window is:
		The focus window alerts Direct3D when an application switches from foreground mode to 
		background mode (via Alt-Tab, a mouse click, or some other method).A single focus 
		window is shared by each device created by an application.
		The device window determines the location and size of the back buffer on screen.This is 
		used by Direct3D when the back buffer contents are copied to the front buffer during 
		Present.
		This method should not be run during the handling of WM_CREATE. An application should 
		never pass a window handle to Direct3D while handling WM_CREATE. Any call to create,
		release, or reset the device must be done using the same thread as the window procedure 
		of the focus window.
		Back buffers created as part of the device are only lockable if 
		D3DPRESENTFLAG_LOCKABLE_BACKBUFFER is specified in the presentation parameters.
		(Multisampled back buffers and depth surfaces are never lockable.)
		The methods Reset, IUnknown, and TestCooperativeLevel must be called from the same 
		thread that used this method to create a device.
		D3DFMT_UNKNOWN can be specified for the windowed mode back buffer format when 
		calling CreateDevice, Reset, and CreateAdditionalSwapChain. This means the application 
		does not have to query the current desktop format before calling CreateDevice for 
		windowed mode. For full-screen mode, the back buffer format must be specified.
		If you attempt to create a device on a 0x0 sized window, CreateDevice will fail.
		Adapter:
		Ordinal number that denotes the display adapter.D3DADAPTER_DEFAULT is always the 
		primary display adapter.
		DeviceType:
		Member of the D3DDEVTYPE enumerated type that denotes the desired device type.If the 
		desired device type is not available,the method will fail.
		hFocusWindow:
		The focus window alerts Direct3D when an application switches from foreground mode to 
		background mode.
		For full-screen mode, the window specified must be a top-level window.
		For windowed mode, this parameter may be NULL only if the hDeviceWindow member of 
		pPresentationParameters is set to a valid, non-NULL value.
		BehaviorFlags:
		Combination of one or more options that control device creation.see D3DCREATE.
		当使用HAL设备时，可在任何时间在软件顶点处理和硬件顶点处理之间进行切换，HAL设备是一种同时
		支持软件顶点处理和硬件顶点处理的设备类型。对于软件顶点处理唯一的要求是，顶点缓冲区必须位于
		系统内存。
		pPresentationParameters[in,out]:
		Pointer to a D3DPRESENT_PARAMETERS structure,describing the presentation parameters 
		for the device to be created.If BehaviorFlags specifies 
		D3DCREATE_ADAPTERGROUP_DEVICE,pPresentationParameters is an array.Regardless of 
		the number of heads that exist,only one depth/stencil surface is automatically created.
		An unsupported refresh rate will default to the closest supported refresh rate below it.For 
		example,if the application specifies 63 hertz,60 hertz will be used.There are no supported 
		refresh rates below 57 hertz.
		pPresentationParameters is both an input and an output parameter.Calling this method 
		may change several members including:
		If BackBufferCount,BackBufferWidth,and BackBufferHeight are 0 before the method is 
		called,they will be changed when the method returns.
		If BackBufferFormat equals D3DFMT_UNKNOWN before the method is called, it will be 
		changed when the method returns.
		ppReturnedDeviceInterface[out,retval]:
		Address of a pointer to the returned IDirect3DDevice9 interface, which represents the 
		created device.
		*/
		hr = pD9->CreateDevice(mAdapterOridinalNumberWhichDeviceBelongTo,mDeviceType,mFocusWindow,
			behaviorFlags,&mDescOfDeviceFeatures,&mDevice);

		/*
		If hardware vertex processing is supported,you can actually use 
		D3DCREATE_MIXED_VERTEXPROCESSING,which allows you to switch between hardware 
		vertex processing and software vertex processing at run time.
		To switch,you use the following method:
		HRESULT SetSoftwareVertexProcessing(BOOL bSoftware);
		*/

		if (FAILED(hr))
		{
			/*
			Try a second time,may fail the first time due to back buffer count,which will be 
			corrected down to 1 by the runtime.
			*/
			hr = pD9->CreateDevice(mAdapterOridinalNumberWhichDeviceBelongTo,mDeviceType,mFocusWindow,
				behaviorFlags, &mDescOfDeviceFeatures, &mDevice);

			//Case hardware vertex processing failed.
			if (FAILED(hr))
			{
				//Try to create the device with mixed vertex processing.
				behaviorFlags &= ~D3DCREATE_HARDWARE_VERTEXPROCESSING;
				behaviorFlags &= ~D3DCREATE_PUREDEVICE;
				behaviorFlags |= D3DCREATE_MIXED_VERTEXPROCESSING;

				hr = pD9->CreateDevice(mAdapterOridinalNumberWhichDeviceBelongTo, mDeviceType, mFocusWindow,
					behaviorFlags, &mDescOfDeviceFeatures, &mDevice);

				if (FAILED(hr))
				{
					//try to create the device with software vertex processing.
					behaviorFlags &= ~D3DCREATE_MIXED_VERTEXPROCESSING;
					behaviorFlags |= D3DCREATE_SOFTWARE_VERTEXPROCESSING;
					hr = pD9->CreateDevice(mAdapterOridinalNumberWhichDeviceBelongTo, mDeviceType, mFocusWindow,
						behaviorFlags, &mDescOfDeviceFeatures, &mDevice);

					if (FAILED(hr))
					{
						//try reference device
						mDeviceType = D3DDEVTYPE_REF;
						hr = pD9->CreateDevice(mAdapterOridinalNumberWhichDeviceBelongTo, mDeviceType, mFocusWindow,
							behaviorFlags, &mDescOfDeviceFeatures, &mDevice);

						if (FAILED(hr))
						{
							UD9_RELEASE(pD9);

							UNG_EXCEPTION("Can not create D3D9 device.");
						}
					}
				}
			}
		}

		//Get current device caps.
		hr = mDevice->GetDeviceCaps(&mCaps);
		if (FAILED(hr))
		{
			UNG_EXCEPTION("Can not get device caps.");
		}

		/*
		Retrieves the creation parameters of the device.
		HRESULT GetCreationParameters(D3DDEVICE_CREATION_PARAMETERS* pParameters);
		You can query the AdapterOrdinal member of the returned D3DDEVICE_CREATION_PARAMETERS 
		structure to retrieve the ordinal of the adapter represented by this device.
		*/
		hr = mDevice->GetCreationParameters(&mCreationParameters);
		if (FAILED(hr))
		{
			UNG_EXCEPTION("Failed get creation parameters.");
		}
	}

	bool D9Device::isUsedInFullScreen() const
	{
		return !mDescOfDeviceFeatures.Windowed;
	}

	bool D9Device::isSupportHardwardVertexProcess() const
	{
		BOOST_ASSERT(mDevice);

		if (mCaps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT)
		{
			return true;
		}

		return false;
	}

	bool D9Device::isPure() const
	{
		BOOST_ASSERT(mDevice);

		if (mCaps.DevCaps & D3DDEVCAPS_PUREDEVICE)
		{
			return true;
		}

		return false;
	}

	void D9Device::buildPresentParameters(D9Window* window)
	{
		BOOST_ASSERT(window);

		//渲染系统
		auto pRS = D9RenderSystem::getInstance();

		//D3D9接口对象
		auto pD9 = pRS->getIDirect3D9();

		D3DCAPS9 caps;
		pD9->GetDeviceCaps(mAdapterOridinalNumberWhichDeviceBelongTo, mDeviceType, &caps);

		auto fullScreen = window->isFullScreen();
		auto pixelFormat = window->getPixelFormat();
		auto depthStencilFormat = window->getDepthStencilFormat();

		D3DSWAPEFFECT se = D3DSWAPEFFECT_DISCARD;

		D3DMULTISAMPLE_TYPE mst = D3DMULTISAMPLE_8_SAMPLES;
		DWORD msq = 0;
		/*
		Determines if a multisampling technique is available on this device.
		HRESULT CheckDeviceMultiSampleType(UINT Adapter,D3DDEVTYPE DeviceType,
		D3DFORMAT SurfaceFormat,BOOL Windowed,D3DMULTISAMPLE_TYPE MultiSampleType,
		DWORD* pQualityLevels);
		Adapter:
		Ordinal number denoting the display adapter to query.D3DADAPTER_DEFAULT is always 
		the primary display adapter.This method returns FALSE when this value equals or exceeds 
		the number of display adapters in the system.
		DeviceType:
		Member of the D3DDEVTYPE enumerated type,identifying the device type.
		SurfaceFormat:
		Member of the D3DFORMAT enumerated type that specifies the format of the surface to 
		be multisampled.
		Windowed:
		Specify TRUE to inquire about windowed multisampling,and specify FALSE to inquire about 
		full-screen multisampling.
		MultiSampleType:
		Member of the D3DMULTISAMPLE_TYPE enumerated type,identifying the multisampling 
		technique to test.
		pQualityLevels:
		pQualityLevels returns the number of device-specific sampling variations available with 
		the given sample type.For example,if the returned value is 3,then quality levels 0,1 and 2 
		can be used when creating resources with the given sample count.The meanings of these 
		quality levels are defined by the device manufacturer and cannot be queried through D3D.
		For example,for a particular device different quality levels at a fixed sample count might 
		refer to different spatial layouts of the sample locations or different methods of resolving.
		This can be NULL if it is not necessary to return the quality levels.
		If the device can perform the specified multisampling method,this method returns 
		D3D_OK.D3DERR_INVALIDCALL is returned if the Adapter or MultiSampleType parameters 
		are invalid.This method returns D3DERR_NOTAVAILABLE if the queried multisampling 
		technique is not supported by this device.D3DERR_INVALIDDEVICE is returned if 
		DeviceType does not apply to this adapter.
		This method is intended for use with both render-target and depth-stencil surfaces 
		because you must create both surfaces multisampled if you want to use them together.
		It is not good practice to switch from one multisample type to another to raise the quality 
		of the antialiasing.
		D3DMULTISAMPLE_NONE enables swap effects other than discarding,locking,and so on.
		*/
		if (se != D3DSWAPEFFECT_DISCARD)
		{
			mst = D3DMULTISAMPLE_NONE;
			msq = 0;
		}
		else
		{
			/*
			We test for hardware multisampling support using both a back buffer format and a 
			depth buffer format.We have to test both since both the back buffer and depth buffer 
			need to be multisampled if they are to work simultaneously.
			*/
#define CHECK_DEVICE_MST																			\
if (FAILED(pD9->CheckDeviceMultiSampleType(caps.AdapterOrdinal,					\
			caps.DeviceType, pixelFormat, !fullScreen, mst, &msq)) ||						\
	FAILED(pD9->CheckDeviceMultiSampleType(caps.AdapterOrdinal,					\
		caps.DeviceType, depthStencilFormat, !fullScreen, mst, &msq)))

			CHECK_DEVICE_MST
			{
				mst = D3DMULTISAMPLE_4_SAMPLES;

				CHECK_DEVICE_MST
				{
					mst = D3DMULTISAMPLE_2_SAMPLES;

					CHECK_DEVICE_MST
					{
						mst = D3DMULTISAMPLE_NONE;
					}
				}
			}
		}

		mDescOfDeviceFeatures.Windowed = !fullScreen;
		mDescOfDeviceFeatures.BackBufferCount = window->isVerticalSync() ? 2 : 1;
		mDescOfDeviceFeatures.BackBufferWidth = window->getWidth();
		mDescOfDeviceFeatures.BackBufferHeight = window->getHeight();
		mDescOfDeviceFeatures.BackBufferFormat = pixelFormat;
		/*
		FSAA(FullSceneAnti-aliasing)全屏抗锯齿
		Multisampling is supported only if the swap effect is D3DSWAPEFFECT_DISCARD.
		*/
		mDescOfDeviceFeatures.MultiSampleType = mst;
		/*
		The number of quality levels for the specified multi-sample type(ranges from 0 to n - 1,
		where n is the value returned by the pQualityLevels parameter)
		记住，这里要减去1。
		*/
		mDescOfDeviceFeatures.MultiSampleQuality = msq - 1;
		/*
		Set the SwapEffect to "discard", which is the most efficient method of presenting the back 
		buffer to the display.
		*/
		mDescOfDeviceFeatures.SwapEffect = se;
		/*
		The device window determines the location and size of the back buffer on screen.
		This is used by Direct3D when the back buffer contents are copied to the front buffer during
		Present.
		For a full-screen application, this is a handle to the top window (which is the focus window).
		For applications that use multiple full-screen devices (such as a multimonitor system),exactly
		one device can use the focus window as the device window. All other devices must have unique
		device windows.
		For a windowed-mode application, this handle will be the default target window for Present. If
		this handle is NULL, the focus window will be taken.
		*/
		mDescOfDeviceFeatures.hDeviceWindow = window->getHandle();
		mDescOfDeviceFeatures.EnableAutoDepthStencil = false;
		mDescOfDeviceFeatures.AutoDepthStencilFormat = depthStencilFormat;
		mDescOfDeviceFeatures.Flags = 0;
		/*
		For windowed mode, the refresh rate must be 0.
		For full-screen mode, the refresh rate is one of the refresh rates returned by 
		EnumAdapterModes.
		*/
		UINT fullScreenRefreshRate = 0;
		if (fullScreen)
		{
			fullScreenRefreshRate = window->getFullScreenDisplayMode()->getRefreshRate();
		}
		mDescOfDeviceFeatures.FullScreen_RefreshRateInHz = fullScreenRefreshRate;
		/*
		D3DPRESENT_INTERVAL_IMMEDIATE:
		This is nearly equivalent to D3DPRESENT_INTERVAL_ONE.
		D3DPRESENT_INTERVAL_ONE:
		The driver will wait for the vertical retrace period(等待垂直折返/重描周期) (the runtime will 
		"beam(光线，波束) follow" to prevent tearing(防止撕裂)). Present operations will not be 
		affected more frequently than the screen refresh; the runtime will complete at most one 
		Present operation per adapter refresh period. This is equivalent to using 
		D3DSWAPEFFECT_COPYVSYNC in DirectX 8.1. This option is always available for both 
		windowed and full-screen swap chains.
		*/
		mDescOfDeviceFeatures.PresentationInterval = D3DPRESENT_INTERVAL_ONE;

		//Check that the interval was supported,revert to 1 to be safe otherwise
		if (!(caps.PresentationIntervals & mDescOfDeviceFeatures.PresentationInterval))
		{
			mDescOfDeviceFeatures.PresentationInterval = D3DPRESENT_INTERVAL_ONE;
		}
	}

	void D9Device::setSharedFocusWindow(HWND sfw)
	{
		mSharedFocusWindow = sfw;
	}

	HWND D9Device::getSharedFocusWindow()
	{
		return mSharedFocusWindow;
	}
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE

/*
Direct3D Devices (Direct3D 9):
A Direct3D device is the rendering component of Direct3D. It encapsulates and stores the 
rendering state. In addition, a Direct3D device performs transformations and lighting 
operations and rasterizes an image to a surface.
Architecturally, Direct3D devices contain a transformation module, a lighting module, and a 
rasterizing module, as the following diagram shows.
如图：Knowledge/DX/002.png所示。
*/

/*
Direct3D设备对象相当于GDI程序中的hdc(设备描述表)，使用GDI绘制图形前，通常需要先利用hdc进行
相关的渲染设备设置，然后通过hdc进行绘制。同样在Direct3D程序中通常先通过设备接口进行渲染设备
设置，然后再渲染图形。而且所有的渲染图形操作必须在函数BeginScene()和EndScene()之间进行。

DirectX技术与Windows固有的GDI相比，速度要快很多倍，且更健壮。
*/

/*
Direct3D渲染设备有两种状态：工作状态（operational state）和丢失状态（lost state）。
在某些情况下，设备会由工作状态转到丢失状态。例如：在全屏运行方式下，Direct3D程序失去键盘焦点时，
Direct3D渲染设备就会丢失。需要注意的是，当设备处于丢失状态时，即使渲染操作失败，所有的图形渲染
操作也会返回成功代码，而IDirect3DDevice9::Present()接口函数(该函数在所有渲染操作结束后调用)会
返回错误代码D3DERR_DEVICELOST。
导致设备丢失最典型的情况是：图形显示丢失焦点，例如，用户按下Alt+Tab键，或弹出一个系统对话框，或
其他程序试图进行全屏幕操作时。
此外，调用IDirect3DDevice9::Reset()接口函数的任何错误也都会导致设备丢失。
设备丢失后，所有继承自接口IUnknown的方法仍然可以正常工作。
1：处理设备丢失：
在重新设置（reset）丢失的设备之前，必须重新创建相关资源（包括视频内存资源）。在设备丢失后，程序
应当查询是否可以将设备恢复（restore）到工作状态，如果不能，则等待直到设备可以恢复。
如果丢失的设备可以恢复，则在恢复之前程序需要销毁所有的显存资源和所有的交换链（swap chains），然
后调用IDirect3DDevice9::Reset()接口函数恢复设备。Reset()函数是设备丢失后设备对象唯一可以调用的
函数，也是将设备由丢失状态恢复到工作状态的唯一函数。但是，一旦应用程序释放了所有的
D3DPOOL_DEFAULT内存资源，包括由函数IDirect3DDevice9::CreateRenderTarget()和
IDirect3DDevice9::CreateDepthStencilSurface()创建的资源，Reset()的调用也将失败。
大多数情况下，Direct3D设备对象的许多方法并不返回设备是否丢失的信息，程序可以继续调用图形渲染函数，
例如：IDirect3DDevice9::DrawPrimitive()，而不会接到任何设备已经丢失的信息。在内部，这些操作都
会被抛弃，直到设备恢复到工作状态。
在设备丢失后，程序可以通过调用IDirect3DDevice9::TestCooperativeLevel()接口函数，确定设备是否能
够恢复。若返回D3D_OK代码，表示设备可以恢复，若返回D3DERR_DEVICELOST()代码，表示目前设备
还不能恢复到工作状态。
2：锁定操作：
设备丢失后，Direct3D在内部已经做了大量的工作确保锁定操作成功。但是，不能保证在锁定操作期间显存
内容是准确无误的，只能保证不会返回错误代码。这样应用程序就可以继续工作，而不管设备是否丢失（当然，
实际上不会渲染出任何图形）。
3：资源：
资源会消耗视频内存（video memory）。因为一个丢失的设备不再和显卡拥有的视频内存相关联，所以当设
备丢失后不可能保证为资源分配视频内存。结果是，尽管所有资源的创建方法都会执行成功并返回D3D_OK，
但是实际上仅仅是为资源分配系统内存。因为任何视频内存资源在设备丢失时必须销毁，所以不会出现重复分
配视频内存的问题。这些假的表面资源看上去允许正常进行锁定和复制操作，直到调用Present()函数发现设
备已经丢失。
在设备从丢失状态恢复到正常状态前，所有的视频内存必须释放。这意味着应用程序需要释放所有由
CreateAdditionalSwapChain()函数创建的交换链和所有D3DPOOL_DEFAULT内存类型的资源。应用程序
不需要释放D3DPOOL_MANAGED和D3DPOOL_SYSTEMMEM内存类型的资源。在设备状态变换时其他状态
数据会自动销毁。
4：返回数据：
Direct3D允许程序调用IDirect3DDevice9::ValidateDevice()函数来确认硬件一次渲染纹理混合和渲染状态
的能力。该方法通常在应用程序初始化时调用，如果设备丢失，则返回D3DERR_DEVICELOST。
Direct3D也允许应用程序从视频内存资源复制生成的或以前写过的图像数据到系统内存资源。因为视频内存资源
可能会丢失，所以当设备丢失时，Direct3D允许这种复制操作失败。
考虑到不同时的查询，如果调用IDirect3DQuery9::GetData()时，可指定FLUSH标志，则返回
D3DERR_DEVICELOST，以通知应用程序IDirect3DQuery9::GetData()永远不会返回S_OK。
既然当设备丢失时没有主表面，调用IDirect3DDevice9::GetFrontBufferData()复制前台缓冲区数据时会失
败。因为设备丢失时，不能创建后台缓冲区，所以调用IDirect3DDevice9::CreateAdditionalSwapChain()
也会失败。这些情况除了IDirect3DDevice9::Present(),IDirect3DDevice9::TestCooperativeLevel(),
IDirect3DDevice9::Reset()外只会在设备丢失时出现。
5：可编程渲染器：
在Direct3D9中，设备从丢失状态恢复到正常状态时，顶点渲染器和像素渲染器不需要重新创建。Direct3D设
备能自动恢复它们。
*/