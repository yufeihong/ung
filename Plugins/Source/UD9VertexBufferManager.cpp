#include "UD9VertexBufferManager.h"

#ifdef UD9_HIERARCHICAL_COMPILE

#include "UD9RenderSystem.h"
#include "UD9VertexDeclaration.h"
#include "UD9VertexBuffer.h"

namespace ung
{
	D9VertexBufferManager::D9VertexBufferManager()
	{
	}

	D9VertexBufferManager::~D9VertexBufferManager()
	{
		BOOST_ASSERT(D9RenderSystem::getD9DeviceManager());
	}

	SeparatedPointer<VertexDeclaration> D9VertexBufferManager::createDeclaration()
	{
		SeparatedPointer<D9VertexDeclaration> p;

		p.create();

		return p;
	}

	SeparatedPointer<VertexBuffer> D9VertexBufferManager::createVertexBuffer(uint32 vertexSize,uint32 numVertices, uint32 usage, bool useShadow)
	{
		SeparatedPointer<D9VertexBuffer> vb;

		vb.create(vertexSize, numVertices,usage, useShadow);

		return vb;
	}
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE