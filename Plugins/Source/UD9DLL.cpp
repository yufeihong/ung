#include "UD9DLL.h"

#ifdef UD9_HIERARCHICAL_COMPILE

#include "UD9Plugin.h"
#include "UD9RenderSystem.h"

namespace ung
{
	static D9Plugin* plugin{};

	void dllCreatePlugin() noexcept
	{
		plugin = UNG_NEW D9Plugin;

		//UNG_PRINT_ONE_LINE("D3D9插件已经创建。");

		PluginManager::getInstance().install(plugin);
	}

	void dllDestroyPlugin()
	{
		PluginManager::getInstance().uninstall(plugin);
		UNG_DEL(plugin);

		//UNG_PRINT_ONE_LINE("D3D9插件已经销毁。");
	}
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE