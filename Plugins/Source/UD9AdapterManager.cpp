#include "UD9AdapterManager.h"

#ifdef UD9_HIERARCHICAL_COMPILE

#include "UFCSTLWrapper.h"
#include "UD9RenderSystem.h"
#include "UD9Adapter.h"
#include "UD9DisplayMode.h"

namespace ung
{
	D9AdapterManager::D9AdapterManager()
	{
		enumerateAdapter();

		/*
		设置active adapter
		D3DADAPTER_DEFAULT:the one displaying the Windows desktop.
		*/
		mActiveAdapter = getAdapterFromOridinalNumber(D3DADAPTER_DEFAULT);
	}

	D9AdapterManager::~D9AdapterManager()
	{
		auto lambdaExpress = [](D9Adapter*& pAdapter)
		{
			UNG_DEL(pAdapter);
		};
		ungContainerLoop(mAdapters, lambdaExpress);

		ungContainerClear(mAdapters);
	}

	bool D9AdapterManager::enumerateAdapter()
	{
		/*
		A display adapter refers to a physical graphics card,although a graphics card with a 
		dual-head display has more than one adapter.Because a system may have more than one 
		adapter (via multiple video cards or dual-head displays),a commercial Direct3D program 
		should enumerate all available adapters and pick the best one for the application.
		*/

		auto pD3D9 = D9RenderSystem::getIDirect3D9();

		//遍历系统上的全部适配器(GetAdapterCount():Returns the number of adapters on the system(比如:1))
		auto adapterCount = pD3D9->GetAdapterCount();
		for(UINT i=0; i < adapterCount; ++i)
		{
			D3DADAPTER_IDENTIFIER9 adapterIdentifier;
			D3DDISPLAYMODE d3ddm;
			D3DCAPS9 d3dcaps9;

			/*
			获取该适配器的标识符，标识符包含对该适配器的描述信息。

			HRESULT GetAdapterIdentifier(UINT Adapter,DWORD Flags,D3DADAPTER_IDENTIFIER9 *pIdentifier);
			Describes the physical display adapters present in the system when the IDirect3D9 
			interface was instantiated.
			Adapter:
			Type:UINT
			Ordinal number that denotes the display adapter.D3DADAPTER_DEFAULT is always the 
			primary display adapter.The minimum value for this parameter is 0,and the maximum 
			value for this parameter is one less than the value returned by GetAdapterCount.
			Flags:
			Type:DWORD
			Flags sets the WHQLLevel member of D3DADAPTER_IDENTIFIER9. Flags can be set to 
			either 0 or D3DENUM_WHQL_LEVEL. If D3DENUM_WHQL_LEVEL is specified, this call 
			can connect to the Internet to download new Microsoft Windows Hardware Quality Labs 
			(WHQL) certificates.
			Differences between Direct3D 9 and Direct3D 9Ex:
			D3DENUM_WHQL_LEVEL is deprecated for Direct3D9Ex running on Windows Vista,
			Windows Server 2008, Windows 7, and Windows Server 2008 R2 (or more current 
			operating system). Any of these operating systems return 1 in the WHQLLevel member 
			of D3DADAPTER_IDENTIFIER9 without checking the status of the driver.
			pIdentifier:
			Type:D3DADAPTER_IDENTIFIER9*
			Pointer to a D3DADAPTER_IDENTIFIER9 structure to be filled with information 
			describing this adapter. If Adapter is greater than or equal to the number of adapters in 
			the system, this structure will be zeroed.
			If the method succeeds, the return value is D3D_OK. D3DERR_INVALIDCALL is returned 
			if Adapter is out of range, if Flags contains unrecognized parameters, or if pIdentifier is 
			NULL or points to unwriteable memory.
			*/
			auto idRet = pD3D9->GetAdapterIdentifier(i,0,&adapterIdentifier);
			BOOST_ASSERT(idRet == D3D_OK);

			/*
			获取该适配器的显示模式。

			HRESULT GetAdapterDisplayMode(UINT Adapter,D3DDISPLAYMODE *pMode);
			Retrieves the current[当前] display mode of the adapter.
			IDirect3D9::GetAdapterDisplayMode can be used to determine the desktop display 
			format.
			Adapter:
			Type:UINT
			Ordinal number that denotes the display adapter to query.D3DADAPTER_DEFAULT is 
			always the primary display adapter.
			pMode:
			Type:D3DDISPLAYMODE*
			Pointer to a D3DDISPLAYMODE structure,to be filled with information describing the 
			current adapter's mode.
			If the method succeeds, the return value is D3D_OK.
			If Adapter is out of range or pMode is invalid, this method returns D3DERR_INVALIDCALL.
			GetAdapterDisplayMode will not return the correct format when the display is in an 
			extended format, such as 2:10:10:10. Instead, it returns the format X8R8G8B8.
			*/
			auto dmRet = pD3D9->GetAdapterDisplayMode(i,&d3ddm);
			BOOST_ASSERT(dmRet == D3D_OK);

			/*
			根据主显卡的性能参数来填充D3DCAPS9结构。
			HRESULT GetDeviceCaps(UINT Adapter,D3DDEVTYPE DeviceType,D3DCAPS9* pCaps);
			Retrieves device-specific information about a device.
			Adapter:
			Ordinal number(序数) that denotes(表示) the display adapter(显示适配器)
			D3DADAPTER_DEFAULT is always the primary display adapter.
			DeviceType:
			Member of the D3DDEVTYPE enumerated type. Denotes the device type.
			硬件设备：D3DDEVTYPE_HAL
			软件设备：D3DDEVTYPE_REF
			pCaps:
			Pointer to a D3DCAPS9 structure to be filled with information describing the capabilities of the device.
			*/
			pD3D9->GetDeviceCaps(i,D3DDEVTYPE_HAL,&d3dcaps9);

			//构造一个Adapter
			D9Adapter* pAdapter = UNG_NEW D9Adapter(i, adapterIdentifier, d3ddm, d3dcaps9);

			//For each adapter,enumerates the supported display modes by calling IDirect3D9::EnumAdapterModes.
			D9DisplayModeCollection* displayModes = pAdapter->getDisplayModeCollection();

			mAdapters.push_back(pAdapter);
		}

		return true;
	}

	uint32 D9AdapterManager::getCount() const
	{
		return mAdapters.size();
	}

	D9Adapter * D9AdapterManager::getActiveAdapter() const
	{
		return mActiveAdapter;
	}

	D9Adapter * D9AdapterManager::getAdapterFromOridinalNumber(uint32 oridinalNumber) const
	{
		auto beg = mAdapters.begin();
		auto end = mAdapters.end();

		while (beg != end)
		{
			auto ap = *beg;
			auto on = ap->getOridinalNumber();

			if (on == oridinalNumber)
			{
				return ap;
			}

			++beg;
		}

		return nullptr;
	}

	D9Adapter * D9AdapterManager::getAdapterFromMonitor(HMONITOR monitor) const
	{
		auto pD9 = D9RenderSystem::getIDirect3D9();

		auto beg = mAdapters.begin();
		auto end = mAdapters.end();

		while (beg != end)
		{
			auto ap = *beg;
			auto on = ap->getOridinalNumber();

			if (pD9->GetAdapterMonitor(on) == monitor)
			{
				return ap;
			}

			++beg;
		}

		return nullptr;
	}
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE