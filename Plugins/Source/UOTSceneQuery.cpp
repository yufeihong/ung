#include "UOTSceneQuery.h"

#ifdef UOT_HIERARCHICAL_COMPILE

#include "UOTNode.h"

namespace ung
{
	UotRaySceneQuery::UotRaySceneQuery(Ray const & ray,ISpatialManager* spatialManager) :
		SingleSceneQuery(spatialManager),
		mRay(ray)
	{
	}

	UotRaySceneQuery::~UotRaySceneQuery()
	{
	}

	SingleSceneQueryResults* UotRaySceneQuery::execute()
	{
		//射线的起点
		auto original = mRay.getOrigin();
		//射线的方向
		auto dir = mRay.getDirection();

		//八叉树根节点
		auto octRootNode = dynamic_cast<OctreeNode*>(mSpatialManager->getRootNode());
		BOOST_ASSERT(octRootNode);

		//给射线的起点，找到一个合适的八叉树节点
		auto originalNode = octRootNode->findSuitableNode(original);

		OctreeNode* nextNode = originalNode;

		auto ret{ false };
		Vector3 out{};
		StrongIObjectPtr theObject{};

		while (nextNode)
		{
			//如果该节点有对象的话
			if (nextNode->hasAttachedObject())
			{
				//遍历该节点中的所有对象
				auto its = nextNode->getObjectsConstIterators();

				auto beg = its.first;
				auto end = its.second;

				while (beg != end)
				{
					//对象
					auto object = *beg;

					//对象的模型空间的AABB
					auto objectBox = object->getAABB();
					//对象到世界空间的变换的逆变换
					auto toModelSpaceOfObject = object->getTransformComponent()->getToWorldMatrixInverse();

					//变换射线到对象的世界坐标
					Ray rayInModelSpaceOfObject = mRay * toModelSpaceOfObject;

					//碰撞检测
					ret = CollisionDetection::intersects(rayInModelSpaceOfObject, objectBox, out);
					if (ret)
					{
						theObject = object;

						break;
					}

					++beg;
				}//end while (beg != end)
			}//if (nextNode->hasAttachedObject())

			//该节点的检查完了
			if (ret)
			{
				mResults->mObject = theObject;
				mResults->mDistance = (out - original).length();

				break;
			}
			else
			{
				//找到射线和当前节点的AABB的碰撞点位于哪一个面上
				int32 entryIndx{ -1 }, exitIndex{ -1 };

				//当前节点位于世界空间的AABB
				auto nodeBox = nextNode->getAABB();

				//此时，射线和nodeBox均位于世界空间
				auto bRet = CollisionDetection::intersects(mRay, nodeBox, out, entryIndx, exitIndex);
				BOOST_ASSERT(bRet);
				BOOST_ASSERT(exitIndex != -1);

				//得到邻居节点
				nextNode = nextNode->getNeighbor(exitIndex);
			}
		}//end while (nextNode->hasAttachedObject())

		return mResults;
	}

	void UotRaySceneQuery::setRay(Ray const & ray)
	{
		mRay = ray;
	}

	//--------------------------------------------------------------

	UotSphereSceneQuery::UotSphereSceneQuery(Sphere const & sphere,ISpatialManager* spatialManager) :
		RangeSceneQuery(spatialManager),
		mSphere(sphere)
	{
	}

	UotSphereSceneQuery::~UotSphereSceneQuery()
	{
	}

	RangeSceneQueryResults * UotSphereSceneQuery::execute()
	{
		//八叉树根节点
		auto octRootNode = dynamic_cast<OctreeNode*>(mSpatialManager->getRootNode());
		BOOST_ASSERT(octRootNode);

		//给球体找到一个合适的八叉树节点
		auto suitableNode = octRootNode->findSuitableNode(mSphere);

		RangeSceneQueryResults* results = mResults;
		Sphere const& sphere = mSphere;

		//碰撞算法
		auto fn = [&results,&sphere](OctreeNode* node)
		{
			//如果该节点关联了对象的话
			if (node->hasAttachedObject())
			{
				//遍历该节点关联的所有对象
				auto its = node->getObjectsConstIterators();
				auto beg = its.first;
				auto end = its.second;
				while (beg != end)
				{
					//对象
					auto object = *beg;

					//对象的世界空间的AABB
					auto worldAABBOfObject = object->getWorldAABB();

					//如果对象的世界空间的AABB和sphere发生了碰撞
					if (CollisionDetection::intersects(sphere,worldAABBOfObject))
					{
						results->mObjects.push_back(object);
					}

					++beg;
				}
			}
		};

		//遍历该节点以及其所有子孙节点所关联的所有对象
		suitableNode->preTraverseDepthFirst(fn);

		return mResults;
	}

	void UotSphereSceneQuery::setSphere(Sphere const & sphere)
	{
		mSphere = sphere;
	}

	//--------------------------------------------------------------

	UotAABBSceneQuery::UotAABBSceneQuery(AABB const & box,ISpatialManager* spatialManager) :
		RangeSceneQuery(spatialManager),
		mBox(box)
	{
	}

	UotAABBSceneQuery::~UotAABBSceneQuery()
	{
	}

	RangeSceneQueryResults * UotAABBSceneQuery::execute()
	{
		//八叉树根节点
		auto octRootNode = dynamic_cast<OctreeNode*>(mSpatialManager->getRootNode());
		BOOST_ASSERT(octRootNode);

		//给AABB找到一个合适的八叉树节点
		auto suitableNode = octRootNode->findSuitableNode(mBox);

		RangeSceneQueryResults* results = mResults;
		AABB const& box = mBox;

		//碰撞算法
		auto fn = [&results, &box](OctreeNode* node)
		{
			//如果该节点关联了对象的话
			if (node->hasAttachedObject())
			{
				//遍历该节点关联的所有对象
				auto its = node->getObjectsConstIterators();
				auto beg = its.first;
				auto end = its.second;
				while (beg != end)
				{
					//对象
					auto object = *beg;

					//对象的世界空间的AABB
					auto worldAABBOfObject = object->getWorldAABB();

					//如果对象的世界空间的AABB和box发生了碰撞(这里不能测试box是否包含对象的世界空间的AABB)
					if (CollisionDetection::intersects(box, worldAABBOfObject))
					{
						results->mObjects.push_back(object);
					}

					++beg;
				}
			}
		};

		//遍历该节点以及其所有子孙节点所关联的所有对象
		suitableNode->preTraverseDepthFirst(fn);

		return mResults;
	}

	void UotAABBSceneQuery::setAABB(AABB const & box)
	{
		mBox = box;
	}

	UotCameraSceneQuery::UotCameraSceneQuery(Camera* camera, ISpatialManager * spatialManager) :
		RangeSceneQuery(spatialManager),
		mCamera(camera)
	{
	}

	UotCameraSceneQuery::~UotCameraSceneQuery()
	{
	}

	RangeSceneQueryResults * UotCameraSceneQuery::execute()
	{
		//八叉树根节点
		auto octRootNode = dynamic_cast<OctreeNode*>(mSpatialManager->getRootNode());
		BOOST_ASSERT(octRootNode);

		RangeSceneQueryResults* results = mResults;

		//获取摄像机的view矩阵和投影矩阵
		auto viewMat = mCamera->getViewMatrix();
		auto projMat = mCamera->getProjectionMatrix();
		auto vpMat = viewMat * projMat;
		//裁剪空间的AABB
		auto clipBox = mCamera->getClipSpaceAABB();

		//碰撞算法
		auto fn = [&results, &clipBox,&vpMat](OctreeNode* node)
		{
			//在世界空间中，该节点的AABB
			auto worldNodeBox = node->getAABB();
			//变换该AABB到裁剪空间
			auto clipNodeBox = worldNodeBox * vpMat;

			//碰撞检测
			auto collisionRet = CollisionDetection::intersects(clipNodeBox, clipBox);

			//如果没有发生碰撞
			if (!collisionRet)
			{
				return;
			}

			//如果该节点关联了对象的话
			if (node->hasAttachedObject())
			{
				//遍历该节点关联的所有对象
				auto its = node->getObjectsConstIterators();
				auto beg = its.first;
				auto end = its.second;
				while (beg != end)
				{
					//对象
					auto object = *beg;

					//对象的世界空间的AABB
					auto worldAABBOfObject = object->getWorldAABB();

					//对象的裁剪空间的AABB
					auto clipAABBOfObject = worldAABBOfObject * vpMat;

					//如果对象的裁剪空间的AABB和clipBox发生了碰撞
					if (CollisionDetection::intersects(clipBox, clipAABBOfObject))
					{
						results->mObjects.push_back(object);
					}

					++beg;
				}
			}
		};

		//从根节点开始遍历
		octRootNode->preTraverseDepthFirst(fn);

		return mResults;
	}

	void UotCameraSceneQuery::setCamera(Camera * camera)
	{
		mCamera = camera;
	}
}//namespace ung

#endif//UOT_HIERARCHICAL_COMPILE