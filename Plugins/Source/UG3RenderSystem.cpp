#include "UG3RenderSystem.h"

#ifdef UG3_HIERARCHICAL_COMPILE

#include "UFCNumericCast.h"
#include "UFCSTLWrapper.h"

#include "UG3Monitor.h"

#pragma warning(push)
#pragma warning(disable : 4800)

namespace
{
	template<class C>
	void removeDuplicates(C& c)
	{
		std::sort(c.begin(), c.end());
		auto p = std::unique(c.begin(), c.end());
		c.erase(p, c.end());
	}
}//namespace

namespace ung
{
	G3RenderSystem* G3RenderSystem::mG3RenderSystem = nullptr;

	G3RenderSystem::G3RenderSystem(HINSTANCE inst) :
		mInstance(inst),
		mType(RenderSystemType::RST_GL),
		mInitialized(false),
		mIsSupportPixelFormatARB(false),
		mIsSupportMultiSampleARB(false),
		mIsSupportFrameBufferSRGBEXT(false),
		mNumExtensions(0),
		mPixelFormat(-1),
		mSamples(0),
		mMonitorCount(-1)
	{
		_enumDisplayMonitors();

		POINT windowAnchorPoint;
		//Fill in anchor point.
		windowAnchorPoint.x = 1111111111;
		windowAnchorPoint.y = 1111111111;
		auto hMonitor = MonitorFromPoint(windowAnchorPoint, MONITOR_DEFAULTTOPRIMARY);
	}

	G3RenderSystem::~G3RenderSystem()
	{
		ungContainerClear(mMonitors);
	}

	void G3RenderSystem::initialize()
	{
		bool ret{ false };

		auto dummyText = L"dummyClassName";

		HWND dummyHWnd = _createDummyWindow(dummyText);

		HDC oldHDC;
		HGLRC oldHRC;
		_getCurrentDCAndContext(oldHDC,oldHRC);

		HGLRC dummyHRC = _createContext(dummyHWnd);

		_setCurrentContext(dummyHWnd,dummyHRC);

		_initGlew();

		_populateVendorAndVersion();

		_populateSupportedExtensions();

		/*
		Proper Context Creation:
		There are a number of WGL extensions that give you greater power and flexibility in 
		creating contexts.

		Get WGL Extensions:
		There are quite a few extensions of interest for doing advanced context creation. Most of 
		them revolve around pixel format creation, with one notable exception.

		Pixel Format Extensions:
		The PFD struct is a nice way to describe your needs to the OpenGL implementation. But it 
		does have one major flaw(缺陷); it isn't extensible. Therefore, there is the 
		WGL_ARB_pixel_format extension. This extension defines a new mechanism for getting a 
		pixel format number, one based on providing a list of attributes and values.
		To use this, the extension must be defined. Much like WGL_ARB_extensions_string, this 
		one has been around for a long time(已经存在很久了), and even old implementations will 
		provide it. So if you've gotten this far, it's a good bet that WGL_ARB_pixel_format is 
		implemented too.
		There are several new functions in this extension, but the one we are interested in is this 
		one:
		BOOL wglChoosePixelFormatARB(HDC hdc,const int* piAttribIList,
								const FLOAT* pfAttribFList,UINT nMaxFormats,int* piFormats,
								UINT* nNumFormats);
		wglChoosePixelFormatARB is analogous(类似) to ChoosePixelFormat. Instead of taking a 
		fixed PFD struct, it takes a list of attributes and values. Many of these attributes have 
		direct analogs to PFD struct fields, but some of them are new. Also, unlike 
		ChoosePixelFormat, this function can return multiple formats that fill the requested 
		parameters. The order of these is in order from best fits to worst, though what 
		constitutes "best" is implementation-defined.
		*/

		_markExtensions();

		_retrievePixelFormatIndex(dummyHWnd);

		/*
		Delete the Context:
		The first step is always to make sure that the context you want to delete is not current.
		Call wglMakeCurrent with NULL for the context.
		Now that the context is not current, you can call wglDeleteContext on it.
		*/
		_setCurrentContext(oldHDC, oldHRC);

		_destroyContext(dummyHRC);

		//clean up our dummy window and class
		_destroyDummyWindow(dummyHWnd,dummyText);

		mG3RenderSystem = this;

		mInitialized = true;
	}

	bool G3RenderSystem::isInitialized() const
	{
		return mInitialized;
	}

	RenderSystemType G3RenderSystem::getType() const
	{
		return mType;
	}

	void G3RenderSystem::prepare()
	{
	}

	void G3RenderSystem::setWindowed(bool windowed)
	{
	}

	bool G3RenderSystem::getWindowed() const
	{
		//BOOST_ASSERT(mActiveWindow);

		//return !(mActiveWindow->isFullScreen());
		return true;
	}

	void G3RenderSystem::setBackBufferWH(uint32 width, uint32 height)
	{
		/*
		创建窗口时，会产生WM_SIZE消息，此时，窗口还没有创建完成。
		*/
		//if (!mActiveWindow)
		//{
		//	return;
		//}

		//mActiveWindow->setWidth(width);
		//mActiveWindow->setHeight(height);
	}

	std::pair<uint32, uint32> G3RenderSystem::getBackBufferWH() const
	{
		return std::pair<uint32, uint32>();
	}

	VertexElementType G3RenderSystem::getNativeColorType() const
	{
		return VertexElementType::VET_COLOR_ARGB;
	}

	void G3RenderSystem::setBackBufferPixelFormat(PixelFormat pf)
	{
	}

	PixelFormat G3RenderSystem::getBackBufferPixelFormat() const
	{
		return PixelFormat();
	}

	IOffscreenPlainSurface* G3RenderSystem::createOffscreenPlainSurface(uint32 width, uint32 height, PixelFormat format, ResourcePoolType pt)
	{
		return nullptr;
	}

	bool G3RenderSystem::createWindow(String const& title, bool isFullScreen, uint32 width, uint32 height,bool primary)
	{
		return false;
	}

	void G3RenderSystem::shutdown()
	{
	}

	bool G3RenderSystem::isDeviceLost()
	{
		return false;
	}

	void G3RenderSystem::onSizeChanged()
	{
	}

	IWindow* G3RenderSystem::getActiveWindow()
	{
		return nullptr;
	}

	void G3RenderSystem::switchBetweenWindowedAndFullScreen()
	{
	}

	void G3RenderSystem::update(real_type dt)
	{
	}

	void G3RenderSystem::renderOneFrame(real_type deltaTime)
	{
	}

	Camera* G3RenderSystem::createCamera(String const& name,Vector3 const & pos, Vector3 const & lookat, real_type aspect)
	{
		return nullptr;
	}

	void G3RenderSystem::removeCamera(String const & name)
	{
	}

	void G3RenderSystem::setMainCamera(Camera * cam)
	{
	}

	Camera* G3RenderSystem::getMainCamera() const
	{
		return nullptr;
	}

	Camera* G3RenderSystem::getCamera(String const & name) const
	{
		return nullptr;
	}

	void G3RenderSystem::setViewPort(Viewport* vp)
	{
	}

	Shader* G3RenderSystem::createVertexShader(String const & fullName)
	{
		return nullptr;
	}

	Shader* G3RenderSystem::createPixelShader(String const & fullName)
	{
		return nullptr;
	}

	void G3RenderSystem::useShader(String const & shaderName)
	{
	}

	void G3RenderSystem::setDefaultRenderStates()
	{
	}

	void G3RenderSystem::setRenderStateCullMode(CullMode cm)
	{
	}

	HINSTANCE G3RenderSystem::getInstanceHandle() const
	{
		return mInstance;
	}

	bool G3RenderSystem::isExtensionSupported(String const& extension)
	{
		auto ret = mSupportedExtensions.find(extension);

		if (ret == mSupportedExtensions.end())
		{
			return false;
		}

		return true;

		//或通过wglewIsSupported()函数来查询
	}

	int32 G3RenderSystem::getMonitorCount() const
	{
		BOOST_ASSERT(mMonitorCount != -1);

		return mMonitorCount;
	}

	void G3RenderSystem::setStaticRenderSystem(G3RenderSystem* rs)
	{
		BOOST_ASSERT(rs);

		mG3RenderSystem = rs;
	}

	G3RenderSystem* G3RenderSystem::getInstance()
	{
		BOOST_ASSERT(mG3RenderSystem);

		return mG3RenderSystem;
	}

	//-------------------------------------------------------------

	HWND G3RenderSystem::_createDummyWindow(wchar_t const* dummyText)
	{
		HINSTANCE hInst = mInstance;

		/*
		When you create your HWND,you need to make sure that it has the CS_OWNDC set for 
		its style.
		CS_OWNDC:Allocates a unique device context for each window in the class.
		*/
		UINT dummyWindowClassStyle = CS_OWNDC;

		WNDCLASS dummyWindowClass;
		memset(&dummyWindowClass, 0, sizeof(WNDCLASS));

		dummyWindowClass.style = dummyWindowClassStyle;
		dummyWindowClass.lpfnWndProc = DefWindowProc;
		//获得目前运行的程序
		dummyWindowClass.hInstance = hInst;
		dummyWindowClass.lpszClassName = dummyText;

		if (RegisterClass(&dummyWindowClass) == 0)
		{
			UNG_EXCEPTION("Dummy window registration failed.");
		}

		HWND dummyHWnd = CreateWindow(dummyText, dummyText,WS_CLIPCHILDREN | WS_POPUP,
			0,0,10,10,0,0,hInst,0);
		BOOST_ASSERT(dummyHWnd);

		_setPixelFormatForDummyWindow(dummyHWnd);

		return dummyHWnd;
	}

	void G3RenderSystem::_destroyDummyWindow(HWND dummyHWnd, wchar_t const * dummyText)
	{
		HINSTANCE hInst = mInstance;

		DestroyWindow(dummyHWnd);
		UnregisterClass(dummyText, hInst);
	}

	void G3RenderSystem::_setPixelFormatForDummyWindow(HWND hWnd)
	{
		/*
		Pixel Format:
		Each window in MS Windows has a Device Context (DC) associated with it. This object can 
		store something called a Pixel Format. This is a generic structure that describes the 
		properties of the default framebuffer that the OpenGL context you want to create should 
		have.
		Setting up the pixel format is non-intuitive(非直观).The way you create a pixel format is 
		that you fill out a struct that describes the features you want. Then you give that struct to 
		a function that will return a number that represents the closest match that it can find in 
		the list of supported pixel formats. You then set this number to be the pixel format of the 
		DC.
		*/
		HDC hDC = GetDC(hWnd);

		PIXELFORMATDESCRIPTOR pfd;
		memset(&pfd,0,sizeof(PIXELFORMATDESCRIPTOR));
		pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
		pfd.nVersion = 1;
		pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
		pfd.iPixelType = PFD_TYPE_RGBA;
		pfd.cColorBits = 32;									//Colordepth of the framebuffer.
		pfd.cDepthBits = 24;									//Number of bits for the depthbuffer
		pfd.cStencilBits = 8;									//Number of bits for the stencilbuffer
		pfd.iLayerType = PFD_MAIN_PLANE;
		
		/*
		Now that we have a PIXELFORMATDESCRIPTOR, we need to convert this into a pixel 
		format number. We do this with the function ChoosePixelFormat. This function takes a 
		device context and PFD struct and returns a pixel format number. If it returns 0, then it 
		could not find a pixel format that matches the description, or the PDF was not filled out 
		correctly.
		*/
		int pixelFormat = ChoosePixelFormat(hDC, &pfd);
		BOOST_ASSERT(pixelFormat != 0);
		
		/*
		Once you have the pixel format number, you can set it into the DC with SetPixelFormat.

		Unfortunately,Windows does not allow the user to change the pixel format of a window.
		You get to set it exactly once.
		(每个窗口只能设置一次)
		*/
		auto ret = SetPixelFormat(hDC, pixelFormat, &pfd);
		BOOST_ASSERT(ret);
	}

	void G3RenderSystem::_getCurrentDCAndContext(HDC & hDC, HGLRC & hRC)
	{
		hDC = wglGetCurrentDC();
		hRC = wglGetCurrentContext();
	}

	HGLRC G3RenderSystem::_createContext(HWND hWnd)
	{
		HDC hDC = GetDC(hWnd);

		/*
		Many of the Windows-specific initialization functions have the "wgl" prefix affixed to them.

		OpenGL doesn't exist until you create an OpenGL context, OpenGL context creation is not
		governed by the OpenGL Specification. It is instead governed by platform-specific APIs.

		Create the Context:
		Once you have set pixel format in the DC, creating the context is easy. You call 
		wglCreateContext. This function takes the DC as a parameter and returns a handle to the 
		the OpenGL context (of type HGLRC, for handle to GL Rendering Context).

		Create a False Context:
		The key problem is this:the function you use to get WGL extensions is, itself, an OpenGL
		extension. Thus like any OpenGL function, it requires an OpenGL context to call it. So in
		order to get the functions we need to create a context, we have to... create a context.
		Fortunately, this context does not need to be our final context. All we need to do is create
		a dummy context to get function pointers, then use those functions directly.
		*/
		HGLRC hRC = wglCreateContext(hDC);
		BOOST_ASSERT(hRC);

		return hRC;
	}

	void G3RenderSystem::_destroyContext(HGLRC hRC)
	{
		wglDeleteContext(hRC);
	}

	void G3RenderSystem::_setCurrentContext(HWND hWnd, HGLRC hRC)
	{
		HDC hDC = GetDC(hWnd);

		/*
		Before you can use OpenGL, the context you created must be made current. This is done 
		with the wglMakeCurrent function. This takes a DC and the HGLRC context. If there is 
		already a current context, then this function will cause the old context to be replaced with 
		the new.OpenGL functions after this will refer to state in the new context,not the old one.
		If you pass NULL for the context, then the old one is removed and OpenGL functions will 
		fail (or crash) as though you had never made a context current.
		The current context is thread-specific; each thread can have a different context current,
		and it's dangerous to have the same context current in multiple threads.
		*/
		bool ret = wglMakeCurrent(hDC,hRC);
		BOOST_ASSERT(ret);
	}

	void G3RenderSystem::_setCurrentContext(HDC hDC, HGLRC hRC)
	{
		wglMakeCurrent(hDC,hRC);
	}

	void G3RenderSystem::_initGlew()
	{
#if UNG_DEBUGMODE
		//只有设置了当前的context，才可以初始化GLEW
		HDC hDC;
		HGLRC hRC;
		_getCurrentDCAndContext(hDC,hRC);
		
		BOOST_ASSERT(hDC);
		BOOST_ASSERT(hRC);
#endif

		/*
		GLEW can be initialized only after an OpenGL context has been created. If you are using 
		GLUT or freeGLUT, this only happens after the window has been created with the 
		glutCreateWindow function.
		*/
		GLenum err = glewInit();
		if (GLEW_OK != err)
		{
			glewExperimental = GL_TRUE;
			err = glewInit();

			if (GLEW_OK != err)
			{
				UNG_EXCEPTION("Failed to initialize glew.");
			}
		}
		/*
		If glewInit returns GLEW_OK then initialization succeeded and you have access to all of 
		the OpenGL core functionality and extensions.

		一个程序中初始化一次即可，不需要每个context都初始化
		*/
	}

	void G3RenderSystem::_populateVendorAndVersion()
	{
		const GLubyte* openGLVendor = glGetString(GL_VENDOR);
		BOOST_ASSERT(openGLVendor);
		mVendor = String((const char*)openGLVendor);

		const GLubyte* openGLVersion = glGetString(GL_VERSION);
		BOOST_ASSERT(openGLVersion);
		mGLVersion = String((const char*)openGLVersion);

		int openGLVersionMajor, openGLVersionMinor;
		glGetIntegerv(GL_MAJOR_VERSION, &openGLVersionMajor);
		glGetIntegerv(GL_MINOR_VERSION, &openGLVersionMinor);
		BOOST_ASSERT(openGLVersionMajor >= 3);
		BOOST_ASSERT(openGLVersionMinor >= 0);
		mGLMajorVersion = numCast<uint32>(openGLVersionMajor);
		mGLMinorVersion = numCast<uint32>(openGLVersionMinor);

		mGLSLVersion = String(reinterpret_cast<const char*>(glGetString(GL_SHADING_LANGUAGE_VERSION))).substr(0, 4);
	}

	void G3RenderSystem::_populateSupportedExtensions()
	{
		/*
		向OpenGL查询当前实现支持多少个扩展。
		*/
		GLint numExtensions{ 0 };
		glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);
		mNumExtensions = numCast<uint32>(numExtensions);

		for (GLint i = 0; i < numExtensions; i++)
		{
			//通过调用glGetStringi函数来获取特定扩展的名称。
			auto cStr = (const char*)glGetStringi(GL_EXTENSIONS, i);
			mSupportedExtensions.put(cStr);
		}
	}

	void G3RenderSystem::_markExtensions()
	{
		//如果支持针对Windows平台的WGL扩展的话
		if (isExtensionSupported("WGL_ARB_extensions_string"))
		{
			mIsSupportPixelFormatARB = isExtensionSupported("WGL_ARB_pixel_format");

			mIsSupportMultiSampleARB = isExtensionSupported("WGL_ARB_multisample");

			mIsSupportFrameBufferSRGBEXT = isExtensionSupported("WGL_EXT_framebuffer_sRGB");
		}
	}

	void G3RenderSystem::_retrievePixelFormatIndex(HWND hWnd)
	{
		HDC hDC = GetDC(hWnd);

		if (mIsSupportPixelFormatARB && mIsSupportMultiSampleARB)
		{
			//enumerate all formats
			static int pixelAttribs[] =
			{
				WGL_SUPPORT_OPENGL_ARB,				GL_TRUE,
				WGL_DRAW_TO_WINDOW_ARB,				GL_TRUE,
				WGL_DOUBLE_BUFFER_ARB,					GL_TRUE,
				WGL_ACCELERATION_ARB,						WGL_FULL_ACCELERATION_ARB,
				WGL_PIXEL_TYPE_ARB,							WGL_TYPE_RGBA_ARB,
				WGL_COLOR_BITS_ARB,							32,
				WGL_DEPTH_BITS_ARB,							24,
				WGL_STENCIL_BITS_ARB,						8,
				WGL_SAMPLE_BUFFERS_ARB,					GL_TRUE,
				//WGL_SAMPLES_ARB的值8的index为19
				WGL_SAMPLES_ARB,								8,
				0
			};

			unsigned int maxCount{ 1 };
			int pixelFormat{ -1 };
			unsigned int formatCount{ 0 };

			//Ask OpenGL to find the most relevant format matching our attribs,Only get one format back.
			wglChoosePixelFormatARB(hDC, pixelAttribs, NULL, maxCount, &pixelFormat, &formatCount);

			if (pixelFormat == -1)
			{
				auto& samples = pixelAttribs[19];

				while (samples > 1)
				{
					samples /= 2;

					wglChoosePixelFormatARB(hDC, pixelAttribs, NULL, maxCount, &pixelFormat, &formatCount);
					
					if (pixelFormat != -1)
					{
						break;
					}
				}
			}

			BOOST_ASSERT(pixelFormat != -1);

			//Check for MSAA
			int attrib[] = { WGL_SAMPLES_ARB };
			int samples = 0;
			wglGetPixelFormatAttribivARB(hDC, pixelFormat, 0, 1, attrib, &samples);

			//获取像素格式的索引
			mPixelFormat = numCast<int32>(pixelFormat);

			mSamples = numCast<uint32>(samples);
		}
	}

	BOOL G3RenderSystem::_enumDisplayMonitorsProc(HMONITOR hMonitor, HDC hDC, LPRECT pRect, LPARAM data)
	{
		monitors_type* pMonitors = (monitors_type*)data;

		G3Monitor* monitorInfo = UNG_NEW G3Monitor(hMonitor);

		pMonitors->push_back(monitorInfo);

		return TRUE;
	}

	void G3RenderSystem::_enumDisplayMonitors()
	{
		BOOST_ASSERT(mMonitorCount == -1);

		/*
		The EnumDisplayMonitors function enumerates display monitors (including invisible 
		pseudo-monitors associated with the mirroring drivers) that intersect a region formed by 
		the intersection of a specified clipping rectangle and the visible region of a device context.
		EnumDisplayMonitors calls an application-defined MonitorEnumProc callback function once 
		for each monitor that is enumerated. Note that GetSystemMetrics (SM_CMONITORS) 
		counts only the display monitors.
		BOOL EnumDisplayMonitors(HDC hdc,LPCRECT lprcClip,MONITORENUMPROC lpfnEnum,LPARAM dwData);
		hdc:
		A handle to a display device context that defines the visible region of interest.
		If this parameter is NULL, the hdcMonitor parameter passed to the callback function will 
		be NULL, and the visible region of interest is the virtual screen that encompasses all the 
		displays on the desktop.
		lprcClip:
		A pointer to a RECT structure that specifies a clipping rectangle. The region of interest is 
		the intersection of the clipping rectangle with the visible region specified by hdc.
		If hdc is non-NULL, the coordinates of the clipping rectangle are relative to the origin of 
		the hdc. If hdc is NULL, the coordinates are virtual-screen coordinates.
		This parameter can be NULL if you don't want to clip the region specified by hdc.
		lpfnEnum:
		A pointer to a MonitorEnumProc application-defined callback function.
		dwData:
		Application-defined data that EnumDisplayMonitors passes directly to the 
		MonitorEnumProc function.
		Return value:
		If the function succeeds, the return value is nonzero.
		If the function fails, the return value is zero.
		Remarks:
		There are two reasons to call the EnumDisplayMonitors function:
		You want to draw optimally into a device context that spans several display monitors, and 
		the monitors have different color formats.
		You want to obtain a handle and position rectangle for one or more display monitors.
		To determine whether all the display monitors in a system share the same color format,
		call GetSystemMetrics (SM_SAMEDISPLAYFORMAT).
		You do not need to use the EnumDisplayMonitors function when a window spans display 
		monitors that have different color formats. You can continue to paint under the 
		assumption that the entire screen has the color properties of the primary monitor. Your 
		windows will look fine. EnumDisplayMonitors just lets you make them look better.
		Setting the hdc parameter to NULL lets you use the EnumDisplayMonitors function to 
		obtain a handle and position rectangle for one or more display monitors. The following 
		table shows how the four combinations of NULL and non-NULLhdc and lprcClip values 
		affect the behavior of the EnumDisplayMonitors function.
		NULL,NULL
		Enumerates all display monitors.
		The callback function receives a NULL HDC.
		NULL,non-NULL
		Enumerates all display monitors that intersect the clipping rectangle. Use virtual screen 
		coordinates for the clipping rectangle.
		The callback function receives a NULL HDC.
		non-NULL,NULL
		Enumerates all display monitors that intersect the visible region of the device context.
		The callback function receives a handle to a DC for the specific display monitor.
		non-NULL,non-NULL
		Enumerates all display monitors that intersect the visible region of the device context and 
		the clipping rectangle. Use device context coordinates for the clipping rectangle.
		The callback function receives a handle to a DC for the specific display monitor.
		*/
		auto ret = EnumDisplayMonitors(NULL,NULL,_enumDisplayMonitorsProc,(LPARAM)&mMonitors);

		BOOST_ASSERT(ret);
	}
}//namespace ung

#pragma warning(pop)

#endif//UG3_HIERARCHICAL_COMPILE