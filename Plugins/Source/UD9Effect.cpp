#include "UD9Effect.h"

#ifdef UD9_HIERARCHICAL_COMPILE

namespace ung
{

}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE

/*
一个效果文件中包含了一种或多种手法(technique)。
手法是绘制某些特效的特定方法。即效果文件为绘制同样的特效提供了一种或多种不同的方式。从而可以针对
不同级别的硬件实现同样效果的不同版本。
每种手法都包含了一条或多条绘制路径(rendering pass)。绘制路径封装了设备状态，采样器，着色器等。
使用多条路径的原因是由于要想实现某些特效，必须对每条路径，以不同的绘制状态和着色器将同一几何体
进行多次绘制。
*/