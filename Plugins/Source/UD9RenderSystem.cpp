#include "UD9RenderSystem.h"

#ifdef UD9_HIERARCHICAL_COMPILE

#include "URMWindowEventUtilities.h"
#include "UD9Surface.h"
#include "UD9Window.h"
#include "UD9AdapterManager.h"
#include "UD9DeviceManager.h"
#include "UD9VertexBufferManager.h"
#include "UD9Device.h"
#include "UD9DisplayMode.h"
#include "UD9Shader.h"
#include "UD9Utilities.h"
#include "UD9Camera.h"
#include "UD9Viewport.h"

#pragma warning(push)

#pragma warning(disable : 4800)

//从“ung::uint32”转换到“LONG”需要收缩转换
#pragma warning(disable : 4838)

namespace ung
{
	D9RenderSystem* D9RenderSystem::mD9RenderSystem = nullptr;
	D9AdapterManager* D9RenderSystem::mD9AdapterManager = nullptr;
	D9DeviceManager* D9RenderSystem::mD9DeviceManager = nullptr;
	D9VertexBufferManager* D9RenderSystem::mD9VertexBufferManager = nullptr;

	D9RenderSystem::D9RenderSystem(HINSTANCE inst) :
		mInstance(inst),
		mType(RenderSystemType::RST_D3D),
		mInitialized(false),
		mActiveWindow(nullptr),
		mActiveDevice(nullptr),
		mMainCamera(nullptr)
	{
		/*
		Direct3D对象时Direct3D程序中需要创建的第一个对象，也是需要最后一个释放的对象。这里所说的
		对象是指COM对象。
		通过Direct3D对象，可以枚举和检索Direct3D(渲染)设备，这样应用程序就可以在不需要创建设备对象
		的前提下选择Direct3D渲染设备。
		Direct3D(渲染)设备，是一个COM对象，它封装和存储了渲染状态，执行坐标变换和光照操作，并将
		图形绘制到显示表面上。
		函数Direct3DCreate9()的参数必须是D3D_SDK_VERSION，只有如此方能保证应用程序使用正确的
		头文件。
		如果调用失败，则返回一个空指针。
		*/
		if (!(mD3D9 = Direct3DCreate9(D3D_SDK_VERSION)))
		{
			UNG_EXCEPTION("Create IDirect3D9 interface fails.");
		}

		//必须支持硬件顶点处理和光照计算
		D3DCAPS9 caps;
		auto ret = mD3D9->GetDeviceCaps(D3DADAPTER_DEFAULT,D3DDEVTYPE_HAL,&caps);
		BOOST_ASSERT(ret == D3D_OK);
		BOOST_ASSERT(caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT);
	}

	D9RenderSystem::~D9RenderSystem()
	{
		//析构去shutdown()

		mMainCamera = nullptr;

		mCameras.deleteAll();
		mCameras.clear();

		mD9RenderSystem = nullptr;
	}

	void D9RenderSystem::initialize()
	{
		mD9RenderSystem = this;

		mD9AdapterManager = UNG_NEW D9AdapterManager;
		mD9DeviceManager = UNG_NEW D9DeviceManager;
		mD9VertexBufferManager = UNG_NEW D9VertexBufferManager;

		mInitialized = true;
	}

	bool D9RenderSystem::isInitialized() const
	{
		return mInitialized;
	}

	RenderSystemType D9RenderSystem::getType() const
	{
		return mType;
	}

	void D9RenderSystem::prepare()
	{
	}

	void D9RenderSystem::setWindowed(bool windowed)
	{
	}

	bool D9RenderSystem::getWindowed() const
	{
		BOOST_ASSERT(mActiveWindow);

		return !(mActiveWindow->isFullScreen());
	}

	void D9RenderSystem::setBackBufferWH(uint32 width, uint32 height)
	{
		/*
		创建窗口时，会产生WM_SIZE消息，此时，窗口还没有创建完成。
		*/
		if (!mActiveWindow)
		{
			return;
		}

		mActiveWindow->setWidth(width);
		mActiveWindow->setHeight(height);
	}

	std::pair<uint32, uint32> D9RenderSystem::getBackBufferWH() const
	{
		return std::pair<uint32, uint32>();
	}

	VertexElementType D9RenderSystem::getNativeColorType() const
	{
		return VertexElementType::VET_COLOR_ARGB;
	}

	void D9RenderSystem::setBackBufferPixelFormat(PixelFormat pf)
	{
	}

	PixelFormat D9RenderSystem::getBackBufferPixelFormat() const
	{
		return PixelFormat();
	}

	IOffscreenPlainSurface* D9RenderSystem::createOffscreenPlainSurface(uint32 width, uint32 height, PixelFormat format, ResourcePoolType pt)
	{
		return UNG_NEW D9OffscreenPlainSurface(width,height,format,pt);
	}

	bool D9RenderSystem::createWindow(String const& title, bool isFullScreen, uint32 width, uint32 height,bool primary)
	{
		_checkWindowTitle(title);

		auto window = UNG_NEW D9Window(title, isFullScreen, width, height, primary,mInstance);

		auto ret = window->create();

		if (ret)
		{
			mWindows.put(title, window);

			mActiveWindow = window;
		}

		mD9DeviceManager->selectDevice(window);

		return ret;
	}

	void D9RenderSystem::shutdown()
	{
		//注销窗口
		unregisterWindows();

		mActiveWindow = nullptr;

		/*
		UnorderedMapWrapper可以在其析构函数中调用clear()函数，这里显式调用的原因是，不能让
		mShaders容器等到D9RenderSystem的析构函数中再去clear，因为，牵扯到IUnknown的Release()
		顺序问题。
		*/
		mShaders.deleteAll();
		mShaders.clear();

		//这里不要mActiveDevice->Release();
		mActiveDevice = nullptr;

		UNG_DEL(mD9VertexBufferManager);

		UNG_DEL(mD9DeviceManager);

		UNG_DEL(mD9AdapterManager);

		/*
		必须在插件处于正常状态时，来释放D3D9的内存。
		否则，COM接口的Release()会异常。
		*/
		UD9_RELEASE(mD3D9);
	}

	bool D9RenderSystem::isDeviceLost()
	{
		return mD9DeviceManager->getActiveDevice()->isLost();
	}

	void D9RenderSystem::onSizeChanged()
	{
		auto pDevice = mD9DeviceManager->getActiveDevice();

		pDevice->onLostDevice();

		auto resetRet = pDevice->reset();
		BOOST_ASSERT(resetRet == D3D_OK);

		pDevice->onResetDevice();
	}

	IWindow* D9RenderSystem::getActiveWindow()
	{
		BOOST_ASSERT(mActiveWindow);

		return mActiveWindow;
	}

	void D9RenderSystem::switchBetweenWindowedAndFullScreen()
	{
		bool windowed = getWindowed();

		auto pWindow = dynamic_cast<D9Window*>(mActiveWindow);
		BOOST_ASSERT(pWindow);

		//改变窗口/全屏标记
		pWindow->changeWindowedFullScreenFlag();

		//在(改变窗口/全屏标记)之后调用
		auto style = pWindow->getStyle();

		if (windowed)
		{
			//切换至全屏模式

			//为全屏模式选择显示模式
			pWindow->_selectFullScreenDisplayMode();

			auto fullScreenDisplayMode = pWindow->getFullScreenDisplayMode();

			auto width = fullScreenDisplayMode->getWidth();
			auto height = fullScreenDisplayMode->getHeight();

			/*
			当窗口处于mined或者maxed状态时，不更新last width和last height
			当然，当窗口处于mined状态时，也无法切换全屏模式
			*/
			if (pWindow->isNormalState())
			{
				pWindow->updateLastWH();
			}

			pWindow->setWidth(width);
			pWindow->setHeight(height);

#if UNG_DEBUGMODE
			auto w = GetSystemMetrics(SM_CXSCREEN);
			auto h = GetSystemMetrics(SM_CYSCREEN);
			
			BOOST_ASSERT(width == w && height == h);
#endif

			auto hWND = pWindow->getHandle();

			/*
			Changes an attribute of the specified window. The function also sets a value at the 
			specified offset in the extra window memory.
			LONG_PTR WINAPI SetWindowLongPtr(HWND hWnd,int nIndex,LONG_PTR dwNewLong);
			GWL_STYLE:Sets a new window style.
			*/
			SetWindowLongPtr(hWND, GWL_STYLE, style);

			/*
			If we call SetWindowLongPtr, MSDN states that we need to call SetWindowPos for the 
			change to take effect.
			In addition,we need to call this function anyway to update the window dimensions.

			Changes the size,position,and Z order of a child,pop-up,or top-level window.These 
			windows are ordered according to their appearance on the screen.The topmost window 
			receives the highest rank and is the first window in the Z order.
			BOOL WINAPI SetWindowPos(HWND hWnd,HWND hWndInsertAfter,int X,int Y,int cx,
			int cy,UINT uFlags);
			hWnd:
			A handle to the window.
			hWndInsertAfter:
			A handle to the window to precede(先于) the positioned window in the Z order.This 
			parameter must be a window handle or one of the following values.
			HWND_BOTTOM:
			Places the window at the bottom of the Z order.If the hWnd parameter identifies a 
			topmost window,the window loses its topmost status and is placed at the bottom of all 
			other windows.
			HWND_NOTOPMOST:
			Places the window above all non-topmost windows (that is,behind all topmost windows).
			This flag has no effect if the window is already a non-topmost window.
			HWND_TOP:
			Places the window at the top of the Z order.
			HWND_TOPMOST:
			Places the window above all non-topmost windows.The window maintains its topmost 
			position even when it is deactivated.
			X:
			The new position of the left side of the window,in client coordinates.
			Y:
			The new position of the top of the window,in client coordinates.
			cx:
			The new width of the window,in pixels.
			cy:
			The new height of the window,in pixels.
			uFlags:
			The window sizing and positioning flags.
			SWP_NOZORDER:
			Retains the current Z order (ignores the hWndInsertAfter parameter).
			SWP_SHOWWINDOW:
			Displays the window.
			If the function succeeds, the return value is nonzero.
			If you have changed certain window data using SetWindowLong,you must(必须) call 
			SetWindowPos for the changes to take effect.Use the following combination for uFlags:
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED.
			A window can be made a topmost window either by setting the hWndInsertAfter 
			parameter to HWND_TOPMOST and ensuring that the SWP_NOZORDER flag is not set,
			or by setting a window's position in the Z order so that it is above any existing topmost 
			windows.When a non-topmost window is made topmost,its owned windows are also 
			made topmost.Its owners,however,are not changed.
			If neither the SWP_NOACTIVATE nor SWP_NOZORDER flag is specified (that is,when 
			the application requests that a window be simultaneously activated and its position in 
			the Z order changed),the value specified in hWndInsertAfter is used only in the 
			following circumstances.
			Neither the HWND_TOPMOST nor HWND_NOTOPMOST flag is specified in 
			hWndInsertAfter.
			The window identified by hWnd is not the active window.
			An application cannot activate an inactive window without also bringing it to the top of 
			the Z order. Applications can change an activated window's position in the Z order 
			without restrictions, or it can activate a window and then move it to the top of the 
			topmost or non-topmost windows.
			If a topmost window is repositioned to the bottom (HWND_BOTTOM) of the Z order or 
			after any non-topmost window, it is no longer topmost. When a topmost window is 
			made non-topmost, its owners and its owned windows are also made non-topmost 
			windows.
			A non-topmost window can own a topmost window, but the reverse cannot occur. Any 
			window (for example, a dialog box) owned by a topmost window is itself made a 
			topmost window, to ensure that all owned windows stay above their owner.
			If an application is not in the foreground, and should be in the foreground, it must call 
			the SetForegroundWindow function.
			To use SetWindowPos to bring a window to the top, the process that owns the window 
			must have SetForegroundWindow permission.
			*/
			auto setWPRet = SetWindowPos(hWND, HWND_TOP, 0, 0, width, height, SWP_NOZORDER | SWP_SHOWWINDOW);
			BOOST_ASSERT(setWPRet);
		}
		else
		{
			//切换至窗口模式

			//为窗口模式选择像素格式
			pWindow->_selectWindowedFormat();

			auto width = pWindow->getLastWidth();
			auto height = pWindow->getLastHeight();

			auto hWND = pWindow->getHandle();

			//显示器句柄
			HMONITOR hMonitor = pWindow->getMonitor();

			//显示器信息
			MONITORINFO monitorInfo = pWindow->_getMonitorInfo(hMonitor);

			auto rcWork = monitorInfo.rcWork;

			//左上角
			uint32 left = INT_MAX;
			uint32 top = INT_MAX;

			//工作区域矩形
			auto rcwLeft = rcWork.left;
			auto rcwRight = rcWork.right;
			auto rcwTop = rcWork.top;
			auto rcwBottom = rcWork.bottom;

			//屏幕宽高(工作区域，高比屏幕的分辨率数值略小)
			uint32 screenWidth = rcwRight - rcwLeft;
			uint32 screenHeight = rcwBottom - rcwTop;

			//客户区域宽高
			uint32 clientWidth = std::min(width, screenWidth);
			uint32 clientHeight = std::min(height, screenHeight);

			pWindow->setWidth(clientWidth);
			pWindow->setHeight(clientHeight);

			left = rcwLeft + (screenWidth - clientWidth) / 2;
			top = rcwTop + (screenHeight - clientHeight) / 2;

			RECT R = {0,0,clientWidth,clientHeight};
			/*
			The window size is larger than the client area size.
			We need to make the window rectangle slightly larger.The AdjustWindowRect function 
			calculates precisely how much larger it needs to be given the desired client rectangle 
			size.
			*/
			AdjustWindowRect(&R, style, false);

			//期望的窗口宽高(窗口整体宽高)
			auto desiredWidth = R.right - R.left;
			auto desiredHeight = R.bottom - R.top;

			SetWindowLongPtr(hWND, GWL_STYLE, style);

			auto setWPRet = SetWindowPos(hWND, HWND_TOP, left, top,
				desiredWidth, desiredHeight, SWP_NOZORDER | SWP_SHOWWINDOW);
			BOOST_ASSERT(setWPRet);

			/*
			如果窗口是从maxed状态切换至全屏模式的话，那么现在从全屏模式切换为窗口模式后，再次发送
			消息，使窗口恢复至maxed状态。
			*/
			if (pWindow->getMinMaxState())
			{
				PostMessage(hWND,WM_SYSCOMMAND, SC_MAXIMIZE,0);
			}

#if UNG_DEBUGMODE
			bool ret{ false };
			RECT rc;

			//必须在SetWindowLongPtr()函数之后来调用，因为如果在之前的话，窗口的句柄还是全屏时的句柄。
			ret = GetWindowRect(hWND, &rc);
			BOOST_ASSERT(ret);
			auto validateLeft = rc.left;
			auto validateTop = rc.top;
			BOOST_ASSERT(validateLeft == left);
			BOOST_ASSERT(validateTop == top);

			ret = GetClientRect(hWND, &rc);
			BOOST_ASSERT(ret);
			BOOST_ASSERT(rc.left == 0);
			BOOST_ASSERT(rc.top == 0);
			auto drawableAreaWidth = rc.right;
			auto drawableAreaHeight = rc.bottom;
			//BOOST_ASSERT(drawableAreaWidth == clientWidth);
			//BOOST_ASSERT(drawableAreaHeight == clientHeight);
#endif
		}

		/*
		Reset the device with the changes.
		We have changed the back buffer dimensions,and thus need to reset the device in order 
		for the changes to take place.
		*/
		onSizeChanged();
	}

	void D9RenderSystem::update(real_type dt)
	{
	}

	void D9RenderSystem::renderOneFrame(real_type deltaTime)
	{
		if (!isDeviceLost())
		{
			//render one frame
			clearFrameBuffer(FBT_COLOR, UNG_COLOR_BLACK);

			startFrame();

			/////
			render();

			endFrame();

			present();
		}
	}

	Camera* D9RenderSystem::createCamera(String const& name,Vector3 const & pos, Vector3 const & lookat, real_type aspect)
	{
		Camera* cam = UNG_NEW D9Camera(name,pos, lookat, aspect);

		mCameras.put(cam->getName(), cam);

		if (!mMainCamera)
		{
			mMainCamera = cam;
		}

		return cam;
	}

	void D9RenderSystem::removeCamera(String const & name)
	{
		BOOST_ASSERT(!name.empty());

		auto findRet = mCameras.find(name);

		if (findRet == mCameras.end())
		{
			return;
		}

		mCameras.take(name);
	}

	void D9RenderSystem::setMainCamera(Camera * cam)
	{
		BOOST_ASSERT(cam);

		mMainCamera = cam;
	}

	Camera* D9RenderSystem::getMainCamera() const
	{
		BOOST_ASSERT(mMainCamera);

		return mMainCamera;
	}

	Camera* D9RenderSystem::getCamera(String const & name) const
	{
		BOOST_ASSERT(!name.empty());

		auto findRet = mCameras.find(name);

		if (findRet == mCameras.end())
		{
			return nullptr;
		}

		return findRet->second;
	}

	void D9RenderSystem::setViewPort(Viewport* vp)
	{
		BOOST_ASSERT(vp);

		vp->set();
	}

	Shader* D9RenderSystem::createVertexShader(String const & fullName)
	{
		Shader* vs = UNG_NEW D9VertexShader(fullName);

		vs->compile();

		mShaders.put(vs->getShaderName(), vs);

		return vs;
	}

	Shader* D9RenderSystem::createPixelShader(String const & fullName)
	{
		Shader* ps = UNG_NEW D9PixelShader(fullName);

		ps->compile();

		mShaders.put(ps->getShaderName(), ps);

		return ps;
	}

	void D9RenderSystem::useShader(String const & shaderName)
	{
		BOOST_ASSERT(mActiveDevice);

		auto findRet = mShaders.find(shaderName);

		if (findRet == mShaders.end())
		{
			UNG_EXCEPTION("The shader program specified by the parameter has not been created yet.");
		}

		auto shader = (*findRet).second;

		auto type = shader->getType();

		HRESULT ret;
		
		switch (type)
		{
		case ShaderType::ST_VERT:
			ret = mActiveDevice->SetVertexShader(boost::polymorphic_cast<D9VertexShader*>(shader)->getShader());
			break;
		case ShaderType::ST_FRAG:
			ret = mActiveDevice->SetPixelShader(boost::polymorphic_cast<D9PixelShader*>(shader)->getShader());
			break;
		}

		BOOST_ASSERT(ret == D3D_OK);
	}

	void D9RenderSystem::setDefaultRenderStates()
	{
		//把顺时针面给cull掉
		setRenderStateCullMode(CullMode::CM_CW);
	}

	void D9RenderSystem::setRenderStateCullMode(CullMode cm)
	{
		BOOST_ASSERT(mActiveDevice);

		auto ret = mActiveDevice->SetRenderState(D3DRS_CULLMODE, ud9ToD3D9CullMode(cm));

		BOOST_ASSERT(ret == D3D_OK);
	}

	void D9RenderSystem::setActiveDevice(IDirect3DDevice9 * dev)
	{
		BOOST_ASSERT(dev);

		mActiveDevice = dev;
	}

	IDirect3DDevice9 * D9RenderSystem::getActiveDevice() const
	{
		BOOST_ASSERT(mActiveDevice);

		return mActiveDevice;
	}

	void D9RenderSystem::clearFrameBuffer(uint32 buffers, const real_color& color, real_type depth, uint32 stencil)
	{
		BOOST_ASSERT(mActiveDevice);

		uint32 flags = 0;

		if (buffers & FBT_COLOR)
		{
			flags |= D3DCLEAR_TARGET;
		}
		if (buffers & FBT_DEPTH)
		{
			flags |= D3DCLEAR_ZBUFFER;
		}
		if (buffers & FBT_STENCIL)
		{
			flags |= D3DCLEAR_STENCIL;
		}

		HRESULT hr;

		if (FAILED(hr = mActiveDevice->Clear(0,NULL,flags,color.getARGB(),depth,stencil)))
		{
			UNG_EXCEPTION("Clear frame buffer failed.");
		}
	}

	void D9RenderSystem::startFrame()
	{
		HRESULT hr;

		BOOST_ASSERT(mActiveDevice);

		/*
		Begins a scene.
		HRESULT BeginScene();
		Applications must call IDirect3DDevice9::BeginScene before performing any rendering 
		and must call IDirect3DDevice9::EndScene when rendering is complete and before calling 
		IDirect3DDevice9::BeginScene again.
		If IDirect3DDevice9::BeginScene fails, the device was unable to begin the scene, and 
		there is no need to call IDirect3DDevice9::EndScene. In fact, calls to 
		IDirect3DDevice9::EndScene will fail if the previous IDirect3DDevice9::BeginScene failed.
		This applies to any application that creates multiple swap chains.
		There should be one IDirect3DDevice9::BeginScene/IDirect3DDevice9::EndScene pair 
		between any successive calls to present (either IDirect3DDevice9::Present or 
		IDirect3DSwapChain9::Present). IDirect3DDevice9::BeginScene should be called once 
		before any rendering is performed, and IDirect3DDevice9::EndScene should be called 
		once after all rendering for a frame has been submitted to the runtime. Multiple 
		non-nested IDirect3DDevice9::BeginScene/IDirect3DDevice9::EndScene pairs between 
		calls to present are legal, but having more than one pair may incur a performance hit. To 
		enable maximal parallelism between the CPU and the graphics accelerator, it is 
		advantageous to call IDirect3DDevice9::EndScene as far ahead of(遥遥领先) calling 
		present as possible.

		The IDirect3DDevice9::BeginScene and ID3DXRenderToSurface::EndScene methods signal 
		to the system when rendering is beginning or is complete.You can call rendering methods 
		only between calls to these methods.Even if rendering methods fail,you should call 
		ID3DXRenderToSurface::EndScene before calling IDirect3DDevice9::BeginScene Again.
		*/
		if (FAILED(hr = mActiveDevice->BeginScene()))
		{
			UNG_EXCEPTION("Error starting frame.");
		}
	}

	void D9RenderSystem::endFrame()
	{
		HRESULT hr;

		BOOST_ASSERT(mActiveDevice);

		/*
		Ends a scene that was begun by calling IDirect3DDevice9::BeginScene.
		HRESULT EndScene();
		When this method succeeds, the scene has been queued up for rendering by the driver.
		This is not a synchronous method, so the scene is not guaranteed to have completed 
		rendering when this method returns.
		*/
		if (FAILED(hr = mActiveDevice->EndScene()))
		{
			UNG_EXCEPTION("Error starting frame.");
		}
	}

	void D9RenderSystem::present()
	{
		BOOST_ASSERT(mActiveDevice);

		//临时
		//auto win = dynamic_cast<D9Window*>(mActiveWindow);
		//win->present();

		//Present the backbuffer contents to the display
		mActiveDevice->Present(NULL, NULL, NULL, NULL);
	}

	HINSTANCE D9RenderSystem::getInstanceHandle() const
	{
		return mInstance;
	}

	void D9RenderSystem::unregisterWindow(IWindow * window)
	{
		BOOST_ASSERT(window);

		auto className = window->getClassName();
		auto title = window->getTitle();

		auto ret = UnregisterClass(className, mInstance);
		BOOST_ASSERT(ret);

		//删除该窗口指针
		UNG_DEL(window);
		//从窗口容器中移除掉该窗口
		mWindows.take(title);
	}

	void D9RenderSystem::unregisterWindows()
	{
		BOOST_ASSERT(mD9DeviceManager);

		auto beg = mWindows.cbegin();
		auto end = mWindows.cend();

		while (beg != end)
		{
			auto window = beg->second;

			auto className = window->getClassName();

			/*
			A DLL must explicitly unregister its classes when it is unloaded.

			Unregisters a window class, freeing the memory required for the class.
			BOOL WINAPI UnregisterClass(LPCTSTR lpClassName,HINSTANCE hInstance);
			Before calling this function, an application must destroy all windows created 
			with the specified class.
			No window classes registered by a DLL are unregistered when the .dll is unloaded.
			lpClassName:
			A null-terminated string or a class atom.If lpClassName is a string,it specifies the 
			window class name.This class name must have been registered by a previous call to 
			the RegisterClass or RegisterClassEx function.System classes,such as dialog box 
			controls,cannot be unregistered.If this parameter is an atom,it must be a class atom 
			created by a previous call to the RegisterClass or RegisterClassEx function.The atom 
			must be in the low-order word of lpClassName;the high-order word must be zero.
			hInstance:
			A handle to the instance of the module that created the class.
			If the function succeeds, the return value is nonzero.
			If the class could not be found or if a window still exists that was created with the class,
			the return value is zero.
			*/
			auto ret = UnregisterClass(className,mInstance);
			BOOST_ASSERT(ret);

			++beg;
		}

		//删除窗口指针
		mWindows.deleteAll();
		//清空窗口容器
		mWindows.clear();
	}

	void D9RenderSystem::setStaticRenderSystem(D9RenderSystem * rs)
	{
		BOOST_ASSERT(rs);

		mD9RenderSystem = rs;
	}

	D9RenderSystem* D9RenderSystem::getInstance()
	{
		BOOST_ASSERT(mD9RenderSystem);

		return mD9RenderSystem;
	}

	IDirect3D9* D9RenderSystem::getIDirect3D9()
	{
		BOOST_ASSERT(mD9RenderSystem);

		auto pD3D = mD9RenderSystem->mD3D9;

		BOOST_ASSERT(pD3D);

		return pD3D;
	}

	//D9Device* D9RenderSystem::getActiveDevice()
	//{
	//	return mD9DeviceManager->getActiveDevice();
	//}

	D9AdapterManager* D9RenderSystem::getD9AdapterManager()
	{
		BOOST_ASSERT(mD9AdapterManager);

		return mD9AdapterManager;
	}

	D9DeviceManager * D9RenderSystem::getD9DeviceManager()
	{
		BOOST_ASSERT(mD9DeviceManager);

		return mD9DeviceManager;
	}

	D9VertexBufferManager* D9RenderSystem::getD9VertexBufferManager()
	{
		BOOST_ASSERT(mD9VertexBufferManager);

		return mD9VertexBufferManager;
	}

	void D9RenderSystem::_checkWindowTitle(String const & title) const
	{
#if UNG_DEBUGMODE
		auto findRet = mWindows.find(title);

		if (findRet != mWindows.end())
		{
			UNG_EXCEPTION("The title of the window specified by the parameter is illegal.");
		}
#endif
	}
}//namespace ung

#pragma warning(pop)

#endif//UD9_HIERARCHICAL_COMPILE

/*
IUnknown Release():
异常，终止的原因是：
释放顺序。
*/

/*
IDirect3D9对象主要有两个用途：设备枚举以及创建IDirect3DDevice9对象。
设备枚举是指获取系统中可用的每块图形卡的性能，显示模式，格式及其他信息。

typedef struct D3DPRESENT_PARAMETERS
{
	UINT											BackBufferWidth;
	UINT											BackBufferHeight;
	D3DFORMAT								BackBufferFormat;
	UINT											BackBufferCount;
	D3DMULTISAMPLE_TYPE				MultiSampleType;
	DWORD										MultiSampleQuality;
	D3DSWAPEFFECT						SwapEffect;
	HWND										hDeviceWindow;
	BOOL										Windowed;
	BOOL										EnableAutoDepthStencil;
	D3DFORMAT								AutoDepthStencilFormat;
	DWORD										Flags;
	UINT											FullScreen_RefreshRateInHz;
	UINT											PresentationInterval;
}D3DPRESENT_PARAMETERS,*LPD3DPRESENT_PARAMETERS;
BackBufferWidth:
Type:UINT
Width of the new swap chain's back buffers, in pixels.
If Windowed is FALSE (the presentation is full-screen), this value must equal the width of one 
of the enumerated display modes found through EnumAdapterModes.
If Windowed is TRUE and either BackBufferWidth or BackBufferHeight is zero, the corresponding 
dimension of the client area of the hDeviceWindow (or the focus window, if hDeviceWindow is 
NULL) is taken.
如果是窗口模式，且该值被设置为了0，则系统自动使用窗口客户区的宽度作为后台缓冲区的宽度。
BackBufferHeight:
Type:UINT
Height of the new swap chain's back buffers, in pixels.
If Windowed is FALSE (the presentation is full-screen), this value must equal the height of one 
of the enumerated display modes found through EnumAdapterModes.
If Windowed is TRUE and either BackBufferWidth or BackBufferHeight is zero, the corresponding 
dimension of the client area of the hDeviceWindow (or the focus window, if hDeviceWindow is 
NULL) is taken.
BackBufferFormat:
Type:D3DFORMAT
The back buffer format.
This value must be one of the render-target formats as validated by CheckDeviceType.
You can use GetDisplayMode to obtain the current format.
In fact, D3DFMT_UNKNOWN can be specified for the BackBufferFormat while in windowed mode.
This tells the runtime to use the current display-mode format and eliminates the need to call 
GetDisplayMode.
For windowed applications, the back buffer format no longer needs to match the display-mode 
format because color conversion can now be done by the hardware (if the hardware supports 
color conversion). The set of possible back buffer formats is constrained, but the runtime will 
allow any valid back buffer format to be presented to any desktop format. (There is the 
additional requirement that the device be operable in the desktop; devices typically do not 
operate in 8 bits per pixel modes.)
Full-screen applications cannot do color conversion.
BackBufferCount:
Type:UINT
This value can be between 0 and D3DPRESENT_BACK_BUFFERS_MAX (or D3DPRESENT_BACK_BUFFERS_MAX_EX 
when using Direct3D 9Ex).
Values of 0 are treated as 1.
If the number of back buffers cannot be created, the runtime will fail the method call and fill 
this value with the number of back buffers that could be created. As a result, an application can 
call the method twice with the same D3DPRESENT_PARAMETERS structure and expect it to 
work the second time.
The method fails if one back buffer cannot be created.
The value of BackBufferCount influences what set of swap effects are allowed.Specifically, any 
D3DSWAPEFFECT_COPY swap effect requires that there be exactly one back buffer.
MultiSampleType:
Type:D3DMULTISAMPLE_TYPE
Member of the D3DMULTISAMPLE_TYPE enumerated type.
The value must be D3DMULTISAMPLE_NONE unless SwapEffect has been set to D3DSWAPEFFECT_DISCARD.
Multisampling is supported only if the swap effect is D3DSWAPEFFECT_DISCARD.
MultiSampleQuality:
Type:DWORD
Quality level.
The valid range is between zero and one less than the level returned by pQualityLevels used by 
CheckDeviceMultiSampleType.
Passing a larger value returns the error D3DERR_INVALIDCALL.
Paired values of render targets or of depth stencil surfaces and D3DMULTISAMPLE_TYPE must 
match.
SwapEffect:
Type:D3DSWAPEFFECT
Member of the D3DSWAPEFFECT enumerated type. The runtime will guarantee the implied 
semantics concerning buffer swap behavior; therefore, if Windowed is TRUE and SwapEffect is 
set to D3DSWAPEFFECT_FLIP, the runtime will create one extra back buffer and copy whichever 
becomes the front buffer at presentation time.
D3DSWAPEFFECT_COPY requires that BackBufferCount be set to 1.
D3DSWAPEFFECT_DISCARD will be enforced in the debug runtime by filling any buffer with 
noise after it is presented.
指定系统如何将后台缓冲区的内容复制到前台缓冲区。
D3DSWAPEFFECT_DISCARD,后台缓存复制到前台缓存后，清除后台缓存内容。
D3DSWAPEFFECT_FLIP,后台缓存复制到前台缓存后，保留后台缓存原内容不变。(当后台缓存数量大于1时使用)
D3DSWAPEFFECT_COPY,后台缓存复制到前台缓存后，保留后台缓存原内容不变。(当后台缓存数量大于1时使用)
D3DSWAPEFFECT_FORCE_DWORD,强制编译器将该类型变量编译为32位大小。
hDeviceWindow:
Type:HWND
The device window determines the location and size of the back buffer on screen.
This is used by Direct3D when the back buffer contents are copied to the front buffer during 
Present.
For a full-screen application, this is a handle to the top window (which is the focus window).
For applications that use multiple full-screen devices (such as a multimonitor system),exactly 
one device can use the focus window as the device window. All other devices must have unique 
device windows.
For a windowed-mode application, this handle will be the default target window for Present. If 
this handle is NULL, the focus window will be taken.
Note that no attempt is made by the runtime to reflect user changes in window size. The back 
buffer is not implicitly reset when this window is reset. However, the Present method does 
automatically track window position changes.
指定图形绘制窗口。如果设置为NULL，则图形绘制窗口为当前被激活的窗口。
Windowed:
Type:BOOL
TRUE if the application runs windowed; FALSE if the application runs full-screen.
EnableAutoDepthStencil:
Type:BOOL
If this value is TRUE, Direct3D will manage depth buffers for the application.The device will 
create a depth-stencil buffer when it is created. The depth-stencil buffer will be automatically 
set as the render target of the device. When the device is reset, the depth-stencil buffer will be 
automatically destroyed and recreated in the new size.
If EnableAutoDepthStencil is TRUE, then AutoDepthStencilFormat must be a valid depth-stencil 
format.
AutoDepthStencilFormat:
Type:D3DFORMAT
Member of the D3DFORMAT enumerated type.
The format of the automatic depth-stencil surface that the device will create.This member is 
ignored unless EnableAutoDepthStencil is TRUE.
Flags:
Type:DWORD
One of the D3DPRESENTFLAG constants.
FullScreen_RefreshRateInHz:
Type:UINT
The rate at which the display adapter refreshes the screen. The value depends on the mode in 
which the application is running:
For windowed mode, the refresh rate must be 0.
For full-screen mode, the refresh rate is one of the refresh rates returned by EnumAdapterModes.
指定显示适配器刷新屏幕的速率。当图形以窗口方式显示时，该值必须设置为0，当以全屏方式显示时，该值
可以取IDirect3D9::EnumAdapterModes()函数返回的刷新频率。如果将该值设置为0，则表示刷新频率为
显示器的默认刷新频率。
PresentationInterval:
Type:UINT
The maximum rate at which the swap chain's back buffers can be presented to the front buffer.
For a detailed explanation of the modes and the intervals that are supported, see D3DPRESENT.
指定后台缓存与前台缓存的最大交换频率。
*/