#include "UD9Viewport.h"

#ifdef UD9_HIERARCHICAL_COMPILE

#include "UD9RenderSystem.h"
#include "UD9Device.h"

namespace ung
{
	D9Viewport::D9Viewport(uint32 left, uint32 top, uint32 width, uint32 height, int32 zOrder) :
		Viewport(zOrder)
	{
		mVP.X = left;
		mVP.Y = top;
		mVP.Width = width;
		mVP.Height = height;
		mVP.MinZ = 0.0;
		mVP.MaxZ = 1.0;
	}

	D9Viewport::~D9Viewport()
	{
	}

	void D9Viewport::clear(uint32 buffers, const real_color& color, real_type depth, uint32 stencil)
	{
		auto pDevice = D9RenderSystem::getInstance()->getActiveDevice();

		uint32 flags = 0;

		if (buffers & FBT_COLOR)
		{
			flags |= D3DCLEAR_TARGET;
		}
		if (buffers & FBT_DEPTH)
		{
			flags |= D3DCLEAR_ZBUFFER;
		}
		if (buffers & FBT_STENCIL)
		{
			flags |= D3DCLEAR_STENCIL;
		}

		HRESULT hr;

		/*
		Clears one or more surfaces such as a render target, multiple render targets, a stencil 
		buffer, and a depth buffer.
		HRESULT Clear(DWORD Count,const D3DRECT* pRects,DWORD Flags,D3DCOLOR Color,
		float Z,DWORD Stencil);
		Count:
		Number of rectangles in the array at pRects. Must be set to 0 if pRects is NULL. May not 
		be 0 if pRects is a valid pointer.
		pRects:
		Pointer to an array of D3DRECT structures that describe the rectangles to clear. Set a 
		rectangle to the dimensions of the rendering target to clear the entire surface. Each 
		rectangle uses screen coordinates that correspond to points on the render target.
		Coordinates are clipped to the bounds of the viewport rectangle.To indicate that the entire 
		viewport rectangle is to be cleared,set this parameter to NULL and Count to 0.
		(The array of rectangles describes the areas on the render-target surface to be cleared.)
		(In most cases,you use a single rectangle that covers the entire rendering target.You do 
		this by setting the first parameter to zero and the second parameter to NULL.)
		Flags:
		Combination of one or more D3DCLEAR flags that specify the surface(s) that will be 
		cleared.
		D3DCLEAR_STENCIL			Clear the stencil buffer.
		D3DCLEAR_TARGET			Clear a render target,or all targets in a multiple render target.
		D3DCLEAR_ZBUFFER			Clear the depth buffer.
		(如果Flags没有深度和模板的话，那么深度值和模板值会被忽略。)
		Color:
		Clear a render target to this ARGB color.
		D3DCOLOR,A four byte value that typically represents the alpha, red, green, and blue 
		components of a color. It can also represent luminance and brightness.
		You can set the D3DCOLOR type using one of the following macros.
		D3DCOLOR_ARGB
		D3DCOLOR_AYUV
		D3DCOLOR_COLORVALUE
		D3DCOLOR_RGBA
		D3DCOLOR_XRGB
		D3DCOLOR_XYUV
		Z:
		Clear the depth buffer to this new z value which ranges from 0 to 1.
		Stencil:
		Clear the stencil buffer to this new value which ranges from 0 to 2 ^ n - 1 (n is the bit 
		depth of the stencil buffer).
		IDirect3DDevice9::Clear will fail if you:
		Try to clear either the depth buffer or the stencil buffer of a render target that does not 
		have an attached depth buffer.
		Try to clear the stencil buffer when the depth buffer does not contain stencil data.
		*/
		if (FAILED(hr = pDevice->Clear(0,NULL,flags,color.getARGB(),depth,stencil)))
		{
			UNG_EXCEPTION("Clear frame buffer failed.");
		}
	}

	uint32 D9Viewport::getLeft() const
	{
		return mVP.X;
	}

	uint32 D9Viewport::getTop() const
	{
		return mVP.Y;
	}

	uint32 D9Viewport::getWidth() const
	{
		return mVP.Width;
	}

	uint32 D9Viewport::getHeight() const
	{
		return mVP.Height;
	}

	real_type D9Viewport::getMinZ() const
	{
		return mVP.MinZ;
	}

	real_type D9Viewport::getMaxZ() const
	{
		return mVP.MaxZ;
	}

	void D9Viewport::set()
	{
		auto pDevice = D9RenderSystem::getInstance()->getActiveDevice();

		auto ret = pDevice->SetViewport(&mVP);

		BOOST_ASSERT(ret == D3D_OK);
	}
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE