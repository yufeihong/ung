#include "UG3Config.h"

#ifdef UG3_HIERARCHICAL_COMPILE

/*
Under Windows, you need to statically link to a library called OpenGL32.lib (note that you still
link to OpenGL32.lib if you're building a 64-bit executable. The "32" part is meaningless).
*/
#pragma comment (lib, "opengl32.lib")

#pragma comment (lib, "glew32.lib")

#if UNG_DEBUGMODE
#pragma comment(lib,"UNG_d.lib")
#else
#pragma comment(lib,"UNG.lib")
#endif

#endif//UG3_HIERARCHICAL_COMPILE

/*
In an effort(在努力下) from Silicon Graphics Inc (SGI) to get more vendors to produce software 
that would run on their workstations, SGI together with Digital Equipment Corporation, IBM,
Intel, and Microsoft formed the OpenGL Architecture Review Board (ARB)(架构审查委员会).On 
July 1, 1992 version 1.0 of the OpenGL specification was released.
In 2006 SGI transfered control of the OpenGL standard from the ARB to a new working group 
within The Khronos Group.
*/

/*
Your graphics vendor will provide the OpenGL DLL that is used at runtime and implements the 
OpenGL functionality.
*/

/*
OpenGL Extensions:
Introduction:
Extensions in OpenGL are the mechanism to introduction new features and techniques into the 
OpenGL programming API. The DirectX API however releases regular updates to the SDK which 
exposes new features and new technology to the graphics programmer. This requires the 
developer to download the latest DirectX SDK for every new release if you want to target the 
new features of the API. OpenGL on the other hand delivers new technology and features 
through extension specifications.
Vendors (like NVIDIA and AMD) must implement the extension specifications(扩展规范) in their 
driver software that they deliver with their products. When you update your driver software,
you will usually receive a new version of the OpenGL32.dll (on Windows) which will be loaded 
dynamically by your OpenGL applications. Your application can then query the DLL to find out 
what functions and features are supported. You must obtain pointers to functions that exist in 
the DLL in order to use the extension’s features.

Extension Specifications:
All extensions must be registered with the Khronos working group.
In most cases, the ARB approved extensions will be promoted into the core API in the next 
OpenGL specification. In this case, the functions, types, and tokens that are part of the 
extension specification will be integrated into the OpenGL core specification and the ARB suffix 
will be dropped from the symbol’s names. It is a requirement that the ARB extension 
mechanism still be supported for ARB extensions that have been promoted to the core API so 
you can still query the extension support using the ARB extension strings and continue to use 
the ARB suffix on the functions, types, and tokens.
For example, the ARB_vertex_buffer_object extension has been integrated into the core 
specification in OpenGL 1.5. So using the extension specification, you can still query for the 
existence of the GL_ARB_vertex_buffer_object extension and you will use the glGenBufferARB,
glBindBufferARB, glBufferDataARB, etc… functions but if the graphics adapter is guaranteed to 
support the OpenGL 1.5 specification, you can also query for the core versions of these 
functions (glGenBuffer, glBindBuffer, glBufferData, etc…) without the ARB suffix.
The only way to check if an extension has become part of the core specification is to read the 
core specification for version of OpenGL you want to target. In the final appendices of the 
OpenGL core specification documentation, you will find lists of extensions that have been 
promoted to the core in each OpenGL release.

Querying Extensions:
Prior to OpenGL 3.0 the method for querying for supported extensions required the use of the 
glGetString method passing GL_EXTENSIONS as the only parameter. The function will return a 
pointer to a GLubyte array (which can be cast to const char* type so it can be treated like a 
string). The string returned by this function is a space-delimited list of all supported extensions.
It is the responsibility of the application programmer to correctly parse this string to determine 
if the requested extension is available.
However, this method seemed to cause some confusion especially when the extension being 
requested is the sub-string of another extension. For example, if one was querying for the 
GL_EXT_pixel_transform extension, an improperly formatted query(格式不正确的查询) may 
return a false positive if the GL_EXT_pixel_transform_color_table is listed even if 
GL_EXT_pixel_transform is not.
As of OpenGL 3.0, the accepted method for querying for supported extensions is the new 
glGetStringi method.
The glGetStringi method declaration is in the glext.h header file.

Query Supported Extensions:
GLEW defines a set of variables that can be used directly in your code to query for OpenGL 
version support or extension support. To check if a particular version of OpenGL is supported,
you can use the following code:
if (GLEW_VERSION_1_3)
{
	//Core profile OpenGL 1.3 is supported.
}
if (GLEW_ARB_vertex_program)
{
	//The ARB_vertex_program extension is supported.
}
These may look like constant macro definitions that are resolved at compile-time, but in fact 
they are actually look-up functions that are resolved at run-time.
You can also check for support of multiple extensions and core profile versions using the 
glewIsSupported function. This function takes a space-delimited string of extensions to check 
for and returns true if all of the extensions are supported.
if (glewIsSupported("GL_VERSION_1_4 GL_ARB_point_sprite"))
{
	//OpenGL 1.4 core profile and point sprite extension supported.
}
Using the glewIsSupported function we supply the extension name strings as they appear in 
the extension specification (without the GLEW prefix).
If everything is okay, you can simply use any of the functions, types, and tokens defined in the 
specification. No further work is required to get access to the function pointers defined by each 
extension, GLEW does that part for you.

Platform Specific Extensions:
Platform specific extensions are separated into two header files: wglew.h and glxew.h, which 
define the available WGL and GLX extensions. To determine if a certain extension is supported,
query WGLEW_{extension name} or GLXEW_{extension_name}. For example:
#include <GL/wglew.h>
if (WGLEW_ARB_pbuffer)
{
	//OK, we can use pbuffers.
}
else
{
	//Sorry, pbuffers will not work on this platform.
}
Alternatively, use wglewIsSupported or glxewIsSupported to check for extensions from a string:
if (wglewIsSupported("WGL_ARB_pbuffer"))
{
	//OK, we can use pbuffers.
}
*/

/*
Load OpenGL Functions:
Loading OpenGL Functions is an important task for initializing OpenGL after creating an OpenGL 
context.

Platform Specific Function Retrieval:
Querying the function pointers requires using a function not defined by the OpenGL API. It is a 
platform-specific function, and the different versions have different semantics.

Windows:
The function of interest here is wglGetProcAddress. This function takes an ASCII string that 
exactly matches (case-sensitive) the function name in question. The functions can be OpenGL 
functions or platform-specific WGL functions.
This function only works in the presence of a valid OpenGL context. Indeed, the function 
pointers it returns are themselves context-specific. The Windows documentation for this 
function states that the functions returned may work with another context, depending on the 
vendor of that context and that context's pixel format.
In practice, if two contexts come from the same vendor and refer to the same GPU, then the 
function pointers pulled from one context will work in the other. This is important when creating 
an OpenGL context in Windows, as you need to create a "dummy" context to get WGL 
extension functions to create the real one.
While the MSDN documentation says that wglGetProcAddress returns NULL on failure, some 
implementations will return other values. 1, 2, and 3 are used, as well as -1.
wglGetProcAddress will not return function pointers from any OpenGL functions that are directly 
exported by the OpenGL32.DLL itself. This means the old ones from OpenGL version 1.1.
Fortunately those functions can be obtained by the Win32's GetProcAddress. On the other hand 
GetProcAddress will not work for the functions for which wglGetProcAddress works. So in order 
to get the address of any GL function one can try with wglGetProcAddress and if it fails, try 
again with the Win32's GetProcAddress:
void* GetAnyGLFuncAddress(const char *name)
{
	void *p = (void *)wglGetProcAddress(name);
	if(p == 0 ||
		(p == (void*)0x1) || (p == (void*)0x2) || (p == (void*)0x3) ||
		(p == (void*)-1) )
	{
		HMODULE module = LoadLibraryA("opengl32.dll");
		p = (void *)GetProcAddress(module, name);
	}
	return p;
}

Function Prototypes:
These all depend on getting function pointers. We have seen the functions used to query them;
we must also have a place to store them.
Function pointers in C have a type, which includes the return type of the function and the types 
of its parameter list. This allows C to call into the memory address returned by our function 
retrieval code. If the actual function signature used by the pointer is different from the one we 
store the pointer value into, then there will be all kinds of problems.
The OpenGL Registry provides access to a header containing function pointer definitions for 
every extension and every core function for version 1.2 and above (since 1.1 and below are 
provided by Windows' gl.h). This header is called glext.h. The headers glxext.h and wglext.h 
are also provided, containing extensions for GLX and WGL. These headers are kept reasonably 
up to date with OpenGL releases.
If you are not going to use an OpenGL loading library (and you are still advised to do so), then 
you must either use one of these headers or generate one yourself. The source of these 
headers are a number of .spec files (whose format seems obvious, but is never really specified 
anywhere) also on the OpenGL Registry page.
The ext.h headers do not define actual function pointers themselves; they define the typedefs 
for function pointers. Let us take glUseProgram as an example. The ext.h file has a typedef as 
follows:
typedef void (APIENTRYP PFNGLUSEPROGRAMPROC) (GLuint program);
The name of the typedef is PFNGLUSEPROGRAMPROC: Pointer to the FunctioN glUseProgram,
which is a PROCedure. The APIENTRYP is part of a bit of macro magic needed to make 
everything work.
The use of the typedef allows us to more easily define a function pointer of the appropriate 
type in our code:
PFNGLUSEPROGRAMPROC glUseProgram;
This is not defined by the header; we have to define it ourselves. Most OpenGL loading libraries 
do not require that you define them yourself.

Platform-specific Extensions:
WGL and GLX themselves can be extended. With a few exceptions, WGL and GLX extensions 
are not advertised in the GL_EXTENSIONS string.

Windows:
To query WGL extensions,you must first create an OpenGL context.Yes,that seems backwards,
but that's how it is. To get the list of WGL extensions, you must create a context, make it 
current, and call wglGetProcAddress("wglGetExtensionsStringARB"). If this function does not 
return a valid pointer (see above for how to check), then the list of WGL extension strings is 
not available. If this pointer is invalid, then the OpenGL extension string may contain some 
WGL extensions. However, this is very unlikely, as this is an old and widely-implemented 
extension.
This function returns a string like glGetString(GL_EXTENSIONS): a space-separated list of 
extensions. So make sure to parse it correctly and don't copy it into a fixed-length buffer.
WGL function retrieval does require an active, current context. However, the use of WGL 
functions do not. Therefore, you can destroy the context after querying all of the WGL 
extension functions.
Function typedefs for WGL extensions are available in wglext.h.
*/

/*
The WGL functions:
This set of functions connects OpenGL to the Windows windowing system. The functions 
manage rendering contexts, display lists, extension functions, and font bitmaps. The WGL 
functions are analogous to the GLX extensions that connect OpenGL to the X Window System.
The names for these functions have a "wgl" prefix.
*/