/*!
 * \brief
 * GL3 GPU program
 * \file UG3Program.h
 *
 * \author Su Yang
 *
 * \date 2017/07/08
 */
#ifndef _UG3_PROGRAM_H_
#define _UG3_PROGRAM_H_

#ifndef _UG3_CONFIG_H_
#include "UG3Config.h"
#endif

#ifdef UG3_HIERARCHICAL_COMPILE

namespace ung
{

}//namespace ung

#endif//UG3_HIERARCHICAL_COMPILE

#endif//_UG3_PROGRAM_H_