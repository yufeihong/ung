/*!
 * \brief
 * D3D9顶点缓冲区管理器
 * \file UD9VertexBufferManager.h
 *
 * \author Su Yang
 *
 * \date 2017/06/09
 */
#ifndef _UD9_VERTEXBUFFERMANAGER_H_
#define _UD9_VERTEXBUFFERMANAGER_H_

#ifndef _UD9_CONFIG_H_
#include "UD9Config.h"
#endif

#ifdef UD9_HIERARCHICAL_COMPILE

#ifndef _URM_VERTEXBUFFERMANAGER_H_
#include "URMVertexBufferManager.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * D3D9顶点缓冲区管理器
	 * \class D9VertexBufferManager
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/09
	 *
	 * \todo
	 */
	class Ud9Export D9VertexBufferManager : public VertexBufferManager
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		D9VertexBufferManager();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~D9VertexBufferManager();

		/*!
		 * \remarks 创建声明
		 * \return 
		*/
		virtual SeparatedPointer<VertexDeclaration> createDeclaration() override;

		/*!
		 * \remarks 创建顶点缓冲区
		 * \return 
		 * \param uint32 vertexSize
		 * \param uint32 numVertices
		 * \param uint32 usage
		 * \param bool useShadow
		*/
		virtual SeparatedPointer<VertexBuffer> createVertexBuffer(uint32 vertexSize,uint32 numVertices,uint32 usage,bool useShadow) override;
	};
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE

#endif//_UD9_VERTEXBUFFERMANAGER_H_