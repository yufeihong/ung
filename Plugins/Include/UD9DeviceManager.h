/*!
 * \brief
 * 抽象设备管理器
 * \file UD9DeviceManager.h
 *
 * \author Su Yang
 *
 * \date 2017/05/14
 */
#ifndef _UD9_DEVICEMANAGER_H_
#define _UD9_DEVICEMANAGER_H_

#ifndef _UD9_CONFIG_H_
#include "UD9Config.h"
#endif

#ifdef UD9_HIERARCHICAL_COMPILE

#ifndef _UFC_STLWRAPPER_H_
#include "UFCSTLWrapper.h"
#endif

namespace ung
{
	class D9Device;
	class D9Window;

	/*!
	 * \brief
	 * 抽象设备管理器
	 * \class D9DeviceManager
	 *
	 * \author Su Yang
	 *
	 * \date 2017/05/14
	 *
	 * \todo
	 */
	class Ud9Export D9DeviceManager : public MemoryPool<D9DeviceManager>
	{
	public:
		using devices_type = SetWrapper<D9Device*>;
		using devices_const_iterator = typename devices_type::const_iterator;
		using devices_const_its = std::pair<devices_const_iterator, devices_const_iterator>;

	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		D9DeviceManager();

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~D9DeviceManager();

		/*!
		 * \remarks 为参数所指定的窗口选择一个抽象设备
		 * \return 
		 * \param D9Window * window
		*/
		D9Device* selectDevice(D9Window* window);

		/*!
		 * \remarks 
		 * \return 
		 * \param uint32 adapterOridinalNumber
		*/
		void createDevice(uint32 adapterOridinalNumber);

		/*!
		 * \remarks 获取active设备
		 * \return 
		*/
		D9Device* getActiveDevice() const;

		/*!
		 * \remarks 获取设备的数量
		 * \return 
		*/
		uint32 getDevicesCount() const;

		/*!
		 * \remarks 获取所有的设备
		 * \return 
		*/
		devices_type const& getAllDevices() const;

		/*!
		 * \remarks 获取所有设备的迭代器
		 * \return 
		*/
		devices_const_its getDevicesIterators() const;

	private:
		devices_type mDevices;
		D9Device* mActiveDevice;
	};
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE

#endif//_UD9_DEVICEMANAGER_H_