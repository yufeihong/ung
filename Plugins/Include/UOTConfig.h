/*!
 * \brief
 * �����ļ�
 * \file UOTConfig.h
 *
 * \author Su Yang
 *
 * \date 2017/03/27
 */
#ifndef _UOT_CONFIG_H_
#define _UOT_CONFIG_H_

#ifndef _UOT_ENABLE_H_
#include "UOTEnable.h"
#endif

#ifdef UOT_HIERARCHICAL_COMPILE

#ifndef _UNG_H_
#include "UNG.h"
#endif

#ifdef UOT_EXPORTS
#define UotExport BOOST_SYMBOL_EXPORT
#else
#define UotExport BOOST_SYMBOL_IMPORT	
#endif

#define UOT_SYMBOL_VISIBLE BOOST_SYMBOL_VISIBLE

extern ung::real_type global_world_size;
extern ung::int32 global_octree_max_depth;

#endif//UOT_HIERARCHICAL_COMPILE

#endif//_UOT_CONFIG_H_