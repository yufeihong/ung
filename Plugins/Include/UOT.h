/*!
 * \brief
 * 松散八叉树。
 * 客户代码包含这个头文件。
 * \file UOT.h
 *
 * \author Su Yang
 *
 * \date 2017/03/27
 */
#ifndef _UOT_H_
#define _UOT_H_

#ifndef _UOT_ENABLE_H_
#include "UOTEnable.h"
#endif

#ifdef UOT_HIERARCHICAL_COMPILE

#ifndef _UOT_NODE_H_
#include "UOTNode.h"
#endif

#ifndef _UOT_SCENEQUERY_H_
#include "UOTSceneQuery.h"
#endif

#endif//UOT_HIERARCHICAL_COMPILE

#endif//_UOT_H_