/*!
 * \brief
 * 八叉树空间管理器
 * \file UOTSpatialManager.h
 *
 * \author Su Yang
 *
 * \date 2017/03/28
 */
#ifndef _UOT_SPATIALMANAGER_H_
#define _UOT_SPATIALMANAGER_H_

#ifndef _UOT_CONFIG_H_
#include "UOTConfig.h"
#endif

#ifdef UOT_HIERARCHICAL_COMPILE

//协变返回类型不能前置声明，必须包含其定义
#ifndef _UOT_NODE_H_
#include "UOTNode.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 八叉树空间管理器
	 * \class OctreeSpatialManager
	 *
	 * \author Su Yang
	 *
	 * \date 2017/03/28
	 *
	 * \todo
	 */
	class OctreeSpatialManager : public SpatialManagerBase
	{
		BOOST_STATIC_ASSERT(boost::is_base_of<SpatialNodeBase, OctreeNode>::value);
		BOOST_STATIC_ASSERT(UFC_SUPERSUBCLASS_STRICT(SpatialNodeBase, OctreeNode));
		BOOST_STATIC_ASSERT(boost::is_convertible<OctreeNode*, ISpatialNode*>::value);

	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		OctreeSpatialManager() = delete;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param ISpatialManagerFactory * creator
		 * \param String const & managerName
		*/
		OctreeSpatialManager(ISpatialManagerFactory* creator,String const& managerName = EMPTY_STRING);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~OctreeSpatialManager();

		/*!
		 * \remarks 获取创建该空间管理器的工厂
		 * \return 
		*/
		virtual ISpatialManagerFactory* getCreator() const override;

		/*!
		 * \remarks 获取空间管理器工厂的标识
		 * \return 
		*/
		virtual String const& getFactoryIdentify() const override;

		/*!
		 * \remarks 获取空间管理器工厂的掩码
		 * \return 
		*/
		virtual SceneTypeMask getFactoryMask() const override;

		/*!
		 * \remarks 获取空间管理器的名字
		 * \return 
		*/
		virtual String const& getManagerName() const override;

		/*!
		 * \remarks 创建节点
		 * \return 协变返回类型
		 * \param SpatialNode * parent
		*/
		virtual OctreeNode* createNode(ISpatialNode* parent) override;

		/*!
		 * \remarks 销毁节点
		 * \return 
		 * \param SpatialNode * node
		*/
		virtual void destroyNode(ISpatialNode* node) override;

		/*!
		 * \remarks 获取空间树的根节点
		 * \return 
		*/
		virtual OctreeNode* getRootNode() const override;

		/*!
		 * \remarks 将对象放置到空间管理器中
		 * \return 返回对象所关联的节点
		 * \param StrongIObjectPtr objectPtr
		*/
		virtual OctreeNode* placeObject(StrongIObjectPtr objectPtr) override;

		/*!
		 * \remarks 将对象从空间管理器中拿走
		 * \return 
		 * \param StrongIObjectPtr objectPtr
		*/
		virtual void takeAwayObject(StrongIObjectPtr objectPtr) override;

		/*!
		 * \remarks 创建射线场景查询
		 * \return 
		 * \param Ray const & ray
		*/
		virtual StrongISceneQueryPtr createRaySceneQuery(Ray const& ray) override;

		/*!
		 * \remarks 创建球体场景查询
		 * \return 
		 * \param Sphere const & sphere
		*/
		virtual StrongISceneQueryPtr createSphereSceneQuery(Sphere const& sphere) override;

		/*!
		 * \remarks 创建AABB场景查询
		 * \return 
		 * \param AABB const & box
		*/
		virtual StrongISceneQueryPtr createAABBSceneQuery(AABB const& box) override;

		/*!
		 * \remarks 创建摄像机场景查询
		 * \return 
		 * \param Camera const & camera
		*/
		virtual StrongISceneQueryPtr createCameraSceneQuery(Camera const& camera) override;

		//-------------------------------------------------------------------------

		/*!
		 * \remarks 初始化八叉树空间管理器
		 * \return 
		 * \param AABB const & worldBox 当前管理器的包围盒
		 * \param int32 maxDepth 当前管理器的最大层数
		*/
		void init(AABB const& worldBox, int32 maxDepth);

	private:
		ISpatialManagerFactory* mCreator;
		String mName;
		AABB mBox;
		int32 mMaxDepth;
		real_type mLooseK;
		OctreeNode* mRootNode;
	};
}//namespace ung

#endif//UOT_HIERARCHICAL_COMPILE

#endif//_UOT_SPATIALMANAGER_H_