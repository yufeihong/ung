/*!
 * \brief
 * D3D9 Ч��
 * \file UD9Effect.h
 *
 * \author Su Yang
 *
 * \date 2017/06/20
 */
#ifndef _UD9_EFFECT_H_
#define _UD9_EFFECT_H_

#ifndef _UD9_CONFIG_H_
#include "UD9Config.h"
#endif

#ifdef UD9_HIERARCHICAL_COMPILE

#ifndef _URM_EFFECT_H_
#include "URMEffect.h"
#endif

namespace ung
{
	class D9Effect : public Effect
	{
	public:
	};
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE

#endif//_UD9_EFFECT_H_