/*!
 * \brief
 * 显示适配器管理器
 * \file UD9AdapterManager.h
 *
 * \author Su Yang
 *
 * \date 2017/05/13
 */
#ifndef _UD9_ADAPTERMANAGER_H_
#define _UD9_ADAPTERMANAGER_H_

#ifndef _UD9_CONFIG_H_
#include "UD9Config.h"
#endif

#ifdef UD9_HIERARCHICAL_COMPILE

namespace ung
{
	class D9Adapter;

	/*!
	 * \brief
	 * 显示适配器管理器
	 * \class D9AdapterManager
	 *
	 * \author Su Yang
	 *
	 * \date 2017/05/13
	 *
	 * \todo
	 */
	class Ud9Export D9AdapterManager : public MemoryPool<D9AdapterManager>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		D9AdapterManager();

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~D9AdapterManager();

		/*!
		 * \remarks 枚举适配器
		 * \return 
		*/
		bool enumerateAdapter();

		/*!
		 * \remarks 获取适配器的数量
		 * \return 
		*/
		uint32 getCount() const;

		/*!
		 * \remarks 获取active适配器
		 * \return 
		*/
		D9Adapter* getActiveAdapter() const;

		/*!
		 * \remarks 获取参数所指定的适配器
		 * \return 
		 * \param uint32 oridinalNumber
		*/
		D9Adapter* getAdapterFromOridinalNumber(uint32 oridinalNumber) const;

		/*!
		 * \remarks 获取与参数所匹配的适配器
		 * \return 
		 * \param HMONITOR monitor 显示器句柄
		*/
		D9Adapter* getAdapterFromMonitor(HMONITOR monitor) const;

	private:
		STL_VECTOR(D9Adapter*) mAdapters;
		D9Adapter* mActiveAdapter;
	};
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE

#endif//_UD9_ADAPTERMANAGER_H_