/*!
 * \brief
 * 物理显示适配器(An adapter is a physical piece of hardware.)
 * \file UD9Adapter.h
 *
 * \author Su Yang
 *
 * \date 2017/05/13
 */
#ifndef _UD9_ADAPTER_H_
#define _UD9_ADAPTER_H_

#ifndef _UD9_CONFIG_H_
#include "UD9Config.h"
#endif

#ifdef UD9_HIERARCHICAL_COMPILE

namespace ung
{
	class D9DisplayModeCollection;

	/*!
	 * \brief
	 * 显示适配器
	 * \class D9Adapter
	 *
	 * \author Su Yang
	 *
	 * \date 2017/05/13
	 *
	 * \todo
	 */
	class Ud9Export D9Adapter : public MemoryPool<D9Adapter>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		D9Adapter();

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param uint32 ordinalNumber 适配器的序数
		 * \param D3DADAPTER_IDENTIFIER9 const & identifier 标识符(包含对适配器的描述)
		 * \param D3DDISPLAYMODE const & desktopDisplayMode
		 * \param D3DCAPS9 const & caps
		*/
		D9Adapter(uint32 ordinalNumber, D3DADAPTER_IDENTIFIER9 const& identifier,
			D3DDISPLAYMODE const& desktopDisplayMode,D3DCAPS9 const& caps);

		/*!
		 * \remarks 拷贝构造函数
		 * \return 
		 * \param D9Adapter const &
		*/
		D9Adapter(D9Adapter const&) = delete;

		/*!
		 * \remarks 拷贝赋值运算符
		 * \return 
		 * \param D9Adapter const &
		*/
		D9Adapter& operator=(D9Adapter const&) = delete;

		/*!
		 * \remarks 移动构造函数
		 * \return 
		 * \param D9Adapter & &
		*/
		D9Adapter(D9Adapter&&) = delete;

		/*!
		 * \remarks 移动赋值运算符
		 * \return 
		 * \param D9Adapter & &
		*/
		D9Adapter& operator=(D9Adapter&&) = delete;

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~D9Adapter();

		/*!
		 * \remarks 获取名字
		 * \return 
		*/
		String getName() const;

		/*!
		 * \remarks 获取描述
		 * \return 
		*/
		String getDescription() const;

		/*!
		 * \remarks 获取适配器的序号
		 * \return 
		*/
		uint32 getOridinalNumber() const;

		/*!
		 * \remarks 获取适配器的标识符
		 * \return 
		*/
		D3DADAPTER_IDENTIFIER9 const& getIdentifier() const;

		/*!
		 * \remarks 获取桌面显示模式(the current display mode of the specified adapter)
		 * \return 
		*/
		D3DDISPLAYMODE const& getDesktopDisplayMode() const;

		/*!
		 * \remarks 获取该适配器的caps
		 * \return 
		*/
		D3DCAPS9 const& getCaps() const;

		/*!
		 * \remarks 获取在该适配器上参数所指定的显示模式的数量
		 * \return 
		 * \param D3DFORMAT fm
		*/
		uint32 getDisplayModeCount(D3DFORMAT fm) const;

		/*!
		 * \remarks 获取该适配器所支持的显示模式(参数所指定的)
		 * \return 
		 * \param D3DFORMAT fm
		 * \param uint32 modeIndex 基于0的索引
		 * \param D3DDISPLAYMODE* pOutMode
		*/
		void getD3DDisplayMode(D3DFORMAT fm,uint32 modeIndex,D3DDISPLAYMODE* pOutMode);

		/*!
		 * \remarks 获取该适配器上的所有D3D9显示模式
		 * \return 
		*/
		D9DisplayModeCollection* getDisplayModeCollection();

	private:
		uint32 mAdapterOrdinalNumber = 0;
		D3DADAPTER_IDENTIFIER9 mAdapterIdentifier;
		//current display mode
		D3DDISPLAYMODE mDesktopDisplayMode;
		D3DCAPS9 mCaps;
		D9DisplayModeCollection* mDisplayModeCollection;
	};
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE

#endif//_UD9_ADAPTER_H_