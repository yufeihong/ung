/*!
 * \brief
 * D3D9窗口
 * \file UD9Window.h
 *
 * \author Su Yang
 *
 * \date 2017/04/29
 */
#ifndef _UD9_WINDOW_H_
#define _UD9_WINDOW_H_

#ifndef _UD9_CONFIG_H_
#include "UD9Config.h"
#endif

#ifdef UD9_HIERARCHICAL_COMPILE

#ifndef _UIF_IWINDOW_H_
#include "UIFIWindow.h"
#endif

namespace ung
{
	class D9DisplayMode;
	class D9Device;

	/*!
	 * \brief
	 * D3D9窗口
	 * \class D9Window
	 *
	 * \author Su Yang
	 *
	 * \date 2017/04/29
	 *
	 * \todo
	 */
	class Ud9Export D9Window : public IWindow
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		D9Window() = delete;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param String const & title
		 * \param bool isFullScreen
		 * \param uint32 width
		 * \param uint32 height
		 * \param bool primary
		 * \param HINSTANCE inst
		*/
		D9Window(String const& title, bool isFullScreen, uint32 width, uint32 height,bool primary,HINSTANCE inst);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~D9Window();

		/*!
		 * \remarks 获取窗口类名字
		 * \return 
		*/
		virtual wchar_t const* getClassName() const override;

		/*!
		 * \remarks 获取窗口的句柄
		 * \return 
		*/
		virtual HWND getHandle() const override;

		/*!
		 * \remarks 获取标题
		 * \return 
		*/
		virtual String const& getTitle() const override;

		/*!
		 * \remarks 是否为全屏
		 * \return 
		*/
		virtual bool isFullScreen() const override;

		/*!
		 * \remarks 是否垂直同步
		 * \return 
		*/
		virtual bool isVerticalSync() const override;

		/*!
		 * \remarks 获取窗口的宽
		 * \return 
		*/
		virtual uint32 getWidth() const override;

		/*!
		 * \remarks 获取窗口的高
		 * \return 
		*/
		virtual uint32 getHeight() const override;

		/*!
		 * \remarks 设置宽
		 * \return 
		 * \param uint32 newWidth
		*/
		virtual void setWidth(uint32 newWidth) override;

		/*!
		 * \remarks 设置高
		 * \return 
		 * \param uint32 newHeight
		*/
		virtual void setHeight(uint32 newHeight) override;

		/*!
		 * \remarks 获取last宽
		 * \return 
		*/
		virtual uint32 getLastWidth() const override;

		/*!
		 * \remarks 获取last高
		 * \return 
		*/
		virtual uint32 getLastHeight() const override;

		/*!
		 * \remarks 更新last宽高
		 * \return 
		*/
		virtual void updateLastWH() override;

		/*!
		 * \remarks build style
		 * \return 
		*/
		virtual void buildStyle() override;

		/*!
		 * \remarks 获取全屏style
		 * \return 
		*/
		virtual uint32 getFullScreenStyle() const override;

		/*!
		 * \remarks 获取窗口style
		 * \return 
		*/
		virtual uint32 getWindowedStyle() const override;

		/*!
		 * \remarks 获取style
		 * \return 
		*/
		virtual uint32 getStyle() const override;

		/*!
		 * \remarks 获取窗口所归属的显示器
		 * \return 
		*/
		virtual HMONITOR getMonitor() const override;

		/*!
		 * \remarks 是否为primary
		 * \return 
		*/
		virtual bool isPrimary() const override;

		/*!
		 * \remarks 创建窗口
		 * \return 
		*/
		virtual bool create() override;

		/*!
		 * \remarks 销毁窗口
		 * \return 
		*/
		virtual void destroy() override;

		/*!
		 * \remarks 设置窗口的最小化/最大化状态(和全屏没关系)
		 * \return false_value:mined,true_value:maxed,indeterminate_value:normal
		 * \param boost::tribool state
		*/
		virtual void setMinMaxState(boost::tribool state) override;

		/*!
		 * \remarks 获取窗口的最小化/最大化状态(和全屏没关系)
		 * \return false_value:mined,true_value:maxed,indeterminate_value:normal
		*/
		virtual boost::tribool getMinMaxState() const override;

		/*!
		 * \remarks 窗口是否处于normal状态(和全屏没关系)
		 * \return 
		*/
		virtual bool isNormalState() const override;

	public:
		/*!
		 * \remarks 设置关联的抽象设备
		 * \return 
		 * \param D9Device * dv
		*/
		void setDevice(D9Device* dv);

		/*!
		 * \remarks 提交
		 * \return 
		*/
		void present();

		/*!
		 * \remarks 获取全屏模式下的显示模式
		 * \return 
		*/
		D9DisplayMode const* getFullScreenDisplayMode() const;

		/*!
		 * \remarks 获取像素格式
		 * \return 
		*/
		D3DFORMAT getPixelFormat() const;

		/*!
		 * \remarks 获取深度和模板格式
		 * \return 
		*/
		D3DFORMAT getDepthStencilFormat() const;

	//private:
		/*!
		 * \remarks 找到窗口对应的显示器句柄(基于默认的适配器)
		 * \return 
		*/
		HMONITOR _findMonitorFromDefaultAdapter();

		/*!
		 * \remarks 找到窗口对应的显示器句柄(基于已经创建的窗口句柄)
		 * \return 
		*/
		HMONITOR _findMonitorFromWindow();

		/*!
		 * \remarks 获取显示器句柄所对应的显示器的信息
		 * \return 
		 * \param HMONITOR hMonitor
		*/
		MONITORINFO _getMonitorInfo(HMONITOR hMonitor);

		/*!
		 * \remarks 获取适配器的序数(这里存在循环查找，原因是显示器句柄是从默认的适配器得来的)
		 * \return 
		*/
		uint32 _getAdapterOridinalNumberFromMonitor();

		/*!
		 * \remarks 计算窗口的left,top锚点，以及窗口的整体宽高
		 * \return 
		 * \param RECT workAreaRectangle work area rectangle of the display monitor
		 * \param uint32 & left
		 * \param uint32 & top
		 * \param uint32 & desiredWidth
		 * \param uint32 & desiredHeight
		*/
		void _calWindowPositionAndSize(RECT workAreaRectangle,uint32& left,uint32& top,uint32& desiredWidth,uint32& desiredHeight);

		/*!
		 * \remarks 注册窗口类
		 * \return 
		*/
		void _registerWindowClass();

		/*!
		 * \remarks 创建窗口
		 * \return 
		 * \param uint32 left
		 * \param uint32 top
		 * \param uint32 desiredWidth 窗口的整体尺寸
		 * \param uint32 desiredHeight
		*/
		void _createWindow(uint32 left,uint32 top,uint32 desiredWidth,uint32 desiredHeight);

		/*!
		 * \remarks 选择深度和模板格式
		 * \return 
		*/
		void _selectDepthStencilFormat();

		/*!
		 * \remarks 选择窗口模式下的像素格式
		 * \return 
		*/
		void _selectWindowedFormat();

		/*!
		 * \remarks 选择全屏模式下的显示模式
		 * \return 
		*/
		void _selectFullScreenDisplayMode();

		/*!
		 * \remarks 改变窗口/全屏标记
		 * \return 
		*/
		void changeWindowedFullScreenFlag();

	private:
		HINSTANCE mInstance;
		wchar_t* mClassName;
		HWND mHWND;
		String mTitle;
		bool mIsFullScreen;
		//客户区域大小(不是窗口的整体大小，是back buffer尺寸)
		uint32 mWidth, mHeight;
		uint32 mLastWidth, mLastHeight;
		IDirect3DSwapChain9* mSwapChain;
		IDirect3DSurface9* mBackBuffer;
		IDirect3DSurface9* mDepthBuffer;
		bool mIsPrimary;
		uint32 mFullScreenStyle = 0;
		uint32 mWindowedStyle = 0;
		HMONITOR mMonitorToWhichWindowBelongs = 0;
		D9Device* mDevice;
		bool mVerticalSync;
		uint32 mColorDepth = 32;
		D3DPRESENT_PARAMETERS mPresentParameters;
		D9DisplayMode* mFullScreenDisplayMode;
		D3DFORMAT mPixelFormat;
		D3DFORMAT mDepthStencilFormat;
		//false_value:mined,true_value:maxed,indeterminate_value:normal
		boost::tribool mState;
	};
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE

#endif//_UD9_WINDOW_H_