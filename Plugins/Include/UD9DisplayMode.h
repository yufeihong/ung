/*!
 * \brief
 * 显示模式
 * \file UD9DisplayMode.h
 *
 * \author Su Yang
 *
 * \date 2017/05/13
 */
#ifndef _UD9_DISPLAYMODE_H_
#define _UD9_DISPLAYMODE_H_

#ifndef _UD9_CONFIG_H_
#include "UD9Config.h"
#endif

#ifdef UD9_HIERARCHICAL_COMPILE

namespace ung
{
	/*!
	 * \brief
	 * 显示模式
	 * \class D9DisplayMode
	 *
	 * \author Su Yang
	 *
	 * \date 2017/05/13
	 *
	 * \todo
	 */
	class Ud9Export D9DisplayMode : public MemoryPool<D9DisplayMode>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		D9DisplayMode();

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param D3DDISPLAYMODE const & dm
		*/
		D9DisplayMode(D3DDISPLAYMODE const& dm);

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~D9DisplayMode();

		/*!
		 * \remarks 获取宽，单位：像素
		 * \return 
		*/
		uint32 getWidth() const;

		/*!
		 * \remarks 获取高，单位：像素
		 * \return 
		*/
		uint32 getHeight() const;

		/*!
		 * \remarks 获取像素格式
		 * \return 
		*/
		D3DFORMAT getPixelFormat() const;

		/*!
		 * \remarks 获取刷新频率
		 * \return 
		*/
		uint32 getRefreshRate() const;

		/*!
		 * \remarks 获取颜色位深
		 * \return 
		*/
		uint32 getColorDepth() const;

		/*!
		 * \remarks 获取D3D9显示模式
		 * \return 
		*/
		D3DDISPLAYMODE const& getD3DDisplayMode() const;

		/*!
		 * \remarks 提升刷新频率
		 * \return 
		 * \param uint32 rr
		*/
		void promoteRefreshRate(uint32 rr);

	private:
		D3DDISPLAYMODE mDisplayMode;
	};

	class D9Adapter;

	/*!
	 * \brief
	 * 显示模式集合
	 * \class D9DisplayModeCollection
	 *
	 * \author Su Yang
	 *
	 * \date 2017/05/13
	 *
	 * \todo
	 */
	class Ud9Export D9DisplayModeCollection : public MemoryPool<D9DisplayModeCollection>
	{
	public:
		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param D9Adapter * ap
		*/
		D9DisplayModeCollection(D9Adapter* ap);

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~D9DisplayModeCollection();

		/*!
		 * \remarks 枚举显示模式
		 * \return 
		*/
		bool enumerateDisplayMode();

		/*!
		 * \remarks 获取显示模式
		 * \return 
		 * \param uint32 width
		 * \param uint32 height
		 * \param D3DFORMAT pixelFormat
		*/
		D9DisplayMode* getDisplayMode(uint32 width, uint32 height, D3DFORMAT pixelFormat);

		/*!
		 * \remarks 获取显示模式的数量
		 * \return 
		*/
		uint32 getCount() const;

		/*!
		 * \remarks 获取存储所有显示模式的容器
		 * \return 
		 * \param D9DisplayMode *
		*/
		STL_VECTOR(D9DisplayMode*) const& getCollection() const;

	private:
		D9Adapter* mAdapter;
		STL_VECTOR(D9DisplayMode*) mCollection;
	};
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE

#endif//_UD9_DISPLAYMODE_H_