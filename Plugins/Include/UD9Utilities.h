/*!
 * \brief
 * D3D9工具
 * \file UD9Utilities.h
 *
 * \author Su Yang
 *
 * \date 2017/05/10
 */
#ifndef _UD9_UTILITIES_H_
#define _UD9_UTILITIES_H_

#ifndef _UD9_CONFIG_H_
#include "UD9Config.h"
#endif

#ifdef UD9_HIERARCHICAL_COMPILE

//
#include "UFCSeparatedPointer.h"

#ifndef _UIF_ENUMCOLLECTION_H_
#include "UIFEnumCollection.h"
#endif

//
#include "UIFIVideoBuffer.h"
#include "UIFISurface.h"

namespace ung
{
	UNG_C_LINK_BEGIN;

	/*!
	 * \remarks 像素格式转换
	 * \return 
	 * \param D3DFORMAT d9fm
	*/
	Ud9Export PixelFormat UNG_STDCALL ud9ToUniformPixelFormat(D3DFORMAT d9fm);

	/*!
	 * \remarks 像素格式转换
	 * \return 
	 * \param PixelFormat uniformpf
	*/
	Ud9Export D3DFORMAT UNG_STDCALL ud9ToD3D9PixelFormat(PixelFormat uniformpf);

	/*!
	 * \remarks 资源池类型转换
	 * \return 
	 * \param ResourcePoolType rpt
	*/
	Ud9Export D3DPOOL UNG_STDCALL ud9ToD3D9ResourcePool(ResourcePoolType rpt);

	/*!
	 * \remarks 资源池类型转换
	 * \return 
	 * \param D3DPOOL rpt
	*/
	Ud9Export ResourcePoolType UNG_STDCALL ud9ToUniformResourcePool(D3DPOOL rpt);

	/*!
	 * \remarks 矩形区域转换
	 * \return 
	 * \param PixelRectangle const & pr
	 * \param SeparatedPointer<RECT>& out 手动销毁
	*/
	Ud9Export void UNG_STDCALL ud9ToD3D9Rect(PixelRectangle const& pr,SeparatedPointer<RECT>& out);

	/*!
	 * \remarks 矩形区域转换
	 * \return 
	 * \param RECT* const rect
	 * \param PixelRectangle & out
	*/
	Ud9Export void UNG_STDCALL ud9ToUniformRect(RECT* const rect,PixelRectangle& out);

	/*!
	 * \remarks lock surface flag转换
	 * \return 
	 * \param LockSurfaceFlag const & lsf
	*/
	Ud9Export uint32 UNG_STDCALL ud9ToD3D9LockSurfaceFlag(LockSurfaceFlag const& lsf);

	/*!
	 * \remarks lock surface flag转换
	 * \return 
	 * \param uint32 lsf
	*/
	Ud9Export LockSurfaceFlag UNG_STDCALL ud9ToUniformLockSurfaceFlag(uint32 lsf);

	/*!
	 * \remarks vertex element type转换
	 * \return 
	 * \param VertexElementType const & vet
	*/
	Ud9Export uint32 UNG_STDCALL ud9ToD3D9ElementType(VertexElementType const& vet);

	/*!
	 * \remarks vertex element type转换
	 * \return 
	 * \param uint32 vet
	*/
	Ud9Export VertexElementType UNG_STDCALL ud9ToUniformElementType(uint32 vet);

	/*!
	 * \remarks vertex semantic转换
	 * \return 
	 * \param VertexElementSemantic const & ves
	*/
	Ud9Export uint32 UNG_STDCALL ud9ToD3D9ElementSemantic(VertexElementSemantic const& ves);

	/*!
	 * \remarks vertex semantic转换
	 * \return 
	 * \param uint32 ves
	*/
	Ud9Export VertexElementSemantic UNG_STDCALL ud9ToUniformElementSemantic(uint32 ves);

	/*!
	 * \remarks usage转换
	 * \return 
	 * \param uint32 us
	*/
	Ud9Export uint32 UNG_STDCALL ud9ToD3D9Usage(uint32 us);

	/*!
	 * \remarks usage转换
	 * \return 
	 * \param uint32 us
	*/
	Ud9Export uint32 UNG_STDCALL ud9ToUniformUsage(uint32 us);

	/*!
	 * \remarks cull mode转换
	 * \return 
	 * \param CullMode cm
	*/
	Ud9Export uint32 UNG_STDCALL ud9ToD3D9CullMode(CullMode cm);

	/*!
	 * \remarks cull mode转换
	 * \return 
	 * \param uint32 cm
	*/
	Ud9Export CullMode UNG_STDCALL ud9ToUniformCullMode(uint32 cm);

	UNG_C_LINK_END;
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE

#endif//_UD9_UTILITIES_H_