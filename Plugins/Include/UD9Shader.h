/*!
 * \brief
 * D3D9 shader
 * \file UD9Shader.h
 *
 * \author Su Yang
 *
 * \date 2017/06/10
 */
#ifndef _UD9_SHADER_H_
#define _UD9_SHADER_H_

#ifndef _UD9_CONFIG_H_
#include "UD9Config.h"
#endif

#ifdef UD9_HIERARCHICAL_COMPILE

#ifndef _URM_SHADER_H_
#include "URMShader.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * D3D9 shader基类
	 * \class D9Shader
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/17
	 *
	 * \todo
	 */
	class D9Shader : public Shader
	{
	public:
		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param String const& fullName
		 * \param ShaderType type
		*/
		D9Shader(String const& fullName,ShaderType type);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~D9Shader();

		/*!
		 * \remarks 指针容器接口
		 * \return 
		*/
		virtual Shader* clone() const override;

		/*!
		 * \remarks 设置常量
		 * \return 
		 * \param const char* name
		 * \param void* pBuffer
		 * \param uint32 bytes
		*/
		virtual void setConstant(const char* name, void* pBuffer,uint32 bytes) override;

	protected:
		ID3DXConstantTable* mConstantTable;
	};

	/*!
	 * \brief
	 * D3D9 vertex shader
	 * \class D9VertexShader
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/10
	 *
	 * \todo
	 */
	class D9VertexShader : public D9Shader
	{
	public:
		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param String const & fullName 全名
		*/
		D9VertexShader(String const& fullName);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~D9VertexShader();

		/*!
		 * \remarks 指针容器接口
		 * \return 
		*/
		virtual Shader* clone() const override;

		/*!
		 * \remarks 编译shader
		 * \return 
		*/
		virtual void compile() override;

	public:
		/*!
		 * \remarks 获取D3D9 shader
		 * \return 
		*/
		IDirect3DVertexShader9* getShader() const;

	private:
		IDirect3DVertexShader9* mShader;
	};//end class D9VertexShader

	/*!
	 * \brief
	 * D3D0 pixel shader
	 * \class D9PixelShader
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/15
	 *
	 * \todo
	 */
	class D9PixelShader : public D9Shader
	{
	public:
		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param String const & fullName 全名
		*/
		D9PixelShader(String const& fullName);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~D9PixelShader();

		/*!
		 * \remarks 指针容器接口
		 * \return 
		*/
		virtual Shader* clone() const override;

		/*!
		 * \remarks 编译shader
		 * \return 
		*/
		virtual void compile() override;

	public:
		/*!
		 * \remarks 获取D3D9 shader
		 * \return 
		*/
		IDirect3DPixelShader9* getShader() const;

	private:
		IDirect3DPixelShader9* mShader;
	};//end class D9PixelShader
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE

#endif//_UD9_SHADER_H_