/*!
 * \brief
 * GL3
 * 客户代码包含这个头文件。
 * \file UG3.h
 *
 * \author Su Yang
 *
 * \date 2017/07/08
 */
#ifndef _UG3_H_
#define _UG3_H_

#ifndef _UG3_ENABLE_H_
#include "UG3Enable.h"
#endif

#ifdef UG3_HIERARCHICAL_COMPILE

#endif//UG3_HIERARCHICAL_COMPILE

#endif//_UG3_H_