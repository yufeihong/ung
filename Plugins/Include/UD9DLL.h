/*!
 * \brief
 * D3D9 dll
 * \file UD9DLL.h
 *
 * \author Su Yang
 *
 * \date 2017/03/27
 */
#ifndef _UD9_DLL_H_
#define _UD9_DLL_H_

#ifndef _UD9_CONFIG_H_
#include "UD9Config.h"
#endif

#ifdef UD9_HIERARCHICAL_COMPILE

namespace ung
{
	UNG_C_LINK_BEGIN;

	/*!
	 * \remarks 创建插件
	 * \return 
	*/
	Ud9Export void dllCreatePlugin() noexcept;

	/*!
	 * \remarks 销毁插件
	 * \return 
	*/
	Ud9Export void dllDestroyPlugin();

	UNG_C_LINK_END;
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE

#endif//_UD9_DLL_H_