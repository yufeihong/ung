/*!
 * \brief
 * 八叉树插件
 * \file UOTPlugin.h
 *
 * \author Su Yang
 *
 * \date 2017/03/27
 */
#ifndef _UOT_PLUGIN_H_
#define _UOT_PLUGIN_H_

#ifndef _UOT_CONFIG_H_
#include "UOTConfig.h"
#endif

#ifdef UOT_HIERARCHICAL_COMPILE

namespace ung
{
	class OctreeSpatialManagerFactory;

	/*!
	 * \brief
	 * 八叉树插件
	 * \class OCTPlugin
	 *
	 * \author Su Yang
	 *
	 * \date 2017/03/27
	 *
	 * \todo
	 */
	class OCTPlugin : public Plugin
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		OCTPlugin();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~OCTPlugin();

		/*!
		 * \remarks 获取插件的名字
		 * \return 
		*/
		virtual const char* getName() const;

		/*!
		 * \remarks 安装插件
		 * \return 
		*/
		virtual void install();

		/*!
		 * \remarks 卸载插件
		 * \return 
		*/
		virtual void uninstall();

	private:
		OctreeSpatialManagerFactory* mOctreeSpatialManagerFactory;
	};
}//namespace ung

#endif//UOT_HIERARCHICAL_COMPILE

#endif//_UOT_PLUGIN_H_