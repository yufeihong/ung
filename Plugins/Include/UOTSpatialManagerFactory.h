/*!
 * \brief
 * 八叉树空间管理器工厂
 * \file UOTSpatialManagerFactory.h
 *
 * \author Su Yang
 *
 * \date 2017/03/28
 */
#ifndef _UOT_SPATIALMANAGERFACTORY_H_
#define _UOT_SPATIALMANAGERFACTORY_H_

#ifndef _UOT_CONFIG_H_
#include "UOTConfig.h"
#endif

#ifdef UOT_HIERARCHICAL_COMPILE

namespace ung
{
	/*!
	 * \brief
	 * 八叉树空间管理器工厂
	 * \class OctreeSpatialManagerFactory
	 *
	 * \author Su Yang
	 *
	 * \date 2017/03/28
	 *
	 * \todo
	 */
	class OctreeSpatialManagerFactory : public SpatialManagerFactoryBase
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		OctreeSpatialManagerFactory();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~OctreeSpatialManagerFactory();

		/*!
		 * \remarks 获取工厂的标识
		 * \return 
		*/
		virtual String const& getIdentity() const override;

		/*!
		 * \remarks 获取工厂的标识
		 * \return 
		*/
		virtual SceneTypeMask getMask() const override;

		/*!
		 * \remarks 创建一个空间管理器
		 * \return 
		 * \param String const & instanceName 空间管理器的名字
		*/
		virtual ISpatialManager* createInstance(String const& instanceName) override;

		/*!
		 * \remarks 销毁一个空间管理器
		 * \return 
		 * \param ISpatialManager * instancePtr 指向空间管理器的指针
		*/
		virtual void destroyInstance(ISpatialManager* instancePtr) override;

	private:
		String mIdentify;
	};
}//namespace ung

#endif//UOT_HIERARCHICAL_COMPILE

#endif//_UOT_SPATIALMANAGERFACTORY_H_