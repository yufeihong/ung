/*!
 * \brief
 * D3D9 vertex buffer
 * \file UD9VertexBuffer.h
 *
 * \author Su Yang
 *
 * \date 2017/06/02
 */
#ifndef _UD9_VERTEXBUFFER_H_
#define _UD9_VERTEXBUFFER_H_

#ifndef _UD9_CONFIG_H_
#include "UD9Config.h"
#endif

#ifdef UD9_HIERARCHICAL_COMPILE

#ifndef _URM_VERTEXBUFFER_H_
#include "URMVertexBuffer.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * D3D9 vertex buffer
	 * \class D9VertexBuffer
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/12
	 *
	 * \todo
	 */
	class Ud9Export D9VertexBuffer : public VertexBuffer
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		D9VertexBuffer();

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param uint32 vertexSize 一个顶点的大小，单位：字节
		 * \param uint32 numVertices
		 * \param uint32 usage
		 * \param bool useShadowBuffer
		*/
		D9VertexBuffer(uint32 vertexSize, uint32 numVertices,uint32 usage, bool useShadowBuffer);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~D9VertexBuffer();

		/*!
		 * \remarks Writes data to the buffer from an area of system memory
		 * \return 
		 * \param uint32 offset The byte offset from the start of the buffer to start writing
		 * \param uint32 length
		 * \param const void* pSource
		 * \param bool discardWholeBuffer 建议为true，这样就允许驱动在写入数据的时候丢弃整个缓存，就可以避免DMA stalls
		*/
		virtual void writeData(uint32 offset,uint32 length,const void* pSource,bool discardWholeBuffer = true) override;

		/*!
		 * \remarks 获取描述
		 * \return 
		 * \param D3DVERTEXBUFFER_DESC & desc
		*/
		void getDesc(D3DVERTEXBUFFER_DESC& desc);

	//protected:
		virtual void* lockImpl(uint32 offset, uint32 length, LockOptions options) override;

		virtual void unlockImpl() override;

	//private:
		IDirect3DVertexBuffer9* mVB;
	};
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE

#endif//_UD9_VERTEXBUFFER_H_