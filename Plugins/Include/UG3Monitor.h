/*!
 * \brief
 * 显示器
 * \file UG3Monitor.h
 *
 * \author Su Yang
 *
 * \date 2017/07/13
 */
#ifndef _UG3_MONITOR_H_
#define _UG3_MONITOR_H_

#ifndef _UG3_CONFIG_H_
#include "UG3Config.h"
#endif

#ifdef UG3_HIERARCHICAL_COMPILE

namespace ung
{
	/*!
	 * \brief
	 * 显示器
	 * \class G3Monitor
	 *
	 * \author Su Yang
	 *
	 * \date 2017/07/13
	 *
	 * \todo
	 */
	class G3Monitor : public MemoryPool<G3Monitor>
	{
	public:
		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param HMONITOR hMonitor
		*/
		G3Monitor(HMONITOR hMonitor);

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~G3Monitor();

	private:
		HMONITOR mHMonitor;
		MONITORINFOEX mInfo;
	};
}//namespace ung

#endif//UG3_HIERARCHICAL_COMPILE

#endif//_UG3_MONITOR_H_