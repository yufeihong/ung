/*!
 * \brief
 * GL3插件
 * \file UG3Plugin.h
 *
 * \author Su Yang
 *
 * \date 2017/07/08
 */
#ifndef _UG3_PLUGIN_H_
#define _UG3_PLUGIN_H_

#ifndef _UG3_CONFIG_H_
#include "UG3Config.h"
#endif

#ifdef UG3_HIERARCHICAL_COMPILE

namespace ung
{
	class G3RenderSystem;

	/*!
	 * \brief
	 * GL3插件
	 * \class G3Plugin
	 *
	 * \author Su Yang
	 *
	 * \date 2017/07/08
	 *
	 * \todo
	 */
	class G3Plugin : public Plugin
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		G3Plugin();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~G3Plugin();

		/*!
		 * \remarks 获取插件的名字
		 * \return 
		*/
		virtual const char* getName() const;

		/*!
		 * \remarks 安装插件
		 * \return 
		*/
		virtual void install();

		/*!
		 * \remarks 卸载插件
		 * \return 
		*/
		virtual void uninstall();

	private:
		G3RenderSystem* mRenderSystem;
	};
}//namespace ung

#endif//UG3_HIERARCHICAL_COMPILE

#endif//_UG3_PLUGIN_H_