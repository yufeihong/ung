/*!
 * \brief
 * D3D9视口
 * \file UD9Viewport.h
 *
 * \author Su Yang
 *
 * \date 2017/06/18
 */
#ifndef _UD9_VIEWPORT_H_
#define _UD9_VIEWPORT_H_

#ifndef _UD9_CONFIG_H_
#include "UD9Config.h"
#endif

#ifdef UD9_HIERARCHICAL_COMPILE

#ifndef _URM_VIEWPORT_H_
#include "URMViewport.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * D3D9视口
	 * \class D9Viewport
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/18
	 *
	 * \todo
	 */
	class D9Viewport : public Viewport
	{
	public:
		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param uint32 left
		 * \param uint32 top
		 * \param uint32 width
		 * \param uint32 height
		 * \param int32 zOrder
		*/
		D9Viewport(uint32 left,uint32 top,uint32 width,uint32 height,int32 zOrder = 0);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~D9Viewport();

		/*!
		 * \remarks clear
		 * \return 
		 * \param uint32 buffers
		 * \param const real_color & color
		 * \param real_type depth
		 * \param uint32 stencil
		*/
		virtual void clear(uint32 buffers, const real_color& color = UNG_COLOR_BLACK,
			real_type depth = 1.0, uint32 stencil = 0) override;

		/*!
		 * \remarks 获取左
		 * \return 
		*/
		virtual uint32 getLeft() const override;

		/*!
		 * \remarks 获取上
		 * \return 
		*/
		virtual uint32 getTop() const override;

		/*!
		 * \remarks 获取宽
		 * \return 
		*/
		virtual uint32 getWidth() const override;

		/*!
		 * \remarks 获取高
		 * \return 
		*/
		virtual uint32 getHeight() const override;

		/*!
		 * \remarks 获取min z
		 * \return 
		*/
		virtual real_type getMinZ() const override;

		/*!
		 * \remarks 获取max z
		 * \return 
		*/
		virtual real_type getMaxZ() const override;

		/*!
		 * \remarks 设置视口
		 * \return 
		*/
		virtual void set() override;

	private:
		D3DVIEWPORT9 mVP;
	};
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE

#endif//_UD9_VIEWPORT_H_