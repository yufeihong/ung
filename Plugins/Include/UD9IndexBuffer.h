/*!
 * \brief
 * D3D9 index buffer
 * \file UD9IndexBuffer.h
 *
 * \author Su Yang
 *
 * \date 2017/06/02
 */
#ifndef _UD9_INDEXBUFFER_H_
#define _UD9_INDEXBUFFER_H_

#ifndef _UD9_CONFIG_H_
#include "UD9Config.h"
#endif

#ifdef UD9_HIERARCHICAL_COMPILE

namespace ung
{
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE

#endif//_UD9_INDEXBUFFER_H_