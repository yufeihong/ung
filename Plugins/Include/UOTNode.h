/*!
 * \brief
 * 八叉树节点。
 * \file UOTNode.h
 *
 * \author Su Yang
 *
 * \date 2017/03/27
 */
#ifndef _UOT_NODE_H_
#define _UOT_NODE_H_

#ifndef _UOT_CONFIG_H_
#include "UOTConfig.h"
#endif

#ifdef UOT_HIERARCHICAL_COMPILE

#ifndef _INCLUDE_STLIB_FUNCTIONAL_
#include <functional>
#define _INCLUDE_STLIB_FUNCTIONAL_
#endif

namespace ung
{
	/*!
	 * \brief
	 * 八叉树节点。
	 * \class OctreeNode
	 *
	 * \author Su Yang
	 *
	 * \date 2017/03/27
	 *
	 * \todo
	 */
	class UotExport OctreeNode : public SpatialNodeBase
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		OctreeNode() = delete;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param OctreeNode * parent
		*/
		OctreeNode(OctreeNode* parent);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~OctreeNode();

		/*!
		 * \remarks 拷贝构造函数
		 * \return 
		 * \param OctreeNode const &
		*/
		OctreeNode(OctreeNode const&) = delete;

		/*!
		 * \remarks 拷贝赋值运算符
		 * \return 
		 * \param OctreeNode const &
		*/
		OctreeNode& operator=(OctreeNode const&) = delete;

		/*!
		 * \remarks 移动构造函数
		 * \return 
		 * \param OctreeNode & &
		*/
		OctreeNode(OctreeNode&&) = delete;

		/*!
		 * \remarks 移动赋值运算符
		 * \return 
		 * \param OctreeNode & &
		*/
		OctreeNode& operator=(OctreeNode&&) = delete;

		//-------------------------------------------------------------------

		/*!
		 * \remarks 获取父节点
		 * \return 协变返回类型
		*/
		virtual OctreeNode* getParent() const override;

		/*!
		 * \remarks 获取当前节点的深度
		 * \return 
		*/
		virtual int32 getDepth() const override;

		/*!
		 * \remarks 关联一个对象到当前节点
		 * \return 
		 * \param StrongIObjectPtr objectPtr
		*/
		virtual void attachObject(StrongIObjectPtr objectPtr) override;

		/*!
		 * \remarks 将一个对象脱离当前节点
		 * \return 
		 * \param StrongIObjectPtr objectPtr
		*/
		virtual void detachObject(StrongIObjectPtr objectPtr) override;

		/*!
		 * \remarks 创建一个孩子节点
		 * \return 
		*/
		virtual OctreeNode* createChild() override;

		/*!
		 * \remarks 创建多个孩子节点
		 * \return 
		*/
		virtual void createChildren() override;

		/*!
		 * \remarks 销毁多个孩子节点
		 * \return 
		*/
		void destroyChildren();

		/*!
		 * \remarks 获取参数所指定的邻居节点
		 * \return 
		 * \param uint32 index
		*/
		OctreeNode* getNeighbor(uint32 index) const;

		/*!
		 * \remarks 获取参数所指定的孩子节点
		 * \return 
		 * \param uint32 index
		*/
		OctreeNode* getChild(uint32 index) const;

		//-------------------------------------------------------------------

		/*!
		 * \remarks 当前节点是否有孩子节点
		 * \return 
		*/
		bool hasChildren() const;

		/*!
		 * \remarks 获取节点的中心坐标
		 * \return 
		*/
		Vector3 const& getCenterPosition() const;

		/*!
		 * \remarks 获取当前节点的AABB(世界空间)
		 * \return 
		*/
		AABB getAABB() const;

		/*!
		 * \remarks 放置对象到八叉树中的一个合适位置
		 * \return 
		 * \param StrongIObjectPtr objectPtr
		*/
		OctreeNode* push(StrongIObjectPtr objectPtr);

		/*!
		 * \remarks 从八叉树中移除对象
		 * \return 
		 * \param StrongIObjectPtr objectPtr
		*/
		void pop(StrongIObjectPtr objectPtr);

		/*!
		 * \remarks 获取节点所关联的对象的数量
		 * \return 
		*/
		uint32 getAttachedObjectCount() const;

		/*!
		 * \remarks 节点是否关联了对象
		 * \return 
		*/
		bool hasAttachedObject() const;

		/*!
		 * \remarks 销毁节点时深度优先遍历(后序:先子后父)
		 * \return 
		 * \param std::function<void(OctreeNode*)> fn
		*/
		void postTraverseDepthFirstForDestroy(std::function<void(OctreeNode*)> fn);

		/*!
		 * \remarks 通用，深度优先遍历(前序:先父后子)
		 * \return 
		 * \param std::function<void(OctreeNode*)> fn
		*/
		void preTraverseDepthFirst(std::function<void(OctreeNode*)> fn);

		/*!
		 * \remarks 为一个给定的点，找到一个合适的节点
		 * \return 
		 * \param Vector3 const & worldPosition 
		*/
		OctreeNode* findSuitableNode(Vector3 const& worldPosition);

		/*!
		 * \remarks 为一个给定的对象，找到一个合适的节点
		 * \return 
		 * \param StrongIObjectPtr objectPtr
		*/
		OctreeNode* findSuitableNode(StrongIObjectPtr objectPtr);

		/*!
		 * \remarks 为一个给定的球体，找到一个合适的节点
		 * \return 
		 * \param Sphere const& sphere
		*/
		OctreeNode* findSuitableNode(Sphere const& sphere);

		/*!
		 * \remarks 为一个给定的AABB，找到一个合适的节点
		 * \return 
		 * \param AABB const& box
		*/
		OctreeNode* findSuitableNode(AABB const& box);

		/*!
		 * \remarks 测试当前节点是否可以容纳参数所指定的对象
		 * \return 
		 * \param StrongIObjectPtr objectPtr
		*/
		bool canContain(StrongIObjectPtr objectPtr);

		/*!
		 * \remarks 测试当前节点是否可以容纳参数所指定的球体
		 * \return 
		 * \param Sphere const & sphere
		*/
		bool canContain(Sphere const& sphere);

		/*!
		 * \remarks 测试当前节点是否可以容纳参数所指定的AABB
		 * \return 
		 * \param AABB const& box 世界空间的AABB
		*/
		bool canContain(AABB const& box);

		/*!
		 * \remarks 获取对象的常量迭代器
		 * \return 
		*/
		typename ListConstIterators<StrongIObjectPtr>::type getObjectsConstIterators() const;

	private:
		/*!
		 * \remarks 计算8个子节点的中心坐标
		 * \return 
		 * \param uint8 index 标记当前节点为父节点8个子节点中的哪一个
		*/
		void calCenterPosition(uint8 index);

		/*!
		 * \remarks 计算给定孩子节点的6个面的邻居节点(父节点调用该函数)
		 * \return 
		 * \param uint8 nodeIndex 标记当前节点为父节点8个子节点中的哪一个
		 * \param uint8 faceIndex 标记当前面为当前节点6个面中的哪一个
		*/
		void calChildrenNeighbor();

		/*!
		 * \remarks 计算每个box的3个外表面的邻居(同时，对邻居的邻居也进行了更新)
		 * \return 
		 * \param uint8 nodeIndex
		 * \param uint8 faceIndex
		*/

		void calOuterSurfaceNeighbor(uint8 faceIndex);

		/*!
		 * \remarks 找到一个可能合适的节点
		 * \return 
		 * \param Vector3 const & worldPosition
		 * \param int32 proDepth
		*/
		OctreeNode* findProbablyNode(Vector3 const& worldPosition,int32 proDepth);

		/*!
		 * \remarks push的实现
		 * \return 
		 * \param StrongIObjectPtr objectPtr
		*/
		OctreeNode* pushImpl(StrongIObjectPtr objectPtr);

	private:
		OctreeNode* mParent;
		OctreeNode* mChildren[8];
		OctreeNode* mNeighbors[6];
		STL_LIST(StrongIObjectPtr) mObjects;
		int32 mDepth;
		Vector3 mCenter;
	};
}//namespace ung

#endif//UOT_HIERARCHICAL_COMPILE

#endif//_UOT_NODE_H_