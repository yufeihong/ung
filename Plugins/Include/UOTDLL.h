/*!
 * \brief
 * dll
 * \file UOTDLL.h
 *
 * \author Su Yang
 *
 * \date 2017/03/27
 */
#ifndef _UOT_DLL_H_
#define _UOT_DLL_H_

#ifndef _UOT_CONFIG_H_
#include "UOTConfig.h"
#endif

#ifdef UOT_HIERARCHICAL_COMPILE

namespace ung
{
	UNG_C_LINK_BEGIN;

	/*!
	 * \remarks 创建插件
	 * \return 
	*/
	UotExport void dllCreatePlugin() noexcept;

	/*!
	 * \remarks 销毁插件
	 * \return 
	*/
	UotExport void dllDestroyPlugin();

	UNG_C_LINK_END;
}//namespace ung

#endif//UOT_HIERARCHICAL_COMPILE

#endif//_UOT_DLL_H_