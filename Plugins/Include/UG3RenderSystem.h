/*!
 * \brief
 * GL3渲染系统
 * \file UG3RenderSystem.h
 *
 * \author Su Yang
 *
 * \date 2017/07/08
 */
#ifndef _UG3_RENDERSYSTEM_H_
#define _UG3_RENDERSYSTEM_H_

#ifndef _UG3_CONFIG_H_
#include "UG3Config.h"
#endif

#ifdef UG3_HIERARCHICAL_COMPILE

#ifndef _UIF_IRENDERSYSTEM_H_
#include "UIFIRenderSystem.h"
#endif

#ifndef _UFC_STLWRAPPER_H_
#include "UFCSTLWrapper.h"
#endif

#ifndef _UFC_COLORVALUE_H_
#include "UFCColorValue.h"
#endif

namespace ung
{
	class G3Monitor;

	/*!
	 * \brief
	 * GL3渲染系统
	 * \class G3RenderSystem
	 *
	 * \author Su Yang
	 *
	 * \date 2017/07/08
	 *
	 * \todo
	 */
	class Ug3Export G3RenderSystem : public IRenderSystem
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		G3RenderSystem() = delete;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param HINSTANCE inst
		*/
		G3RenderSystem(HINSTANCE inst);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~G3RenderSystem();

		/*!
		 * \remarks 初始化
		 * \return 
		*/
		virtual void initialize() override;

		/*!
		 * \remarks 是否已经初始化了
		 * \return 
		*/
		virtual bool isInitialized() const override;

		/*!
		 * \remarks 获取渲染系统类型
		 * \return 
		*/
		virtual RenderSystemType getType() const override;

		/*!
		 * \remarks 渲染前的准备工作
		 * \return 
		*/
		virtual void prepare() override;

		/*!
		 * \remarks 设置窗口/全屏
		 * \return 
		 * \param bool windowed true:窗口
		*/
		virtual void setWindowed(bool windowed) override;

		/*!
		 * \remarks 获取是否为窗口
		 * \return true:窗口
		*/
		virtual bool getWindowed() const override;

		/*!
		 * \remarks 设置back buffer的宽高
		 * \return 
		 * \param uint32 width
		 * \param uint32 height
		*/
		virtual void setBackBufferWH(uint32 width, uint32 height) override;

		/*!
		 * \remarks 获取back buffer的宽高
		 * \return 
		*/
		virtual std::pair<uint32, uint32> getBackBufferWH() const override;

		/*!
		 * \remarks Get the native color type for this rendersystem.
         * \return 
        */
        virtual VertexElementType getNativeColorType() const override;

		/*!
		 * \remarks 设置back buffer的像素格式
		 * \return 
		 * \param PixelFormat pf
		*/
		virtual void setBackBufferPixelFormat(PixelFormat pf) override;

		/*!
		 * \remarks 获取back buffer的像素格式
		 * \return 
		*/
		virtual PixelFormat getBackBufferPixelFormat() const override;

		/*!
		 * \remarks 创建一个offscreen plain表面
		 * \return 
		 * \param uint32 width 单位：像素个数
		 * \param uint32 height
		 * \param PixelFormat format
		 * \param ResourcePoolType pt
		*/
		virtual IOffscreenPlainSurface* createOffscreenPlainSurface(uint32 width,uint32 height,PixelFormat format, ResourcePoolType pt = ResourcePoolType::RPT_DEFAULT) override;
		
		/*!
		 * \remarks 创建窗口
		 * \return 
		 * \param String const & title
		 * \param bool isFullScreen
		 * \param uint32 width back buffer width
		 * \param uint32 height back buffer height
		 * \param bool primary
		*/
		virtual bool createWindow(String const& title, bool isFullScreen, uint32 width, uint32 height,bool primary = false) override;

		/*!
		 * \remarks 关闭渲染系统(不要在卸载/销毁插件时调用，而应在插件正常状态时调用)
		 * \return 
		*/
		virtual void shutdown() override;

		/*!
		 * \remarks 设备是否丢失
		 * \return 
		*/
		virtual bool isDeviceLost() override;

		/*!
		 * \remarks 处理窗口大小变化的消息
		 * \return 
		*/
		virtual void onSizeChanged() override;

		/*!
		 * \remarks 获取active window
		 * \return 
		*/
		virtual IWindow* getActiveWindow() override;

		/*!
		 * \remarks 在窗口模式和全屏模式之间切换
		 * \return 
		*/
		virtual void switchBetweenWindowedAndFullScreen() override;

		/*!
		 * \remarks 更新
		 * \return 
		 * \param real_type dt 单位:毫秒
		*/
		virtual void update(real_type dt) override;

		/*!
		 * \remarks 渲染一帧
		 * \return 
		 * \param real_type deltaTime 单位：毫秒
		*/
		virtual void renderOneFrame(real_type deltaTime) override;

		/*!
		 * \remarks 创建摄像机
		 * \return 
		 * \param String const & name
		 * \param Vector3 const & pos
		 * \param Vector3 const & lookat
		 * \param real_type aspect
		*/
		virtual Camera* createCamera(String const& name,Vector3 const& pos, Vector3 const& lookat, real_type aspect) override;

		/*!
		 * \remarks 删除摄像机
		 * \return 
		 * \param String const & name
		*/
		virtual void removeCamera(String const& name) override;

		/*!
		 * \remarks 设置主摄像机
		 * \return 
		 * \param Camera * cam
		*/
		virtual void setMainCamera(Camera* cam) override;

		/*!
		 * \remarks 获取主摄像机
		 * \return 
		*/
		virtual Camera* getMainCamera() const override;

		/*!
		 * \remarks 获取参数所指定的摄像机
		 * \return 
		 * \param String const & name
		*/
		virtual Camera* getCamera(String const& name) const override;

		/*!
		 * \remarks 设置视口变换
		 * \return 
		 * \param Viewport * vp
		*/
		virtual void setViewPort(Viewport* vp) override;

		/*!
		 * \remarks 创建顶点shader
		 * \return 
		 * \param String const & fullName
		*/
		virtual Shader* createVertexShader(String const& fullName) override;

		/*!
		 * \remarks 创建片段shader
		 * \return 
		 * \param String const & fullName
		*/
		virtual Shader* createPixelShader(String const& fullName) override;

		/*!
		 * \remarks 使用shader
		 * \return 
		 * \param String const & shaderName
		*/
		virtual void useShader(String const& shaderName) override;

		/*!
		 * \remarks 设置默认的渲染状态
		 * \return 
		*/
		virtual void setDefaultRenderStates() override;

		/*!
		 * \remarks 设置cull mode
		 * \return 
		 * \param CullMode cm
		*/
		virtual void setRenderStateCullMode(CullMode cm = CullMode::CM_CW) override;

	public:
		/*!
		 * \remarks 获取DLL instance句柄
		 * \return 
		*/
		HINSTANCE getInstanceHandle() const;

		/*!
		 * \remarks 查询参数所指定的扩展是否支持
		 * \return 
		 * \param String const & extension
		*/
		bool isExtensionSupported(String const& extension);

		/*!
		 * \remarks 获取显示器的数量
		 * \return 
		*/
		int32 getMonitorCount() const;

	public:
		/*!
		 * \remarks 给mG3RenderSystem赋值
		 * \return 
		 * \param G3RenderSystem* rs
		*/
		static void setStaticRenderSystem(G3RenderSystem* rs);

		/*!
		 * \remarks 获取单件实例
		 * \return 
		*/
		static G3RenderSystem* getInstance();

	private:
		/*!
		 * \remarks 创建一个dummy窗口
		 * \return 
		 * \param wchar_t const* dummyText
		*/
		HWND _createDummyWindow(wchar_t const* dummyText);

		/*!
		 * \remarks 销毁一个dummy窗口
		 * \return 
		 * \param HWND dummyHWnd
		 * \param wchar_t const* dummyText
		*/
		void _destroyDummyWindow(HWND dummyHWnd,wchar_t const* dummyText);

		/*!
		 * \remarks 为dummy窗口设置像素格式
		 * \return 
		 * \param HWND hWnd
		*/
		void _setPixelFormatForDummyWindow(HWND hWnd);

		/*!
		 * \remarks 获取当前的DC和RC
		 * \return 
		 * \param HDC& hDC
		 * \param HGLRC& hRC
		*/
		void _getCurrentDCAndContext(HDC& hDC,HGLRC& hRC);

		/*!
		 * \remarks 创建一个context
		 * \return 
		 * \param HWND hWnd
		*/
		HGLRC _createContext(HWND hWnd);

		/*!
		 * \remarks 销毁一个context
		 * \return 
		 * \param HGLRC hRC
		*/
		void _destroyContext(HGLRC hRC);

		/*!
		 * \remarks 设置当前的context
		 * \return 
		 * \param HWND hWnd
		 * \param HGLRC hRC
		*/
		void _setCurrentContext(HWND hWnd,HGLRC hRC);

		/*!
		 * \remarks 设置当前的context
		 * \return 
		 * \param HDC hDC
		 * \param HGLRC hRC
		*/
		void _setCurrentContext(HDC hDC,HGLRC hRC);

		/*!
		 * \remarks 初始化GLEW
		 * \return 
		*/
		void _initGlew();

		/*!
		 * \remarks 填充厂商和OpenGL版本信息
		 * \return 
		*/
		void _populateVendorAndVersion();

		/*!
		 * \remarks 填充所支持的扩展
		 * \return 
		*/
		void _populateSupportedExtensions();

		/*!
		 * \remarks 标记所支持的扩展
		 * \return 
		*/
		void _markExtensions();

		/*!
		 * \remarks 获取像素格式的索引
		 * \return 
		 * \param HWND hWnd
		*/
		void _retrievePixelFormatIndex(HWND hWnd);

		/*!
		 * \remarks 用于枚举显示器函数的回调函数
		 * \return 
		 * \param HMONITOR hMonitor
		 * \param HDC hDC
		 * \param LPRECT pRect
		 * \param LPARAM data
		*/
		static BOOL CALLBACK _enumDisplayMonitorsProc(HMONITOR hMonitor, HDC hDC, LPRECT pRect, LPARAM data);

		/*!
		 * \remarks 枚举显示器
		 * \return 
		*/
		void _enumDisplayMonitors();

	private:
		HINSTANCE mInstance;
		RenderSystemType mType;
		bool mInitialized;
		HGLRC mContext;
		bool mIsSupportPixelFormatARB;
		bool mIsSupportMultiSampleARB;
		bool mIsSupportFrameBufferSRGBEXT;
		String mVendor;
		String mGLVersion;
		uint32 mGLMajorVersion;
		uint32 mGLMinorVersion;
		String mGLSLVersion;
		uint32 mNumExtensions;
		int32 mPixelFormat;
		uint32 mSamples;
		SetWrapper<String> mSupportedExtensions;
		using monitors_type = STL_VECTOR(G3Monitor*);
		monitors_type mMonitors;
		int32 mMonitorCount;
		IWindow* mActiveWindow;

		//String:title
		UnorderedMapWrapper<String, IWindow*> mWindows;

		static G3RenderSystem* mG3RenderSystem;
	};
}//namespace ung

#endif//UG3_HIERARCHICAL_COMPILE

#endif//_UG3_RENDERSYSTEM_H_