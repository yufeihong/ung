/*!
 * \brief
 * D3D9表面(offscreen plain surface )
 * \file UD9Surface.h
 *
 * \author Su Yang
 *
 * \date 2017/04/29
 */
#ifndef _UD9_SURFACE_H_
#define _UD9_SURFACE_H_

#ifndef _UD9_CONFIG_H_
#include "UD9Config.h"
#endif

#ifdef UD9_HIERARCHICAL_COMPILE

#ifndef _UIF_ISURFACE_H_
#include "UIFISurface.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * D3D9表面
	 * \class D9OffscreenPlainSurface
	 *
	 * \author Su Yang
	 *
	 * \date 2017/05/01
	 *
	 * \todo
	 */
	class Ud9Export D9OffscreenPlainSurface : public IOffscreenPlainSurface
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		D9OffscreenPlainSurface() = delete;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param uint32 width 单位：像素个数
		 * \param uint32 height
		 * \param PixelFormat pf
		 * \param ResourcePoolType pt
		*/
		D9OffscreenPlainSurface(uint32 width,uint32 height,PixelFormat pf,ResourcePoolType pt);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~D9OffscreenPlainSurface();

		/*!
		 * \remarks 获取宽度
		 * \return 像素个数
		*/
		virtual uint32 getWidth() const override;

		/*!
		 * \remarks 获取高度
		 * \return 像素个数
		*/
		virtual uint32 getHeight() const override;

		/*!
		 * \remarks 获取像素格式
		 * \return 
		*/
		virtual PixelFormat getFormat() const override;

		/*!
		 * \remarks 获取颜色深度
		 * \return 
		*/
		virtual uint32 getColorDepth() const override;

		/*!
		 * \remarks 锁定一个区域(This method allows us to obtain a pointer to the surface memory(
		 * a pointer to the pixel data array).Then,with some pointer arithmetic,we can read and
		 * write to each pixel in the surface.)
		 * \return 
		 * \param PixelRectangle const& rect 如果是默认构造的话，则表示整个表面
		 * \param LockSurfaceFlag lsFlag
		 * \param uint32& pitch
		*/
		virtual uint32* lock(PixelRectangle const& rect, LockSurfaceFlag lsFlag,uint32& pitch) override;

		/*!
		 * \remarks 解锁
		 * \return 
		*/
		virtual void unlock() override;

	private:
		uint32 mWidth, mHeight;
		PixelFormat mFormat;
		ResourcePoolType mPoolType;
		D3DSURFACE_DESC mDesc;
		IDirect3DSurface9* mSurface;
	};
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE

#endif//_UD9_SURFACE_H_