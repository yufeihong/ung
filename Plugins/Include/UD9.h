/*!
 * \brief
 * D3D9
 * 客户代码包含这个头文件。
 * \file UD9.h
 *
 * \author Su Yang
 *
 * \date 2017/04/28
 */
#ifndef _UD9_H_
#define _UD9_H_

#ifndef _UD9_ENABLE_H_
#include "UD9Enable.h"
#endif

#ifdef UD9_HIERARCHICAL_COMPILE

#ifndef _UD9_ADAPTER_H_
#include "UD9Adapter.h"
#endif

#ifndef _UD9_ADAPTERMANAGER_H_
#include "UD9AdapterManager.h"
#endif

#ifndef _UD9_DEVICE_H_
#include "UD9Device.h"
#endif

#ifndef _UD9_DEVICEMANAGER_H_
#include "UD9DeviceManager.h"
#endif

#ifndef _UD9_DISPLAYMODE_H_
#include "UD9DisplayMode.h"
#endif

#ifndef _UD9_SURFACE_H_
#include "UD9Surface.h"
#endif

#ifndef _UD9_WINDOW_H_
#include "UD9Window.h"
#endif

#ifndef _UD9_RENDERSYSTEM_H_
#include "UD9RenderSystem.h"
#endif

#endif//UD9_HIERARCHICAL_COMPILE

#endif//_UD9_H_