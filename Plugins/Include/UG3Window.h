/*!
 * \brief
 * GL3窗口
 * \file UG3Window.h
 *
 * \author Su Yang
 *
 * \date 2017/07/08
 */
#ifndef _UG3_WINDOW_H_
#define _UG3_WINDOW_H_

#ifndef _UG3_CONFIG_H_
#include "UG3Config.h"
#endif

#ifdef UG3_HIERARCHICAL_COMPILE

#ifndef _UIF_IWINDOW_H_
#include "UIFIWindow.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * GL3窗口
	 * \class G3Window
	 *
	 * \author Su Yang
	 *
	 * \date 2017/07/09
	 *
	 * \todo
	 */
	class Ug3Export G3Window : public IWindow
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		G3Window() = delete;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param String const& title
		 * \param bool isFullScreen
		 * \param uint32 width
		 * \param uint32 height
		 * \param bool primary
		 * \param HINSTANCE inst
		*/
		G3Window(String const& title, bool isFullScreen, uint32 width, uint32 height,bool primary,HINSTANCE inst);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~G3Window();

		/*!
		 * \remarks 获取窗口类名字
		 * \return 
		*/
		virtual wchar_t const* getClassName() const override;

		/*!
		 * \remarks 获取窗口的句柄
		 * \return 
		*/
		virtual HWND getHandle() const override;

		/*!
		 * \remarks 获取标题
		 * \return 
		*/
		virtual String const& getTitle() const override;

		/*!
		 * \remarks 是否为全屏
		 * \return 
		*/
		virtual bool isFullScreen() const override;

		/*!
		 * \remarks 是否垂直同步
		 * \return 
		*/
		virtual bool isVerticalSync() const override;

		/*!
		 * \remarks 获取窗口的宽
		 * \return 
		*/
		virtual uint32 getWidth() const override;

		/*!
		 * \remarks 获取窗口的高
		 * \return 
		*/
		virtual uint32 getHeight() const override;

		/*!
		 * \remarks 设置宽
		 * \return 
		 * \param uint32 newWidth
		*/
		virtual void setWidth(uint32 newWidth) override;

		/*!
		 * \remarks 设置高
		 * \return 
		 * \param uint32 newHeight
		*/
		virtual void setHeight(uint32 newHeight) override;

		/*!
		 * \remarks 获取last宽
		 * \return 
		*/
		virtual uint32 getLastWidth() const override;

		/*!
		 * \remarks 获取last高
		 * \return 
		*/
		virtual uint32 getLastHeight() const override;

		/*!
		 * \remarks 更新last宽高
		 * \return 
		*/
		virtual void updateLastWH() override;

		/*!
		 * \remarks build style
		 * \return 
		*/
		virtual void buildStyle() override;

		/*!
		 * \remarks 获取全屏style
		 * \return 
		*/
		virtual uint32 getFullScreenStyle() const override;

		/*!
		 * \remarks 获取窗口style
		 * \return 
		*/
		virtual uint32 getWindowedStyle() const override;

		/*!
		 * \remarks 获取style
		 * \return 
		*/
		virtual uint32 getStyle() const override;

		/*!
		 * \remarks 获取窗口所归属的显示器
		 * \return 
		*/
		virtual HMONITOR getMonitor() const override;

		/*!
		 * \remarks 是否为primary
		 * \return 
		*/
		virtual bool isPrimary() const override;

		/*!
		 * \remarks 创建窗口
		 * \return 
		*/
		virtual bool create() override;

		/*!
		 * \remarks 销毁窗口
		 * \return 
		*/
		virtual void destroy() override;

		/*!
		 * \remarks 设置窗口的最小化/最大化状态(和全屏没关系)
		 * \return false_value:mined,true_value:maxed,indeterminate_value:normal
		 * \param boost::tribool state
		*/
		virtual void setMinMaxState(boost::tribool state) override;

		/*!
		 * \remarks 获取窗口的最小化/最大化状态(和全屏没关系)
		 * \return false_value:mined,true_value:maxed,indeterminate_value:normal
		*/
		virtual boost::tribool getMinMaxState() const override;

		/*!
		 * \remarks 窗口是否处于normal状态(和全屏没关系)
		 * \return 
		*/
		virtual bool isNormalState() const override;

	public:
		/*!
		 * \remarks 提交
		 * \return 
		*/
		void present();

	private:
		/*!
		 * \remarks 找到窗口对应的显示器句柄(基于默认的适配器)
		 * \return 
		*/
		//HMONITOR _findMonitorFromDefaultAdapter();

		/*!
		 * \remarks 找到窗口对应的显示器句柄(基于已经创建的窗口句柄)
		 * \return 
		*/
		HMONITOR _findMonitorFromWindow();

		/*!
		 * \remarks 获取显示器句柄所对应的显示器的信息
		 * \return 
		 * \param HMONITOR hMonitor
		*/
		MONITORINFO _getMonitorInfo(HMONITOR hMonitor);

		/*!
		 * \remarks 计算窗口的left,top锚点，以及窗口的整体宽高
		 * \return 
		 * \param RECT workAreaRectangle work area rectangle of the display monitor
		 * \param uint32& left
		 * \param uint32& top
		 * \param uint32& desiredWidth
		 * \param uint32& desiredHeight
		*/
		void _calWindowPositionAndSize(RECT workAreaRectangle,uint32& left,uint32& top,uint32& desiredWidth,uint32& desiredHeight);

		/*!
		 * \remarks 注册窗口类
		 * \return 
		*/
		void _registerWindowClass();

		/*!
		 * \remarks 创建窗口
		 * \return 
		 * \param uint32 left
		 * \param uint32 top
		 * \param uint32 desiredWidth 窗口的整体尺寸
		 * \param uint32 desiredHeight
		*/
		void _createWindow(uint32 left,uint32 top,uint32 desiredWidth,uint32 desiredHeight);

		/*!
		 * \remarks 选择深度和模板格式
		 * \return 
		*/
		void _selectDepthStencilFormat();

		/*!
		 * \remarks 选择窗口模式下的像素格式
		 * \return 
		*/
		void _selectWindowedFormat();

		/*!
		 * \remarks 选择全屏模式下的显示模式
		 * \return 
		*/
		void _selectFullScreenDisplayMode();

		/*!
		 * \remarks 改变窗口/全屏标记
		 * \return 
		*/
		void changeWindowedFullScreenFlag();

	private:
		HINSTANCE mInstance;
		wchar_t* mClassName;
		HWND mHWND;
		String mTitle;
		bool mIsFullScreen;
		//客户区域大小(不是窗口的整体大小，是back buffer尺寸)
		uint32 mWidth, mHeight;
		uint32 mLastWidth, mLastHeight;
		bool mIsPrimary;
		uint32 mFullScreenStyle = 0;
		uint32 mWindowedStyle = 0;
		HMONITOR mMonitorToWhichWindowBelongs = 0;
		bool mVerticalSync;
		uint32 mColorDepth = 32;
		//false_value:mined,true_value:maxed,indeterminate_value:normal
		boost::tribool mState;
	};
}//namespace ung

#endif//UG3_HIERARCHICAL_COMPILE

#endif//_UG3_WINDOW_H_