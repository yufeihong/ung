/*!
 * \brief
 * D3D9插件
 * \file UD9Plugin.h
 *
 * \author Su Yang
 *
 * \date 2017/03/27
 */
#ifndef _UD9_PLUGIN_H_
#define _UD9_PLUGIN_H_

#ifndef _UD9_CONFIG_H_
#include "UD9Config.h"
#endif

#ifdef UD9_HIERARCHICAL_COMPILE

namespace ung
{
	class D9RenderSystem;

	/*!
	 * \brief
	 * D3D9插件
	 * \class D9Plugin
	 *
	 * \author Su Yang
	 *
	 * \date 2017/03/27
	 *
	 * \todo
	 */
	class D9Plugin : public Plugin
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		D9Plugin();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~D9Plugin();

		/*!
		 * \remarks 获取插件的名字
		 * \return 
		*/
		virtual const char* getName() const;

		/*!
		 * \remarks 安装插件
		 * \return 
		*/
		virtual void install();

		/*!
		 * \remarks 卸载插件
		 * \return 
		*/
		virtual void uninstall();

	private:
		D9RenderSystem* mRenderSystem;
	};
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE

#endif//_UD9_PLUGIN_H_