/*!
 * \brief
 * 配置文件
 * \file UD9Config.h
 *
 * \author Su Yang
 *
 * \date 2017/04/28
 */
#ifndef _UD9_CONFIG_H_
#define _UD9_CONFIG_H_

#ifndef _UD9_ENABLE_H_
#include "UD9Enable.h"
#endif

#ifdef UD9_HIERARCHICAL_COMPILE

#ifndef _UNG_H_
#include "UNG.h"
#endif

#ifndef _INCLUDE_D9_D3D9_H_
//该头文件必须位于tinyxml2.h之前
#include <d3d9.h>
#define _INCLUDE_D9_D3D9_H_
#endif

#ifndef _INCLUDE_D9_D3DX9_H_
#include <d3dx9.h>
#define _INCLUDE_D9_D3DX9_H_
#endif

#ifdef UD9_EXPORTS
#define Ud9Export BOOST_SYMBOL_EXPORT
#else
#define Ud9Export BOOST_SYMBOL_IMPORT	
#endif

#define UD9_SYMBOL_VISIBLE BOOST_SYMBOL_VISIBLE

//使COM对象的引用计数器减1
#define UD9_RELEASE(p)									\
{																		\
	if (p)																\
	{																	\
		uint32 refCount = INT_MAX;						\
		refCount = p->Release();							\
		while (refCount > 0)									\
		{																\
			refCount = p->Release();						\
		}																\
		p = nullptr;												\
	}																	\
}

#endif//UD9_HIERARCHICAL_COMPILE

#endif//_UD9_CONFIG_H_