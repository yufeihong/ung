/*!
 * \brief
 * 八叉树场景查询
 * \file UOTSceneQuery.h
 *
 * \author Su Yang
 *
 * \date 2017/04/13
 */
#ifndef _UOT_SCENEQUERY_H_
#define _UOT_SCENEQUERY_H_

#ifndef _UOT_CONFIG_H_
#include "UOTConfig.h"
#endif

#ifdef UOT_HIERARCHICAL_COMPILE

namespace ung
{
	class UotExport UotRaySceneQuery : public SingleSceneQuery
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		UotRaySceneQuery() = default;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param Ray const & ray
		 * \param ISpatialManager* spatialManager
		*/
		UotRaySceneQuery(Ray const& ray, ISpatialManager* spatialManager);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~UotRaySceneQuery();

		/*!
		 * \remarks 执行查询
		 * \return 
		*/
		virtual SingleSceneQueryResults* execute() override;

		/*!
		 * \remarks 设置射线
		 * \return 
		 * \param Ray const & ray
		*/
		void setRay(Ray const& ray);

	protected:
		Ray mRay;
	};//end class UotRaySceneQuery

	//--------------------------------------------------------------

	/*!
	 * \brief
	 * 球体查询
	 * \class UotSphereSceneQuery
	 *
	 * \author Su Yang
	 *
	 * \date 2017/04/03
	 *
	 * \todo
	 */
	class UotExport UotSphereSceneQuery : public RangeSceneQuery
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		UotSphereSceneQuery() = default;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param Sphere const & sphere
		 * \param ISpatialManager* spatialManager
		*/
		UotSphereSceneQuery(Sphere const& sphere,ISpatialManager* spatialManager);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~UotSphereSceneQuery();

		/*!
		 * \remarks 执行查询
		 * \return 
		*/
		virtual RangeSceneQueryResults* execute() override;

		/*!
		 * \remarks 设置Sphere
		 * \return 
		 * \param Sphere const & sphere
		*/
		void setSphere(Sphere const& sphere);

	protected:
		Sphere mSphere;
	};

	//--------------------------------------------------------------

	/*!
	 * \brief
	 * AABB查询
	 * \class UotAABBSceneQuery
	 *
	 * \author Su Yang
	 *
	 * \date 2017/04/03
	 *
	 * \todo
	 */
	class UotExport UotAABBSceneQuery : public RangeSceneQuery
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		UotAABBSceneQuery() = default;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param AABB const & box
		 * \param ISpatialManager* spatialManager
		*/
		UotAABBSceneQuery(AABB const& box,ISpatialManager* spatialManager);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~UotAABBSceneQuery();

		/*!
		 * \remarks 执行查询
		 * \return 
		*/
		virtual RangeSceneQueryResults* execute() override;

		/*!
		 * \remarks 设置AABB
		 * \return 
		 * \param AABB const & box
		*/
		void setAABB(AABB const& box);

	protected:
		AABB mBox;
	};

	//--------------------------------------------------------------

	/*!
	 * \brief
	 * 摄像机查询
	 * \class UotCameraSceneQuery
	 *
	 * \author Su Yang
	 *
	 * \date 2017/04/03
	 *
	 * \todo
	 */
	class UotExport UotCameraSceneQuery : public RangeSceneQuery
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		UotCameraSceneQuery() = default;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param Camera* camera
		 * \param ISpatialManager* spatialManager
		*/
		UotCameraSceneQuery(Camera* camera,ISpatialManager* spatialManager);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~UotCameraSceneQuery();

		/*!
		 * \remarks 执行查询
		 * \return 
		*/
		virtual RangeSceneQueryResults* execute() override;

		/*!
		 * \remarks 设置摄像机
		 * \return 
		 * \param Camera* camera
		*/
		void setCamera(Camera* camera);

	protected:
		Camera* mCamera;
	};
}//namespace ung

#endif//UOT_HIERARCHICAL_COMPILE

#endif//_UOT_SCENEQUERY_H_