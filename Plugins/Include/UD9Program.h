/*!
 * \brief
 * D3D9 GPU program
 * \file UD9Program.h
 *
 * \author Su Yang
 *
 * \date 2017/06/10
 */
#ifndef _UD9_PROGRAM_H_
#define _UD9_PROGRAM_H_

#ifndef _UD9_CONFIG_H_
#include "UD9Config.h"
#endif

#ifdef UD9_HIERARCHICAL_COMPILE

#ifndef _URM_PROGRAM_H_
#include "URMProgram.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * D3D9 Program
	 * \class D9Program
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/10
	 *
	 * \todo
	 */
	class Ud9Export D9Program : public Program
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		D9Program();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~D9Program();

		/*!
		 * \remarks 连接shader到program
		 * \return 
		 * \param Shader * shader
		*/
		virtual void attach(Shader* shader) override;

		/*!
		 * \remarks 从program中分离shader
		 * \return 
		 * \param Shader * shader
		*/
		virtual void detach(Shader* shader) override;

		/*!
		 * \remarks 从program中分离所有的shader
		 * \return 
		*/
		virtual void detachAll() override;

		/*!
		 * \remarks 链接GPU程序
		 * \return 
		*/
		virtual void link() override;

		/*!
		 * \remarks 使用program
		 * \return 
		*/
		virtual void use() override;
	};
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE

#endif//_UD9_PROGRAM_H_