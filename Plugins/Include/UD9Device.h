/*!
 * \brief
 * D3D9渲染设备(抽象)(represent the display adapter)
 * \file UD9Device.h
 *
 * \author Su Yang
 *
 * \date 2017/05/13
 */
#ifndef _UD9_DEVICE_H_
#define _UD9_DEVICE_H_

#ifndef _UD9_CONFIG_H_
#include "UD9Config.h"
#endif

#ifdef UD9_HIERARCHICAL_COMPILE

#ifndef _UFC_STLWRAPPER_H_
#include "UFCSTLWrapper.h"
#endif

namespace ung
{
	class D9Window;

	/*!
	 * \brief
	 * D3D9渲染设备
	 * \class D9Device
	 *
	 * \author Su Yang
	 *
	 * \date 2017/05/13
	 *
	 * \todo
	 */
	class Ud9Export D9Device : public MemoryPool<D9Device>
	{
	public:
		D9Device(D3DDEVTYPE devType,UINT adapterOridinalNumber,HMONITOR hMonitor);

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~D9Device();

		/*!
		 * \remarks 获取该抽象设备所对应的适配器的序号
		 * \return 
		*/
		uint32 getAdapterOridinalNumber() const;

		/*!
		 * \remarks 获取抽象设备的类型
		 * \return 
		*/
		D3DDEVTYPE getType() const;

		/*!
		 * \remarks 获取D3D9抽象设备
		 * \return 
		*/
		IDirect3DDevice9* getDevice() const;

		/*!
		 * \remarks 关联窗口到该设备(一个设备可以关联多个窗口)
		 * \return 
		 * \param D9Window* window
		*/
		void attachRenderWindow(D9Window* window);

		/*!
		 * \remarks 移除窗口
		 * \return 
		 * \param D9Window* window
		*/
		void detachRenderWindow(D9Window* window);

		/*!
		 * \remarks 获取主窗口
		 * \return 
		*/
		D9Window* getPrimaryWindow() const;

		/*!
		 * \remarks 设备是否丢失(在每个消息循环周期中调用)
		 * \return true:丢失
		*/
		bool isLost();

		/*!
		 * \remarks 释放D3DPOOL_DEFAULT pool资源
		 * \return 
		*/
		void onLostDevice();

		/*!
		 * \remarks 重新初始化D3DPOOL_DEFAULT pool资源，恢复设备状态
		 * \return 
		*/
		void onResetDevice();

		/*!
		 * \remarks reset device
		 * \return 
		*/
		HRESULT reset();

		/*!
		 * \remarks 创建IDirect3DDevice9
		 * \return 
		*/
		void create();

		/*!
		 * \remarks 该设备是否用于全屏
		 * \return 
		*/
		bool isUsedInFullScreen() const;

		/*!
		 * \remarks 是否支持变换和光照的硬件计算
		 * \return 
		*/
		bool isSupportHardwardVertexProcess() const;

		/*!
		 * \remarks Can be a pure device
		 * \return 
		*/
		bool isPure() const;

		/*!
		 * \remarks build D3DPRESENT_PARAMETERS
		 * \return 
		 * \param D9Window* window
		*/
		void buildPresentParameters(D9Window* window);

		/*!
		 * \remarks 设置shared focus window handle
		 * \return 
		 * \param HWND sfw
		*/
		static void setSharedFocusWindow(HWND sfw);

		/*!
		 * \remarks 获取shared focus window handle
		 * \return 
		*/
		static HWND getSharedFocusWindow();

	private:
		D3DPRESENT_PARAMETERS mDescOfDeviceFeatures;
		D3DDEVTYPE mDeviceType;
		/*
		Software controller of the physical graphics device hardware.
		Through this interface we can interact with the hardware and instruct it to do things (such 
		as clearing a surface and drawing 3D geometry).

		Our C++ object that represents the physical hardware device we use for displaying 3D 
		graphics.
		*/
		IDirect3DDevice9* mDevice;
		//该device所属于的适配器的序数
		UINT mAdapterOridinalNumberWhichDeviceBelongTo;
		//该device所属于的显示器
		HMONITOR mMonitorWhichDeviceBelongTo;
		SetWrapper<D9Window*> mWindows;
		//True if device entered lost state.
		bool mIsLost;
		static HWND mSharedFocusWindow;
		//The focus window this device attached to
		HWND mFocusWindow;
		D3DCAPS9 mCaps;
		D3DDEVICE_CREATION_PARAMETERS mCreationParameters;
	};
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE

#endif//_UD9_DEVICE_H_