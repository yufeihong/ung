/*!
 * \brief
 * GL3����
 * \file UG3Utilities.h
 *
 * \author Su Yang
 *
 * \date 2017/07/08
 */
#ifndef _UG3_UTILITIES_H_
#define _UG3_UTILITIES_H_

#ifndef _UG3_CONFIG_H_
#include "UG3Config.h"
#endif

#ifdef UG3_HIERARCHICAL_COMPILE

namespace ung
{

}//namespace ung

#endif//UG3_HIERARCHICAL_COMPILE

#endif//_UG3_UTILITIES_H_