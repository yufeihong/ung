/*!
 * \brief
 * D3D9渲染系统
 * \file UD9RenderSystem.h
 *
 * \author Su Yang
 *
 * \date 2017/04/28
 */
#ifndef _UD9_RENDERSYSTEM_H_
#define _UD9_RENDERSYSTEM_H_

#ifndef _UD9_CONFIG_H_
#include "UD9Config.h"
#endif

#ifdef UD9_HIERARCHICAL_COMPILE

#ifndef _UIF_IRENDERSYSTEM_H_
#include "UIFIRenderSystem.h"
#endif

#ifndef _UFC_STLWRAPPER_H_
#include "UFCSTLWrapper.h"
#endif

#ifndef _UFC_COLORVALUE_H_
#include "UFCColorValue.h"
#endif

namespace ung
{
	class D9AdapterManager;
	class D9Window;
	class D9DeviceManager;
	class D9Device;
	class D9VertexBufferManager;
	
	/*!
	 * \brief
	 * D3D9渲染系统
	 * \class D9RenderSystem
	 *
	 * \author Su Yang
	 *
	 * \date 2017/05/10
	 *
	 * \todo
	 */
	class Ud9Export D9RenderSystem : public IRenderSystem
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		D9RenderSystem() = delete;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param HINSTANCE inst
		*/
		D9RenderSystem(HINSTANCE inst);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~D9RenderSystem();

		/*!
		 * \remarks 初始化
		 * \return 
		*/
		virtual void initialize() override;

		/*!
		 * \remarks 是否已经初始化了
		 * \return 
		*/
		virtual bool isInitialized() const override;

		/*!
		 * \remarks 获取渲染系统类型
		 * \return 
		*/
		virtual RenderSystemType getType() const override;

		/*!
		 * \remarks 渲染前的准备工作
		 * \return 
		*/
		virtual void prepare() override;

		/*!
		 * \remarks 设置窗口/全屏
		 * \return 
		 * \param bool windowed true:窗口
		*/
		virtual void setWindowed(bool windowed) override;

		/*!
		 * \remarks 获取是否为窗口
		 * \return true:窗口
		*/
		virtual bool getWindowed() const override;

		/*!
		 * \remarks 设置back buffer的宽高
		 * \return 
		 * \param uint32 width
		 * \param uint32 height
		*/
		virtual void setBackBufferWH(uint32 width, uint32 height) override;

		/*!
		 * \remarks 获取back buffer的宽高
		 * \return 
		*/
		virtual std::pair<uint32, uint32> getBackBufferWH() const override;

		/*!
		 * \remarks Get the native color type for this rendersystem.
         * \return 
        */
        virtual VertexElementType getNativeColorType() const override;

		/*!
		 * \remarks 设置back buffer的像素格式
		 * \return 
		 * \param PixelFormat pf
		*/
		virtual void setBackBufferPixelFormat(PixelFormat pf) override;

		/*!
		 * \remarks 获取back buffer的像素格式
		 * \return 
		*/
		virtual PixelFormat getBackBufferPixelFormat() const override;

		/*!
		 * \remarks 创建一个offscreen plain表面
		 * \return 
		 * \param uint32 width 单位：像素个数
		 * \param uint32 height
		 * \param PixelFormat format
		 * \param ResourcePoolType pt
		*/
		virtual IOffscreenPlainSurface* createOffscreenPlainSurface(uint32 width,uint32 height,PixelFormat format, ResourcePoolType pt = ResourcePoolType::RPT_DEFAULT) override;
		
		/*!
		 * \remarks 创建窗口
		 * \return 
		 * \param String const & title
		 * \param bool isFullScreen
		 * \param uint32 width back buffer width
		 * \param uint32 height back buffer height
		 * \param bool primary
		*/
		virtual bool createWindow(String const& title, bool isFullScreen, uint32 width, uint32 height,bool primary = false) override;

		/*!
		 * \remarks 关闭渲染系统(不要在卸载/销毁插件时调用，而应在插件正常状态时调用)
		 * \return 
		*/
		virtual void shutdown() override;

		/*!
		 * \remarks 设备是否丢失
		 * \return 
		*/
		virtual bool isDeviceLost() override;

		/*!
		 * \remarks 处理窗口大小变化的消息
		 * \return 
		*/
		virtual void onSizeChanged() override;

		/*!
		 * \remarks 获取active window
		 * \return 
		*/
		virtual IWindow* getActiveWindow() override;

		/*!
		 * \remarks 在窗口模式和全屏模式之间切换
		 * \return 
		*/
		virtual void switchBetweenWindowedAndFullScreen() override;

		/*!
		 * \remarks 更新
		 * \return 
		 * \param real_type dt 单位:毫秒
		*/
		virtual void update(real_type dt) override;

		/*!
		 * \remarks 渲染一帧
		 * \return 
		 * \param real_type deltaTime 单位：毫秒
		*/
		virtual void renderOneFrame(real_type deltaTime) override;

		/*!
		 * \remarks 创建摄像机
		 * \return 
		 * \param String const & name
		 * \param Vector3 const & pos
		 * \param Vector3 const & lookat
		 * \param real_type aspect
		*/
		virtual Camera* createCamera(String const& name,Vector3 const& pos, Vector3 const& lookat, real_type aspect) override;

		/*!
		 * \remarks 删除摄像机
		 * \return 
		 * \param String const & name
		*/
		virtual void removeCamera(String const& name) override;

		/*!
		 * \remarks 设置主摄像机
		 * \return 
		 * \param Camera * cam
		*/
		virtual void setMainCamera(Camera* cam) override;

		/*!
		 * \remarks 获取主摄像机
		 * \return 
		*/
		virtual Camera* getMainCamera() const override;

		/*!
		 * \remarks 获取参数所指定的摄像机
		 * \return 
		 * \param String const & name
		*/
		virtual Camera* getCamera(String const& name) const override;

		/*!
		 * \remarks 设置视口变换
		 * \return 
		 * \param Viewport * vp
		*/
		virtual void setViewPort(Viewport* vp) override;

		/*!
		 * \remarks 创建顶点shader
		 * \return 
		 * \param String const & fullName
		*/
		virtual Shader* createVertexShader(String const& fullName) override;

		/*!
		 * \remarks 创建片段shader
		 * \return 
		 * \param String const & fullName
		*/
		virtual Shader* createPixelShader(String const& fullName) override;

		/*!
		 * \remarks 使用shader
		 * \return 
		 * \param String const & shaderName
		*/
		virtual void useShader(String const& shaderName) override;

		/*!
		 * \remarks 设置默认的渲染状态
		 * \return 
		*/
		virtual void setDefaultRenderStates() override;

		/*!
		 * \remarks 设置cull mode
		 * \return 
		 * \param CullMode cm
		*/
		virtual void setRenderStateCullMode(CullMode cm = CullMode::CM_CW) override;

	public:
		/*!
		 * \remarks 设置active device
		 * \return 
		 * \param IDirect3DDevice9 * dev
		*/
		void setActiveDevice(IDirect3DDevice9* dev);

		/*!
		 * \remarks 获取active device
		 * \return 
		*/
		IDirect3DDevice9* getActiveDevice() const;

		/*!
		 * \remarks 清空一个或多个表面的内容。(清空viewport的颜色缓冲区，深度缓冲区，模板缓冲区)
		 * \return 
		 * \param uint32 buffers
		 * \param const real_color& color
		 * \param real_type depth
		 * \param uint32 stencil
		*/
		void clearFrameBuffer(uint32 buffers,const real_color& color = UNG_COLOR_BLACK,
			real_type depth = 1.0,uint32 stencil = 0);

		/*!
		 * \remarks 渲染开始
		 * \return 
		*/
		void startFrame();

		/////
		typedef void (*f)();
		void setRender(f ff)
		{
			mr = ff;
		}
		void render()
		{
			mr();
		}

		/*!
		 * \remarks 渲染结束
		 * \return 
		*/
		void endFrame();

		/*!
		 * \remarks 提交(presents the front buffer to the screen)
		 * \return 
		*/
		void present();

	public:
		/*!
		 * \remarks 获取DLL instance句柄
		 * \return 
		*/
		HINSTANCE getInstanceHandle() const;

		/*!
		 * \remarks 注销参数所指定的窗口
		 * \return 
		 * \param IWindow * window
		*/
		void unregisterWindow(IWindow* window);

		/*!
		 * \remarks 注销所有的窗口
		 * \return 
		*/
		void unregisterWindows();

	public:
		/*!
		 * \remarks 给mD9RenderSystem赋值
		 * \return 
		 * \param D9RenderSystem * rs
		*/
		static void setStaticRenderSystem(D9RenderSystem* rs);

		/*!
		 * \remarks 获取单件实例
		 * \return 
		*/
		static D9RenderSystem* getInstance();

		/*!
		 * \remarks 获取IDirect3D9接口对象
		 * \return 
		*/
		static IDirect3D9* getIDirect3D9();

		/*!
		 * \remarks 获取active D9Device
		 * \return 
		*/
		//static D9Device* getActiveDevice();

		/*!
		 * \remarks 获取适配器管理器
		 * \return 
		*/
		static D9AdapterManager* getD9AdapterManager();

		/*!
		 * \remarks 获取抽象设备管理器
		 * \return 
		*/
		static D9DeviceManager* getD9DeviceManager();

		/*!
		 * \remarks 获取顶点缓冲区管理器
		 * \return 
		*/
		static D9VertexBufferManager* getD9VertexBufferManager();

	//private:
		/*!
		 * \remarks 检查重复的窗口标题
		 * \return 
		 * \param String const & title
		*/
		void _checkWindowTitle(String const& title) const;

	private:
		HINSTANCE mInstance;
		RenderSystemType mType;
		bool mInitialized;
		IDirect3D9* mD3D9;
		IWindow* mActiveWindow;
		IDirect3DDevice9* mActiveDevice;

		Camera* mMainCamera;

		//String:name
		UnorderedMapWrapper<String, Camera*> mCameras;
		
		//String:title
		UnorderedMapWrapper<String, IWindow*> mWindows;
		//String:文件名(不包含路径)
		UnorderedMapWrapper<String, Shader*> mShaders;

		static D9RenderSystem* mD9RenderSystem;
		static D9AdapterManager* mD9AdapterManager;
		static D9DeviceManager* mD9DeviceManager;
		static D9VertexBufferManager* mD9VertexBufferManager;
		/////
		f mr;
	};
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE

#endif//_UD9_RENDERSYSTEM_H_