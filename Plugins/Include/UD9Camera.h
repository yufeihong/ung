/*!
 * \brief
 * D3D9 摄像机
 * \file UD9Camera.h
 *
 * \author Su Yang
 *
 * \date 2017/06/18
 */
#ifndef _UD9_CAMERA_H_
#define _UD9_CAMERA_H_

#ifndef _UD9_CONFIG_H_
#include "UD9Config.h"
#endif

#ifdef UD9_HIERARCHICAL_COMPILE

#ifndef _USM_CAMERA_H_
#include "USMCamera.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * D3D9 摄像机
	 * \class D9Camera
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/18
	 *
	 * \todo
	 */
	class D9Camera : public Camera
	{
	public:
		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param String const & name
		 * \param Vector3 const & pos
		 * \param Vector3 const & lookat
		 * \param real_type aspect
		 * \param Radian fovY
		 * \param real_type nDis
		 * \param real_type fDis
		*/
		D9Camera(String const& name,Vector3 const& pos, Vector3 const& lookat, real_type aspect, Radian fovY = Radian(Consts<real_type>::THIRDPI), real_type nDis = 1.0, real_type fDis = 1000.0);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~D9Camera();

		/*!
		 * \remarks 获取投影矩阵
		 * \return 
		*/
		virtual const Matrix4& getProjectionMatrix() const override;

		/*!
		 * \remarks 获取裁剪空间的AABB
		 * \return 
		*/
		virtual AABB getClipSpaceAABB() const override;

		/*!
		 * \remarks 创建视口
		 * \return 
		 * \param uint32 left
		 * \param uint32 top
		 * \param uint32 width
		 * \param uint32 height
		 * \param int32 zOrder
		*/
		virtual Viewport* createViewport(uint32 left,uint32 top,uint32 width,uint32 height,int32 zOrder = 0) override;
	};
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE

#endif//_UD9_CAMERA_H_