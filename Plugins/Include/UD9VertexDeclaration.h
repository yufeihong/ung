/*!
 * \brief
 * D3D9顶点声明
 * \file UD9VertexDeclaration.h
 *
 * \author Su Yang
 *
 * \date 2017/06/02
 */
#ifndef _UD9_VERTEXDECLARATION_H_
#define _UD9_VERTEXDECLARATION_H_

#ifndef _UD9_CONFIG_H_
#include "UD9Config.h"
#endif

#ifdef UD9_HIERARCHICAL_COMPILE

#ifndef _URM_VERTEXDECLARATION_H_
#include "URMVertexDeclaration.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * D3D9顶点声明
	 * \class D9VertexDeclaration
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/02
	 *
	 * \todo
	 */
	class D9VertexDeclaration : public VertexDeclaration
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		D9VertexDeclaration();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~D9VertexDeclaration();

		/*!
		 * \remarks 在调用完所有addElement()函数后，必须调用该函数
		 * \return 
		*/
		virtual void addElementFinish() override;

		/*!
		 * \remarks 插入一个VertexElement
		 * \return A reference to the VertexElement added
		 * \param uint32 atPosition
		 * \param uint32 source
		 * \param uint32 offset
		 * \param VertexElementSemantic semantic
		 * \param VertexElementType theType
		 * \param uint32 index
		*/
		//virtual const VertexElement& insertElement(uint32 atPosition,uint32 source,uint32 offset,VertexElementSemantic semantic,VertexElementType theType,uint32 index = 0) override;

		/*!
		 * \remarks 移除一个VertexElement
		 * \return 
		 * \param uint32 elem_index
		*/
		//virtual void removeElement(uint32 elem_index) override;

		/*!
		 * \remarks 
		 * \return 
		 * \param VertexElementSemantic semantic
		 * \param uint32 index 用于标记纹理坐标等重复性elements
		*/
		//virtual void removeElement(VertexElementSemantic semantic,uint32 index = 0) override;

		/*!
		 * \remarks 移除所有的VertexElement
		 * \return 
		*/
		//virtual void removeAllElements() override;

		/*!
		 * \remarks 就地修改一个element
		 * \return 
		 * \param uint32 elem_index
		 * \param uint32 source
		 * \param uint32 offset
		 * \param VertexElementSemantic semantic
		 * \param VertexElementType theType
		 * \param uint32 index
		*/
		//virtual void modifyElement(uint32 elem_index,uint32 source,uint32 offset,VertexElementSemantic semantic,VertexElementType theType,uint32 index = 0) override;

		/*!
		 * \remarks 设置顶点声明
		 * \return 
		*/
		virtual void set() override;

	private:
		IDirect3DVertexDeclaration9* mD3D9Declaration;
	};
}//namespace ung

#endif//UD9_HIERARCHICAL_COMPILE

#endif//_UD9_VERTEXDECLARATION_H_