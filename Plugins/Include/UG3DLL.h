/*!
 * \brief
 * GL3 dll
 * \file UG3DLL.h
 *
 * \author Su Yang
 *
 * \date 2017/07/08
 */
#ifndef _UG3_DLL_H_
#define _UG3_DLL_H_

#ifndef _UG3_CONFIG_H_
#include "UG3Config.h"
#endif

#ifdef UG3_HIERARCHICAL_COMPILE

namespace ung
{
	UNG_C_LINK_BEGIN;

	/*!
	 * \remarks 创建插件
	 * \return 
	*/
	Ug3Export void dllCreatePlugin() noexcept;

	/*!
	 * \remarks 销毁插件
	 * \return 
	*/
	Ug3Export void dllDestroyPlugin();

	UNG_C_LINK_END;
}//namespace ung

#endif//UG3_HIERARCHICAL_COMPILE

#endif//_UG3_DLL_H_