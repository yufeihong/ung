/*!
 * \brief
 * 配置文件
 * \file UG3Config.h
 *
 * \author Su Yang
 *
 * \date 2017/07/08
 */
#ifndef _UG3_CONFIG_H_
#define _UG3_CONFIG_H_

#ifndef _UG3_ENABLE_H_
#include "UG3Enable.h"
#endif

#ifdef UG3_HIERARCHICAL_COMPILE

/*
OpenGL Loading Library:
An OpenGL Loading Library is a library that loads pointers to OpenGL functions at runtime, core 
as well as extensions. This is required to access functions from OpenGL versions above 1.1 on 
most platforms. Extension loading libraries also abstracts away the difference between the 
loading mechanisms on different platforms.
Most extension loading libraries override the need to include gl.h at all. Instead, they provide 
their own header that must be used. Most extension loading libraries use code generation to 
construct the code that loads the function pointers and the included headers.

GLEW(OpenGL Extension Wrangler):
The OpenGL Extension Wrangler library provides access to all GL entrypoints. It supports 
Windows, MacOS X, Linux, and FreeBSD.
As with most other loaders, you should not include gl.h, glext.h, or any other gl related header 
file before glew.h, otherwise you'll get an error message that you have included gl.h before 
glew.h. In fact, you shouldn't be including gl.h at all.glew.h replaces it.
GLEW also provides wglew.h which provides Windows specific GL functions (wgl functions). If 
you include wglext.h before wglew.h, GLEW will complain. GLEW also provides glxew.h for X 
windows systems. If you include glxext.h before glxew.h, GLEW will complain.
Initialization of GLEW 1.13.0 and earlier:
GLEW up to version 1.13.0 has a problem with core contexts. It calls 
glGetString(GL_EXTENSIONS),which causes GL_INVALID_ENUM on GL 3.2+ core context as 
soon as glewInit() is called.It also doesn't fetch the function pointers.
(GLEW,1.13.0版本，及其之前的版本与OpenGL 3.2+共同使用时，有问题)
GLEW version 2.0.0+ uses glGetStringi instead.
The only fix for earlier versions is to use glewExperimental:
//If using GLEW version 1.13 or earlier
glewExperimental=TRUE;
GLenum err=glewInit();
if(err!=GLEW_OK)
{
	//Problem: glewInit failed,something is seriously wrong.
	cout << "glewInit failed: " << glewGetErrorString(err) << endl;
	exit(1);
}
glewExperimental is a variable that is already defined by GLEW. You must set it to GL_TRUE 
before calling glewInit().
You might still get GL_INVALID_ENUM (depending on the version of GLEW you use), but at 
least GLEW ignores glGetString(GL_EXTENSIONS) and gets all function pointers.
If you are creating a GL context the old way or if you are creating a backward compatible 
context for GL 3.2+, then you don't need glewExperimental.

If you plan on using extensions in your program, you can use the OpenGL Extension Wrangler 
Library (GLEW).

To use the static library you must define the GLEW_STATIC macro in your code before you 
include the glew.h header file.
#define GLEW_STATIC
#include <gl/glew.h>
This is the only header file that you need to include to get access to all of the extensions that 
are supported by GLEW.
*/

//glew
#ifndef _INCLUDE_GL_GLEW_H_
#include "GL/glew.h"
#define _INCLUDE_GL_GLEW_H_
#endif

//windows gl
#ifndef _INCLUDE_GL_WGLEW_H_
#include "GL/wglew.h"
#define _INCLUDE_GL_WGLEW_H_
#endif

/*
glewExperimental = GL_TRUE;
当环境创建失败的时候(比如：版本号错误)，这里就会失败。
glew初始化失败的原因一般是在建立OpenGL context之前去初始化了。
*/
#define GLEW_INIT					\
GLenum err = glewInit();			\
if (GLEW_OK != err)					\
{												\
	exit(EXIT_FAILURE);				\
}

#ifndef _UNG_H_
#include "UNG.h"
#endif

#ifndef _UFC_STLWRAPPER_H_
#include "UFCSTLWrapper.h"
#endif

#ifdef UG3_EXPORTS
#define Ug3Export BOOST_SYMBOL_EXPORT
#else
#define Ug3Export BOOST_SYMBOL_IMPORT	
#endif

#define UG3_SYMBOL_VISIBLE BOOST_SYMBOL_VISIBLE

#endif//UG3_HIERARCHICAL_COMPILE

#endif//_UG3_CONFIG_H_