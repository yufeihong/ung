/*!
 * \brief
 * ���Ͷ���
 * \file UBSForwardTypeDefine.h
 *
 * \author Su Yang
 *
 * \date 2017/04/16
 */
#ifndef _UBS_FORWARDTYPEDEFINE_H_
#define _UBS_FORWARDTYPEDEFINE_H_

#ifndef _UBS_CONFIG_H_
#include "UBSConfig.h"
#endif

#ifdef UBS_HIERARCHICAL_COMPILE

namespace ung
{
	template<typename T>
	struct VectorIterators
	{
		using type = std::pair<typename std::vector<T>::iterator, typename std::vector<T>::iterator>;
	};

	template<typename T>
	struct ListIterators
	{
		using type = std::pair<typename std::list<T>::iterator, typename std::list<T>::iterator>;
	};

	template<typename T>
	struct DequeIterators
	{
		using type = std::pair<typename std::deque<T>::iterator, typename std::deque<T>::iterator>;
	};

	template<typename T>
	struct ForwardListIterators
	{
		using type = std::pair<typename std::forward_list<T>::iterator, typename std::forward_list<T>::iterator>;
	};

	template<typename T>
	struct StackIterators
	{
		using type = std::pair<typename std::stack<T>::iterator, typename std::stack<T>::iterator>;
	};

	template<typename T>
	struct SetIterators
	{
		using type = std::pair<typename std::set<T>::iterator, typename std::set<T>::iterator>;
	};

	template<typename T>
	struct MultiSetIterators
	{
		using type = std::pair<typename std::multiset<T>::iterator, typename std::multiset<T>::iterator>;
	};

	template<typename K,typename T>
	struct MapIterators
	{
		using type = std::pair<typename std::map<K,T>::iterator, typename std::map<K,T>::iterator>;
	};

	template<typename K,typename T>
	struct MultiMapIterators
	{
		using type = std::pair<typename std::multimap<K,T>::iterator, typename std::multimap<K,T>::iterator>;
	};

	template<typename T>
	struct UnorderedSetIterators
	{
		using type = std::pair<typename std::unordered_set<T>::iterator, typename std::unordered_set<T>::iterator>;
	};

	template<typename K,typename T>
	struct UnorderedMapIterators
	{
		using type = std::pair<typename std::unordered_map<K,T>::iterator, typename std::unordered_map<K,T>::iterator>;
	};

	template<typename T>
	struct UnorderedMultiSetIterators
	{
		using type = std::pair<typename std::unordered_multiset<T>::iterator, typename std::unordered_multiset<T>::iterator>;
	};

	template<typename K,typename T>
	struct UnorderedMultiMapIterators
	{
		using type = std::pair<typename std::unordered_multimap<K,T>::iterator, typename std::unordered_multimap<K,T>::iterator>;
	};

	template<typename T>
	struct VectorConstIterators
	{
		using type = std::pair<typename std::vector<T>::const_iterator, typename std::vector<T>::const_iterator>;
	};

	template<typename T>
	struct ListConstIterators
	{
		using type = std::pair<typename std::list<T>::const_iterator, typename std::list<T>::const_iterator>;
	};

	template<typename T>
	struct DequeConstIterators
	{
		using type = std::pair<typename std::deque<T>::const_iterator, typename std::deque<T>::const_iterator>;
	};

	template<typename T>
	struct ForwardListConstIterators
	{
		using type = std::pair<typename std::forward_list<T>::const_iterator, typename std::forward_list<T>::const_iterator>;
	};

	template<typename T>
	struct StackConstIterators
	{
		using type = std::pair<typename std::stack<T>::const_iterator, typename std::stack<T>::const_iterator>;
	};

	template<typename T>
	struct SetConstIterators
	{
		using type = std::pair<typename std::set<T>::const_iterator, typename std::set<T>::const_iterator>;
	};

	template<typename T>
	struct MultiSetConstIterators
	{
		using type = std::pair<typename std::multiset<T>::const_iterator, typename std::multiset<T>::const_iterator>;
	};

	template<typename K,typename T>
	struct MapConstIterators
	{
		using type = std::pair<typename std::map<K,T>::const_iterator, typename std::map<K,T>::const_iterator>;
	};

	template<typename K,typename T>
	struct MultiMapConstIterators
	{
		using type = std::pair<typename std::multimap<K,T>::const_iterator, typename std::multimap<K,T>::const_iterator>;
	};

	template<typename T>
	struct UnorderedSetConstIterators
	{
		using type = std::pair<typename std::unordered_set<T>::const_iterator, typename std::unordered_set<T>::const_iterator>;
	};

	template<typename K,typename T>
	struct UnorderedMapConstIterators
	{
		using type = std::pair<typename std::unordered_map<K,T>::const_iterator, typename std::unordered_map<K,T>::const_iterator>;
	};

	template<typename T>
	struct UnorderedMultiSetConstIterators
	{
		using type = std::pair<typename std::unordered_multiset<T>::const_iterator, typename std::unordered_multiset<T>::const_iterator>;
	};

	template<typename K,typename T>
	struct UnorderedMultiMapConstIterators
	{
		using type = std::pair<typename std::unordered_multimap<K,T>::const_iterator, typename std::unordered_multimap<K,T>::const_iterator>;
	};
}//namespace ung

#endif//UBS_HIERARCHICAL_COMPILE

#endif//_UBS_FORWARDTYPEDEFINE_H_