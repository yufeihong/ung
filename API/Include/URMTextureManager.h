/*!
 * \brief
 * 纹理管理器，支持多线程安全。
 * \file URMTextureManager.h
 *
 * \author Su Yang
 *
 * \date 2016/11/13
 */
#ifndef _URM_TEXTUREMANAGER_H_
#define _URM_TEXTUREMANAGER_H_

#ifndef _URM_CONFIG_H_
#include "URMConfig.h"
#endif

#ifdef URM_HIERARCHICAL_COMPILE

namespace ung
{
	class Image;

	/*!
	 * \brief
	 * 纹理管理器，支持多线程安全。
	 * \class TextureManager
	 *
	 * \author Su Yang
	 *
	 * \date 2016/11/13
	 *
	 * \todo
	 */
	class TextureManager : public Singleton<TextureManager>
	{
	public:
		using pointer = std::shared_ptr<Image>;
		using const_pointer = pointer const;

		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		TextureManager();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~TextureManager();

		/*!
		 * \remarks 创建纹理
		 * \return 
		 * \param String const& textureName 仅文件名，不包含路径
		*/
		pointer createTexture(String const& textureName);

		/*!
		 * \remarks 移除容器中的指针，但是并不保证释放纹理所占用的内存
		 * \return 
		 * \param String const & name
		*/
		void eraseTexture(String const& textureName);

		/*!
		 * \remarks 从容器中删除给定的纹理
		 * \return 
		 * \param pointer texturePtr
		*/
		void eraseTexture(pointer texturePtr);

		/*!
		 * \remarks 部署纹理的后台载入
		 * \return 
		 * \param pointer texturePtr
		*/
		void buildTexture(pointer texturePtr);

		/*!
		 * \remarks 通过纹理名字来查询纹理
		 * \return 
		 * \param String const & textureName
		*/
		const_pointer getTexture(String const& textureName) const;

		/*!
		 * \remarks 通过纹理名字来查询纹理
		 * \return 
		 * \param String const & textureName
		*/
		pointer getTexture(String const& textureName);

	private:
		/*!
		 * \remarks 把纹理给存储到容器中
		 * \return 
		 * \param String const & textureName
		 * \param pointer texturePtr
		*/
		void addTexture(String const& textureName,pointer texturePtr);

		/*!
		 * \remarks 从容器中删除掉给定的纹理
		 * \return 
		 * \param String const & textureName
		*/
		void removeTexture(String const& textureName);

	private:
		STL_UNORDERED_MAP(String, pointer) mTextures;
	};
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

#endif//_URM_TEXTUREMANAGER_H_