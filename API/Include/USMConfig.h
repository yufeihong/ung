/*!
 * \brief
 * 内部代码包含这个头文件。
 * \file USMConfig.h
 *
 * \author Su Yang
 *
 * \date 2016/11/17
 */
#ifndef _USM_CONFIG_H_
#define _USM_CONFIG_H_

#ifndef _USM_ENABLE_H_
#include "USMEnable.h"
#endif

#ifdef USM_HIERARCHICAL_COMPILE

//其它库的头文件
#ifndef _UBS_H_
#include "UBS.h"
#endif

#ifndef _UTM_H_
#include "UTM.h"
#endif

#ifndef _UMM_H_
#include "UMM.h"
#endif

#ifndef _UFC_H_
#include "UFC.h"
#endif

#ifndef _UML_H_
#include "UML.h"
#endif

#ifndef _UPE_H_
#include "UPE.h"
#endif

#ifndef _URM_H_
#include "URM.h"
#endif

#ifndef _USM_FORWARDTYPEDEFINE_H_
#include "USMForwardTypeDefine.h"
#endif

#endif//USM_HIERARCHICAL_COMPILE

#endif//_USM_CONFIG_H_