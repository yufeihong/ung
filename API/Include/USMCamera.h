/*!
 * \brief
 * 摄像机
 * \file USMCamera.h
 *
 * \author Su Yang
 *
 * \date 2017/04/26
 */
#ifndef _USM_CAMERA_H_
#define _USM_CAMERA_H_

#ifndef _USM_CONFIG_H_
#include "USMConfig.h"
#endif

#ifdef USM_HIERARCHICAL_COMPILE

#ifndef _USM_FRUSTUM_H_
#include "USMFrustum.h"
#endif

namespace ung
{
	class IRenderTarget;
	class Viewport;

	/*!
	 * \brief
	 * 摄像机
	 * \class Camera
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/18
	 *
	 * \todo
	 */
	class UngExport Camera : public MemoryPool<Camera>
	{
	public:
		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param String const & name
		 * \param Vector3 const & pos
		 * \param Vector3 const & lookat
		 * \param real_type aspect
		 * \param Radian fovY
		 * \param real_type nDis
		 * \param real_type fDis
		*/
		Camera(String const& name,Vector3 const& pos, Vector3 const& lookat, real_type aspect, Radian fovY = Radian(Consts<real_type>::THIRDPI), real_type nDis = 1.0, real_type fDis = 1000.0);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~Camera();

		/*!
		 * \remarks 获取摄像机的名字
		 * \return 
		*/
		String const& getName() const;

		/*!
		 * \remarks 设置摄像机的位置
		 * \return 
		 * \param Vector3 const & pos
		*/
		void setPosition(Vector3 const& pos);

		/*!
		 * \remarks 获取摄像机的位置
		 * \return 
		*/
		Vector3 const& getPosition() const;

		/*!
		 * \remarks 设置摄像机的目标
		 * \return 
		 * \param Vector3 const & lookat
		*/
		void setLookAt(Vector3 const& lookat);

		/*!
		 * \remarks 获取摄像机的目标
		 * \return 
		*/
		Vector3 const& getLookAt() const;

		/*!
		 * \remarks 设置Field Of View(FOV)(Y-dimension)
		 * \return 
		 * \param const Radian & fovy
		*/
		void setFOVy(const Radian& fovy);

		/*!
		 * \remarks 获取FOVy
		 * \return 单位为弧度
		*/
		const Radian& getFOVy() const;

		/*!
		 * \remarks 设置近截面的位置
		 * \return 
		 * \param real_type nearDist 近截面距离观察点的距离
		*/
		void setNearClipDistance(real_type nearDist);

		/*!
		 * \remarks 获取近截面的位置
		 * \return 
		*/
		real_type getNearClipDistance() const;

		/*!
		 * \remarks 设置远截面的位置
		 * \return 
		 * \param real_type farDist 远截面距离观察点的距离
		*/
		void setFarClipDistance(real_type farDist);

		/*!
		 * \remarks 获取远截面的位置
		 * \return 
		*/
		real_type getFarClipDistance() const;

		/*!
		 * \remarks 设置aspect ratio（宽高比率）
		 * \return 
		 * \param real_type ratio
		*/
		void setAspectRatio(real_type ratio);

		/*!
		 * \remarks 获取aspect ratio
		 * \return 
		*/
		real_type getAspectRatio() const;

		/*!
		 * \remarks 获取投影矩阵
		 * \return 
		*/
		virtual const Matrix4& getProjectionMatrix() const PURE;

		/*!
		 * \remarks 获取近平面的宽度
		 * \return 
		*/
		real_type getNearPlaneWidth() const;

		/*!
		 * \remarks 获取近平面的高度
		 * \return 
		*/
		real_type getNearPlaneHeight() const;

		/*!
		 * \remarks 获取远平面的宽度
		 * \return 
		*/
		real_type getFarPlaneWidth() const;

		/*!
		 * \remarks 获取远平面的高度
		 * \return 
		*/
		real_type getFarPlaneHeight() const;

		/*!
		 * \remarks 获取右方向
		 * \return 
		*/
		Vector3 getRightDir() const;

		/*!
		 * \remarks 获取上方向
		 * \return 
		*/
		Vector3 getUpDir() const;

		/*!
		 * \remarks 获取观察方向
		 * \return 
		*/
		Vector3 getLookDir() const;

		/*!
		 * \remarks 获取近平面的中心点
		 * \return 
		*/
		Vector3 getNearPlaneCenter() const;

		/*!
		 * \remarks 获取远平面的中心点
		 * \return 
		*/
		Vector3 getFarPlaneCenter() const;

		/*!
		 * \remarks 获取参数所指定的平截头体角
		 * \return 
		 * \param uint32 index
		*/
		Vector3 getIndexedCorner(uint32 index) const;

		/*!
		 * \remarks 获取view矩阵
		 * \return 
		*/
		Matrix4 const& getViewMatrix() const;

		/*!
		 * \remarks 获取裁剪空间的AABB
		 * \return 
		*/
		virtual AABB getClipSpaceAABB() const PURE;

		/*!
		 * \remarks 创建视口
		 * \return 
		 * \param uint32 left
		 * \param uint32 top
		 * \param uint32 width
		 * \param uint32 height
		 * \param int32 zOrder
		*/
		virtual Viewport* createViewport(uint32 left,uint32 top,uint32 width,uint32 height,int32 zOrder = 0) PURE;

		/*!
		 * \remarks 获取关联的视口
		 * \return 
		*/
		Viewport* getAttachedViewport() const;

	private:
		/*!
		 * \remarks 更新view矩阵
		 * \return 
		*/
		void updateViewMatrix();

	protected:
		String mName;
		Vector3 mPosition;
		Vector3 mLookAt;
		Matrix4 mView;
		Frustum mFrustum;
		Viewport* mViewport;
	};
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE

#endif//_USM_CAMERA_H_