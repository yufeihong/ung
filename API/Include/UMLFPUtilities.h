/*!
 * \brief
 * 浮点数Utilities。不要include这个文件，而是按需所取包含其中的某个头文件。
 * \file UMLFPUtilities.h
 *
 * \author Su Yang
 *
 * \date 2016/06/15
 */
#ifndef _UML_FPUTILITIES_H_
#define _UML_FPUTILITIES_H_

#ifndef _UML_CONFIG_H_
#include "UMLConfig.h"
#endif

#ifdef UML_HIERARCHICAL_COMPILE

#ifndef _INCLUDE_BOOST_ROUND_HPP_
#include "boost/math/special_functions/round.hpp"											//四舍五入
#define _INCLUDE_BOOST_ROUND_HPP_
#endif

#ifndef _INCLUDE_BOOST_TRUNC_HPP_
#include "boost/math/special_functions/trunc.hpp"											//截断
#define _INCLUDE_BOOST_TRUNC_HPP_
#endif

#ifndef _INCLUDE_BOOST_MODF_HPP_
#include "boost/math/special_functions/modf.hpp"											//分割整数部分和小数部分
#define _INCLUDE_BOOST_MODF_HPP_
#endif

#ifndef _INCLUDE_BOOST_FPCLASSIFY_HPP
#include "boost/math/special_functions/fpclassify.hpp"									//分类：无穷和NaN
#define _INCLUDE_BOOST_FPCLASSIFY_HPP
#endif

#ifndef _INCLUDE_BOOST_SIGN_HPP_
#include "boost/math/special_functions/sign.hpp"											//符号
#define _INCLUDE_BOOST_SIGN_HPP_
#endif

#ifndef _INCLUDE_BOOST_NEXT_HPP_
#include "boost/math/special_functions/next.hpp"											//从两边位的角度去看待浮点数的尾数
#define _INCLUDE_BOOST_NEXT_HPP_
#endif

#ifndef _INCLUDE_BOOST_RELATIVEDIFFERENCE_HPP_
#include "boost/math/special_functions/relative_difference.hpp"						//Relative Comparison
#define _INCLUDE_BOOST_RELATIVEDIFFERENCE_HPP_
#endif

#endif//UML_HIERARCHICAL_COMPILE

#endif//_UML_FPUTILITIES_H_