/*!
 * \brief
 * 几何对象-球体
 * \file UPESphere.h
 *
 * \author Su Yang
 *
 * \date 2016/05/22
 */
#ifndef _UPE_SPHERE_H_
#define _UPE_SPHERE_H_

#ifndef _UPE_CONFIG_H_
#include "UPEConfig.h"
#endif

#ifdef UPE_HIERARCHICAL_COMPILE

#ifndef _UIF_IBOUNDINGVOLUME_H_
#include "UIFIBoundingVolume.h"
#endif

namespace ung
{
	class Plane;
	class AABB;

	/*!
	 * \brief
	 * 球体
	 * \class Sphere
	 *
	 * \author Su Yang
	 *
	 * \date 2016/05/22
	 *
	 * \todo
	 */
	class UngExport Sphere : public IBoundingVolume
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		Sphere();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~Sphere();

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param const Vector3 & c 球心位置
		 * \param const real_type r 球体半径
		*/
		Sphere(const Vector3& c, const real_type r);

		/*!
		 * \remarks 拷贝构造函数
		 * \return 
		 * \param const Sphere & s
		*/
		Sphere(const Sphere& s);

		/*!
		 * \remarks 拷贝赋值运算符
		 * \return 
		 * \param const Sphere & s
		*/
		Sphere& operator=(const Sphere& s);

		/*!
		 * \remarks 设置球心位置
		 * \return 
		 * \param const Vector3 & c
		*/
		void setCenter(const Vector3& c);

		/*!
		 * \remarks 设置球体的半径
		 * \return 
		 * \param const real_type r
		*/
		void setRadius(const real_type r);

		/*!
		 * \remarks 获取球心位置
		 * \return 
		*/
		const Vector3 getCenter() const;

		/*!
		 * \remarks 获取球体半径
		 * \return 
		*/
		const real_type getRadius() const;

		/*!
		 * \remarks 让当前球体能够包裹住给定参数的球体
		 * \return 
		 * \param const Sphere & s
		*/
		void merge(const Sphere& s);

		/*!
		 * \remarks 获取模型空间的AABB(认为球心在原点)
		 * \return 
		*/
		AABB getAABB() const;

		/*!
		 * \remarks 获取世界空间的AABB
		 * \return 
		*/
		AABB getWorldAABB() const;

	private:
		//(X - C) · (X - C) = r ^ 2
		Vector3 mC;
		real_type mR;
	};
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

#endif//_UPE_SPHERE_H_