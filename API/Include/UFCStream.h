/*!
 * \brief
 * 封装流。
 * \file UFCStream.h
 *
 * \author Su Yang
 *
 * \date 2016/12/15
 */
#ifndef _UFC_STREAM_H_
#define _UFC_STREAM_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#ifndef _UFC_FILTER_H_
#include "UFCFilter.h"
#endif

 //基本流处理
#ifndef _INCLUDE_BOOST_STREAM_HPP_
#include "boost/iostreams/stream.hpp"
#define _INCLUDE_BOOST_STREAM_HPP_
#endif

//过滤流
#ifndef _INCLUDE_BOOST_FILTERINGSTREAM_HPP_
#include "boost/iostreams/filtering_stream.hpp"
#define _INCLUDE_BOOST_FILTERINGSTREAM_HPP_
#endif

//流处理函数
#ifndef _INCLUDE_BOOST_OPERATIONS_HPP_
#include "boost/iostreams/operations.hpp"
#define _INCLUDE_BOOST_OPERATIONS_HPP_
#endif

//数组设备
#ifndef _INCLUDE_BOOST_DEVICEARRAY_HPP_
#include "boost/iostreams/device/array.hpp"
#define _INCLUDE_BOOST_DEVICEARRAY_HPP_
#endif

//设备适配器
#ifndef _INCLUDE_BOOST_DEVICEBACKINSERTER_HPP_
#include "boost/iostreams/device/back_inserter.hpp"
#define _INCLUDE_BOOST_DEVICEBACKINSERTER_HPP_
#endif

//预定义过滤器
#ifndef _INCLUDE_BOOST_FILTERCOUNTER_HPP_
#include "boost/iostreams/filter/counter.hpp"
#define _INCLUDE_BOOST_FILTERCOUNTER_HPP_
#endif

 //算法
#ifndef _INCLUDE_BOOST_IOSTREAMSCOPY_HPP_
#pragma warning(disable:4244)
#include "boost/iostreams/copy.hpp"
#define _INCLUDE_BOOST_IOSTREAMSCOPY_HPP_
#endif

//type traits
#ifndef _INCLUDE_BOOST_TYPETRAITS_HPP_
#include "boost/type_traits.hpp"
#define _INCLUDE_BOOST_TYPETRAITS_HPP_
#endif

//iostreams库的所有traits工具
#ifndef _INCLUDE_BOOST_IOSTREAMSTRAITS_HPP_
#include "boost/iostreams/traits.hpp"
#define _INCLUDE_BOOST_IOSTREAMSTRAITS_HPP_
#endif

//模板元编程
#ifndef _INCLUDE_BOOST_MPLIF_HPP_
#include "boost/mpl/if.hpp"
#define _INCLUDE_BOOST_MPLIF_HPP_
#endif

namespace ung
{
	/*!
	 * \remarks 从内存拷贝数据到内存
	 * \return
	 * \param const Ch* const source 指向常量的常量指针
	 * \param Ch* const sink 常量指针
	 * \param size_t processSize
	*/
	template<typename Ch,typename Filter = DefaultFilter<Ch>>
	int ufcMemoryToMemory(const Ch* const source,size_t sourceSize,Ch* const sink,size_t processSize = MEGABYTES * 10)
	{
		using namespace boost::iostreams;
		using namespace boost;
		using namespace boost::mpl;

		using element_type = Ch;
		using filter_type = Filter;

		/*
		流处理的数据通常是字符类型（char或者wchar_t），但也可以使用模板参数指定处理任意的类型，因此，
		流是一套完整的数据处理框架。
		仅支持element_type为char或者wchar_t:
		*/
		BOOST_STATIC_ASSERT((is_same<element_type, char>::value || is_same<element_type, wchar_t>::value) && "Only support char or wchar_t");

		//确保是过滤器类
		BOOST_STATIC_ASSERT(is_filter<filter_type>::value && "The second template param is not a class of filter");

		//确保源数据有效
		BOOST_ASSERT(source && "The pointer to source data is not valid");
		BOOST_ASSERT(sourceSize > 0 && "Source data is not valid");

		//确保读取的数据字节数有效
		BOOST_ASSERT(sink && "The pointer to sink space is not valid");
		BOOST_ASSERT(processSize > 0 && "Process data bytes must greater than 0");

		//防止读取越界
		processSize = std::min(sourceSize,processSize);

		/*
		array_source是一个设备，它可以把一个字符缓冲区（字符数组）给适配成一个数组设备。

		设备最基本的特征是它们能够处理的“字符类型”，可以使用元函数char_type_of<T>::type来获得，它类似于标准库的std::char_traits<Ch>::char_type

		流上的所有设备协同工作的最低要求是它们必须处理的是同一种“字符类型”。

		数组设备提供了对内存字符序列的访问，可以把字符缓冲区适配成seekable设备。数组设备不负责缓冲区的内存管理，因此它通常搭配
		静态数组或者std::vector来使用。

		源设备(通过“源数据”来构造)
		如果通过数组引用的方式来构造一个数组设备，那么其真正的长度将包含字符串最后的null标记（也就是字符数组真正的长度）。例如：
		char src[]{"123"};
		array_source as(src);
		assert(as.input_sequence().second == src + 4);
		数组设备的成员函数input_sequence()和output_sequence()返回一个pair对象，标记了数组设备所控制的序列范围。
		*/
		basic_array_source<element_type> arraySourceDevice(source, source + processSize);
		//检查是一个设备
		BOOST_STATIC_ASSERT(is_device<basic_array_source<element_type>>::value);
		//检查源设备的模式
		BOOST_STATIC_ASSERT(is_same<mode_of<basic_array_source<element_type>>::type,input_seekable>::value);
		//检查源设备的分类
		BOOST_STATIC_ASSERT(is_convertible<category_of<basic_array_source<element_type>>::type, input_seekable>::value);
		BOOST_STATIC_ASSERT(is_convertible<category_of<basic_array_source<element_type>>::type, device_tag>::value);

		/*
		把流给连接到一个设备，形成一个与标准库流完全兼容的新的流。
		stream是标准库流的子类，因此具有标准IO流的所有能力。
		stream并不像标准库流那样用名字来区分输入流和输出流，它的方向完全取决于它关联的设备。

		输入流(通过“源设备”来构造)
		*/
		stream<basic_array_source<element_type>> in(arraySourceDevice);
		BOOST_ASSERT(in.is_open());
		/*
		此时可以使用重载的operator*和operator->来获得流内部的设备引用，就像是一个智能指针。
		*/

		//目的设备
		basic_array_sink<element_type> arraySinkDevice(sink, sink + processSize);
		//检查目的设备的模式
		BOOST_STATIC_ASSERT(is_same<mode_of<basic_array_sink<element_type>>::type, output_seekable>::value);
		//检查目的设备的分类
		BOOST_STATIC_ASSERT(is_convertible<category_of<basic_array_sink<element_type>>::type,output_seekable>::value);
		BOOST_STATIC_ASSERT(is_convertible<category_of<basic_array_sink<element_type>>::type,device_tag>::value);

		//根据element_type是char或者wchar_t来选择输出过滤流
		using filtering_ostream_type = if_c<is_same<element_type, char>::value, filtering_ostream, filtering_wostream>::type;
	
		//检查过滤器的分类
		BOOST_STATIC_ASSERT(is_convertible<category_of<filter_type>::type, filter_tag>::value);
	
		/*
		与stream不同，filtering_stream不能直接连接设备来决定它的模式，而是需要使用模板参数来指明，但
		我们通常会直接使用定义好的filtering_istream和filtering_ostream。
		管道操作只能针对设备，它不能连接流，如果要把流对象加入过滤流则必须使用成员函数push()。

		输出流，需要连接sink设备作为链的结束。
		*/
		filtering_ostream_type out(filter_type() | arraySinkDevice);
		BOOST_ASSERT(out.is_complete());

		/*
		template<typename Source,typename Sink>
		std::streamsize copy(Source& src,Sink& snk);
		它有4种重载的形式，用来应对src和snk是设备或流的情形。比如：
		char ar[]{"abc"};
		std::string str;
		//从源设备到标准输出流
		copy(stream<array_source>(ar),cout);
		//从源设备到接收设备
		copy(stream<array_source>(ar),boost::iostream::back_inserter(str));

		std::copy()操纵迭代器，把数据从一个可读迭代器拷贝到另一个可写迭代器。
		boost::iostreams::copy()则用来操作流，把数据从一个输入流拷贝到输出流。

		驱动整个数据流：从源设备或流中读入字符，再写入到接收设备或流中，最后关闭两个设备并返回处理
		的字符数。

		让数据动起来
		*/
		std::streamsize retCount = copy(in, out);
		BOOST_ASSERT(retCount = processSize);

		/*
		过滤流的成员函数component<>()可以返回过滤流的设备链中的第n个设备:
		component<Filter>(n)，然后这个返回的设备可以调用其（Filter）成员函数。
		*/

		return processSize;
	}

	//------------------------------------------------------------------------------------

	//从内存拷贝数据到容器
	template<typename Ch, typename Con, typename Filter = DefaultFilter<Ch>>
	int ufcMemoryToContainer(const Ch* const source, size_t sourceSize, Con& sink, size_t processSize = MEGABYTES * 10)
	{
		using namespace boost::iostreams;
		using namespace boost;
		using namespace boost::mpl;

		using element_type = Ch;
		using container_type = Con;
		using filter_type = Filter;

		BOOST_STATIC_ASSERT((is_same<element_type, char>::value || is_same<element_type, wchar_t>::value) && "Only support char or wchar_t");
		//确保内存字符类型与容器元素类型一致
		BOOST_STATIC_ASSERT(is_same<element_type, typename container_type::value_type>::value && "The container value type does not match the char type");
		BOOST_STATIC_ASSERT(is_filter<filter_type>::value && "The second template param is not a class of filter");
		BOOST_ASSERT(source && "The pointer to source data is not valid");
		BOOST_ASSERT(sourceSize > 0 && "Source data is not valid");
		BOOST_ASSERT(processSize > 0 && "Process data bytes must greater than 0");

		processSize = std::min(sourceSize, processSize);

		stream<basic_array_source<element_type>> in(source, source + processSize);

		using filtering_ostream_type = if_c<is_same<element_type, char>::value, filtering_ostream, filtering_wostream>::type;
		/*
		boost::iostreams::back_inserter()是一个接收设备生成器，可以把一个标准库容器适配成一个接收设备
		*/
		filtering_ostream_type out(filter_type() | boost::iostreams::back_inserter(sink));

		copy(in, out);

		return processSize;
	}

	//从容器拷贝数据到内存
	template<typename Ch, typename Con, typename Filter = DefaultFilter<Ch>>
	int ufcContainerToMemory(Con const& source,Ch* const sink, size_t processSize = MEGABYTES * 10)
	{
		using namespace boost::iostreams;
		using namespace boost;
		using namespace boost::mpl;

		using element_type = Ch;
		using container_type = Con;
		using filter_type = Filter;

		size_t sourceSize = source.size();

		BOOST_STATIC_ASSERT((is_same<element_type, char>::value || is_same<element_type, wchar_t>::value) && "Only support char or wchar_t");
		//确保内存字符类型与容器元素类型一致
		BOOST_STATIC_ASSERT(is_same<element_type, typename container_type::value_type>::value && "The container value type does not match the char type");
		BOOST_STATIC_ASSERT(is_filter<filter_type>::value && "The second template param is not a class of filter");
		BOOST_ASSERT(sourceSize > 0 && "The container is empty");
		BOOST_ASSERT(processSize > 0 && "Process data bytes must greater than 0");

		processSize = std::min(sourceSize, processSize);

		using filtering_istream_type = if_c<is_same<element_type, char>::value, filtering_istream, filtering_wistream>::type;
		/*
		iostreams库不能把一个标准库容器直接适配成源设备，但是借助boost::make_iterator_range()函数，可以把标准库容器直接传递给
		过滤流创建出一个输入流。
		*/
		filtering_istream_type in(make_iterator_range(source));

		using filtering_ostream_type = if_c<is_same<element_type, char>::value, filtering_ostream, filtering_wostream>::type;
		filtering_ostream_type out(filter_type() | basic_array_sink<element_type>(sink, sink + processSize));

		copy(in, out);

		return processSize;
	}

	//------------------------------------------------------------------------------------

	//从文件拷贝数据到内存
	template<typename Filter = DefaultFilter<char>>
	void ufcFileToMemory(std::string const& fileFullName,std::string& sink)
	{
		using namespace boost::iostreams;

		using filter_type = Filter;

		BOOST_STATIC_ASSERT(is_filter<filter_type>::value && "The second template param is not a class of filter");

		file_descriptor_source in(fileFullName);

		filtering_ostream out(filter_type() | boost::iostreams::back_inserter(sink));

		copy(in, out);
	}

	//从内存拷贝数据到文件
	template<typename Filter = DefaultFilter<char>>
	void ufcMemoryToFile(const char* const source,size_t sourceSize,const char* const fileFullName)
	{
		using namespace boost::iostreams;

		using filter_type = Filter;

		BOOST_STATIC_ASSERT(is_filter<filter_type>::value && "The second template param is not a class of filter");

		array_source in(source,source + sourceSize);

		filtering_ostream out(filter_type() | file_descriptor_sink(fileFullName));

		copy(in, out);
	}
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_STREAM_H_