/*!
 * \brief
 * �������ó�ʼ����
 * \file USMInit.h
 *
 * \author Su Yang
 *
 * \date 2016/11/24
 */
#ifndef _UBS_INIT_H_
#define _UBS_INIT_H_

#ifndef _UBS_CONFIG_H_
#include "UBSConfig.h"
#endif

#ifdef UBS_HIERARCHICAL_COMPILE

namespace ung
{
	extern String global_plugins_path;
}//namespace ung

namespace ung
{
	UNG_C_LINK_BEGIN;

	UngExport void UNG_STDCALL ubsInit();

	UNG_C_LINK_END;
}//namespace ung

#endif//UBS_HIERARCHICAL_COMPILE

#endif//_UBS_INIT_H_