/*!
 * \brief
 * Null类型
 * \file UFCNullType.h
 *
 * \author Su Yang
 *
 * \date 2016/04/24
 */
#ifndef _UFC_NULLTYPE_H_
#define _UFC_NULLTYPE_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

namespace ung
{
	namespace utp
	{
		/*!
		 * \brief
		 * NullType可被视为一个结束记号,类似传统C字符串的\0功能,我们可以定义一个只持有单一元素的Typelist如下:typedef Typelist<int,NullType> OneTypeOnly;
		 * \class classname
		 *
		 * \author Su Yang
		 *
		 * \date 2016/04/24
		 *
		 * \todo
		 */
		class NullType;
	}//namespace utp
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_NULLTYPE_H_