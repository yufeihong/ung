/*!
 * \brief
 * 常量
 * \file UMLConsts.h
 *
 * \author Su Yang
 *
 * \date 2016/06/15
 */
#ifndef _UML_CONSTS_H_
#define _UML_CONSTS_H_

#ifndef _UML_CONFIG_H_
#include "UMLConfig.h"
#endif

#ifdef UML_HIERARCHICAL_COMPILE

#ifndef _INCLUDE_BOOST_CONSTANTS_HPP_
#include "boost/math/constants/constants.hpp"
#define _INCLUDE_BOOST_CONSTANTS_HPP_
#endif

namespace ung
{
	/*!
	 * \brief
	 * 常量。
	 * \class Contants
	 *
	 * \author Su Yang
	 *
	 * \date 2016/06/15
	 *
	 * \todo
	 */
	template<typename T>
	class Consts
	{
	public:
		using type = T;

		static const type SIXTHPI;																	//六分之一
		static const type FOURTHPI;																//四分之一
		static const type THIRDPI;																	//三分之一
		static const type HALFPI;
		static const type TWOTHIRDSPI;															//三分之二
		static const type THREEQUARTERSPI;													//四分之三
		static const type PI;
		static const type TWOPI;

		static const type THIRD;																		//三分之一
		static const type TWOTHIRDS;																//三分之二
		static const type THREEQUARTERS;														//四分之三
		static const type ROOTTWO;																//根号2
		static const type ROOTTHREE;																//根号3
		static const type RADIAN;																	//1弧度为多少度
		static const type DEGREE;																	//1度为多少弧度

		//------------------------------------------------------------------------------------------------------------------------------

		static const type EPSILON;

		static const type ZERO;
	};

	#define CONSTANTS_STATICMEMBER_DEFINE template<typename T> const T Consts<T>

	CONSTANTS_STATICMEMBER_DEFINE::SIXTHPI = boost::math::constants::sixth_pi<type>();
	CONSTANTS_STATICMEMBER_DEFINE::FOURTHPI = boost::math::constants::half_pi<type>() * 0.5;
	CONSTANTS_STATICMEMBER_DEFINE::THIRDPI = boost::math::constants::third_pi<type>();
	CONSTANTS_STATICMEMBER_DEFINE::HALFPI = boost::math::constants::half_pi<type>();
	CONSTANTS_STATICMEMBER_DEFINE::TWOTHIRDSPI = boost::math::constants::two_thirds_pi<type>();
	CONSTANTS_STATICMEMBER_DEFINE::THREEQUARTERSPI = boost::math::constants::three_quarters_pi<type>();
	CONSTANTS_STATICMEMBER_DEFINE::PI = boost::math::constants::pi<type>();
	CONSTANTS_STATICMEMBER_DEFINE::TWOPI = boost::math::constants::two_pi<type>();

	CONSTANTS_STATICMEMBER_DEFINE::THIRD = boost::math::constants::third<type>();
	CONSTANTS_STATICMEMBER_DEFINE::TWOTHIRDS = boost::math::constants::two_thirds<type>();
	CONSTANTS_STATICMEMBER_DEFINE::THREEQUARTERS = boost::math::constants::three_quarters<type>();
	CONSTANTS_STATICMEMBER_DEFINE::ROOTTWO = boost::math::constants::root_two<type>();
	CONSTANTS_STATICMEMBER_DEFINE::ROOTTHREE = boost::math::constants::root_three<type>();
	CONSTANTS_STATICMEMBER_DEFINE::RADIAN = boost::math::constants::radian<type>();
	CONSTANTS_STATICMEMBER_DEFINE::DEGREE = boost::math::constants::degree<type>();

	CONSTANTS_STATICMEMBER_DEFINE::EPSILON = std::numeric_limits<type>::epsilon();
	/*
	e:表示10
	a * 10 ^ b,表示为a * eb
	0.000001
	*/
#if UNG_USE_DOUBLE
	CONSTANTS_STATICMEMBER_DEFINE::ZERO = 1e-006;
#else
	CONSTANTS_STATICMEMBER_DEFINE::ZERO = 1e-004;
#endif
}//namespace ung

#endif//UML_HIERARCHICAL_COMPILE

#endif//_UML_CONSTS_H_