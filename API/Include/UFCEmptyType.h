/*!
 * \brief
 * Empty类型
 * \file UFCEmptyType.h
 *
 * \author Su Yang
 *
 * \date 2016/04/24
 */
#ifndef _UFC_EMPTYTYPE_H_
#define _UFC_EMPTYTYPE_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

namespace ung
{
	namespace utp
	{
		/*!
		 * \brief
		 * 一个不持有任何东西的类，扮演一个strawman class（稻草人类）
		 * \class EmptyType
		 *
		 * \author Su Yang
		 *
		 * \date 2016/04/24
		 *
		 * \todo
		 */
		class EmptyType
		{
		};


		inline bool operator==(const EmptyType&, const EmptyType&)
		{
			return true;
		}

		inline bool operator<(const EmptyType&, const EmptyType&)
		{
			return false;
		}

		inline bool operator>(const EmptyType&, const EmptyType&)
		{
			return false;
		}
	}//namespace utp
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_EMPTYTYPE_H_