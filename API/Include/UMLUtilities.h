/*!
 * \brief
 * 实用工具
 * \file UMLUtilities.h
 *
 * \author Su Yang
 *
 * \date 2016/11/21
 */
#ifndef _UML_UTILITIES_H_
#define _UML_UTILITIES_H_

#ifndef _UML_CONFIG_H_
#include "UMLConfig.h"
#endif

#ifdef UML_HIERARCHICAL_COMPILE

#ifndef _UML_TYPEDEF_H_
#include "UMLTypedef.h"
#endif

#ifndef _INCLUDE_BOOST_RANGE_HPP_
#include "boost/range.hpp"
#define _INCLUDE_BOOST_RANGE_HPP_
#endif

namespace ung
{
	UNG_C_LINK_BEGIN;

	/*!
	 * \remarks 判断是否是2的整数倍
	 * \return 
	 * \param uint32 i
	*/
	UngExport bool UNG_STDCALL ungIsPow2(uint32 i);

	/*!
	 * \remarks 计算以2为底的对数(根据结果来计算指数是多少)
	 * \return 
	 * \param uint32 n
	*/
	UngExport uint32 UNG_STDCALL ungLog2(uint32 n);

	/*!
	 * \remarks 点积
	 * \return 
	 * \param Vector3 const & v1
	 * \param Vector3 const & v2
	*/
	UngExport real_type UNG_STDCALL ungVector3DotProduct(Vector3 const& v1, Vector3 const& v2);

	/*!
	 * \remarks 叉积
	 * \return 
	 * \param Vector3 const & left
	 * \param Vector3 const & right
	 * \param Vector3 & cross
	*/
	UngExport void UNG_STDCALL ungVector3CrossProduct(Vector3 const& left, Vector3 const& right,Vector3& cross);

	/*!
	 * \remarks 把一个4x4矩阵所代表的变换，应用到3维向量上
	 * \return 变换后的3维向量
	 * \param Vector3 & vec3Out 同return
	 * \param Vector3 const & vec3In
	 * \param Mat4 const & mat4In
	*/
	UngExport Vector3& UNG_STDCALL ungTransformVec(Vector3& vec3Out,Vector3 const& vec3In,Matrix4 const& mat4In);

	/*!
	 * \remarks 把一个4x4矩阵所代表的变换，应用到3维顶点上
	 * \return 变换后的3维顶点，位于w = 1平面上
	 * \param Vector3 & vec3Out 同return
	 * \param Vector3 const & vec3In
	 * \param Mat4 const & mat4In
	*/
	UngExport Vector3& UNG_STDCALL ungTransformPos(Vector3& vec3Out,Vector3 const& vec3In,Matrix4 const& mat4In);

	/*!
	 * \remarks 计算标量三重积。
	 * \return 平行六面体的有符号体积。四面体体积的6倍。
	 * \param Vector3 const & u
	 * \param Vector3 const & v
	 * \param Vector3 const & w
	*/
	UngExport real_type UNG_STDCALL ungScalarTripleProduct(Vector3 const& u,Vector3 const& v,Vector3 const& w);

	/*!
	 * \remarks 计算由3个不相关的向量确定的平行六面体的有符号体积
	 * \return 
	 * \param Vector3 const & v1
	 * \param Vector3 const & v2
	 * \param Vector3 const & v3
	*/
	UngExport real_type UNG_STDCALL ungSignedVolumeOfParallelepiped(Vector3 const& v1, Vector3 const& v2, Vector3 const& v3);

	/*!
	 * \remarks 求解线性方程组
	 * 用法：
	 * ax + by = e
	 * cx + dy = f
	 * 那么系数矩阵为:
	 * a	b
	 * c	d
	 * 常数向量为：
	 * e	f
	 * 待求解向量为：
	 * x	y
	 * \return 
	 * \param Matrix2 const & coefficientMatrix 系数矩阵
	 * \param Vector2 const & constantVector 常数向量
	 * \param Vector2 & out 待求解的向量
	*/
	UngExport bool UNG_STDCALL ungSolvingLinearEquations2(Matrix2 const& coefficientMatrix, Vector2 const& constantVector, Vector2& out);

	/*!
	 * \remarks 求解线性方程组
	 * \return 
	 * \param Matrix3 const & coefficientMatrix 系数矩阵
	 * \param Vector3 const & constantVector 常数向量
	 * \param Vector3 & out 待求解的向量
	*/
	UngExport bool UNG_STDCALL ungSolvingLinearEquations3(Matrix3 const& coefficientMatrix, Vector3 const& constantVector, Vector3& out);

	/*!
	 * \remarks 求解线性方程组
	 * \return 
	 * \param Matrix4 const & coefficientMatrix 系数矩阵
	 * \param Vector4 const & constantVector 常数向量
	 * \param Vector4 & out 待求解的向量
	*/
	UngExport bool UNG_STDCALL ungSolvingLinearEquations4(Matrix4 const& coefficientMatrix, Vector4 const& constantVector, Vector4& out);

	UNG_C_LINK_END;

	/*!
	 * \remarks 把一个4x4矩阵所代表的变换，应用到一组3维向量上
	 * \return 存放变换后的一组3维向量的容器
	 * \param ForwardRange & ContainerOut 同return
	 * \param ForwardRange const & ContainerIn
	 * \param Mat4 const& mat4In
	*/
	template<typename ForwardRange>
	ForwardRange& UNG_STDCALL ungTransformVecRange(ForwardRange& ContainerOut,ForwardRange const& ContainerIn, Matrix4 const& mat4In)
	{
		BOOST_CONCEPT_ASSERT((boost::ForwardRangeConcept<ForwardRange>));
		BOOST_ASSERT(!boost::empty(ContainerIn));
		BOOST_ASSERT(ContainerOut.size() >= ContainerIn.size());

		auto func = [&](Vector3 const& v3) -> Vector3
		{
			Vector3 v3Out;
			v3Out.x = v3.x * mat4In.m[0][0] + v3.y * mat4In.m[1][0] + v3.z * mat4In.m[2][0];
			v3Out.y = v3.x * mat4In.m[0][1] + v3.y * mat4In.m[1][1] + v3.z * mat4In.m[2][1];
			v3Out.z = v3.x * mat4In.m[0][2] + v3.y * mat4In.m[1][2] + v3.z * mat4In.m[2][2];

			return v3Out;
		};

		std::transform(boost::begin(ContainerIn), boost::end(ContainerIn), boost::begin(ContainerOut),func);

		return ContainerOut;
	}

	/*!
	 * \remarks 把一个4x4矩阵所代表的变换，应用到一组3维顶点上
	 * \return 存放变换后的一组3维顶点的容器，顶点位于w = 1平面上
	 * \param ForwardRange & ContainerOut 同return
	 * \param ForwardRange const & ContainerIn
	 * \param Mat4 const& mat4In
	*/
	template<typename ForwardRange>
	ForwardRange& UNG_STDCALL ungTransformPosRange(ForwardRange& ContainerOut, ForwardRange const& ContainerIn, Matrix4 const& mat4In)
	{
		BOOST_CONCEPT_ASSERT((boost::ForwardRangeConcept<ForwardRange>));
		BOOST_ASSERT(!boost::empty(ContainerIn));
		BOOST_ASSERT(ContainerOut.size() >= ContainerIn.size());

		auto func = [&](Vector3 const& v3) -> Vector3
		{
			Vector3 v3Out;

			real_type w = 1.0;

			v3Out.x = v3.x * mat4In.m[0][0] + v3.y * mat4In.m[1][0] + v3.z * mat4In.m[2][0] + mat4In.m[3][0];
			v3Out.y = v3.x * mat4In.m[0][1] + v3.y * mat4In.m[1][1] + v3.z * mat4In.m[2][1] + mat4In.m[3][1];
			v3Out.z = v3.x * mat4In.m[0][2] + v3.y * mat4In.m[1][2] + v3.z * mat4In.m[2][2] + mat4In.m[3][2];
			w		   = v3.x * mat4In.m[0][3] + v3.y * mat4In.m[1][3] + v3.z * mat4In.m[2][3] + mat4In.m[3][3];

			BOOST_ASSERT(w);

			real_type wInv = 1.0 / w;

			v3Out.x *= wInv;
			v3Out.y *= wInv;
			v3Out.z *= wInv;

			return v3Out;
		};

		std::transform(boost::begin(ContainerIn), boost::end(ContainerIn), boost::begin(ContainerOut), func);

		return ContainerOut;
	}
}//namespace ung

#endif//UML_HIERARCHICAL_COMPILE

#endif//_UML_UTILITIES_H_