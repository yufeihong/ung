/*!
 * \brief
 * HashSet
 * \file UTLHashSet.h
 *
 * \author Su Yang
 *
 * \date 2016/04/09
 */
#ifndef _UNG_UTL_HASHSET_H_
#define _UNG_UTL_HASHSET_H_

#ifndef _UNG_UTL_CONFIG_H_
#include "UTLConfig.h"
#endif

#ifndef _UNG_UTL_HASHTABLE_H_
#include "UTLHashTable.h"
#endif

namespace ung
{
	namespace utl
	{
		/*!
		 * \brief
		 * HashSet
		 * \class HashSet
		 *
		 * \author Su Yang
		 *
		 * \date 2016/04/09
		 *
		 * \todo
		 */
		template <typename Value, typename HashFcn = boost::hash<Value>, typename EqualKey = std::equal_to<Value>, typename Alloc = Allocator<HashTableNode<Value>>>
		class HashSet
		{
		private:
			typedef HashTable<Value, Value, HashFcn, std::identity<Value>,EqualKey, Alloc> table_type;
			table_type mTable;

		public:
			typedef typename table_type::key_type key_type;
			typedef typename table_type::value_type value_type;
			typedef typename table_type::hasher hasher;
			typedef typename table_type::key_equal key_equal;

			typedef typename table_type::size_type size_type;
			typedef typename table_type::difference_type difference_type;
			typedef typename table_type::const_pointer pointer;
			typedef typename table_type::const_pointer const_pointer;
			typedef typename table_type::const_reference reference;
			typedef typename table_type::const_reference const_reference;

			typedef typename table_type::const_iterator iterator;
			typedef typename table_type::const_iterator const_iterator;

			hasher getHasher() const
			{
				return mTable.getHasher();
			}

			key_equal getEqual() const
			{
				return mTable.getEqual();
			}

			HashSet() :
				mTable(100, hasher(), key_equal())
			{
			}

			explicit HashSet(size_type n) :
				mTable(n, hasher(), key_equal())
			{
			}

			HashSet(size_type n, const hasher& hf) :
				mTable(n, hf, key_equal())
			{
			}

			HashSet(size_type n, const hasher& hf, const key_equal& eql) :
				mTable(n, hf, eql)
			{
			}

			template <class InputIterator>
			HashSet(InputIterator first, InputIterator last) :
				mTable(100, hasher(), key_equal())
			{
				mTable.insertUnique(first, last);
			}

			template <class InputIterator>
			HashSet(InputIterator first, InputIterator last, size_type n) :
				mTable(n, hasher(), key_equal())
			{
				mTable.insertUnique(first, last);
			}

			template <class InputIterator>
			HashSet(InputIterator first, InputIterator last, size_type n,const hasher& hf) :
				mTable(n, hf, key_equal())
			{
				mTable.insertUnique(first, last);
			}

			template <class InputIterator>
			HashSet(InputIterator first, InputIterator last, size_type n,const hasher& hf, const key_equal& eql) :
				mTable(n, hf, eql)
			{
				mTable.insertUnique(first, last);
			}

			size_type size() const
			{
				return mTable.size();
			}

			size_type max_size() const
			{
				return mTable.max_size();
			}

			bool empty() const
			{
				return mTable.empty();
			}

			void swap(HashSet& hs)
			{
				mTable.swap(hs.mTable);
			}

			iterator begin() const
			{
				return mTable.begin();
			}

			iterator end() const
			{
				return mTable.end();
			}

			std::pair<iterator, bool> insert(const value_type& obj)
			{
				std::pair<typename table_type::iterator, bool> p = mTable.insertUnique(obj);

				return std::pair<iterator, bool>(p.first, p.second);
			}

			template <class InputIterator>
			void insert(InputIterator first, InputIterator last)
			{
				mTable.insertUnique(first, last);
			}

			std::pair<iterator, bool> insertNoResize(const value_type& obj)
			{
				std::pair<typename table_type::iterator, bool> p = mTable.insertUniqueNoResize(obj);

				return std::pair<iterator, bool>(p.first, p.second);
			}

			iterator find(const key_type& key) const
			{
				return mTable.find(key);
			}

			size_type count(const key_type& key) const
			{
				return mTable.count(key);
			}

			std::pair<iterator, iterator> equalRange(const key_type& key) const
			{
				return mTable.equalRange(key);
			}

			size_type erase(const key_type& key)
			{
				return mTable.erase(key);
			}

			void erase(iterator it)
			{
				mTable.erase(it);
			}

			void erase(iterator first, iterator last)
			{
				mTable.erase(first, last);
			}

			void clear()
			{
				mTable.clear();
			}

			void resize(size_type hint)
			{
				mTable.resize(hint);
			}

			size_type bucketCount() const
			{
				return mTable.bucketCount();
			}

			size_type maxBucketCount() const
			{
				return mTable.maxBucketCount();
			}

			size_type elemsNumInBucket(size_type n) const
			{
				return mTable.elemsNumInBucket(n);
			}

			template <typename Value, typename HashFcn, typename EqualKey, typename Alloc>
			friend bool operator== (const HashSet<Value, HashFcn, EqualKey, Alloc>&, const HashSet<Value, HashFcn, EqualKey, Alloc>&);

			template <typename Value, typename HashFcn, typename EqualKey, typename Alloc>
			friend bool operator!= (const HashSet<Value, HashFcn, EqualKey, Alloc>&, const HashSet<Value, HashFcn, EqualKey, Alloc>&);

			template <typename Value, typename HashFcn, typename EqualKey, typename Alloc>
			inline void swap(HashSet<Value, HashFcn, EqualKey, Alloc>&, HashSet<Value, HashFcn, EqualKey, Alloc>&);
		};

		template <typename Value, typename HashFcn, typename EqualKey, typename Alloc>
		inline bool operator==(const HashSet<Value, HashFcn, EqualKey, Alloc>& hs1,const HashSet<Value, HashFcn, EqualKey, Alloc>& hs2)
		{
			return hs1.mTable == hs2.mTable;
		}

		template <typename Value, typename HashFcn, typename EqualKey, typename Alloc>
		inline bool operator!=(const HashSet<Value, HashFcn, EqualKey, Alloc>& hs1,const HashSet<Value, HashFcn, EqualKey, Alloc>& hs2)
		{
			return !(hs1 == hs2);
		}

		template <typename Value, typename HashFcn, typename EqualKey, typename Alloc>
		inline void swap(HashSet<Value, HashFcn, EqualKey, Alloc>& hs1,HashSet<Value, HashFcn, EqualKey, Alloc>& hs2)
		{
			hs1.swap(hs2);
		}
	}//namespace utl
}//namespace ung

#endif//_UNG_UTL_HASHSET_H_