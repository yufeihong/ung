/*!
 * \brief
 * 场景管理器基类。
 * \file USMSceneManagerBase.h
 *
 * \author Su Yang
 *
 * \date 2016/11/26
 */
#ifndef _USM_SCENEMANAGERBASE_H_
#define _USM_SCENEMANAGERBASE_H_

#ifndef _USM_CONFIG_H_
#include "USMConfig.h"
#endif

#ifdef USM_HIERARCHICAL_COMPILE

#ifndef _UIF_ISCENEMANAGER_H_
#include "UIFISceneManager.h"
#endif

namespace ung
{
	class Camera;

	/*!
	 * \brief
	 * 场景管理器基类。
	 * \class SceneManagerBase
	 *
	 * \author Su Yang
	 *
	 * \date 2016/11/26
	 *
	 * \todo
	 */
	class UngExport SceneManagerBase : public ISceneManager
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		SceneManagerBase() = default;

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~SceneManagerBase() = default;

		//-------------------------------------------------------------------------------------------

		/*!
		 * \remarks 获取场景名字
		 * \return 
		*/
		virtual String const& getName() const override;

		/*!
		 * \remarks 获取场景类型
		 * \return 
		*/
		virtual SceneType getSceneType() const override;

		/*!
		 * \remarks 获取场景图根节点
		 * \return 
		*/
		virtual StrongISceneNodePtr getSceneRootNode() const override;

		//-------------------------------------------------------------------------------------------

		/*!
		 * \remarks 创建场景对象
		 * \return 
		 * \param String const & objectDataFileFullName
		 * \param String const & objectName
		*/
		virtual StrongIObjectPtr createObject(String const& objectDataFileFullName, String const& objectName = EMPTY_STRING) override;

		/*!
		 * \remarks 销毁对象
		 * \return 
		 * \param String const & objectName
		 * \param bool remove true:从场景节点的容器中移除
		*/
		virtual void destroyObject(String const& objectName,bool remove = true) override;

		/*!
		 * \remarks 销毁对象
		 * \return 
		 * \param StrongIObjectPtr objectPtr
		 * \param bool remove true:从场景节点的容器中移除
		*/
		virtual void destroyObject(StrongIObjectPtr objectPtr,bool remove = true) override;

		/*!
		 * \remarks 获取一个对象
		 * \return 
		 * \param String const& objectName
		*/
		virtual StrongIObjectPtr getObject(String const& objectName) override;

		/*!
		 * \remarks 获取对象池中对象的数量
		 * \return 
		*/
		virtual uint32 getAllObjectCount() const override;

		/*!
		 * \remarks 获取场景节点池中场景节点的数量
		 * \return 
		*/
		virtual uint32 getAllSceneNodeCount() const override;

		//-------------------------------------------------------------------------------------------

		/*!
		 * \remarks 创建一个场景节点
		 * \return 
		 * \param WeakISceneNodePtr parentPtr
		 * \param String const & nodeName
		*/
		virtual StrongISceneNodePtr createSceneNode(WeakISceneNodePtr parentPtr,String const& nodeName = EMPTY_STRING) override;

		/*!
		 * \remarks 销毁一个场景节点(包括该节点的family节点，以及一切关联的对象)
		 * \return 
		 * \param String const & nodeName
		*/
		virtual void destroySceneNode(String const& nodeName) override;

		/*!
		 * \remarks 销毁一个场景节点(包括该节点的family节点，以及一切关联的对象)
		 * \return 
		 * \param StrongISceneNodePtr nodePtr
		*/
		virtual void destroySceneNode(StrongISceneNodePtr nodePtr) override;

		//-------------------------------------------------------------------------------------------

		/*!
		 * \remarks 获取当前的空间管理器
		 * \return 
		*/
		virtual ISpatialManager* getSpatialManager() const override;

		//-------------------------------------------------------------------------------------------

		/*!
		 * \remarks 创建射线场景查询
		 * \return 
		 * \param Ray const & ray
		*/
		virtual StrongISceneQueryPtr createRaySceneQuery(Ray const& ray) override;

		/*!
		 * \remarks 创建球体场景查询
		 * \return 
		 * \param Sphere const & sphere
		*/
		virtual StrongISceneQueryPtr createSphereSceneQuery(Sphere const& sphere) override;

		/*!
		 * \remarks 创建AABB场景查询
		 * \return 
		 * \param AABB const & box
		*/
		virtual StrongISceneQueryPtr createAABBSceneQuery(AABB const& box) override;

		/*!
		 * \remarks 创建摄像机场景查询
		 * \return 
		 * \param Camera const & camera
		*/
		virtual StrongISceneQueryPtr createCameraSceneQuery(Camera const& camera) override;
	};
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE

#endif//_USM_SCENEMANAGERBASE_H_