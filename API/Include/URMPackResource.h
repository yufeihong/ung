/*!
 * \brief
 * 包资源
 * \file URMPackResource.h
 *
 * \author Su Yang
 *
 * \date 2016/12/20
 */
#ifndef _URM_PACKRESOURCE_H_
#define _URM_PACKRESOURCE_H_

#ifndef _URM_CONFIG_H_
#include "URMConfig.h"
#endif

#ifdef URM_HIERARCHICAL_COMPILE

#ifndef _INCLUDE_BOOST_MAPPEDREGION_HPP_
#include "boost/interprocess/mapped_region.hpp"
#define _INCLUDE_BOOST_MAPPEDREGION_HPP_
#endif

namespace ung
{
	class UnpackHelperInfo;

	/*!
	 * \brief
	 * 包资源
	 * \class PackResource
	 *
	 * \author Su Yang
	 *
	 * \date 2016/12/20
	 *
	 * \todo
	 */
	class PackResource : MemoryPool<PackResource>
	{
	public:
		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param String const& packFullName 包的全名
		*/
		PackResource(String const& packFullName);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~PackResource();

		/*!
		 * \remarks 从包中提取数据
		 * \return 内存中的数据，数据文件的大小
		 * \param const char* fileFullName 具体文件的全名(注意包含包文件的父路径)
		*/
		std::pair<std::shared_ptr<char>, int64> extractData(const char* fileFullName);

	private:
		String mFullName;
		UnpackHelperInfo mHelperInfos;
	};
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

#endif//_URM_PACKRESOURCE_H_