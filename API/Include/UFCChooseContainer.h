/*!
 * \brief
 * 用宏来控制容器的内存分配方式。
 * \file UFCChooseContainer.h
 *
 * \author Su Yang
 *
 * \date 2016/11/22
 */
#ifndef _UFC_CHOOSECONTAINER_H_
#define _UFC_CHOOSECONTAINER_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

/*
在内存整洁的情况下，对使用自定义分配器的标准库容器的测试：
vector会比使用标准库容器的默认分配器多用15% ~ 18%的时间。
list会比使用标准库容器的默认分配器多用4% ~ 5%的时间。
map会比使用标准库容器的默认分配器消耗略少的时间。
考虑到默认分配器将会受到内存碎片的影响，所以需要在游戏完成时，再次进行测试，目前两种选择通过宏来控制。
考虑到在开发过程中可以对自定义内存池的压力测试，所以暂时先使用自定义分配器。
另，std::array不支持指定分配器。std::vector<bool>使用有优化的默认标准库设置。
*/
#ifndef UFC_USE_STL_DEFAULT_ALLOCATOR
#define STL_STRING std::basic_string<char,std::char_traits<char>,ContainerAllocator<char>>
#define STL_VECTOR(T) std::vector<T,ContainerAllocator<T>>
#define STL_DEQUE(T) std::deque<T,ContainerAllocator<T>>
#define STL_LIST(T) std::list<T,ContainerAllocator<T>>
#define STL_FORWARD_LIST(T) std::forward_list<T,ContainerAllocator<T>>
#define STL_SET(K) std::set<K,std::less<K>,ContainerAllocator<K>>
#define STL_MULTISET(K) std::multiset<K,std::less<K>,ContainerAllocator<K>>
#define STL_UNORDERED_SET(K) std::unordered_set<K,std::hash<K>,std::equal_to<K>,ContainerAllocator<K>>
#define STL_UNORDERED_MULTISET(K) std::unordered_multiset<K,std::hash<K>,std::equal_to<K>,ContainerAllocator<K>>
#define STL_MAP(K,T) std::map<K,T,std::less<K>,ContainerAllocator<std::pair<const K,T>>>
#define STL_MULTIMAP(K,T) std::multimap<K,T,std::less<K>,ContainerAllocator<std::pair<const K,T>>>
#define STL_UNORDERED_MAP(K,T) std::unordered_map<K,T,std::hash<K>,std::equal_to<K>,ContainerAllocator<std::pair<const K,T>>>
#define STL_UNORDERED_MAP_H(K,T,H) std::unordered_map<K,T,H,std::equal_to<K>,ContainerAllocator<std::pair<const K,T>>>
#define STL_UNORDERED_MULTIMAP(K,T) std::unordered_multimap<K,T,std::hash<K>,std::equal_to<K>,ContainerAllocator<std::pair<const K,T>>>
#define STL_STACK(T) std::stack<T,STL_DEQUE(T)>
#else
#define STL_STRING std::basic_string<char,std::char_traits<char>,std::allocator<char>>
#define STL_VECTOR(T) std::vector<T>
#define STL_DEQUE(T) std::deque<T>
#define STL_LIST(T) std::list<T>
#define STL_FORWARD_LIST(T) std::forward_list<T>
#define STL_SET(K) std::set<K>
#define STL_MULTISET(K) std::multiset<K>
#define STL_UNORDERED_SET(K) std::unordered_set<K>
#define STL_UNORDERED_MULTISET(K) std::unordered_multiset<K>
#define STL_MAP(K,T) std::map<K,T>
#define STL_MULTIMAP(K,T) std::multimap<K,T>
#define STL_UNORDERED_MAP(K,T) std::unordered_map<K,T>
#define STL_UNORDERED_MAP_H(K,T,H) std::unordered_map<K,T,H>
#define STL_UNORDERED_MULTIMAP(K,T) std::unordered_multimap<K,T>
#define STL_STACK(T) std::stack<T>
#endif//UFC_USE_STL_DEFAULT_ALLOCATOR

/*
对Boost库的指针容器的包装，专门用于说明“容器持有指针，且容器负责释放内存”的语义。
当离开作用域时，会自动调用clear()函数，然后自动释放内存。
*/
#define POINTER_VECTOR(T) boost::ptr_vector<T,BoostPointerContainerClone<T>,ContainerAllocator<T>>
#define POINTER_LIST(T) boost::ptr_list<T,BoostPointerContainerClone<T>,ContainerAllocator<T>>
#define POINTER_DEQUE(T) boost::ptr_deque<T,BoostPointerContainerClone<T>,ContainerAllocator<T>>
#define POINTER_ARRAY(T,N) boost::ptr_array<T,N,BoostPointerContainerClone<T>>
#define POINTER_CIRCULAR(T) boost::ptr_circular_buffer<T,BoostPointerContainerClone<T>,ContainerAllocator<T>>
#define POINTER_SET(K) boost::ptr_set<K,std::less<K>,BoostPointerContainerClone<K>,ContainerAllocator<K>>
#define POINTER_MULTISET(K) boost::ptr_multiset<K,std::less<K>,BoostPointerContainerClone<K>,ContainerAllocator<K>>
#define POINTER_UNORDERED_SET(K) boost::ptr_unordered_set<K,std::hash<K>,std::equal_to<K>,BoostPointerContainerClone<K>,ContainerAllocator<K>>
#define POINTER_UNORDERED_MULTISET(K) boost::ptr_unordered_multiset<K,std::hash<K>,std::equal_to<K>,BoostPointerContainerClone<K>,ContainerAllocator<K>>
#define POINTER_MAP(K,T) boost::ptr_map<K,T,std::less<K>,BoostPointerContainerClone<T>,ContainerAllocator<std::pair<const K,T>>>
#define POINTER_MULTIMAP(K,T) boost::ptr_multimap<K,T,std::less<K>,BoostPointerContainerClone<T>,ContainerAllocator<std::pair<const K,T>>>
#define POINTER_UNORDERED_MAP(K,T) boost::ptr_unordered_multimap<K,T,std::hash<K>,std::equal_to<K>,BoostPointerContainerClone<T>,ContainerAllocator<std::pair<const K,T>>>
#define POINTER_UNORDERED_MULTIMAP(K,T) boost::ptr_unordered_multimap<K,T,std::hash<K>,std::equal_to<K>,BoostPointerContainerClone<T>,ContainerAllocator<std::pair<const K,T>>>
#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_CHOOSECONTAINER_H_