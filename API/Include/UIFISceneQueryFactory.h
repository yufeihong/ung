/*!
 * \brief
 * 场景查询工厂接口
 * \file UIFISceneQueryFactory.h
 *
 * \author Su Yang
 *
 * \date 2017/04/14
 */
#ifndef _UIF_ISCENEQUERYFACTORY_H_
#define _UIF_ISCENEQUERYFACTORY_H_

#ifndef _UIF_CONFIG_H_
#include "UIFConfig.h"
#endif

#ifdef UIF_HIERARCHICAL_COMPILE

namespace ung
{
	/*!
	 * \brief
	 * 场景查询工厂接口
	 * \class ISceneQueryFactory
	 *
	 * \author Su Yang
	 *
	 * \date 2017/04/14
	 *
	 * \todo
	 */
	//template<typename... ArgumentTypes>
	//class ISceneQueryFactory : public MemoryPool<ISceneQueryFactory<DummyType>>
	//{
	//public:
	//	ISceneQueryFactory() = default;

	//	virtual ~ISceneQueryFactory() = default;

	//	virtual StrongISceneQueryPtr createSingleSceneQuery(String const& singleSceneQueryTypeName, ArgumentTypes const&... args) = 0;
	//};
}//namespace ung

#endif//UIF_HIERARCHICAL_COMPILE

#endif//_UIF_ISCENEQUERYFACTORY_H_