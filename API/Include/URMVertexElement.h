/*!
 * \brief
 * 顶点元素
 * \file URMVertexElement.h
 *
 * \author Su Yang
 *
 * \date 2017/05/30
 */
#ifndef _URM_VERTEXELEMENT_H_
#define _URM_VERTEXELEMENT_H_

#ifndef _URM_CONFIG_H_
#include "URMConfig.h"
#endif

#ifdef URM_HIERARCHICAL_COMPILE

#ifndef _URM_ENUMCOLLECTION_H_
#include "URMEnumCollection.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 顶点元素(Describes one component of the vertex.)
	 * \class VertexElement
	 *
	 * \author Su Yang
	 *
	 * \date 2017/05/30
	 *
	 * \todo
	 */
	class UngExport VertexElement : public MemoryPool<VertexElement>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		VertexElement() = delete;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param uint8 stream
		 * \param uint8 offset
		 * \param VertexElementSemantic semantic
		 * \param VertexElementType theType
		 * \param uint8 index
		*/
		VertexElement(uint8 stream,uint8 offset,VertexElementSemantic semantic,VertexElementType theType,uint8 index = 0);

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~VertexElement();

		/*!
		 * \remarks 拷贝构造函数
		 * \return 
		 * \param VertexElement const & rightInst
		*/
		VertexElement(VertexElement const& rightInst) noexcept;

		/*!
		 * \remarks 拷贝赋值运算符
		 * \return 
		 * \param VertexElement const & rightInst
		*/
		VertexElement& operator=(VertexElement const& rightInst) noexcept;

		/*!
		 * \remarks 移动构造函数
		 * \return 
		 * \param VertexElement & & moveInst
		*/
		VertexElement(VertexElement&& moveInst) noexcept;

		/*!
		 * \remarks 移动赋值运算符
		 * \return 
		 * \param VertexElement & & moveInst
		*/
		VertexElement& operator=(VertexElement&& moveInst) noexcept;

		/*!
		 * \remarks 指针容器接口
		 * \return 
		*/
		VertexElement* clone() const;

		/*!
		 * \remarks Gets the vertex buffer index from where this element draws it's values
		 * \return 
		*/
		uint8 getStream() const;

		/*!
		 * \remarks Gets the offset into the buffer where this element starts
		 * \return 
		*/
		uint8 getOffset() const;

		/*!
		 * \remarks Gets the data format of this element
		 * \return 
		*/
		VertexElementType getType() const;

		/*!
		 * \remarks Gets the meaning of this element
		 * \return 
		*/
		VertexElementSemantic getSemantic() const;

		/*!
		 * \remarks Gets the index of this element,only applicable for repeating elements
		 * \return 
		*/
		uint8 getUsageIndex() const;

		/*!
		 * \remarks Gets the size of this element in bytes
		 * \return 
		*/
		uint32 getSize() const;

		/*!
		 * \remarks helping to calculate offsets
		 * \return 
		 * \param VertexElementType etype
		*/
		static uint32 getTypeSize(VertexElementType etype);

		/*!
		 * \remarks returns the count of values in a given type
		 * \return 
		 * \param VertexElementType etype
		*/
		static uint32 getTypeCount(VertexElementType etype);

		/*!
		 * \remarks 重载==
		 * \return 
		 * \param const VertexElement& rightInst
		*/
		bool operator==(const VertexElement& rightInst) const;

		/*!
		 * \remarks 重载!=
		 * \return 
		 * \param const VertexElement& rightInst
		*/
		bool operator!=(const VertexElement& rightInst) const;

	protected:
		//Specifies the stream with which the vertex component is associated.
		uint8 mStream = 0;
		//Used to identify multiple vertex components of the same usage.
		uint8 mUsageIndex = 0;
		/*
		The offset,in bytes,from the start of the vertex structure(起点与偏移针对的是顶点结构) to 
		the start of the vertex component.
		*/
		uint8 mOffset = 0;
		//The meaning of the element(语义)
		VertexElementSemantic mSemantic;
		//Specifies the data type of the vertex element.
		VertexElementType mType;
	};
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

#endif//_URM_VERTEXELEMENT_H_