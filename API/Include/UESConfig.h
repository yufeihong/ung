/*!
 * \brief
 * 配置文件
 * \file UESConfig.h
 *
 * \author Su Yang
 *
 * \date 2016/12/25
 */
#ifndef _UES_CONFIG_H_
#define _UES_CONFIG_H_

#ifndef _UES_ENABLE_H_
#include "UESEnable.h"
#endif

#ifdef UES_HIERARCHICAL_COMPILE

//其它库的头文件
#ifndef _UBS_H_
#include "UBS.h"
#endif

#ifndef _UTM_H_
#include "UTM.h"
#endif

#ifndef _UMM_H_
#include "UMM.h"
#endif

#ifndef _UFC_H_
#include "UFC.h"
#endif

#endif//UES_HIERARCHICAL_COMPILE

#endif//_UES_CONFIG_H_