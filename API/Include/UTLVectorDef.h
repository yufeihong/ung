/*!
 * \brief
 * Vector容器的函数定义。
 * \file UTLVectorDef.h
 *
 * \author Su Yang
 *
 * \date 2016/01/28
 */
#ifndef _UNG_UTL_VECTOR_DEF_H_
#define _UNG_UTL_VECTOR_DEF_H_

#ifndef _UNG_UTL_VECTOR_H_
#include "UTLVector.h"
#endif

#pragma warning(push)
#pragma warning(disable : 4018)				//有符号/无符号不匹配

namespace ung
{
	namespace utl
	{
		template<typename Alloc>
		VectorAllocatorHolder<Alloc>::VectorAllocatorHolder() noexcept(boost::has_nothrow_default_constructor<Alloc>::value) :
			Alloc(),
			mStart(pointer()),
			mSize(0),
			mCapacity(0)
		{
		}

		template<typename Alloc>
		VectorAllocatorHolder<Alloc>::VectorAllocatorHolder(size_type initialSize) :
			Alloc(),
			mStart(pointer()),
			mSize(initialSize),
			mCapacity(initialSize)
		{
			if (mSize)
			{
				mStart = allocate(mSize);
			}
		}

		template<typename Alloc>
		VectorAllocatorHolder<Alloc>::VectorAllocatorHolder(VectorAllocatorHolder<Alloc> const& right) :
			Alloc(),
			mStart(pointer()),
			mSize(right.mSize),
			mCapacity(right.mCapacity)
		{
			mStart = allocate(mCapacity);
		}

		template<typename Alloc>
		VectorAllocatorHolder<Alloc>& VectorAllocatorHolder<Alloc>::operator=(VectorAllocatorHolder<Alloc> const& right)
		{
			if (this == &right)
			{
				return *this;
			}

			if (right.mSize == 0)
			{
				deallocate(mStart, mCapacity);
				mStart = nullptr;
				mCapacity = mSize = 0;

				return *this;
			}

			if (mCapacity < right.mSize)
			{
				deallocate(mStart, mCapacity);
				mSize = right.mSize;
				mCapacity = right.mCapacity;
				mStart = allocate(mCapacity);
			}
			else
			{
				mSize = right.mSize;
			}

			return *this;
		}

		template<typename Alloc>
		VectorAllocatorHolder<Alloc>::VectorAllocatorHolder(VectorAllocatorHolder<Alloc>&& right) :
			Alloc(std::move(right)),
			mStart(std::move(right.mStart)),
			mSize(std::move(right.mSize)),
			mCapacity(std::move(right.mCapacity))
		{
			right.mStart = pointer();
			right.mSize = 0;
			right.mCapacity = 0;
		}

		template<typename Alloc>
		VectorAllocatorHolder<Alloc>& VectorAllocatorHolder<Alloc>::operator=(VectorAllocatorHolder<Alloc>&& right)
		{
			if (this == &right)
			{
				return *this;
			}

			deallocate(mStart, mCapacity);
			mStart = std::move(right.mStart);
			mSize = std::move(right.mSize);
			mCapacity = std::move(right.mCapacity);

			right.mStart = pointer();
			right.mSize = 0;
			right.mCapacity = 0;

			return *this;
		}

		template<typename Alloc>
		VectorAllocatorHolder<Alloc>::~VectorAllocatorHolder() noexcept
		{
			if (mCapacity)
			{
				deallocate(mStart, mCapacity);
				mStart = nullptr;
			}
		}

		template<typename Alloc>
		void VectorAllocatorHolder<Alloc>::adjustCapacity(size_type newCapacity)
		{
			if (newCapacity < mSize)
			{
				newCapacity = mSize;
			}

			if (mCapacity < newCapacity)
			{
				newCapacity *= 2;
				size_type oldCapacity = mCapacity;
				pointer oldStart = mStart;
				mCapacity = newCapacity;
				mStart = allocate(mCapacity);

				if (mSize > 0)
				{
					std::uninitialized_copy_n(oldStart, mSize, mStart);
				}

				deallocate(oldStart, oldCapacity);
			}

			//if (mCapacity > newCapacity)
			//{
			//	deallocate(mStart + newCapacity, mCapacity - newCapacity);
			//}
		}

		template<typename Alloc>
		void VectorAllocatorHolder<Alloc>::shrink()
		{
			if (mCapacity == mSize)
			{
				return;
			}

			deallocate(mStart + mSize, mCapacity - mSize);
			mCapacity = mSize;
		}

		template<typename Alloc>
		typename VectorAllocatorHolder<Alloc>::pointer const& VectorAllocatorHolder<Alloc>::start() const
		{
			return mStart;
		}

		template<typename Alloc>
		typename VectorAllocatorHolder<Alloc>::size_type const& VectorAllocatorHolder<Alloc>::size() const
		{
			return mSize;
		}

		template<typename Alloc>
		void VectorAllocatorHolder<Alloc>::incrementSize(size_type step)
		{
			mSize += step;
		}

		template<typename Alloc>
		void VectorAllocatorHolder<Alloc>::decrementSize(size_type step)
		{
			mSize -= step;
		}

		template<typename Alloc>
		typename VectorAllocatorHolder<Alloc>::size_type const& VectorAllocatorHolder<Alloc>::capacity() const
		{
			return mCapacity;
		}

		template<typename Alloc>
		void VectorAllocatorHolder<Alloc>::swap(VectorAllocatorHolder<Alloc>& right)
		{
			if (this == &right)
			{
				return;
			}

			std::swap(mCapacity, right.mCapacity);
			std::swap(mSize, right.mSize);
			std::swap(mStart, right.mStart);
		}

		//----------------------------------------------------------------------------

		/*
		//模板类型别名:
		template<typename T, typename Alloc = Allocator<T>>
		using TA = std::vector<T,Alloc>;
		BOOST_CONCEPT_ASSERT((boost_concepts::ReadableIteratorConcept<TA<int>::iterator>));											//可读迭代器
		BOOST_CONCEPT_ASSERT((boost_concepts::WritableIteratorConcept<TA<int>::iterator, TA<int>::value_type>));				//可写迭代器
		BOOST_CONCEPT_ASSERT((boost_concepts::SwappableIteratorConcept<TA<int>::iterator>));											//可交换迭代器
		BOOST_CONCEPT_ASSERT((boost_concepts::LvalueIteratorConcept<TA<int>::iterator>));												//左值迭代器
		BOOST_CONCEPT_ASSERT((boost_concepts::IncrementableIteratorConcept<TA<int>::iterator>));									//可递增迭代器
		BOOST_CONCEPT_ASSERT((boost_concepts::SinglePassIteratorConcept<TA<int>::iterator>));											//单遍迭代器
		BOOST_CONCEPT_ASSERT((boost_concepts::ForwardTraversalConcept<TA<int>::iterator>));											//前向迭代器
		BOOST_CONCEPT_ASSERT((boost_concepts::BidirectionalTraversalConcept<TA<int>::iterator>));										//双向迭代器
		BOOST_CONCEPT_ASSERT((boost_concepts::RandomAccessTraversalConcept<TA<int>::iterator>));									//随机访问遍历迭代器

		BOOST_CONCEPT_ASSERT((boost_concepts::ReadableIteratorConcept<TA<int>::const_iterator>));									//可读迭代器
		//BOOST_CONCEPT_ASSERT((boost_concepts::WritableIteratorConcept<TA<int>::const_iterator, TA<int>::value_type>));	//可写迭代器
		//BOOST_CONCEPT_ASSERT((boost_concepts::SwappableIteratorConcept<TA<int>::const_iterator>));								//可交换迭代器
		BOOST_CONCEPT_ASSERT((boost_concepts::LvalueIteratorConcept<TA<int>::const_iterator>));										//左值迭代器
		BOOST_CONCEPT_ASSERT((boost_concepts::IncrementableIteratorConcept<TA<int>::const_iterator>));							//可递增迭代器
		BOOST_CONCEPT_ASSERT((boost_concepts::SinglePassIteratorConcept<TA<int>::const_iterator>));								//单遍迭代器
		BOOST_CONCEPT_ASSERT((boost_concepts::ForwardTraversalConcept<TA<int>::const_iterator>));									//前向迭代器
		BOOST_CONCEPT_ASSERT((boost_concepts::BidirectionalTraversalConcept<TA<int>::const_iterator>));							//双向迭代器
		BOOST_CONCEPT_ASSERT((boost_concepts::RandomAccessTraversalConcept<TA<int>::const_iterator>));						//随机访问遍历迭代器
		*/

		template<typename T,typename Alloc>
		Vector<T, Alloc>::Vector() noexcept :
			mHolder()
		{
		}

		template<typename T, typename Alloc>
		Vector<T, Alloc>::Vector(size_type initialSize) : 
			mHolder(initialSize)
		{
			std::uninitialized_fill_n(begin(), initialSize, std::move(value_type()));
		}

		template<typename T, typename Alloc>
		Vector<T, Alloc>::Vector(size_type initialSize, const_reference val) :
			mHolder(initialSize)
		{
			std::uninitialized_fill_n(begin(),initialSize,val);
		}

		template<typename T, typename Alloc>
		Vector<T, Alloc>::Vector(self_const_reference right) : 
			mHolder(right.mHolder)
		{
			std::uninitialized_copy_n(right.begin(), right.mHolder.size(), begin());
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::self_reference Vector<T, Alloc>::operator=(self_const_reference right)
		{
			mHolder = right.mHolder;
			std::uninitialized_copy_n(right.begin(), right.mHolder.size(), begin());

			return *this;
		}

		template<typename T, typename Alloc>
		Vector<T, Alloc>::Vector(self_reference_r right) :
			mHolder(std::move(right.mHolder))
		{
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::self_reference Vector<T, Alloc>::operator=(self_reference_r right)
		{
			mHolder = std::move(right.mHolder);

			return *this;
		}

		template<typename T, typename Alloc>
		template<typename iteratorT>
		Vector<T, Alloc>::Vector(iteratorT first, iteratorT last, typename boost::enable_if<has_type_iterator_category<iteratorT>>::type *) :
			mHolder(last - first)
		{
			std::uninitialized_copy(first, last, begin());
		}

		template<typename T, typename Alloc>
		Vector<T, Alloc>::Vector(std::initializer_list<value_type> il) :
			mHolder(il.end() - il.begin())
		{
			std::uninitialized_copy(il.begin(), il.end(), begin());
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::self_reference Vector<T, Alloc>::operator=(std::initializer_list<value_type> il)
		{
			mHolder = std::move(allocator_holder(il.end() - il.begin()));
			std::uninitialized_copy(il.begin(), il.end(), begin());

			return *this;
		}

		template<typename T, typename Alloc>
		template<typename iteratorT>
		void Vector<T, Alloc>::assign(iteratorT first, iteratorT last, typename boost::enable_if<has_type_iterator_category<iteratorT>>::type *)
		{
			reserve(last - first);
			std::uninitialized_copy(first, last, begin());
			mHolder.incrementSize(last - first);
		}

		template<typename T, typename Alloc>
		void Vector<T, Alloc>::assign(std::initializer_list<value_type> il)
		{
			reserve(il.end() - il.begin());
			std::uninitialized_copy(il.begin(), il.end(), begin());
			mHolder.incrementSize(il.end() - il.begin());
		}

		template<typename T, typename Alloc>
		void Vector<T, Alloc>::assign(size_type count, const_reference val)
		{
			reserve(count);
			std::uninitialized_fill_n(begin(), count, val);
			mHolder.incrementSize(count);
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::iterator Vector<T, Alloc>::begin()
		{
			return mHolder.start();
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::const_iterator Vector<T, Alloc>::begin() const
		{
			return cbegin();
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::const_iterator Vector<T, Alloc>::cbegin() const
		{
			return mHolder.start();
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::reverse_iterator Vector<T, Alloc>::rbegin()
		{
			return reverse_iterator(mHolder.start() + mHolder.size());
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::const_reverse_iterator Vector<T, Alloc>::rbegin() const
		{
			return crbegin();
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::const_reverse_iterator Vector<T, Alloc>::crbegin() const
		{
			return const_reverse_iterator(mHolder.start() + mHolder.size());
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::iterator Vector<T, Alloc>::end()
		{
			return mHolder.start() + mHolder.size();
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::const_iterator Vector<T, Alloc>::end() const
		{
			return cend();
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::const_iterator Vector<T, Alloc>::cend() const
		{
			return mHolder.start() + mHolder.size();
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::reverse_iterator Vector<T, Alloc>::rend()
		{
			return reverse_iterator(mHolder.start());
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::const_reverse_iterator Vector<T, Alloc>::rend() const
		{
			return crend();
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::const_reverse_iterator Vector<T, Alloc>::crend() const
		{
			return const_reverse_iterator(mHolder.start());
		}

		template<typename T, typename Alloc>
		bool Vector<T, Alloc>::empty() const
		{
			return !(mHolder.size());
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::size_type Vector<T, Alloc>::size() const
		{
			return mHolder.size();
		}

		template<typename T, typename Alloc>
		void Vector<T, Alloc>::resize(size_type newSize)
		{
			if (newSize == mHolder.size())
			{
				return;
			}
			else if (newSize > mHolder.size())
			{
				reserve(newSize);
				std::uninitialized_fill_n(end(), newSize - mHolder.size(), std::move(value_type()));
				mHolder.incrementSize(newSize - mHolder.size());
			}
			else
			{
				priv_resize_destructor(newSize, boost::has_trivial_destructor<T>());
				mHolder.decrementSize(mHolder.size() - newSize);
			}
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::size_type Vector<T, Alloc>::capacity() const
		{
			return mHolder.capacity();
		}

		template<typename T, typename Alloc>
		void Vector<T, Alloc>::reserve(size_type newSize)
		{
			mHolder.adjustCapacity(newSize);
		}

		template<typename T, typename Alloc>
		void Vector<T, Alloc>::shrink_to_fit()
		{
			mHolder.shrink();
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::reference Vector<T, Alloc>::front()
		{
			BOOST_ASSERT(mHolder.size());
			return *begin();
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::const_reference Vector<T, Alloc>::front() const
		{
			BOOST_ASSERT(mHolder.size());
			return *begin();
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::reference Vector<T, Alloc>::back()
		{
			BOOST_ASSERT(mHolder.size());
			return *(end() - 1);
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::const_reference Vector<T, Alloc>::back() const
		{
			BOOST_ASSERT(mHolder.size());
			return *(end() - 1);
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::reference Vector<T, Alloc>::operator[](size_type pos)
		{
			BOOST_ASSERT(mHolder.size() > pos);
			return *(begin() + pos);
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::const_reference Vector<T, Alloc>::operator[](size_type pos) const
		{
			BOOST_ASSERT(mHolder.size() > pos);
			return *(begin() + pos);
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::reference Vector<T, Alloc>::at(size_type pos)
		{
			if (pos >= mHolder.size())
			{
				//抛出异常
			}
			return *(begin() + pos);
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::const_reference Vector<T, Alloc>::at(size_type pos) const
		{
			if (pos >= mHolder.size())
			{
				//抛出异常
			}
			return *(begin() + pos);
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::pointer Vector<T, Alloc>::data()
		{
			return mHolder.start();
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::const_pointer Vector<T, Alloc>::data() const
		{
			return mHolder.start();
		}

		template<typename T, typename Alloc>
		template<typename ...Arguments>
		void Vector<T, Alloc>::emplace_back(Arguments&& ...args)
		{
			reserve(mHolder.size() + 1);
			::new(mHolder.start() + mHolder.size()) T(std::forward<Arguments>(args)...);
			mHolder.incrementSize();
		}

		template<typename T, typename Alloc>
		template<typename ...Arguments>
		typename Vector<T, Alloc>::iterator Vector<T, Alloc>::emplace(const_iterator where, Arguments && ...args)
		{
			difference_type dif = where - cbegin();
			BOOST_ASSERT(dif >= 0 && dif <= mHolder.size());
			emplace_back(std::forward<Arguments>(args)...);
			/*
			template<class ForwardIterator>
			void rotate(ForwardIterator _First,ForwardIterator _Middle,ForwardIterator _Last):
			Exchanges the elements in two adjacent ranges.
			_First,A forward iterator addressing the position of the first element in the range to be rotated.
			_Middle,A forward iterator defining the boundary within the range that addresses the position of the first element in the second part of the range 
			whose elements are to be exchanged with those in the first part of the range.
			_ Last,A forward iterator addressing the position one past the final element in the range to be rotated.
			*/
			std::rotate(begin() + dif, end() - 1, end());
			return iterator(boost::next(begin(),dif).getPointer());
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::iterator Vector<T, Alloc>::insert(const_iterator where, size_type count, const_reference val)
		{
			difference_type dif = where - cbegin();
			BOOST_ASSERT(dif >= 0 && dif <= mHolder.size());
			reserve(mHolder.size() + count);
			std::uninitialized_fill_n(end(), count, val);
			mHolder.incrementSize(count);
			std::rotate(begin() + dif, end() - count, end());
			return iterator(boost::next(begin(), dif).getPointer());
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::iterator Vector<T, Alloc>::insert(const_iterator where, const_reference val)
		{
			return insert(where,1,val);
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::iterator Vector<T, Alloc>::insert(const_iterator where, reference_r val)
		{
			return insert(where, 1, std::move(val));
		}

		template<typename T, typename Alloc>
		template<typename iteratorT>
		typename boost::enable_if<typename Vector<T, Alloc>::has_type_iterator_category<iteratorT>, iteratorT>::type
		Vector<T, Alloc>::insert(const_iterator where, iteratorT first, iteratorT last)
		{
			difference_type dif = where - cbegin();
			BOOST_ASSERT(dif >= 0 && dif <= mHolder.size());
			size_type count = last - first;
			reserve(mHolder.size() + count);
			std::uninitialized_copy(first, last, end());
			mHolder.incrementSize(count);
			std::rotate(begin() + dif, end() - count, end());

			return iteratorT(boost::next(begin(), dif).getPointer());
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::iterator Vector<T, Alloc>::insert(const_iterator where, std::initializer_list<value_type> il)
		{
			difference_type dif = where - cbegin();
			BOOST_ASSERT(dif >= 0 && dif <= mHolder.size());
			size_type count = il.end() - il.begin();
			reserve(mHolder.size() + count);
			std::uninitialized_copy(il.begin(), il.end(), end());
			mHolder.incrementSize(count);
			std::rotate(begin() + dif, end() - count, end());

			return iterator(boost::next(begin(), dif).getPointer());
		}

		template<typename T, typename Alloc>
		void Vector<T, Alloc>::push_back(const_reference val)
		{
			reserve(mHolder.size() + 1);
			*end() = val;
			mHolder.incrementSize();
		}

		template<typename T, typename Alloc>
		void Vector<T, Alloc>::push_back(reference_r val)
		{
			reserve(mHolder.size() + 1);
			*end() = std::move(val);
			mHolder.incrementSize();
		}

		template<typename T, typename Alloc>
		void Vector<T, Alloc>::pop_back()
		{
			resize(mHolder.size() - 1);
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::iterator Vector<T, Alloc>::erase(const_iterator where)
		{
			difference_type dif = where - cbegin();
			BOOST_ASSERT(dif >= 0 && dif <= mHolder.size());
			std::rotate(begin() + dif,begin() + dif + 1,end());
			resize(mHolder.size() - 1);

			return begin() +dif;
		}

		template<typename T, typename Alloc>
		typename Vector<T, Alloc>::iterator Vector<T, Alloc>::erase(const_iterator first, const_iterator last)
			{
				BOOST_ASSERT(last - cbegin() > first - cbegin());
				difference_type dif = std::distance(cbegin(), first);
				difference_type len = std::distance(first, last);
				std::rotate(begin() + dif, begin() + dif + len, end());
				resize(mHolder.size() - len);
				return begin() +dif;
			}

		template<typename T, typename Alloc>
		void Vector<T, Alloc>::clear()
		{
			resize(0);
			mHolder.adjustCapacity(0);
		}

		template<typename T, typename Alloc>
		void Vector<T, Alloc>::swap(self_reference right)
		{
			mHolder.swap(right.mHolder);
		}

		//---------------------------------------------------------------------------

		template<typename T, typename Alloc>
		void Vector<T, Alloc>::priv_resize_destructor(size_type newSize, boost::true_type)
		{
			priv_resize_delete(newSize, boost::is_pointer<T>());
		}

		template<typename T, typename Alloc>
		void Vector<T, Alloc>::priv_resize_destructor(size_type newSize, boost::false_type)
		{
			iterator first = begin() + newSize;
			iterator last = begin() + mHolder.size();
			while (first != last)
			{
				/*
				后置递增运算符的优先级高于解引用运算符，因此*first++等价于*(first++)。first++把first的值加1，然后返回first的初始值的副本作为其求值
				结果，此时解引用运算符的运算对象是first未增加之前的值。
				或者(*first++).~T();
				*/
				(first++)->~T();
			}
		}

		template<typename T, typename Alloc>
		void Vector<T, Alloc>::priv_resize_delete(size_type newSize, boost::true_type)
		{
			iterator first = begin() + newSize;
			iterator last = begin() + mHolder.size();
			while (first != last)
			{
				delete(*first);
				*first++ = nullptr;
			}
		}

		template<typename T, typename Alloc>
		void Vector<T, Alloc>::priv_resize_delete(size_type newSize, boost::false_type)
		{
		}

		//--------------------------------------------------------------------------------------

		template<typename T, typename Alloc>
		void swap(Vector<T, Alloc>& left, Vector<T, Alloc>& right)
		{
			left.swap(right);
		}

		template<typename T, typename Alloc>
		bool operator==(const Vector<T, Alloc>& left, const Vector<T, Alloc>& right)
		{
			return (left.size() == right.size() && std::equal(left.begin(), left.end(), right.begin()));
		}

		template<typename T, typename Alloc>
		bool operator!=(const Vector<T, Alloc>& left, const Vector<T, Alloc>& right)
		{
			return (!(left == right));
		}

		template<typename T, typename Alloc>
		bool operator<(const Vector<T, Alloc>& left, const Vector<T, Alloc>& right)
		{
			return (std::lexicographical_compare(left.begin(), left.end(), right.begin(), right.end()));
		}

		template<typename T, typename Alloc>
		bool operator>(const Vector<T, Alloc>& left, const Vector<T, Alloc>& right)
		{
			return (right < left);
		}

		template<typename T, typename Alloc>
		bool operator<=(const Vector<T, Alloc>& left, const Vector<T, Alloc>& right)
		{
			return (!(right < left));
		}

		template<typename T, typename Alloc>
		bool operator>=(const Vector<T, Alloc>& left, const Vector<T, Alloc>& right)
		{
			return (!(left < right));
		}
	}//namespace utl
}//namespace ung

#pragma warning(pop)

#endif//_UNG_UTL_VECTOR_DEF_H_

 /*
 uninitialized_fill(beg,end,val):
 以val初始化[beg,end)。
 uninitialized_fill_n(beg,num,val):
 以val初始化beg开始的num个元素。
 uninitialized_copy(beg,end,mem):
 以[beg,end)之各元素初始化自mem开始的各个元素。
 uninitialized_copy_n(beg,num,mem):
 以始自beg的num个元素初始化自mem开始的元素。
 */