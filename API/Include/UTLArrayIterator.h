/*!
 * \brief
 * 数组容器的迭代器。
 * \file UTLArrayIterator.h
 *
 * \author Su Yang
 *
 * \date 2016/01/27
 */
#ifndef _UNG_UTL_ARRAYITERATOR_H_
#define _UNG_UTL_ARRAYITERATOR_H_

#ifndef _UNG_UTL_CONFIG_H_
#include "UTLConfig.h"
#endif

namespace ung
{
	namespace utl
	{
		using ArrayIteratorTag = boost::random_access_traversal_tag;												//或者std::random_access_iterator_tag

		/*
		The user of iterator_facade derives his iterator class from a specialization of iterator_facade and passes the derived iterator class as 
		iterator_facade's first template parameter. The order of the other template parameters have been carefully chosen to take advantage of 
		useful defaults. For example,when defining a constant lvalue iterator, the user can pass a const-qualified(const限定) version of the 
		iterator's value_type as iterator_facade's Value parameter and omit the Reference parameter which follows.

		In addition to implementing the core interface functions,an iterator derived from iterator_facade typically defines several constructors.To 
		model any of the standard iterator concepts, the iterator must at least have a copy constructor. Also, if the iterator type X is meant to be 
		automatically interoperate with another iterator type Y (as with constant and mutable iterators) then there must be an implicit conversion 
		from X to Y or from Y to X (but not both), typically implemented as a conversion constructor. Finally, if the iterator is to model Forward 
		Traversal Iterator or a more-refined iterator concept, a default constructor is required.
		*/
		/*!
		 * \brief
		 * 数组容器的迭代器。
		 * \class ArrayIterator
		 *
		 * \author Su Yang
		 *
		 * \date 2016/01/27
		 *
		 * \todo
		 */
		template<typename T>
		class ArrayIterator : public boost::iterator_facade<ArrayIterator<T>, T, ArrayIteratorTag>
		{
			/*
			Iterator Core Access:
			iterator_facade and the operator implementations need to be able to access the core member functions in the derived class. Making 
			the core member functions public would expose an implementation detail to the user. The design used here ensures that 
			implementation details do not appear in the public interface of the derived iterator type.
			Preventing direct access to the core member functions has two advantages. First, there is no possibility for the user to accidently use 
			a member function of the iterator when a member of the value_type was intended. This has been an issue with smart pointer 
			implementations in the past. The second and main advantage is that library implementers can freely exchange a hand-rolled iterator 
			implementation for one based on iterator_facade without fear of breaking code that was accessing the public core member functions 
			directly.
			In a naive implementation, keeping the derived class' core member functions private would require it to grant friendship to 
			iterator_facade and each of the seven operators. In order to reduce the burden of limiting access, iterator_core_access is provided, a 
			class that acts as a gateway to the core member functions in the derived iterator class. The author of the derived class only needs to 
			grant friendship to iterator_core_access to make his core member functions available to the library.
			iterator_core_access will be typically implemented as an empty class containing only private static member functions which invoke the 
			iterator core member functions. There is, however, no need to standardize the gateway protocol. Note that even if iterator_core_access 
			used public member functions it would not open a safety loophole, as every core member function preserves the invariants of the 
			iterator.
			*/
			friend class boost::iterator_core_access;

		public:
			using value_type = T;
			using self_type = ArrayIterator<value_type>;
			using base_type = boost::iterator_facade<self_type, value_type, ArrayIteratorTag>;
			using iterator_category = ArrayIteratorTag;
			
			using reference = typename boost::add_lvalue_reference<value_type>::type;					//T&;
			BOOST_STATIC_ASSERT(boost::is_lvalue_reference<reference>::value);
			using pointer = typename boost::add_pointer<value_type>::type;									//T*;
			BOOST_STATIC_ASSERT(boost::is_pointer<pointer>::value);
			using difference_type = std::ptrdiff_t;

			/*!
			 * \remarks 默认构造函数
			 * \return 
			*/
			ArrayIterator() :
				mPointer(nullptr)
			{
			}

			/*!
			 * \remarks 构造函数
			 * \return 
			 * \param pointer ptr 指向数组容器元素的指针
			*/
			explicit ArrayIterator(pointer ptr) :
				mPointer(ptr)
			{
			}

			/*
			To model any of the standard iterator concepts, the iterator must at least have a copy constructor.
			*/
			/*!
			 * \remarks 拷贝构造函数
			 * \return 
			 * \param self_type const & other 
			*/
			ArrayIterator(self_type const& other) :
				mPointer(other.mPointer)
			{
			}

			/*!
			 * \remarks 拷贝赋值运算符
			 * \return void
			 * \param self_type const & other 
			*/
			void operator=(self_type const& other)
			{
				mPointer = other.mPointer;
			}

		private:
			/*
			Used to implement Iterator Concept(s):
			dereference():
			Readable Iterator, Writable Iterator.

			increment():
			Incrementable Iterator.
			equal():
			Single Pass Iterator.
			difference_type类型定义，distance_to()，迭代器缺省构造函数:
			Forward Traversal Iterator.
			decrement():
			Bidirectional Traversal Iterator.
			advance(),distance_to():
			Random Access Traversal Iterator.
			*/			

			reference dereference() const
			{
				return *mPointer;
			}

			void increment()
			{
				++mPointer;
			}

			bool equal(self_type const& other) const
			{
				return mPointer == other.mPointer;
			}

			void decrement()
			{
				--mPointer;
			}

			difference_type distance_to(self_type const& other) const
			{
				return other.mPointer - mPointer;
			}

			void advance(difference_type n)
			{
				mPointer += n;
			}

		private:
			pointer mPointer;
		};//ArrayIterator
		BOOST_CONCEPT_ASSERT((boost::DefaultConstructible<ArrayIterator<int>>));				//缺省构造函数
		BOOST_CONCEPT_ASSERT((boost::CopyConstructible<ArrayIterator<int>>));				//拷贝构造函数
		BOOST_CONCEPT_ASSERT((boost::EqualityComparable<ArrayIterator<int>>));				//相等比较
	}//namespace utl
}//namespace ung

#endif//_UNG_UTL_ARRAYITERATOR_H_

/*
迭代器模式：
迭代器模式有两个基本的参与者：聚合和迭代器。
聚合定义了元素的集合方式，它对用户是不透明的，但对迭代器开放了访问接口，允许迭代器访问。
迭代器依赖于聚合，它对外提供遍历和访问聚合的接口，这样用户无需关心聚合的内部结构就能够以任意的方式访问聚合里的元素。
聚合和迭代器是彼此独立的，这意味着它们都可以是多态的（包括静态多态和动态多态），而且在一个聚合上可以执行多个不同的迭代，一个迭代器也可以同时对多
个聚合执行迭代。
除了聚合和迭代器，迭代器模式还可能有其它参与者，例如迭代器产生器，它产生基于某个聚合的某个访问方式的迭代器。迭代器产生器可以是一个单独的工厂类，
也可以是聚合或者迭代器的某个工厂方法。
为了访问和遍历聚合，迭代器必须具备四个操作接口：First、Next、IsDone和CurrentItem，分别用于执行迭代器初始化、前进、检测是否完成迭代和访问当前元
素。基本接口以外迭代器还可以拥有更多的接口以提供更强大更方便的访问和遍历功能。以std::vector和vector::iterator来举例：vector定义了一个元素的聚合，
其内部实现对用户来说是不透明的（虽然大多数情况下是一个原生数组），vector::iterator是基于这个聚合的一个迭代器，它可以正向遍历vector。vector的成员
函数begin()/end()是迭代器产生器，可以产生聚合上的迭代器，同时它们还对应迭代器的First和IsDone操作，确定了迭代器的起点和终点。vector::iterator重载
了operator++和operator*，可以在聚合上前进和访问聚合里的元素，对应迭代器的Next和CurrentItem操作。

标准迭代器：
C++标准中将迭代器分为五类，它们的特征简要描述如下：
1：输入迭代器。或称“只读迭代器”（从迭代器输入），只提供operator++，可以执行相等比较。
2：输出迭代器。或称“只写迭代器”（向迭代器输出），只提供operator++，没有相等比较功能。
3：前向迭代器。可以读写，只提供operator++，可以执行相等比较和赋值操作。
4：双向迭代器。在前向迭代器的基础上增加了operator--，也就是说可以执行后退操作。
5：随机访问迭代器。在双向迭代器的基础上增加了迭代器的算术运算功能，提供operator[]和operator+=。
对于标准容器来说，
std::forward_list的迭代器是前向迭代器，
std::list,std::set,std::map的迭代器都是双向迭代器，
内建数组，std::vector,std::deque,std::string的迭代器则是随机访问迭代器。
为了区分不同类型的迭代器，标准库使用一些tag类（空类）作为迭代器的标签。

新式迭代器：
iterator库定义了一组基于标准库的新的迭代器概念、构造框架和有用的适配器，能够更轻松地应用迭代器模式来创建、使用迭代器类型。它针对标准库的不足，区分
了迭代器的值访问概念和遍历概念，重新划分了迭代器的类型。
根据迭代器的值访问概念，迭代器可分为以下四类：
1：可读迭代器。提供operator*，返回可转换为类型T的右值。
2：可写迭代器。提供operator*，可以执行赋值操作（不一定是左值）。
3：可交换迭代器。两个迭代器所指的值可用标准库的iter_swap()函数交换，即同时满足可读迭代器和可写迭代器的要求。
4：左值迭代器。满足可交换迭代器，并且operator*可返回左值，即一个类型T的引用。
根据迭代器的遍历概念，迭代器可分为以下五类（下面的迭代器概念均在前一个的基础上递增定义）：
1：可递增迭代器。提供operator++，可拷贝构造和赋值。
2：单遍迭代器。增加operator==、operator!=比较操作。
3：前向遍历迭代器。增加difference_type类型定义，可计算迭代器的距离，可缺省构造。
4：双向遍历迭代器。增加operator--，可以逆向遍历。
5：随机访问遍历迭代器。增加迭代器的算术运算和比较运算，并提供operator[]。
使用Boost的新式迭代器分类，标准库原有的五个迭代器类别就可以更精确地定义如下：
1：输入迭代器				= 可读迭代器 + 单遍迭代器
2：输出迭代器				= 可写迭代器 + 可递增迭代器
3：前向迭代器				= 左值迭代器 + 前向遍历迭代器
4：双向迭代器				= 左值迭代器 + 双向遍历迭代器
5：随机访问迭代器			= 左值迭代器 + 随机访问遍历迭代器
而标准库中“不是容器的容器”vector<bool>的迭代器（它返回一个代理而不是bool）也就可以被正确归类为可交换迭代器（可读迭代器 + 可写迭代器） + 随机访问
遍历迭代器。

iterator_traits:
iterator_traits库提供了标准的元函数来访问迭代器的基本属性，较标准库的std::iterator_traits而言，它更加规范，更容易被用在泛型编程和模板元编程中。

迭代器特征类：
标准库提供std::iterator_traits<>来获得迭代器（或指针）的属性，可以获得迭代器（指针）所必备的五个类型信息：
1：iterator_category:		迭代器的分类
2：value_type:				迭代器所指的值类型
3：reference:					迭代器的值引用类型
4：pointer:						迭代器的指针类型
5：difference_type:			迭代器的距离类型
std::iterator_traits<>是非标准元函数。
iterator_traits库把std::iterator_traits<>中被“揉成一团”的元数据分解开来，形成了五个标准元函数，而功能则是完全相同的：
1：iterator_category<I>:			返回迭代器的分类
2：iterator_value<I>:				返回迭代器所指的值类型
3：iterator_reference<I>:			返回迭代器的值引用类型
4：iterator_pointer<I>:			返回迭代器的指针类型
5：iterator_difference<I>:		返回迭代器的距离类型
实际上，这五个元函数没有做任何自己的工作，只是调用了std::iterator_traits<>，把非标准的内部类型定义转化成了标准的::type返回。
用法：
std::cout << typeid(boost::iterator_category<boost::array<int, 1>::iterator>::type).name() << std::endl;

iterator_facade:
iterator_facade（迭代器外观）是iterator库的一个重要组件，它使用外观模式提供一个辅助类，能够更容易地创建符合标准的迭代器。iterator_facade定义了数个
迭代器的核心接口，用户只需要实现这些核心功能就可以编写正确且完备的迭代器。
迭代器的核心操作：
一个完备的迭代器拥有相当多的外部接口和内部的类型定义，但它存在一个必需的核心操作集合，这个集合定义了迭代器的必需功能。
iterator_facade要求用户的迭代器类必须实现下面的五个功能（共六个接口，但依据迭代器的类型某些可以不实现）：
1：解引用：			dereference() const，实现可读迭代器和可写迭代器必需
2：相等比较：		equal() const，实现单遍迭代器必需
3：递增：			increment()，实现可递增迭代器和前向遍历迭代器必需
4：递减：			decrement()，实现双向遍历迭代器必需
5：距离计算：		advance()和distance_to() const，实现随机访问遍历迭代器必需
这些核心操作将被iterator_facade用来实现迭代器的外部接口，所以这些函数通常应该是private的。为了使iterator_facade能够访问这些核心操作函数，库又提供
了一个辅助类boost::iterator_core_access，它定义了若干静态成员函数可以访问private核心操作，用户迭代器需要把它声明为友元。
此外，因为迭代器通常需要拷贝和赋值，故，用户自定义迭代器还需要有拷贝构造函数和缺省构造函数。
iterator_facade类摘要：
template<
	typename Derived,							//迭代器子类
	typename Value,								//迭代器值类型
	typename CategoryOrTraversal,			//迭代器的分类
	typename Reference = Value&,			//迭代器的值引用类型
	typename Difference = ptrdiff_t			//迭代器的距离类型
	>
class iterator_facade
{
public:
	//迭代器各种必需的类型定义
	typedef remove_const<Value>::type		value_type;
	typedef Reference								reference;
	typedef Value*									pointer;
	typedef Difference								difference_type;
	typedef some_define

	//迭代器的各种操作定义
	Reference			operator*() const;
	some_define		operator->() const;
	some_define		operator[](difference_type n) const;
	Derived&			operator++();
	Derived				operator++(int);
	Derived&			operator--();
	Derived				operator--(int);
	Derived&			operator+=(difference_type n);
	Derived&			operator-=(difference_type n);
	Derived				operator-(difference_type n) const;
};
...				//许多关系运算符定义
iterator_facade基于用户迭代器的六个核心操作实现了数个迭代器接口。它有五个模板参数，但后两个可以使用缺省参数，我们通常只需要关心前三个。
Derived:是子类的名字，也就是用户正在编写的自己的迭代器（即基类链技术）。
Value:是迭代器的值类型。
CategoryOrTraversal:是迭代器的分类，它既可以使用标准分类也可以使用新式分类。
由于CategoryOrTraversal的使用比较复杂，下面把它的取值单独列出来：
std::input_iterator_tag								标准的输入迭代器
std::output_iterator_tag							标准的输出迭代器
std::forward_iterator_tag							标准的前向迭代器
std::bidirectional_iterator_tag					标准的双向迭代器
std::random_access_iterator_tag				标准的随机访问迭代器

boost::incrementable_traversal_tag			可递增遍历迭代器
boost::single_pass_traversal_tag				单遍迭代器
boost::forward_traversal_tag					前向遍历迭代器
boost::bidirectional_traversal_tag				双向遍历迭代器
boost::random_access_traversal_tag			随机访问遍历迭代器
使用iterator_facade之前，我们必须要规划自己的迭代器的几个基本特征，包括迭代器的名称、在何种集合上迭代、迭代的对象（解引用的类型）和迭代遍历的类型。
首先，迭代器要用public的方式继承iterator_facade，同时指定iterator_facade的模板参数。
接下来，我们要考虑迭代器的构造函数：迭代器必须能够拷贝构造和赋值（可递增迭代器概念），如果是前向遍历迭代器则还需要缺省构造函数。构造函数通常需要
传入被迭代的集合对象。还要有一个成员变量来保存迭代的位置，这样我们才能执行递增或递减操作。
最后，我们就可以实现迭代器所必需的核心操作函数了。
*/