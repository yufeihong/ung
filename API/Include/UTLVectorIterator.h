/*!
 * \brief
 * UTL容器Vector的迭代器。
 * \file UTLVectorIterator.h
 *
 * \author Su Yang
 *
 * \date 2016/01/28
 */
#ifndef _UNG_UTL_VECTORITERATOR_H_
#define _UNG_UTL_VECTORITERATOR_H_

#ifndef _UNG_UTL_CONFIG_H_
#include "UTLConfig.h"
#endif

namespace ung
{
	namespace utl
	{
		using VectorIteratorTag = std::random_access_iterator_tag;

		/*
		const and an iterator's value_type:
		The C++ standard requires an iterator's value_type not be const-qualified, so iterator_facade strips the const from its Value parameter in
		order to produce the iterator's value_type. Making the Value argument const provides a useful hint to iterator_facade that the iterator is a
		constant iterator, and the default Reference argument will be correct for all lvalue iterators.
		iterator_facade中源代码:
		typedef remove_const<Value>::type value_type;
		*/
		/*!
		 * \brief
		 * Vector容器的迭代器。
		 * \class VectorIterator
		 *
		 * \author Su Yang
		 *
		 * \date 2016/01/28
		 *
		 * \todo
		 */
		template<typename T>
		class VectorIterator : public boost::iterator_facade<VectorIterator<T>, T, VectorIteratorTag>
		{
			friend class boost::iterator_core_access;

		public:
			using value_type = T;
			using self_type = VectorIterator<value_type>;
			using base_type = boost::iterator_facade<self_type, value_type, VectorIteratorTag>;
			using iterator_category = VectorIteratorTag;

			using reference = typename iterator_facade::reference;//typename boost::add_lvalue_reference<value_type>::type;
			BOOST_STATIC_ASSERT(boost::is_lvalue_reference<reference>::value);
			/*
			Constant and Mutable iterators:
			The term mutable iterator means an iterator through which the object it references (its "referent") can be modified. A constant iterator
			is one which doesn't allow modification of its referent.
			The words constant and mutable don't refer to the ability to modify the iterator itself. For example, an int const* is a non-const
			constant iterator, which can be incremented but doesn't allow modification of its referent, and int* const is a const mutable iterator,
			which cannot be modified but which allows modification of its referent.
			*/
			//using pointer = typename boost::mpl::if_c<C, value_type const*, value_type*>::type;
			using pointer = typename iterator_facade::pointer;//value_type*;
			BOOST_STATIC_ASSERT(boost::is_pointer<pointer>::value);
			using difference_type = std::ptrdiff_t;

			//BOOST_STATIC_ASSERT(boost::is_same<typename base_type::value_type, value_type>::value);
			BOOST_STATIC_ASSERT(boost::is_same<typename base_type::reference, reference>::value);
			BOOST_STATIC_ASSERT(boost::is_same<typename base_type::pointer, value_type*>::value);

			//is_pointer认为std::shared_ptr和boost::shared_ptr都不是指针。
			BOOST_STATIC_ASSERT(!boost::is_pointer<std::shared_ptr<value_type>>::value);
			BOOST_STATIC_ASSERT(!boost::is_pointer<boost::shared_ptr<value_type>>::value);

			//using value_type_remove_pointer = typename boost::remove_pointer<value_type>::type;

			/*!
			 * \remarks 默认构造函数
			 * \return 
			 * \param pointer const & p 指向容器元素类型的指针
			*/
			VectorIterator(pointer const& p = nullptr) :
				mPointer(p)
			{
			}

			/*!
			 * \remarks 拷贝构造函数
			 * \return 
			 * \param self_type const & other 
			*/
			VectorIterator(self_type const& other) :
				mPointer(other.getPointer())
			{
			}

			/*!
			 * \remarks 拷贝赋值运算符
			 * \return void
			 * \param self_type const & other 
			*/
			void operator=(self_type const& other)
			{
				mPointer = other.getPointer();
			}

			//VectorIterator(VectorIterator<value_type const> const& other) :
			//	mPointer(other.getPointer())
			//{
			//}

			/*!
			 * \remarks 返回迭代器包装的指针
			 * \return pointer const& 常量指针
			*/
			pointer const& getPointer() const
			{
				return mPointer;
			}

			/*!
			 * \remarks 同上
			 * \return pointer& 引用
			*/
			pointer& getPointer()
			{
				return mPointer;
			}

		private:
			reference dereference() const
			{
				return *mPointer;
			}

			void increment()
			{
				++mPointer;
			}

			bool equal(self_type const& other) const
			{
				return mPointer == other.getPointer();
			}

			void decrement()
			{
				--mPointer;
			}

			difference_type distance_to(self_type const& other) const
			{
				return other.getPointer() - mPointer;
			}

			void advance(difference_type n)
			{
				mPointer += n;
			}

		private:
			pointer mPointer;
		};

		/*!
		 * \brief
		 * Vector容器的常量迭代器。
		 * \class VectorIterator
		 *
		 * \author Su Yang
		 *
		 * \date 2016/01/28
		 *
		 * \todo
		 */
		template<typename T>
		class VectorConstIterator : public boost::iterator_facade<VectorConstIterator<T>, const T, VectorIteratorTag>			//注意第二个模板参数
		{
			friend class boost::iterator_core_access;

		public:
			using value_type = T;
			using self_type = VectorConstIterator<value_type>;
			using iterator = VectorIterator<value_type>;//VectorIterator<typename boost::remove_const<value_type>::type>;
			using iterator_category = VectorIteratorTag;
			using reference = typename iterator_facade::reference;
			using pointer = typename iterator_facade::pointer;
			using difference_type = std::ptrdiff_t;

			/*!
			 * \remarks 默认构造函数
			 * \return 
			 * \param pointer p 指向容器元素类型的指针
			*/
			VectorConstIterator(pointer p = nullptr) :
				mPointer(p)
			{
			}

			/*!
			 * \remarks 通过非常量迭代器来构造常量迭代器
			 * \return 
			 * \param iterator const& it
			*/
			VectorConstIterator(iterator const& it) :
				mPointer(it.getPointer())
			{
			}

			/*!
			 * \remarks 拷贝构造函数
			 * \return 
			 * \param self_type const & other 
			*/
			VectorConstIterator(self_type const& other) :
				mPointer(other.getPointer())
			{
			}

			/*!
			 * \remarks 拷贝赋值运算符
			 * \return void
			 * \param self_type const & other 
			*/
			void operator=(self_type const& other)
			{
				mPointer = other.getPointer();
			}

			/*!
			 * \remarks 返回迭代器包装的指针
			 * \return
			*/
			pointer getPointer() const
			{
				return mPointer;
			}

		private:
			reference dereference() const
			{
				return *mPointer;
			}

			void increment()
			{
				++mPointer;
			}

			bool equal(self_type const& other) const
			{
				return mPointer == other.getPointer();
			}

			void decrement()
			{
				--mPointer;
			}

			difference_type distance_to(self_type const& other) const
			{
				return other.getPointer() - mPointer;
			}

			void advance(difference_type n)
			{
				mPointer += n;
			}

		private:
			pointer mPointer;
		};
	}//namespace utl
}//namespace ung

#endif//_UNG_UTL_VECTORITERATOR_H_
