/*!
 * \brief
 * 对GPU/AGP Buffer的抽象接口
 * \file UIFIVideoBuffer.h
 *
 * \author Su Yang
 *
 * \date 2017/05/30
 */
#ifndef _UIF_IVIDEOBUFFER_H_
#define _UIF_IVIDEOBUFFER_H_

#ifndef _UIF_CONFIG_H_
#include "UIFConfig.h"
#endif

#ifdef UIF_HIERARCHICAL_COMPILE

namespace ung
{
	/*!
	 * \brief
	 * 对GPU/AGP Buffer的抽象接口
	 * \class IVideoBuffer
	 *
	 * \author Su Yang
	 *
	 * \date 2017/05/30
	 *
	 * \todo
	 */
	class UngExport IVideoBuffer : public MemoryPool<IVideoBuffer>
	{
	public:
		enum Usage : uint8
		{
			/*
			一旦创建了,应用程序就很少去更改.
			读取video memory很慢。

			Static buffers are generally placed in video memory where their contents can be 
			processed most efficiently.However,once in video memory we cannot efficiently access 
			the data,that is to say,accessing video memory is very slow.For this reason we use 
			static buffers to hold static data (data that will not need to be changed (accessed) very 
			frequently).Terrains and city buildings are examples of good candidates for static 
			buffers because the terrain and building geometry usually does not change during the 
			course of the application.Static buffers should be filled once with geometry at 
			application initialization time and never at run time.
			(通常被放置在video memory里面。适用于地形，建筑。)

			Lock一个static buffer(该static buffer正在被GPU所使用)对效率有影响，特别是在一帧中lock好
			几次。
			*/
			VB_USAGE_STATIC = 1,

			/*
			标明应用程序会经常性的让CPU去更改这块硬件buffer.
			以这个flag创建的硬件buffer,会存在于AGP(速度介于system memory和video memory之间,其实
			还是内存的一块)中。读取AGP依然很慢。
			如果没有指定VB_USAGE_DYNAMIC，那么buffer就是VB_USAGE_STATIC的。
			VB_USAGE_DYNAMIC is strictly enforced through the VB_LOCK_DISCARD and 
			VB_LOCK_NO_OVERWRITE locking flags.As a result,VB_LOCK_DISCARD and 
			VB_LOCK_NO_OVERWRITE are valid only on vertex buffers created with 
			VB_USAGE_DYNAMIC.They are not valid flags on static vertex buffers.
			VB_USAGE_DYNAMIC和RPT_MANAGED不兼容，不能一起使用。

			Dynamic buffers are generally placed in AGP memory where their memory can be 
			transferred over to the graphics card quickly.Dynamic buffers are not processed as 
			quickly as static buffers because the data must be transferred to video memory before 
			rendering,but the benefit of dynamic buffers is that they can be updated reasonably 
			fast (fast CPU writes).Therefore,if you need to update the contents of a buffer 
			frequently,it should be made dynamic.Particle systems are good candidates for dynamic 
			buffers because they are animated,and thus their geometry is usually updated every 
			frame.
			(通常被放置在AGP里面，AGP里面的内容可以很快速地被输送到video memory。可以使CPU进
			行快速地写操作。适用于粒子(因为粒子是animated)。)

			Dynamic vertex buffers with the VB_USAGE_WRITE_ONLY flag are placed in video 
			memory,and dynamic vertex buffers without the VB_USAGE_WRITE_ONLY flag are 
			placed in AGP memory.
			(如果和VB_USAGE_WRITE_ONLY标记一起使用，那么通常被放置在video memory里面)

			This usage flag causes UNG to optimize for frequent lock operations.
			VB_USAGE_DYNAMIC is only useful when the buffer is locked frequently.Data that 
			remains constant should be placed in a static vertex or index buffer.
			*/
			VB_USAGE_DYNAMIC = 2,

			/* 
			总是返回一个指向新的，空的缓存的pointer。
			避免DMA(Direct Memory Access,它允许不同速度的硬件装置来沟通,而不需要依于CPU的大量中断
			负载) stalls，因为你可以写入到一个新的缓存当上一个正在被使用的时候。

			Informs the system that the application writes only to the vertex buffer. Using this flag 
			enables the driver to choose the best memory location for efficient write operations 
			and rendering. Attempts to read from a vertex buffer that is created with this capability 
			will fail. Buffers created with RPT_DEFAULT that do not specify 
			VB_USAGE_WRITE_ONLY may suffer a severe performance penalty. 
			VB_USAGE_WRITE_ONLY only affects the performance of RPT_DEFAULT buffers.
			(使用VB_USAGE_WRITE_ONLY标记可以让驱动程序去选择最好的memory location，以便于写和
			渲染操作能够更高效。尝试读取，会失败。使用标记RPT_DEFAULT(资源内存池类型)创建的缓冲
			区，如果没有指定VB_USAGE_WRITE_ONLY标记的话，可能会遭受到严重的性能损失。
			VB_USAGE_WRITE_ONLY标记所带来的性能提高，仅适用于通过RPT_DEFAULT标记所创建的缓
			冲区)
			*/
			VB_USAGE_WRITE_ONLY = 4,

			/*
			标明应用程序会定期重新填充缓冲区的内容.不是更新，而是从无到有,从头开始的产生，因此不在意
			内容是否lost。这个flag只有和VB_USAGE_DYNAMIC_WRITE_ONLY结合使用的时候,才有意义.
			*/
			VB_USAGE_DISCARDABLE = 8,

			/*
			用于标记一个不能包含clipping information的顶点缓冲区。
			仅当你指出顶点缓冲区里面的数据是transformed，那么可以使用该标记。
			如果顶点缓冲区里面的数据是untransformed，那么即使你使用了该标记，该标记也会被忽略掉。
			使用VB_USAGE_DONOTCLIP标记可以略少占用memory。
			*/
			VB_USAGE_DONOTCLIP = 16
		};

		/*
		缓存的构造和加锁之间存在相互作用,这种作用对执行效果有着重要的影响.下面给出一些相关提示:

		1.理论上最"完美"的缓存,是那种通过VB_USAGE_STATIC_WRITE_ONLY类型创建,而且不创建自身的
		备份缓存,并且只进行一次VB_LOCK_DISCARD的锁操作用于传入数据.之后永远不要去再次动它.

		2.如果你需要频繁更新缓存,就要对执行性能进行一些妥协.这时候使用
		VB_USAGE_DYNAMIC_WRITE_ONLY(强烈建议使用
		VB_USAGE_DYNAMIC_WRITE_ONLY_DISCARDABLE)类型来创建缓存(仍然不要创建相应的备份
		缓存),之后通过VB_LOCK_DISCARD来进行全部缓存的锁操作,或者你不能对其全部加锁,那么可以考
		虑通过VB_LOCK_NO_OVERWRITE来锁住部分缓存.

		3.如果你真的需要从缓存中读取数据的话,建立一个备份缓存.并确认通过VB_LOCK_READ_ONLY类型
		锁来进行读取,因为这样可以禁止在解锁的时候上载数据.你也可以把这一条联合上面两点来作为新的条
		款,在可能的时候尽量把缓存设为静态的——请记住"_WRITE_ONLY"后缀只是针对真实的硬件缓存,如
		果拥有备份缓存,你仍然可以进行安全的读取操作.

		4.在对顶点的不同元素需要不同的使用模式的时候,把这些顶点缓存元素拆分成不同的部分.如果你只需
		要更新纹理坐标,那么就把纹理坐标单独放到一个缓存区域,然后把其他的元素拆分放置在
		VB_USAGE_STATIC_WRITE_ONLY类型的缓存区域中.

		Locking a static vertex buffer while the graphics processor is using the buffer can have a
		significant performance penalty(惩罚).
		*/

		enum LockOptions : uint32
		{
			/*
			这种类型的"锁"允许read/write缓存数据，内容被保留——这也是效率最差的。
			*/
			VB_LOCK_NORMAL = 0,

			/*
			使用这种类型的"锁"意味着你希望在每次操作时把缓存中所有的内容都丢弃.一般这种操作也是会在
			每帧都处理的环境下才会使用,它是禁止读出的.但是一旦我们如此声明,也就基本上是向引擎宣称我
			们不会对缓冲区的内容感兴趣,那么就不应当创建备份缓冲区.意味着对显卡的当前渲染操作不会有任
			何性能影响,因为你已经承诺提供全新的数据.
			只允许用于创建时使用VB_USAGE_DYNAMIC的缓存。
			不要对VB_LOCK_DISCARD类型的指针进行读操作。
			VB_LOCK_DISCARD is a valid lock flag for dynamic textures.

			The application discards all memory within the locked region.For vertex and index 
			buffers,the entire buffer will be discarded.This option is only valid when the resource is 
			created with dynamic usage.

			VB_LOCK_DISCARD indicates that the application does not need to keep the old vertex 
			or index data in the buffer.If the graphics processor is still using the buffer when lock 
			is called with VB_LOCK_DISCARD,a pointer to a new region of memory is returned 
			instead of the old buffer data.This allows the graphics processor to continue using the 
			old data while the application places data in the new buffer.
			Note that locking a buffer with VB_LOCK_DISCARD always discards the entire buffer,
			specifying a nonzero offset or limited size field does not preserve(保存，保持) 
			information in unlocked areas of the buffer.

			The VB_LOCK_DISCARD flag is valid only on buffers created with VB_USAGE_DYNAMIC.
			*/
			VB_LOCK_DISCARD = 1,

			/*
			使用这种类型的"锁"意味着你只能从缓存中读取内容.使用这种操作的时候最好建立备份缓存,因为这
			样就能避免真正的从显卡中读取数据.
			只允许用于创建时没有使用VB_USAGE_DYNAMIC的缓存，只能用于静态缓存。
			不要对VB_LOCK_READ_ONLY类型的指针进行写操作。
			*/
			VB_LOCK_READ_ONLY = 2,

			/*
			除了应用程序保证不覆盖当前帧使用的缓存的任何区域，其他的和VB_LOCK_DISCARD一样。
			当我们有些时候需要更新部分缓冲区而非全部缓冲区时,使用VB_LOCK_DISCARD就显的不适合了.
			此时我们就需要使用这种锁了,它的效率依旧很高。
			这种类型的"锁"只在没有使用备份缓存的时候才有用处.
			只允许用于创建时使用VB_USAGE_DYNAMIC的缓存。
			As VB_LOCK_DISCARD,except the application guarantees not to overwrite any region 
			of the buffer which has already been used in this frame,can allow some optimisation on 
			some APIs.

			Indicates that memory that was referred to in a drawing call since the last lock without 
			this flag will not be modified during the lock. This can enable optimizations when the 
			application is appending data to a resource. Specifying this flag enables the driver to 
			return immediately if the resource is in use, otherwise, the driver must finish using the 
			resource before returning from locking.

			There are cases where the amount of data the application needs to store per lock is 
			small,such as adding four vertices to render a sprite.VB_LOCK_NO_OVERWRITE 
			indicates that the application will not overwrite data already in use in the dynamic 
			buffer.The lock call will return a pointer to the old data,allowing the application to add 
			new data in unused regions of the vertex or index buffer.The application should not 
			modify vertices or indices used in a draw operation as they might still be in use by the 
			graphics processor.The application should then use VB_LOCK_DISCARD after the 
			dynamic buffer is full to receive a new region of memory,discarding the old vertex or 
			index data after the graphics processor is finished.

			The VB_LOCK_NO_OVERWRITE flag is valid only on buffers created with 
			VB_USAGE_DYNAMIC.
			*/
			VB_LOCK_NO_OVERWRITE = 4
		};

	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		IVideoBuffer() = default;

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~IVideoBuffer() = default;

		/*!
		 * \remarks Lock the entire buffer for reading / writing(lock整个表面)
		 * \return 
		 * \param LockOptions options
		*/
		virtual void* lock(LockOptions options) PURE;

		/*!
		 * \remarks Lock the buffer for reading / writing(如果要lock整个表面的话，offset和length应该都设置为0)
		 * \return 
		 * \param uint32 offset
		 * \param uint32 length 要lock的区域的size，单位：字节
		 * \param LockOptions options
		*/
		virtual void* lock(uint32 offset, uint32 length, LockOptions options) PURE;

		/*!
		 * \remarks Releases the lock on this buffer
		 * 当缓存处于locked的状态，这个时候去切换video mode，会抛出异常，必须重新upload数据，如果
		 * 要100%确保数据不会lost，那么就使用readData和writeData。
		 * \return 
		*/
		virtual void unlock() PURE;

		/*!
		 * \remarks Reads data from the buffer and places it in the memory pointed to by pDest
		 * \return 
		 * \param uint32 offset
		 * \param uint32 length
		 * \param void* pDest
		*/
		virtual void readData(uint32 offset,uint32 length,void* pDest) PURE;

		/*!
		 * \remarks Writes data to the buffer from an area of system memory
		 * \return 
		 * \param uint32 offset The byte offset from the start of the buffer to start writing
		 * \param uint32 length
		 * \param const void* pSource
		 * \param bool discardWholeBuffer 建议为true，这样就允许驱动在写入数据的时候丢弃整个缓存，就可以避免DMA stalls
		*/
		virtual void writeData(uint32 offset,uint32 length,const void* pSource,bool discardWholeBuffer = true) PURE;

		/*!
		 * \remarks Copy data from another buffer into this one
		 * \return 
		 * \param IVideoBuffer& srcBuffer 源，不能为通过VB_USAGE_WRITE_ONLY创建的
		 * \param uint32 srcOffset
		 * \param uint32 dstOffset
		 * \param uint32 length
		 * \param bool discardWholeBuffer If true,will discard the entire contents of this buffer before copying
		*/
		virtual void copyData(IVideoBuffer& srcBuffer, uint32 srcOffset, uint32 dstOffset, uint32 length,bool discardWholeBuffer = true) PURE;

		/*!
		 * \remarks Copy all data from another buffer into this one
		 * \return 
		 * \param IVideoBuffer& srcBuffer
		*/
		virtual void copyData(IVideoBuffer& srcBuffer) PURE;

		/*!
		 * \remarks Updates the real buffer from the shadow buffer,if required.
		 * \return 
		*/
		virtual void updateFromShadow() PURE;

		/*!
		 * \remarks 获取缓存的大小，单位：字节
		 * \return 
		*/
		virtual uint32 getSizeInBytes() const PURE;

		/*!
		 * \remarks 获取创建缓存时的Usage flags
		 * \return 
		*/
		virtual uint32 getUsage() const PURE;

		/*!
		 * \remarks 这个缓存是否有shadow
		 * \return 
		*/
		virtual bool hasShadowBuffer() const PURE;

		/*!
		 * \remarks 当前的状态是否为locked
		 * \return 
		*/
		virtual bool isLocked() const PURE;

	public:
		virtual void* lockImpl(uint32 offset, uint32 length, LockOptions options) PURE;

		virtual void unlockImpl() PURE;
	};
}//namespace ung

#endif//UIF_HIERARCHICAL_COMPILE

#endif//_UIF_IVIDEOBUFFER_H_