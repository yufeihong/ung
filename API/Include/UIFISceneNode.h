/*!
 * \brief
 * 场景图节点接口。
 * \file UIFISceneNode.h
 *
 * \author Su Yang
 *
 * \date 2016/11/25
 */
#ifndef _UIF_ISCENENODE_H_
#define _UIF_ISCENENODE_H_

#ifndef _UIF_CONFIG_H_
#include "UIFConfig.h"
#endif

#ifdef UIF_HIERARCHICAL_COMPILE

#ifndef _UIF_IMOVABLE_H_
#include "UIFIMovable.h"
#endif

#ifndef _INCLUDE_STLIB_FUNCTIONAL_
#include <functional>
#define _INCLUDE_STLIB_FUNCTIONAL_
#endif

namespace ung
{
	/*!
	 * \brief
	 * 场景图节点接口
	 * \class ISceneNode
	 *
	 * \author Su Yang
	 *
	 * \date 2016/11/25
	 *
	 * \todo
	 */
	class UngExport ISceneNode : public MemoryPool<ISceneNode>,public std::enable_shared_from_this<ISceneNode>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		ISceneNode() = default;

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~ISceneNode() = default;

		/*!
		 * \remarks 获取当前节点的名字
		 * \return 
		*/
		virtual String const& getName() const = 0;

		/*!
		 * \remarks 获取当前节点的父节点。
		 * \return 
		*/
		virtual WeakISceneNodePtr getParent() const = 0;

		/*!
		 * \remarks 渲染前行为
		 * \return 
		*/
		virtual void preRender() = 0;

		/*!
		 * \remarks 渲染
		 * \return 
		*/
		virtual void render() = 0;

		/*!
		 * \remarks 渲染后行为
		 * \return 
		*/
		virtual void postRender() = 0;

		//------------------------------------------------------------------------------------------

		/*!
		 * \remarks 获取所有的（直接）孩子数量
		 * \return 
		*/
		virtual uint32 getChildrenCount() = 0;

		/*!
		 * \remarks 获取所有的（直接和非直接）孩子数量
		 * \return 
		*/
		virtual uint32 getFamilyCount() = 0;

		/*!
		 * \remarks 从父节点脱离
		 * \return 
		*/
		virtual void detachFromParent() = 0;

		/*!
		 * \remarks 关联到父节点
		 * \return 
		 * \param StrongISceneNodePtr parentPtr
		*/
		virtual void attachToParent(StrongISceneNodePtr parentPtr) = 0;

		//------------------------------------------------------------------------------------------

		/*!
		 * \remarks 关联对象
		 * \return 
		 * \param StrongIObjectPtr objectPtr
		*/
		virtual void attachObject(StrongIObjectPtr objectPtr) = 0;

		/*!
		 * \remarks 分离对象
		 * \return 
		 * \param StrongIObjectPtr objectPtr
		 * \param bool remove true:从容器中移除对象
		*/
		virtual void detachObject(StrongIObjectPtr objectPtr,bool remove = true) = 0;

		/*!
		 * \remarks 获取(直接)关联在该节点下的对象数量
		 * \return 
		*/
		virtual uint32 getAttachedObjectCount() = 0;

		/*!
		 * \remarks 获取(直接和非直接)关联在该节点下的对象数量
		 * \return 
		*/
		virtual uint32 getAttachedAllObjectCount() = 0;

		//------------------------------------------------------------------------------------------

		/*!
		 * \remarks 必须update后才可正常使用
		 * \return 
		*/
		virtual void update() = 0;

		/*!
		 * \remarks 获取可运动的属性对象
		 * \return 
		*/
		virtual IMovable& getMovableAttribute() = 0;

		/*!
		 * \remarks 获取可运动的属性对象
		 * \return 
		*/
		virtual IMovable const& getMovableAttribute() const = 0;

		/*!
		 * \remarks 存储子节点
		 * \return 
		 * \param StrongISceneNodePtr childPtr
		*/
		virtual void saveChild(StrongISceneNodePtr childPtr) = 0;

		/*!
		 * \remarks 移除子节点
		 * \return 
		 * \param StrongISceneNodePtr childPtr
		*/
		virtual void removeChild(StrongISceneNodePtr childPtr) = 0;

		/*!
		 * \remarks 设置父节点
		 * \return 
		 * \param WeakISceneNodePtr parentPtr
		*/
		virtual void setParent(WeakISceneNodePtr parentPtr) = 0;

		/*!
		 * \remarks 该场景节点是否为根节点
		 * \return 
		*/
		virtual bool isRoot() const = 0;

		/*!
		 * \remarks 获取节点所关联的直接对象的数量
		 * \return 
		*/
		virtual uint32 getObjectsSize() const = 0;

		/*!
		 * \remarks 通知所有的子孙节点需要更新
		 * \return 
		*/
		virtual void notifyDescendant() = 0;

		/*!
		 * \remarks 所关联的直接对象需要更新
		 * \return 
		*/
		virtual void notifyObjects() = 0;

		/*!
		 * \remarks 重新计算该节点的world
		 * \return 
		*/
		virtual void buildWorld() = 0;
	};
}//namespace ung

#endif//UIF_HIERARCHICAL_COMPILE

#endif//_UIF_ISCENENODE_H_

/*
ISceneNode的强引用，仅由其父节点持有。SceneManager中的节点池持有的是弱引用。

创建节点的两种方式：
1：在场景管理器中进行创建。
2：创建当前节点的子节点。
*/