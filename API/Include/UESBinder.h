/*!
 * \brief
 * 为function object提供一个operator ==运算符
 * \file UESBinder.h
 *
 * \author Su Yang
 *
 * \date 2016/12/25
 */
#ifndef _UES_BINDER_H_
#define _UES_BINDER_H_

#ifndef _UES_CONFIG_H_
#include "UESConfig.h"
#endif

#ifdef UES_HIERARCHICAL_COMPILE

#ifndef _INCLUDE_FASTDELEGATE_H_
#include "FastDelegate.h"
#define _INCLUDE_FASTDELEGATE_H_
#endif

#endif//UES_HIERARCHICAL_COMPILE

#endif//_UES_BINDER_H_