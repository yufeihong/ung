/*!
 * \brief
 * 像素矩形
 * \file URMPixelRectangle.h
 *
 * \author Su Yang
 *
 * \date 2017/05/04
 */
#ifndef _URM_PIXELRECTANGLE_H_
#define _URM_PIXELRECTANGLE_H_

#ifndef _URM_CONFIG_H_
#include "URMConfig.h"
#endif

#ifdef URM_HIERARCHICAL_COMPILE

namespace ung
{
	/*!
	 * \brief
	 * 像素矩形
	 * \class PixelRectangle
	 *
	 * \author Su Yang
	 *
	 * \date 2017/05/04
	 *
	 * \todo
	 */
	class UngExport PixelRectangle : MemoryPool<PixelRectangle>
	{
	public:
		/*!
		 * \remarks 默认构造函数(整个表面)
		 * \return 
		*/
		PixelRectangle();

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param int32 minx
		 * \param int32 miny
		 * \param int32 maxx
		 * \param int32 maxy
		*/
		PixelRectangle(int32 minx, int32 miny, int32 maxx, int32 maxy);

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~PixelRectangle();

		/*!
		 * \remarks 设置指向表面内存开始处的指针
		 * \return 
		 * \param void * pBits
		*/
		void setStart(void* pBits);

		/*!
		 * \remarks 获取指向表面内存开始处的指针
		 * \return 
		*/
		void* getStart() const;

		/*!
		 * \remarks 获取min x
		 * \return 
		*/
		int32 getMinX() const;

		/*!
		 * \remarks 获取min y
		 * \return 
		*/
		int32 getMinY() const;

		/*!
		 * \remarks 获取max x
		 * \return 
		*/
		int32 getMaxX() const;

		/*!
		 * \remarks 获取max y
		 * \return 
		*/
		int32 getMaxY() const;

		/*!
		 * \remarks 设置min x
		 * \return 
		 * \param int32 x
		*/
		void setMinX(int32 x);

		/*!
		 * \remarks 设置min y
		 * \return 
		 * \param int32 y
		*/
		void setMinY(int32 y);

		/*!
		 * \remarks 设置max x
		 * \return 
		 * \param int32 x
		*/
		void setMaxX(int32 x);

		/*!
		 * \remarks 设置max y
		 * \return 
		 * \param int32 y
		*/
		void setMaxY(int32 y);

		/*!
		 * \remarks 是否为整个表面
		 * \return 
		*/
		bool isEntire() const;

	private:
		void* mStart;
		bool mEntire;
		int32 mMinX;
		int32 mMinY;
		int32 mMaxX;
		int32 mMaxY;
	};
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

#endif//_URM_PIXELRECTANGLE_H_