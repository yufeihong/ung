/*!
 * \brief
 * 窗口消息工具
 * \file URMWindowEventUtilities.h
 *
 * \author Su Yang
 *
 * \date 2017/05/11
 */
#ifndef _URM_WINDOWEVENTUTILITIES_H_
#define _URM_WINDOWEVENTUTILITIES_H_

#ifndef _URM_CONFIG_H_
#include "URMConfig.h"
#endif

#ifdef URM_HIERARCHICAL_COMPILE

#ifndef WIN32_LEAN_AND_MEAN
#if UNG_PLATFORM == UNG_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#if !defined(NOMINMAX) && defined(_MSC_VER)
#define NOMINMAX																					//required to stop windows.h messing up std::min
#endif
#include <windows.h>
#endif
#endif

namespace ung
{
	/*!
	 * \brief
	 * 窗口消息工具
	 * \class WindowEventUtilities
	 *
	 * \author Su Yang
	 *
	 * \date 2017/05/11
	 *
	 * \todo
	 */
	class UngExport WindowEventUtilities : public MemoryPool<WindowEventUtilities>
	{
	public:
#if UNG_PLATFORM == UNG_PLATFORM_WIN32
		/*!
		 * \remarks 处理消息响应
		 * \return 
		 * \param HWND hWnd 接收消息的窗口句柄
		 * \param UINT msg 消息
		 * \param WPARAM wParam 指定其余的，消息特定的信息。该参数的内容与msg参数值有关
		 * \param LPARAM lParam 指定其余的，消息特定的信息。该参数的内容与msg参数值有关
		*/
		static LRESULT UNG_STDCALL windowProc(HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam);
#endif

		/*!
		 * \remarks 消息泵
		 * \return 
		*/
		static void messagePump();
	};
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

#endif//_URM_WINDOWEVENTUTILITIES_H_