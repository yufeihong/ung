/*!
 * \brief
 * 打包。支持空目录，嵌套目录，中文目录，中文文件名。支持大文件。支持加密。
 * \file UFCPack.h
 *
 * \author Su Yang
 *
 * \date 2016/12/20
 */
#ifndef _UFC_PACk_H_
#define _UFC_PACk_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#ifndef _UFC_CHOOSECONTAINER_H_
#include "UFCChooseContainer.h"
#endif

//文件设备
#ifndef _INCLUDE_BOOST_DEVICEFILEDESCRIPTOR_HPP_
#include "boost/iostreams/device/file_descriptor.hpp"
#define _INCLUDE_BOOST_DEVICEFILEDESCRIPTOR_HPP_
#endif

namespace ung
{
	/*!
	 * \brief
	 * 打包
	 * \class Pack
	 *
	 * \author Su Yang
	 *
	 * \date 2016/12/20
	 *
	 * \todo
	 */
	class UngExport Pack : public MemoryPool<Pack>
	{
	public:
		/*!
		 * \remarks 
		 * \return 
		 * \param String const & packDir 要打包的目录
		 * \param String const & packName 生成的包文件名
		*/
		Pack(String const& packDir,String const& packName);

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~Pack();

		/*!
		 * \remarks 对目录进行打包(支持迭代目录)
		 * \return 
		*/
		void packToDisk();

	private:
		/*!
		 * \remarks 把名字块插入到zip文件的头部
		 * \return 
		 * \param boost::iostreams::file_descriptor_sink& snk
		 * \param String& names
		*/
		void nameBlockToStream(boost::iostreams::file_descriptor_sink& snk,String& names);

		/*!
		 * \remarks 把真正的压缩数据插入到zip文件中
		 * \return 
		 * \param boost::iostreams::file_descriptor_sink& snk
		 * \param std::shared_ptr<STL_VECTOR(String)> fileFullNamesPtr
		 * \param std::vector<std::tuple<unsigned int,int64,unsigned int>& items
		*/
		void compressedBlockToStream(boost::iostreams::file_descriptor_sink& snk,
			std::shared_ptr<STL_VECTOR(String)> fileFullNamesPtr,
			std::vector<std::tuple<unsigned int, int64, unsigned int,unsigned int,int64>>& items,
			std::vector<unsigned int>& sectionsSize);

		/*!
		 * \remarks 把每段的大小数据插入到zip文件中
		 * \return 
		 * \param boost::iostreams::file_descriptor_sink & snk
		 * \param std::vector<unsigned int> const & sectionsSize
		*/
		void sectionsBlockToStream(boost::iostreams::file_descriptor_sink& snk,
			std::vector<unsigned int> const& sectionsSize);

		/*!
		 * \remarks 把条目数据插入到zip文件中
		 * \return 
		 * \param boost::iostreams::file_descriptor_sink& snk
		 * \param std::vector<std::tuple<unsigned int,int64,unsigned int,unsigned int,int64>& items 全名的字符数，原始大小，在section块中的第一个索引,段数，在压缩数据中的偏移
		*/
		void itemsBlockToStream(boost::iostreams::file_descriptor_sink& snk,
			std::vector<std::tuple<unsigned int, int64, unsigned int,unsigned int,int64>> const& items);

		/*!
		 * \remarks 把一个unsigned int给插入到一个char输出流里
		 * \return 
		 * \param boost::iostreams::file_descriptor_sink& snk
		 * \param unsigned int ui32
		*/
		void ui32ToStream(boost::iostreams::file_descriptor_sink& snk, unsigned int ui32);

		/*!
		 * \remarks 把一个64位unsigned int给插入到一个char输出流里
		 * \return 
		 * \param boost::iostreams::file_descriptor_sink & snk
		 * \param int64 ui64
		*/
		void int64ToStream(boost::iostreams::file_descriptor_sink& snk, int64 ui64);
	private:
		String mPackDir;
		String mPackName;
	};
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_PACk_H_

/*
boost文件映射，如果文件过大会抛出异常。
实测：1G没有问题。
那么，待压缩的所有文件总字节数不能超过1G。

解决方案：
pack时，如果单个文件大于1G，则必须分段来映射，这样就没有问题。
unpack时，如果总压缩包大于1G：
1：如果pack时没有采用分段映射，则此时可以分段映射压缩包来获取记录和数据信息。
2：如果pack时采用了分段映射，那么必须修改代码逻辑，将记录信息分开存放，否则解压缩是必然要采用
分段映射，这时解压缩逻辑无法知道对源文件分段和压缩文件分段的对应信息。
*/