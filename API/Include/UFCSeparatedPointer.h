/*!
 * \brief
 * 创建和删除分离的指针
 * \file UFCSeparatedPointer.h
 *
 * \author Su Yang
 *
 * \date 2017/06/04
 */
#ifndef _UFC_SEPARATEDPOINTER_H_
#define _UFC_SEPARATEDPOINTER_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#ifndef _UFC_METAFUNCTION_H_
#include "UFCMetaFunction.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 创建和删除分离的指针
	 * \class SeparatedPointer
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/04
	 *
	 * \todo
	 */
	template<typename T>
	class SeparatedPointer : public MemoryPool<SeparatedPointer<T>>
	{
	public:
		template<typename U>
		friend class SeparatedPointer;

		using pointee = T;
		using pointer = pointee*;
		using reference = pointee&;
		using dispatch_type = typename boost::mpl::if_c <
			boost::has_new_operator<pointee>::value,
			boost::true_type, boost::false_type > ::type;

		/*
		没有通过构造函数和析构函数来实现RAII(在构造函数中new，在析构函数中delete)的原因：
		因为，不一定就需要new，有的情况就是需要指针为nullptr(nullptr用于某种标志)，这种情况下，如
		果把new给写到构造函数中的话，就不可避免的进行了new操作，那么如果在构造函数中不进行new操
		作，而仅把指针初始化为nullptr的话，当另外一种情况下，需要new操作时，就必须手动调用create()
		函数，当然，这时候可以通过析构函数来自动释放，但是问题是，已经手动调用了create()函数，却无
		需手动调用destroy()来释放的话，显得“创建”和“销毁”接口不匹配。
		但是，毕竟手动调用destroy()，可能存在忘记的情况，所以，干脆在析构函数中做一个判断，如果忘记
		手动调用destroy()，就在析构函数中来进行调用。那么，为了和析构函数匹配，就添加一个默认的构造
		函数，来把指针给设置为空。把指针在构造函数中设置为空，那么也有可能，当确实需要new操作时，
		忘记，或者误认为在构造函数中已经进行了new操作，所以，在解引用和调用运算符函数中，进行了空
		指针判断。但是get()函数中没有进行空指针的assert判断，这是因为如果确实需要的是空指针的话，有
		了assert判断，就会抛出异常。所以，该模板工具的使用风险，仅在对get出来的原始指针进行使用，不
		过，不要紧，因为原始指针已经在构造函数中给初始化为nullptr了，所以，这里不存在“悬指针”的风险，
		只是如果通过get()函数得到一个nullptr指针，然用这个nullptr指针去做调用操作的话，编译器自然会
		抛出一个“异常”，或者说会“崩”掉。
		*/

		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		SeparatedPointer() :
			mPointer(nullptr)
		{
		}

		/*!
		 * \remarks 析构函数(这里无需virtual)
		 * \return 
		*/
		~SeparatedPointer()
		{
			//对空指针进行delete操作，相当于没有任何操作
			destroy();
		}

		/*!
		 * \remarks 拷贝构造函数
		 * \return 
		 * \param SeparatedPointer & copyInst
		*/
		SeparatedPointer(SeparatedPointer& copyInst) = delete;

		/*!
		 * \remarks 拷贝赋值运算符
		 * \return 
		 * \param SeparatedPointer & copyInst
		*/
		SeparatedPointer& operator=(SeparatedPointer& copyInst) = delete;

		/*!
		 * \remarks 移动构造函数
		 * \return 
		 * \param SeparatedPointer & & moveInst
		*/
		SeparatedPointer(SeparatedPointer&& moveInst) noexcept :
		mPointer(moveInst._transfer())
		{
		}

		/*!
		 * \remarks 移动赋值运算符
		 * \return 
		 * \param SeparatedPointer & & moveInst
		*/
		SeparatedPointer& operator=(SeparatedPointer&& moveInst) noexcept
		{
			if (this != &moveInst)
			{
				mPointer = moveInst._transfer();
			}

			return *this;
		}

		/*!
		 * \remarks 支持子类到基类的转换
		 * \return 
		 * \param SeparatedPointer<U> & subInst
		*/
		template<typename U>
		SeparatedPointer(SeparatedPointer<U>& subInst)
		{
			BOOST_STATIC_ASSERT(UFC_SUPERSUBCLASS_STRICT(T, U));

			/*
			这里不能使用subInst.get()，因为这样的话，紧接着subInst就会被析构，从而导致
			SeparatedPointer<T>::mPointer成为了一个悬指针。
			*/
			mPointer = subInst._transfer();
		}

		/*!
		 * \remarks 支持子类到基类的转换
		 * \return 
		 * \param SeparatedPointer & subInst
		*/
		template<typename U>
		SeparatedPointer& operator=(SeparatedPointer<U>& subInst)
		{
			BOOST_STATIC_ASSERT(UFC_SUPERSUBCLASS_STRICT(T, U));

			if (this != &subInst)
			{
				mPointer = subInst._transfer();
			}

			return *this;
		}

		/*!
		 * \remarks 创建(可变参数模板)
		 * \return 
		 * \param Args const & ... args
		*/
		template<typename... Args>
		void create(Args const&... args)
		{
			/*
			sizeof...(Args)			//类型参数的数目
			sizeof...(args)				//函数参数的数目

			包扩展的pattern:
			Args const&

			转发参数包:
			std::forward<Args>(args)...
			*/

			mPointer = _create(dispatch_type(),args...);
		}

		/*!
		 * \remarks 销毁(可以手动调用，也可以不调用而通过析构函数调用)
		 * \return 
		*/
		void destroy()
		{
			_destroy(dispatch_type());
		}

		/*!
		 * \remarks 解引用
		 * \return 
		*/
		reference operator*() const
		{
			BOOST_ASSERT(mPointer);

			return *mPointer;
		}

		/*!
		 * \remarks 调用
		 * \return 
		*/
		pointer operator->() const
		{
			BOOST_ASSERT(mPointer);

			return mPointer;
		}

		/*!
		 * \remarks 获取原始指针
		 * \return 
		*/
		pointer get() const
		{
			return mPointer;
		}

		/*!
		 * \remarks operator bool
		 * \return 
		*/
		explicit operator bool() const noexcept
		{
			return mPointer != nullptr;
		}

		/*!
		 * \remarks operator !
		 * \return 
		*/
		bool operator!() const noexcept
		{
			return mPointer == nullptr;
		}

	private:
		template<typename... Args>
		pointer _create(boost::true_type,Args const&... args)
		{
			BOOST_STATIC_ASSERT(boost::has_new_operator<pointee>::value);

			return UNG_NEW pointee(args...);
		}

		template<typename... Args>
		pointer _create(boost::false_type,Args const&... args)
		{
			return GLOBAL_NEW pointee(args...);
		}

		void _destroy(boost::true_type)
		{
			UNG_DEL(mPointer);
		}

		void _destroy(boost::false_type)
		{
			GLOBAL_DEL(mPointer);
		}

		/*!
		 * \remarks 返回指针给别人，同时设置自己的指针为nullptr
		 * \return 
		*/
		pointer _transfer()
		{
			BOOST_ASSERT(mPointer);

			auto p = mPointer;

			mPointer = nullptr;

			return p;
		}

	private:
		pointer mPointer;
	};
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_SEPARATEDPOINTER_H_