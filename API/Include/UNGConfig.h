/*!
 * \brief
 * �ڲ������ļ���
 * \file UNGConfig.h
 *
 * \author Su Yang
 *
 * \date 2017/03/29
 */
#ifndef _UNG_CONFIG_H_
#define _UNG_CONFIG_H_

#ifndef _UNG_ENABLE_H_
#include "UNGEnable.h"
#endif

#ifdef UNG_HIERARCHICAL_COMPILE

#ifndef _UBS_H_
#include "UBS.h"
#endif

#ifndef _UTM_H_
#include "UTM.h"
#endif

#ifndef _UMM_H_
#include "UMM.h"
#endif

#ifndef _UFC_H_
#include "UFC.h"
#endif

#ifndef _UES_H_
#include "UES.h"
#endif

#ifndef _UML_H_
#include "UML.h"
#endif

#ifndef _UPE_H_
#include "UPE.h"
#endif

#ifndef _URM_H_
#include "URM.h"
#endif

#ifndef _USM_H_
#include "USM.h"
#endif

#endif//UNG_HIERARCHICAL_COMPILE

#endif//_UNG_CONFIG_H_