/*!
 * \brief
 * Ч��
 * \file URMEffect.h
 *
 * \author Su Yang
 *
 * \date 2017/06/20
 */
#ifndef _URM_EFFECT_H_
#define _URM_EFFECT_H_

#ifndef _URM_CONFIG_H_
#include "URMConfig.h"
#endif

#ifdef URM_HIERARCHICAL_COMPILE

namespace ung
{
	/*!
	 * \brief
	 * Ч��
	 * \class Effect
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/20
	 *
	 * \todo
	 */
	class Effect : public MemoryPool<Effect>
	{
	public:
	};
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

#endif//_URM_EFFECT_H_