/*!
 * \brief
 * 数值类型的边界与最小正规值
 * \file UFCNumBounds.h
 *
 * \author Su Yang
 *
 * \date 2017/06/17
 */
#ifndef _UFC_NUMBOUNDS_H_
#define _UFC_NUMBOUNDS_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#ifndef _INCLUDE_BOOST_BOUNDS_HPP_
#include "boost/numeric/conversion/bounds.hpp"
#define _INCLUDE_BOOST_BOUNDS_HPP_
#endif

namespace ung
{
	/*!
	 * \brief
	 * 数值类型的边界与最小正规值
	 * \class NumBounds
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/17
	 *
	 * \todo
	 */
	template<typename N>
	class NumBounds
	{
	public:
		/*!
		 * \remarks 下界
		 * \return 
		*/
		static N lowest()
		{
			return boost::numeric::bounds<N>::lowest();
		}

		/*!
		 * \remarks 上界
		 * \return 
		*/
		static N highest()
		{
			return boost::numeric::bounds<N>::highest();
		}

		/*!
		 * \remarks 最小正规值
		 * \return 
		*/
		static N smallest()
		{
			return boost::numeric::bounds<N>::smallest();
		}
	};
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_NUMBOUNDS_H_