/*!
 * \brief
 * UBS配置文件
 * \file UBSConfig.h
 *
 * \author Su Yang
 *
 * \date 2017/03/12
 */
#ifndef _UBS_CONFIG_H_
#define _UBS_CONFIG_H_

#ifndef _UBS_ENABLE_H_
#include "UBSEnable.h"
#endif

#ifdef UBS_HIERARCHICAL_COMPILE

#ifndef _UBS_GLOBALCONFIG_H_
#include "UBSGlobalConfig.h"
#endif

#ifndef _UBS_MACRO_H_
#include "UBSMacro.h"
#endif

#ifndef _UBS_STRING_H_
#include "UBSString.h"
#endif

#ifndef _UBS_FLOAT_H_
#include "UBSFloat.h"
#endif

#ifndef _INCLUDE_BOOST_CONFIG_HPP_
#include "boost/config.hpp"
#define _INCLUDE_BOOST_CONFIG_HPP_
#endif

#if defined(UNG_CLOSE_SIMD) || UNG_USE_DOUBLE
#define UNG_USE_SIMD 0
#else
//SIMD需要和单精度浮点数共同开启
#define UNG_USE_SIMD 1
#endif

#endif//UBS_HIERARCHICAL_COMPILE

#endif//_UBS_CONFIG_H_