/*!
 * \brief
 * binary heap(complete binary tree完全二叉树)
 * \file UTLHeap.h
 *
 * \author Su Yang
 *
 * \date 2016/02/24
 */
#ifndef _UNG_UTL_HEAP_H_
#define _UNG_UTL_HEAP_H_

#ifndef _UNG_UTL_CONFIG_H_
#include "UTLConfig.h"
#endif

namespace ung
{
	namespace utl
	{
		namespace heap
		{
			/*!
			* \remarks push到heap
			* \return
			* \param RandomAccessIterator first
			* \param RandomAccessIterator last
			*/
			template<typename RandomAccessIterator>
			inline void push_heap(RandomAccessIterator first, RandomAccessIterator last)
			{
				/*
				注意：此时，新元素应已置于底部容器的最尾端
				*/

				using value_type = boost::iterator_value<RandomAccessIterator>::type;
				using distance_type = boost::iterator_difference<RandomAccessIterator>::type;

				//元素值
				value_type val = *(last - 1);
				//第一个洞号
				distance_type holeIndex = (last - first) - 1;
				//根节点洞号
				distance_type topIndex = 0;
				//找出父节点
				distance_type parent = (holeIndex - 1) / 2;

				//当尚未到达顶端，且父节点小于新值
				while (holeIndex > topIndex && *(first + parent) < val)				//*(first + parent):vec[parent]
				{
					//令洞值为父值
					*(first + holeIndex) = *(first + parent);
					//percolate up:调整洞号，向上提升至父节点
					holeIndex = parent;
					//新洞的父节点
					parent = (holeIndex - 1) / 2;
				}//持续至顶端，或满足heap的次序特性为止

				//令洞值为新值，完成插入操作
				*(first + holeIndex) = val;
			}

			/*!
			 * \remarks 将键值最大的节点放置到vector的最后一个位置
			 * \return 
			 * \param RandomAccessIterator first
			 * \param RandomAccessIterator last
			*/
			template<typename RandomAccessIterator>
			inline void pop_heap(RandomAccessIterator first, RandomAccessIterator last)
			{
				using value_type = boost::iterator_value<RandomAccessIterator>::type;
				using distance_type = boost::iterator_difference<RandomAccessIterator>::type;

				distance_type len = last - first - 1;
				//最后一个节点
				value_type val = *(last - 1);
				//让最后一个节点持有根节点，用于vector的back()和pop_back()
				*(last - 1) = *first;

				adjust_heap_(first, distance_type(0), len,val);

				/*
				要获取，然后删除此时vector的最后一个元素(持有根节点)，需要通过vector对象来操作
				*/
			}

			/*!
			 * \remarks 获得一个递增序列
			 * \return 
			 * \param RandomAccessIterator first
			 * \param RandomAccessIterator last
			*/
			template<typename RandomAccessIterator>
			void sort_heap(RandomAccessIterator first, RandomAccessIterator last)
			{
				while (last - first > 1)
				{
					ung::utl::heap::pop_heap(first, last--);
				}
			}

			/*!
			 * \remarks 将[first,last)排列为一个heap
			 * \return 
			 * \param RandomAccessIterator first
			 * \param RandomAccessIterator last
			*/
			template<typename RandomAccessIterator>
			void make_heap(RandomAccessIterator first, RandomAccessIterator last)
			{
				using value_type = boost::iterator_value<RandomAccessIterator>::type;
				using distance_type = boost::iterator_difference<RandomAccessIterator>::type;

				//vector长度
				distance_type len = last - first;

				//如果长度为0或1，不必重新排列
				if (len < 2)
				{
					return;
				}

				//找出第一个需要重排的子树头部，以holeIndex标出。任何叶子节点都不需执行perlocate down
				distance_type holeIndex = (len - 2) / 2;
				while (true)
				{
					adjust_heap_(first, holeIndex, len, value_type(*(first + holeIndex)));
					if (holeIndex == 0)
					{
						return;
					}
					--holeIndex;
				}
			}

			template<typename RandomAccessIterator,typename Distance,typename T>
			void adjust_heap_(RandomAccessIterator first, Distance holeIndex, Distance len,T val)
			{
				using value_type = boost::iterator_value<RandomAccessIterator>::type;
				using distance_type = boost::iterator_difference<RandomAccessIterator>::type;

				//右子节点
				distance_type rightChildIndex = 2 * holeIndex + 2;

				/*
				比较洞节点之左右两个子值，然后以rightChildIndex代表较大子节点
				-1:pos从0开始,减少一次计算，因为当rightChildIndex值为last - first - 1时，已经表示容器最后一个元素了
				*/
				while (rightChildIndex < len)
				{
					if (*(first + (rightChildIndex - 1)) > *(first + rightChildIndex))
					{
						--rightChildIndex;
					}

					//percolate down:令较大子值为洞值，再令洞号下移至较大子节点处
					*(first + holeIndex) = *(first + rightChildIndex);
					holeIndex = rightChildIndex;

					//找出新洞节点的右子节点
					rightChildIndex = 2 * holeIndex + 2;
				}
				//如果rightChildIndex == 最后一个节点
				if (rightChildIndex == len)
				{
					--rightChildIndex;
					*(first + holeIndex) = *(first + rightChildIndex);
					holeIndex = rightChildIndex;
				}

				//让最终的holeIndex持有原vector最后一个元素
				*(first + holeIndex) = val;
				//执行一次percolate up(上溯)
				ung::utl::heap::push_heap(first, first + holeIndex + 1);
			}
		}//namespace heap
	}//namespace utl
}//namespace ung

#endif//_UNG_UTL_HEAP_H_

/*
heap并不归属于STL容器组件，它是个幕后英雄，扮演priority queue的助手。顾名思义，priority queue允许用户以任何次序将任何元素推入容器内，
但取出时一定是从优先权最高的元素开始取。binary max heap正是具有这样的特性，适合作为priority queue的底层机制。
所谓binary heap就是一种complete binary tree(完全二叉树)。也就是说，整棵binary tree除了最底层的叶子节点之外，是填满的，而最底层的叶子
节点由左至右又不得有空隙。
complete binary tree整棵树内没有任何节点漏洞，这带来一个极大的好处：我们可以利用array来存储所有节点。假设将array的#0元素保留(或设为
无限大或无限小值)，那么当complete binary tree中的某个节点位于array的i处时，其左子节点必位于array的2i处，其右子节点必位于array的2i + 1
处，其父节点必位于i / 2处(取整数)。通过这么简单的位置规则，array可以轻易实现出complete binary tree。这种以array表述tree的方式，我们称为
隐式表述法(implicit representation)。
这么一来，我们需要的工具就很简单了：一个array和一组heap算法(用来插入元素，删除元素，取极值，将某一整组数据排列成一个heap)。array的缺点
是无法动态改变大小，而heap却需要这项功能，因此，以vector代替array是更好的选择。
根据元素排列方式，heap可分为max-heap和min-heap两种，前者每个节点的键值(key)都大于或等于其子节点键值，后者的每个节点键值(key)都小于
或等于其子节点键值。因此，max-heap的最大值在根节点，并总是位于底层array或vector的起头处。min-heap的最小值在根节点，亦总是位于底层
array或vector的起头处。STL供应的是max-heap。
heap没有迭代器
heap的所有元素都必须遵循特别的(complete binary tree)排列规则，所以heap不提供遍历功能，也不提供迭代器。

push_heap:
为了满足complete binary tree的条件，新加入的元素一定要放在最下一层作为叶子节点，并填补在由左至右的第一个空格，也就是把新元素插入在底层
vector的end()处。
新元素是否适合于现有位置呢？为满足max-heap条件(每个节点的键值都大于或等于其子节点键值)，我们执行一个percolate up(上溯)程序：将新节点
拿来与其父节点比较，如果其键值比父节点大，就父子对换位置。如此一直上溯，直到不需对换或直到根节点为止。

pop_heap:
把根节点键值放置到vector的最后一个pos上
设置洞口为根节点的较大子节点，持续下行
把原vector的最后一个元素放置到最终确定的洞口上
从vector的first到最终洞口+1位置，执行push_heap(排序)
1：根节点和其较大子节点对调，并持续下放，直至叶子节点为止
2：令holeIndex为原本最后一个节点值
3：对holeIndex执行上溯

sort_heap:
既然每次pop_heap可获得heap中键值最大的元素，如果持续对整个heap做pop_heap操作，每次将操作范围从后向前缩减一个元素(因为pop_heap会
把键值最大的元素放在底部容器的最尾端)，当整个程序执行完毕时，我们便有了一个递增序列
*/