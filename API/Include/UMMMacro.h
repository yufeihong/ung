/*!
 * \brief
 * 宏
 * \file UMMMacro.h
 *
 * \author Su Yang
 *
 * \date 2016/06/15
 */
#ifndef _UMM_MACRO_H_
#define _UMM_MACRO_H_

#ifndef _UMM_CONFIG_H_
#include "UMMConfig.h"
#endif

#ifdef UMM_HIERARCHICAL_COMPILE

//小型区块的上调边界（值为多少，便为多少字节对齐），应该使用16字节对齐
//UNG_BYTESALIGNED---8
//UNG_SIMD_ALIGNMENT---16
#define UMM_BYTESALIGNED UNG_SIMD_ALIGNMENT

/*
自由链表的个数。这是一种实现快速高效的内存管理器的通用设计模式。
如果觉得调用第一级分配器次数太多的话，仅仅只需要修改下面这个宏的值就行了。
目前值32，可以让最后一个自由链表的每个小格子能够容纳512字节的对象(当采用16位对齐时)。
*/
#define UMM_FREELISTS_COUNT 32

/*
自由链表中小型区块的上限，超过这个上线，就调用第一级配置器。
这里的计算方式说明：
UMM_FREELISTS_COUNT：代表最后，也是最大的一个空闲列表，这个空闲列表的每一个小格子（其中存
放一个实际的对象）的大小为:UMM_BYTESALIGNED * UMM_FREELISTS_COUNT。也就是说，当一个
对象的大小超过了这个小格子的存放能力，那么就会调用第一级配置器。
*/
#define UMM_LEVEL_TWO_MAX_BYTES UMM_BYTESALIGNED * UMM_FREELISTS_COUNT

//-----------------------------------------------------------------------------------------------

#define UMM_OPERATOR_NEW																			\
void* operator new(size_t size) {if (size == 0){size = 1;}return PoolAllocator::allocate(size);}

#define UMM_OPERATOR_DELETE																		\
void operator delete(void* p, size_t size) noexcept {if (!p){return;}PoolAllocator::deallocate(p, size);BigDog::getInstance().doDelete(p);p = nullptr;}

#define UMM_OPERATOR_NEW_ARRAY																\
void* operator new[](size_t elementsTotalSize) {if (elementsTotalSize == 0){elementsTotalSize = 1;}return PoolAllocator::allocate(elementsTotalSize);}

#define UMM_OPERATOR_DELETE_ARRAY															\
void operator delete[](void* p, size_t elementsTotalSize) noexcept {if (!p){return;}PoolAllocator::deallocate(p, elementsTotalSize);BigDog::getInstance().doDelete(p);p = nullptr;}

#if UNG_DEBUGMODE
#define UMM_OPERATOR_NEW_INFO																	\
void* operator new(size_t size, std::string const& info) {if (size == 0){size = 1;}void* address = PoolAllocator::allocate(size);BigDog::getInstance().doNew(address,size,info);return address;}

#define UMM_OPERATOR_DELETE_INFO																\
void operator delete(void* p, size_t size, std::string const& info) noexcept {if (!p){return;}PoolAllocator::deallocate(p, size);BigDog::getInstance().doDelete(p);p = nullptr;}

#define UMM_OPERATOR_NEW_ARRAY_INFO														\
void* operator new[](size_t elementsTotalSize, std::string const& info) {if (elementsTotalSize == 0){elementsTotalSize = 1;}void* address = PoolAllocator::allocate(elementsTotalSize);BigDog::getInstance().doNew(address, elementsTotalSize, info);return address;}

#define UMM_OPERATOR_DELETE_ARRAY_INFO													\
void operator delete[](void* p, size_t elementsTotalSize, std::string const& info) noexcept {if (!p){return;}PoolAllocator::deallocate(p, elementsTotalSize);BigDog::getInstance().doDelete(p);p = nullptr;}
#else
#define UMM_OPERATOR_NEW_INFO
#define UMM_OPERATOR_DELETE_INFO
#define UMM_OPERATOR_NEW_ARRAY_INFO
#define UMM_OPERATOR_DELETE_ARRAY_INFO
#endif

//多重继承时，operator new,operator delete must be disambiguated。
#define UMM_OVERRIDE_NEW_DELETE																\
UMM_OPERATOR_NEW																						\
UMM_OPERATOR_DELETE																					\
UMM_OPERATOR_NEW_ARRAY																			\
UMM_OPERATOR_DELETE_ARRAY																		\
UMM_OPERATOR_NEW_INFO																				\
UMM_OPERATOR_DELETE_INFO																			\
UMM_OPERATOR_NEW_ARRAY_INFO																	\
UMM_OPERATOR_DELETE_ARRAY_INFO

#define GLOBAL_NEW new
#define GLOBAL_DEL(p) delete p;p = nullptr;

#if UNG_DEBUGMODE
#define ALLOCATE_INFO std::string("FILE:") + std::string(__FILE__) + std::string("@FUNC:") + BOOST_CURRENT_FUNCTION + std::string("@LINE:") + boost::lexical_cast<std::string>(__LINE__)
#else
#define ALLOCATE_INFO
#endif

#if UNG_DEBUGMODE
#define UNG_NEW new(ALLOCATE_INFO)
#else
#define UNG_NEW new
#endif
#define UNG_NEW_SMART UNG_NEW
#define UNG_NEW_ARRAY UNG_NEW
#define UNG_DEL(p) delete p;p = nullptr;
#define UNG_DEL_ARRAY delete[]

//未找到匹配的删除运算符；如果初始化引发异常，则不会释放内存
#pragma warning(disable : 4291)

#endif//UMM_HIERARCHICAL_COMPILE

#endif//_UMM_MACRO_H_

/*
//UMM_OPERATOR_NEW:
//当编译器调用operator new时, 把存储指定类型对象所需的字节数传给size_t形参
void* operator new(size_t size)															//隐式静态，无需写出static
{
	if (size == 0)
	{
		size = 1;
	}

	return PoolAllocator::allocate(size);
}

//UMM_OPERATOR_DELETE:
//p所指对象的字节数, 可用于删除继承体系中的对象, 如果基类有一个虚析构函数, 则字节数将因待删除指针所指对象的动态类型不同而有所区别
void operator delete(void* p, size_t size) noexcept
{
	//C++保证，删除0值的指针是安全的。
	if (!p)
	{
		return;
	}

	PoolAllocator::deallocate(p, size);

#if UNG_DEBUGMODE
	//移除信息
	BigDog::getInstance().doDelete(p);
#endif

	p = nullptr;
}

//UMM_OPERATOR_NEW_ARRAY:
//elementsTotalSize 数组所有元素的总字节数
void* operator new[](size_t elementsTotalSize)
{
	if (elementsTotalSize == 0)
	{
		elementsTotalSize = 1;
	}

	return PoolAllocator::allocate(elementsTotalSize);
}

//UMM_OPERATOR_DELETE_ARRAY:
//elementsTotalSize 数组所有元素的总字节数
void operator delete[](void* p, size_t elementsTotalSize) noexcept
{
	if (!p)
	{
		return;
	}

	PoolAllocator::deallocate(p, elementsTotalSize);

#if UNG_DEBUGMODE
	//移除信息
	BigDog::getInstance().doDelete(p);
#endif

	p = nullptr;
}

#if UNG_DEBUGMODE
//UMM_OPERATOR_NEW_INFO :
void* operator new(size_t size, std::string const& info)
{
	if (size == 0)
	{
		size = 1;
	}

	void* address = PoolAllocator::allocate(size);

	//收集信息
	BigDog::getInstance().doNew(address,size,info);

	return address;
}

//UMM_OPERATOR_DELETE_INFO:
//这个版本是为了匹配上面的版本，这个版本仅在上面的new发生异常时由系统来调用
void operator delete(void* p, size_t size, std::string const& info) noexcept
{
	if (!p)
	{
		return;
	}

	PoolAllocator::deallocate(p, size);

	//移除信息
	BigDog::getInstance().doDelete(p);

	p = nullptr;
}

//UMM_OPERATOR_NEW_ARRAY_INFO:
void* operator new[](size_t elementsTotalSize, std::string const& info)
{
	if (elementsTotalSize == 0)
	{
		elementsTotalSize = 1;
	}

	void* address = PoolAllocator::allocate(elementsTotalSize);

	//收集信息
	BigDog::getInstance().doNew(address, elementsTotalSize, info);

	return address;
}

//UMM_OPERATOR_DELETE_ARRAY_INFO:
//匹配上面版本，仅在上面的new[]发生异常时由系统来调用
void operator delete[](void* p, size_t elementsTotalSize, std::string const& info) noexcept
{
	if (!p)
	{
		return;
	}

	PoolAllocator::deallocate(p, elementsTotalSize);

	//移除信息
	BigDog::getInstance().doDelete(p);

	p = nullptr;
}
#endif
*/