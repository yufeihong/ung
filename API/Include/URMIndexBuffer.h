/*!
 * \brief
 * index buffer
 * \file URMIndexBuffer.h
 *
 * \author Su Yang
 *
 * \date 2017/05/30
 */
#ifndef _URM_INDEXBUFFER_H_
#define _URM_INDEXBUFFER_H_

#ifndef _URM_CONFIG_H_
#include "URMConfig.h"
#endif

#ifdef URM_HIERARCHICAL_COMPILE

#ifndef _URM_VIDEOBUFFER_H_
#include "URMVideoBuffer.h"
#endif

#ifndef _URM_ENUMCOLLECTION_H_
#include "URMEnumCollection.h"
#endif

namespace ung
{
	class UngExport IndexBuffer : public VideoBuffer
	{
	protected:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		IndexBuffer();

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param IndexType idxType
		 * \param uint32 numIndexes
		 * \param uint32 usage
		 * \param bool useShadowBuffer
		*/
		IndexBuffer(IndexType idxType, uint32 numIndexes,uint32 usage, bool useShadowBuffer);

	public:
		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		virtual ~IndexBuffer();

		/*!
		 * \remarks Get the type of indexes used
		 * \return 
		*/
		IndexType getType() const;

		/*!
		 * \remarks Get the number of indexes
		 * \return 
		*/
		uint32 getNumIndexes() const;

		/*!
		 * \remarks Get the size in bytes of each index
		 * \return 
		*/
		uint32 getIndexSize() const;

		/*!
		 * \remarks Reads data from the buffer and places it in the memory pointed to by pDest
		 * \return 
		 * \param uint32 offset
		 * \param uint32 length
		 * \param void * pDest
		*/
		virtual void readData(uint32 offset,uint32 length,void* pDest) = 0;

		/*!
		 * \remarks Writes data to the buffer from an area of system memory
		 * \return 
		 * \param uint32 offset The byte offset from the start of the buffer to start writing
		 * \param uint32 length
		 * \param const void * pSource
		 * \param bool discardWholeBuffer 建议为true，这样就允许驱动在写入数据的时候丢弃整个缓存，就可以避免DMA stalls
		*/
		virtual void writeData(uint32 offset,uint32 length,const void* pSource,bool discardWholeBuffer = false) = 0;

	protected:
		IndexType mIndexType;
		uint32 mNumIndexes;
		uint32 mIndexSize;
	};
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

#endif//_URM_INDEXBUFFER_H_