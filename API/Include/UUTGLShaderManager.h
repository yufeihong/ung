/*!
 * \brief
 * OpenGL shader管理器
 * \file UUTGLShaderManager.h
 *
 * \author Su Yang
 *
 * \date 2016/11/13
 */
#ifndef _UUT_GLSHADERMANAGER_H_
#define _UUT_GLSHADERMANAGER_H_

#ifndef _UUT_CONFIG_H_
#include "UUTConfig.h"
#endif

#ifdef UUT_HIERARCHICAL_COMPILE

namespace ung
{
	class GLShader;
	class GLProgram;

	/*!
	 * \brief
	 * shader管理器
	 * \class ShaderManager
	 *
	 * \author Su Yang
	 *
	 * \date 2016/11/13
	 *
	 * \todo
	 */
	class UngExport ShaderManager : public Singleton<ShaderManager>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		ShaderManager() = default;

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~ShaderManager();

		/*!
		 * \remarks 创建一个program
		 * \return 
		 * \param String const & materialFileName
		*/
		std::shared_ptr<GLProgram> createProgram(String const& materialFileName);

		/*!
		 * \remarks link program
		 * \return 
		 * \param std::shared_ptr<GLProgram> program
		*/
		void linkProgram(std::shared_ptr<GLProgram> program);

		/*!
		 * \remarks 使用给定的program
		 * \return 
		 * \param std::shared_ptr<GLProgram> program
		*/
		void useProgram(std::shared_ptr<GLProgram> program);

	private:
		/*!
		 * \remarks 创建一个shader
		 * \return 
		 * \param String const & fileName shader源文件名
		*/
		std::shared_ptr<GLShader> createShader(String const& fileName);

		/*!
		 * \remarks 把shader给存储到容器中
		 * \return 
		 * \param String const & shaderName
		 * \param std::shared_ptr<GLShader> shader
		*/
		void insertShader(String const& shaderName,std::shared_ptr<GLShader> shader);

		/*!
		 * \remarks 从容器中删除给定shader
		 * \return 
		 * \param String const & shaderName
		*/
		void eraseShader(String const& shaderName);

		/*!
		 * \remarks 从容器中删除所有的shader
		 * \return 
		*/
		void eraseAllShader();

		/*!
		 * \remarks 通过shader名来获取一个shader
		 * \return 
		 * \param String const & shaderName
		*/
		std::shared_ptr<GLShader> getGLShader(String const& shaderName);

		/*!
		 * \remarks 通过材质名来获取一个program
		 * \return 
		 * \param String const & materialName
		*/
		std::shared_ptr<GLProgram> getGLProgram(String const& materialName);

		/*!
		 * \remarks 把program给存储到容器中
		 * \return 
		 * \param String const & materialName
		 * \param std::shared_ptr<GLProgram> program
		*/
		void insertProgram(String const& materialName, std::shared_ptr<GLProgram> program);

		/*!
		 * \remarks 从容器中删除给定材质所对应的program
		 * \return 
		 * \param String const & materialName
		*/
		void eraseProgram(String const& materialName);

		/*!
		 * \remarks 从容器中删除所有的program
		 * \return 
		*/
		void eraseAllProgram();

	private:
		//String:着色器程序的文件名
		STL_UNORDERED_MAP(String, std::shared_ptr<GLShader>) mShaders;
		//String:材质的文件名
		STL_UNORDERED_MAP(String, std::shared_ptr<GLProgram>) mPrograms;
	};
}//namespace ung

#endif//UUT_HIERARCHICAL_COMPILE

#endif//_UUT_GLSHADERMANAGER_H_