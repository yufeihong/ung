/*!
 * \brief
 * 几何对象-射线
 * \file UPERay.h
 *
 * \author Su Yang
 *
 * \date 2016/05/22
 */
#ifndef _UPE_RAY_H_
#define _UPE_RAY_H_

#ifndef _UPE_CONFIG_H_
#include "UPEConfig.h"
#endif

#ifdef UPE_HIERARCHICAL_COMPILE

#ifndef _UPE_GEOMETRY_H_
#include "UPEGeometry.h"
#endif

namespace ung
{
	class Plane;

	/*!
	 * \brief
	 * Ray
	 * \class Ray
	 *
	 * \author Su Yang
	 *
	 * \date 2016/05/22
	 *
	 * \todo
	 */
	class UngExport Ray : public Geometry
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		Ray();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~Ray();

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param const Vector3 & o 起点
		 * \param const Vector3 & d 方向向量
		*/
		Ray(const Vector3& o, const Vector3& d);

		/*!
		 * \remarks 拷贝构造函数
		 * \return 
		 * \param const Ray & r
		*/
		Ray(const Ray& r);

		/*!
		 * \remarks 拷贝赋值运算符
		 * \return 
		 * \param const Ray & r
		*/
		Ray& operator=(const Ray& r);

		/*!
		 * \remarks 设置起点
		 * \return 
		 * \param const Vector3 & o
		*/
		void setOrigin(const Vector3& o);

		/*!
		 * \remarks 获取起点
		 * \return 
		*/
		const Vector3& getOrigin() const;

		/*!
		 * \remarks 设置方向向量
		 * \return 
		 * \param const Vector3 & d
		*/
		void setDirection(const Vector3& d);

		/*!
		 * \remarks 获取方向向量
		 * \return 
		*/
		const Vector3& getDirection() const;

		/*!
		 * \remarks 获取参数t所形成的射线尾端
		 * \return 
		 * \param real_type t
		*/
		Vector3 getPoint(real_type t) const;

		/*!
		 * \remarks 对射线进行空间变换(改变当前射线)
		 * \return 
		 * \param Matrix4 const & mat4
		*/
		Ray& operator*=(Matrix4 const& mat4);

		/*!
		 * \remarks 对射线进行空间变换(不改变当前射线)
		 * \return 
		 * \param Matrix4 const & mat4
		*/
		Ray operator*(Matrix4 const& mat4);

	private:
		//R(t) = P + td,|d| = 1.0,t ≥ 0
		Vector3 mOrigin{ 0.0, 0.0, 0.0 };
		Vector3 mDirection{ 0.0, 0.0, -1.0 };
	};
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

#endif//_UPE_RAY_H_