/*!
 * \brief
 * 顶点声明
 * \file URMVertexDeclaration.h
 *
 * \author Su Yang
 *
 * \date 2017/05/31
 */
#ifndef _URM_VERTEXDECLARATION_H_
#define _URM_VERTEXDECLARATION_H_

#ifndef _URM_CONFIG_H_
#include "URMConfig.h"
#endif

#ifdef URM_HIERARCHICAL_COMPILE

#ifndef _URM_VERTEXELEMENT_H_
#include "URMVertexElement.h"
#endif

#ifndef _UFC_POINTERCONTAINERWRAPPER_H_
#include "UFCPointerContainerWrapper.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 声明一组顶点的格式,由vertex elements组成
	 * 描述顶点的组织结构(A description of vertex structure.)。
	 * We describe a vertex declaration by an array of VertexElement.
	 * The data in this vertex buffer will have to be associated with a semantic element of the 
	 * rendering pipeline,e.g. a position,or texture coordinates.This is done using the 
	 * VertexDeclaration class,which itself contains VertexElement structures referring to the 
	 * source data.
	 * \class VertexDeclaration
	 *
	 * \author Su Yang
	 *
	 * \date 2017/05/31
	 *
	 * \todo
	 */
	class UngExport VertexDeclaration : public MemoryPool<VertexDeclaration>
	{
	public:
		//这里注意，不要写成VertexElement*
		using VertexElementList = PointerListWrapper<VertexElement>;
		using borrow_type = typename VertexElementList::borrow_type;

	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		VertexDeclaration();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~VertexDeclaration();

		/*!
		 * \remarks 在声明的尾部添加一个VertexElement*
		 * \return 
		 * \param VertexElement* ve
		*/
		virtual void addElement(VertexElement* ve);

		/*!
		 * \remarks 在声明的尾部添加一个VertexElement*
		 * \return 
		 * \param uint8 stream
		 * \param uint8 offset
		 * \param VertexElementSemantic
		 * \param VertexElementType theType
		 * \param uint8 index
		*/
		virtual void addElement(uint8 stream,uint8 offset,VertexElementSemantic semantic,VertexElementType theType,uint8 index = 0);

		/*!
		 * \remarks 在调用完所有addElement()函数后，必须调用该函数
		 * \return 
		*/
		virtual void addElementFinish() PURE;

		/*!
		 * \remarks 移除参数所指定的语义的元素
		 * \return 
		 * \param VertexElementSemantic semantic
		 * \param uint8 index 用于标记纹理坐标等重复性elements
		*/
		virtual void removeElement(VertexElementSemantic semantic,uint8 index = 0);

		/*!
		 * \remarks 移除所有的VertexElement
		 * \return 
		*/
		virtual void removeAllElements();

		/*!
		 * \remarks 获取声明中elements的个数
		 * \return 
		*/
		uint32 getElementCount() const;

		/*!
		 * \remarks 就地修改一个element
		 * \return 
		 * \param uint8 elem_index
		 * \param uint8 source
		 * \param uint8 offset
		 * \param VertexElementSemantic semantic
		 * \param VertexElementType theType
		 * \param uint8 index
		*/
		virtual void modifyElement(uint8 elem_index,uint8 source,uint8 offset,VertexElementSemantic semantic,VertexElementType theType,uint8 index = 0);

		/*!
		 * \remarks Finds a VertexElement
		 * \return 
		 * \param VertexElementSemantic sem
		 * \param uint8 index
		*/
		virtual borrow_type findElementBySemantic(VertexElementSemantic sem,uint8 index = 0);

		/*!
		 * \remarks 设置顶点声明
		 * \return 
		*/
		virtual void set() PURE;

		/*!
		 * \remarks 对elements进行排序
		 * \return 
		*/
		void sortElements();

		/*!
		 * \remarks Gets the vertex size defined by this declaration for a given source
		 * \return 
		 * \param uint8 stream
		*/
		virtual uint32 getVertexSize(uint8 stream);

		/*!
		 * \remarks 重载==
		 * \return 
		 * \param const VertexDeclaration & rightInst
		*/
		bool operator==(const VertexDeclaration& rightInst) const;

		/*!
		 * \remarks 重载!=
		 * \return 
		 * \param const VertexDeclaration & rightInst
		*/
		bool operator!=(const VertexDeclaration& rightInst) const;

	protected:
		/*
		顶点声明(VertexDeclaration)实际上就是对VertexElement数组的一个封装。
		Each element in the array describes one component of the vertex.
		*/
		VertexElementList mElementList;
		bool mAddVertexElementFlag;
	};
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

#endif//_URM_VERTEXDECLARATION_H_