/*!
 * \brief
 * 解包帮助信息。
 * \file UFCUnpackHelperInfo.h
 *
 * \author Su Yang
 *
 * \date 2016/12/21
 */
#ifndef _UFC_UNPACKHELPERINFO_H_
#define _UFC_UNPACKHELPERINFO_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#ifndef _UFC_CHOOSECONTAINER_H_
#include "UFCChooseContainer.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 解包帮助信息结构(对包的解析数据)
	 * \class UnpackHelperInfo
	 *
	 * \author Su Yang
	 *
	 * \date 2016/12/20
	 *
	 * \todo
	 */
	class UnpackHelperInfo : public MemoryPool<UnpackHelperInfo>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		UnpackHelperInfo() = default;

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~UnpackHelperInfo();

		/*!
		 * \remarks 获取包的名字
		 * \return 
		*/
		String const& getPackName() const;

		/*!
		 * \remarks 查询给定文件名在包中的索引
		 * \return 
		 * \param const char* fileName
		*/
		unsigned int getFileIndex(const char* fileName) const;

		//包的名字
		String mPackName;
		//文件数
		unsigned int mFileCount = 0;
		//section数
		unsigned int mSectionCount = 0;
		//压缩数据的偏移
		unsigned int mDataOffset = 0;
		//所有的文件名
		STL_VECTOR(String) mNames;
		//所有的section
		STL_VECTOR(unsigned int) mSections;
		//所有的条目(原始大小，文件在section块中的第一个索引，段数，文件在压缩数据中的偏移)
		using item_type = std::tuple<int64, unsigned int, unsigned int, int64>;
		STL_VECTOR(item_type) mItems;
		//映射的分段信息。0:这个映射距离压缩数据开头处的偏移,1:这个映射的实际字节数,2:这个映射所对应的起始段索引,3:这个map的结束段索引，下一个map的起始段索引
		using mapInfo_type = std::tuple<int64, unsigned int, unsigned int, unsigned int>;
		STL_VECTOR(mapInfo_type) mMaps;
	};
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_UNPACKHELPERINFO_H_