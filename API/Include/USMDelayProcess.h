/*!
 * \brief
 * 延迟过程
 * \file USMDelayProcess.h
 *
 * \author Su Yang
 *
 * \date 2016/12/14
 */
#ifndef _USM_DELAYPROCESS_H_
#define _USM_DELAYPROCESS_H_

#ifndef _USM_CONFIG_H_
#include "USMConfig.h"
#endif

#ifdef USM_HIERARCHICAL_COMPILE

#ifndef _USM_PROCESS_H_
#include "USMProcess.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 延迟过程
	 * \class DelayProcess
	 *
	 * \author Su Yang
	 *
	 * \date 2016/12/14
	 *
	 * \todo
	 */
	class UngExport DelayProcess : public Process
	{
	public:
		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param uint32 timeToDelay
		*/
		explicit DelayProcess(uint32 timeToDelay);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~DelayProcess();

	protected:
		virtual void onUpdate(ufast deltaMS) override;

	private:
		uint32 mTimeToDelay;
		uint32 mTimeDelayedSoFar;
	};
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE

#endif//_USM_DELAYPROCESS_H_