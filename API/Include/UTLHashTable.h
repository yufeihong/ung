/*!
 * \brief
 * HashTable
 * \file UTLHashTable.h
 *
 * \author Su Yang
 *
 * \date 2016/04/07
 */
#ifndef _UNG_UTL_HASHTABLE_H_
#define _UNG_UTL_HASHTABLE_H_

#ifndef _UNG_UTL_CONFIG_H_
#include "UTLConfig.h"
#endif

#ifndef _UNG_UTL_ALLOCATOR_H_
#include "UTLAllocator.h"
#endif

#ifndef _UNG_UTL_VECTOR_H_
#include "UTLVector.h"
#endif

#ifndef _UNG_UTL_HASHTABLEITERATOR_H_
#include "UTLHashTableIterator.h"
#endif

#ifndef _UNG_UTL_CONSTRUCT_H_
#include "UTLConstruct.h"
#endif

namespace ung
{
	namespace utl
	{
		enum {tableSizeCount = 28};

		/*
		虽然开链法并不要求表格大小必须为质数，但这里我们仍然以质数来设计表格大小，并且先将28个质数（逐渐呈现大约两倍的关系）计算好，以备随时访问
		*/
		static const uintfast32 tableSizeArray[tableSizeCount] =
		{
		  53,						97,					193,				389,				769,
		  1543,					3079,					6151,				12289,			24593,
		  49157,					98317,				196613,			393241,			786433,
		  1572869,				3145739,			6291469,		12582917,		25165843,
		  50331653,			100663319,		201326611,	402653189,	805306457,
		  1610612741,		3221225473,		4294967291
		};

		inline uintfast32 nextTableSize(uintfast32 n)
		{
			const uintfast32* first = tableSizeArray;
			const uintfast32* last = tableSizeArray + (sfast)tableSizeCount;

			/*
			lower_bound(beg,end,val)
			lower_bound(beg,end,val,comp)
			返回一个迭代器，表示第一个小于等于val的元素，如果不存在这样的元素，则返回end
			使用lower_bound()，序列需先排序（质数数组已经排好了序）
			*/
			const uintfast32* pos = std::lower_bound(first, last, n);
			return pos == last ? *(last - 1) : *pos;
		}

		/*!
		 * \brief
		 * 散列表的节点
		 * \class HashTableNode
		 *
		 * \author Su Yang
		 *
		 * \date 2016/04/08
		 *
		 * \todo
		 */
		template <typename Value>
		struct HashTableNode
		{
			using pointer = HashTableNode<Value>*;

			pointer mNext;
			Value mValue;
		};

		/*!
		 * \brief
		 * 散列表
		 * \class HashTable
		 *
		 * \author Su Yang
		 *
		 * \date 2016/04/08
		 *
		 * \todo
		 */
		/*
		Value:节点的实值型别
		Key:节点的键值型别
		HashFcn:函数对象，hash function，计算元素位置（bucket位置）的函数
		ExtractKey:函数对象，用于从节点中取出键值
		EqualKey:函数对象，判断键值相同与否
		*/
		template <typename Value, typename Key, typename HashFcn,typename ExtractKey, typename EqualKey, typename Alloc = Allocator<HashTableNode<Value>>>
		class HashTable
		{
		public:
			using value_type = Value;
			using key_type = Key;
			using hasher = HashFcn;
			using key_extract = ExtractKey;
			using key_equal = EqualKey;
			using alloc_type = Alloc;

			using pointer = value_type*;
			using const_pointer = value_type const*;
			using reference = value_type&;
			using const_reference = value_type const&;
			using difference_type = std::ptrdiff_t;
			using size_type = usize;

			using iterator = HashTableIterator<value_type, key_type, hasher, key_extract, key_equal, alloc_type>;
			using const_iterator = HashTableConstIterator<value_type, key_type, hasher, key_extract, key_equal, alloc_type>;

		private:
			using node_type = HashTableNode<value_type>;
			using node_ptr = typename HashTableNode<value_type>::pointer;

		public:
			HashTable(size_type n,const hasher& hf,const key_equal& eql,const key_extract& ext) :
				mHasher(hf),
				mEqual(eql),
				mGetKey(ext),
				mElementNum(0)
			{
				initializeBuckets(n);
			}

			HashTable(size_type n,const hasher& hf,const key_equal& eql) :
				mHasher(hf),
				mEqual(eql),
				mGetKey(key_extract()),
				mElementNum(0)
			{
				initializeBuckets(n);
			}

			HashTable(const HashTable& ht) :
				mHasher(ht.mHasher),
				mEqual(ht.mEqual),
				mGetKey(ht.mGetKey),
				mElementNum(0)
			{
				copyFrom(__ht);
			}

			HashTable& operator=(const HashTable& ht)
			{
				if (&ht != this)
				{
					clear();
					mHasher = ht.mHasher;
					mEqual = ht.mEqual;
					mGetKey = ht.mGetKey;
					copyFrom(ht);
				}

				return *this;
			}

			~HashTable()
			{
				clear();
			}

			size_type size() const
			{
				return mElementNum;
			}

			size_type max_size() const
			{
				return size_type(-1);
			}

			bool empty() const
			{
				return size() == 0;
			}

			void swap(HashTable& ht)
			{
				std::swap(mHasher, ht.mHasher);
				std::swap(mEqual, ht.mEqual);
				std::swap(mGetKey, ht.mGetKey);
				mBuckets.swap(ht.mBuckets);
				std::swap(mElementNum, ht.mElementNum);
			}

			iterator begin()
			{
				for (size_type n = 0; n < mBuckets.size(); ++n)
				{
					if (mBuckets[n])
					{
						return iterator(mBuckets[n], this);
					}
				}

				return end();
			}

			const_iterator begin() const
			{
				for (size_type n = 0; n < mBuckets.size(); ++n)
				{
					if (mBuckets[n])
					{
						return const_iterator(mBuckets[n], this);
					}
				}

				return end();
			}

			iterator end()
			{
				return iterator(nullptr, this);
			}

			const_iterator end() const
			{
				return const_iterator(nullptr, this);
			}

		public:
			/*!
			 * \remarks 获取bucket的个数，即buckets vector的大小
			 * \return 
			*/
			size_type bucketCount() const
			{
				return mBuckets.size();
			}

			/*!
			 * \remarks 获取bucket可能的最大个数
			 * \return 
			*/
			size_type maxBucketCount() const
			{
				return tableSizeArray[(sfast)tableSizeCount - 1];
			}

			/*!
			 * \remarks 获取给定bucket中的元素个数
			 * \return 
			 * \param size_type bkt
			*/
			size_type elemsNumInBucket(size_type bkt) const
			{
				size_type ret = 0;

				/*
				mBuckets[bkt]:在这里作为右值，所以会调用const版本的operator[]()函数
				*/
				for (node_ptr cur = mBuckets[bkt]; cur; cur = cur->mNext)
				{
					++ret;
				}

				return ret;
			}

			/*!
			 * \remarks 插入元素，不允许重复
			 * \return 
			 * \param const value_type & obj
			*/
			std::pair<iterator, bool> insertUnique(const value_type& obj)
			{
				//判断是否需要重建表格，如需要就扩充
				resize(mElementNum + 1);

				return insertUniqueNoResize(obj);
			}

			/*!
			 * \remarks 插入元素，允许重复
			 * \return 
			 * \param const value_type & obj
			*/
			iterator insertEqual(const value_type& obj)
			{
				resize(mElementNum + 1);

				return insertEqualNoResize(obj);
			}

			/*!
			 * \remarks 在不需要重建表格的情况下插入新节点，键值不允许重复
			 * \return 
			 * \param const value_type & obj
			*/
			std::pair<iterator, bool> insertUniqueNoResize(const value_type& obj)
			{
				//计算obj的位置
				const size_type n = calPos(obj);
				//令first指向bucket对应之串行头部
				node_ptr first = mBuckets[n];

				//如果mBuckets[n]已被占用，此时first将不为nullptr，于是进入以下循环，走过bucket所对应的整个链表
				for (node_ptr cur = first; cur; cur = cur->mNext)
				{
					//如果发现与链表中的某键值相同，就不插入，立刻返回
					if (mEqual(mGetKey(cur->mValue), mGetKey(obj)))
					{
						return std::pair<iterator, bool>(iterator(cur, this), false);
					}
				}

				//离开上面循环（或根本未进入循环）时，first指向bucket所指链表的头部节点
				//产生新节点
				node_ptr tmp = newNode(obj);
				tmp->mNext = first;
				//令新节点成为链表的第一个节点
				mBuckets[n] = tmp;
				++mElementNum;

				return std::pair<iterator, bool>(iterator(tmp, this), true);
			}

			/*!
			 * \remarks 在不需要重建表格的情况下插入新节点，键值允许重复
			 * \return 指向新增节点的迭代器
			 * \param const value_type & obj
			*/
			iterator insertEqualNoResize(const value_type& obj)
			{
				//计算obj的位置
				const size_type n = calPos(obj);
				//令first指向bucket对应之链表头部
				node_ptr first = mBuckets[n];

				//如果mBuckets[n]已被占用，此时first将不为nullptr，于是进入以下循环，走过bucket所对应的整个链表
				for (node_ptr cur = first; cur; cur = cur->mNext)
				{
					//如果发现与链表中的某键值相同，就马上插入，然后返回
					if (mEqual(mGetKey(cur->mValue), mGetKey(obj)))
					{
						//产生新节点
						node_ptr tmp = newNode(obj);
						//将新节点插入于目前位置之后
						tmp->mNext = cur->mNext;
						cur->mNext = tmp;
						++mElementNum;

						//返回一个迭代器，指向新增节点
						return iterator(tmp, this);
					}
				}

				//进行至此，表示没有发现重复的键值
				//产生新节点
				node_ptr tmp = newNode(obj);
				//将新节点插入于链表头部
				tmp->mNext = first;
				mBuckets[n] = tmp;
				++mElementNum;

				//返回一个迭代器，指向新增节点
				return iterator(tmp, this);
			}

			template <class InputIterator>
			void insertUnique(InputIterator first, InputIterator last)
			{
				insertUnique(first, last, Iter_cat(first));
			}

			template <class InputIterator>
			void insertEqual(InputIterator first, InputIterator last)
			{
				insertEqual(first, last, Iter_cat(first));
			}

			template <class InputIterator>
			void insertUnique(InputIterator first, InputIterator last,std::input_iterator_tag)
			{
				for (; first != last; ++first)
				{
					insertUnique(*first);
				}
			}

			template <class InputIterator>
			void insertEqual(InputIterator first, InputIterator last,std::input_iterator_tag)
			{
				for (; first != last; ++first)
				{
					insertEqual(*first);
				}
			}

			template <class ForwardIterator>
			void insertUnique(ForwardIterator first, ForwardIterator last,std::forward_iterator_tag)
			{
				size_type n = 0;
				distance(first, last, n);
				resize(mElementNum + n);
				for (; n > 0; --n, ++first)
				{
					insertUniqueNoResize(*first);
				}
			}

			template <class ForwardIterator>
			void insertEqual(ForwardIterator first, ForwardIterator last,std::forward_iterator_tag)
			{
				size_type n = 0;
				distance(first, last, n);
				resize(mElementNum + n);
				for (; n > 0; --n, ++first)
				{
					insertEqualNoResize(*first);
				}
			}

			reference findOrInsert(const value_type& obj)
			{
				resize(mElementNum + 1);

				size_type n = calPos(obj);
				node_ptr first = mBuckets[n];

				for (node_ptr cur = first; cur; cur = cur->mNext)
				{
					if (mEqual(mGetKey(cur->mValue), mGetKey(obj)))
					{
						return cur->mValue;
					}
				}

				node_ptr tmp = newNode(obj);
				tmp->mNext = first;
				mBuckets[n] = tmp;
				++mElementNum;

				return tmp->mValue;
			}

			iterator find(const key_type& key)
			{
				//首先寻找落在哪一个bucket内
				size_type n = calPosFromKey(key);
				node_ptr first;
				//从bucket list的头开始，一一比对每个元素的键值，比对成功就跳出
				for (first = mBuckets[n];first && !mEqual(mGetKey(first->mValue), key);first = first->mNext)
				{
				}

				return iterator(first, this);
			}

			const_iterator find(const key_type& key) const
			{
				size_type n = calPosFromKey(key);
				const node_ptr first;
				for (first = mBuckets[n];first && !mEqual(mGetKey(first->mValue), key);first = first->mNext)
				{
				}

				return const_iterator(first, this);
			}

			size_type count(const key_type& key) const
			{
				//首先寻找落在哪一个bucket内
				const size_type n = calPosFromKey(key);
				size_type ret = 0;
				
				//从bucket list的头开始，一一比对每个元素的键值，比对成功就累加1
				for (const node_type* cur = mBuckets[n]; cur; cur = cur->mNext)
				{
					if (mEqual(mGetKey(cur->mValue), key))
					{
						++ret;
					}
				}

				return ret;
			}

			std::pair<iterator, iterator> equalRange(const key_type& key)
			{
				typedef pair<iterator, iterator> Pii;
				const size_type n = calPosFromKey(key);

				for (node_ptr first = mBuckets[n]; first; first = first->mNext)
				{
					if (mEqual(mGetKey(first->mValue), key))
					{
						for (node_ptr cur = first->mNext; cur; cur = cur->mNext)
						{
							if (!mEqual(mGetKey(cur->mValue), key))
							{
								return Pii(iterator(first, this), iterator(cur, this));
							}
						}

						for (size_type m = n + 1; m < mBuckets.size(); ++m)
						{
							if (mBuckets[m])
							{
								return Pii(iterator(first, this), iterator(mBuckets[m], this));
							}
						}

						return Pii(iterator(first, this), end());
					}
				}

				return Pii(end(), end());
			}

			std::pair<const_iterator, const_iterator> equalRange(const key_type& key) const
			{
				typedef pair<const_iterator, const_iterator> Pii;
				const size_type n = calPosFromKey(key);

				for (const node_ptr first = mBuckets[n];first;first = first->mNext)
				{
					if (mEqual(mGetKey(first->mValue), key))
					{
						for (const node_ptr cur = first->mNext; cur; cur = cur->mNext)
						{
							if (!mEqual(mGetKey(cur->mValue), key))
							{
								return Pii(const_iterator(first, this), const_iterator(cur, this));
							}
						}

						for (size_type m = n + 1; m < mBuckets.size(); ++m)
						{
							if (mBuckets[m])
							{
								return Pii(const_iterator(first, this), const_iterator(mBuckets[m], this));
							}
						}

						return Pii(const_iterator(first, this), end());
					}
				}

				return Pii(end(), end());
			}

			size_type erase(const key_type& key)
			{
				const size_type n = calPosFromKey(key);
				node_ptr first = mBuckets[n];
				size_type erased = 0;

				if (first)
				{
					node_ptr cur = first;
					node_ptr next = cur->mNext;
					while (next)
					{
						if (mEqual(mGetKey(next->mValue), key))
						{
							cur->mNext = next->mNext;
							deleteNode(next);
							next = cur->mNext;
							++erased;
							--mElementNum;
						}
						else
						{
							cur = next;
							next = cur->mNext;
						}
					}

					if (mEqual(mGetKey(first->mValue), key))
					{
						mBuckets[n] = first->mNext;
						deleteNode(first);
						++erased;
						--mElementNum;
					}
				}

				return erased;
			}
			void erase(const iterator& it)
			{
				node_ptr p = it._M_cur;
				if (p)
				{
					const size_type n = calPos(p->mValue);
					node_ptr cur = mBuckets[n];

					if (cur == p)
					{
						mBuckets[n] = cur->mNext;
						deleteNode(cur);
						--mElementNum;
					}
					else
					{
						node_ptr next = cur->mNext;
						while (next)
						{
							if (next == p)
							{
								cur->mNext = next->mNext;
								deleteNode(next);
								--mElementNum;
								break;
							}
							else
							{
								cur = next;
								next = cur->mNext;
							}
						}
					}
				}
			}

			void erase(iterator first, iterator last)
			{
				size_type bktFirst = first._M_cur ? calPos(first._M_cur->mValue) : mBuckets.size();
				size_type bktLast = last._M_cur ? calPos(last._M_cur->mValue) : mBuckets.size();

				if (first._M_cur == last._M_cur)
				{
					return;
				}
				else if (bktFirst == bktLast)
				{
					eraseBucket(bktFirst, first._M_cur, last._M_cur);
				}
				else
				{
					eraseBucket(bktFirst, first._M_cur, 0);
					for (size_type n = bktFirst + 1; n < bktLast; ++n)
					{
						eraseBucket(n, 0);
					}

					if (bktLast != mBuckets.size())
					{
						eraseBucket(bktLast, last._M_cur);
					}
				}
			}

			void erase(const const_iterator& it)
			{
				erase(iterator(const_cast<node_ptr>(it._M_cur),const_cast<hashtable*>(it._M_ht)));
			}

			void erase(const_iterator first, const_iterator last)
			{
				erase(iterator(const_cast<node_ptr>(first._M_cur),const_cast<hashtable*>(first._M_ht)),
							iterator(const_cast<node_ptr>(last._M_cur),const_cast<hashtable*>(last._M_ht)));
			}

			/*!
			 * \remarks 判断是否需要重建表格，如果不需要立刻返回，如果需要就开始重建
			 * \return 
			 * \param size_type numHint
			*/
			void resize(size_type numHint)
			{
				const size_type old_n = mBuckets.size();
				//需要重新配置
				if (numHint > old_n)
				{
					//找出下一个质数
					const size_type n = nextSize(numHint);
					if (n > old_n)
					{
						//设立新的buckets，tmp在离开作用域时释放
						Vector<node_ptr> tmp(n, nullptr);

						try
						{
							//处理每一个旧的bucket
							for (size_type bkt = 0; bkt < old_n; ++bkt)
							{
								//指向节点所对应之串行的起始节点
								node_ptr first = mBuckets[bkt];
								//处理每一个旧bucket所含（串行）的每一个节点
								while (first)
								{
									//找出节点落在哪一个新bucket中
									size_type newBkt = calPos(first->mValue, n);
									//令旧bucket指向其所对应之串行的下一个节点（以便迭代处理）
									mBuckets[bkt] = first->mNext;
									//将当前节点插入到新bucket内，成为其对应串行的第一个节点
									first->mNext = tmp[newBkt];
									tmp[newBkt] = first;
									//回到旧bucket所指的待处理串行，准备处理下一个节点
									first = mBuckets[bkt];
								}
							}
							//新旧两个buckets交换，注意：交换两方如果大小不同，大的会变小，小的会变大。
							mBuckets.swap(tmp);
						}
						catch (...)
						{
							for (size_type bkt = 0; bkt < tmp.size(); ++bkt)
							{
								while (tmp[bkt])
								{
									node_ptr next = tmp[bkt]->mNext;
									deleteNode(tmp[bkt]);
									tmp[bkt] = next;
								}
							}
							throw;
						}
					}//end if (n > old_n)
				}//end if (numHint > old_n)
			}

			/*!
			 * \remarks 释放bucket list的空间，vector空间没有释放掉
			 * \return 
			*/
			void clear()
			{
				//针对每一个bucket
				for (size_type i = 0; i < mBuckets.size(); ++i)
				{
					node_ptr cur = mBuckets[i];
					//将bucket list中的每一个节点删除掉
					while (cur)
					{
						node_ptr next = cur->mNext;
						deleteNode(cur);
						cur = next;
					}
					//令bucket内容为空指针
					mBuckets[i] = nullptr;
				}
				mElementNum = 0;
				//注意：buckets vector并未释放掉空间，仍保有原来大小
			}

		private:
			/*!
			 * \remarks 获取散列表的下一个大小
			 * \return 最接近n并大于或等于n的质数
			 * \param size_type n
			*/
			size_type nextSize(size_type n) const
			{
				return nextTableSize(n);
			}

			/*!
			 * \remarks 初始化散列表
			 * \return 
			 * \param size_type n
			*/
			void initializeBuckets(size_type n)
			{
				const size_type bktNum = nextSize(n);
				mBuckets.reserve(bktNum);
				mBuckets.insert(mBuckets.end(), bktNum, nullptr);
				mElementNum = 0;
			}

			/*!
			 * \remarks 根据key计算位置
			 * \return 
			 * \param const key_type & key
			*/
			size_type calPosFromKey(const key_type& key) const
			{
				return calPosFromKey(key, mBuckets.size());
			}

			/*!
			 * \remarks 根据key计算位置
			 * \return 
			 * \param const key_type & key
			 * \param size_t n
			*/
			size_type calPosFromKey(const key_type& key, size_t n) const
			{
				return mHasher(key) % n;
			}

			/*!
			 * \remarks 计算位置
			 * \return 
			 * \param const value_type & obj
			*/
			size_type calPos(const value_type& obj) const
			{
				return calPosFromKey(mGetKey(obj));
			}

			/*!
			 * \remarks 计算位置
			 * \return 
			 * \param const value_type & obj
			 * \param size_t n
			*/
			size_type calPos(const value_type& obj, size_t n) const
			{
				return calPosFromKey(mGetKey(obj), n);
			}

			/*!
			 * \remarks 配置节点
			 * \return 
			 * \param const value_type & obj
			*/
			node_ptr newNode(const value_type& obj)
			{
				node_ptr n = getNode();
				n->mNext = nullptr;

				try
				{
				  construct(&n->mValue, obj);

				  return n;
				}
				catch (...)
				{
					putNode(n);
					throw;
				}
			}

			/*!
			 * \remarks 释放节点
			 * \return 
			 * \param node_ptr n
			*/
			void deleteNode(node_ptr n)
			{
				destroy(&n->mValue);
				putNode(n);
			}

			void eraseBucket(const size_type n, node_ptr first, node_ptr last)
			{
				node_ptr cur = mBuckets[n];
				if (cur == first)
				{
					eraseBucket(n, last);
				}
				else
				{
					node_ptr next;
					for (next = cur->mNext;next != first;cur = next, next = cur->mNext)
						;

					while (next != last)
					{
						cur->mNext = next->mNext;
						deleteNode(next);
						next = cur->mNext;
						--mElementNum;
					}
				}
			}

			void eraseBucket(const size_type n, node_ptr last)
			{
				node_ptr cur = mBuckets[n];
				while (cur != last)
				{
					node_ptr next = cur->mNext;
					deleteNode(cur);
					cur = next;
					mBuckets[n] = cur;
					--mElementNum;
				}
			}

			/*!
			 * \remarks 拷贝
			 * \return 
			 * \param const HashTable & ht
			*/
			void copyFrom(const HashTable& ht)
			{
				//先清除己方的list的空间
				mBuckets.clear();
				//如果己方空间大于对方，就不动，如果己方空间小于对方，就会增大
				mBuckets.reserve(ht.mBuckets.size());
				//从己方的vector尾端开始，插入n个元素，其值为空指针，注意：此时的vector为空，所以所谓尾端，就是起头处
				mBuckets.insert(mBuckets.end(), ht.mBuckets.size(), nullptr);

				try
				{
					for (size_type i = 0; i < ht.mBuckets.size(); ++i)
					{
						//复制vector的每一个元素（是个指针，指向节点）
						const node_type* cur = ht.mBuckets[i];
						if (cur)
						{
							node_ptr cpy = newNode(cur->mValue);
							mBuckets[i] = cpy;

							//针对同一个list，复制每一个节点
							for (node_ptr next = cur->mNext; next; cur = next, next = cur->mNext)
							{
								cpy->mNext = newNode(next->mValue);
								cpy = cpy->mNext;
							}
						}
					}
					mElementNum = ht.mElementNum;
				}
				catch (...)
				{
					clear();
					throw;
				}
			}

			hasher getHasher() const
			{
				return mHasher;
			}

			key_equal getEqual() const
			{
				return mEqual;
			}

		private:
			node_ptr getNode()
			{
				return mNodeAllocator.allocate(1);
			}

			void putNode(node_ptr p)
			{
				mNodeAllocator.deallocate(p, 1);
			}

			alloc_type mNodeAllocator;
			size_type mElementNum;
			Vector<node_ptr> mBuckets;
			//函数对象
			hasher mHasher;
			key_equal mEqual;
			key_extract mGetKey;

			friend class HashTableIterator<Value, Key, HashFcn,ExtractKey, EqualKey, Alloc>;
			friend class HashTableConstIterator<Value, Key, HashFcn,ExtractKey, EqualKey, Alloc>;
			friend bool operator== (const HashTable&,const HashTable&);
		};

		template <typename Value, typename Key, typename HashFcn,typename ExtractKey, typename EqualKey, typename Alloc>
		bool operator==(const HashTable<Value, Key, HashFcn, ExtractKey, EqualKey, Alloc>& x,
									const HashTable<Value, Key, HashFcn, ExtractKey, EqualKey, Alloc>& y)
		{
			typedef typename HashTable<Value, Key, HashFcn, ExtractKey, EqualKey, Alloc>::node_type node;

			if (x.mBuckets.size() != y.mBuckets.size())
			{
				return false;
			}

			for (sfast n = 0; n < x.mBuckets.size(); ++n)
			{
				node* cur1 = x.mBuckets[n];
				node* cur2 = y.mBuckets[n];
				for (; cur1 && cur2 && cur1->mValue == cur2->mValue;cur1 = __cur1->mNext, cur2 = cur2->mNext)
				{
				}

				if (cur1 || cur2)
				{
					return false;
				}
	
			}

			return true;
		}

		template <typename Value, typename Key, typename HashFcn,typename ExtractKey, typename EqualKey, typename Alloc>
		inline void swap(HashTable<Value, Key, HashFcn, ExtractKey, EqualKey, Alloc>& x,HashTable<Value, Key, HashFcn, ExtractKey, EqualKey, Alloc>& y)
		{
			x.swap(y);
		}
	}//namespace utl
}//namespace ung

#endif//_UNG_UTL_HASHTABLE_H_