/*!
 * \brief
 * 对Boost库指针容器的包装
 * \file UFCPointerContainerWrapper.h
 *
 * \author Su Yang
 *
 * \date 2017/06/08
 */
#ifndef _UFC_POINTERCONTAINERWRAPPER_H_
#define _UFC_POINTERCONTAINERWRAPPER_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#ifndef _UFC_CHOOSECONTAINER_H_
#include "UFCChooseContainer.h"
#endif

#ifndef _INCLUDE_BOOST_PTRLIST_HPP_
#include "boost/ptr_container/ptr_list.hpp"
#define _INCLUDE_BOOST_PTRLIST_HPP_
#endif

#ifndef _INCLUDE_BOOST_PTRMAT_HPP_
#include "boost/ptr_container/ptr_map.hpp"
#define _INCLUDE_BOOST_PTRMAT_HPP_
#endif

namespace ung
{
	/*!
	 * \brief
	 * 对boost::ptr_list的包装
	 * \class PointerListWrapper
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/08
	 *
	 * \todo
	 */
	template<typename T>
	class PointerListWrapper : public POINTER_LIST(T)
	{
	public:
		template<typename TT>
		class BorrowImpl
		{
		public:
			using con_pointer = PointerListWrapper<TT>*;

			/*!
			 * \remarks 默认构造函数，需要定义
			 * \return 
			*/
			BorrowImpl(con_pointer pCon = nullptr) :
				mCon(pCon)
			{
			}

			/*!
			 * \remarks 调用运算符
			 * \return 
			 * \param TT* p 普通指针
			*/
			void operator()(TT* p)
			{
				BOOST_ASSERT(mCon && p);

				/*
				在borrow的这段时间，如果容器本身发生了变化，比如，容器可能被销毁了，那么mCon就
				成为了一个悬指针，当时通过一个悬指针去进行调用操作的话，应该会“崩”掉。
				*/

				//归还
				mCon->push_back(p);
			}

		private:
			con_pointer mCon;
		};//end class BorrowImpl

	public:
		/*
		用户不需要担心释放，因为是std::unique_ptr，但是这里的“释放”，其实是“归还”
		*/
		using borrow_type = std::unique_ptr<T, BorrowImpl<T>>;
		using iterators_type = std::pair<iterator,iterator>;
		using const_iterators_type = std::pair<const_iterator,const_iterator>;

		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		PointerListWrapper() = default;

		/*!
		 * \remarks 获取迭代器对
		 * \return 
		*/
		iterators_type getIterators()
		{
			return std::make_pair(begin(), end());
		}

		/*!
		 * \remarks 获取迭代器对
		 * \return 
		*/
		const_iterators_type getIterators() const
		{
			return std::make_pair(cbegin(), cend());
		}

		/*!
		 * \remarks 从指针容器中“暂借”一个指针出来，用完了会自动归还
		 * \return 
		 * \param T const& val 注意，不是查找指针
		*/
		borrow_type borrow(T const& val)
		{
			auto first = begin();
			auto last = end();

			while (first != last)
			{
				if (*first == val)
				{
					break;
				}

				++first;
			}

			if (first != end())
			{
				auto ap = release(first);
				auto p = ap.release();

				//返回对象
				borrow_type ret{p,BorrowImpl<T>(this)};

				return ret;
			}
			else
			{
				return borrow_type{};
			}
		}

		/*!
		 * \remarks 从指针容器中“暂借”一个指针出来，用完了会自动归还
		 * \return 
		 * \param iterator it 这里传拷贝和引用都可以。选择传递一个拷贝
		*/
		borrow_type borrow(iterator it)
		{
			if (it != end())
			{
				//必须在release()之前。
				//auto keyValue = (*it).first;

				auto ap = release(it);
				auto p = ap.release();

				//返回对象
				borrow_type ret{ p,BorrowImpl<T>(this) };

				return ret;
			}
			else
			{
				return borrow_type{};
			}
		}
	};//end class PointerListWrapper

	//------------------------------------------------------------------------------------------------

	/*!
	 * \brief
	 * 对boost::ptr_map的包装
	 * \class PointerMapWrapper
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/08
	 *
	 * \todo
	 */
	template<typename K,typename T>
	class PointerMapWrapper : public POINTER_MAP(K,T)
	{
	public:
		template<typename KK,typename TT>
		class BorrowImpl
		{
		public:
			using con_pointer = PointerMapWrapper<KK, TT>*;

			/*!
			 * \remarks 默认构造函数，不会被调用，但是需要定义
			 * \return 
			*/
			BorrowImpl() :
				mCon(nullptr)
			{
			}

			/*!
			 * \remarks 构造函数
			 * \return 
			 * \param con_pointer pCon 持有一个指向容器的指针
			 * \param KK& key 本身就是const的
			*/
			BorrowImpl(con_pointer pCon,KK& key) :
				mCon(pCon),
				mKey(key)
			{
			}

			/*!
			 * \remarks 调用运算符
			 * \return 
			 * \param TT* p 普通指针
			*/
			void operator()(TT* p)
			{
				BOOST_ASSERT(mCon && p);

				//归还
				auto ret = mCon->insert(mKey, p);

				//说明在borrow的这段时间，容器本身发生了变化，比如，容器可能被销毁了
				if (!(ret.second))
				{
					UNG_EXCEPTION("Failed to return pointer to ptr_container.");
				}
			}

		private:
			con_pointer mCon;
			//K本身就是const的
			KK mKey;
		};//end class BorrowImpl

	public:
		/*
		用户不需要担心释放，因为是std::unique_ptr，但是这里的“释放”，其实是“归还”
		*/
		using borrow_type = std::unique_ptr<T, BorrowImpl<K, T>>;
		using iterators_type = std::pair<iterator,iterator>;
		using const_iterators_type = std::pair<const_iterator,const_iterator>;

		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		PointerMapWrapper()
		{
		}

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param InputIterator first
		 * \param InputIterator last
		*/
		template<typename InputIterator>
		PointerMapWrapper(InputIterator first, InputIterator last) :
			ptr_map(first,last)
		{
		}

		/*!
		 * \remarks 获取迭代器对
		 * \return 
		*/
		iterators_type getIterators()
		{
			return std::make_pair(begin(), end());
		}

		/*!
		 * \remarks 获取迭代器对
		 * \return 
		*/
		const_iterators_type getIterators() const
		{
			return std::make_pair(cbegin(), cend());
		}

		/*!
		 * \remarks 从指针容器中“暂借”一个指针出来，用完了会自动归还
		 * \return 
		 * \param K& key 这里K本身就是const的
		*/
		borrow_type borrow(K& key)
		{
			auto it = find(key);

			if (it != end())
			{
				auto ap = release(it);
				auto p = ap.release();

				//返回对象
				borrow_type ret{p,BorrowImpl<K,T>(this, key)};

				return ret;
			}
			else
			{
				return borrow_type{};
			}
		}

		/*!
		 * \remarks 从指针容器中“暂借”一个指针出来，用完了会自动归还
		 * \return 
		 * \param iterator it 这里传拷贝和引用都可以。选择传递一个拷贝
		*/
		borrow_type borrow(iterator it)
		{
			if (it != end())
			{
				//必须在release()之前。
				auto keyValue = (*it).first;

				auto ap = release(it);

				auto p = ap.release();

				//返回对象
				borrow_type ret{ p,BorrowImpl<K,T>(this,keyValue) };

				return ret;
			}
			else
			{
				return borrow_type{};
			}
		}
	};//end class PointerMapWrapper
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_POINTERCONTAINERWRAPPER_H_