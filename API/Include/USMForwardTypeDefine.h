/*!
 * \brief
 * 前置类型定义。
 * \file USMForwardTypeDefine.h
 *
 * \author Su Yang
 *
 * \date 2017/03/07
 */

#ifndef _USM_FORWARDTYPEDEFINE_H_
#define _USM_FORWARDTYPEDEFINE_H_

#ifndef _USM_CONFIG_H_
#include "USMConfig.h"
#endif

#ifdef USM_HIERARCHICAL_COMPILE

namespace ung
{
	class Object;
	using StrongObjectPtr = std::shared_ptr<Object>;
	using WeakObjectPtr = std::weak_ptr<Object>;

	class ObjectComponent;
	using StrongObjectComponentPtr = std::shared_ptr<ObjectComponent>;

	class ObjectFactory;
	using UniqueObjectFactoryPtr = std::unique_ptr<ObjectFactory>;

	class SceneNode;
	using StrongSceneNodePtr = std::shared_ptr<SceneNode>;
	using WeakSceneNodePtr = std::weak_ptr<SceneNode>;

	class SceneManagerBase;
	using StrongSceneManager = std::shared_ptr<SceneManagerBase>;
	using WeakSceneManager = std::weak_ptr<SceneManagerBase>;
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE

#endif//_USM_FORWARDTYPEDEFINE_H_