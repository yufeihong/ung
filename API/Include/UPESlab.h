/*!
 * \brief
 * Kay–Kajiya平行平面空间包围体(用于加速计算光线-物体的相交测试)
 * \file UPESlab.h
 *
 * \author Su Yang
 *
 * \date 2017/04/06
 */
#ifndef _UPE_SLAB_H_
#define _UPE_SLAB_H_

#ifndef _UPE_CONFIG_H_
#include "UPEConfig.h"
#endif

#ifdef UPE_HIERARCHICAL_COMPILE

#ifndef _UIF_IBOUNDINGVOLUME_H_
#include "UIFIBoundingVolume.h"
#endif

namespace ung
{
	class UngExport Slab : public IBoundingVolume
	{
	private:
		Vector3 mNormal;
		real_type mNear;
		real_type mFar;
	};
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

#endif//_UPE_SLAB_H_