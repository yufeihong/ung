/*!
 * \brief
 * 警告。
 * \file UBSWarning.h
 *
 * \author Su Yang
 *
 * \date 2017/03/12
 */
#ifndef _UBS_WARNING_H_
#define _UBS_WARNING_H_

//在导出类中使用STL
#pragma warning(disable:4251)

#endif//_UBS_WARNING_H_