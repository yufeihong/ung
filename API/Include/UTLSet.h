/*!
 * \brief
 * Set
 * \file UTLSet.h
 *
 * \author Su Yang
 *
 * \date 2016/04/05
 */
#ifndef _UNG_UTL_SET_H_
#define _UNG_UTL_SET_H_

#ifndef _UNG_UTL_CONFIG_H_
#include "UTLConfig.h"
#endif

#ifndef _UNG_UTL_ALLOCATOR_H_
#include "UTLAllocator.h"
#endif

#ifndef _UNG_UTL_RBTREE_H_
#include "UTLRBTree.h"
#endif

namespace ung
{
	namespace utl
	{
		/*!
		 * \brief
		 * Set
		 * \class Set
		 *
		 * \author Su Yang
		 *
		 * \date 2016/04/05
		 *
		 * \todo
		 */
		template<typename Key,typename Compare = std::less<Key>>
		class Set
		{
		public:
			using key_type = Key;
			using value_type = Key;
			using key_compare = Compare;
			using value_compare = key_compare;
			using self_type = Set<key_type, key_compare>;

		private:
			using rep_type = RBTree<key_type, value_type, std::identity<value_type>, key_compare>;

		public:
			using pointer = typename rep_type::const_pointer;
			using const_pointer = typename rep_type::const_pointer;
			using reference = typename rep_type::const_reference;
			using const_reference = typename rep_type::const_reference;
			using iterator = typename rep_type::const_iterator;
			using const_iterator = typename rep_type::const_iterator;
			using reverse_iterator = typename rep_type::const_reverse_iterator;
			using const_reverse_iterator = typename rep_type::const_reverse_iterator;
			using size_type = typename rep_type::size_type;
			using difference_type = typename rep_type::difference_type;

			/*!
			 * \remarks 默认构造函数
			 * \return 
			*/
			Set() :
				mTree(key_compare())
			{
			}

			/*!
			 * \remarks 构造函数
			 * \return 
			 * \param key_compare const & comp
			*/
			explicit Set(key_compare const& comp) :
				mTree(comp)
			{
			}

			/*!
			 * \remarks 构造函数
			 * \return 
			 * \param InputIterator first
			 * \param InputIterator last
			*/
			template<typename InputIterator>
			Set(InputIterator first, InputIterator last) :
				mTree(key_compare())
			{
				mTree.insertUnique(first, last);
			}

			/*!
			 * \remarks 构造函数
			 * \return 
			 * \param InputIterator first
			 * \param InputIterator last
			 * \param key_compare const & comp
			*/
			template<typename InputIterator>
			Set(InputIterator first, InputIterator last, key_compare const& comp) :
				mTree(comp)
			{
				mTree.insertUnique(first, last);
			}

			/*!
			 * \remarks 拷贝构造函数
			 * \return 
			 * \param self_type const & x
			*/
			Set(self_type const& x) :
				mTree(x.mTree)
			{
			}

			/*!
			 * \remarks 拷贝赋值运算符
			 * \return 
			 * \param self_type const & x
			*/
			self_type& operator=(self_type const& x)
			{
				mTree = x.mTree;

				return *this;
			}

			/*!
			 * \remarks 移动构造函数
			 * \return 
			 * \param self_type&& right
			*/
			Set(self_type&& right) noexcept :
				mTree(std::move(right.mTree))
			{
			}

			/*!
			 * \remarks 移动赋值运算符
			 * \return 
			 * \param self_type&& right
			*/
			self_type& operator=(self_type&& right) noexcept
			{
				mTree = std::move(right.mTree);

				return *this;
			}

			/*!
			 * \remarks 构造函数
			 * \return 
			 * \param std::initializer_list<value_type> il
			*/
			Set(std::initializer_list<value_type> il) :
				mTree(key_compare())
			{
				this->insert(il.begin(), il.end());
			}

			/*!
			 * \remarks 构造函数
			 * \return 
			 * \param std::initializer_list<value_type> il
			 * \param const key_compare & comp
			*/
			Set(std::initializer_list<value_type> il,const key_compare& comp) :
				mTree(comp)
			{
				this->insert(il.begin(), il.end());
			}

			/*!
			 * \remarks 重载赋值运算符
			 * \return 
			 * \param std::initializer_list<value_type> il
			*/
			self_type& operator=(std::initializer_list<value_type> il)
			{
				clear();

				insert(il.begin(), il.end());

				return *this;
			}

			/*!
			 * \remarks 获取指向首元素的迭代器
			 * \return 
			*/
			iterator begin() const
			{
				return mTree.begin();
			}

			/*!
			 * \remarks 获取指向尾后位置的迭代器
			 * \return 
			*/
			iterator end() const
			{
				return mTree.end();
			}

			/*!
			 * \remarks 获取指向尾后位置的迭代器
			 * \return 
			*/
			reverse_iterator rbegin() const
			{
				return mTree.rbegin();
			}

			/*!
			 * \remarks 获取指向首元素的迭代器
			 * \return 
			*/
			reverse_iterator rend() const
			{
				return mTree.rend();
			}

			/*!
			 * \remarks 是否为空
			 * \return 
			*/
			bool empty() const
			{
				return mTree.empty();
			}

			/*!
			 * \remarks 获取元素数量
			 * \return 
			*/
			size_type size() const
			{
				return mTree.size();
			}

			/*!
			 * \remarks 交换
			 * \return 
			 * \param self_type & x
			*/
			void swap(self_type& x)
			{
				mTree.swap(x.mTree);
			}

			/*!
			 * \remarks 插入
			 * \return 
			 * \param value_type && v
			*/
			std::pair<iterator, bool> insert(value_type&& v)
			{
				return mTree.insertUnique(std::forward<value_type>(v));
			}

			/*!
			 * \remarks 插入
			 * \return 
			 * \param const_iterator pos
			 * \param value_type && v
			*/
			iterator insert(const_iterator pos, value_type&& v)
			{
				return mTree.insertUnique(pos, std::forward<value_type>(v));
			}

			/*!
			 * \remarks 插入
			 * \return 
			 * \param const_reference x
			*/
			std::pair<iterator, bool> insert(const_reference x)
			{
				std::pair<typename rep_type::iterator, bool> p = mTree.insertUnique(x);

				//return std::pair<iterator, bool>(p.first, p.second);
				iterator a = p.first;
				bool b = p.second;
				return std::pair<iterator, bool>(a, b);
			}

			/*!
			 * \remarks 插入
			 * \return 
			 * \param iterator position
			 * \param const_reference x
			*/
			iterator insert(iterator position, const_reference x)
			{
				return mTree.insertUnique((typename rep_type::iterator&)position,x);
			}

			/*!
			 * \remarks 插入
			 * \return 
			 * \param InputIterator first
			 * \param InputIterator last
			*/
			template<typename InputIterator>
			void insert(InputIterator first, InputIterator last)
			{
				mTree.insertUnique(first, last);
			}

			/*!
			 * \remarks 插入
			 * \return 
			 * \param std::initializer_list<value_type> il
			*/
			void insert(std::initializer_list<value_type> il)
			{
				insert(il.begin(), il.end());
			}

			void erase(iterator position)
			{
				mTree.erase((typename rep_type::iterator&)position);
			}

			size_type erase(key_type const& x)
			{
				return mTree.erase(x);
			}

			void erase(iterator first, iterator last)
			{
				mTree.erase((typename rep_type::iterator&)first,(typename rep_type::iterator&)last);
			}

			void clear()
			{
				mTree.clear();
			}

			iterator find(key_type const& x) const
			{
				return mTree.find(x);
			}

			size_type count(key_type const& x) const
			{
				return mTree.count(x);
			}

			iterator lowerBound(key_type const& x) const
			{
				return mTree.lowerBound(x);
			}

			iterator upperBound(key_type const& x) const
			{
				return mTree.upperBound(x);
			}

			std::pair<iterator,iterator> equalRange(key_type const& x) const
			{
				return mTree.equalRange(x);
			}

		private:
			rep_type mTree;										//采用红黑树来表现Set

			friend bool operator==(Set const& x, Set const& y);
			friend bool operator<(Set const& x, Set const& y);
			friend void swap(Set& x, Set& y);
		};

		template<typename Key,typename Compare>
		inline bool operator==(Set<Key, Compare> const& x, Set<Key, Compare> const& y)
		{
			return x.mTree == y.mTree;
		}

		template<typename Key, typename Compare>
		inline bool operator<(Set<Key, Compare> const& x, Set<Key, Compare> const& y)
		{
			return x.mTree < y.mTree;
		}

		template<typename Key, typename Compare>
		inline void swap(Set<Key, Compare>& x,Set<Key, Compare>& y)
		{
			x.swap(y);
		}
	}//namespace utl
}//namespace ung

#endif//_UNG_UTL_SET_H_