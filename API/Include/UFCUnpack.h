/*!
 * \brief
 * 解包
 * \file UFCUnpack.h
 *
 * \author Su Yang
 *
 * \date 2016/12/20
 */
#ifndef _UFC_UNPACK_H_
#define _UFC_UNPACK_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#ifndef _UFC_CHOOSECONTAINER_H_
#include "UFCChooseContainer.h"
#endif

namespace ung
{
	class UnpackHelperInfo;

	/*!
	 * \brief
	 * 解包
	 * \class Unpack
	 *
	 * \author Su Yang
	 *
	 * \date 2016/12/20
	 *
	 * \todo
	 */
	class UngExport Unpack
	{
	public:
		/*!
		 * \remarks 解压给定包到磁盘
		 * \return 
		 * \param const char* packName
		*/
		static void unpackToDisk(const char* packName);

		//-------------------------------------------------------------------------------------------

		/*!
		 * \remarks 获取压缩包内所有文件的详细信息(客户代码每构造一个包，调用一次来持有包的解析信息)
		 * \return 
		 * \param const char* packName
		 * \param UnpackHelperInfo& helper
		*/
		static void analysisPack(const char* packName,UnpackHelperInfo& helper);

		/*!
		 * \remarks 提取给定文件到内存
		 * \return 
		 * \param UnpackHelperInfo const & helper 包的解析信息
		 * \param const char * fileName 要提取的文件名
		*/
		static std::pair<std::shared_ptr<char>,int64> extractToMemory(UnpackHelperInfo const& helper, const char* fileName);
	};
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_UNPACK_H_