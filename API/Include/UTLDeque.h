/*!
 * \brief
 * 双端队列。
 * \file UTLDeque.h
 *
 * \author Su Yang
 *
 * \date 2016/02/18
 */
#ifndef _UNG_UTL_DEQUE_H_
#define _UNG_UTL_DEQUE_H_

#ifndef _UNG_UTL_CONFIG_H_
#include "UTLConfig.h"
#endif

#ifndef _UNG_UTL_ALLOCATOR_H_
#include "UTLAllocator.h"
#endif

#ifndef _UNG_UTL_DEQUEITERATOR_H_
#include "UTLDequeIterator.h"
#endif

#ifndef _UNG_UTL_CONSTRUCT_H_
#include "UTLConstruct.h"
#endif

namespace ung
{
	namespace utl
	{
		/*!
		 * \brief
		 * 双端队列
		 * \class Deque
		 *
		 * \author Su Yang
		 *
		 * \date 2016/02/22
		 *
		 * \todo
		 */
		template<typename T,typename Alloc = Allocator<T>,usize BufSize = 0>
		class Deque
		{
		public:
			using value_type = T;
			using pointer = value_type*;
			using reference = value_type&;
			using const_reference = value_type const&;
			using self_type = Deque<value_type, Alloc, BufSize>;

			using iterator = DequeIterator<value_type, BufSize>;
			using const_iterator = DequeIterator<value_type const, BufSize>;
			typedef std::reverse_iterator<iterator> reverse_iterator;
			typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

			/*!
			 * \remarks 默认构造函数
			 * \return
			*/
			Deque() :
				mStart(),
				mFinish(),
				mMap(nullptr),
				mMapSize(0)
			{
			}

			/*!
			 * \remarks 构造函数，值为：value_type()
			 * \return
			 * \param usize count 数量
			*/
			explicit Deque(usize count) :
				Deque(count, value_type())
			{
			}

			/*!
			 * \remarks 构造函数
			 * \return
			 * \param int n 数量
			 * \param value_type const & value 值
			*/
			Deque(int n, value_type const& value) :
				mStart(),
				mFinish(),
				mMap(nullptr),
				mMapSize(0)
			{
				initialize(n, value);
			}

			/*!
			 * \remarks 拷贝构造函数
			 * \return
			 * \param Deque const & right
			*/
			Deque(self_type const& right)
			{
				initialize(right.begin(), right.end());
			}

			/*!
			 * \remarks 拷贝赋值运算符
			 * \return 
			 * \param self_type const & right
			*/
			self_type& operator=(self_type const& right)
			{
				if (this != &right)
				{
					tidy();

					mMap = right.mMap;
					mMapSize = right.mMapSize;
					mStart = right.mStart;
					mFinish = right.mFinish;
				}

				return *this;
			}

			/*!
			 * \remarks 构造函数，从[first,last)构造
			 * \return
			 * \param Iter first
			 * \param Iter last
			*/
			template<typename Iter, class = typename std::enable_if<std::_Is_iterator<Iter>::value, void>::type>
			Deque(Iter first, Iter last)
			{
				initialize(first, last);
			}

			/*!
			 * \remarks 构造函数，移动right
			 * \return
			 * \param self_type & & right
			*/
			Deque(self_type&& right) noexcept :
			mMap(right.mMap),
				mMapSize(right.mMapSize),
				mStart(right.mStart),
				mFinish(right.mFinish)
			{
				right.mMap = nullptr;
				right.mMapSize = 0;
				right.mStart = nullptr;
				right.mFinish = nullptr;
			}

			/*!
			 * \remarks 移动赋值运算符
			 * \return 
			 * \param self_type & & right
			*/
			self_type& operator=(self_type&& right) noexcept
			{
				if (this != &right)
				{
					tidy();

					//从right接管资源
					mMap = right.mMap;
					mMapSize = right.mMapSize;
					mStart = right.mStart;
					mFinish = right.mFinish;

					//将right置于可析构状态
					right.mMap = nullptr;
					right.mMapSize = 0;
					right.mStart = nullptr;
					right.mFinish = nullptr;
				}

				return *this;
			}

			/*!
			 * \remarks 构造函数
			 * \return 
			 * \param std::initializer_list<value_type> il 初始化列表
			*/
			Deque(std::initializer_list<value_type> il)
			{
				initialize(il.begin(), il.end());
			}

			/*!
			 * \remarks 重载赋值运算符
			 * \return 
			 * \param std::initializer_list<value_type> il 初始化列表
			*/
			self_type& operator=(std::initializer_list<value_type> il)
			{
				assign(il.begin(), il.end());

				return *this;
			}

			/*!
			 * \remarks 析构函数
			 * \return 
			*/
			~Deque() noexcept
			{
				tidy();
			}

			/*!
			 * \remarks 赋值
			 * \return 
			 * \param usize count 数量
			 * \param const value_type& val 元素值
			*/
			void assign(usize count, const value_type& val)
			{
				clear();

				ssize c = count;
				while (c-- > 0)
				{
					push_back(val);
				}
			}

			/*!
			 * \remarks 赋值
			 * \return 
			 * \param Iter first 区间开始
			 * \param Iter last 区间结束
			*/
			template<class Iter>
			typename std::enable_if<std::_Is_iterator<Iter>::value,void>::type assign(Iter first, Iter last)
			{
				clear();

				for (; first != last; ++first)
				{
					emplace_back(*first);
				}
			}

			/*!
			 * \remarks 赋值
			 * \return 
			 * \param std::initializer_list<value_type> il 初始化列表
			*/
			void assign(std::initializer_list<value_type> il)
			{
				assign(il.begin(), il.end());
			}

			/*!
			 * \remarks 在双端队列的第一个元素之前放置一个新的元素
			 * \return 
			 * \param value_type const & value
			*/
			void push_front(value_type const& value)
			{
				if (mStart.mCur != mStart.mFirst)
				{
					//第一缓冲区尚有备用空间
					construct(mStart.mCur - 1, value);
					--mStart.mCur;
				}
				else
				{
					//第一缓冲区已无备用空间
					push_front_aux(value);
				}
			}

			/*!
			 * \remarks 在双端队列的最后一个元素之后放置一个新的元素
			 * \return 
			 * \param value_type const & value
			*/
			void push_back(value_type const& value)
			{
				if (mFinish.mCur != mFinish.mLast - 1)
				{
					//最后缓冲区尚有两个(含)以上的元素备用空间
					construct(mFinish.mCur, value);
					++mFinish.mCur;
				}
				else
				{
					//最后缓冲区只剩下一个元素备用空间
					push_back_aux(value);
				}
			}

			/*!
			 * \remarks 在第一个元素之前构造一个新的元素
			 * \return 
			 * \param Arguments&& ... args
			*/
			template<typename... Arguments>
			void emplace_front(Arguments&&... args)
			{
				if (mStart.mCur != mStart.mFirst)
				{
					//第一缓冲区尚有备用空间
					construct(mStart.mCur - 1, std::forward<Arguments>((args)...));
					--mStart.mCur;
				}
				else
				{
					//第一缓冲区已无备用空间
					emplace_front_aux(std::forward<Arguments>((args)...));
				}
			}

			/*!
			 * \remarks 在最后一个元素之后构造一个新的元素
			 * \return 
			 * \param Arguments && ... args
			*/
			template<typename... Arguments>
			void emplace_back(Arguments&&... args)
			{
				if (mFinish.mCur != mFinish.mLast - 1)
				{
					//最后缓冲区尚有两个(含)以上的元素备用空间
					construct(mFinish.mCur, std::forward<Arguments>((args)...));
					++mFinish.mCur;
				}
				else
				{
					//最后缓冲区只剩下一个元素备用空间
					emplace_back_aux(std::forward<Arguments>((args)...));
				}
			}

			/*!
			 * \remarks 在迭代器where指向的元素之前创建一个由args构建的元素
			 * \return 返回指向新添加的元素的迭代器
			 * \param const_iterator where
			 * \param Arguments && ... args
			*/
			template<typename... Arguments>
			iterator emplace(const_iterator where,Arguments&&... args)
			{
				usize off = where - begin();

				usize sz = this->size();

				if (sz < off)
				{
					//抛出异常
				}

				//离头比较近，push到front然后rotate
				if (off <= this->sz / 2)
				{
					emplace_front(std::forward<Arguments>(args)...);
					std::rotate(begin(), begin() + 1, begin() + 1 + off);
				}
				//离尾比较近，push到back然后rotate
				else
				{
					emplace_back(std::forward<Arguments>(args)...);
					std::rotate(begin() + off, end() - 1, end());
				}

				return (begin() + off);
			}

			/*!
			* \remarks 在pos之前插入一个元素
			* \return 返回指向新添加的元素的迭代器
			* \param iterator pos
			* \param value_type const & elem
			*/
			iterator insert(const_iterator pos, value_type const& elem)
			{
				//如果插入点是deque最前端，交给push_front去做
				if (pos.mCur == mStart.mCur)
				{
					push_front(elem);

					return mStart;
				}
				//如果插入点是deque最尾端，交给push_back去做
				else if (pos.mCur == mFinish.mCur)
				{
					push_back(elem);
					iterator it = mFinish;
					--it;

					return it;
				}
				else
				{
					return insert_aux(pos, elem);
				}
			}

			/*!
			 * \remarks 在迭代器where指向的元素之前创建一个值为elem的元素
			 * \return 返回指向新添加的元素的迭代器
			 * \param const_iterator where
			 * \param value_type&& elem 右值引用
			*/
			iterator insert(const_iterator where, value_type&& elem)
			{
				return emplace(where, std::move(elem));
			}

			template<class Iter>
			typename std::enable_if<std::_Is_iterator<Iter>::value,iterator>::type insert(const_iterator where, Iter first, Iter last)
			{
				usize off = where - begin();

				usize sz = this->size();
				usize oldsize = sz;

				if (sz < off)
				{
					//抛出异常
				}

				if (first == last)
					;
				//临近头部，push到front然后rotate
				else if (off <= sz / 2)
				{
					for (; first != last; ++first)
					{
						push_front(*first);
					}

					usize num = last - first;
					/*
					把上面push_front的给反转一下。
					template<class BidirectionalIterator>
					void reverse(BidirectionalIterator _First,BidirectionalIterator _Last);
					Reverses(反转，颠倒) the order of the elements within a range.
					_First,A bidirectional iterator pointing to the position of the first element in the range within which the elements are being permuted.
					_Last,A bidirectional iterator pointing to the position one past the final element in the range within which the elements are being permuted.
					*/
					std::reverse(begin(), begin() + num);

					std::rotate(begin(), begin() + num, begin() + num + off);
				}
				//临近尾部，push到back然后rotate
				else
				{
					for (; first != last; ++first)
					{
						push_back(*first);
					}

					std::rotate(where, begin() + oldsize, end());
				}

				return where;
			}

			/*!
			 * \remarks 在迭代器where指向的元素之前插入初始化列表
			 * \return 返回指向新添加的元素的迭代器
			 * \param const_iterator where
			 * \param std::initializer_list<value_type> il 初始化列表
			*/
			iterator insert(const_iterator where,std::initializer_list<value_type> il)
			{
				return insert(where, il.begin(), il.end());
			}

			/*!
			 * \remarks 删除双端队列的最后一个元素
			 * \return 
			*/
			void pop_back()
			{
				if (mFinish.mCur != mFinish.mFirst)
				{
					//最后缓冲区有一个(或更多)元素
					destroy(mFinish.mCur--);
				}
				else
				{
					//最后缓冲区没有任何元素
					pop_back_aux();
				}
			}

			/*!
			 * \remarks 删除双端队列的第一个元素
			 * \return 
			*/
			void pop_front()
			{
				if (mStart.mCur != mStart.mLast - 1)
				{
					//第一缓冲区有两个(或更多)元素
					destroy(mStart.mCur++);
				}
				else
				{
					//第一缓冲区仅有一个元素
					pop_front_aux();
				}
			}

			/*!
			 * \remarks 获取双端队列的第一个元素
			 * \return 常量引用
			*/
			const_reference front() const
			{
				return *begin();
			}

			/*!
			 * \remarks 获取双端队列的第一个元素
			 * \return 引用
			*/
			reference front()
			{
				return *mStart;
			}

			/*!
			 * \remarks 获取双端队列的最后一个元素
			 * \return 常量引用
			*/
			const_reference back() const
			{
				return *(end() - 1);
			}

			/*!
			 * \remarks 获取双端队列的最后一个元素
			 * \return 引用
			*/
			reference back()
			{
				return *(mFinish - 1);
			}

			/*!
			 * \remarks 获取给定位置上的元素，位置不合法抛出异常
			 * \return 常量引用
			 * \param usize pos
			*/
			const_reference at(usize pos) const
			{
				if (this->size() <= pos)
				{
					//抛出异常
				}

				return *(begin() + pos);
			}

			/*!
			 * \remarks 获取给定位置上的元素，位置不合法抛出异常
			 * \return 引用
			 * \param usize pos
			*/
			reference at(usize pos)
			{
				if (this->size() <= pos)
				{
					//抛出异常
				}

				return *(begin() + pos);
			}

			/*!
			 * \remarks 重载[]运算符
			 * \return 常量引用
			 * \param usize pos
			*/
			const_reference operator[](usize pos) const
			{
				return (*(begin() + pos));
			}

			/*!
			 * \remarks 重载[]运算符
			 * \return 引用
			 * \param usize pos
			*/
			reference operator[](usize pos)
			{
				return mStart[difference_type(pos)];
			}

			/*!
			 * \remarks 元素个数
			 * \return 
			*/
			usize size() const
			{
				return mFinish - mStart;
			}

			/*!
			 * \remarks 是否为空
			 * \return 
			*/
			bool empty() const
			{
				return mFinish == mStart;
			}

			/*!
			 * \remarks 清除pos所指的元素。pos为清除点
			 * \return 
			 * \param iterator pos
			*/
			iterator erase(iterator pos)
			{
				iterator next = pos;
				++next;

				//清除点之前的元素个数
				difference_type num = pos - mStart;

				//如果清除点之前的元素比较少，就移动清除点之前的元素
				if (num < (size() >> 1))
				{
					std::copy_backward(mStart, pos, next);
					//移动完毕，最前一个元素冗余，去除之
					pop_front();
				}
				//如果清除点之后的元素比较少，就移动清除点之后的元素
				else
				{
					std::copy(next, mFinish, pos);
					//移动完毕，最后一个元素冗余，去除之
					pop_back();
				}

				return mStart + num;
			}

			/*!
			 * \remarks 清除[first,last)区间内的所有元素
			 * \return 
			 * \param iterator first
			 * \param iterator last
			*/
			iterator erase(iterator first, iterator last)
			{
				const usize bs = bufSize(BufSize, sizeof(value_type));

				//如果清除区间就是整个deque，直接调用clear()即可
				if (first == mStart && last == mFinish)
				{
					clear();
					return mFinish;
				}

				difference_type n = last - first;							//清除区间的长度
				difference_type n_before = first - mStart;			//清除区间前方的元素个数

				//如果前方的元素比较少
				if (n_before < (size() - n) / 2)
				{
					//向后移动前方元素(覆盖清除区间)
					std::copy_backward(mStart, first, last);

					//标记deque的新起点
					iterator newStart = mStart + n;

					//移动完毕，将冗余的元素析构
					destroy(mStart, newStart);

					//将冗余的缓冲区释放
					for (map_pointer cur = mStart.mNode; cur < newStart.mNode; ++cur)
					{
						mDataAllocator.deallocate(*cur, bs);
					}

					//设定deque的新起点
					mStart = newStart;
				}
				//如果清除区间后方的元素比较少
				else
				{
					//向前移动后方元素(覆盖清除区间)
					copy(last, mFinish, first);

					//标记deque的新尾点
					iterator newFinish = mFinish - n;

					//移动完毕，将冗余的元素析构
					destroy(newFinish, mFinish);

					//将冗余的缓冲区释放
					for (map_pointer cur = newFinish.mNode + 1; cur <= mFinish.mNode; ++cur)
					{
						mDataAllocator.deallocate(*cur, bs);
					}

					//设定deque的新尾点
					mFinish = newFinish;
				}

				return mStart + n_before;
			}

			/*!
			 * \remarks 清除整个deque。deque的最初状态(无任何元素时)保有一个缓冲区，因此，清除完成后恢复到初始状态，也一样要保留一个缓冲区。这是deque的策略。
			 * \return 
			*/
			void clear()
			{
				const usize bs = bufSize(BufSize, sizeof(value_type));

				//以下针对头尾以外的每一个缓冲区(它们一定都是饱满的)
				for (map_pointer node = mStart.mNode + 1; node < mFinish.mNode; ++ node)
				{
					//将缓冲区内的所有元素析构
					destroy(*node, *node + bs);
					
					//释放缓冲区内存
					mDataAllocator.deallocate(*node, bs);
				}

				//至少有头尾两个缓冲区
				if (mStart.mNode != mFinish.mNode)
				{
					//将头缓冲区的目前所有元素析构
					destroy(mStart.mCur, mStart.mLast);
					//将尾缓冲区的目前所有元素析构
					destroy(mFinish.mFirst, mFinish.mCur + 1);

					//释放尾缓冲区。注意：头缓冲区保留
					mDataAllocator.deallocate(mFinish.mFirst, bs);
				}
				//只有一个缓冲区
				else
				{
					//将此唯一缓冲区内的所有元素析构
					destroy(mStart.mCur, mFinish.mCur + 1);
					
					//注意：并不释放缓冲区空间，这唯一的缓冲区将保留
				}

				//调整状态
				mFinish = mStart;
			}

			/*!
			 * \remarks 与right交换内容
			 * \return 
			 * \param self_type & right
			*/
			void swap(self_type& right)
			{
				if (this == &right)
					;
				else
				{
					//认为是same allocator, swap control information
					std::swap(mMap, right.mMap);
					std::swap(mMapSize, right.mMapSize);
					std::swap(mStart, right.mStart);
					std::swap(mFinish, right.mFinish);
				}
			}

			/*!
			 * \remarks 获取指向第一个元素的迭代器
			 * \return 
			*/
			iterator begin()
			{
				return mStart;
			}

			/*!
			 * \remarks 获取指向第一个元素的常量迭代器，常量对象调用
			 * \return 
			*/
			const_iterator begin() const
			{
				return mStart;
			}

			/*!
			 * \remarks 获取指向第一个元素的常量迭代器
			 * \return 
			*/
			const_iterator cbegin() const
			{
				return mStart;
			}

			/*!
			 * \remarks 获取逆序队列的第一个元素的前一位置的迭代器
			 * \return 
			*/
			reverse_iterator rbegin()
			{
				return reverse_iterator(end());
			}

			/*!
			 * \remarks 获取逆序队列的第一个元素的前一位置的常量迭代器，常量对象调用
			 * \return 
			*/
			const_reverse_iterator rbegin() const
			{
				return const_reverse_iterator(end());
			}

			/*!
			 * \remarks 获取逆序队列的第一个元素的前一位置的迭代器
			 * \return 
			*/
			const_reverse_iterator crbegin() const
			{
				return rbegin();
			}

			/*!
			 * \remarks 获取指向最后一个元素下一位置的迭代器
			 * \return 
			*/
			iterator end()
			{
				return mFinish;
			}

			/*!
			 * \remarks 获取指向最后一个元素下一位置的常量迭代器，常量对象调用
			 * \return 
			*/
			const_iterator end() const
			{
				return mFinish;
			}

			/*!
			 * \remarks 获取指向最后一个元素下一位置的常量迭代器
			 * \return 
			*/
			const_iterator cend() const
			{
				return mFinish;
			}

			/*!
			 * \remarks 获取逆序队列的最后一个元素的迭代器
			 * \return 
			*/
			reverse_iterator rend()
			{
				return reverse_iterator(begin());
			}

			/*!
			 * \remarks 获取逆序队列的最后一个元素的常量迭代器，常量对象调用
			 * \return 
			*/
			const_reverse_iterator rend() const
			{
				return const_reverse_iterator(begin());
			}

			/*!
			 * \remarks 获取逆序队列的最后一个元素的常量迭代器
			 * \return 
			*/
			const_reverse_iterator crend() const
			{
				return rend();
			}

		private:
			using map_pointer = pointer*;

			using map_allocator = Allocator<pointer>;
			map_allocator mMapAllocator;
			using data_allocator = Allocator<value_type>;
			data_allocator mDataAllocator;

			/*!
			 * \remarks 负责产生并安排好deque的结构，并将元素的初值设定妥当
			 * \return 
			 * \param usize num 数量
			 * \param value_type const & value 值
			*/
			void initialize(usize num, value_type const& value)
			{
				/*
				把deque的结构都产生并安排好。
				*/
				build(num);

				map_pointer cur;
				//为每个节点的缓冲区设定初值
				for (cur = mStart.mNode; cur < mFinish.mNode; ++cur)
				{
					std::uninitialized_fill(*cur, *cur + bufSize(BufSize, sizeof(value_type)), value);
				}
				//最后一个节点的设定稍有不同(因为尾端可能有备用空间，不必设初值)
				uninitialized_fill(mFinish.mFirst, mFinish.mCur, value);
			}

			/*!
			 * \remarks 负责产生并安排好deque的结构，并将元素的初值设定妥当
			 * \return 
			 * \param Iter first 区间起始
			 * \param Iter last 区间结束
			*/
			template<class Iter>
			void initialize(Iter first, Iter last)
			{
				build(last - first);

				usize bs = bufSize(BufSize, sizeof(value_type));

				map_pointer cur;
				for (cur = mStart.mNode; cur < mFinish.mNode; ++cur)
				{
					/*
					区间并不一定就是从某个缓冲区的第一个元素开始的，所以，在给目标缓冲区设定值的时候，区间所表示的源可能会跨越缓冲区。
					*/
					for (pointer elemIndex = *cur; elemIndex < *cur + bs && first != last; ++elemIndex,++first)
					{
						*elemIndex = *first;
					}
				}
			}

			/*!
			 * \remarks 负责产生并安排好deque的结构
			 * \return 
			 * \param usize num
			*/
			void build(usize num)
			{
				/*
				需要节点数为：(元素个数 / 每个缓冲区可容纳的元素个数) + 1
				如果刚好整除，会多配一个节点
				*/
				usize nodeNum = num / bufSize(BufSize, sizeof(value_type)) + 1;

				/*
				一个map要管理几个节点?最少8个，最多是“所需节点数加2”(前后各预留一个，扩充时可用)
				*/
				mMapSize = std::max(8,nodeNum + 2);
				mMap = mMapAllocator.allocate(mMapSize);
				//以上配置出一个“具有mMapSize个节点”的map

				/*
				以下令start和finish指向map所拥有之全部节点的最中央区段。保持在最中央，可使头尾两端的扩充能量一样大。
				每个节点对应一个缓冲区。
				*/
				map_pointer start = mMap + (mMapSize - nodeNum) / 2;
				map_pointer finish = start + nodeNum - 1;

				/*
				为map内的每个现用节点配置缓冲区，所有缓冲区加起来就是deque的可用空间。(最后一个缓冲区可能留有一些余裕)
				*/
				map_pointer cur;
				for (cur = start; cur <= finish; ++cur)
				{
					*cur = allocateNode();
				}

				/*
				为deque内的两个迭代器mStart和mFinish设定正确内容
				*/
				mStart.setNode(start);
				mFinish.setNode(finish);
				mStart.mCur = mStart.mFirst;
				mFinish.mCur = mFinish.mFirst + num % bufSize(BufSize, sizeof(value_type));
			}

			/*!
			 * \remarks 分配缓冲区空间(单位为元素)
			 * \return 
			*/
			pointer allocateNode()
			{
				mDataAllocator.allocate(bufSize(BufSize, sizeof(value_type)));
			}

			/*!
			 * \remarks 释放缓冲区中的元素
			 * \return 
			 * \param pointer node
			 * \param usize num
			*/
			void deallocateNode(pointer node,usize num)
			{
				mDataAllocator.deallocate(node, num);
			}

			/*!
			 * \remarks 重新整治map
			 * \return 
			 * \param usize nodes_to_add
			 * \param bool add_at_front
			*/
			void reallocateMap(usize nodes_to_add, bool add_at_front)
			{
				usize old_num_nodes = mFinish.mNode - mStart.mNode + 1;		//map中已经被使用的nodes
				usize new_num_nodes = old_num_nodes + nodes_to_add;

				map_pointer newStart;

				if (mMapSize > 2 * new_num_nodes)
				{
					newStart = mMap + (mMapSize - new_num_nodes) / 2 + (add_at_front ? nodes_to_add : 0);

					if (newStart < mStart.mNode)
					{
						/*
						template<class InputIterator, class OutputIterator>
						OutputIterator copy(InputIterator _First,InputIterator _Last,OutputIterator _DestBeg):
						Assigns the values of elements from a source range to a destination range, iterating through the source sequence of elements and assigning 
						them new positions in a forward direction.
						_First,An input iterator addressing the position of the first element in the source range.
						_Last,An input iterator addressing the position that is one past the final element in the source range.
						_DestBeg,An output iterator addressing the position of the first element in the destination range.
						Return Value,An output iterator addressing the position that is one past the final element in the destination range, that is, the iterator 
						addresses _Result + (_Last – _First ).
						*/
						std::copy(mStart.mNode, mFinish.mNode + 1, newStart);
					}
					else
					{
						/*
						template<class BidirectionalIterator1, class BidirectionalIterator2>
						BidirectionalIterator2 copy_backward(BidirectionalIterator1 _First,BidirectionalIterator1 _Last,BidirectionalIterator2 _DestEnd):
						Assigns the values of elements from a source range to a destination range, iterating through the source sequence of elements and assigning 
						them new positions in a backward direction.
						_First,A bidirectional iterator addressing the position of the first element in the source range.
						_Last,A bidirectional iterator addressing the position that is one past the final element in the source range.
						_DestEnd,A bidirectional iterator addressing the position of one past the final element in the destination range.
						Return Value,An output iterator addressing the position that is one past the final element in the destination range, that is, the iterator 
						addresses _DestEnd – (_Last – _First ).
						*/
						std::copy_backward(mStart.mNode, mFinish.mNode + 1, newStart + old_num_nodes);
					}
				}
				else
				{
					usize newMapSize = mMapSize + std::max(mMapSize, nodes_to_add) + 2;
					//配置一块空间，准备给新map使用
					map_pointer newMap = mMapAllocator.allocate(newMapSize);
					newStart = newMap + (newMapSize - new_num_nodes) / 2 + (add_at_front ? nodes_to_add : 0);
					//把原map内容拷贝过来
					std::copy(mStart.mNode, mFinish.mNode + 1, newStart);
					//释放原map
					mMapAllocator.deallocate(mMap, mMapSize);
					//设定新map的起始地址和大小
					mMap = newMap;
					mMapSize = newMapSize;
				}

				//重新设定迭代器mStart和mFinish
				mStart.setNode(newStart);
				mFinish.setNode(newStart + old_num_nodes - 1);
			}

			/*!
			 * \remarks 判断map是否需要重新整治
			 * \return 
			 * \param usize nodes_to_add
			*/
			void reserve_map_at_front(usize nodes_to_add = 1)
			{
				if (nodes_to_add > mStart.mNode - map)
				{
					//如果map前端的节点备用空间不足，则必须重换一个map(配置更大的，拷贝原来的，释放原来的)
					reallocateMap(nodes_to_add, true);
				}
			}

			/*!
			 * \remarks 判断map是否需要重新整治
			 * \return 
			 * \param usize nodes_to_add
			*/
			void reserve_map_at_back(usize nodes_to_add = 1)
			{
				if (nodes_to_add + 1 > mMapSize - (mFinish.mNode - map))
				{
					//如果map尾端的节点备用空间不足，则必须重换一个map(配置更大的，拷贝原来的，释放原来的)
					reallocateMap(nodes_to_add, false);
				}
			}

			/*!
			 * \remarks 只有当mStart.mCur == mStart.mFirst时才会被调用。也就是说，只有当第一个缓冲区没有任何备用元素时才会被调用
			 * \return 
			 * \param value_type const & value 元素值
			*/
			void push_front_aux(value_type const& value)		//aux:auxiliary:辅助的
			{
				value_type valueCopy = value;

				//若符合某种条件则必须重换一个map
				reserve_map_at_front();

				//配置一个新节点(缓冲区)
				*(mStart.mNode - 1) = allocateNode();

				try
				{
					//改变mStart，令其指向新节点
					mStart.setNode(mStart.mNode - 1);
					//设定mStart的状态
					mStart.mCur = mStart.mLast - 1;
					construct(mStart.mCur, valueCopy);
				}
				catch (...)
				{
					//commit or rollback:若非全部成功，就一个不留
					mStart.setNode(mStart.mNode + 1);
					mStart.mCur = mStart.mFirst;
					deallocateNode(*(mStart.mNode - 1));

					throw;
				}
			}

			/*!
			 * \remarks 只有当mFinish.mCur == mFinish.mLast - 1时才会被调用。也就是说，只有当最后一个缓冲区只剩下一个备用元素空间时才会被调用
			 * \return 
			 * \param value_type const & value
			*/
			void push_back_aux(value_type const& value)
			{
				value_type valueCopy = value;

				//若符合某种条件则必须重换一个map
				reserve_map_at_back();

				//配置一个新节点(缓冲区)
				*(mFinish.mNode + 1) = allocateNode();

				try
				{
					construct(mFinish.mCur, valueCopy);

					//改变mFinish，令其指向新节点
					mFinish.setNode(mFinish.mNode + 1);
					//设定mFinish的状态
					mFinish.mCur = mFinish.mFirst;
				}
				catch (...)
				{
					deallocateNode(*(mFinish.mNode + 1));

					throw;
				}
			}

			/*!
			* \remarks 只有当mStart.mCur == mStart.mFirst时才会被调用。也就是说，只有当第一个缓冲区没有任何备用元素时才会被调用
			* \return
			* \param Arguments && ... args 用于构造元素对象的参数
			*/
			template<typename... Arguments>
			void emplace_front_aux(Arguments&&... args)
			{
				//若符合某种条件则必须重换一个map
				reserve_map_at_front();

				//配置一个新节点(缓冲区)
				*(mStart.mNode - 1) = allocateNode();

				try
				{
					//改变mStart，令其指向新节点
					mStart.setNode(mStart.mNode - 1);
					//设定mStart的状态
					mStart.mCur = mStart.mLast - 1;
					construct(mStart.mCur, std::forward<Arguments>((args)...));
				}
				catch (...)
				{
					//commit or rollback:若非全部成功，就一个不留
					mStart.setNode(mStart.mNode + 1);
					mStart.mCur = mStart.mFirst;
					deallocateNode(*(mStart.mNode - 1));

					throw;
				}
			}

			template<typename... Arguments>
			void emplace_back_aux(Arguments&&... args)
			{
				//若符合某种条件则必须重换一个map
				reserve_map_at_back();

				//配置一个新节点(缓冲区)
				*(mFinish.mNode + 1) = allocateNode();

				try
				{
					construct(mFinish.mCur, std::forward<Arguments>((args)...));

					//改变mFinish，令其指向新节点
					mFinish.setNode(mFinish.mNode + 1);
					//设定mFinish的状态
					mFinish.mCur = mFinish.mFirst;
				}
				catch (...)
				{
					deallocateNode(*(mFinish.mNode + 1));

					throw;
				}
			}

			/*!
			 * \remarks 只有当mFinish.mCur == mFinish.mFirst时才会被调用
			 * \return 
			*/
			void pop_back_aux()
			{
				destroy(mFinish.mCur);

				//释放最后一个缓冲区
				deallocateNode(mFinish.mFirst);

				mFinish.setNode(mFinish.mNode - 1);
				mFinish.mCur = mFinish.mLast - 1;
			}

			/*!
			 * \remarks 只有当mStart.mCur == mStart.mLast - 1时才会被调用
			 * \return 
			*/
			void pop_front_aux()
			{
				//将第一缓冲区的最后一个，唯一一个元素析构
				destroy(mStart.mCur);

				//释放第一缓冲区
				deallocateNode(mStart.mFirst);

				mStart.setNode(mStart.mNode + 1);
				mStart.mCur = mStart.mFirst;
			}

			/*!
			 * \remarks insert辅助函数
			 * \return 
			 * \param iterator pos
			 * \param value_type const & elem
			*/
			iterator insert_aux(iterator pos, value_type const& elem)
			{
				//插入点之前的元素个数
				difference_type num = pos - mStart;

				value_type elemCopy = elem;
				
				//如果插入点之前的元素个数比较少
				if (num < size() / 2)
				{
					//在最前端放置与第一个元素同值的元素
					push_front(front());

					//标记记号，然后进行元素移动
					iterator front1 = mStart;
					++front1;
					iterator front2 = front1;
					++front2;
					pos = mStart + num;
					iterator pos1 = pos;
					++pos1;
					std::copy(front2, pos1, front1);
				}
				//如果插入点之后的元素个数比较少
				else
				{
					//在最尾端放置与最后一个元素同值得元素
					push_back(back());

					//标记记号，然后进行元素移动
					iterator back1 = mFinish;
					--back1;
					iterator back2 = back1;
					--back2;
					pos = mStart + num;
					copy_backward(pos, back2, back1);
				}

				//在插入点上设定新值
				*pos = elemCopy;

				return pos;
			}

			/*!
			 * \remarks 销毁双端队列
			 * \return 
			*/
			void tidy()
			{
				const usize bs = bufSize(BufSize, sizeof(value_type));
				//删除所有的元素，并只剩下一个缓冲区
				clear();
				//释放仅有的一个缓冲区
				mDataAllocator.deallocate(mStart.mNode, bs);
				//释放map
				mMapAllocator.deallocate(mMap, mMapSize);
			}

			map_pointer mMap;
			usize mMapSize;
			iterator mStart;
			iterator mFinish;
		};

		/*!
		 * \remarks 交换
		 * \return 
		 * \param Deque<T,Alloc,BufSize>& left
		 * \param Deque<T,Alloc,BufSize>& right
		*/
		template<typename T, typename Alloc, usize BufSize>
		inline void swap(Deque<T, Alloc, BufSize>& left, Deque<T, Alloc, BufSize>& right)
		{
			left.swap(right);
		}

		/*!
		 * \remarks 测试left队列是否==right队列
		 * \return 
		 * \param const Deque<T,Alloc,BufSize>& left
		 * \param const Deque<T,Alloc,BufSize>& right
		*/
		template<typename T, typename Alloc, usize BufSize>
		inline bool operator==(const Deque<T, Alloc, BufSize>& left,const Deque<T, Alloc, BufSize>& right)
		{
			return left.size() == right.size() && std::equal(left.begin(), left.end(), right.begin());
		}

		/*!
		 * \remarks 测试left队列是否!=right队列
		 * \return 
		 * \param const Deque<T,Alloc,BufSize>& left
		 * \param const Deque<T,Alloc,BufSize>& right
		*/
		template<typename T, typename Alloc, usize BufSize>
		inline bool operator!=(const Deque<T, Alloc, BufSize>& left,const Deque<T, Alloc, BufSize>& right)
		{
			return !(left == right);
		}

		/*!
		 * \remarks 测试left队列是否<right队列
		 * \return 
		 * \param const Deque<T,Alloc,BufSize>& left
		 * \param const Deque<T,Alloc,BufSize>& right
		*/
		template<typename T, typename Alloc, usize BufSize>
		inline bool operator<(const Deque<T, Alloc, BufSize>& left,const Deque<T, Alloc, BufSize>& right)
		{
			/*
			lexicographical_compare:
			Compares element by element between two sequences to determine which is lesser of the two.
			*/
			return std::lexicographical_compare(left.begin(), left.end(),right.begin(), right.end());
		}

		/*!
		 * \remarks 测试left队列是否<=right队列
		 * \return 
		 * \param const Deque<T,Alloc,BufSize>& left
		 * \param const Deque<T,Alloc,BufSize>& right
		*/
		template<typename T, typename Alloc, usize BufSize>
		inline bool operator<=(const Deque<T, Alloc, BufSize>& left,const Deque<T, Alloc, BufSize>& right)
		{
			return !(right < left);
		}

		/*!
		 * \remarks 测试left队列是否>right队列
		 * \return 
		 * \param const Deque<T,Alloc,BufSize>& left
		 * \param const Deque<T,Alloc,BufSize>& right
		*/
		template<typename T, typename Alloc, usize BufSize>
		inline bool operator>(const Deque<T, Alloc, BufSize>& left,const Deque<T, Alloc, BufSize>& right)
		{
			return right < left;
		}

		/*!
		 * \remarks 测试left队列是否>=right队列
		 * \return 
		 * \param const Deque<T,Alloc,BufSize>& left
		 * \param const Deque<T,Alloc,BufSize>& right
		*/
		template<typename T, typename Alloc, usize BufSize>
		inline bool operator>=(const Deque<T, Alloc, BufSize>& left,const Deque<T, Alloc, BufSize>& right)
		{
			return !(left < right);
		}
	}//namespace utl
}//namespace ung

#endif//_UNG_UTL_DEQUE_H_

/*
vector是单向开口的连续线性空间，deque是双向开口的连续线性空间。
所谓双向开口，意思是可以在头尾两端分别做元素的插入和删除操作。
vector当然也可以在头尾两端进行操作，但是其头部操作效率奇差，无法被接受。
deque和vector的最大差异，一在于deque允许于常数时间内对起头端进行元素的插入或移除操作，二在于deque没有所谓容量(capacity)观念，因为它是动态地
以分段连续空间组合而成，随时可以增加一段新的空间并链接起来。换句话说，像vector那样“因旧空间不足而重新配置一块更大空间，然后复制元素，再释放旧空间”
这样的事情在deque是不会发生的。也因此，deque没有必要提供所谓的空间保留(reserve)功能。
虽然deque也提供Random Access Iterator，但它的迭代器并不是普通指针，其复杂度和vector不可以道里计。因此，除非必要，我们应尽可能选择使用vector而非
deque。对deque进行的排序操作，为了最高效率，可将deque先完整复制到一个vector身上，将vector排序后(利用STL sort算法)，再复制回deque。
deque系由一段一段的定量连续空间构成。一旦有必要在deque的前端或尾端增加新空间，便配置一段定量连续空间，串接在整个deque的头端或尾端。deque的最大
任务，便是在这些分段的定量连续空间上，维护其整体连续的假象，并提供随机存取的接口，避开了“重新配置，复制，释放”的轮回，代价则是复杂的迭代器架构。有
分段连续线性空间，就必须有中央控制(中控器)。
deque采用一块所谓的map(注意，不是STL的map容器)作为主控。这里所谓map是一小块连续空间，其中每个元素(此处称为一个节点，node)都是指针，指向另一段
(较大的)连续线性空间，称为缓冲区。缓冲区才是deque的存储空间主体。SGI STL允许我们指定缓冲区大小，默认值0表示将使用512字节缓冲区。
deque是分段连续空间。维持其“整体连续”假象的任务，落在了迭代器的operator++和operator--两个运算子身上。
deque除了维护一个先前说过的指向map的指针外，也维护start，finish两个迭代器，分别指向第一缓冲区的第一个元素和最后缓冲区的最后一个元素(的下一位置)。
此外，它当然也必须记住目前的map大小。因为一旦map所提供的节点不足，就必须重新配置更大的一块map。
*/