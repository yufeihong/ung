/*!
 * \brief
 * STL容器包装
 * \file UFCSTLWrapper.h
 *
 * \author Su Yang
 *
 * \date 2017/04/13
 */
#ifndef _UFC_STLWRAPPER_H_
#define _UFC_STLWRAPPER_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#ifndef _UFC_CHOOSECONTAINER_H_
#include "UFCChooseContainer.h"
#endif

#ifndef _UFC_METAFUNCTION_H_
#include "UFCMetaFunction.h"
#endif

#ifndef _INCLUDE_BOOST_MPLIF_HPP_
#include "boost/mpl/if.hpp"
#define _INCLUDE_BOOST_MPLIF_HPP_
#endif

#ifndef _INCLUDE_BOOST_POINTEE_HPP_
#include "boost/pointee.hpp"
#define _INCLUDE_BOOST_POINTEE_HPP_
#endif

namespace ung
{
	/*!
	 * \brief
	 * 对std::set的包装
	 * \class SetWrapper
	 *
	 * \author Su Yang
	 *
	 * \date 2017/05/14
	 *
	 * \todo
	 */
	template<typename K>
	class SetWrapper
	{
	public:
		using container_type = STL_SET(K);
		using key_type = typename container_type::key_type;
		using value_type = typename container_type::value_type;
		using reference = typename container_type::reference;
		using const_reference = typename container_type::const_reference;
		using pointer = typename container_type::pointer;
		using const_pointer = typename container_type::const_pointer;
		using iterator = typename container_type::iterator;
		using const_iterator = typename container_type::const_iterator;
		using reverse_iterator = typename container_type::reverse_iterator;
		using const_reverse_iterator = typename container_type::const_reverse_iterator;
		using size_type = typename container_type::size_type;
		using difference_type = typename container_type::difference_type;

		using iterators_type = std::pair<iterator, iterator>;
		using const_iterators_type = std::pair<const_iterator, const_iterator>;

		using is_element_smart_type = typename boost::mpl::if_c<
			utp::isSmartPtr<value_type>::value,
			boost::true_type, boost::false_type>::type;

		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		SetWrapper()
		{
		}

		/*!
		 * \remarks 拷贝构造函数
		 * \return 
		 * \param SetWrapper const & copyInst
		*/
		SetWrapper(SetWrapper const& copyInst) :
			mCon(copyInst.mCon)
		{
		}

		/*!
		 * \remarks 拷贝赋值运算符
		 * \return 
		 * \param SetWrapper const & copyInst
		*/
		SetWrapper& operator=(SetWrapper const& copyInst)
		{
			mCon = copyInst.mCon;

			return *this;
		}

		/*!
		 * \remarks 移动构造函数
		 * \return 
		 * \param SetWrapper & & moveInst
		*/
		SetWrapper(SetWrapper&& moveInst) :
			mCon(std::move(moveInst.mCon))
		{
		}

		/*!
		 * \remarks 移动赋值运算符
		 * \return 
		 * \param SetWrapper & & moveInst
		*/
		SetWrapper& operator=(SetWrapper&& moveInst)
		{
			mCon = std::move(moveInst.mCon);

			return *this;
		}

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param std::initializer_list<value_type> initList
		*/
		SetWrapper(std::initializer_list<value_type> initList) :
			mCon(initList)
		{
		}

		/*!
		 * \remarks 重载=
		 * \return 
		 * \param std::initializer_list<value_type> initList
		*/
		SetWrapper& operator=(std::initializer_list<value_type> initList)
		{
			mCon = initList;

			return *this;
		}

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param Iter first
		 * \param Iter last
		*/
		template<typename Iter>
		SetWrapper(Iter first, Iter last) :
			mCon(first, last)
		{
		}

		~SetWrapper()
		{
			clear();
		}

		/*!
		 * \remarks Inserts an element constructed in place (no copy or move operations are performed).
		 * No iterators or references are invalidated by this function.
		 * \return A pair whose bool component returns true if an insertion was made, and false if the map already contained an element whose value had an equivalent value in the ordering. The iterator component of the return value pair returns the address where a new element was inserted (if the bool component is true) or where the element was already located (if the bool component is false).
		 * \param Args&& ... args The arguments forwarded to construct an element to be inserted into the set unless it already contains an element whose value is equivalently ordered.
		*/
		template <typename... Args>
		std::pair<iterator, bool>emplace(Args&&... args)
		{
			return mCon.emplace(boost::forward<Args>(args)...);
		}

		/*!
		 * \remarks Inserts an element constructed in place (no copy or move operations are performed), with a placement hint.
		 * No iterators or references are invalidated by this function.
		 * \return An iterator to the newly inserted element.
		 * If the insertion failed because the element already exists, returns an iterator to the existing element.
		 * \param const_iterator where The place to start searching for the correct point of insertion. (If that point immediately precedes where, insertion can occur in amortized constant time instead of logarithmic time.)
		 * \param Args&& ... args
		*/
		template <typename... Args>
		iterator emplace_hint(const_iterator where,Args&&... args)
		{
			return mCon.emplace(where,boost::forward<Args>(args)...);
		}

		/*!
		 * \remarks 交换
		 * \return 
		 * \param SetWrapper const & rightInst
		*/
		void swap(SetWrapper const& rightInst)
		{
			mCon.swap(rightInst.mCon);
		}

#if UNG_DEBUGMODE
#define CHECK_REPEATED_KEY(key)																			\
		auto findRet = find(key);																					\
		if (findRet != end())																							\
		{																														\
			UNG_EXCEPTION("The key value specified by the parameter is repeated.");		\
		}
#else
#define CHECK_REPEATED_KEY(key)
#endif

		/*!
		 * \remarks 插入
		 * \return 
		 * \param const value_type & val single element
		*/
		std::pair<iterator, bool> insert(const value_type& val)
		{
			CHECK_REPEATED_KEY(val);

			return mCon.insert(val);
		}

		/*!
		 * \remarks 插入
		 * \return 
		 * \param value_type&& val single element,perfect forwarded
		*/
		std::pair<iterator, bool> insert(value_type&& val)
		{
			CHECK_REPEATED_KEY(val);

			return mCon.insert(std::forward<value_type>(val));
		}

		/*!
		 * \remarks 插入
		 * \return 
		 * \param const_iterator where
		 * \param const value_type & val single element with hint
		*/
		iterator insert(const_iterator where,const value_type& val)
		{
			CHECK_REPEATED_KEY(val);

			return mCon.insert(where,val);
		}

		/*!
		 * \remarks 插入
		 * \return 
		 * \param const_iterator where
		 * \param value_type&& val single element,perfect forwarded,with hint
		*/
		iterator insert(const_iterator where, value_type&& val)
		{
			CHECK_REPEATED_KEY(val);

			return mCon.insert(where,std::forward<value_type>(val));
		}

		/*!
		 * \remarks 插入
		 * \return 
		 * \param InputIter first range
		 * \param InputIter last
		*/
		template <typename InputIter>
		void insert(InputIter first, InputIter last)
		{
			return mCon.insert(first, last);
		}

		/*!
		 * \remarks 插入
		 * \return 
		 * \param std::initializer_list<value_type> initList initializer list
		*/
		void insert(std::initializer_list<value_type> initList)
		{
			return mCon.insert(initList);
		}

		/*!
		 * \remarks Removes an element or a range of elements in a set from specified positions or removes elements that match a specified key.
		 * \return the number of elements that have been removed from the set.
		 * \param const key_type& key The key value of the elements to be removed.
		*/
		size_type erase(const key_type& key)
		{
			auto eraseRet = mCon.erase(key);

			BOOST_ASSERT(eraseRet == 1);

			return eraseRet;
		}

		/*!
		 * \remarks 
		 * \return a bidirectional iterator that designates the first element remaining beyond any elements removed, or an element that is the end of the set if no such element exists.
		 * \param const_iterator where Position of the element to be removed.
		*/
		iterator erase(const_iterator where)
		{
			return mCon.erase(where);
		}

		/*!
		 * \remarks 
		 * \return a bidirectional iterator that designates the first element remaining beyond any elements removed, or an element that is the end of the set if no such element exists.
		 * \param const_iterator first Position of the first element to be removed.
		 * \param const_iterator last Position just beyond the last element to be removed.
		*/
		iterator erase(const_iterator first, const_iterator last)
		{
			return mCon.erase(first, last);
		}

		/*!
		 * \remarks Erases all the elements of a set.
		 * \return 
		*/
		void clear()
		{
			_clear(is_element_smart_type());
		}

		/*!
		 * \remarks Returns an iterator that refers to the location of an element in a set that has a key equivalent to a specified key.
		 * \return An iterator that refers to the location of an element with a specified key, or the location succeeding the last element in the set ( set::end()) if no match is found for the key.
		 * \param const key_type& key The key value to be matched by the sort key of an element from the set being searched.
		*/
		iterator find(const key_type& key)
		{
			return mCon.find(key);
		}

		/*!
		 * \remarks 
		 * \return 
		 * \param const key_type & key
		*/
		const_iterator find(const key_type& key) const
		{
			return mCon.find(key);
		}

		/*!
		 * \remarks 
		 * \return 
		*/
		bool empty() const
		{
			return mCon.empty();
		}

		/*!
		 * \remarks Returns the number of elements in the set.
		 * \return 
		*/
		size_type size() const
		{
			return mCon.size();
		}

		/*!
		 * \remarks Returns the number of elements in a set whose key matches a parameter-specified key.
		 * \return 1 if the set contains an element whose sort key matches the parameter key. 0 if the set does not contain an element with a matching key.
		 * \param const key_type& key
		*/
		size_type count(const key_type& key) const
		{
			return mCon.count(key);
		}

		/*!
		 * \remarks Returns the maximum length of the set.
		 * \return 
		*/
		size_type max_size() const
		{
			return mCon.max_size();
		}

		/*!
		 * \remarks Returns an iterator that addresses the first element in the set.
		 * \return A bidirectional iterator addressing the first element in the set or the location succeeding an empty set.
		*/
		iterator begin()
		{
			return mCon.begin();
		}

		/*!
		 * \remarks 
		 * \return 
		*/
		const_iterator begin() const
		{
			return mCon.begin();
		}

		/*!
		 * \remarks 
		 * \return 
		*/
		const_iterator cbegin() const
		{
			return mCon.cbegin();
		}

		/*!
		 * \remarks 
		 * \return 
		*/
		reverse_iterator rbegin()
		{
			return mCon.rbegin();
		}

		/*!
		 * \remarks 
		 * \return 
		*/
		const_reverse_iterator rbegin() const
		{
			return mCon.rbegin();
		}

		/*!
		 * \remarks Returns a const iterator addressing the first element in a reversed set.
		 * \return 
		*/
		const_reverse_iterator crbegin() const
		{
			return mCon.crbegin();
		}

		/*!
		 * \remarks Returns the past-the-end iterator.
		 * \return 
		*/
		iterator end()
		{
			return mCon.end();
		}

		/*!
		 * \remarks 
		 * \return 
		*/
		const_iterator end() const
		{
			return mCon.end();
		}

		/*!
		 * \remarks Returns a const iterator that addresses the location just beyond the last element in a range.
		 * \return 
		*/
		const_iterator cend() const
		{
			return mCon.cend();
		}

		/*!
		 * \remarks Returns an iterator that addresses the location succeeding the last element in a reversed set.
		 * \return 
		*/
		reverse_iterator rend()
		{
			return mCon.rend();
		}

		/*!
		 * \remarks 
		 * \return 
		*/
		const_reverse_iterator rend() const
		{
			return mCon.rend();
		}

		/*!
		 * \remarks Returns a const iterator that addresses the location succeeding the last element in a reversed set.
		 * \return 
		*/
		const_reverse_iterator crend() const
		{
			return mCon.crend();
		}

	public:
		/*!
		 * \remarks 放置
		 * \return 
		 * \param value_type const & val
		*/
		void put(value_type const& val)
		{
			insert(val);
		}

		/*!
		 * \remarks 移除
		 * \return 
		 * \param key_type const& key
		*/
		void take(key_type const& key)
		{
			erase(key);
		}

		/*!
		 * \remarks 获取迭代器对
		 * \return 
		*/
		iterators_type getIterators()
		{
			return std::make_pair(begin(), end());
		}

		/*!
		 * \remarks 获取迭代器对
		 * \return 
		*/
		const_iterators_type getIterators() const
		{
			return std::make_pair(cbegin(), cend());
		}

		/*!
		 * \remarks 遍历，然后对每个元素应用算法
		 * \return 
		 * \param F func
		*/
		template<typename F>
		void loop(F func)
		{
			auto beg = begin();
			auto ed = end();

			while (beg != ed)
			{
				func(*beg);

				++beg;
			}
		}

		/*!
		 * \remarks delete容器中的全部普通指针
		 * \return 
		*/
		void deleteAll()
		{
			//确保是普通指针类型
			BOOST_ASSERT(boost::is_pointer<value_type>::value);

			//解引用后的类型
			using pointee_type = typename boost::pointee<value_type>::type;

			//解引用后的类型是否重载了new/delete运算符
			using dispatch_type = typename boost::mpl::if_c <
				boost::has_new_operator<pointee_type>::value,
				boost::true_type, boost::false_type > ::type;

			_deleteAll(dispatch_type());
		}

	private:
		void _clear(boost::true_type)
		{
			auto beg = begin();
			auto ed = end();

			while (beg != ed)
			{
				(*beg).reset();

				++beg;
			}

			mCon.clear();
		}

		void _clear(boost::false_type)
		{
			//普通指针需要手动调用deleteAll()函数来析构。
			mCon.clear();
		}

		void _deleteAll(boost::true_type)
		{
			auto lambdaExpress = [](value_type v)
			{
				UNG_DEL(v);
			};

			loop(lambdaExpress);
		}

		void _deleteAll(boost::false_type)
		{
			auto lambdaExpress = [](value_type v)
			{
				delete v;
				v = nullptr;
			};

			loop(lambdaExpress);
		}

	private:
		container_type mCon;
	};//end class SetWrapper

	/*!
	 * \brief
	 * 对std::unordered_map的包装
	 * \class UnorderedMapWrapper
	 *
	 * \author Su Yang
	 *
	 * \date 2017/05/11
	 *
	 * \todo
	 */
	template<typename K,typename T>
	class UnorderedMapWrapper
	{
	public:
		using container_type = STL_UNORDERED_MAP(K, T);
		using key_type = typename container_type::key_type;
		using value_type = typename container_type::value_type;
		using mapped_type = typename container_type::mapped_type;
		using reference = typename container_type::reference;
		using const_reference = typename container_type::const_reference;
		using size_type = typename container_type::size_type;
		using difference_type = typename container_type::difference_type;
		using pointer = typename container_type::pointer;
		using const_pointer = typename container_type::const_pointer;
		using iterator = typename container_type::iterator;
		using const_iterator = typename container_type::const_iterator;

		using iterators_type = std::pair<iterator, iterator>;
		using const_iterators_type = std::pair<const_iterator, const_iterator>;

		//mapped_type是否为标准库智能指针
		using is_smart_mapped_type = typename boost::mpl::if_c<
			utp::isSmartPtr<mapped_type>::value,
			boost::true_type, boost::false_type>::type;

		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		UnorderedMapWrapper()
		{
		}

		/*!
		 * \remarks 拷贝构造函数
		 * \return 
		 * \param UnorderedMapWrapper const & copyInst
		*/
		UnorderedMapWrapper(UnorderedMapWrapper const& copyInst) :
			mCon(copyInst.mCon)
		{
		}

		/*!
		 * \remarks 拷贝赋值运算符
		 * \return 
		 * \param UnorderedMapWrapper const & copyInst
		*/
		UnorderedMapWrapper& operator=(UnorderedMapWrapper const& copyInst)
		{
			mCon = copyInst.mCon;

			return *this;
		}

		/*!
		 * \remarks 移动构造函数
		 * \return 
		 * \param UnorderedMapWrapper & & moveInst
		*/
		UnorderedMapWrapper(UnorderedMapWrapper&& moveInst) :
			mCon(std::move(moveInst.mCon))
		{
		}

		/*!
		 * \remarks 移动赋值运算符
		 * \return 
		 * \param UnorderedMapWrapper & & moveInst
		*/
		UnorderedMapWrapper& operator=(UnorderedMapWrapper&& moveInst)
		{
			mCon = std::move(moveInst.mCon);

			return *this;
		}

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param InputIt first
		 * \param InputIt last
		*/
		template<typename InputIt>
		UnorderedMapWrapper(InputIt first, InputIt last) :
			mCon(first,last)
		{
		}

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param std::initializer_list<value_type> initList
		*/
		UnorderedMapWrapper(std::initializer_list<value_type> initList) :
			mCon(initList)
		{
		}

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~UnorderedMapWrapper()
		{
			clear();
		}

		/*!
		 * \remarks Inserts an element constructed in place (no copy or move operations are performed) into an unordered_map.
		 * \return 
		 * \param BOOST_FWD_REF(Args)... args
		*/
		template <typename... Args>
		std::pair<iterator, bool> emplace(BOOST_FWD_REF(Args)... args)
		{
			return mCon.emplace(boost::forward<Args>(args)...);
		}

		/*!
		 * \remarks 重载=
		 * \return 
		 * \param std::initializer_list<value_type> initList
		*/
		UnorderedMapWrapper& operator=(std::initializer_list<value_type> initList)
		{
			mCon = initList;

			return *this;
		}

		/*!
		 * \remarks Swaps the contents of two containers(常数时间).
		 * \return 
		 * \param UnorderedMapWrapper & rightInst
		*/
		void swap(UnorderedMapWrapper& rightInst)
		{
			mCon.swap(rightInst.mCon);
		}

#if UNG_DEBUGMODE
#define CHECK_REPEATED_KEY(key)																			\
		auto findRet = find(key);																					\
		if (findRet != end())																							\
		{																														\
			UNG_EXCEPTION("The key value specified by the parameter is repeated.");		\
		}
#else
#define CHECK_REPEATED_KEY(key)
#endif

		/*!
		 * \remarks Inserts an element or a range of elements into an unordered_map.
		 * \return 
		 * \param value_type const & val single element
		*/
		std::pair<iterator, bool> insert(value_type const& val)
		{
			CHECK_REPEATED_KEY(val.first);

			return mCon.insert(val);
		}

		/*!
		 * \remarks 
		 * \return 
		 * \param BOOST_RV_REF(value_type) val single element, perfect forwarded
		*/
		std::pair<iterator, bool> insert(BOOST_RV_REF(value_type) val)
		{
			CHECK_REPEATED_KEY(val.first);

			return mCon.insert(std::forward<value_type>(val));
		}

		/*!
		 * \remarks 
		 * \return 
		 * \param const_iterator where
		 * \param value_type const & val single element with hint
		*/
		iterator insert(const_iterator where, value_type const& val)
		{
			CHECK_REPEATED_KEY(val.first);

			return mCon.insert(where, val);
		}

		/*!
		 * \remarks 
		 * \return 
		 * \param const_iterator where
		 * \param BOOST_RV_REF(value_type) val single element, perfect forwarded, with hint
		*/
		iterator insert(const_iterator where, BOOST_RV_REF(value_type) val)
		{
			CHECK_REPEATED_KEY(val.first);

			return mCon.insert(where,std::forward<value_type>(val));
		}

		/*!
		 * \remarks 
		 * \return 
		 * \param InputIt first range
		 * \param InputIt last
		*/
		template <typename InputIt>
		void insert(InputIt first, InputIt last)
		{
			return mCon.insert(first, last);
		}

		/*!
		 * \remarks 
		 * \return 
		 * \param std::initializer_list<value_type> initList initializer list
		*/
		void insert(std::initializer_list<value_type> initList)
		{
			return mCon.insert(initList);
		}

		/*!
		 * \remarks Removes an element or a range of elements in a unordered_map from specified positions or removes elements that match a specified key.
		 * \return a bidirectional iterator that designates the first element remaining beyond any elements removed, or an element that is the end of the map if no such element exists.
		 * \param const_iterator where Position of the element to be removed.
		*/
		iterator erase(const_iterator where)
		{
			return mCon.erase(where);
		}

		/*!
		 * \remarks 
		 * \return the number of elements that have been removed from the unordered_map.
		 * \param const key_type & key The key value of the elements to be removed.
		*/
		size_type erase(const key_type& key)
		{
			auto eraseRet = mCon.erase(key);
			BOOST_ASSERT(eraseRet == 1);

			return eraseRet;
		}

		/*!
		 * \remarks 
		 * \return a bidirectional iterator that designates the first element remaining beyond any elements removed, or an element that is the end of the map if no such element exists.
		 * \param const_iterator first Position of the first element to be removed.
		 * \param const_iterator last Position just beyond the last element to be removed.
		*/
		iterator erase(const_iterator first, const_iterator last)
		{
			return mCon.erase(first, last);
		}

		/*!
		 * \remarks Removes all elements(calls unordered_map::erase( unordered_map::begin(), unordered_map::end())).
		 * \return 
		*/
		void clear()
		{
			_clear(is_smart_mapped_type());
		}

		/*!
		 * \remarks Tests whether no elements are present.
		 * \return 
		*/
		bool empty() const
		{
			return mCon.empty();
		}

		/*!
		 * \remarks Counts the number of elements.
		 * \return 
		*/
		size_type size() const
		{
			return mCon.size();
		}

		/*!
		 * \remarks Gets the maximum size of the controlled sequence.
		 * \return The member function returns the length of the longest sequence that the object can control.
		*/
		size_type max_size() const
		{
			return mCon.max_size();
		}

		/*!
		 * \remarks Finds or inserts an element with the specified key.
		 * If the argument key value is not found, then it is inserted along with the default value 
		 * of the data type.
		 * operator[] may be used to insert elements into a map m using m[_Key] = DataValue;
		 * where DataValue is the value of the mapped_type of the element with a key value of _Key.
		 * When using operator[] to insert elements, the returned reference does not indicate 
		 * whether an insertion is changing a pre-existing element or creating a new one. The 
		 * member functions find and insert can be used to determine whether an element with a 
		 * specified key is already present before an insertion.
		 * \return A reference to the data value of the inserted element.
		 * \param const key_type & key
		*/
		mapped_type& operator[](const key_type& key)
		{
			return mCon[key];
		}

		/*!
		 * \remarks Finds an element in a unordered_map with a specified key value.
		 * If the argument key value is not found, then the function throws an object of class out_of_range.
		 * \return A reference to the data value of the element found.
		 * \param const key_type & key
		*/
		mapped_type& at(const key_type& key)
		{
			return mCon.at(key);
		}

		/*!
		 * \remarks 
		 * \return 
		 * \param const key_type & key
		*/
		mapped_type const& at(const key_type& key) const
		{
			return mCon.at(key);
		}

		/*!
		 * \remarks Finds an element that matches a specified key.
		 * \return unordered_map::equal_range(keyval).first
		 * \param const key_type & key
		*/
		iterator find(const key_type& key)
		{
			return mCon.find(key);
		}

		/*!
		 * \remarks 
		 * \return 
		 * \param const key_type & key
		*/
		const_iterator find(const key_type& key) const
		{
			return mCon.find(key);
		}

		/*!
		 * \remarks Designates the beginning of the controlled sequence or a bucket.
		 * \return a forward iterator that points at the first element of the sequence (or just beyond the end of an empty sequence).
		*/
		iterator begin()
		{
			return mCon.begin();
		}

		/*!
		 * \remarks 
		 * \return 
		*/
		const_iterator begin() const
		{
			return mCon.begin();
		}

		/*!
		 * \remarks 
		 * \return 
		*/
		iterator end()
		{
			return mCon.end();
		}

		/*!
		 * \remarks 
		 * \return 
		*/
		const_iterator end() const
		{
			return mCon.end();
		}

		/*!
		 * \remarks 
		 * \return 
		*/
		const_iterator cbegin() const
		{
			return mCon.cbegin();
		}

		/*!
		 * \remarks 
		 * \return 
		*/
		const_iterator cend() const
		{
			return mCon.cend();
		}

	public:
		/*!
		 * \remarks 放置到池中
		 * \return 
		 * \param key_type const & key
		 * \param mapped_type const & val
		*/
		void put(key_type const& key, mapped_type const& val)
		{
			insert(value_type(key,val));
		}

		/*!
		 * \remarks 从池中移除
		 * \return 
		 * \param key_type const & key
		*/
		void take(key_type const& key)
		{
			erase(key);
		}

		/*!
		 * \remarks 获取迭代器对
		 * \return 
		*/
		iterators_type getIterators()
		{
			return std::make_pair(begin(), end());
		}

		/*!
		 * \remarks 获取迭代器对
		 * \return 
		*/
		const_iterators_type getIterators() const
		{
			return std::make_pair(cbegin(), cend());
		}

		/*!
		 * \remarks 遍历，然后对每个元素应用算法
		 * \return 
		 * \param F func
		*/
		template<typename F>
		void loop(F func)
		{
			auto beg = begin();
			auto ed = end();

			while (beg != ed)
			{
				auto v = beg->second;

				func(v);

				++beg;
			}
		}

		/*!
		 * \remarks delete容器中的全部普通指针
		 * \return 
		*/
		void deleteAll()
		{
			//确保是普通指针类型
			BOOST_ASSERT(boost::is_pointer<mapped_type>::value);

			//解引用后的类型
			using pointee_type = typename boost::pointee<mapped_type>::type;

			//解引用后的类型是否重载了new/delete运算符
			using dispatch_type = typename boost::mpl::if_c <
				boost::has_new_operator<pointee_type>::value,
				boost::true_type, boost::false_type > ::type;

			_deleteAll(dispatch_type());
		}

	private:
		void _clear(boost::true_type)
		{
			auto beg = begin();
			auto ed = end();

			while (beg != ed)
			{
				auto smart = beg->second;

				smart.reset();

				++beg;
			}

			mCon.clear();
		}

		void _clear(boost::false_type)
		{
			//普通指针需要手动调用deleteAll()函数来析构。
			mCon.clear();
		}

		void _deleteAll(boost::true_type)
		{
			auto lambdaExpress = [](mapped_type v)
			{
				UNG_DEL(v);
			};

			loop(lambdaExpress);
		}

		void _deleteAll(boost::false_type)
		{
			auto lambdaExpress = [](mapped_type v)
			{
				delete v;
				v = nullptr;
			};

			loop(lambdaExpress);
		}

	private:
		container_type mCon;
	};//end class UnorderedMapWrapper

	//--------------------------------------------------------

	/*!
	 * \remarks 对顺序容器clear()函数的包装
	 * \return 
	 * \param C& con
	*/
	template<typename C>
	void ungContainerClear(C& con)
	{
		//是否是线性序列容器
		BOOST_CONCEPT_ASSERT((boost::Sequence<C>));

		using is_smart_element_type = typename boost::mpl::if_c<
			utp::isSmartPtr<typename C::value_type>::value,
			boost::true_type, boost::false_type>::type;

		_ungContainerClear(is_smart_element_type(),con);
	}

	/*!
	 * \remarks 当顺序容器的元素是标准库智能指针
	 * \return 
	 * \param C& con
	*/
	template<typename C>
	void _ungContainerClear(boost::true_type,C& con)
	{
		auto beg = std::begin(con);
		auto ed = std::end(con);

		while (beg != ed)
		{
			//若此时(*beg).use_count()为1的话

			(*beg).reset();

			//那么此时(*beg).use_count()为0，且在reset时会调用析构函数

			++beg;
		}

		con.clear();
	}

	/*!
	 * \remarks 当顺序容器的元素不是标准库智能指针
	 * \return 
	 * \param C& con
	*/
	template<typename C>
	void _ungContainerClear(boost::false_type,C& con)
	{
		con.clear();
	}

	/*!
	 * \remarks 对顺序容器中的每个元素实施F算法
	 * \return 
	 * \param C & con
	 * \param F func
	*/
	template<typename C, typename F>
	void ungContainerLoop(C& con, F func)
	{
		//是否是线性序列容器
		BOOST_CONCEPT_ASSERT((boost::Sequence<C>));

		auto beg = con.begin();
		auto ed = con.end();

		while (beg != ed)
		{
			func(*beg);

			++beg;
		}
	}
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_STLWRAPPER_H_

/*
wrapper支持范围for。
*/