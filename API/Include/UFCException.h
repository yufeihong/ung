/*!
 * \brief
 * 异常实现。
 * \file UFCException.h
 *
 * \author Su Yang
 *
 * \date 2016/01/31
 */
#ifndef _UFC_EXCEPTION_H_
#define _UFC_EXCEPTION_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#ifndef _UFC_LOGMANAGER_H_
#include "UFCLogManager.h"
#endif

#ifndef _INCLUDE_BOOST_EXCEPTION_ALL_HPP_
#include "boost/exception/all.hpp"
#define _INCLUDE_BOOST_EXCEPTION_ALL_HPP_
#endif

namespace ung
{
	/*!
	 * \brief
	 * 异常实现。
	 * \class ExceptionImpl
	 *
	 * \author Su Yang
	 *
	 * \date 2016/01/31
	 *
	 * \todo
	 */
	class ExceptionImpl : public virtual std::exception, public virtual boost::exception
	{
	public:
		/*!
		 * \remarks 构造函数
		 * \return 
		*/
		ExceptionImpl() = default;
	};

	using ERRINFO_FILE_NAME					= boost::errinfo_file_name;
	using ERRINFO_INVALID_PARAMS		= boost::error_info<struct tag_invalid_params, String>;

#define EXCEPTION_POS boost::throw_function(BOOST_CURRENT_FUNCTION)												\
										<< boost::throw_file(__FILE__)																			\
										<< boost::throw_line(__LINE__)

#define UNG_EXCEPTION_BASE(name,type,param)																						\
try																																							\
{																																								\
	throw ExceptionImpl() << EXCEPTION_POS << boost::error_info<struct tag_##name,type>(param);			\
}																																								\
catch (ExceptionImpl& e)																															\
{																																								\
	LogManager::getInstance().logException(boost::diagnostic_information(e));												\
	throw;																																					\
}

//使用这个宏
#define UNG_EXCEPTION(desc) UNG_EXCEPTION_BASE(UNG_ERROR,String,desc)
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_EXCEPTION_H_