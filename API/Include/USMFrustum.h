/*!
 * \brief
 * 平截头体
 * \file USMFrustum.h
 *
 * \author Su Yang
 *
 * \date 2016/05/22
 */
#ifndef _USM_FRUSTUM_H_
#define _USM_FRUSTUM_H_

#ifndef _USM_CONFIG_H_
#include "USMConfig.h"
#endif

#ifdef USM_HIERARCHICAL_COMPILE

namespace ung
{
	/*!
	 * \brief
	 * 平截头体
	 * \class Frustum
	 *
	 * \author Su Yang
	 *
	 * \date 2017/02/09
	 *
	 * \todo
	 */
	class UngExport Frustum : public MemoryPool<Frustum>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		Frustum();

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param real_type aspect 宽高比
		 * \param Radian fovY
		 * \param real_type nDis
		 * \param real_type fDis
		*/
		Frustum(real_type aspect, Radian fovY = Radian(Consts<real_type>::THIRDPI), real_type nDis = 1.0, real_type fDis = 1000.0);

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~Frustum();

		/*!
		 * \remarks 设置Field Of View(FOV)(Y-dimension)
		 * \return 
		 * \param const Radian & fovy
		*/
		void setFOVy(const Radian& fovy);

		/*!
		 * \remarks 获取FOVy
		 * \return 单位为弧度
		*/
		const Radian& getFOVy() const;

		/*!
		 * \remarks 设置近截面的位置
		 * \return 
		 * \param real_type nearDist 近截面距离观察点的距离
		*/
		void setNearClipDistance(real_type nearDist);

		/*!
		 * \remarks 获取近截面的位置
		 * \return 
		*/
		real_type getNearClipDistance() const;

		/*!
		 * \remarks 设置远截面的位置
		 * \return 
		 * \param real_type farDist 远截面距离观察点的距离
		*/
		void setFarClipDistance(real_type farDist);

		/*!
		 * \remarks 获取远截面的位置
		 * \return 
		*/
		real_type getFarClipDistance() const;

		/*!
		 * \remarks 设置aspect ratio（宽高比率）
		 * \return 
		 * \param real_type ratio
		*/
		void setAspectRatio(real_type ratio);

		/*!
		 * \remarks 获取aspect ratio
		 * \return 
		*/
		real_type getAspectRatio() const;

		/*!
		 * \remarks 获取投影矩阵(OpenGL版)
		 * \return 
		*/
		const Matrix4& getProjectionMatrixOpenGL() const;

		/*!
		 * \remarks 获取投影矩阵(D3D版)
		 * \return 
		*/
		const Matrix4& getProjectionMatrixD3D() const;

		/*!
		 * \remarks 获取近平面的宽度
		 * \return 
		*/
		real_type getNearPlaneWidth() const;

		/*!
		 * \remarks 获取近平面的高度
		 * \return 
		*/
		real_type getNearPlaneHeight() const;

		/*!
		 * \remarks 获取远平面的宽度
		 * \return 
		*/
		real_type getFarPlaneWidth() const;

		/*!
		 * \remarks 获取远平面的高度
		 * \return 
		*/
		real_type getFarPlaneHeight() const;

	private:
		/*!
		 * \remarks 更新投影矩阵
		 * \return 
		*/
		void updateProjectionMatrix();

	private:
		Radian mFOVy;
		real_type mNearDist;
		real_type mFarDist;
		real_type mAspect;
		Matrix4 mProjMat;
	};
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE

#endif//_USM_FRUSTUM_H_