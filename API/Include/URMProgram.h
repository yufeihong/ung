/*!
 * \brief
 * GPU Program
 * \file URMProgram.h
 *
 * \author Su Yang
 *
 * \date 2017/06/10
 */
#ifndef _URM_PROGRAM_H_
#define _URM_PROGRAM_H_

#ifndef _URM_CONFIG_H_
#include "URMConfig.h"
#endif

#ifdef URM_HIERARCHICAL_COMPILE

#ifndef _URM_SHADER_H_
#include "URMShader.h"
#endif

#ifndef _UFC_POINTERCONTAINERWRAPPER_H_
#include "UFCPointerContainerWrapper.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * Program
	 * \class Program
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/10
	 *
	 * \todo
	 */
	class UngExport Program : public MemoryPool<Program>
	{
	public:
		using shaders_type = PointerListWrapper<Shader>;
		using borrow_type = typename shaders_type::borrow_type;

		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		Program();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~Program();

		/*!
		 * \remarks 连接shader到program
		 * \return 
		 * \param Shader * shader
		*/
		virtual void attach(Shader* shader) PURE;

		/*!
		 * \remarks 从program中分离shader
		 * \return 
		 * \param Shader * shader
		*/
		virtual void detach(Shader* shader) PURE;

		/*!
		 * \remarks 从program中分离所有的shader
		 * \return 
		*/
		virtual void detachAll() PURE;

		/*!
		 * \remarks 链接GPU程序
		 * \return 
		*/
		virtual void link() PURE;

		/*!
		 * \remarks 使用program
		 * \return 
		*/
		virtual void use() PURE;

	protected:
		shaders_type mShaders;
	};
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

#endif//_URM_PROGRAM_H_