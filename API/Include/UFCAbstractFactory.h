/*!
 * \brief
 * 抽象工厂
 * \file UFCAbstractFactory.h
 *
 * \author Su Yang
 *
 * \date 2016/04/24
 */
#ifndef _UFC_ABSTRACTFACTORY_H_
#define _UFC_ABSTRACTFACTORY_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#ifndef _UFC_METAFUNCTION_H_
#include "UFCMetaFunction.h"
#endif

#ifndef _UFC_GENSCATTERHIERARCHY_H_
#include "UFCGenScatterHierarchy.h"
#endif

#ifndef _UFC_GENLINEARHIERARCHY_H_
#include "UFCGenLinearHierarchy.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 抽象工厂的默认Unit，用于GenScatterHierarchy
	 * \class AbstractFactoryDefaultUnit
	 *
	 * \author Su Yang
	 *
	 * \date 2016/11/16
	 *
	 * \todo
	 */
	template <typename T>
	class AbstractFactoryDefaultUnit
	{
	public:
		virtual ~AbstractFactoryDefaultUnit()
		{
		}

		virtual T* doCreate(utp::Type2Type<T>) = 0;
	};

	/*!
	 * \brief
	 * 抽象工厂的Unit，用于GenLinearHierarchy
	 * \class AbstractFactoryUnit
	 *
	 * \author Su Yang
	 *
	 * \date 2016/11/16
	 *
	 * \todo
	 */
	template <typename ConcreteProduct, typename Base>
	class AbstractFactoryUnit : public Base
	{
		typedef typename Base::ProductList BaseProductList;

	protected:
		typedef typename BaseProductList::Tail ProductList;

	public:
		typedef typename BaseProductList::Head AbstractProduct;

		ConcreteProduct* doCreate(utp::Type2Type<AbstractProduct>)
		{
			return new ConcreteProduct;
		}
	};

	/*!
	 * \brief
	 * 抽象工厂
	 * \class AbstractFactory
	 *
	 * \author Su Yang
	 *
	 * \date 2016/11/16
	 *
	 * \todo
	 */
	template <typename TList, template <typename> class Unit = AbstractFactoryDefaultUnit>
	class AbstractFactory : public utp::GenScatterHierarchy<TList, Unit>
	{
	public:
		//抽象产品列表
		typedef TList ProductList;

		template <typename T>
		T* create()
		{
			Unit<T>& unit = *this;

			return unit.doCreate(utp::Type2Type<T>());
		}
	};

	/*!
	 * \brief
	 * 具体工厂
	 * \class ConcreteFactory
	 *
	 * \author Su Yang
	 *
	 * \date 2016/11/16
	 *
	 * \todo
	 */
	template <typename AbstractFact, template <typename, typename> class Creator = AbstractFactoryUnit, typename TList = typename AbstractFact::ProductList>
	class ConcreteFactory : public utp::GenLinearHierarchy<typename utp::Reverse<TList>::result, Creator, AbstractFact>
	{
	public:
		typedef typename AbstractFact::ProductList ProductList;
		typedef TList ConcreteProductList;
	};
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_ABSTRACTFACTORY_H_