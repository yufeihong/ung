/*!
 * \brief
 * 红黑树
 * \file UTLRBTree.h
 *
 * \author Su Yang
 *
 * \date 2016/03/17
 */
#ifndef _UNG_UTL_RBTREE_H_
#define _UNG_UTL_RBTREE_H_

#ifndef _UNG_UTL_CONFIG_H_
#include "UTLConfig.h"
#endif

#ifndef _UNG_UTL_ALLOCATOR_H_
#include "UTLAllocator.h"
#endif

#ifndef _UNG_UTL_RBTREEITERATOR_H_
#include "UTLRBTreeIterator.h"
#endif

#ifndef _UNG_UTL_CONSTRUCT_H_
#include "UTLConstruct.h"
#endif

namespace ung
{
	namespace utl
	{
		using color_type = bool;
		color_type const rbtree_red = false;
		color_type const rbtree_black = true;

		/*!
		 * \brief
		 * 红黑树的节点基类。
		 * \class rbtree_node_base
		 *
		 * \author Su Yang
		 *
		 * \date 2016/03/17
		 *
		 * \todo
		 */
		struct rbtree_node_base
		{
			using base_ptr = rbtree_node_base*;

			/*!
			 * \remarks 获取值最小的节点
			 * \return 
			 * \param base_ptr x
			*/
			static base_ptr minimum(base_ptr x)
			{
				while (x->mLeft)
				{
					x = x->mLeft;
				}

				return x;
			}

			/*!
			 * \remarks 获取值最大的节点
			 * \return 
			 * \param base_ptr x
			*/
			static base_ptr maximum(base_ptr x)
			{
				while (x->mRight)
				{
					x = x->mRight;
				}

				return x;
			}

			color_type mColor;
			/*
			由于RB-tree的各种操作时常需要上溯其父节点，所以特别在数据结构中安排一个parent指针
			*/
			base_ptr mParent = nullptr;
			base_ptr mLeft = nullptr;
			base_ptr mRight = nullptr;
		};

		/*!
		 * \brief
		 * 红黑树的节点类。
		 * \class rbtree_node
		 *
		 * \author Su Yang
		 *
		 * \date 2016/03/17
		 *
		 * \todo
		 */
		template<typename T>
		struct rbtree_node : public rbtree_node_base
		{
			using value_type = T;
			using pointer = rbtree_node<value_type>*;

			value_type mValue;			//节点值
		};

		/*!
		 * \brief
		 * 红黑树。
		 * \class RBTree
		 *
		 * \author Su Yang
		 *
		 * \date 2016/03/17
		 *
		 * \todo
		 */
		template<typename Key,typename Value,typename KeyOfValue,typename Compare,typename Alloc = Allocator<rbtree_node<Value>>>
		class RBTree
		{
		public:
			using key_type = Key;
			using value_type = Value;
			using pointer = value_type*;
			using const_pointer = value_type const*;
			using reference = value_type&;
			using const_reference = value_type const&;
			using size_type = usize;
			using difference_type = ssize;
			using node_allocator = Alloc;
			using self_type = RBTree<key_type, value_type, KeyOfValue, Compare, Alloc>;

			using node_base_ptr = typename rbtree_node_base::base_ptr;
			using node_ptr = typename rbtree_node<value_type>::pointer;

			using iterator = typename boost::mpl::if_<boost::is_same<key_type, value_type>,
																		typename RBTreeIterator<value_type,reference,pointer>::const_iterator,
																		typename RBTreeIterator<value_type,reference,pointer>::iterator>::type;
			using const_iterator = typename RBTreeIterator<value_type,reference,pointer>::const_iterator;
			using reverse_iterator = std::reverse_iterator<iterator>;
			using const_reverse_iterator = std::reverse_iterator<const_iterator>;

			/*!
			 * \remarks 默认构造函数
			 * \return 
			 * \param Compare const & comp
			*/
			RBTree(Compare const& comp = Compare()) :
				mNodeCount(0),
				mKeyCompare(comp)
			{
				init_();
			}

			/*!
			 * \remarks 拷贝构造函数
			 * \return 
			 * \param self_type const & x
			*/
			RBTree(self_type const& x) :
				mNodeCount(0),
				mKeyCompare(x.mKeyCompare)
			{
				if (x.root() == nullptr)
				{
					init_();
				}
				else
				{
					color(mHeader) = rbtree_red;
					root() = copy_(x.root(), mHeader);
					leftMost() = minimum(root());
					rightMost() = maximum(root());
				}

				mNodeCount = x.mNodeCount;
			}

			/*!
			 * \remarks 拷贝赋值运算符
			 * \return 
			 * \param self_type const& x
			*/
			self_type& operator=(self_type const& x)
			{
				if (this != &x)
				{
					clear();
					mNodeCount = 0;
					mKeyCompare = x.mKeyCompare;
					if (x.root() == nullptr)
					{
						root() = nullptr;
						leftMost() = mHeader;
						rightMost() = mHeader;
					}
					else
					{
						root() = copy_(x.root(), mHeader);
						leftMost() = minimum(root());
						rightMost() = maximum(root());
						mNodeCount = x.mNodeCount;
					}
				}

				return *this;
			}

			/*!
			 * \remarks 析构函数
			 * \return 
			*/
			~RBTree()
			{
				clear();

				putNode(mHeader);
			}

			/*!
			 * \remarks 分配一个节点
			 * \return 
			*/
			node_ptr getNode()
			{
				return mNodeAllocator.allocate(1);
			}

			/*!
			 * \remarks 释放一个节点
			 * \return 
			 * \param node_ptr p
			*/
			void putNode(node_ptr p)
			{
				mNodeAllocator.deallocate(p,1);
			}

			/*!
			 * \remarks 创建一个节点
			 * \return 
			 * \param value_type const & v 节点的值
			*/
			node_ptr createNode(value_type const& v)
			{
				//配置空间
				node_ptr tmp = getNode();

				//构造内容
				try
				{
					construct(&tmp->mValue, v);
				}
				catch (...)
				{
					putNode(tmp);
					throw;
				}

				return tmp;
			}

			/*!
			 * \remarks 复制一个节点
			 * \return 
			 * \param node_ptr n
			*/
			node_ptr cloneNode(node_ptr n)
			{
				node_ptr tmp = createNode(n->mValue);
				tmp->mColor = n->mColor;
				tmp->mLeft = nullptr;
				tmp->mRight = nullptr;

				return tmp;
			}

			/*!
			 * \remarks 销毁节点
			 * \return 
			 * \param node_ptr n
			*/
			void destroyNode(node_ptr n)
			{
				//析构内容
				destroy(&n->mValue);
				//释放内存
				putNode(n);
			}

			void clear()
			{
				if (mNodeCount != 0)
				{
					erase_(root());
					leftMost() = mHeader;
					root() = nullptr;
					rightMost() = mHeader;
					mNodeCount = 0;
				}
			}

			/*!
			 * \remarks 获取红黑树的大小比较准则
			 * \return 
			*/
			Compare getKeyComp() const
			{
				return mKeyCompare;
			}

			/*!
			 * \remarks 获取指向首元素的迭代器
			 * \return 
			*/
			iterator begin()
			{
				//红黑树的起头为最左（最小）节点处
				return iterator(leftMost());
			}

			/*!
			 * \remarks 获取指向首元素的迭代器,常量对象调用
			 * \return 常量迭代器
			*/
			const_iterator begin() const
			{
				return const_iterator(leftMost());
			}

			/*!
			 * \remarks 获取指向尾后位置的迭代器
			 * \return 
			*/
			iterator end()
			{
				//红黑树的终点为Header所指处
				return iterator(mHeader);
			}

			/*!
			 * \remarks 获取指向尾后位置的迭代器,常量对象调用
			 * \return 常量迭代器
			*/
			const_iterator end() const
			{
				return const_iterator(mHeader);
			}

			/*!
			 * \remarks 获取指向尾后位置的迭代器
			 * \return 
			*/
			reverse_iterator rbegin()
			{
				return reverse_iterator(end());
			}

			/*!
			 * \remarks 获取指向尾后位置的迭代器,常量对象调用
			 * \return 常量迭代器
			*/
			const_reverse_iterator rbegin() const
			{
				return const_reverse_iterator(end());
			}

			/*!
			 * \remarks 获取指向首元素的迭代器
			 * \return 
			*/
			reverse_iterator rend()
			{
				return reverse_iterator(begin());
			}

			/*!
			 * \remarks 获取指向首元素的迭代器,常量对象调用
			 * \return 常量迭代器
			*/
			const_reverse_iterator rend() const
			{
				return const_reverse_iterator(begin());
			}

			/*!
			 * \remarks 判断是否为空
			 * \return 
			*/
			bool empty() const
			{
				return mNodeCount == 0;
			}

			/*!
			 * \remarks 获取元素数量
			 * \return 
			*/
			size_type size() const
			{
				return mNodeCount;
			}

			/*!
			 * \remarks 交换
			 * \return 
			 * \param self_type& x
			*/
			void swap(self_type& x)
			{
				std::swap(mHeader, x.mHeader);
				std::swap(mNodeCount, x.mNodeCount);
				std::swap(mKeyCompare, x.mKeyCompare);
			}

			/*!
			 * \remarks 插入到红黑树中，保持节点值独一无二
			 * \return 
			 * \param value_type const& v
			*/
			std::pair<iterator, bool> insertUnique(value_type const& v)
			{
				node_ptr y = mHeader;
				node_ptr x = root();									//从根节点开始
				bool comp = true;

				while (x)
				{
					y = x;
					//v键值小于目前节点之键值?
					comp = mKeyCompare(KeyOfValue()(v), key(x));
					//遇“大”则往左，遇“小于或等于”则往右
					x = comp ? left(x) : right(x);
				}

				//离开while循环后，y所指即插入点之父节点（此时的它必为叶子节点）

				//令迭代器it指向插入点之父节点y
				iterator it(y);

				//如果离开while循环时comp为真（表示遇“大”，将插入于左侧）
				if (comp)
				{
					//如果插入点之父节点为最左节点
					if (it == begin())
					{
						//x为插入点，y为插入点之父节点，v为新值
						return std::pair<iterator, bool>(insert_(x, y, v), true);
					}
					else
					{
						--it;
					}
				}

				//新键值不与既有节点之键值重复的话，执行安插操作
				if (mKeyCompare(key(it.mNode),KeyOfValue()(v)))
				{
					//x为插入点，y为插入点之父节点，v为新值
					return std::pair<iterator, bool>(insert_(x, y, v), true);
				}

				//进行至此，表示新值一定与树中键值重复，那么就不插入新值
				return std::pair<iterator, bool>(it, false);
			}

			/*!
			 * \remarks 插入到红黑树中，保持节点值独一无二
			 * \return 
			 * \param iterator position
			 * \param const value_type& v
			*/
			iterator insertUnique(iterator position, const_reference v)
			{
				if (position.mNode == mHeader->mLeft)
				{
					if (size() > 0 && mKeyCompare(KeyOfValue()(v), Key(position.mNode)))
					{
						return insert_(position.mNode, position.mNode, v);
					}
					else
					{
						return insertUnique(v).first;
					}
				}
				else if (position.mNode == mHeader)
				{
					if (mKeyCompare(Key(rightMost()), KeyOfValue()(v)))
					{
						return insert_(nullptr, rightMost(), v);
					}
					else
					{
						return insertUnique(v).first;
					}	
				}
				else
				{
					iterator bef = position;
					--bef;
					if (mKeyCompare(Key(bef.mNode), KeyOfValue()(v)) && mKeyCompare(KeyOfValue()(v), Key(position.mNode)))
					{
						if (right(bef.mNode) == nullptr)
						{
							return insert_(0, bef.mNode, v);
						}
						else
						{
							return insert_(position.mNode, position.mNode, v);
						}	
					}
					else
					{
						return insertUnique(v).first;
					}	
				}
			}

			/*!
			 * \remarks 插入到红黑树中，保持节点值独一无二
			 * \return 
			 * \param const_iterator first
			 * \param const_iterator last
			*/
			void insertUnique(const_iterator first, const_iterator last)
			{
				for (; first != last; ++first)
				{
					insertUnique(*first);
				}
			}

			/*!
			 * \remarks 插入到红黑树中，保持节点值独一无二
			 * \return 
			 * \param const_pointer first
			 * \param const_pointer last
			*/
			void insertUnique(const_pointer first, const_pointer last)
			{
				for (; first != last; ++first)
				{
					insertUnique(*first);
				}
			}

			/*!
			 * \remarks 插入到红黑树中，允许节点值重复
			 * \return 
			 * \param const_reference x
			*/
			iterator insertEqual(const_reference x)
			{
				node_ptr y = mHeader;
				node_ptr x = root();									//从根节点开始

				while (x)
				{
					y = x;
					//遇“大”则往左，遇“小于或等于”则往右
					x = mKeyCompare(KeyOfValue()(v), key(x)) ? left(x) : right(x);
				}

				//x为新值插入点，y为插入点的父节点，v为新值
				return insert_(x, y, v);
			}

			/*!
			 * \remarks 插入到红黑树中，允许节点值重复
			 * \return 
			 * \param iterator position
			 * \param const_reference v
			*/
			iterator insertEqual(iterator position, const_reference v)
			{
				if (position.mNode == mHeader->mLeft)
				{
					if (size() > 0 && !mKeyCompare(key(position.mNode), KeyOfValue()(v)))
					{
						return insert_(position.mNode, position.mNode, v);
					}
					else
					{
						return insertEqual(v);
					}
				}
				else if (position.mNode == mHeader)
				{
					if (!mKeyCompare(KeyOfValue()(v), key(rightMost())))
					{
						return insert_(nullptr, rightMost(), v);
					}
					else
					{
						return insertEqual(v);
					}
				}
				else
				{
					iterator bef = position;
					--bef;
					if (!mKeyCompare(KeyOfValue()(v), key(bef.mNode)) && !mKeyCompare(key(position.mNode), KeyOfValue()(v)))
					{
						if (right(bef.mNode) == nullptr)
						{
							return insert_(nullptr, bef.mNode, v);
						}
						else
						{
							return insert_(position.mNode, position.mNode, v);
						}
					}
					else
					{
						return insertEqual(v);
					}
				}
			}

			/*!
			 * \remarks 插入到红黑树中，允许节点值重复
			 * \return 
			 * \param const_iterator first
			 * \param const_iterator last
			*/
			void insertEqual(const_iterator first, const_iterator last)
			{
				for (; first != last; ++first)
				{
					insertEqual(*first);
				}
			}

			/*!
			 * \remarks 插入到红黑树中，允许节点值重复
			 * \return 
			 * \param const_pointer first
			 * \param const_pointer last
			*/
			void insertEqual(const_pointer first, const_pointer last)
			{
				for (; first != last; ++first)
				{
					insertEqual(*first);
				}
			}

			/*!
			 * \remarks 删除迭代器所指向的节点
			 * \return 
			 * \param iterator position
			*/
			void erase(iterator position)
			{
				node_ptr y = (node_ptr)rebalance_for_erase_(position.mNode,mHeader->mParent,mHeader->mLeft,mHeader->mRight);
				destroyNode(y);
				--mNodeCount;
			}

			/*!
			 * \remarks 删除迭代器区间[first,last)内的节点
			 * \return 
			 * \param iterator first
			 * \param iterator last
			*/
			void erase(iterator first, iterator last)
			{
				if (first == begin() && last == end())
				{
					clear();
				}
				else
				{
					while (first != last)
					{
						erase(first++);
					}
				}
			}

			/*!
			 * \remarks 删除关键字所标识的节点
			 * \return 
			 * \param const Key & x
			*/
			size_type erase(const Key& x)
			{
				std::pair<iterator, iterator> p = equalRange(x);
				size_type n = 0;

				iterator f = p.first;
				iterator l = p.second;
				while (f != l)
				{
					++f;
					++n;
				}

				erase(p.first, p.second);

				return n;
			}

			/*!
			 * \remarks 删除关键字区间[first,last)所标识的节点
			 * \return 
			 * \param const Key * first
			 * \param const Key * last
			*/
			void erase(const Key* first, const Key* last)
			{
				while (first != last)
				{
					erase(*first++);
				}
			}

			/*!
			 * \remarks 查找关键字所标识的节点
			 * \return 
			 * \param const Key & k
			*/
			iterator find(const Key& k)
			{
				node_ptr y = mHeader;
				node_ptr x = root();

				while (x)
				{
					if (!mKeyCompare(key(x), k))
					{
						y = x, x = left(x);
					}
					else
					{
						x = right(x);
					}
				}

				iterator it = iterator(y);

				return (it == end() || mKeyCompare(k, key(it.mNode))) ? end() : it;
			}

			/*!
			 * \remarks 查找关键字所标识的节点，常量对象调用
			 * \return 常量迭代器
			 * \param const Key & k
			*/
			const_iterator find(const Key& k) const
			{
				node_ptr y = mHeader;
				node_ptr x = root();

				while (x)
				{
					if (!mKeyCompare(key(x), k))
					{
						y = x, x = left(x);
					}
					else
					{
						x = right(x);
					}
				}

				const_iterator it = const_iterator(y);

				return (it == end() || mKeyCompare(k, key(it.mNode))) ? end() : it;
			}

			/*!
			 * \remarks 
			 * \return 
			 * \param const Key & k
			*/
			size_type count(const Key& k) const
			{
				std::pair<const_iterator, const_iterator> p = equalRange(k);
				size_type n = 0;
				while (p.first != p.second)
				{
					++p.first;
					++n;
				}

				return n;
			}

			iterator lowerBound(const Key& k)
			{
				node_ptr y = mHeader;
				node_ptr x = root();

				while (x)
				{
					if (!mKeyCompare(key(x), k))
					{
						y = x, x = left(x);
					}
					else
					{
						x = right(x);
					}	
				}

				return iterator(y);
			}

			
			const_iterator lowerBound(const Key& k) const
			{
				node_ptr y = mHeader;
				node_ptr x = root();

				while (x)
				{
					if (!mKeyCompare(key(x), k))
					{
						y = x;
						x = left(x);
					}
					else
					{
						x = right(x);
					}
				}

				return const_iterator(y);
			}

			iterator upperBound(const Key& k)
			{
				node_ptr y = mHeader;
				node_ptr x = root();

				while (x)
				{
					if (mKeyCompare(k, key(x)))
					{
						y = x;
						x = left(x);
					}
					else
					{
						x = right(x);
					}
				}

				return iterator(y);
			}

			const_iterator upperBound(const Key& k) const
			{
				node_ptr y = mHeader;
				node_ptr x = root();

				while (x)
				{
					if (mKeyCompare(k, key(x)))
					{
						y = x;
						x = left(x);
					}
					else
					{
						x = right(x);
					}
				}

				return const_iterator(y);
			}

			std::pair<iterator,iterator> equalRange(const Key& k)
			{
				return std::pair<iterator, iterator>(lowerBound(k), upperBound(k));
			}

			std::pair<const_iterator,const_iterator> equalRange(const Key& k) const
			{
				return std::pair<const_iterator, const_iterator>(lowerBound(k),upperBound(k));
			}

			int black_count_(node_base_ptr node, node_base_ptr root)
			{
				if (node == 0)
				{
					return 0;
				}
				else
				{
					int bc = node->mColor == rbtree_black ? 1 : 0;
					if (node == root)
					{
						return bc;
					}
					else
					{
						return bc + black_count_(node->mParent, root);
					}
				}
			}

			bool verify() const
			{
				if (mNodeCount == 0 || begin() == end())
				{
					return mNodeCount == 0 && begin() == end() && mHeader->mLeft == mHeader && mHeader->mRight == mHeader;
				}

				int len = black_count_(leftMost(), root());
				for (const_iterator it = begin(); it != end(); ++it)
				{
					node_ptr x = (node_ptr)it.mNode;
					node_ptr L = left(x);
					node_ptr R = right(x);

					if (x->mColor == rbtree_red)
					{
						if ((L && L->mColor == rbtree_red) || (R && R->mColor == rbtree_red))
						{
							return false;
						}
					}

					if (L && mKeyCompare(key(x), key(L)))
					{
						return false;
					}
					if (R && mKeyCompare(key(R), key(x)))
					{
						return false;
					}

					if (!L && !R && black_count_(x, root()) != len)
					{
						return false;
					}
				}

				if (leftMost() != rbtree_node_base::minimum(root()))
				{
					return false;
				}
				if (rightMost() != rbtree_node_base::maximum(root()))
				{
					return false;
				}

				return true;
			}

			/*!
			 * \remarks 获取Header的成员
			 * \return 
			*/
			node_ptr& root() const
			{
				return (node_ptr&)mHeader->mParent;
			}

			/*!
			 * \remarks 获取Header的成员
			 * \return 
			*/
			node_ptr& leftMost() const
			{
				return (node_ptr&)mHeader->mLeft;
			}

			/*!
			* \remarks 获取Header的成员
			* \return
			*/
			node_ptr& rightMost() const
			{
				return (node_ptr&)mHeader->mRight;
			}

			/*!
			 * \remarks 获取给定节点的左子节点
			 * \return 
			 * \param node_ptr x
			*/
			static node_ptr& left(node_ptr x)
			{
				return (node_ptr&)(x->mLeft);
			}

			/*!
			* \remarks 获取给定节点的左子节点
			* \return
			* \param node_base_ptr x
			*/
			static node_ptr& left(node_base_ptr x)
			{
				return (node_ptr&)(x->mLeft);
			}

			/*!
			* \remarks 获取给定节点的右子节点
			* \return
			* \param node_ptr x
			*/
			static node_ptr& right(node_ptr x)
			{
				return (node_ptr&)(x->mRight);
			}

			/*!
			* \remarks 获取给定节点的右子节点
			* \return
			* \param node_base_ptr x
			*/
			static node_ptr& right(node_base_ptr x)
			{
				return (node_ptr&)(x->mRight);
			}

			/*!
			* \remarks 获取给定节点的父节点
			* \return
			* \param node_ptr x
			*/
			static node_ptr& parent(node_ptr x)
			{
				return (node_ptr&)(x->mParent);
			}

			/*!
			* \remarks 获取给定节点的父节点
			* \return
			* \param node_base_ptr x
			*/
			static node_ptr& parent(node_base_ptr x)
			{
				return (node_ptr&)(x->mParent);
			}

			/*!
			* \remarks 获取给定节点的值
			* \return
			* \param node_ptr x
			*/
			static reference& value(node_ptr x)
			{
				return x->mValue;
			}

			/*!
			* \remarks 获取给定节点的值
			* \return
			* \param node_base_ptr x
			*/
			static reference& value(node_base_ptr x)
			{
				return ((node_ptr)x)->mValue;
			}

			static key_type const& key(node_ptr x)
			{
				return KeyOfValue()(value(x));
			}

			static key_type const& key(node_base_ptr x)
			{
				return KeyOfValue()(value(node_ptr(x)));
			}

			/*!
			 * \remarks 获取给定节点的颜色值
			 * \return 
			 * \param node_ptr x
			*/
			static color_type& color(node_ptr x)
			{
				return x->mColor;
			}

			/*!
			* \remarks 获取给定节点的颜色值
			* \return
			* \param node_base_ptr x
			*/
			static color_type& color(node_base_ptr x)
			{
				return ((node_ptr)x)->mColor;
			}

			/*!
			 * \remarks 获取最小值节点
			 * \return 
			 * \param node_ptr x
			*/
			static node_ptr minimum(node_ptr x)
			{
				return (node_ptr)rbtree_node_base::minimum(x);
			}

			/*!
			* \remarks 获取最大值节点
			* \return
			* \param node_ptr x
			*/
			static node_ptr maximum(node_ptr x)
			{
				return (node_ptr)rbtree_node_base::maximum(x);
			}

		private:
			/*!
			 * \remarks 插入
			 * \return 
			 * \param node_base_ptr x_ 新值插入点
			 * \param node_base_ptr y_ 插入点之父节点
			 * \param value_type const & v 新值
			*/
			iterator insert_(node_base_ptr x_, node_base_ptr y_, value_type const& v)
			{
				node_ptr x = (node_ptr)x_;
				node_ptr y = (node_ptr)y_;
				node_ptr z;

				if (y == mHeader || x || mKeyCompare(KeyOfValue()(v),key(y)))
				{
					//产生一个新节点
					z = createNode(v);
					left(y) = z;
					if (y == mHeader)
					{
						root() = z;
						rightMost() = z;
					}
					//如果y为最左节点
					else if(y == leftMost())
					{
						//维护leftMost()，使它永远指向最左节点
						leftMost() = z;
					}
				}
				else
				{
					//产生一个新节点
					z = createNode(v);
					//令新节点成为插入点之父节点y的右子节点
					right(y) = z;
					if (y == rightMost())
					{
						//维护rightMost()，使它永远指向最右节点
						rightMost() = z;
					}
				}

				//设定新节点的父节点
				parent(z) = y;
				//设定新节点的左子节点
				left(z) = nullptr;
				//设定新节点的右子节点
				right(z) = nullptr;
				//新节点的颜色将在rebalance_()设定（并调整）
				rebalance_(z, mHeader->mParent);

				++mNodeCount;

				//返回一个迭代器，指向新增节点
				return iterator(z);
			}

			void rebalance_(node_base_ptr x, node_base_ptr& root)
			{
#if UTL_DEBUGMODE
				std::cout << "call rebalance_" << std::endl;
#endif

				//新节点必为红
				x->mColor = rbtree_red;

				while (x != root && x->mParent->mColor == rbtree_red)
				{
					//父节点为祖父节点之左子节点
					if (x->mParent == x->mParent->mParent->mLeft)
					{
						//令y为伯父节点
						node_base_ptr y = x->mParent->mParent->mRight;
						//伯父节点存在，且为红
						if (y && y->mColor == rbtree_red)
						{
							//更改父节点为黑
							x->mParent->mColor = rbtree_black;
							//更改伯父节点为黑
							y->mColor = rbtree_black;
							//更改祖父节点为红
							x->mParent->mParent->mColor = rbtree_red;

							x = x->mParent->mParent;
						}
						//无伯父节点，或伯父节点为黑
						else
						{
							//如果新节点为父节点之右子节点
							if (x == x->mParent->mRight)
							{
								x = x->mParent;

								//第一参数为左旋点
								rotate_left_(x, root);
							}

							//改变颜色
							x->mParent->mColor = rbtree_black;
							x->mParent->mParent->mColor = rbtree_red;

							//第一参数为右旋点
							rotate_right_(x->mParent->mParent, root);
						}
					}
					//父节点为祖父节点之右子节点
					else
					{
						//令y为伯父节点
						node_base_ptr y = x->mParent->mParent->mLeft;

						//有伯父节点，且为红
						if (y && y->mColor == rbtree_red)
						{
							//更改父节点为黑
							x->mParent->mColor = rbtree_black;
							//更改伯父节点为黑
							y->mColor = rbtree_black;
							//更改祖父节点为红
							x->mParent->mParent->mColor = rbtree_red;

							//准备继续往上层检查
							x = x->mParent->mParent;
						}
						//无伯父节点，或伯父节点为黑
						else
						{
							//如果新节点为父节点之左子节点
							if (x == x->mParent->mLeft)
							{
								x = x->mParent;

								//第一参数为右旋点
								rotate_right_(x, root);
							}

							//改变颜色
							x->mParent->mColor = rbtree_black;
							x->mParent->mParent->mColor = rbtree_red;

							//第一参数为左旋点
							rotate_left_(x->mParent->mParent, root);
						}
					}
				}//while结束

				//根节点永远为黑
				root->mColor = rbtree_black;
			}

			node_base_ptr rebalance_for_erase_(node_base_ptr z,node_base_ptr& root,node_base_ptr& leftmost,node_base_ptr& rightmost)
			{
#if UTL_DEBUGMODE
				std::cout << "call rebalance_for_erase_" << std::endl;
#endif

				node_base_ptr y = z;
				node_base_ptr x = nullptr;
				node_base_ptr x_parent = nullptr;

				if (y->mLeft == nullptr)
				{
					x = y->mRight;
				}
				else
				{
					if (y->mRight == nullptr)
					{
						x = y->mLeft;
					}
					else
					{
						y = y->mRight;
						while (y->mLeft)
						{
							y = y->mLeft;
						}

						x = y->mRight;
					}
				}

				if (y != z)
				{
					z->mLeft->mParent = y;
					y->mLeft = z->mLeft;
					if (y != z->mRight)
					{
						x_parent = y->mParent;
						if (x) x->mParent = y->mParent;
						y->mParent->mLeft = x;
						y->mRight = z->mRight;
						z->mRight->mParent = y;
					}
					else
					{
						x_parent = y;
					}
							
					if (root == z)
					{
						root = y;
					}	
					else if (z->mParent->mLeft == z)
					{
						z->mParent->mLeft = y;
					}	
					else
					{
						z->mParent->mRight = y;
					}
							
					y->mParent = z->mParent;
					std::swap(y->mColor, z->mColor);
					y = z;
				}
				else
				{
					x_parent = y->mParent;
					if (x)
					{
						x->mParent = y->mParent;
					}
							
					if (root == z)
					{
						root = x;
					}
							
					else
					{
						if (z->mParent->mLeft == z)
						{
							z->mParent->mLeft = x;
						}
						else
						{
							z->mParent->mRight = x;
						}
					}
								
					if (leftmost == z)
					{
						if (z->mRight == nullptr)
						{
							leftmost = z->mParent;
						}
						else
						{
							leftmost = rbtree_node_base::minimum(x);
						}
					}
								
					if (rightmost == z)
					{
						if (z->mLeft == nullptr)
						{
							rightmost = z->mParent;
						}
						else
						{
							rightmost = rbtree_node_base::maximum(x);
						}
					}
				}

				if (y->mColor != rbtree_red)
				{
					while (x != root && (!x || x->mColor == rbtree_black))
					{
						if (x == x_parent->mLeft)
						{
							node_base_ptr w = x_parent->mRight;

							if (w->mColor == rbtree_red)
							{
								w->mColor = rbtree_black;
								x_parent->mColor = rbtree_red;
								rotate_left_(x_parent, root);
								w = x_parent->mRight;
							}

							if ((w->mLeft == nullptr || w->mLeft->mColor == rbtree_black) && (w->mRight == nullptr || w->mRight->mColor == rbtree_black))
							{
								w->mColor = rbtree_red;
								x = x_parent;
								x_parent = x_parent->mParent;
							}
							else
							{
								if (w->mRight == nullptr || w->mRight->mColor == rbtree_black)
								{
									if (w->mLeft)
									{
										w->mLeft->mColor = rbtree_black;
									}

									w->mColor = rbtree_red;
									rotate_right_(w, root);
									w = x_parent->mRight;
								}
								w->mColor = x_parent->mColor;
								x_parent->mColor = rbtree_black;
								if (w->mRight) w->mRight->mColor = rbtree_black;
								rotate_left_(x_parent, root);
								break;
							}
						}
						else
						{
							node_base_ptr w = x_parent->mLeft;
							if (w->mColor == rbtree_red)
							{
								w->mColor = rbtree_black;
								x_parent->mColor = rbtree_red;
								rotate_right_(x_parent, root);
								w = x_parent->mLeft;
							}
							if ((w->mRight == nullptr || w->mRight->mColor == rbtree_black) && (w->mLeft == nullptr || w->mLeft->mColor == rbtree_black))
							{
								w->mColor = rbtree_red;
								x = x_parent;
								x_parent = x_parent->mParent;
							}
							else
							{
								if (w->mLeft == nullptr || w->mLeft->mColor == rbtree_black)
								{
									if (w->mRight)
									{
										w->mRight->mColor = rbtree_black;
									}

									w->mColor = rbtree_red;
									rotate_left_(w, root);
									w = x_parent->mLeft;
								}
								w->mColor = x_parent->mColor;
								x_parent->mColor = rbtree_black;
								if (w->mLeft) w->mLeft->mColor = rbtree_black;
								rotate_right_(x_parent, root);
								break;
							}
						}
					}//end while

					if (x)
					{
						x->mColor = rbtree_black;
					}
				}//end if (y->mColor != rbtree_red)

				return y;
			}

			/*!
			 * \remarks 新节点必为红节点，如果插入处之父节点亦为红节点，就违反了红黑树规则，需要做旋转
			 * \return 
			 * \param node_base_ptr x 旋转点
			 * \param node_base_ptr & root
			*/
			void rotate_left_(node_base_ptr x, node_base_ptr& root)
			{
#if UTL_DEBUGMODE
				std::cout << "call rotate_left_" << std::endl;
#endif

				//令y为旋转点的右子节点
				node_base_ptr y = x->mRight;
				x->mRight = y->mLeft;
				if (y->mLeft)
				{
					y->mLeft->mParent = x;
				}
				y->mParent = x->mParent;

				//令y完全顶替x的地位（必须将x对其父节点的关系完全接收过来）
				//x为根节点
				if (x == root)
				{
					root = y;
				}
				//x为其父节点的左子节点
				else if (x == x->mParent->mLeft)
				{
					x->mParent->mLeft = y;
				}
				//x为其父节点的右子节点
				else
				{
					x->mParent->mRight = y;
				}

				y->mLeft = x;
				x->mParent = y;
			}

			/*!
			 * \remarks 新节点必为红节点，如果插入处之父节点亦为红节点，就违反了红黑树规则，需要做旋转
			 * \return 
			 * \param node_base_ptr x 旋转点
			 * \param node_base_ptr & root
			*/
			void rotate_right_(node_base_ptr x,node_base_ptr& root)
			{
#if UTL_DEBUGMODE
				std::cout << "call rotate_right_" << std::endl;
#endif

				//y为旋转点的左子节点
				node_base_ptr y = x->mLeft;
				x->mLeft = y->mRight;
				if (y->mRight)
				{
					y->mRight->mParent = x;
				}
				y->mParent = x->mParent;

				//令y完全顶替x的地位（必须将x对其父节点的关系完全接收过来）
				//x为根节点
				if (x == root)
				{
					root = y;
				}
				//x为其父节点的右子节点
				else if (x == x->mParent->mRight)
				{
					x->mParent->mRight = y;
				}
				//x为其父节点的左子节点
				else
				{
					x->mParent->mLeft = y;
				}

				y->mRight = x;
				x->mParent = y;
			}

			node_ptr copy_(node_ptr x, node_ptr p)
			{
				node_ptr t = cloneNode(x);
				t->mParent = p;

				try
				{
					if (x->mRight)
					{
						t->mRight = copy_(right(x), t);
					}
					p = t;
					x = left(x);

					while (x != 0)
					{
						node_ptr y = cloneNode(x);
						p->mLeft = y;
						y->mParent = p;
						if (x->mRight)
						{
							y->mRight = copy_(right(x), y);
						}
						p = y;
						x = left(x);
					}
				}
				catch (...)
				{
					erase_(t);
					throw;
				}

				return t;
			}

			void erase_(node_ptr x)
			{
				while (x)
				{
					erase_(right(x));
					node_ptr y = left(x);
					destroyNode(x);
					x = y;
				}
			}

			void init_()
			{
				//分配一个节点空间，令Header指向它
				mHeader = getNode();
				//令Header为红色，用来在iterator.operator--中区分Header和root
				color(mHeader) = rbtree_red;
				//设置Header的3个指针成员
				root() = nullptr;										//parent
				leftMost() = mHeader;
				rightMost() = mHeader;
			}

		private:
			node_allocator mNodeAllocator;					//专属空间配置器，每次可恰恰配置一个节点
			size_type mNodeCount;
			node_ptr mHeader;										//实现上的一个技巧,Header和root互为对方的父节点。其父节点指向根节点，左子节点指向最小节点，右子节点指向最大节点
			Compare mKeyCompare;								//节点间的键值大小比较准则
		};

		/*!
		 * \remarks 判断x == y
		 * \return 
		 * \param const RBTree<Key,Value,KeyOfValue,Compare,Alloc>& x
		 * \param const RBTree<Key,Value,KeyOfValue,Compare,Alloc>& y
		*/
		template <typename Key,typename Value,typename KeyOfValue,typename Compare,typename Alloc>
		inline bool operator==(const RBTree<Key, Value, KeyOfValue, Compare, Alloc>& x,const RBTree<Key, Value, KeyOfValue, Compare, Alloc>& y)
		{
			return x.size() == y.size() && std::equal(x.begin(), x.end(), y.begin());
		}

		/*!
		 * \remarks 判断x < y
		 * \return 
		 * \param const RBTree<Key,Value,KeyOfValue,Compare,Alloc>& x
		 * \param const RBTree<Key,Value,KeyOfValue,Compare,Alloc>& y
		*/
		template <typename Key,typename Value,typename KeyOfValue,typename Compare,typename Alloc>
		inline bool operator<(const RBTree<Key, Value, KeyOfValue, Compare, Alloc>& x,const RBTree<Key, Value, KeyOfValue, Compare, Alloc>& y)
		{
			return std::lexicographical_compare(x.begin(), x.end(),y.begin(), y.end());
		}

		/*!
		 * \remarks 判断x != y
		 * \return 
		 * \param const RBTree<Key,Value,KeyOfValue,Compare,Alloc>& x
		 * \param const RBTree<Key,Value,KeyOfValue,Compare,Alloc>& y
		*/
		template <typename Key,typename Value,typename KeyOfValue,typename Compare,typename Alloc>
		inline bool operator!=(const RBTree<Key, Value, KeyOfValue, Compare, Alloc>& x,const RBTree<Key, Value, KeyOfValue, Compare, Alloc>& y)
		{
			return !(x == y);
		}

		/*!
		 * \remarks 判断x > y
		 * \return 
		 * \param const RBTree<Key,Value,KeyOfValue,Compare,Alloc>& x
		 * \param const RBTree<Key,Value,KeyOfValue,Compare,Alloc>& y
		*/
		template <typename Key,typename Value,typename KeyOfValue,typename Compare,typename Alloc>
		inline bool operator>(const RBTree<Key, Value, KeyOfValue, Compare, Alloc>& x,const RBTree<Key, Value, KeyOfValue, Compare, Alloc>& y)
		{
			return y < x;
		}

		/*!
		 * \remarks 判断x <= y
		 * \return 
		 * \param const RBTree<Key,Value,KeyOfValue,Compare,Alloc>& x
		 * \param const RBTree<Key,Value,KeyOfValue,Compare,Alloc>& y
		*/
		template <typename Key,typename Value,typename KeyOfValue,typename Compare,typename Alloc>
		inline bool operator<=(const RBTree<Key, Value, KeyOfValue, Compare, Alloc>& x,const RBTree<Key, Value, KeyOfValue, Compare, Alloc>& y)
		{
			return !(y < x);
		}

		/*!
		 * \remarks 判断x >= y
		 * \return 
		 * \param const RBTree<Key,Value,KeyOfValue,Compare,Alloc>& x
		 * \param const RBTree<Key,Value,KeyOfValue,Compare,Alloc>& y
		*/
		template <typename Key,typename Value,typename KeyOfValue,typename Compare,typename Alloc>
		inline bool operator>=(const RBTree<Key, Value, KeyOfValue, Compare, Alloc>& x,const RBTree<Key, Value, KeyOfValue, Compare, Alloc>& y)
		{
			return !(x < y);
		}

		/*!
		 * \remarks 交换x和y
		 * \return 
		 * \param RBTree<Key,Value,KeyOfValue,Compare,Alloc>& x
		 * \param RBTree<Key,Value,KeyOfValue,Compare,Alloc>& y
		*/
		template <typename Key,typename Value,typename KeyOfValue,typename Compare,typename Alloc>
		inline void swap(RBTree<Key, Value, KeyOfValue, Compare, Alloc>& x,RBTree<Key, Value, KeyOfValue, Compare, Alloc>& y)
		{
			x.swap(y);
		}
	}//namespace utl
}//namespace ung

#endif//_UNG_UTL_RBTREE_H_