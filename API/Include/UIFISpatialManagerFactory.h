/*!
 * \brief
 * 空间管理器工厂接口
 * \file UIFISpatialManagerFactory.h
 *
 * \author Su Yang
 *
 * \date 2017/03/27
 */
#ifndef _UIF_ISPATIALMANAGERFACTORY_H_
#define _UIF_ISPATIALMANAGERFACTORY_H_

#ifndef _UIF_CONFIG_H_
#include "UIFConfig.h"
#endif

#ifdef UIF_HIERARCHICAL_COMPILE

namespace ung
{
	class ISpatialManager;

	/*!
	 * \brief
	 * 空间管理器工厂接口
	 * \class ISpatialManagerFactory
	 *
	 * \author Su Yang
	 *
	 * \date 2017/03/27
	 *
	 * \todo
	 */
	class UngExport ISpatialManagerFactory : public MemoryPool<ISpatialManagerFactory>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		ISpatialManagerFactory() = default;

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~ISpatialManagerFactory() = default;

		/*!
		 * \remarks 获取工厂的标识
		 * \return 
		*/
		virtual String const& getIdentity() const = 0;

		/*!
		 * \remarks 获取工厂的标识
		 * \return 
		*/
		virtual SceneTypeMask getMask() const = 0;

		/*!
		 * \remarks 创建一个空间管理器
		 * \return 
		 * \param String const & instanceName 空间管理器的名字
		*/
		virtual ISpatialManager* createInstance(String const& instanceName) = 0;

		/*!
		 * \remarks 销毁一个空间管理器
		 * \return 
		 * \param ISpatialManager * instancePtr 指向空间管理器的指针
		*/
		virtual void destroyInstance(ISpatialManager* instancePtr) = 0;
	};
}//namespace ung

#endif//UIF_HIERARCHICAL_COMPILE

#endif//_UIF_ISPATIALMANAGERFACTORY_H_