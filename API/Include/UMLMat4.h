/*!
 * \brief
 * 4x4矩阵(row_major)
 * \file UMLMat4.h
 *
 * \author Su Yang
 *
 * \date 2016/06/15
 */
#ifndef _UML_MAT4_H_
#define _UML_MAT4_H_

#ifndef _UML_CONFIG_H_
#include "UMLConfig.h"
#endif

#ifdef UML_HIERARCHICAL_COMPILE

#ifndef _UML_VEC3_H_
#include "UMLVec3.h"
#endif

#ifndef _UML_VEC4_H_
#include "UMLVec4.h"
#endif

#ifndef _UML_QUATER_H_
#include "UMLQuater.h"
#endif

#ifndef _UML_MAT3_H_
#include "UMLMat3.h"
#endif

#ifndef _INCLUDE_BOOST_CONCEPTCHECK_HPP_
#include "boost/concept_check.hpp"
#define _INCLUDE_BOOST_CONCEPTCHECK_HPP_
#endif

#pragma warning(push)

 //从“double”转换到“float”
#pragma warning(disable : 4244)

namespace ung
{
	/*!
	 * \brief
	 * 4x4矩阵(row_major)
	 * \class Mat4
	 *
	 * \author Su Yang
	 *
	 * \date 2016/06/15
	 *
	 * \todo
	 */
	template<typename T>
	class Mat4 : public MemoryPool<Mat4<T>>
	{
	public:
		using element_type = T;
		using math_type = MathImpl<element_type>;

		/*!
		 * \remarks 打印Mat4
		 * \return 
		 * \param std::ostream & o
		 * \param const Mat4<T> & mat
		*/
		friend std::ostream& operator << (std::ostream& o, const Mat4<T>& mat)
		{
			o << "Mat4(";
			for (uintfast8 i = 0; i < 4; ++i)
			{
				o << " row" << (unsigned)i << "{";
				for (uintfast8 j = 0; j < 4; ++j)
				{
					o << mat.m[i][j] << " ";
				}
				o << "}";
			}
			o << ")";
			return o;
		}

	public:
		/*!
		 * \remarks 默认构造函数(默认情况下，构造一个单位矩阵)
		 * \return 
		*/
		Mat4()
		{
			for (auto &row : m)
			{
				for (auto &e : row)
				{
					e = 0.0;
				}
			}
			//单位矩阵
			m[0][0] = m[1][1] = m[2][2] = m[3][3] = 1.0;
		}

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~Mat4() = default;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param T m00
		 * \param T m01
		 * \param T m02
		 * \param T m03
		 * \param T m10
		 * \param T m11
		 * \param T m12
		 * \param T m13
		 * \param T m20
		 * \param T m21
		 * \param T m22
		 * \param T m23
		 * \param T m30
		 * \param T m31
		 * \param T m32
		 * \param T m33
		*/
		Mat4(T m00, T m01, T m02, T m03,
			T m10, T m11, T m12, T m13,
			T m20, T m21, T m22, T m23,
			T m30, T m31, T m32, T m33)
		{
			m[0][0] = m00;
			m[0][1] = m01;
			m[0][2] = m02;
			m[0][3] = m03;
			m[1][0] = m10;
			m[1][1] = m11;
			m[1][2] = m12;
			m[1][3] = m13;
			m[2][0] = m20;
			m[2][1] = m21;
			m[2][2] = m22;
			m[2][3] = m23;
			m[3][0] = m30;
			m[3][1] = m31;
			m[3][2] = m32;
			m[3][3] = m33;
		}

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param const T(* const ar)[4] 数组
		 * \param const uint32 rowSize
		*/
		Mat4(const T(*const ar)[4], const uint32 rowSize)
		{
			BOOST_ASSERT(rowSize == 4);
			memcpy(m, ar, 16 * sizeof(T));
		}

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param const T * const ar 指针
		 * \param const uint32 sz
		*/
		Mat4(const T *const ar, const uint32 sz)
		{
			BOOST_ASSERT(sz == 16);
			memcpy(m, ar, 16 * sizeof(T));
		}

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param const T(& ar)[16] 数组
		*/
		explicit Mat4(const T(&ar)[16])
		{
			memcpy(m, ar, 16 * sizeof(T));
		}

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param const T(& ar)[4][4] 二维数组
		*/
		explicit Mat4(const T(&ar)[4][4])
		{
			memcpy(m, ar, 16 * sizeof(T));
		}

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param Vec4<T> const & row0
		 * \param Vec4<T> const & row1
		 * \param Vec4<T> const & row2
		 * \param Vec4<T> const & row3
		*/
		Mat4(Vec4<T> const& row0,Vec4<T> const& row1,Vec4<T> const& row2,Vec4<T> const& row3)
		{
			m[0][0] = row0.x;
			m[0][1] = row0.y;
			m[0][2] = row0.z;
			m[0][3] = row0.w;

			m[1][0] = row1.x;
			m[1][1] = row1.y;
			m[1][2] = row1.z;
			m[1][3] = row1.w;

			m[2][0] = row2.x;
			m[2][1] = row2.y;
			m[2][2] = row2.z;
			m[2][3] = row2.w;

			m[3][0] = row3.x;
			m[3][1] = row3.y;
			m[3][2] = row3.z;
			m[3][3] = row3.w;
		}

		/*!
		 * \remarks 拷贝构造函数
		 * \return 
		 * \param const Mat4 & other
		*/
		Mat4(const Mat4& other)
		{
			memcpy(m, other.m, 16 * sizeof(T));
		}

		/*!
		 * \remarks 拷贝赋值运算符
		 * \return 
		 * \param const Mat4 & other
		*/
		Mat4& operator=(const Mat4& other)
		{
			if (this != &other)
			{
				memcpy(m, other.m, 16 * sizeof(T));
			}

			return *this;
		}

		/*!
		 * \remarks 移动构造函数
		 * \return 
		 * \param Mat4&& other
		*/
		Mat4(Mat4&& other) noexcept
		{
			for (uintfast8 r = 0; r < 4; ++r)
			{
				for (uintfast8 c = 0; c < 4; ++c)
				{
					m[r][c] = other.m[r][c];
				}
			}
		}

		/*!
		 * \remarks 移动赋值运算符
		 * \return 
		 * \param Mat4 & & other
		*/
		Mat4& operator=(Mat4&& other) noexcept
		{
			if (this != &other)
			{
				for (uintfast8 r = 0; r < 4; ++r)
				{
					for (uintfast8 c = 0; c < 4; ++c)
					{
						m[r][c] = other.m[r][c];
					}
				}
			}

			return *this;
		}

		/*!
		 * \remarks 移动构造函数
		 * \return 
		 * \param Mat4<U>&& moveInst
		*/
		template<typename U>
		Mat4(Mat4<U>&& moveInst) noexcept
		{
			//BOOST_CONCEPT_ASSERT((boost::Convertible<U,T>));

			for (uintfast8 r = 0; r < 4; ++r)
			{
				for (uintfast8 c = 0; c < 4; ++c)
				{
					m[r][c] = moveInst.m[r][c];

					moveInst.m[r][c] = 0.0;
				}
			}
		}

		/*!
		 * \remarks 移动赋值运算符
		 * \return 
		 * \param Mat4&& moveInst
		*/
		template<typename U>
		Mat4& operator=(Mat4&& moveInst) noexcept
		{
			//BOOST_CONCEPT_ASSERT((boost::Convertible<U,T>));

			if (this != &other)
			{
				for (uintfast8 r = 0; r < 4; ++r)
				{
					for (uintfast8 c = 0; c < 4; ++c)
					{
						m[r][c] = moveInst.m[r][c];

						moveInst.m[r][c] = 0.0;
					}
				}
			}

			return *this;
		}

		/*!
		 * \remarks 构造函数，从一个3x3的旋转或者缩放矩阵来构建一个没有平移的4x4矩阵.
		 * \return 
		 * \param const Mat3<T> & m3x3
		*/
		Mat4(const Mat3<T>& m3x3)
		{
			/*
			任意3x3变换矩阵在4D中表示为:
			m11 m12 m13       m11 m12 m13 0
			m21 m22 m23 => m21 m22 m23 0
			m31 m32 m33       m31 m32 m33 0
										0      0      0   1
			*/

			operator=(IDENTITY);
			operator=(m3x3);
		}

		/*!
		 * \remarks 构造函数，从一个表示旋转的四元数来构建一个没有平移的4x4矩阵
		 * \return 
		 * \param const Quater<T> & q
		*/
		Mat4(const Quater<T>& q)
		{
			operator=(IDENTITY);
			setRotateFromQuaternion(q);
		}

		/*!
		 * \remarks 获取某一行
		 * \return 返回的是副本，不是引用
		 * \param uint32 iRow [0,3]
		*/
		Vec4<T> operator[](uint32 iRow)
		{
			BOOST_ASSERT(iRow >= 0 && iRow <= 3);

			return Vec4<T>(m[iRow]);
		}

		/*!
		 * \remarks 获取某一行
		 * \return 返回的是副本，不是引用
		 * \param uint32 iRow [0,3]
		*/
		const Vec4<T> operator[](uint32 iRow) const
		{
			BOOST_ASSERT(iRow >= 0 && iRow <= 3);

			return Vec4<T>(m[iRow]);
		}

		/*!
		 * \remarks 重载()
		 * \return 拷贝
		 * \param usize row
		 * \param usize col
		*/
		T operator()(usize row, usize col) const
		{
			BOOST_ASSERT(row >= 0 && row <= 3);
			BOOST_ASSERT(col >= 0 && col <= 3);

			return m[row][col];
		}

		/*!
		 * \remarks 重载()
		 * \return 引用
		 * \param usize row
		 * \param usize col
		*/
		T& operator()(usize row, usize col)
		{
			BOOST_ASSERT(row >= 0 && row <= 3);
			BOOST_ASSERT(col >= 0 && col <= 3);

			return m[row][col];
		}

		/*!
		 * \remarks 获取参数所指定的元素
		 * \return 引用(注意：auto m00 = m3.getElement(0, 0);auto推断出来的m00的类型是T，而不是T&，必须写成auto&)
		 * \param usize r
		 * \param usize c
		*/
		T& getElement(usize r, usize c)
		{
			BOOST_ASSERT(r >= 0 && r < 4);
			BOOST_ASSERT(c >= 0 && c < 4);

			return m[r][c];
		}

		/*!
		 * \remarks 获取参数所指定的元素
		 * \return 常量引用
		 * \param usize r
		 * \param usize c
		*/
		T const& getElement(usize r, usize c) const
		{
			BOOST_ASSERT(r >= 0 && r < 4);
			BOOST_ASSERT(c >= 0 && c < 4);

			return m[r][c];
		}

		/*!
		 * \remarks 获取某一行
		 * \return 返回的是副本，不是引用
		 * \param const usize iRow
		*/
		Vec4<T> getRow(const usize iRow) const
		{
			BOOST_ASSERT(iRow < 4);
			return Vec4<T>(m[iRow][0], m[iRow][1], m[iRow][2], m[iRow][3]);
		}

		/*!
		 * \remarks 获取某一列
		 * \return 返回的是副本，不是引用
		 * \param const usize iRow
		*/
		Vec4<T> getColumn(const usize iCol) const
		{
			BOOST_ASSERT(iCol < 4);
			return Vec4<T>(m[0][iCol], m[1][iCol], m[2][iCol],m[3][iCol]);
		}

		/*!
		 * \remarks 设置给定行
		 * \return 
		 * \param const usize iRow
		 * \param const Vec4<T> & vec
		*/
		void setRow(const usize iRow, const Vec4<T>& vec)
		{
			BOOST_ASSERT(iRow < 4);
			m[iRow][0] = vec.x;
			m[iRow][1] = vec.y;
			m[iRow][2] = vec.z;
			m[iRow][3] = vec.w;
		}

		/*!
		 * \remarks 设置给定列
		 * \return 
		 * \param const usize iCol
		 * \param const Vec4<T> & vec
		*/
		void setColumn(const usize iCol, const Vec4<T>& vec)
		{
			BOOST_ASSERT(iCol < 4);
			m[0][iCol] = vec.x;
			m[1][iCol] = vec.y;
			m[2][iCol] = vec.z;
			m[3][iCol] = vec.w;
		}

		/*!
		 * \remarks 获取00的地址
		 * \return 
		*/
		T* getAddress()
		{
			return &m[0][0];
		}

		/*!
		 * \remarks 获取00的地址
		 * \return 
		*/
		T const* getAddress() const
		{
			return &m[0][0];
		}

		/*!
		 * \remarks 交换
		 * \return 
		 * \param Mat4& other
		*/
		void swap(Mat4& other)
		{
			for (uintfast8 r = 0; r < 4; ++r)
			{
				for (uintfast8 c = 0; c < 4; ++c)
				{
					std::swap(m[r][c], other.m[r][c]);
				}
			}
		}

		/*!
		 * \remarks 判断当前矩阵是否等于给定矩阵
		 * \return 
		 * \param const Mat4 & other
		*/
		bool operator==(const Mat4& other) const
		{
			for (uintfast8 r = 0; r < 4; r++)
			{
				for (uintfast8 c = 0; c < 4; c++)
				{
					if (!(math_type::isEqual(m[r][c], other.m[r][c])))
					{
						return false;
					}
				}
			}

			return true;
		}

		/*!
		 * \remarks 判断当前矩阵是否不等于给定矩阵
		 * \return 
		 * \param const Mat4 & other
		*/
		bool operator!=(const Mat4& other) const
		{
			return !(operator==(other));
		}

		/*!
		 * \remarks 重载赋值运算符
		 * \return 
		 * \param const Mat3<T> & mat3
		*/
		void operator=(const Mat3<T>& mat3)
		{
			for (uintfast8 r = 0; r < 3; ++r)
			{
				for (uintfast8 c = 0; c < 3; ++c)
				{
					m[r][c] = mat3.m[r][c];
				}
			}
		}

		/*!
		 * \remarks 计算当前矩阵的转置矩阵，不改变当前矩阵
		 * \return 
		 * \param Mat4& outMat
		*/
		void transpose(Mat4& outMat) const
		{
			/*
			矩阵的转置（transpose）可通过交换矩阵的行和列来实现。
			*/

			for (uintfast8 r = 0; r < 4; ++r)
			{
				for (uintfast8 c = 0; c < 4; ++c)
				{
					outMat.m[r][c] = m[c][r];
				}
			}
		}

		/*!
		 * \remarks 当前矩阵的转置矩阵，改变了当前矩阵
		 * \return 
		*/
		void transpose()
		{
			using std::swap;

			swap(m[0][1], m[1][0]);
			swap(m[0][2], m[2][0]);
			swap(m[0][3], m[3][0]);

			swap(m[1][2], m[2][1]);
			swap(m[1][3], m[3][1]);

			swap(m[2][3], m[3][2]);
		}

		/*!
		 * \remarks 计算当前矩阵的delta值(行列式)
		 * \return 
		*/
		T determinant() const
		{
			//矩阵的转置矩阵，其行列式和原矩阵相等。

			return m[0][0] * detHelper(*this, 1, 2, 3, 1, 2, 3)
				- m[0][1] * detHelper(*this, 1, 2, 3, 0, 2, 3)
				+ m[0][2] * detHelper(*this, 1, 2, 3, 0, 1, 3)
				- m[0][3] * detHelper(*this, 1, 2, 3, 0, 1, 2);
		}

		/*!
		 * \remarks 计算当前矩阵的逆矩阵(不改变当前矩阵)
		 * 一个仿射变换是否是可逆的?
		 * 一个仿射变换就是线性变换加上平移,显然,我们可以用相反的量"撤销"平移部分,而线性变换中除了投影以外,其他变换都能"撤销".当物体
		 * 被投影时,某一维有用的信息被抛弃了,而这些信息是不可能恢复的.因此,所有基本变换除了投影都是可逆的.
		 * \return 要检查返回的bool值
		 * \param Mat4& outerMat
		*/
		bool inverse(Mat4& outerMat) const
		{
			/*
			只有方阵才可能有逆矩阵，并非所有的方阵都有逆矩阵。
			一个矩阵与其逆矩阵的乘积为单位矩阵：M * M(-1) = M(-1) * M = I。
			逆矩阵在求矩阵方程中的其他矩阵时，非常有用。
			如果A和B均可逆的话，那么(AB)(-1) = B(-1) * A(-1)。
			*/

			T m00 = m[0][0], m01 = m[0][1], m02 = m[0][2], m03 = m[0][3];
			T m10 = m[1][0], m11 = m[1][1], m12 = m[1][2], m13 = m[1][3];
			T m20 = m[2][0], m21 = m[2][1], m22 = m[2][2], m23 = m[2][3];
			T m30 = m[3][0], m31 = m[3][1], m32 = m[3][2], m33 = m[3][3];

			T v0 = m20 * m31 - m21 * m30;
			T v1 = m20 * m32 - m22 * m30;
			T v2 = m20 * m33 - m23 * m30;
			T v3 = m21 * m32 - m22 * m31;
			T v4 = m21 * m33 - m23 * m31;
			T v5 = m22 * m33 - m23 * m32;

			T t00 = +(v5 * m11 - v4 * m12 + v3 * m13);
			T t10 = -(v5 * m10 - v2 * m12 + v1 * m13);
			T t20 = +(v4 * m10 - v2 * m11 + v0 * m13);
			T t30 = -(v3 * m10 - v1 * m11 + v0 * m12);

			T det = t00 * m00 + t10 * m01 + t20 * m02 + t30 * m03;

			/*
			上面计算出来的det等于determinant()的返回值。
			如果行列式为0.0的话，那么逆矩阵不存在(矩阵的行，线性相关)
			*/
			if (math_type::isZero(det))
			{
				return false;
			}

			T invDet = 1 / det;

			T d00 = t00 * invDet;
			T d10 = t10 * invDet;
			T d20 = t20 * invDet;
			T d30 = t30 * invDet;

			T d01 = -(v5 * m01 - v4 * m02 + v3 * m03) * invDet;
			T d11 = +(v5 * m00 - v2 * m02 + v1 * m03) * invDet;
			T d21 = -(v4 * m00 - v2 * m01 + v0 * m03) * invDet;
			T d31 = +(v3 * m00 - v1 * m01 + v0 * m02) * invDet;

			v0 = m10 * m31 - m11 * m30;
			v1 = m10 * m32 - m12 * m30;
			v2 = m10 * m33 - m13 * m30;
			v3 = m11 * m32 - m12 * m31;
			v4 = m11 * m33 - m13 * m31;
			v5 = m12 * m33 - m13 * m32;

			T d02 = +(v5 * m01 - v4 * m02 + v3 * m03) * invDet;
			T d12 = -(v5 * m00 - v2 * m02 + v1 * m03) * invDet;
			T d22 = +(v4 * m00 - v2 * m01 + v0 * m03) * invDet;
			T d32 = -(v3 * m00 - v1 * m01 + v0 * m02) * invDet;

			v0 = m21 * m10 - m20 * m11;
			v1 = m22 * m10 - m20 * m12;
			v2 = m23 * m10 - m20 * m13;
			v3 = m22 * m11 - m21 * m12;
			v4 = m23 * m11 - m21 * m13;
			v5 = m23 * m12 - m22 * m13;

			T d03 = -(v5 * m01 - v4 * m02 + v3 * m03) * invDet;
			T d13 = +(v5 * m00 - v2 * m02 + v1 * m03) * invDet;
			T d23 = -(v4 * m00 - v2 * m01 + v0 * m03) * invDet;
			T d33 = +(v3 * m00 - v1 * m01 + v0 * m02) * invDet;

			outerMat = Mat4(
				d00, d01, d02, d03,
				d10, d11, d12, d13,
				d20, d21, d22, d23,
				d30, d31, d32, d33);

			return true;
		}

		/*!
		 * \remarks 设定当前矩阵的缩放因子
		 * set的意思是对当前矩阵的数据进行覆写
		 * \return 
		 * \param const Vec3<T> & v
		*/
		void setScale(const Vec3<T>& v)
		{
			m[0][0] = v.x;
			m[1][1] = v.y;
			m[2][2] = v.z;
		}

		/*!
		 * \remarks 设定当前矩阵的缩放因子
		 * \return 
		 * \param T sx
		 * \param T sy
		 * \param T sz
		*/
		void setScale(T sx,T sy,T sz)
		{
			m[0][0] = sx;
			m[1][1] = sy;
			m[2][2] = sz;
		}

		/*!
		 * \remarks 设置统一缩放因子
		 * \return 
		 * \param T s
		*/
		void setScale(T s)
		{
			m[0][0] = s;
			m[1][1] = s;
			m[2][2] = s;
		}

		/*!
		 * \remarks 获取当前矩阵的缩放因子
		 * \return 
		 * \param Vec3<T>& outVec3
		*/
		void getScale(Vec3<T>& outVec3) const
		{
			outVec3.x = m[0][0];
			outVec3.y = m[1][1];
			outVec3.z = m[2][2];
		}

		/*!
		 * \remarks 通过一个缩放因子来构造一个包含缩放效果的4x4矩阵
		 * \return 
		 * \param const Vec3<T> & v
		*/
		static Mat4 makeScale(const Vec3<T>& v)
		{
			Mat4 outMat{};
			outMat.m[0][0] = v.x;
			outMat.m[1][1] = v.y;
			outMat.m[2][2] = v.z;

			return std::move(outMat);
		}

		/*!
		 * \remarks 通过一个缩放因子来构造一个包含缩放效果的4x4矩阵
		 * \return 
		 * \param T sx
		 * \param T sy
		 * \param T sz
		*/
		static Mat4 makeScale(T sx, T sy, T sz)
		{
			Mat4 outMat{};
			outMat.m[0][0] = sx;
			outMat.m[1][1] = sy;
			outMat.m[2][2] = sz;

			return std::move(outMat);
		}

		/*!
		 * \remarks 绕x轴旋转r弧度.
		 * \return 
		 * \param Radian r
		*/
		void setRotateX(Radian r)
		{
			//获取当前矩阵的平移因子
			Vec3<T> trans{};
			getTrans(trans);

			//构造一个绕x轴旋转r弧度的Matrix3,然后赋值给当前矩阵
			*this = Mat3<T>::makeRotateX(r);

			//恢复当前矩阵的平移因子
			setTrans(trans);
		}

		/*!
		 * \remarks 绕x轴旋转d度.
		 * \return 
		 * \param Degree d
		*/
		void setRotateX(Degree d)
		{
			setRotateX(d.toRadian());
		}

		/*!
		 * \remarks 通过一个旋转因子来构造一个包含绕x轴旋转r弧度效果的4x4矩阵
		 * \return 
		 * \param Radian r 弧度
		*/
		static Mat4 makeRotateX(Radian r)
		{
			Mat4 outMat{};
			outMat.setRotateX(r);

			return std::move(outMat);
		}

		/*!
		 * \remarks 通过一个旋转因子来构造一个包含绕x轴旋转d度效果的4x4矩阵
		 * \return 
		 * \param Degree d 度
		*/
		static Mat4 makeRotateX(Degree d)
		{
			Mat4 outMat{};
			outMat.setRotateX(d);

			return std::move(outMat);
		}

		/*!
		 * \remarks 绕y轴旋转r弧度.
		 * \return 
		 * \param Radian r
		*/
		void setRotateY(Radian r)
		{
			//获取当前矩阵的平移因子
			Vec3<T> trans{};
			getTrans(trans);

			//构造一个绕y轴旋转r弧度的Matrix3,然后赋值给当前矩阵
			*this = Mat3<T>::makeRotateY(r);

			//恢复当前矩阵的平移因子
			setTrans(trans);
		}

		/*!
		 * \remarks 绕y轴旋转d度.
		 * \return 
		 * \param Degree d
		*/
		void setRotateY(Degree d)
		{
			setRotateY(d.toRadian());
		}

		/*!
		 * \remarks 通过一个旋转因子来构造一个包含绕y轴旋转r弧度效果的4x4矩阵
		 * \return 
		 * \param Radian r
		*/
		static Mat4 makeRotateY(Radian r)
		{
			Mat4 outMat{};
			outMat.setRotateY(r);

			return std::move(outMat);
		}

		/*!
		 * \remarks 通过一个旋转因子来构造一个包含绕y轴旋转d度效果的4x4矩阵
		 * \return 
		 * \param Degree d
		*/
		static Mat4 makeRotateY(Degree d)
		{
			Mat4 outMat{};
			outMat.setRotateY(d);

			return std::move(outMat);
		}

		/*!
		 * \remarks 绕z轴旋转r弧度.
		 * \return 
		 * \param Radian r
		*/
		void setRotateZ(Radian r)
		{
			//获取当前矩阵的平移因子
			Vec3<T> trans{};
			getTrans(trans);

			//构造一个绕z轴旋转r弧度的Matrix3,然后赋值给当前矩阵
			*this = Mat3<T>::makeRotateZ(r);

			//恢复当前矩阵的平移因子
			setTrans(trans);
		}

		/*!
		 * \remarks 绕z轴旋转d度.
		 * \return 
		 * \param Degree d
		*/
		void setRotateZ(Degree d)
		{
			setRotateZ(d.toRadian());
		}

		/*!
		 * \remarks 通过一个旋转因子来构造一个包含绕z轴旋转r弧度效果的4x4矩阵
		 * \return 
		 * \param Radian r
		*/
		static Mat4 makeRotateZ(Radian r)
		{
			Mat4 outMat{};
			outMat.setRotateZ(r);

			return std::move(outMat);
		}

		/*!
		 * \remarks 通过一个旋转因子来构造一个包含绕z轴旋转d度效果的4x4矩阵
		 * \return 
		 * \param Degree d
		*/
		static Mat4 makeRotateZ(Degree d)
		{
			Mat4 outMat{};
			outMat.setRotateZ(d);

			return std::move(outMat);
		}

		/*!
		 * \remarks Axis-Angle to Matrix(绕任意轴旋转r弧度).
		 * \return 
		 * \param const Vec3<T> & n
		 * \param Radian r
		*/
		void setRotateAnyAxis(const Vec3<T>& n, Radian r)
		{
			//获取当前矩阵的平移因子
			Vec3<T> trans{};
			getTrans(trans);

			//构造一个绕任意轴旋转r弧度的Matrix3,然后赋值给当前矩阵
			*this = Mat3<T>::makeRotateAnyAxis(n,r);

			//恢复当前矩阵的平移因子
			setTrans(trans);
		}

		/*!
		 * \remarks Axis-Angle to Matrix(绕任意轴旋转d度).
		 * \return 
		 * \param const Vec3<T>& n
		 * \param Degree d
		*/
		void setRotateAnyAxis(const Vec3<T>& n, Degree d)
		{
			setRotateAnyAxis(n,d.toRadian());
		}

		/*!
		 * \remarks Axis-Angle to Matrix(绕任意轴旋转r弧度).
		 * \return 
		 * \param const Vec3<T> & n
		 * \param Radian r
		*/
		static Mat4 makeRotateAnyAxis(const Vec3<T>& n, Radian r)
		{
			Mat4 outMat{};
			outMat.setRotateAnyAxis(n, r);

			return std::move(outMat);
		}

		/*!
		 * \remarks Axis-Angle to Matrix(绕任意轴旋转d度).
		 * \return 
		 * \param const Vec3<T> & n
		 * \param Degree d
		*/
		static Mat4 makeRotateAnyAxis(const Vec3<T>& n, Degree d)
		{
			Mat4 outMat{};
			outMat.setRotateAnyAxis(n,d);

			return std::move(outMat);
		}

		/*!
		 * \remarks Quater to Matrix
		 * \return 
		 * \param const Quater<T> & q
		*/
		void setRotateFromQuaternion(const Quater<T>& q)
		{
			//表示旋转的四元数必须是单位四元数
			BOOST_ASSERT(q.isUnit());

			//当前矩阵的平移因子不变

			Mat3<T> m3{};
			m3.setRotateFromQuaternion(q);
			for (uintfast8 r = 0; r < 3; ++r)
			{
				for (uintfast8 c = 0; c < 3; ++c)
				{
					m[r][c] = m3[r][c];
				}
			}
		}

		/*!
		 * \remarks Quater to Matrix
		 * \return 
		 * \param const Quater<T> & q
		*/
		static Mat4 makeRotateFromQuaternion(const Quater<T>& q)
		{
			Mat4 outMat{};
			outMat.setRotateFromQuaternion(q);

			return std::move(outMat);
		}

		/*!
		 * \remarks 设置矩阵的平移变换.
		 * \return 
		 * \param const Vec3<T> & v
		*/
		void setTrans(const Vec3<T>& v)
		{
			m[3][0] = v.x;
			m[3][1] = v.y;
			m[3][2] = v.z;
		}

		/*!
		 * \remarks 设置矩阵的平移变换.
		 * \return 
		 * \param T tx
		 * \param T ty
		 * \param T tz
		*/
		void setTrans(T tx, T ty, T tz)
		{
			m[3][0] = tx;
			m[3][1] = ty;
			m[3][2] = tz;
		}

		/*!
		 * \remarks 获取矩阵的平移因子
		 * \return 
		 * \param Vec3<T> & outVec3
		*/
		void getTrans(Vec3<T>& outVec3) const
		{
			outVec3.x = m[3][0];
			outVec3.y = m[3][1];
			outVec3.z = m[3][2];
		}

		Vec3<T> getTrans() const
		{
			return Vec3<T>(m[3][0], m[3][1], m[3][2]);
		}

		/*!
		 * \remarks 通过给定的平移因子来构造一个包含平移效果的4x4矩阵
		 * \return 
		 * \param const Vec3<T>& v
		*/
		static Mat4 makeTrans(const Vec3<T>& v)
		{
			//默认构造函数会构造出一个单位矩阵，所以这里无需再设置outMat的其它项。
			Mat4 outMat{};
			outMat.m[3][0] = v.x;
			outMat.m[3][1] = v.y;
			outMat.m[3][2] = v.z;

			return std::move(outMat);
		}

		/*!
		 * \remarks 通过给定的平移因子来构造一个包含平移效果的4x4矩阵
		 * \return 
		 * \param T tx
		 * \param T ty
		 * \param T tz
		*/
		static Mat4 makeTrans(T tx, T ty, T tz)
		{
			Mat4 outMat{};
			outMat.m[3][0] = tx;
			outMat.m[3][1] = ty;
			outMat.m[3][2] = tz;

			return std::move(outMat);
		}

		/*!
		 * \remarks 当前矩阵代表一个仿射变换，从仿射变换中抽取出线性变换
		 * \return 
		*/
		Mat3<T> extract3x3Matrix() const
		{
			Mat3<T> m3x3{};

			for (uintfast8 r = 0; r < 3; ++r)
			{
				for (uintfast8 c = 0; c < 3; ++c)
				{
					m3x3.m[r][c] = m[r][c];
				}
			}

			return m3x3;
		}

		/*!
		 * \remarks 判断当前矩阵是否是一个仿射矩阵
		 * An affine matrix is a 4x4 matrix with row 3 equal to (0,0,0,1),no projective coefficients.
		 * \return 
		 * \param void
		*/
		bool isAffine(void) const
		{
			return (math_type::isZero(m[3][0]) && math_type::isZero(m[3][1]) && math_type::isZero(m[3][2]) && math_type::isEqual(m[3][3], 1.0));
		}

		/*!
		 * \remarks 获取当前矩阵的逆转置矩阵（先计算逆矩阵，然后把计算得来的逆矩阵进行转置），不改变当前矩阵
		 * \return 
		*/
		bool getInverseTranspose(Mat4& outMat) const
		{
			Mat4 invMat;
			bool ret = inverse(invMat);
			
			//如果当前矩阵没有逆矩阵
			if (!ret)
			{
				return false;
			}

			invMat.transpose(outMat);

			return true;
		}

		/*!
		 * \remarks 两个矩阵相乘(没有改变当前矩阵的数据)
		 * \return 
		 * \param const Mat4 & other
		*/
		Mat4 operator*(const Mat4 &other) const
		{
			/*
			借助矩阵乘法，我们能够对向量实施变换，也可将几个变换进行组合。
			为了计算矩阵乘积AB，矩阵A的列数必须等于矩阵B的行数。
			矩阵乘法一般不具有交换性，即AB != BA。
			C = AB，其中C的第ij个元素的值等于A的第i个行向量与B的第j个列向量的点积。
			*/

			Mat4 ret;

			for (uintfast8 r = 0; r < 4; ++r)
			{
				for (uintfast8 c = 0; c < 4; ++c)
				{
					ret.m[r][c] = m[r][0] * other.m[0][c] + m[r][1] * other.m[1][c] + m[r][2] * other.m[2][c] + m[r][3] * other.m[3][c];
				}
			}

			return ret;
		}

		/*!
		 * \remarks Mat4 *= Mat4
		 * \return 
		 * \param Mat4 const & other
		*/
		Mat4& operator*=(Mat4 const& other)
		{
			Mat4 temp = operator*(other);

			swap(temp);

			return *this;
		}

		/*
		The view matrix represents the camera's position/rotation in space, and
		where it's facing, while the projection matrix represents the camera's aspect ratio
		and bounds (also known as the camera's frustum), and how the scene is stretched/
		warped to give an appearance of depth (which we call perspective).
		*/
		/*!
		 * \remarks 计算view矩阵(摄像机的局部坐标系，其朝向为负z轴)
		 * \return 
		 * \param const Vec3<T> & eyeWorldPos The position of the camera in the world
		 * \param const Vec3<T> & targetWorldPos The point in the world at which you want to 
		 * aim the camera.
		 * \param const Vec3<T> & upDir Indicates which direction is "up" in the 3D world.This is 
		 * usually always the vector coincident with the y-axis - (0, 1, 0),but if for some reason 
		 * you want an upside-down camera,you can specify (0, -1, 0) instead.
		*/
		static Mat4 makeView(const Vec3<T>& eyeWorldPos, const Vec3<T>& targetWorldPos, const Vec3<T>& upDir = Vec3<T>(0,1,0))
		{
			/*
			三个参数只是形象地描述了摄像的局部坐标系与世界坐标系的相对关系。
			也就是说，摄像机的局部坐标系原点，以及三个基向量它们用世界坐标系来表述的时候，所需要的数
			值。
			Given the position of the camera in world space,its target point in world space,and the 
			world up vector,we have the origin of the camera and can derive its axis vectors 
			relative to the world space.
			*/

			/*
			任何相机变换都是平移加旋转。
			把相机坐标系通过平移和旋转，给变换到和世界坐标系重合，那么此时世界空间中的点的值，也就是其位于相机空间中的值。

			我们使用右手坐标系，使用行为主的矩阵。
			那么，因为一个坐标系能用任意3个线性无关（也就是不再同一平面上）的基向量来定义。
			所以，相机空间就可以表示为：
			U.x		U.y		U.z
			V.x		V.y		V.z
			N.x		N.y		N.z
			上面的矩阵，其中每一行都是相机坐标系的一个基向量（用世界坐标系所描述的基向量）用这3个基向量所组成的矩阵，可以把相机空间的点给变换到描述相机
			坐标系的基向量的世界坐标系中，那么相反，要把世界坐标系的点给变换到相机空间，就需要上面这个矩阵的逆矩阵。
			因为描述相机空间的3个基向量是被世界坐标系所描述的，所以可以认为世界空间是相机空间的父空间。
			因为u,v和n都是单位向量，并且两两互相垂直，所以它的逆矩阵就是它的转置矩阵。
			*/

			Mat4 outMat{};

			//UVN相机模型，其中U,V,N必须是单位向量。
			//右向量
			Vec3<T> U;
			//上向量
			Vec3<T> V = upDir;
			//从目标位置指向相机位置，（之所以是从目标指向相机，是因为我们使用的是右手坐标系），视见方向为-N
			Vec3<T> N;

			auto sLen = V.squaredLength();
			if (!math_type::isEqual(sLen, 1.0))
			{
				V.normalize();
			}

			N = eyeWorldPos - targetWorldPos;			//右手坐标系
			//N = targetWorldPos - eyeWorldPos;		//左手坐标系，已通过D3DXMatrixLookAtLH验证
			N.normalize();

			U = V.crossProductUnit(N);
			U.normalize();

			//叉积a x b结果向量的长度，等于|a| * |b| * sinθ，也等于以a和b为两边的平行四边形的面积，所以这里不需要再进行normalize了
			V = N.crossProductUnit(U);

			BOOST_ASSERT(math_type::isEqual(U.dotProduct(V), 0.0));
			BOOST_ASSERT(math_type::isEqual(U.dotProduct(N), 0.0));
			BOOST_ASSERT(math_type::isEqual(N.dotProduct(V), 0.0));

			/*
			要将世界坐标系中的点p(x,y,z)变换为UVN系统中的点，只需分别计算p(x,y,z)与u,v和n的点积即可。
			换句话说，需要将x乘以（u轴和世界坐标系x轴之间的共线程度）...，而点积的结果描述了两个向量的“相似”程度，值越大，两个向量越相近。
			下面这个矩阵的列向量为u,v和n。
			*/
			outMat.m[0][0] = U.x;
			outMat.m[1][0] = U.y;
			outMat.m[2][0] = U.z;

			outMat.m[0][1] = V.x;
			outMat.m[1][1] = V.y;
			outMat.m[2][1] = V.z;

			outMat.m[0][2] = N.x;
			outMat.m[1][2] = N.y;
			outMat.m[2][2] = N.z;

			/*
			先平移，再旋转。因为相机坐标系是在世界空间描述的，所以其旋转需要先平移到世界空间的原点，否则旋转不是原地旋转而是会改变相机空间的位置。
			Mat4 t;
			Mat4::makeTrans(t,-eyeWorldPos.x,-eyeWorldPos.y,-eyeWorldPos.z);
			outMat = t * outMat;
			下面通过点积来计算最终的平移量，其结果等价于上式的矩阵乘法。
			注意：矩阵乘法的顺序。
			*/
			outMat.m[3][0] = -eyeWorldPos.dotProduct(U);
			outMat.m[3][1] = -eyeWorldPos.dotProduct(V);
			outMat.m[3][2] = -eyeWorldPos.dotProduct(N);

#if UNG_DEBUGMODE
			BOOST_ASSERT((Vec4<T>(U,0.0) * outMat) == Vec4<T>(1.0,0.0,0.0,0.0));
			BOOST_ASSERT((Vec4<T>(V,0.0) * outMat) == Vec4<T>(0.0,1.0,0.0,0.0));
			BOOST_ASSERT((Vec4<T>(N,0.0) * outMat) == Vec4<T>(0.0,0.0,1.0,0.0));
#endif

			/*
			令：
			p：用世界坐标系描述的摄像机的局部坐标系的原点
			r，u，f：用世界坐标系描述的摄像机的局部坐标系三个基向量
			也就是说，站在摄像机的局部坐标系来看，其原点为(0,0,0)，三个基向量分别为(1,0,0),(0,1,0)和
			(0,0,1)，但是站在世界坐标系来看，其原点和三个基向量分别为p,r,u,f。
			那么把摄像机从其局部空间给变换到世界空间的世界变换用W来表示：
			W：
			r.x				r.y				r.z				0
			u.x			u.y			u.z			0
			f.x				f.y				f.z				0
			p.x			p.y			p.z			1
			但是，这样的变换，会将摄像机放置在世界空间的任意位置和任意朝向，这种情况下，再去计算投影
			矩阵就比较繁琐。
			所以，干脆，令摄像机为整个场景的中心(Let the camera to be the center of the scene(i.e.,
			we describe the objects' vertices relative to the camera coordinate system).)。所以，实
			际上，我们需要的是上述世界变换W的一个逆变换，这样，我们就能把(已经)位于世界空间的顶点给
			变换到摄像机的局部空间里面去了。
			令W_-1为这个逆变换。
			上面的W实际上是一个旋转和平移矩阵的组合，也就是说W = RT
			令V为最终的view矩阵。
			那么：
			V = W_-1 = (RT)_-1 = T_-1 * R_-1 = T_-1 * R_t
			其中R_t为R的转置。
			那么：
			V = 
			1			0			0			0				r.x			u.x		f.x			0
			0			1			0			0		*		r.y			u.y		f.y			0
			0			0			1			0				r.z			u.z		f.z			0
			-p.x		-p.y		-p.z		1				0			0			0			1
			=
			r.x				u.x				f.x				0
			r.y				u.y				f.y				0
			r.z				u.z				f.z				0
			-(p · r)		-(p · u)			-(p · f)		1
			这样，计算出来的V矩阵，就可以把世界空间的顶点给变换到view空间(摄像机空间)里面去了。
			*/

			/*
			该函数(makeView())与下面的函数(D3DXMatrixLookAtRH())计算结果完全相同，使用相同的参
			数，同时也说明了说明D3DXMatrixLookAtRH()函数(右手)将摄像机移动到原点，且摄像机的朝向
			为-z轴(指向屏幕内)。
			这里没有用BOOST_ASSERT()去验证，是因为要包含头文件和连接一些库。在单元测试里面有验证。
			
			Builds a right-handed,look-at matrix.
			D3DXMATRIX* D3DXMatrixLookAtRH(D3DXMATRIX* pOut,const D3DXVECTOR3* pEye,const D3DXVECTOR3* pAt,const D3DXVECTOR3* pUp);
			This function uses the following formula to compute the returned matrix:
			zaxis = normal(Eye - At)
			xaxis = normal(cross(Up, zaxis))
			yaxis = cross(zaxis, xaxis)

			xaxis.x						yaxis.x						zaxis.x					0
			xaxis.y						yaxis.y						zaxis.y					0
			xaxis.z						yaxis.z						zaxis.z					0
			dot(xaxis, eye)			dot(yaxis, eye)			dot(zaxis, eye)		1
			*/

			return std::move(outMat);
		}

		/*!
		 * \remarks 计算投影矩阵(D3D)
		 * \return 
		 * \param T aspect 宽高比(Where w is the width of the view window and h is the height of 
		 * the view window (units in view space)).For consistency(一致性),we like the ratio of the 
		 * projection window dimensions to be the same as the ratio of the back buffer dimensions.
		 * \param const T yFov 弧度，垂直方向上的可见区域(field of view)
		 * \param T zNear 近剪裁面与原点之间的距离
		 * \param T zFar 远剪裁面与原点之间的距离
		*/
		static Mat4 makePerspectiveD3D(T aspect,const T yFov = Consts<T>::THIRDPI,T zNear = 1.0,T zFar = 1000.0)
		{
			/*
			Once all the vertices of the scene are in view space and lighting has been completed,a 
			projection transformation is applied.After projection,D3D expects the coordinates of 
			the vertices inside the frustum to be in so-called normalized device coordinates,where 
			the x- and y-coordinates are in the range [-1, 1],and the zcoordinates are in the 
			range [0, 1].
			The projection transform maps the frustum into a box.This may seem 
			counterintuitive(违反直觉的) because isn't the projection transformation supposed to 
			generate 2D projected points?Indeed it actually does:The transformed x- and 
			y-coordinates represent the 2D projected vertices,and the normalized z-coordinate is 
			just used for depth buffering.
			*/

			/*
			平截锥体的金字塔顶位于相机坐标系的原点，并采用相机坐标系的-z轴作为自身轴向。
			在构建视见转换操作过程中，仅定义了相机的外部参数，即EYE,AT和UP。而视锥体则掌控了相机的内部结构，该过程类似于相机的镜头选取方案。
			在顶点处理阶段，投影转换可视为最后一步操作，该操作将相机空间内的对象转换至剪裁空间内。随后，剪裁操作将在剪裁空间内完成。
			通过投影转换可将平截锥体转换为轴对齐的立方体视见体，其中，立方体的x,y值范围均为[-1,1]。
			在D3D中，z值范围为[-1,0]。
			在OpenGL中，z值范围为[-1,1]。
			投影转换并不生成2D图像，仅使得场景中的3D对象产生变形效果。
			视锥体中的投影线，相交于相机原点位置处。这里，原点通常称为投影中心（COP）。

			令A = cot(fovy * 0.5),cot:余切

			列为主，右手剪裁空间，z:[-1,0]
			A / aspect				0					0					0
			0							A					0					0
			0							0					f / (f - n)		nf / (f - n)
			0							0					-1					0

			通常情况下，投影转换顶点的w坐标为-n。(所以，投影矩阵的[3][2]值为-1，这样计算出来的w值，即为-z(右手空间中))
			投影转换后，顶点将进入硬件光栅化阶段。在光栅化阶段，剪裁空间使用左手规则。而从右手转换到左手需要做两件事情：
			1，调整顶点顺序。（光栅化自动调整）
			2，z逆置。
			z逆置，等价于逆转z轴，那么：
			z:由[-1,0]变为[0,1]
			x,y值范围均为[-1,1]。
			方法为，逆置上面矩阵的第3行(从1算)，给其添加一个负号，即为
			列为主，左手剪裁空间，z:由[-1,0]变为[0,1]
			A / aspect				0					0						0
			0							A					0						0
			0							0					-(f / (f - n))		-(nf / (f - n))
			0							0					-1						0
			*/

			/*
			D3D:左手剪裁空间，行为主，z:[0,1]
			A / aspect				0					0						0
			0							A					0						0
			0							0					-(f / (f - n))		-1
			0							0					-(nf / (f - n))		0
			这里第3列可用来控制顶点z的正负
			*/

			/*
			OpenGL:左手剪裁空间，行为主，z值范围为[-1,1]。
			A / aspect				0					0								0
			0							A					0								0
			0							0					-((f + n)/ (f - n)))		-1
			0							0					-(2nf / (f - n))			0
			*/

			//如果误写为度，则报错
			BOOST_ASSERT(yFov > 0.0 && yFov < Consts<T>::PI);

			Mat4 outMat{};

			T cotValue = math_type::calCot(Radian(yFov * 0.5));

			outMat.m[0][0] = cotValue / aspect;
			outMat.m[1][1] = cotValue;
			outMat.m[2][2] = zFar / (zNear - zFar);
			outMat.m[2][3] = -1;
			outMat.m[3][2] = zNear * zFar / (zNear - zFar);

			outMat.m[3][3] = 0;

			/*
			该函数(makePerspectiveD3D())与下面的函数(D3DXMatrixPerspectiveFovRH())计算结果完全
			相同，使用相同的参数。
			Builds a right-handed perspective projection matrix based on a field of view.
			D3DXMATRIX* D3DXMatrixPerspectiveFovRH(D3DXMATRIX* pOut,FLOAT fovy,FLOAT Aspect,FLOAT zn,FLOAT zf);
			This function computes the returned matrix as shown:
			xScale		0				0						0
			0				yScale		0						0
			0				0				zf/(zn-zf)			-1
			0				0				zn*zf/(zn-zf)		0
			where:
			yScale = cot(fovY/2)
			xScale = yScale / aspect ratio
			
			纵横比用于校正由方形的投影窗口到矩形的显示屏的映射而引发的畸变。
			*/

			return std::move(outMat);
		}

		/*!
		 * \remarks 计算投影矩阵(OpenGL)
		 * \return 
		 * \param T aspect 宽高比
		 * \param const T yFov 弧度，垂直方向上的可见区域
		 * \param T zNear 近剪裁面与原点之间的距离
		 * \param T zFar 远剪裁面与原点之间的距离
		*/
		static Mat4 makePerspectiveOpenGL(T aspect,const T yFov = Consts<T>::THIRDPI,T zNear = 1.0,T zFar = 1000.0)
		{
			//如果误写为度，则报错
			BOOST_ASSERT(yFov > 0.0 && yFov < Consts<T>::PI);

			Mat4 outMat{};

			T cotValue = math_type::calCot(Radian(yFov * 0.5));

			outMat.m[0][0] = cotValue / aspect;
			outMat.m[1][1] = cotValue;
			outMat.m[2][2] = (zNear + zFar) / (zNear - zFar);
			outMat.m[2][3] = -1;
			outMat.m[3][2] = 2.0 * zNear * zFar / (zNear - zFar);

			outMat.m[3][3] = 0;

			return std::move(outMat);
		}

	public:
		static const Mat4 ZERO;
		static const Mat4 ZEROAFFINE;
		static const Mat4 IDENTITY;

		T m[4][4];
	};

	//----------------------------------------------------------------------------------

	template<typename T>
	const Mat4<T> Mat4<T>::ZERO(
		0, 0, 0, 0,
		0, 0, 0, 0,
		0, 0, 0, 0,
		0, 0, 0, 0);

	template<typename T>
	const Mat4<T> Mat4<T>::ZEROAFFINE(
		0, 0, 0, 0,
		0, 0, 0, 0,
		0, 0, 0, 0,
		0, 0, 0, 1);

	/*
	单位矩阵是方阵（square matrix）。
	*/
	template<typename T>
	const Mat4<T> Mat4<T>::IDENTITY(
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1);

	template<typename T>
	T detHelper(const Mat4<T>& m, const usize r0, const usize r1, const usize r2,const usize c0, const usize c1, const usize c2)
	{
		return	m[r0][c0] * (m[r1][c1] * m[r2][c2] - m[r2][c1] * m[r1][c2]) -
					m[r0][c1] * (m[r1][c0] * m[r2][c2] - m[r2][c0] * m[r1][c2]) +
					m[r0][c2] * (m[r1][c0] * m[r2][c1] - m[r2][c0] * m[r1][c1]);
	}
}//namespace ung

#pragma warning(pop)

#endif//UML_HIERARCHICAL_COMPILE

#endif//_UML_MAT4_H_