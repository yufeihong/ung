/*!
 * \brief
 * 场景管理器接口。
 * \file UIFISceneManager.h
 *
 * \author Su Yang
 *
 * \date 2016/11/26
 */
#ifndef _UIF_ISCENEMANAGER_H_
#define _UIF_ISCENEMANAGER_H_

#ifndef _UIF_CONFIG_H_
#include "UIFConfig.h"
#endif

#ifdef UIF_HIERARCHICAL_COMPILE

namespace ung
{
	class ISpatialManager;
	class Ray;
	class Sphere;
	class AABB;
	class Camera;

	enum SceneType : uint8
	{
		ST_GENERIC = 1,
		ST_EXTERIOR = 2,
		ST_INTERIOR = 4
	};

	/*!
	 * \brief
	 * 场景管理器接口。
	 * 这里刻意不从Singleton继承，是因为具有同样的空间管理器的场景管理器可能同时有多于1个的存在。
	 * \class ISceneManager
	 *
	 * \author Su Yang
	 *
	 * \date 2016/11/26
	 *
	 * \todo
	 */
	class UngExport ISceneManager : public MemoryPool<ISceneManager>,public std::enable_shared_from_this<ISceneManager>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		ISceneManager() = default;

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~ISceneManager() = default;

		//-------------------------------------------------------------------------------------------

		/*!
		 * \remarks 获取场景名字
		 * \return 
		*/
		virtual String const& getName() const = 0;

		/*!
		 * \remarks 获取场景类型
		 * \return 
		*/
		virtual SceneType getSceneType() const = 0;

		/*!
		 * \remarks 获取场景图根节点
		 * \return 
		*/
		virtual StrongISceneNodePtr getSceneRootNode() const = 0;

		//-------------------------------------------------------------------------------------------

		/*!
		 * \remarks 创建场景对象
		 * \return 
		 * \param String const & objectDataFileFullName
		 * \param String const & objectName
		*/
		virtual StrongIObjectPtr createObject(String const& objectDataFileFullName, String const& objectName = EMPTY_STRING) = 0;

		/*!
		 * \remarks 销毁对象
		 * \return 
		 * \param String const & objectName
		 * \param bool remove true:从场景节点的容器中移除
		*/
		virtual void destroyObject(String const& objectName,bool remove = true) = 0;

		/*!
		 * \remarks 销毁对象
		 * \return 
		 * \param StrongIObjectPtr objectPtr
		 * \param bool remove true:从场景节点的容器中移除
		*/
		virtual void destroyObject(StrongIObjectPtr objectPtr,bool remove = true) = 0;

		/*!
		 * \remarks 获取一个对象
		 * \return 
		 * \param String const& objectName
		*/
		virtual StrongIObjectPtr getObject(String const& objectName) = 0;

		/*!
		 * \remarks 获取对象池中对象的数量
		 * \return 
		*/
		virtual uint32 getAllObjectCount() const = 0;

		/*!
		 * \remarks 获取场景节点池中场景节点的数量
		 * \return 
		*/
		virtual uint32 getAllSceneNodeCount() const = 0;

		//-------------------------------------------------------------------------------------------

		/*!
		 * \remarks 创建一个场景节点
		 * \return 
		 * \param WeakISceneNodePtr parentPtr
		 * \param String const & nodeName
		*/
		virtual StrongISceneNodePtr createSceneNode(WeakISceneNodePtr parentPtr,String const& nodeName = EMPTY_STRING) = 0;

		/*!
		 * \remarks 销毁一个场景节点(包括该节点的family节点，以及一切关联的对象)
		 * \return 
		 * \param String const & nodeName
		*/
		virtual void destroySceneNode(String const& nodeName) = 0;

		/*!
		 * \remarks 销毁一个场景节点(包括该节点的family节点，以及一切关联的对象)
		 * \return 
		 * \param StrongISceneNodePtr nodePtr
		*/
		virtual void destroySceneNode(StrongISceneNodePtr nodePtr) = 0;

		//-------------------------------------------------------------------------------------------

		/*!
		 * \remarks 获取当前的空间管理器
		 * \return 
		*/
		virtual ISpatialManager* getSpatialManager() const = 0;

		//-------------------------------------------------------------------------------------------

		/*!
		 * \remarks 创建射线场景查询
		 * \return 
		 * \param Ray const & ray
		*/
		virtual StrongISceneQueryPtr createRaySceneQuery(Ray const& ray) = 0;

		/*!
		 * \remarks 创建球体场景查询
		 * \return 
		 * \param Sphere const & sphere
		*/
		virtual StrongISceneQueryPtr createSphereSceneQuery(Sphere const& sphere) = 0;

		/*!
		 * \remarks 创建AABB场景查询
		 * \return 
		 * \param AABB const & box
		*/
		virtual StrongISceneQueryPtr createAABBSceneQuery(AABB const& box) = 0;

		/*!
		 * \remarks 创建摄像机场景查询
		 * \return 
		 * \param Camera const & camera
		*/
		virtual StrongISceneQueryPtr createCameraSceneQuery(Camera const& camera) = 0;
	};
}//namespace ung

#endif//UIF_HIERARCHICAL_COMPILE

#endif//_UIF_ISCENEMANAGER_H_