#ifndef _UTM_THREADPOOL_H_
#define _UTM_THREADPOOL_H_

#ifndef _UTM_CONFIG_H_
#include "UTMConfig.h"
#endif

#ifdef UTM_HIERARCHICAL_COMPILE

#ifndef _UTM_SCOPEDTHREAD_H_
#include "UTMScopedThread.h"
#endif

#ifndef _UTM_SAFEQUEUE_H_
#include "UTMSafeQueue.h"
#endif

#ifndef _UTM_TASK_H_
#include "UTMTask.h"
#endif

#ifndef _INCLUDE_STLIB_ATOMIC_
#include <atomic>
#define _INCLUDE_STLIB_ATOMIC_
#endif

#ifndef _INCLUDE_STLIB_FUTURE_
#include <future>
#define _INCLUDE_STLIB_FUTURE_
#endif

#ifndef _INCLUDE_STLIB_TYPETRAITS_
#include <type_traits>
#define _INCLUDE_STLIB_TYPETRAITS_
#endif

namespace ung
{
	class ThreadPool : boost::noncopyable
	{
	public:
		/*
		仅主线程调用。
		因为这里的参数f不带有参数，所以调用submit时，应该传入bind后的可调用对象。
		std::future<>对象用来保存任务的返回值，并且允许调用者等待任务结束。
		std::result_of<F()>::type为不带参数地调用F(类型的一个可调用对象)的返回值。
		*/
		template<typename F>
		std::future<typename std::result_of<F()>::type> submit(F f)
		{
			using result_type = typename std::result_of<F()>::type;

			/*
			包装可调用对象到std::package_task中。
			*/
			std::packaged_task<result_type()> task(std::move(f));

			/*
			std::future<>，用来表现某一操作的成果(outcome)：可能是个返回值或是一个异常，但不会二者都是。这份成果被管理于一个shared state内，
			后者可被std::async(),std::packaged_task或promise创建出来，这份成果也许尚未存在，因此future持有的也可能是“生成该成果”的每一件必要
			东西。
			成果只能被取出一次，因此future可能处于有效或无效状态：有效意味着“某一操作的成果或异常”尚未被取出。
			如果future的模板参数是个引用，那么get()便返回一个引用指向返回值。
			future既不提供copy构造函数也不提供copy assignment运算符，确保绝不会有两个对象共享某一后台操作之状态。“将某个future对象的状态move
			至另一个”的唯一办法是：调用move构造函数或move assignment运算符。

			虽然future被用于线程间通信，但是future对象本身却并不提供同步访问。如果多个线程需要访问同一个future对象，它们必须通过互斥元或其他同步
			机制来保护访问。
			*/
			std::future<result_type> fut(task.get_future());

			/*
			std::package_task是可移动，但不可复制的。
			移动std::package_task，然后调用Task的移动构造函数来push进去。
			其中Task的移动构造函数的模板参数为std::package_task。
			*/
			mTasks.push(std::move(task));

			return fut;
		}

		//记得在程序结束时调用ThreadPool::getInstance().closePool();
		void closePool()
		{
			//确保没有任务了
			while (true)
			{
				if (mTasks.empty())
				{
					mStopFlag = true;

					break;
				}
				else
				{
					std::this_thread::yield();
				}
			}
		}

		//----------------------------------------------------------------------------------------------------------

	public:
		UngExport static ThreadPool& getInstance()
		{
			std::call_once(mOnceFlag, init);

			return init();
		}

	private:
		/*
		构造函数只会被调用一次，就在第一次ThreadPool::getInstance()的时候。
		*/
		ThreadPool() :
			mStopFlag(false)
		{
#if UTM_DEBUGMODE
			//flag不存在data race，因为同一时间只会有一个线程access flag，所以不需用std::atomic<int> flag。
			static int flag = 0;
			++flag;
			BOOST_ASSERT(flag == 1);
#endif

			unsigned int coreNum = std::thread::hardware_concurrency();
			if (coreNum == 0)
			{
				coreNum = 2;
			}
			//当前机器为2核，为了测试，这里会超额订阅(oversubscription)，但是发现即便超额订阅也比线程池中只创建一个线程快，故，这样。
			unsigned int poolSize = coreNum;															

			try
			{
				for (unsigned int i = 0; i < poolSize; ++i)
				{
					ScopedThread st(std::thread(&ThreadPool::workingThread, this));
					mPool.push_back(std::move(st));
				}
			}
			catch (...)
			{
				mStopFlag = true;

				//throw;
				BOOST_ASSERT(0 && "fail to create thread pool.");
			}
		}

		virtual ~ThreadPool()
		{
			if (!mStopFlag)
			{
				BOOST_ASSERT(0 && "must call closePool() to make sure there is no tasks in the pool,before closing the engine.");
			}
		}

		/*
		被线程所执行的函数。
		*/
		void workingThread()
		{
			while (!mStopFlag)
			{
				Task task;

				if (mTasks.tryPop(task))
				{
					//执行真正的任务。
					task();
				}
				else
				{
					std::this_thread::yield();
				}
			}
		}

		/*
		初次被某个线程使用过后，其他线程再也不需要它。比如lazy initialization(缓式初始化)。
		如果两个或多个线程检查初始化是否尚未发生，然后启动初始化，就会可能发生data race。所以必须针对concurrent access保护“检查及初始化”程序区。
		可以使用mutex，但C++标准库为此提供了一个特殊解法。只需使用一个std::once_flag以及调用std::call_once。
		*/
		static ThreadPool& init()
		{
			static ThreadPool obj;

			return obj;
		}

		static std::once_flag mOnceFlag;
		std::atomic<bool> mStopFlag;
		SafeQueue<Task> mTasks;
		std::vector<ScopedThread> mPool;														//最先析构
	};
}//namespace ung

#endif//UTM_HIERARCHICAL_COMPILE

#endif//_UTM_THREADPOOL_H_