/*!
 * \brief
 * TypeList，通过宏来使用，支持最多10个类型
 * \file UFCTypeList.h
 *
 * \author Su Yang
 *
 * \date 2016/04/24
 */
#ifndef _UFC_TYPELIST_H_
#define _UFC_TYPELIST_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#ifndef _UFC_NULLTYPE_H_
#include "UFCNullType.h"
#endif

#ifndef _UFC_EMPTYTYPE_H_
#include "UFCEmptyType.h"
#endif

#ifndef _UFC_METAFUNCTION_H_
#include "UFCMetaFunction.h"
#endif

namespace ung
{
	namespace utp
	{
		template <typename T, typename U>
		struct TypeList
		{
			typedef T Head;											//first element, a non-typelist type by convention(按照惯例)
			typedef U Tail;											//second element, can be another typelist
		};

		//-----------------------------------------------------------------------------------------------------------------------------------------------------

		/*!
		 * \brief
		 * MakeTypeList<T1, T2, ...>::result
		 * Returns a typelist that is of T1, T2, ...
		 * 用法：using tl3 = ung::MakeTypeList<char, short, int>::result;
		 * 和直接使用宏没有什么区别。
		 * \class MakeTypeList
		 *
		 * \author Su Yang
		 *
		 * \date 2016/04/24
		 *
		 * \todo
		 */
		template <typename T1 = NullType, typename T2 = NullType, typename T3 = NullType,typename T4 = NullType, typename T5 = NullType,
						typename T6 = NullType,typename T7 = NullType, typename T8 = NullType, typename T9 = NullType,typename T10 = NullType>
		struct MakeTypeList
		{
		private:
			typedef typename MakeTypeList<T2, T3, T4,T5, T6, T7,T8, T9, T10>::result TailResult;

		public:
			typedef TypeList<T1, TailResult> result;
		};

		template<>
		struct MakeTypeList<>
		{
			typedef NullType result;
		};

		/*!
		 * \brief
		 * 计算一个typelist的长度（包含type的个数）
		 * 用法：using tl5 = TYPELIST_5(int, real_type, float, char,short);cout << Length<tl5>::value << endl;（输出5）
		 * 返回一个编译期常量。
		 * \class Length
		 *
		 * \author Su Yang
		 *
		 * \date 2016/04/24
		 *
		 * \todo
		 */
		template <typename TList>
		struct Length;

		//全特化，只匹配NullType
		template <>
		struct Length<NullType>
		{
			enum { value = 0 };
		};

		//偏特化，可匹配任何TypeList<T, U>，包括“复合型（compound）typelist”---亦即U本身又是个Typelist<V, W>。
		template <typename T, typename U>
		struct Length<TypeList<T, U>>
		{
			//递归式运算，当tail变成NullType时，吻合第一份特化定义，于是停止递归并传回结果。
			enum { value = 1 + Length<U>::value };
		};

		/*!
		 * \brief
		 * 获取给定索引的类型，使访问线性化
		 * 用法：using tl5 = TYPELIST_5(int, double, float, char,short);
		 * cout << typeid(TypeAt<tl5,1>::result).name() << endl;（输出double）
		 * 注意：typeid不属于std名字空间，包含头文件#include <typeinfo>后，直接使用即可。
		 * 注意：索引从0开始。
		 * 如果给定编译器整数常量超越了边界，则编译错误:使用了未定义类型TypeAt<Tail,0>
		 * \class TypeAt
		 *
		 * \author Su Yang
		 *
		 * \date 2016/04/24
		 *
		 * \todo
		 */
		template <typename TList, unsigned int index>
		struct TypeAt;

		//如果TList不为null，且i为0，那么result就是TList的头部。
		template <typename Head, typename Tail>
		struct TypeAt<TypeList<Head, Tail>, 0>
		{
			typedef Head result;
		};

		//如果TList不为null且i不为0，那么result就是“将TypeAt施行于TList尾端，同时索引为i - 1”的结果
		template <typename Head, typename Tail, unsigned int i>
		struct TypeAt<TypeList<Head, Tail>, i>
		{
			typedef typename TypeAt<Tail, i - 1>::result result;
		};

		//如果逾界（out-of-bound）访问，造成编译错误。

		/*!
		 * \brief
		 * 实作出TypeAt的功能，不同之处是它对逾界访问更加宽容，以NullType为传回值，取代编译错误消息。
		 * Finds the type at a given index in a typelist
		 * Invocations (TList is a typelist and index is a compile-time integral constant):
		 * a) TypeAtNonStrict<TList, index>::result
		 * Returns the type in position 'index' in TList, or NullType if index is out-of-bounds
		 * b) TypeAtNonStrict<TList, index, D>::result
		 * Returns the type in position 'index' in TList, or D if index is out-of-bounds
		 * \class TypeAtNonStrict
		 *
		 * \author Su Yang
		 *
		 * \date 2016/04/24
		 *
		 * \todo
		 */
		template <typename TList, unsigned int index,typename DefaultType = NullType>
		struct TypeAtNonStrict
		{
			typedef DefaultType result;
		};

		template <typename Head, typename Tail, typename DefaultType>
		struct TypeAtNonStrict<TypeList<Head, Tail>, 0, DefaultType>
		{
			typedef Head result;
		};

		template <typename Head, typename Tail, unsigned int i, typename DefaultType>
		struct TypeAtNonStrict<TypeList<Head, Tail>, i, DefaultType>
		{
			typedef typename TypeAtNonStrict<Tail, i - 1, DefaultType>::result result;
		};

		/*!
		 * \brief
		 * 获取给定类型的索引值
		 * 用法：using tl5 = TYPELIST_5(int, real_type, float, char,short);cout << IndexOf<tl5,float>::value << endl;（输出2）
		 * 如果没有找到，则返回NullType
		 * \class IndexOf
		 *
		 * \author Su Yang
		 *
		 * \date 2016/04/24
		 *
		 * \todo
		 */
		template <typename TList, typename T>
		struct IndexOf;

		//如果TList是NullType，令value为-1
		template <typename T>
		struct IndexOf<NullType, T>
		{
			enum { value = -1 };
		};

		//如果TList的头端是T，令value为0
		template <typename T, typename Tail>														//template <typename Head, typename Tail>
		struct IndexOf<TypeList<T, Tail>, T>
		{
			enum { value = 0 };
		};

		//将IndexOf施行于“TList尾端”，并将结果置于一个暂时变量temp，如果temp为 - 1，令value为 - 1，否则令value为1 + temp
		template <typename Head, typename Tail, typename T>
		struct IndexOf<TypeList<Head, Tail>, T>
		{
		private:
			enum { temp = IndexOf<Tail, T>::value };

		public:
			enum { value = (temp == -1 ? -1 : 1 + temp) };
		};

		/*!
		 * \brief
		 * 附加一个type或者一个typelist至另一个
		 * Invocation (TList is a typelist and T is either a type or a typelist):Append<TList, T>::result
		 * Returns a typelist that is TList followed by T and NullType-terminated
		 * 用法：typedef Append<SignedIntegrals,
		 *														TYPELIST_3(float,double,long double)>::result SignedTypes
		 * SignedTypes便涵盖了C++所有带正负号的数值型别。
		 * \class Append
		 *
		 * \author Su Yang
		 *
		 * \date 2016/04/24
		 *
		 * \todo
		 */
		template <typename TList, typename T>
		struct Append;

		//如果TList是NullType而且T是NullType，那么令result为NullType
		template <>
		struct Append<NullType, NullType>
		{
			typedef NullType result;
		};

		//如果TList是NullType，且T是个type（而非typelist），那么result将是“只含唯一元素T”的一个typelist
		template <typename T>
		struct Append<NullType, T>
		{
			typedef TypeList<T, NullType> result;
		};

		//如果TList是NullType，且T是一个typelist，那么result便是T本身。
		template <typename Head, typename Tail>
		struct Append<NullType, TypeList<Head, Tail> >
		{
			typedef TypeList<Head, Tail> result;
		};

		//如果TList是non-null，那么result将是个typelist，以TList::Head为其头端，并以“T附加至TList::Tail”的结果为其尾端
		template <typename Head, typename Tail, typename T>
		struct Append<TypeList<Head, Tail>, T>
		{
			typedef TypeList<Head,typename Append<Tail, T>::result> result;
		};

		/*!
		 * \brief
		 * 删除第一次出现的类型
		 * Invocation (TList is a typelist and T is a type):Erase<TList, T>::result
		 * Returns a typelist that is TList without the first occurence of T
		 * 用法：typedef Erase<someTypesWithFloat,float>::result someTypesWithoutFloat;(移除掉了第一个float)
		 * \class Erase
		 *
		 * \author Su Yang
		 *
		 * \date 2016/04/24
		 *
		 * \todo
		 */
		template <typename TList, typename T>
		struct Erase;

		//如果TList是NullType，那么result就是NullType
		template <typename T>
		struct Erase<NullType, T>
		{
			typedef NullType result;
		};

		//如果T等同于TList::Head，那么result就是TList::Tail
		template <typename T, typename Tail>
		struct Erase<TypeList<T, Tail>, T>
		{
			typedef Tail result;
		};

		//result将是一个typelist，它以TList::Head为头端，并以“将Erase施行于TList::Tail”所得结果为尾端
		template <typename Head, typename Tail, typename T>
		struct Erase<TypeList<Head, Tail>, T>
		{
			typedef TypeList<Head,typename Erase<Tail, T>::result> result;
		};

		/*!
		 * \brief
		 * 删除typelist中某个型别的所有出现个体
		 * Invocation (TList is a typelist and T is a type):EraseAll<TList, T>::result
		 * Returns a typelist that is TList without any occurence of T
		 * \class EraseAll
		 *
		 * \author Su Yang
		 *
		 * \date 2016/04/24
		 *
		 * \todo
		 */
		template <typename TList, typename T>
		struct EraseAll;

		template <typename T>
		struct EraseAll<NullType, T>
		{
			typedef NullType result;
		};
		template <typename T, typename Tail>
		struct EraseAll<TypeList<T, Tail>, T>
		{
			typedef typename EraseAll<Tail, T>::result result;
		};
		template <typename Head, typename Tail, typename T>
		struct EraseAll<TypeList<Head, Tail>, T>
		{
			typedef TypeList<Head,typename EraseAll<Tail, T>::result> result;
		};

		/*!
		 * \brief
		 * 删除重复的类型（保留1个）
		 * 用法：using tl4 = TYPELIST_4(int, real_type, int,int);
		 *			using tl4_nd = NoDuplicates<tl4>::result;
		 *			cout << Length<tl4_nd>::value << endl;(输出2)
		 * \class NoDuplicates
		 *
		 * \author Su Yang
		 *
		 * \date 2016/04/24
		 *
		 * \todo
		 */
		template <typename TList>
		struct NoDuplicates;

		//如果TList是NullType，那么就令result为NullType
		template <>
		struct NoDuplicates<NullType>
		{
			typedef NullType result;
		};

		/*
		将NoDuplicates施行于TList::Tail身上，获得一个暂时的typelist L1，将Erase施行于L1，第二个参数为TList::Head，获得L2，result是个typelist，其头端
		为TList::Head，尾端为L2
		*/
		template <typename Head, typename Tail>
		struct NoDuplicates< TypeList<Head, Tail> >
		{
		private:
			typedef typename NoDuplicates<Tail>::result L1;
			/*
			第一感觉以为应该使用EraseAll，但答案是：Erase被实施于NoDuplicates递归操作之后，这意味着我们实施的“对象”是一个“不再有任何重复个体”的型别，所以
			最多只会有一个型别个体被移除。
			*/
			typedef typename Erase<L1, Head>::result L2;

		public:
			typedef TypeList<Head, L2> result;
		};

		/*!
		 * \brief
		 * 把出现的第一个T类型给替换为U类型
		 * Invocation (TList is a typelist, T, U are types):Replace<TList, T, U>::result
		 * Returns a typelist in which the first occurence of T is replaced with U
		 * \class Replace
		 *
		 * \author Su Yang
		 *
		 * \date 2016/04/24
		 *
		 * \todo
		 */
		template <typename TList, typename T, typename U>
		struct Replace;

		//如果TList是NullType，那么就令result为NullType
		template <typename T, typename U>
		struct Replace<NullType, T, U>
		{
			typedef NullType result;
		};

		//如果TList的头端是T，那么result将是一个typelist，以U为其头端并以TList::Tail为其尾端。
		template <typename T, typename Tail, typename U>
		struct Replace<TypeList<T, Tail>, T, U>
		{
			typedef TypeList<U, Tail> result;
		};

		//result是一个typelist，以TList::Head为其头端，并以“Replace施行于TList”的结果为其尾端
		template <typename Head, typename Tail, typename T, typename U>
		struct Replace<TypeList<Head, Tail>, T, U>
		{
			typedef TypeList<Head,typename Replace<Tail, T, U>::result> result;
		};

		/*!
		 * \brief
		 * 把出现的所有T类型给替换为U类型
		 * Invocation (TList is a typelist, T, U are types):Replace<TList, T, U>::result
		 * Returns a typelist in which all occurences of T is replaced with U
		 * \class ReplaceAll
		 *
		 * \author Su Yang
		 *
		 * \date 2016/04/24
		 *
		 * \todo
		 */
		template <typename TList, typename T, typename U>
		struct ReplaceAll;

		template <typename T, typename U>
		struct ReplaceAll<NullType, T, U>
		{
			typedef NullType result;
		};

		template <typename T, typename Tail, typename U>
		struct ReplaceAll<TypeList<T, Tail>, T, U>
		{
			typedef TypeList<U, typename ReplaceAll<Tail, T, U>::result> result;
		};

		template <typename Head, typename Tail, typename T, typename U>
		struct ReplaceAll<TypeList<Head, Tail>, T, U>
		{
			typedef TypeList<Head,typename ReplaceAll<Tail, T, U>::result> result;
		};

		/*!
		 * \brief
		 * 逆序
		 * Invocation (TList is a typelist):Reverse<TList>::result
		 * Returns a typelist that is TList reversed
		 * \class Reverse
		 *
		 * \author Su Yang
		 *
		 * \date 2016/04/24
		 *
		 * \todo
		 */
		template <typename TList>
		struct Reverse;

		template <>
		struct Reverse<NullType>
		{
			typedef NullType result;
		};

		template <typename Head, typename Tail>
		struct Reverse< TypeList<Head, Tail> >
		{
			typedef typename Append<typename Reverse<Tail>::result, Head>::result result;
		};

		/*!
		 * \brief
		 * 获取基类T的最深的派生型别，如果没有任何派生型别就返回T自己
		 * Invocation (TList is a typelist, T is a type):MostDerived<TList, T>::result
		 * \class MostDerived
		 *
		 * \author Su Yang
		 *
		 * \date 2016/04/24
		 *
		 * \todo
		 */
		template <typename TList, typename T>
		struct MostDerived;

		//如果TList是NullType，令result为T。
		template <typename T>
		struct MostDerived<NullType, T>
		{
			typedef T result;
		};

		/*
		将MostDerived施行于TList::Tail和T身上，获得一个Candidate。如果TList::Head派生自Candidate，令result为TList::Head。否则，令result为Candidate。
		*/
		template <typename Head, typename Tail, typename T>
		struct MostDerived<TypeList<Head, Tail>, T>
		{
		private:
			typedef typename MostDerived<Tail, T>::result Candidate;

		public:
			typedef typename Select<SuperSubclass<Candidate, Head>::value,Head, Candidate>::result result;
		};

		/*!
		 * \brief
		 * 让子类出现在基类的前面
		 * Invocation (TList is a typelist):DerivedToFront<TList>::result
		 * Returns the reordered（重新排序） TList
		 * \class DerivedToFront
		 *
		 * \author Su Yang
		 *
		 * \date 2016/04/24
		 *
		 * \todo
		 */
		template <typename TList>
		struct DerivedToFront;

		//如果TList是NullType，那么就令result为NullType
		template <>
		struct DerivedToFront<NullType>
		{
			typedef NullType result;
		};

		/*
		从TList::Head到TList::Tail，找出最末端派生型别，存储于暂时变量TheMostDerived中。以TList::Head取代TList::Tail中的TheMostDerived，获得L。建立一
		个typelist，以TheMostDerived为其头端，以L为其尾端。
		*/
		template <typename Head, typename Tail>
		struct DerivedToFront<TypeList<Head, Tail> >
		{
		private:
			typedef typename MostDerived<Tail, Head>::result TheMostDerived;
			typedef typename Replace<Tail,TheMostDerived, Head>::result Temp;
			typedef typename DerivedToFront<Temp>::result L;

		public:
			typedef TypeList<TheMostDerived, L> result;
		};
	}//namespace utp
}//namespace ung

/*
将TypeList的生成线性化。
用法：
typedef TYPELIST_4(signed char,short int,int,long int) SignedIntegrals;
*/
#define TYPELIST_1(T1) ung::utp::TypeList<T1,ung::utp::NullType>
#define TYPELIST_2(T1, T2) ung::utp::TypeList<T1, TYPELIST_1(T2)>
#define TYPELIST_3(T1, T2, T3) ung::utp::TypeList<T1, TYPELIST_2(T2, T3)>
#define TYPELIST_4(T1, T2, T3, T4) ung::utp::TypeList<T1, TYPELIST_3(T2, T3, T4)>
#define TYPELIST_5(T1, T2, T3, T4, T5) ung::utp::TypeList<T1, TYPELIST_4(T2, T3, T4, T5)>
#define TYPELIST_6(T1, T2, T3, T4, T5, T6) ung::utp::TypeList<T1, TYPELIST_5(T2, T3, T4, T5, T6)>
#define TYPELIST_7(T1, T2, T3, T4, T5, T6, T7) ung::utp::TypeList<T1, TYPELIST_6(T2, T3, T4, T5, T6, T7)>
#define TYPELIST_8(T1, T2, T3, T4, T5, T6, T7, T8) ung::utp::TypeList<T1, TYPELIST_7(T2, T3, T4, T5, T6, T7, T8)>
#define TYPELIST_9(T1, T2, T3, T4, T5, T6, T7, T8, T9) ung::utp::TypeList<T1, TYPELIST_8(T2, T3, T4, T5, T6, T7, T8, T9)>
#define TYPELIST_10(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10) ung::utp::TypeList<T1, TYPELIST_9(T2, T3, T4, T5, T6, T7, T8, T9, T10)>

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_TYPELIST_H_