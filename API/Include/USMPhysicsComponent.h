/*!
 * \brief
 * 物理组件。
 * \file USMPhysicsComponent.h
 *
 * \author Su Yang
 *
 * \date 2017/03/31
 */
#ifndef _USM_PHYSICSCOMPONENT_H_
#define _USM_PHYSICSCOMPONENT_H_

#ifndef _USM_CONFIG_H_
#include "USMConfig.h"
#endif

#ifdef USM_HIERARCHICAL_COMPILE

#ifndef _USM_OBJECTCOMPONENT_H_
#include "USMObjectComponent.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 物理组件
	 * \class PhysicsComponent
	 *
	 * \author Su Yang
	 *
	 * \date 2017/03/31
	 *
	 * \todo
	 */
	class UngExport PhysicsComponent : public ObjectComponent
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		PhysicsComponent();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~PhysicsComponent();

		/*!
		 * \remarks 根据xml来初始化
		 * \return 
		 * \param tinyxml2::XMLElement * pComponentElement
		*/
		virtual void init(tinyxml2::XMLElement* pComponentElement) override;

		/*!
		 * \remarks 对象组装完成后，对象可能会再根据其都由那些组件所组合的来决定是否再进行一些初始化工作。
		 * \return 
		*/
		virtual void postInit() override;

		/*!
		 * \remarks 更新
		 * \return 
		*/
		virtual void update() override;

	private:
		String mShape;
	};
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE

#endif//_USM_PHYSICSCOMPONENT_H_