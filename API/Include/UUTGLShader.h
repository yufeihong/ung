/*!
 * \brief
 * OpenGL shader
 * \file UUTGLShader.h
 *
 * \author Su Yang
 *
 * \date 2016/11/13
 */
#ifndef _UUT_GLSHADER_H_
#define _UUT_GLSHADER_H_

#ifndef _UUT_CONFIG_H_
#include "UUTConfig.h"
#endif

#ifdef UUT_HIERARCHICAL_COMPILE

namespace ung
{
	class Parser;

	/*!
	 * \brief
	 * 对OpenGL shader的一个封装
	 * \class GLShader
	 *
	 * \author Su Yang
	 *
	 * \date 2016/11/13
	 *
	 * \todo
	 */
	class UngExport GLShader : public MemoryPool<GLShader>
	{
	public:
		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param String const & fileName
		*/
		GLShader(String const& fileName);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~GLShader();

		/*!
		 * \remarks 读取shader源文件
		 * \return 
		 * \param String const & fileName
		*/
		void read(String const& fileName);

		/*!
		 * \remarks 编译shader
		 * \return 
		*/
		void compile();

		/*!
		 * \remarks 获取shader的标识符
		 * \return 
		*/
		GLuint const getShaderObject() const;

	private:
		GLenum mType;
		String mName;
		GLuint mShaderObject;
		std::unique_ptr<Parser> mParser;
	};
}//namespace ung

#endif//UUT_HIERARCHICAL_COMPILE

#endif//_UUT_GLSHADER_H_