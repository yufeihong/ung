/*!
 * \brief
 * 过程管理器
 * \file USMProcessManager.h
 *
 * \author Su Yang
 *
 * \date 2016/12/14
 */
#ifndef _USM_PROCESSMANAGER_H_
#define _USM_PROCESSMANAGER_H_

#ifndef _USM_CONFIG_H_
#include "USMConfig.h"
#endif

#ifdef USM_HIERARCHICAL_COMPILE

namespace ung
{
	class Process;

	/*!
	 * \brief
	 * 过程管理器
	 * \class ProcessManager
	 *
	 * \author Su Yang
	 *
	 * \date 2016/12/14
	 *
	 * \todo
	 */
	class UngExport ProcessManager : public Singleton<ProcessManager>
	{
	public:
		using StrongProcessPtr = std::shared_ptr<Process>;
		using WeakProcessPtr = std::weak_ptr<Process>;

	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		ProcessManager();

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~ProcessManager();

		/*!
		 * \remarks 更新所有的“过程”
		 * \return first为成功的数量(“过程”链成功为单位)，second为放弃的数量
		 * \param ufast deltaMS 单位：毫秒
		*/
		std::pair<ufast,ufast> updateAllProcesses(ufast deltaMS);

		/*!
		 * \remarks 添加“过程”到管理器的容器中，以便于在下一帧“过程”可以运行
		 * \return 
		 * \param StrongProcessPtr pProcess
		*/
		WeakProcessPtr addProcess(StrongProcessPtr pProcess);

		/*!
		 * \remarks 放弃所有的“过程”
		 * \return 
		 * \param bool immediate
		*/
		void abortAllProcesses(bool immediate);

		/*!
		 * \remarks 获取“过程”的数量
		 * \return 
		*/
		ufast getProcessCount() const;

	private:
		/*!
		 * \remarks 清空存储“过程”的容器
		 * \return 
		*/
		void clearAllProcesses();

	private:
		STL_LIST(StrongProcessPtr) mProcesses;
	};
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE

#endif//_USM_PROCESSMANAGER_H_