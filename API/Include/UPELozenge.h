/*!
 * \brief
 * 菱形(球体扫掠矩形)(类似于带有圆角边的OBB，用于替代OBB)
 * \file UPELozenge.h
 *
 * \author Su Yang
 *
 * \date 2017/04/06
 */
#ifndef _UPE_LOZENGE_H_
#define _UPE_LOZENGE_H_

#ifndef _UPE_CONFIG_H_
#include "UPEConfig.h"
#endif

#ifdef UPE_HIERARCHICAL_COMPILE

#ifndef _UIF_IBOUNDINGVOLUME_H_
#include "UIFIBoundingVolume.h"
#endif

namespace ung
{
	class UngExport Lozenge : public IBoundingVolume
	{
	private:
		Vector3 mCenter;
		Vector3 mEdgesAxes[2];
		real_type mRadius;
	};
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

#endif//_UPE_LOZENGE_H_