/*!
 * \brief
 * 四元数。
 * \file UMLQuater.h
 *
 * \author Su Yang
 *
 * \date 2016/06/15
 */
#ifndef _UML_QUATER_H_
#define _UML_QUATER_H_

#ifndef _UML_CONFIG_H_
#include "UMLConfig.h"
#endif

#ifdef UML_HIERARCHICAL_COMPILE

#ifndef _UML_TYPEDEF_H_
#include "UMLTypedef.h"
#endif

#ifndef _UML_MAT3_H_
#include "UMLMat3.h"
#endif

#ifndef _UML_VEC3_H_
#include "UMLVec3.h"
#endif

#ifndef _UML_VEC4_H_
#include "UMLVec4.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 四元数
	 * \class Quater
	 *
	 * \author Su Yang
	 *
	 * \date 2017/03/19
	 *
	 * \todo
	 */
	template<typename T>
	class Quater : public MemoryPool<Quater<T>>
	{
		/*!
		 * \remarks 取负
		 * \return 
		 * \param Quater const & q
		*/
		friend Quater operator-(Quater const& q)
		{
			return Quater(-q.w, -q.x, -q.y, -q.z);
		}

		/*!
		 * \remarks 标量+四元数
		 * \return 
		 * \param T const & s
		 * \param Quater const & q
		*/
		friend Quater operator+(T const& s, Quater const& q)
		{
			return q + s;
		}

		/*!
		 * \remarks 标量*四元数
		 * \return 
		 * \param T const & s
		 * \param Quater const & q
		*/
		friend Quater operator*(T const& s, Quater const& q)
		{
			return q * s;
		}

	public:
		using element_type = T;
		using math_type = MathImpl<element_type>;

		/*!
		 * \remarks 默认构造函数
		 * \return 
		 * \param T ww
		 * \param T xx
		 * \param T yy
		 * \param T zz
		*/
		Quater(T ww = 1.0,T xx = 0.0,T yy = 0.0,T zz = 0.0) :
			w(ww),
			x(xx),
			y(yy),
			z(zz)
		{
		}

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~Quater() = default;

		/*!
		 * \remarks 拷贝构造函数
		 * \return 
		 * \param Quater const & other
		*/
		Quater(Quater const& other) :
			w(other.w),
			x(other.x),
			y(other.y),
			z(other.z)
		{
		}

		/*!
		 * \remarks 拷贝运算符
		 * \return 
		 * \param Quater const & other
		*/
		Quater& operator=(Quater const& other)
		{
			if (this != &other)
			{
				w = other.w;
				x = other.x;
				y = other.y;
				z = other.z;
			}

			return *this;
		}

		/*!
		 * \remarks 移动构造函数
		 * \return 
		 * \param Quater & & other
		*/
		Quater(Quater&& other) :
			w(other.w),
			x(other.x),
			y(other.y),
			z(other.z)
		{
		}

		/*!
		 * \remarks 移动赋值运算符
		 * \return 
		 * \param Quater & & other
		*/
		Quater& operator=(Quater&& other)
		{
			if (this != &other)
			{
				w = other.w;
				x = other.x;
				y = other.y;
				z = other.z;
			}

			return *this;
		}

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param const Mat3<T> & rot 旋转矩阵
		*/
		Quater(const Mat3<T>& rot)
		{
			this->fromRotationMatrix(rot);
		}

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param const Vec3<T> & axis 旋转轴
		 * \param const Radian& r 旋转角度(弧度)
		*/
		Quater(const Vec3<T>& axis,const Radian& r)
		{
			this->fromAxisAngle(axis,r);
		}

		/*!
		 * \remarks 设置为单位四元数
		 * \return void
		*/
		void setIdentity()
		{
			w = 1.0;
			x = y = z = 0.0;
		}

		/*!
		 * \remarks 判断当前四元数是否表示一个零旋转
		 * \return 
		*/
		bool isIdentity() const
		{
			return (math_type::isUnit(w) && 
				math_type::isZero(x) && 
				math_type::isZero(y) && 
				math_type ::isZero(z));
		}

		/*!
		 * \remarks 四元数长度的平方
		 * \return 
		*/
		T squaredLength() const
		{
			return w * w + x * x + y * y + z * z;
		}

		/*!
		 * \remarks 四元数的长度
		 * \return 
		*/
		T length() const
		{
			T sl = squaredLength();

			if (math_type::isUnit(sl))
			{
				return 1.0;
			}

			return math_type::calSqrt(sl);
		}

		/*!
		 * \remarks 是否是单位长度
		 * \return 
		*/
		bool isUnit() const
		{
			return math_type::isUnit(squaredLength());
		}

		/*!
		 * \remarks 通常四元数都是正则化的.
		 * 这个函数是为了防止误差扩大,连续多个四元数操作可能导致误差扩大.如果要对同一四元数执行上百次连续运算,可能就要进行一次规范化了.
		 * 虽然欧拉角向四元数的转换只产生正则化的四元数,避免了误差扩大的可能.但是矩阵和四元数间的转换却存在这一问题.
		 * \return void
		*/
		void normalize()
		{
			if (isUnit())
			{
				return;
			}

			T len = length();

			T inv = 1.0 / len;

			x *= inv;
			y *= inv;
			z *= inv;
			w *= inv;

			BOOST_ASSERT(isUnit());
		}

		/*!
		 * \remarks 不改变当前四元数
		 * \return 一个单位化的四元数
		*/
		Quater normalizeCopy()
		{
			Quater ret = *this;
			ret.normalize();

			return ret;
		}

		/*!
		 * \remarks 四元数的共轭
		 * \return ung::Quater
		*/
		Quater conjugate() const
		{
			return Quater(w, -x, -y, -z);
		}

		/*!
		 * \remarks 返回当前四元数的逆
		 * \return 
		*/
		Quater inverse() const
		{
			if (isUnit())
			{
				return conjugate();
			}

			T inv = 1.0 / length();
			return Quater(w * inv, (-x) * inv, (-y) * inv, (-z) * inv);
		}

		/*!
		 * \remarks 两个四元数的"差".
		 * "差"被定义为一个方位到另一个方位的角位移.换句话说,给定方位a和b,能够计算从a旋转到b的角位移d.
		 * \return ung::Quater
		 * \param const Quater& q 
		*/
		Quater difference(const Quater& q) const
		{
			return inverse() * q;
		}

		/*!
		 * \remarks 构建绕X轴旋转的四元数(构建出来的四元数是单位四元数)
		 * \return void
		 * \param Radian r 弧度
		*/
		void setRotateX(Radian r)
		{
			Radian hr = r * 0.5;

			w = math_type::calCos(hr);
			x = math_type::calSin(hr);
			y = 0.0;
			z = 0.0;

			BOOST_ASSERT(isUnit());
		}

		/*!
		 * \remarks 构建绕Y轴旋转的四元数
		 * \return void
		 * \param Radian r 弧度
		*/
		void setRotateY(Radian r)
		{
			Radian hr = r * 0.5;

			w = math_type::calCos(hr);
			x = 0.0;
			y = math_type::calSin(hr);
			z = 0.0;

			BOOST_ASSERT(isUnit());
		}

		/*!
		 * \remarks 构建绕Z轴旋转的四元数
		 * \return void
		 * \param Radian r 弧度 
		*/
		void setRotateZ(Radian r)
		{
			Radian hr = r * 0.5;

			w = math_type::calCos(hr);
			x = 0.0;
			y = 0.0;
			z = math_type::calSin(hr);

			BOOST_ASSERT(isUnit());
		}

		/*!
		 * \remarks 构建绕X轴旋转的四元数
		 * \return void
		 * \param Degree d 度
		*/
		void setRotateX(Degree d)
		{
			setRotateX(d.toRadian());
		}

		/*!
		 * \remarks 构建绕Y轴旋转的四元数
		 * \return void
		 * \param Degree d 度
		*/
		void setRotateY(Degree d)
		{
			setRotateY(d.toRadian());
		}

		/*!
		 * \remarks 构建绕Z轴旋转的四元数
		 * \return void
		 * \param Degree d 度
		*/
		void setRotateZ(Degree d)
		{
			setRotateZ(d.toRadian());
		}

		/*!
		 * \remarks 构建绕X轴旋转的四元数
		 * \return 
		 * \param Radian r 弧度 
		*/
		static Quater makeRotateX(Radian r)
		{
			Quater q;

			q.setRotateX(r);

			return std::move(q);
		}

		/*!
		 * \remarks 构建绕Y轴旋转的四元数
		 * \return 
		 * \param Radian r 弧度
		*/
		static Quater makeRotateY(Radian r)
		{
			Quater q;

			q.setRotateY(r);

			return std::move(q);
		}

		/*!
		 * \remarks 构建绕Z轴旋转的四元数
		 * \return 
		 * \param Radian r 弧度
		*/
		static Quater makeRotateZ(Radian r)
		{
			Quater q;

			q.setRotateZ(r);

			return std::move(q);
		}

		/*!
		 * \remarks 构建绕X轴旋转的四元数
		 * \return 
		 * \param Degree d 度
		*/
		static Quater makeRotateX(Degree d)
		{
			Quater q;

			q.setRotateX(d);

			return std::move(q);
		}

		/*!
		 * \remarks 构建绕Y轴旋转的四元数
		 * \return 
		 * \param Degree d 度
		*/
		static Quater makeRotateY(Degree d)
		{
			Quater q;

			q.setRotateY(d);

			return std::move(q);
		}

		/*!
		 * \remarks 构建绕Z轴旋转的四元数
		 * \return 
		 * \param Degree d 度
		*/
		static Quater makeRotateZ(Degree d)
		{
			Quater q;

			q.setRotateZ(d);

			return std::move(q);
		}

		/*!
		 * \remarks 构建绕任意轴旋转的四元数(构建出来的四元数是单位四元数)
		 * \return 
		 * \param const Vec3& n 轴
		 * \param Radian r 弧度
		*/
		void setRotateAnyAxis(const Vec3<T>& n, Radian r)
		{
			Vec3<T> nCopy{ n };

			if (!nCopy.isUnitLength())
			{
				nCopy.normalize();
			}

			Radian hr = r * 0.5;
			T sinValue = math_type::calSin(hr);

			w = math_type::calCos(hr);
			x = sinValue * nCopy.x;
			y = sinValue * nCopy.y;
			z = sinValue * nCopy.z;

			BOOST_ASSERT(isUnit());
		}

		/*!
		 * \remarks 构建绕任意轴旋转的四元数
		 * \return 
		 * \param const Vec3& n 轴
		 * \param Degree d 度
		*/
		void setRotateAnyAxis(const Vec3<T>& n, Degree d)
		{
			setRotateAnyAxis(n,d.toRadian());
		}

		/*!
		 * \remarks 构建绕任意轴旋转的四元数
		 * \return 
		 * \param const Vec3& n 
		 * \param Radian r 弧度
		*/
		static Quater makeRotateAnyAxis(const Vec3<T>& n, Radian r)
		{
			Quater q;

			q.setRotateAnyAxis(n, r);

			return std::move(q);
		}

		/*!
		 * \remarks 构建绕任意轴旋转的四元数
		 * \return 
		 * \param const Vec3& n 
		 * \param Degree d 度
		*/
		static Quater makeRotateAnyAxisD(const Vec3<T>& n,Degree d)
		{
			Quater q;

			q.setRotateAnyAxis(n,d);

			return std::move(q);
		}

		/*!
		 * \remarks 旋转矩阵到四元数
		 * \return 
		 * \param const Mat3<T> & rotMat3
		*/
		void fromRotationMatrix(const Mat3<T>& rotMat3)
		{
			rotMat3.getQuaternionOfRotate(*this);
		}

		/*!
		 * \remarks 四元数到旋转矩阵
		 * \return 
		*/
		Mat3<T> getMatrixOfRotation() const
		{
			Mat3<T> rotMat3{};
			rotMat3.setRotateFromQuaternion(*this);

			return std::move(rotMat3);
		}

		/*!
		 * \remarks 从轴-角对来构建一个四元数
		 * \return 
		 * \param const Vec3<T> & axis 单位长度的轴
		 * \param const Radian& r 弧度
		*/
		void fromAxisAngle(const Vec3<T>& axis,const Radian& r)
		{
			BOOST_ASSERT(axis.isUnitLength());

			Radian halfAngle(r * 0.5);
			T sinValue = Math::calSin(halfAngle);

			w = Math::calCos(halfAngle);
			x = sinValue*axis.x;
			y = sinValue*axis.y;
			z = sinValue*axis.z;

			BOOST_ASSERT(isUnit());
		}

		/*!
		 * \remarks 从轴-角对来构建一个四元数
		 * \return 
		 * \param const Vec3<T> & axis 单位长度的轴
		 * \param const Degree& d 度
		*/
		void fromAxisAngle(const Vec3<T>& axis,const Degree& d)
		{
			fromAxisAngle(axis,d.toRadian());
		}

		/*!
		 * \remarks 抽取四元数的轴-角
		 * \return 返回的轴是单位向量
		 * \param Vec3 & axis 
		 * \param Radian& r 弧度
		*/
		void extractAxisAngle(Vec3<T>& axis, Radian& r) const
		{
			T len2 = x * x + y * y + z * z;

			if (len2 > 0.0)
			{
				r = Radian(2.0 * math_type::calACos(w));
				T invLen = 1.0 / math_type::calSqrt(len2);

				axis.x = x * invLen;
				axis.y = y * invLen;
				axis.z = z * invLen;
			}
			else
			{
				//如果旋转轴为0向量的话，返回一个代表正x轴的向量
				r = Radian(0.0);
				axis.x = 1.0;
				axis.y = 0.0;
				axis.z = 0.0;
			}

			BOOST_ASSERT(axis.isUnitLength());
		}

		/*!
		 * \remarks 两个四元数相加
		 * \return 
		 * \param Quater const & q
		*/
		Quater operator+(Quater const& q) const
		{
			return Quater(w + q.w,x + q.x,y + q.y,z + q.z);
		}

		/*!
		 * \remarks 四元数 + 标量
		 * \return 
		 * \param T const & s
		*/
		Quater operator+(T const& s) const
		{
			Quater<T> ret(q);
			ret.w += s;

			return std::move(ret);
		}

		/*!
		 * \remarks 两个四元数相减
		 * \return 
		 * \param Quater const & q
		*/
		Quater operator-(Quater const& q) const
		{
			return operator+(-q);
		}

		/*!
		 * \remarks 两个四元数相乘(p * q != q * p)
		 * \return 
		 * \param const Quater& q
		*/
		Quater operator*(const Quater& q) const
		{
			return Quater(
					w * q.w - x * q.x - y * q.y - z * q.z,
					w * q.x + x * q.w + y * q.z - z * q.y,
					w * q.y + y * q.w + z * q.x - x * q.z,
					w * q.z + z * q.w + x * q.y - y * q.x);
		}

		/*!
		 * \remarks 重载*=运算符
		 * \return 
		 * \param Quater const & q
		*/
		Quater& operator*=(Quater const& q)
		{
			BOOST_ASSERT(q.isUnit());

			Quater temp{};

			temp = (*this) * q;

			//交换*this和temp
			swap(temp);

			BOOST_ASSERT(isUnit());

			return *this;
		}

		/*!
		 * \remarks 对向量进行旋转(Vector afterRotateVec3 = q * beforeRotateVec3)
		 * \return 
		 * \param const Vec3<T>& v
		*/
		Vec3<T> operator*(const Vec3<T>& v) const
		{
			Vec3<T> uv, uuv;
			Vec3<T> qvec(x, y, z);

			//crossProduct()没有进行规范化操作
			uv = qvec.crossProduct(v);

			uuv = qvec.crossProduct(uv);

			uv *= (2.0 * w);
			uuv *= 2.0;

			return v + uv + uuv;
		}

		/*!
		 * \remarks 对向量进行旋转(忽略Vector4.w)
		 * \return 
		 * \param const Vec4<T> & v
		*/
		Vec4<T> operator*(const Vec4<T>& v) const
		{
			Vec3<T> v3{v.x,v.y,v.z};

			auto rotatedV3 = (*this) * v3;

			return Vec4<T>{rotatedV3,v.w};
		}

		/*!
		 * \remarks 四元数 * 标量
		 * \return 
		 * \param T const& s
		*/
		Quater operator*(T const& s) const
		{
			return Quater<T>(s * w,s * x,s * y,s * z);
		}

		/*!
		 * \remarks 重载[]
		 * \return 
		 * \param const size_t i
		*/
		T operator[](const size_t i) const
		{
			BOOST_ASSERT(i < 4);

			return *(&w+i);
		}

		/*!
		 * \remarks 重载[]
		 * \return 
		 * \param const size_t i
		*/
		T& operator[](const size_t i)
		{
			BOOST_ASSERT(i < 4);

			return *(&w+i);
		}

		/*!
		 * \remarks 判断两个四元数是否相等
		 * \return 
		 * \param const Quater & rhs
		*/
		bool operator==(const Quater& q) const
		{
			
			return (math_type::isEqual(w, q.w) && 
				math_type::isEqual(x, q.x) && 
				math_type::isEqual(y, q.y) && 
				math_type::isEqual(z, q.z));
		}

		/*!
		 * \remarks 判断两个四元数是否不等
		 * \return 
		 * \param const Quater & rhs
		*/
		bool operator!=(const Quater& q) const
		{
			return !operator==(q);
		}

		/*!
		 * \remarks 交换
		 * \return 
		 * \param Quater& other
		*/
		void swap(Quater& other)
		{
			std::swap(w,other.w);
			std::swap(x,other.x);
			std::swap(y,other.y);
			std::swap(z,other.z);

			BOOST_ASSERT(isUnit());
		}

		/*!
		 * \remarks 点积(可用来计算两个单位四元数之间的夹角)
		 * \return 
		 * \param const Quater & q
		*/
		T dot(const Quater& q) const
		{
			return w * q.w + x * q.x + y * q.y + z * q.z;
		}

		/*!
		 * \remarks Slerp(Spherical linear interpolation(球面线性插值))(其中包括旋转性的线性插值)
		 * \return 
		 * \param const Quater & p 
		 * \param const Quater & q 
		 * \param T t 
		 * \param bool shortestPath 
		*/
		static Quater slerp(const Quater& p, const Quater& q, T t, bool shortestPath = false);

		/*!
		 * \remarks 球形二次差值(四元数样条)(未测试)
		 * \return 
		 * \param const Quater & rkP 
		 * \param const Quater & rkA 
		 * \param const Quater & rkB 
		 * \param const Quater & rkQ 
		 * \param T t 
		 * \param bool shortestPath 
		*/
		static Quater squad(const Quater& rkP,const Quater& rkA,const Quater& rkB,const Quater& rkQ,T t,bool shortestPath = false)
		{
			T slerpT = 2.0 * t * (1.0 - t);
			Quater slerpP = slerp(p, q, t, shortestPath);
			Quater slerpQ = slerp(a, b, t);

			return slerp(slerpP, slerpQ, slerpT);
		}

		/*!
		 * \remarks Computes the n-th power of the quaternion q(未测试)
		 * \return 
		 * \param Quater const & q 
		 * \param uint32 n 
		*/
		Quater pow(Quater const & q,uint32 n)
		{
			if (n > 1)
			{
				int m = n >> 1;

				Quater result = pow(q, m);

				result *= result;

				if (n != (m << 1))
				{
					result *= q;
				}

				return result;
			}
			else if (n == 1)
			{
				return q;
			}
			else if (n == 0)
			{
				return(Quater(static_cast<T>(1)));
			}
			else
			{
				return pow(Quater(static_cast<T>(1)) / q, -n);
			}
		}

		/*!
		 * \remarks 四元数的对数(未测试)
		 * \return 
		*/
		Quater log() const
		{
			Quater q;
			q.w = 0.0;

			if (math_type::calAbs(w) < 1.0)
			{
				Radian r(math_type::calACos(w));
				T sinValue = math_type::calSin(r);

				if (math_type::isZero(math_type::calAbs(sinValue)))
				{
					T invValue = r.getRadian() / sinValue;
					q.x = invValue * x;
					q.y = invValue * y;
					q.z = invValue * z;

					return q;
				}
			}

			q.x = x;
			q.y = y;
			q.z = z;

			return q;
		}

		static const Quater ZERO;
		static const Quater IDENTITY;

		T w,x,y,z;
	};

	template<typename T>
	const Quater<T> Quater<T>::ZERO(0, 0, 0, 0);

	/*
	单位四元数
	q和-q代表的实际角位移是相同的.如果我们将θ加上360度的倍数,不会改变q代表的角位移,但它使q的四个分量都变负了.因此,3D中的任意角位移都
	有两种不同的四元数表示方法,它们互相为负.
	几何上,存在两个"单位"四元数,它们代表没有角位移:[1,0]和[-1,0],其中的0代表零向量.当θ是TPI的偶数倍时,有第一种形式,cos(θ / 2) = 1;
	θ是TPI的奇数倍时,有第二种形式,cos(θ / 2) = -1.在两种情况下,都有sin(θ / 2) = 0,所以n的值无关紧要.所以说,当旋转角θ是TPI的整数倍
	时,方位并没有改变,并且旋转轴也是无关紧要的.
	数学上,实际只有一个单位四元数:[1,0].用任意四元数q乘以单位四元数[1,0],结果仍是q.任意四元数q乘以另一个"几何单位"四元数[-1,0]时得到
	-q.几何上,因为q和-q代表的角位移相同,可以认为结果是相同的.但在数学上,q和-q不相等,所以[-1,0]并不是"真正"的单位四元数.
	*/
	template<typename T>
	const Quater<T> Quater<T>::IDENTITY(1, 0, 0, 0);

	template<typename T>
	Quater<T> Quater<T>::slerp(const Quater& p, const Quater& q, T t,bool shortestPath)
	{
		/*
		Slerp(A,B,1.0) = A
		Slerp(A,B,0.0) = B
		恒定速度,并且扭矩最小(除非shortestPath为false)
		不可交换:
		Slerp(A,B,0.75) != Slerp(B,A,0.25);
		*/

		//要求p和q都是单位四元数
		BOOST_ASSERT(p.isUnit());
		BOOST_ASSERT(q.isUnit());

		BOOST_ASSERT(t >= 0.0 && t <= 1.0);

		T cosValue = q.dot(p);
		Quater ret;

		if (cosValue < 0.0 && shortestPath)
		{
			cosValue = -cosValue;
			ret = -p;
		}
		else
		{
			ret = p;
		}

		if (math_type::calAbs(cosValue) < 1 - Consts<T>::ZERO)
		{
			T sinValue = math_type::calSqrt(1 - cosValue * cosValue);
			//atan2返回的反正切值y/x在范围-PI到PI弧度。
			T alpha = std::atan2(sinValue, cosValue);
			T invSinValue = 1.0 / sinValue;
			T k0 = math_type::calSin((1.0 - t) * alpha) * invSinValue;
			T k1 = math_type::calSin(t * alpha) * invSinValue;

			return k0 * q + k1 * ret;
		}
		else
		{
			/*
			p和q很接近,做线性插值.(fCos ~= +1)
			p和q几乎inverse,有无限多个可能的差值,这里只是做线性插值.
			*/
			Quater r = (1.0 - t) * q + t * ret;

			//旋转性的线性插值LERP运算一般来说并不保持矢量长度
			r.normalize();

			return r;
		}
	}
}//namespace ung

#endif//UML_HIERARCHICAL_COMPILE

#endif//_UML_QUATER_H_