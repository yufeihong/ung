/*!
 * \brief
 * 场景管理器。通过插件来支持。场景管理器，管理的就是对象。
 * 支持空间一致性和渲染器状态一致性。
 * 客户代码包含这个头文件。
 * \file USM.h
 *
 * \author Su Yang
 *
 * \date 2016/11/17
 */
#ifndef _USM_H_
#define _USM_H_

#ifndef _USM_ENABLE_H_
#include "USMEnable.h"
#endif

#ifdef USM_HIERARCHICAL_COMPILE

#ifndef _USM_INIT_H_
#include "USMInit.h"
#endif

#ifndef _USM_MANUALGEOMETRY_H_
#include "USMManualGeometry.h"
#endif

#ifndef _USM_OBJECT_H_
#include "USMObject.h"
#endif

#ifndef _USM_OBJECTCOMPONENT_H_
#include "USMObjectComponent.h"
#endif

#ifndef _USM_TRANSFORMCOMPONENT_H_
#include "USMTransformComponent.h"
#endif

#ifndef _USM_PICKUPCOMPONENT_H_
#include "USMPickupComponent.h"
#endif

#ifndef _USM_SPATIALNODEBASE_H_
#include "USMSpatialNodeBase.h"
#endif

#ifndef _USM_SCENENODE_H_
#include "USMSceneNode.h"
#endif

#ifndef _USM_SCENEMANAGERBASE_H_
#include "USMSceneManagerBase.h"
#endif

#ifndef _USM_SCENEMANAGER_H_
#include "USMSceneManager.h"
#endif

#ifndef _USM_SPATIALMANAGERBASE_H_
#include "USMSpatialManagerBase.h"
#endif

#ifndef _USM_SPATIALMANAGERFACTORYBASE_H_
#include "USMSpatialManagerFactoryBase.h"
#endif

#ifndef _USM_SPATIALMANAGERENUMERATOR_H_
#include "USMSpatialManagerEnumerator.h"
#endif

#ifndef _USM_SCENEQUERY_H_
#include "USMSceneQuery.h"
#endif

#ifndef _USM_CAMERA_H_
#include "USMCamera.h"
#endif

#endif//USM_HIERARCHICAL_COMPILE

#endif//_USM_H_