/*!
 * \brief
 * 场景对象组件。
 * \file USMObjectComponent.h
 *
 * \author Su Yang
 *
 * \date 2016/12/13
 */
#ifndef _USM_OBJECTCOMPONENT_H_
#define _USM_OBJECTCOMPONENT_H_

#ifndef _USM_CONFIG_H_
#include "USMConfig.h"
#endif

#ifdef USM_HIERARCHICAL_COMPILE

#ifndef _UIF_IOBJECTCOMPONENT_H_
#include "UIFIObjectComponent.h"
#endif

#ifndef _INCLUDE_TINYXML2_H_
#include "tinyxml2.h"
#define _INCLUDE_TINYXML2_H_
#endif

namespace ung
{
	class Object;

	/*!
	 * \brief
	 * 场景对象组件。(组件就是对feature的封装，组件通过XML中的数据来描述，这也表达了数据驱动。)
	 * \class ObjectComponent
	 *
	 * \author Su Yang
	 *
	 * \date 2016/12/13
	 *
	 * \todo
	 */
	class UngExport ObjectComponent : public IObjectComponent
	{
		BOOST_STATIC_ASSERT(boost::has_new_operator<ObjectComponent>::value);

		friend class ObjectFactory;

	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		ObjectComponent() = delete;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param String const & name
		*/
		ObjectComponent(String const& name);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~ObjectComponent();

		/*!
		 * \remarks 初始化组件
		 * \return 
		 * \param tinyxml2::XMLElement* pComponentElement
		*/
		virtual void init(tinyxml2::XMLElement* pComponentElement) override;

		/*!
		 * \remarks 对象组装完成后，对象可能会再根据其都由那些组件所组合的来决定是否再进行一些初始化工作。
		 * 而对象的后初始化工作，会调用组件的后初始化。
		 * \return 
		*/
		virtual void postInit() override;

		/*!
		 * \remarks 更新组件
		 * \return 
		*/
		virtual void update() override;

		/*!
		 * \remarks 获取组件的名字
		 * \return 
		*/
		virtual String const& getName() const override;

		/*!
		 * \remarks 设置该组件所属的对象
		 * \return 
		 * \param StrongIObjectPtr pOwner
		*/
		virtual void setOwner(StrongIObjectPtr pOwner) override;

	protected:
		//xml中的组件名字，约定为组件的类名
		String mName;
		//以便于组件之间的交流(设计为：对象和组件互相持有对方的强引用)
		StrongIObjectPtr mOwner;
	};
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE

#endif//_USM_OBJECTCOMPONENT_H_