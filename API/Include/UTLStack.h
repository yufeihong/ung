/*!
 * \brief
 * 栈。
 * \file UtilStack.h
 *
 * \author Su Yang
 *
 * \date 2016/02/18
 */
#ifndef _UNG_UTL_STACK_H_
#define _UNG_UTL_STACK_H_

#ifndef _UNG_UTL_CONFIG_H_
#include "UTLConfig.h"
#endif

namespace ung
{
	namespace utl
	{
		/*!
		 * \brief
		 * 栈。默认用vector实现。
		 * \class Stack
		 *
		 * \author Su Yang
		 *
		 * \date 2016/02/18
		 *
		 * \todo
		 */
		template<typename T,typename CONT = std::vector>
		class Stack
		{
		public:
			/*!
			 * \remarks 默认构造函数
			 * \return 
			*/
			Stack() = default;

			/*!
			 * \remarks 压入
			 * \return 
			 * \param T const & el
			*/
			void push(T const& el)
			{
				mCont.push_back(el);
			}

			/*!
			 * \remarks 弹出
			 * \return 
			*/
			void pop()
			{
				mCont.pop_back();
			}

			/*!
			 * \remarks 栈顶元素
			 * \return 
			*/
			T& top()
			{
				return mCont.back();
			}

			/*!
			 * \remarks 栈顶元素。常量对象调用
			 * \return 
			*/
			T const& top() const
			{
				return mCont.back();
			}

			/*!
			* \remarks 获取元素数量
			* \return
			*/
			usize size() const
			{
				return mCont.size();
			}

			/*!
			 * \remarks 是否为空
			 * \return 
			*/
			bool empty() const
			{
				return mCont.empty();
			}

			/*!
			 * \remarks 清空
			 * \return 
			*/
			void clear()
			{
				mCont.clear();
			}

		private:
			CONT<T> mCont;
		};
	}//namespace utl
}//namespace ung

#endif//_UNG_UTL_STACK_H_

/*
可以将vector，list或deque用作stack的底层容器。
如果在const对象上调用，top()方法返回顶部元素的const引用。如果在非const对象上调用，则返回非const引用。
pop()不返回弹出的元素。如果需要获得一份副本，必须首先通过top()获得这个元素。

把线性表的插入和删除操作限制在同一端进行，就得到栈数据结构。因此，栈是一个后进先出(last-in-first-out,LIFO)的数据结构。
计算机是如何执行递归函数的呢？答案是使用递归工作栈(recursion stack)。当一个函数被调用时，一个返回地址(即被调函数一旦执行完，接下去要执行的程序指令的地址)和被调函数的
局部变量和形参的值都要存储在递归工作栈中。当执行一次返回时，被调函数的局部变量和形参的值被恢复为调用之前的值(这些值存储在递归工作栈的顶部)，而且程序从返回地址处继续
执行，这个返回地址也存储在递归工作栈的顶部。

Stack是限制插入和删除只能在一个位置上进行的表，该位置是表的末端，叫做栈的顶(top)。栈顶的元素是唯一可见的元素。
当调用一个新函数时，主调例程的所有局部变量需要由系统存储起来，否则被调用的新函数将会覆盖调用例程的变量。不仅如此，该主调例程的当前位置必须要存储，以便在新函数运行完
后知道向哪里转移。

STL中的通用栈类实现为容器适配器：使用以指定方式运行的容器。
栈容器不是重新创建的，它只是对已有容器做适当的调整。
默认情况下，deque是底层容器，但是用户可以使用链表或向量。

stack允许新增元素，移除元素，取得最顶端元素。但除了最顶端外，没有任何其它方法可以存取stack的其它元素。换言之，stack不允许有遍历行为。
stack没有迭代器。stack所有元素的进出都必须符合“先进后出”的条件，只有stack顶端的元素，才有机会被外界取用。stack不提供走访功能，也不提供迭代器。
*/