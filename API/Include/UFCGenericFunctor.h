/*!
 * \brief
 * 泛化仿函数
 * \file UFCGenericFunctor.h
 *
 * \author Su Yang
 *
 * \date 2017/04/22
 */
#ifndef _UFC_GENERICFUNCTOR_H_
#define _UFC_GENERICFUNCTOR_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#ifndef _UFC_TYPELIST_H_
#include "UFCTypeList.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 用以将“函数调用行为”抽象化的多态接口
	 * \class GenericFunctorImpl
	 *
	 * \author Su Yang
	 *
	 * \date 2017/04/22
	 *
	 * \todo
	 */
	template<typename R,typename TL = utp::NullType>
	class GenericFunctorImpl
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		GenericFunctorImpl() = default;

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~GenericFunctorImpl() = default;

		/*!
		 * \remarks 重载函数调用运算符
		 * \return 
		*/
		virtual R operator()() = 0;

		/*!
		 * \remarks 产生GenericFunctorImpl对象的一份多态拷贝
		 * \return 
		*/
		virtual GenericFunctorImpl* clone() const = 0;
	};

	//偏特化
	template<typename R>
	class GenericFunctorImpl<R,utp::NullType>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		GenericFunctorImpl() = default;

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~GenericFunctorImpl() = default;

		/*!
		 * \remarks 重载函数调用运算符
		 * \return 
		*/
		virtual R operator()() = 0;

		/*!
		 * \remarks 产生GenericFunctorImpl对象的一份多态拷贝
		 * \return 
		*/
		virtual GenericFunctorImpl* clone() const = 0;
	};

	template<typename R,typename P1>
	class GenericFunctorImpl<R, TYPELIST_1(P1)>
	{
	public:
		GenericFunctorImpl() = default;

		virtual ~GenericFunctorImpl() = default;

		virtual R operator()(P1) = 0;

		virtual GenericFunctorImpl* clone() const = 0;
	};

	template<typename R, typename P1,typename P2>
	class GenericFunctorImpl<R, TYPELIST_2(P1,P2)>
	{
	public:
		GenericFunctorImpl() = default;

		virtual ~GenericFunctorImpl() = default;

		virtual R operator()(P1,P2) = 0;

		virtual GenericFunctorImpl* clone() const = 0;
	};

	template<typename R, typename P1, typename P2, typename P3>
	class GenericFunctorImpl<R, TYPELIST_3(P1, P2,P3)>
	{
	public:
		GenericFunctorImpl() = default;

		virtual ~GenericFunctorImpl() = default;

		virtual R operator()(P1, P2,P3) = 0;

		virtual GenericFunctorImpl* clone() const = 0;
	};

	template<typename R, typename P1, typename P2, typename P3,typename P4>
	class GenericFunctorImpl<R, TYPELIST_4(P1, P2, P3,P4)>
	{
	public:
		GenericFunctorImpl() = default;

		virtual ~GenericFunctorImpl() = default;

		virtual R operator()(P1, P2, P3,P4) = 0;

		virtual GenericFunctorImpl* clone() const = 0;
	};

	template<typename R, typename P1, typename P2, typename P3, typename P4,typename P5>
	class GenericFunctorImpl<R, TYPELIST_5(P1, P2, P3, P4,P5)>
	{
	public:
		GenericFunctorImpl() = default;

		virtual ~GenericFunctorImpl() = default;

		virtual R operator()(P1, P2, P3, P4,P5) = 0;

		virtual GenericFunctorImpl* clone() const = 0;
	};

	/*!
	 * \brief
	 * 为了实现以仿函数的对象来构造一个仿函数的功能
	 * \class GenericFunctorHandler
	 *
	 * \author Su Yang
	 *
	 * \date 2017/04/22
	 *
	 * \todo
	 */
	template<typename ParentFunctor,typename Fun>
	class GenericFunctorHandler : public ParentFunctor::impl_type,public MemoryPool<GenericFunctorHandler<ParentFunctor,Fun>>
	{
	public:
		using result_type = typename ParentFunctor::result_type;

		using parm1_type = typename ParentFunctor::parm1_type;
		using parm2_type = typename ParentFunctor::parm2_type;
		using parm3_type = typename ParentFunctor::parm3_type;
		using parm4_type = typename ParentFunctor::parm4_type;
		using parm5_type = typename ParentFunctor::parm5_type;

		GenericFunctorHandler(Fun const& fun) :
			mFun(fun)
		{
		}

		GenericFunctorHandler* clone() const
		{
			return UNG_NEW GenericFunctorHandler(*this);
		}

		result_type operator()()
		{
			return mFun();
		}

		result_type operator()(parm1_type p1)
		{
			return mFun(p1);
		}

		result_type operator()(parm1_type p1,parm2_type p2)
		{
			return mFun(p1,p2);
		}

		result_type operator()(parm1_type p1, parm2_type p2,parm3_type p3)
		{
			return mFun(p1, p2,p3);
		}

		result_type operator()(parm1_type p1, parm2_type p2, parm3_type p3,parm4_type p4)
		{
			return mFun(p1, p2, p3,p4);
		}

		result_type operator()(parm1_type p1, parm2_type p2, parm3_type p3, parm4_type p4,parm5_type p5)
		{
			return mFun(p1, p2, p3, p4,p5);
		}

	private:
		Fun mFun;
	};

	template <class ParentFunctor, typename PointerToObj, typename PointerToMemFunction>
	class MemberFunctorHandler : public ParentFunctor::impl_type,public MemoryPool<MemberFunctorHandler<ParentFunctor, PointerToObj, PointerToMemFunction>>
	{
	public:
		using result_type = typename ParentFunctor::result_type;

		using parm1_type = typename ParentFunctor::parm1_type;
		using parm2_type = typename ParentFunctor::parm2_type;
		using parm3_type = typename ParentFunctor::parm3_type;
		using parm4_type = typename ParentFunctor::parm4_type;
		using parm5_type = typename ParentFunctor::parm5_type;

		MemberFunctorHandler(const PointerToObj& pObj, PointerToMemFunction pMemFn) :
			mPointerToObj(pObj),
			mPointerToMemFunction(pMemFn)
		{
		}

		MemberFunctorHandler* clone() const
		{
			return UNG_NEW MemberFunctorHandler(*this);
		}

		result_type operator()()
		{
			return ((*mPointerToObj).*mPointerToMemFunction)();
		}

		result_type operator()(parm1_type p1)
		{
			return ((*mPointerToObj).*mPointerToMemFunction)(p1);
		}

		result_type operator()(parm1_type p1,parm2_type p2)
		{
			return ((*mPointerToObj).*mPointerToMemFunction)(p1, p2);
		}

		result_type operator()(parm1_type p1, parm2_type p2,parm3_type p3)
		{
			return ((*mPointerToObj).*mPointerToMemFunction)(p1, p2,p3);
		}

		result_type operator()(parm1_type p1, parm2_type p2,parm3_type p3,parm4_type p4)
		{
			return ((*mPointerToObj).*mPointerToMemFunction)(p1, p2,p3,p4);
		}

		result_type operator()(parm1_type p1, parm2_type p2,parm3_type p3,parm4_type p4,parm5_type p5)
		{
			return ((*mPointerToObj).*mPointerToMemFunction)(p1, p2,p3,p4,p5);
		}

	private:
		PointerToObj mPointerToObj;
		PointerToMemFunction mPointerToMemFunction;
	};

	/*!
	 * \brief
	 * 泛化仿函数
	 * \class GenericFunctor
	 *
	 * \author Su Yang
	 *
	 * \date 2017/04/22
	 *
	 * \todo
	 */
	template<typename R,typename TL = utp::NullType>
	class GenericFunctor
	{
	public:
		using result_type = R;
		using parm_type_list = TL;
		using self_type = GenericFunctor<result_type, parm_type_list>;
		using impl_type = GenericFunctorImpl<result_type, parm_type_list>;

		using parm1_type = typename utp::ParameterType<typename utp::TypeAtNonStrict<parm_type_list, 0,utp::EmptyType>::result>::type;
		using parm2_type = typename utp::ParameterType<typename utp::TypeAtNonStrict<parm_type_list, 1,utp::EmptyType>::result>::type;
		using parm3_type = typename utp::ParameterType<typename utp::TypeAtNonStrict<parm_type_list, 2,utp::EmptyType>::result>::type;
		using parm4_type = typename utp::ParameterType<typename utp::TypeAtNonStrict<parm_type_list, 3,utp::EmptyType>::result>::type;
		using parm5_type = typename utp::ParameterType<typename utp::TypeAtNonStrict<parm_type_list, 4,utp::EmptyType>::result>::type;

		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		GenericFunctor() = default;

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~GenericFunctor()
		{
			UNG_DEL(mImpl);
		}

		/*!
		 * \remarks 拷贝构造函数
		 * \return 
		 * \param GenericFunctor const & other
		*/
		GenericFunctor(GenericFunctor const& other)
		{
			mImpl = other.mImpl->clone();
		}

		/*!
		 * \remarks 拷贝赋值运算符
		 * \return 
		 * \param GenericFunctor const & other
		*/
		GenericFunctor& operator=(GenericFunctor const& other)
		{
			if (this != &other)
			{
				UNG_DEL(mImpl);
				mImpl = other.mImpl->clone();
			}

			return *this;
		}

		explicit GenericFunctor(impl_type* impl) :
			mImpl(impl)
		{
		}

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param Fun const & fun 以仿函数Fun的对象为参数(包括普通函数指针)
		*/
		template<typename Fun>
		GenericFunctor(Fun const& fun) :
			mImpl(UNG_NEW GenericFunctorHandler<self_type,Fun>(fun))
		{
		}

		/*!
		 * \remarks 通过成员函数指针来构造一个仿函数
		 * \return 
		 * \param const PointerToObj & pObj 指向成员对象的指针
		 * \param PointerToMemFunction pMemFunc 指向成员函数的指针
		*/
		template <typename PointerToObj, typename PointerToMemFunction>
		GenericFunctor(const PointerToObj& pObj, PointerToMemFunction pMemFunc) :
			mImpl(UNG_NEW MemberFunctorHandler<self_type,PointerToObj,PointerToMemFunction>(pObj, pMemFunc))
		{
		}

		/*!
		 * \remarks 转发至GenericFunctorImpl::operator()
		 * \return 
		*/
		result_type operator()()
		{
			return (*mImpl)();
		}

		result_type operator()(parm1_type p1)
		{
			return (*mImpl)(p1);
		}

		result_type operator()(parm1_type p1,parm2_type p2)
		{
			return (*mImpl)(p1,p2);
		}

		result_type operator()(parm1_type p1, parm2_type p2, parm3_type p3)
		{
			return (*mImpl)(p1, p2,p3);
		}

		result_type operator()(parm1_type p1, parm2_type p2, parm3_type p3, parm4_type p4)
		{
			return (*mImpl)(p1, p2,p3,p4);
		}

		result_type operator()(parm1_type p1, parm2_type p2, parm3_type p3, parm4_type p4, parm5_type p5)
		{
			return (*mImpl)(p1, p2,p3,p4,p5);
		}

	private:
		impl_type* mImpl;
	};
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_GENERICFUNCTOR_H_