/*!
 * \brief
 * 解析器
 * \file UFCParser.h
 *
 * \author Su Yang
 *
 * \date 2016/05/16
 */
#ifndef _UFC_PARSER_H_
#define _UFC_PARSER_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#ifndef _INCLUDE_BOOST_MAPPEDREGION_HPP_
#include "boost/interprocess/mapped_region.hpp"
#define _INCLUDE_BOOST_MAPPEDREGION_HPP_
#endif

#ifndef _INCLUDE_BOOST_INIPARSER_HPP_
#include "boost/property_tree/ini_parser.hpp"
#define _INCLUDE_BOOST_INIPARSER_HPP_
#endif

#ifndef _INCLUDE_BOOST_INFOPARSER_HPP_
#include "boost/property_tree/info_parser.hpp"
#define _INCLUDE_BOOST_INFOPARSER_HPP_
#endif

#ifndef _INCLUDE_BOOST_JSONPARSER_HPP_
#pragma warning(disable:4715)		//json_parser.hpp:不是所有的控件路径都返回值
#include "boost/property_tree/json_parser.hpp"
#define _INCLUDE_BOOST_JSONPARSER_HPP_
#endif

#ifndef _INCLUDE_BOOST_XMLPARSER_HPP_
#include "boost/property_tree/xml_parser.hpp"
#define _INCLUDE_BOOST_XMLPARSER_HPP_
#endif

#ifndef _UFC_CHOOSECONTAINER_H_
#include "UFCChooseContainer.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 文本解析器
	 * \class Parser
	 *
	 * \author Su Yang
	 *
	 * \date 2016/05/16
	 *
	 * \todo
	 */
	class UngExport Parser : boost::noncopyable,public MemoryPool<Parser>
	{
	public:
		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param String const & fullName 文件全名
		*/
		Parser(String const& fullName);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~Parser();

		/*!
		 * \remarks 获取文件全名
		 * \return 
		*/
		virtual String const& getFileFullName() const;

		/*!
		 * \remarks 获取文件名
		 * \return 
		*/
		virtual String const& getFileName() const;

		/*!
		 * \remarks 获取文件的扩展名
		 * \return 
		*/
		virtual String const& getExtension() const;

		/*!
		 * \remarks 获取文件内容的字节数
		 * \return 
		*/
		virtual usize const getSize() const;

		/*!
		 * \remarks 获取指向文件内容的char指针
		 * \return 
		*/
		virtual const char* getSource() const;

	protected:
		String mFullName;
		String mFileName;
		String mExtension;
		usize mSize;
		char* mSource;
		boost::interprocess::mapped_region mRegion;
	};//class Parser

	/*!
	 * \brief
	 * property tree(属性树)解析器
	 * \class PropertyTreeParser
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/10
	 *
	 * \todo
	 */
	class PropertyTreeParser : public Parser
	{
	public:
		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param String const & fileName 文件全名
		*/
		PropertyTreeParser(String const& fileName);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~PropertyTreeParser();

		/*!
		 * \remarks 用于key-value
		 * \return 
		 * \param String const & key
		 * \param String & value
		*/
		void get(String const& key, String& value);

		/*!
		 * \remarks 用于key-value
		 * \return 
		 * \param char const * key
		 * \param String & value
		*/
		void get(char const* key, String& value);

		/*!
		 * \remarks 用于key-value
		 * \return 
		 * \param char const * key
		 * \param int & value
		*/
		void get(char const* key, int& value);

		/*!
		 * \remarks 用于key-values
		 * \return 
		 * \param String const & key
		 * \param STL_VECTOR(String) & vec
		*/
		void get(String const& key, STL_VECTOR(String)& vec);

		/*
		用于读取如下结构：
		<Materials>
			<Material>
				<Name></Name>
				<Ambient></Ambient>
				<Diffuse></Diffuse>
				<Specular></Specular>
				<Shininess></Shininess>
				<ShininessStrength></ShininessStrength>
				<Transparency></Transparency>
				<BlendMode></BlendMode>
				<TwoSide></TwoSide>
				<TextureCount></TextureCount>
			</Material>
			<Material>
				<Name></Name>
				<Ambient></Ambient>
				<Diffuse></Diffuse>
				<Specular></Specular>
				<Shininess></Shininess>
				<ShininessStrength></ShininessStrength>
				<Transparency></Transparency>
				<BlendMode></BlendMode>
				<TwoSide></TwoSide>
				<TextureCount></TextureCount>
			</Material>
		</Materials>
		*/
		void get_custom(String const& key, STL_VECTOR(String) const& keyVec,STL_VECTOR(STL_VECTOR(String))& values);

		/*
		用于读取如下结构：
		<Materials>
			<Material>
				<Textures>
				<Texture></Texture>
				</Textures>
			</Material>
			<Material>
				<Textures>
				<Texture></Texture>
				</Textures>
			</Material>
		</Materials>
		*/
		void get_custom(String const& key1, String const& key2,STL_VECTOR(STL_VECTOR(String))& values);

		/*
		用于读取如下结构：
		<Meshs>
			<Mesh>
				<SubMeshs>
					<SubMesh>
						<MaterialName></MaterialName>
						<TrianglesMode></TrianglesMode>
						<FaceCount></FaceCount>
						<VertexCount></VertexCount>
						<IndexCount></IndexCount>
					</SubMesh>
				</SubMeshs>
			</Mesh>
		</Meshs>
		*/
		void get_custom(String const& key1, String const& key2,STL_VECTOR(String) const& keyVec,STL_VECTOR(STL_VECTOR(String))& values);

	private:
		void read(std::istringstream& input);

	protected:
		boost::property_tree::ptree mTree;
	};//class PropertyTreeParser
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_PARSER_H_