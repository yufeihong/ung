/*!
 * \brief
 * 内存池
 * \file UMMPool.h
 *
 * \author Su Yang
 *
 * \date 2016/04/10
 */
#ifndef _UMM_POOL_H_
#define _UMM_POOL_H_

#ifndef _UMM_CONFIG_H_
#include "UMMConfig.h"
#endif

#ifdef UMM_HIERARCHICAL_COMPILE

#ifndef _UMM_ALLOCATOR_H_
#include "UMMAllocator.h"
#endif

#ifndef _UMM_MACRO_H_
#include "UMMMacro.h"
#endif

#ifndef _UMM_BIGDOG_H_
#include "UMMBigDog.h"
#endif

namespace ung
{
	/*!
	* \brief
	* 动态内存分配策略
	* \class MemoryPool
	*
	* \author Su Yang
	*
	* \date 2016/04/11
	*
	* \todo
	*/
	template<typename T>
	class MemoryPool
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		MemoryPool() = default;

		/*
		如果基类的析构函数不是虚函数，则delete一个指向派生类对象的基类指针将产生未定义的行为。
		很明显，假如存在：
		class Cat : public MemoryPool<Cat>
		{
			...
		};
		我们不会存在这样的代码：
		MemoryPool<Cat>* p = new Cat;
		delete p;
		*/
		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~MemoryPool() = default;

		//重载new和delete运算符。
		UMM_OVERRIDE_NEW_DELETE;
	};//end class MemoryPool
}//namespace ung

#endif//UMM_HIERARCHICAL_COMPILE

#endif//_UMM_POOL_H_