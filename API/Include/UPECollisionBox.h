/*!
 * \brief
 * 碰撞box。
 * \file UPECollisionBox.h
 *
 * \author Su Yang
 *
 * \date 2016/12/03
 */
#ifndef _UPE_COLLISIONBOX_H_
#define _UPE_COLLISIONBOX_H_

#ifndef _UPE_CONFIG_H_
#include "UPEConfig.h"
#endif

#ifdef UPE_HIERARCHICAL_COMPILE

namespace ung
{
	/*!
	 * \brief
	 * Collision Box
	 * \class CollisionBox
	 *
	 * \author Su Yang
	 *
	 * \date 2016/12/03
	 *
	 * \todo
	 */
	class CollisionBox : public btBoxShape
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		CollisionBox() = delete;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param Vector3 const & boxHalfExtents
		*/
		CollisionBox(Vector3 const& boxHalfExtents);

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param real_type halfW
		 * \param real_type halfH
		 * \param real_type halfZ
		*/
		CollisionBox(real_type halfW,real_type halfH,real_type halfZ);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~CollisionBox();
	};
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

#endif//_UPE_COLLISIONBOX_H_