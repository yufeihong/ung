/*!
 * \brief
 * 类型特征的“榨汁机”。
 * \file UTLTypeTraits.h
 *
 * \author Su Yang
 *
 * \date 2016/01/28
 */
#ifndef _UNG_UTL_TYPETRAITS_H_
#define _UNG_UTL_TYPETRAITS_H_

#ifndef _INCLUDE_STLIB_TYPETRAITS_
#include <type_traits>
#define _INCLUDE_STLIB_TYPETRAITS_
#endif

namespace ung
{
	namespace utl
	{
		/*!
		 * \remarks 获取迭代器的分类
		 * \return 
		 * \param Iter const &
		*/
		template<typename Iter>
		inline typename std::iterator_traits<Iter>::iterator_category Iter_cat(Iter const&)
		{
			typename std::iterator_traits<Iter>::iterator_category cat;

			return cat;
		}

		/*!
		 * \brief
		 * 选择函数，接受一个pair，传回其第一元素
		 * \class select1st
		 *
		 * \author Su Yang
		 *
		 * \date 2016/04/06
		 *
		 * \todo
		 */
		template<typename Pair>
		struct select1st
		{
			const typename Pair::first_type& operator()(Pair const& x) const
			{
				return x.first;
			}
		};
	}//namespace utl
}//namespace ung

#endif//_UNG_UTL_TYPETRAITS_H_