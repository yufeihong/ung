#ifndef _USM_SCENEMANAGERDEF_H_
#define _USM_SCENEMANAGERDEF_H_

#ifndef _USM_SCENEMANAGER_H_
#include "USMSceneManager.h"
#endif

#ifdef USM_HIERARCHICAL_COMPILE

#ifndef _USM_OBJECT_H_
#include "USMObject.h"
#endif

#ifndef _USM_SCENENODE_H_
#include "USMSceneNode.h"
#endif

#ifndef _USM_OBJECTFACTORY_H_
#include "USMObjectFactory.h"
#endif

#ifndef _USM_SPATIALMANAGERENUMERATOR_H_
#include "USMSpatialManagerEnumerator.h"
#endif

namespace ung
{
	const String rootSceneNodeName{ "rootSceneNode" };

	template<SceneType ST>
	SceneManager<ST>::SceneManager(String const& name) :
		mName(name.empty() ? UniqueID().toString() : name),
		mObjectPool(ObjectPool<object_aggregates_type>("SceneManager_mObjectPool")),
		mSceneNodePool(ObjectPool<scenenode_aggregates_type>("SceneManager_mSceneNodePool")),
		mSceneRootNode(UNG_NEW_SMART SceneNode(WeakSceneNodePtr{}, rootSceneNodeName)),
		mObjectFactory(UNG_NEW_SMART ObjectFactory)
	{
		//存储场景根节点
		mSceneNodePool.save(rootSceneNodeName, mSceneRootNode);

		//更新场景根节点
		mSceneRootNode->update();

		//根据场景类型，创建空间管理器
		mSpatialManager = SpatialManagerEnumerator::getInstance().createSpatialManager(ST);
	}

	template<SceneType ST>
	SceneManager<ST>::~SceneManager()
	{
		//销毁该场景的空间管理器
		SpatialManagerEnumerator::getInstance().destroySpatialManager(mSpatialManager);
	}

	//-------------------------------------------------------------------------------------------

	template<SceneType ST>
	String const & ung::SceneManager<ST>::getName() const
	{
		return mName;
	}

	template<SceneType ST>
	SceneType SceneManager<ST>::getSceneType() const
	{
		return ST;
	}

	template<SceneType ST>
	StrongISceneNodePtr SceneManager<ST>::getSceneRootNode() const
	{
		BOOST_ASSERT(mSceneRootNode);

		return mSceneRootNode;
	}

	//-------------------------------------------------------------------------------------------

	template<SceneType ST>
	StrongIObjectPtr SceneManager<ST>::createObject(String const& objectDataFileFullName, String const& objectName)
	{
		//通过对象工厂来创建具体的对象
		auto objectPtr = mObjectFactory->createObject(shared_from_this(),objectDataFileFullName, objectName);

		//将对象存放到有场景管理器所持有的对象池中
		mObjectPool.save(objectPtr->getName(), objectPtr);

		return objectPtr;
	}

	template<SceneType ST>
	void SceneManager<ST>::destroyObject(String const& objectName,bool remove)
	{
		//获取对象
		auto objectPtr = getObject(objectName);

		//如果该对象不存在
		if (!objectPtr)
		{
			return;
		}

		destroyObject(objectPtr,remove);
	}

	template<SceneType ST>
	void SceneManager<ST>::destroyObject(StrongIObjectPtr objectPtr,bool remove)
	{
		BOOST_ASSERT(objectPtr);

		/*
		对象的强引用分别被各种ObjectComponent，SceneNode和SceneManager持有。
		所以要销毁一个对象时，必须先将对象和其所关联组件进行脱离，然后和其所关联的场景节点进行脱离。
		*/

		//和场景节点脱离(先于和组件脱离，先于和空间管理器节点脱离)
		objectPtr->detachFromSceneNode(remove);

		//和空间管理器节点脱离(必须在和场景节点脱离后再调用这个函数)
		objectPtr->adjustmentHangInSpatial();

		//和场景管理器脱离
		mObjectPool.remove(objectPtr->getName());

		//和组件脱离
		objectPtr->destroy();

		/*
		至此，所有对该对象的强引用已经释放，从而触发该对象的析构函数。
		而如果在通过场景管理器销毁对象的局部作用域中存在对该对象的强引用，那么该对象的析构函数
		会在代码离开这个局部作用域后触发。
		*/
	}

	template<SceneType ST>
	StrongIObjectPtr SceneManager<ST>::getObject(String const& objectName)
	{
		return mObjectPool.getStrong(objectName);
	}

	template<SceneType ST>
	uint32 SceneManager<ST>::getAllObjectCount() const
	{
		return mObjectPool.size();
	}

	template<SceneType ST>
	uint32 SceneManager<ST>::getAllSceneNodeCount() const
	{
		return mSceneNodePool.size();
	}

	template<SceneType ST>
	StrongISceneNodePtr SceneManager<ST>::createSceneNode(WeakISceneNodePtr parentPtr,String const& nodeName)
	{
		//在这里创建子节点的时候，已经设置了子节点的父节点
		StrongISceneNodePtr childPtr(UNG_NEW_SMART SceneNode(parentPtr,nodeName));
		BOOST_ASSERT(childPtr);

		//把上面创建的子节点存储为父节点的孩子节点
		parentPtr.lock()->saveChild(childPtr);

		//把上面创建的子节点存储到场景管理器的场景节点池中
		mSceneNodePool.save(childPtr->getName(), childPtr);

		return childPtr;
	}

	template<SceneType ST>
	void SceneManager<ST>::destroySceneNode(String const& nodeName)
	{
		//得到这个场景节点的强引用
		auto nodePtr = mSceneNodePool.getStrong(nodeName);
		BOOST_ASSERT(nodePtr);

		//向下转换类型
		StrongSceneNodePtr concreteNode = std::static_pointer_cast<SceneNode>(nodePtr);

		/*
		先销毁直接和非直接的孩子节点(没有涉及到该节点本身)。
		遍历这个节点的所有直接和非直接的孩子节点。
		深度优先，后序遍历，先子后父遍历，遍历的同时销毁每个节点所关联的对象，然后销毁节点本身。
		也就是说，如果要销毁整个场景的话，只需要销毁掉场景根节点即可。
		*/

		//对于每一个节点，应用该算法
		auto fn1 = [this](StrongSceneNodePtr n)
		{
			//遍历该节点所关联的所有对象
			auto beg = n->mObjects.begin();
			auto end = n->mObjects.end();
			while (beg != end)
			{
				//对象
				auto objectPtr = (*beg).second;

				//不和节点脱离，以免迭代器失效而崩溃
				destroyObject(objectPtr, false);

				++beg;
			}

			//清空该节点所持有的所有直接对象
			n->mObjects.clear();

			//清空该节点所持有的所有直接孩子
			n->mChildren.clear();

			/*
			这里如果调用SceneNode::detachFromParent()让该节点脱离其父节点的话，就会从父节点
			用来保存其直接孩子节点的容器中移除掉一个元素，这样就会导致容器的迭代器失效，进而会造
			成遍历算法崩溃。
			*/

			/*
			从场景管理器中移除对该节点的弱引用，虽然是弱引用，但是必须移除，否则无法释放掉容器中
			元素本身所占用的空间，这跟元素为弱引用无关。
			*/
			mSceneNodePool.remove(n->getName());
		};

		concreteNode->postTraverseDepthFirst(fn1);

		/*
		至此，所有直接与非直接对象已经与组件脱离，与场景管理器脱离。
		所有直接与非直接孩子节点已经与场景管理器脱离。
		*/

		//专门处理当前节点
		auto beg = concreteNode->mObjects.begin();
		auto end = concreteNode->mObjects.end();
		while (beg != end)
		{
			//对象
			auto objectPtr = (*beg).second;

			//不和节点脱离，以免迭代器失效而崩溃
			destroyObject(objectPtr, false);

			++beg;
		}

		//清空该节点所持有的所有直接对象
		concreteNode->mObjects.clear();
		//清空该节点所持有的所有直接孩子
		concreteNode->mChildren.clear();
		mSceneNodePool.remove(concreteNode->getName());
		//该节点和其父节点脱离
		concreteNode->detachFromParent();
	}

	template<SceneType ST>
	void SceneManager<ST>::destroySceneNode(StrongISceneNodePtr nodePtr)
	{
		BOOST_ASSERT(nodePtr);

		destroySceneNode(nodePtr->getName());
	}

	template<SceneType ST>
	ISpatialManager * SceneManager<ST>::getSpatialManager() const
	{
		BOOST_ASSERT(mSpatialManager);

		return mSpatialManager;
	}

	template<SceneType ST>
	StrongISceneQueryPtr SceneManager<ST>::createRaySceneQuery(Ray const & ray)
	{
		return mSpatialManager->createRaySceneQuery(ray);
	}

	template<SceneType ST>
	StrongISceneQueryPtr SceneManager<ST>::createSphereSceneQuery(Sphere const & sphere)
	{
		return mSpatialManager->createSphereSceneQuery(sphere);
	}

	template<SceneType ST>
	StrongISceneQueryPtr SceneManager<ST>::createAABBSceneQuery(AABB const & box)
	{
		return mSpatialManager->createAABBSceneQuery(box);
	}

	template<SceneType ST>
	StrongISceneQueryPtr SceneManager<ST>::createCameraSceneQuery(Camera const & camera)
	{
		return mSpatialManager->createCameraSceneQuery(camera);
	}
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE

#endif//_USM_SCENEMANAGERDEF_H_