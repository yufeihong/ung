/*!
 * \brief
 * 四边形
 * \file UPEQuadrilateral.h
 *
 * \author Su Yang
 *
 * \date 2017/04/11
 */
#ifndef _UPE_QUADRILATERAL_H_
#define _UPE_QUADRILATERAL_H_

#ifndef _UPE_CONFIG_H_
#include "UPEConfig.h"
#endif

#ifdef UPE_HIERARCHICAL_COMPILE

#ifndef _UPE_GEOMETRY_H_
#include "UPEGeometry.h"
#endif

namespace ung
{
	class Triangle;

	/*!
	 * \brief
	 * 四边形
	 * \class Quadrilateral
	 *
	 * \author Su Yang
	 *
	 * \date 2017/04/11
	 *
	 * \todo
	 */
	class UngExport Quadrilateral : Geometry
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		Quadrilateral() = delete;

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~Quadrilateral() = default;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param Vector3 const & a 逆时针环绕顺序，共面
		 * \param Vector3 const & b
		 * \param Vector3 const & c
		 * \param Vector3 const & d
		*/
		Quadrilateral(Vector3 const& a, Vector3 const& b, Vector3 const& c, Vector3 const& d);

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param Triangle const & triangle
		 * \param Vector3 const & p 与三角形共面的一点
		*/
		Quadrilateral(Triangle const& triangle, Vector3 const& p);

		/*!
		 * \remarks 获取四边形的四个顶点位置
		 * \return 
		*/
		std::array<Vector3, 4> const& getVertices() const;

	private:
		std::array<Vector3, 4> mVertices;
	};
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

#endif//_UPE_QUADRILATERAL_H_