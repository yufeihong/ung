/*!
 * \brief
 * ColorValue的定义头文件
 * \file UFCColorValueDef.h
 *
 * \author Su Yang
 *
 * \date 2017/06/01
 */
#ifndef _UFC_COLORVALUEDEF_H_
#define _UFC_COLORVALUEDEF_H_

#ifndef _UFC_COLORVALUE_H_
#include "UFCColorValue.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#pragma warning(push)

//从“double”转换到“float”
#pragma warning(disable : 4244)

namespace
{
	using namespace ung;

	//设计层次要做调整
	template<typename T>
	bool is_real_type_equal(T a, T b, T tolerance = 1e-004)
	{
		T diff = std::fabs(a - b);

		if (diff <= tolerance)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}//namespace

namespace ung
{
	template<typename T>
	ColorValue<T>::ColorValue(T rr, T gg, T bb, T aa) :
		r(rr),
		g(gg),
		b(bb),
		a(aa)
	{
	}

	template<typename T>
	ColorValue<T>::ColorValue(RGBA color)
	{
		setRGBA(color);
	}

	template<typename T>
	void ColorValue<T>::setRGBA(const RGBA val)
	{
		uint32 val32 = val;

		//Convert from 32bit pattern.(RGBA = 8888)

		//Red
		r = ((val32 >> 24) & 0xFF) / 255.0;

		//Green
		g = ((val32 >> 16) & 0xFF) / 255.0;

		//Blue
		b = ((val32 >> 8) & 0xFF) / 255.0;

		//Alpha
		a = (val32 & 0xFF) / 255.0;
	}

	template<typename T>
	void ColorValue<T>::setARGB(const ARGB val)
	{
		uint32 val32 = val;

		//Alpha
		a = ((val32 >> 24) & 0xFF) / 255.0;

		//Red
		r = ((val32 >> 16) & 0xFF) / 255.0;

		//Green
		g = ((val32 >> 8) & 0xFF) / 255.0;

		//Blue
		b = (val32 & 0xFF) / 255.0;
	}

	template<typename T>
	void ColorValue<T>::setBGRA(const BGRA val)
	{
		uint32 val32 = val;

		//Blue
		b = ((val32 >> 24) & 0xFF) / 255.0;

		//Green
		g = ((val32 >> 16) & 0xFF) / 255.0;

		//Red
		r = ((val32 >> 8) & 0xFF) / 255.0;

		//Alpha
		a = (val32 & 0xFF) / 255.0;
	}

	template<typename T>
	void ColorValue<T>::setABGR(const ABGR val)
	{
		uint32 val32 = val;

		//Alpha
		a = ((val32 >> 24) & 0xFF) / 255.0;

		//Blue
		b = ((val32 >> 16) & 0xFF) / 255.0;

		//Green
		g = ((val32 >> 8) & 0xFF) / 255.0;

		//Red
		r = (val32 & 0xFF) / 255.0;
	}

	template<typename T>
	RGBA ColorValue<T>::getRGBA() const
	{
		uint8 val8;
		uint32 val32 = 0;

		//Convert to 32bit pattern.(RGBA = 8888)

		//Red
		val8 = static_cast<uint8>(r * 255);
		val32 = val8 << 24;

		//Green
		val8 = static_cast<uint8>(g * 255);
		val32 += val8 << 16;

		//Blue
		val8 = static_cast<uint8>(b * 255);
		val32 += val8 << 8;

		//Alpha
		val8 = static_cast<uint8>(a * 255);
		val32 += val8;

		return val32;
	}

	template<typename T>
	ARGB ColorValue<T>::getARGB() const
	{
		uint8 val8;
		uint32 val32 = 0;

		//Alpha
		val8 = static_cast<uint8>(a * 255);
		val32 = val8 << 24;

		//Red
		val8 = static_cast<uint8>(r * 255);
		val32 += val8 << 16;

		//Green
		val8 = static_cast<uint8>(g * 255);
		val32 += val8 << 8;

		//Blue
		val8 = static_cast<uint8>(b * 255);
		val32 += val8;

		return val32;
	}

	template<typename T>
	BGRA ColorValue<T>::getBGRA() const
	{
		uint8 val8;
		uint32 val32 = 0;

		//Blue
		val8 = static_cast<uint8>(b * 255);
		val32 = val8 << 24;

		//Green
		val8 = static_cast<uint8>(g * 255);
		val32 += val8 << 16;

		//Red
		val8 = static_cast<uint8>(r * 255);
		val32 += val8 << 8;

		//Alpha
		val8 = static_cast<uint8>(a * 255);
		val32 += val8;

		return val32;
	}

	template<typename T>
	ABGR ColorValue<T>::getABGR() const
	{
		uint8 val8;
		uint32 val32 = 0;

		//Alpha
		val8 = static_cast<uint8>(a * 255);
		val32 = val8 << 24;

		//Blue
		val8 = static_cast<uint8>(b * 255);
		val32 += val8 << 16;

		//Green
		val8 = static_cast<uint8>(g * 255);
		val32 += val8 << 8;

		//Red
		val8 = static_cast<uint8>(r * 255);
		val32 += val8;

		return val32;
	}

	template<typename T>
	void ColorValue<T>::clamp()
	{
#define CLAMP_COLOR(c)						\
		if (c < 0.0)										\
		{													\
			c = 0.0;										\
		}													\
		else if (c > 1.0)								\
		{													\
			c = 1.0;										\
		}

		CLAMP_COLOR(r);
		CLAMP_COLOR(g);
		CLAMP_COLOR(b);
		CLAMP_COLOR(a);
	}

	template<typename T>
	ColorValue<T> ColorValue<T>::clampCopy() const
	{
		ColorValue cpy = *this;
		cpy.clamp();

		return cpy;
	}

	template<typename T>
	T ColorValue<T>::operator[](const size_t i) const
	{
		BOOST_ASSERT(i < 4);

		return *(&r + i);
	}

	template<typename T>
	T& ColorValue<T>::operator[](const size_t i)
	{
		BOOST_ASSERT(i < 4);

		return *(&r + i);
	}

	template<typename T>
	T* ColorValue<T>::ptr()
	{
		return &r;
	}

	template<typename T>
	const T* ColorValue<T>::ptr() const
	{
		return &r;
	}

	template<typename T>
	ColorValue<T> ColorValue<T>::operator+(const ColorValue& right) const
	{
		ColorValue sum;

		sum.r = r + right.r;
		sum.g = g + right.g;
		sum.b = b + right.b;
		sum.a = a + right.a;

		return sum;
	}

	template<typename T>
	ColorValue<T> ColorValue<T>::operator-(const ColorValue& right) const
	{
		ColorValue diff;

		diff.r = r - right.r;
		diff.g = g - right.g;
		diff.b = b - right.b;
		diff.a = a - right.a;

		return diff;
	}

	template<typename T>
	ColorValue<T> ColorValue<T>::operator*(const T s) const
	{
		ColorValue prod;

		prod.r = r * s;
		prod.g = g * s;
		prod.b = b * s;
		prod.a = a * s;

		return prod;
	}

	template<typename T>
	ColorValue<T> ColorValue<T>::operator*(const ColorValue& right) const
	{
		ColorValue prod;

		prod.r = r * right.r;
		prod.g = g * right.g;
		prod.b = b * right.b;
		prod.a = a * right.a;

		return prod;
	}

	template<typename T>
	ColorValue<T> ColorValue<T>::operator/(const ColorValue& right) const
	{
		ColorValue prod;

		prod.r = r / right.r;
		prod.g = g / right.g;
		prod.b = b / right.b;
		prod.a = a / right.a;

		return prod;
	}

	template<typename T>
	ColorValue<T> ColorValue<T>::operator/(const T s) const
	{
		BOOST_ASSERT(s != 0.0);

		ColorValue div;

		T inv = 1.0 / s;

		div.r = r * inv;
		div.g = g * inv;
		div.b = b * inv;
		div.a = a * inv;

		return div;
	}

	template<typename T>
	ColorValue<T>& ColorValue<T>::operator+=(const ColorValue& right)
	{
		r += right.r;
		g += right.g;
		b += right.b;
		a += right.a;

		return *this;
	}

	template<typename T>
	ColorValue<T>& ColorValue<T>::operator-=(const ColorValue& right)
	{
		r -= right.r;
		g -= right.g;
		b -= right.b;
		a -= right.a;

		return *this;
	}

	template<typename T>
	ColorValue<T>& ColorValue<T>::operator*=(const T s)
	{
		r *= s;
		g *= s;
		b *= s;
		a *= s;

		return *this;
	}

	template<typename T>
	ColorValue<T>& ColorValue<T>::operator/=(const T s)
	{
		BOOST_ASSERT(s != 0.0);

		T inv = 1.0 / s;

		r *= inv;
		g *= inv;
		b *= inv;
		a *= inv;

		return *this;
	}

	template<typename T>
	bool ColorValue<T>::operator==(const ColorValue& right) const
	{
#define REAL_EQUAL(a,b) is_real_type_equal(a,b)

		return (REAL_EQUAL(r,right.r) && REAL_EQUAL(g,right.g) && 
			REAL_EQUAL(b,right.b) && REAL_EQUAL(a,right.a));
	}

	template<typename T>
	bool ColorValue<T>::operator!=(const ColorValue& right) const
	{
		return !(*this == right);
	}
}//namespace ung

#pragma warning(pop)

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_COLORVALUEDEF_H_

/*
这种写法是为了兼容包含模型和显式实例化。
当使用包含模型时，include def.h文件
当使用显式实例化时，include .h文件
如果不需要这种兼容，可以把def文件和explicit inst .cpp文件合二为一。
*/