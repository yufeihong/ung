/*!
 * \brief
 * 可运动接口。
 * \file UIFIMovable.h
 *
 * \author Su Yang
 *
 * \date 2017/03/24
 */
#ifndef _UIF_IMOVABLE_H_
#define _UIF_IMOVABLE_H_

#ifndef _UIF_CONFIG_H_
#include "UIFConfig.h"
#endif

#ifdef UIF_HIERARCHICAL_COMPILE

namespace ung
{
	/*!
	 * \brief
	 * 可运动接口。
	 * \class IMovable
	 *
	 * \author Su Yang
	 *
	 * \date 2017/03/24
	 *
	 * \todo
	 */
	class UngExport IMovable : public virtual MemoryPool<IMovable>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		IMovable() = default;

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~IMovable() = default;

		/*!
		 * \remarks 拷贝构造函数
		 * \return 
		 * \param IMovable const &
		*/
		IMovable(IMovable const&) = default;

		/*!
		 * \remarks 拷贝赋值运算符
		 * \return 
		 * \param IMovable const &
		*/
		IMovable& operator=(IMovable const&) = default;

		/*!
		 * \remarks 移动构造函数
		 * \return 
		 * \param IMovable & &
		*/
		IMovable(IMovable &&) = default;

		/*!
		 * \remarks 移动赋值运算符
		 * \return 
		 * \param IMovable & &
		*/
		IMovable& operator=(IMovable &&) = default;

		/*!
		 * \remarks 设置更新标记
		 * \return 
		 * \param bool flag
		*/
		virtual void setNeedUpdateFlag(bool flag) = 0;

		/*!
		 * \remarks 获取更新标记
		 * \return 
		*/
		virtual bool getNeedUpdateFlag() const = 0;

		/*!
		 * \remarks 覆盖设置在模型空间的旋转
		 * \return 
		 * \param Vector3 const & axis
		 * \param Radian r 弧度
		*/
		virtual void setRotate(Vector3 const& axis,Radian r) = 0;

		/*!
		 * \remarks 覆盖设置在模型空间的旋转
		 * \return 
		 * \param Vector3 const & axis
		 * \param Degree d 度
		*/
		virtual void setRotate(Vector3 const& axis,Degree d) = 0;

		/*!
		 * \remarks 覆盖设置在模型空间的缩放
		 * \return 
		 * \param real_type s
		*/
		virtual void setScale(real_type s) = 0;

		/*!
		 * \remarks 覆盖设置在父空间的平移
		 * \return 
		 * \param real_type x
		 * \param real_type y
		 * \param real_type z
		*/
		virtual void setTranslate(real_type x, real_type y, real_type z) = 0;

		/*!
		 * \remarks 覆盖设置在父空间的平移
		 * \return 
		 * \param Vector3 t
		*/
		virtual void setTranslate(Vector3 t) = 0;

		/*!
		 * \remarks 累计在模型空间的旋转
		 * \return 
		 * \param Vector3 const & axis
		 * \param Radian r 弧度
		*/
		virtual void addUpRotate(Vector3 const& axis,Radian r) = 0;

		/*!
		 * \remarks 累计在模型空间的旋转
		 * \return 
		 * \param Vector3 const & axis
		 * \param Degree d 度
		*/
		virtual void addUpRotate(Vector3 const& axis,Degree d) = 0;

		/*!
		 * \remarks 累计在模型空间的缩放
		 * \return 
		 * \param real_type s
		*/
		virtual void addUpScale(real_type s) = 0;

		/*!
		 * \remarks 累计在父空间的平移
		 * \return 
		 * \param real_type x
		 * \param real_type y
		 * \param real_type z
		*/
		virtual void addUpTranslate(real_type x, real_type y, real_type z) = 0;

		/*!
		 * \remarks 累计在父空间的平移
		 * \return 
		 * \param Vector3 t
		*/
		virtual void addUpTranslate(Vector3 t) = 0;

		/*!
		 * \remarks 获取从模型空间到父空间的变换
		 * \return 
		*/
		virtual SQT const& getToParentSQT() const = 0;

		/*!
		 * \remarks 获取从模型空间到父空间的变换
		 * \return 
		*/
		virtual Matrix4 getToParentMatirx() const = 0;

		/*!
		 * \remarks 获取从模型空间到世界空间的变换
		 * \return 
		*/
		virtual SQT const& getToWorldSQT() const = 0;

		/*!
		 * \remarks 获取从模型空间到世界空间的变换
		 * \return 
		*/
		virtual Matrix4 getToWorldMatrix() const = 0;

		/*!
		 * \remarks 获取从模型空间到世界空间的变换的逆变换
		 * \return 
		*/
		virtual SQT getToWorldSQTInverse() const = 0;

		/*!
		 * \remarks 获取从模型空间到世界空间的变换的逆变换
		 * \return 
		*/
		virtual Matrix4 getToWorldMatrixInverse() const = 0;

		/*!
		 * \remarks 获取在父空间的位置
		 * \return 
		*/
		virtual Vector4 getInParentPosition() = 0;

		/*!
		 * \remarks 获取在父空间的朝向
		 * \return 
		*/
		virtual Vector3 getInParentOrientation() = 0;

		/*!
		 * \remarks 获取在父空间的缩放
		 * \return 
		*/
		virtual real_type getInParentScale() = 0;

		/*!
		 * \remarks 获取在世界空间的位置
		 * \return 
		*/
		virtual Vector4 getInWorldPosition() = 0;

		/*!
		 * \remarks 获取在世界空间的朝向
		 * \return 
		*/
		virtual Vector3 getInWorldOrientation() = 0;

		/*!
		 * \remarks 获取在世界空间的缩放
		 * \return 
		*/
		virtual real_type getInWorldScale() = 0;

		/*!
		 * \remarks 更新到世界空间的变换
		 * \return 
		*/
		virtual void update() = 0;

		/*!
		 * \remarks 设置world矩阵
		 * \return 
		 * \param SQT const & world
		*/
		virtual void setWorld(SQT const& world) = 0;
	};
}//namespace ung

#endif//UIF_HIERARCHICAL_COMPILE

#endif//_UIF_IMOVABLE_H_

/*
约定：
在模型空间中，中心点：(0,0,0)，朝向为：(0,0,1)，缩放为：1

空间：
1，模型空间
旋转，缩放 -> localSQT
2，父空间
平移 -> localSQT
3，世界空间
worldSQT = parentSQT * localSQT
位置 = modelCenter * worldSQT
朝向 = modelOrientation * worldSQT
缩放 = worldSQT.mScale
*/