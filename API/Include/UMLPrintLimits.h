/*!
 * \brief
 * 打印极值。
 * \file UMLPrintLimits.h
 *
 * \author Su Yang
 *
 * \date 2016/06/15
 */
#ifndef _UML_PRINTLIMITS_H_
#define _UML_PRINTLIMITS_H_

#ifndef _UML_CONFIG_H_
#include "UMLConfig.h"
#endif

#ifdef UML_HIERARCHICAL_COMPILE

#ifndef _INCLUDE_STLIB_IOMANIP_
#include <iomanip>
#define _INCLUDE_STLIB_IOMANIP_
#endif

namespace ung
{
	/*
	输出补白：
	当按列打印数据时，我们常常需要非常精细地控制数据格式。
	标准库提供了一些操纵符帮助我们完成所需的控制：
	setw，指定下一个数字或字符串值的最小空间。
	left，表示左对齐输出。
	right，表示右对齐输出，右对齐是默认格式。
	internal，控制负数的符号的位置，它左对齐符号，右对齐值，用空格填满所有中间空间。
	setfill，允许指定一个字符代替默认的空格来补白输出。

	hex，十六进制
	dec，十进制
	*/
	template<class T>
	void printRow(const char* name, T val, bool ok = true)
	{
		std::cout << std::left << std::setw(10) << name << std::right;
		std::cout << std::hex << std::setfill('0');

		if (ok)
		{
			//x86系列则采用little endian方式存储数据
			for (size_t i = sizeof(T) - 1; i < sizeof(T); --i)
			{
				unsigned char c = *(reinterpret_cast<unsigned char*>(&val) + i);
				std::cout << std::setw(2) << static_cast<unsigned int>(c) << ' ';
			}
		}
		else
		{
			for (size_t i = 0; i < sizeof(T); ++i)
			{
				std::cout << "-- ";
			}
		}

		std::cout << std::endl;
		std::cout << std::dec << std::setfill(' ');
	}

	/*
	is_specialized，类型是否有极值
	is_signed，类型带有正负号
	is_bounded，数值集的个数有限（对所有内建类型而言，此成员均为true）
	min，最小有限值（只有当is_bounded || !is_signed成立才有意义）
	max，最大有限值（只有当is_bounded成立才有意义）
	denorm_min，最小正值的denormalized value(非标准化的值)
	has_denorm，类型是否允许denormalized value（亦即variable numbers of exponent bits）
	infinity，表现“正无穷大”（如果有的话）
	has_infinity，类型有“正无穷大”表述
	quiet_NaN，安静表达“Not a Number”，如果能够的话
	has_quiet_NaN，类型拥有nonsignaling “Not a Number”表述
	signaling_NaN，Signaling “Not a Number”，如果能够的话
	has_signaling_NaN，类型拥有signaling “Not a Number”表述形式
	*/
	template<class T>
	void printTable()
	{
		printRow("0", (T)0);
		printRow("sn.min", std::numeric_limits<T>::denorm_min(), std::numeric_limits<T>::has_denorm);
		printRow("-sn.min", -std::numeric_limits<T>::denorm_min(), std::numeric_limits<T>::has_denorm);
		printRow("n.min/256", (std::numeric_limits<T>::min)() / 256);
		printRow("n.min/2", (std::numeric_limits<T>::min)() / 2);
		printRow("-n.min/2", -(std::numeric_limits<T>::min)() / 2);
		printRow("n.min", (std::numeric_limits<T>::min)());
		printRow("1", (T)1);
		printRow("3/4", (T)3 / (T)4);
		printRow("4/3", (T)4 / (T)3);
		printRow("max", (std::numeric_limits<T>::max)());
		printRow("inf", std::numeric_limits<T>::infinity(), std::numeric_limits<T>::has_infinity);
		printRow("q.nan", std::numeric_limits<T>::quiet_NaN(), std::numeric_limits<T>::has_quiet_NaN);
		printRow("s.nan", std::numeric_limits<T>::signaling_NaN(), std::numeric_limits<T>::has_signaling_NaN);

		std::cout << std::endl;
	}
}//namespace ung

#endif//UML_HIERARCHICAL_COMPILE

#endif//_UML_PRINTLIMITS_H_

/*
打印结果：
如图PrintLimits所示
*/