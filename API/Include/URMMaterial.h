/*!
 * \brief
 * 对材质的一个封装，支持多线程安全。
 * \file URMMaterial.h
 *
 * \author Su Yang
 *
 * \date 2016/11/13
 */
#ifndef _URM_MATERIAL_H_
#define _URM_MATERIAL_H_

#ifndef _URM_CONFIG_H_
#include "URMConfig.h"
#endif

#ifdef URM_HIERARCHICAL_COMPILE

#ifndef _URM_RESOURCE_H_
#include "URMResource.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 对材质的一个封装，支持多线程安全。
	 * \class Material
	 *
	 * \author Su Yang
	 *
	 * \date 2016/11/13
	 *
	 * \todo
	 */
	class Material : public Resource,public std::enable_shared_from_this<Material>
	{
	public:
		/*!
		 * \remarks 创建具体资源的回调函数
		 * \return 
		 * \param String const & fullName 包含路径
		*/
		static std::shared_ptr<Resource> creatorCallback(String const& fullName);

		/*!
		 * \remarks 默认构造函数。载入一个blank材质
		 * \return 
		*/
		Material();

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param String const & fullName 必须包含路径
		*/
		Material(String const& fullName);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~Material();

		/*!
		 * \remarks 把load任务送入后台线程
		 * \return 
		*/
		virtual void build() override;

		/*!
		 * \remarks 获取环境光数据
		 * \return 
		*/
		Vector3 const& getAmbient() const;

		/*!
		 * \remarks 获取漫反射光数据
		 * \return 
		*/
		Vector3 const& getDiffuse() const;

		/*!
		 * \remarks 获取高光数据
		 * \return 
		*/
		Vector3 const& getSpecular() const;

		/*!
		 * \remarks 获取反射度
		 * \return 
		*/
		real_type const& getShininess() const;

		/*!
		 * \remarks 获取反射度强度
		 * \return 
		*/
		real_type const& getShininessStrength() const;

		/*!
		 * \remarks 获取透明数据
		 * \return 
		*/
		real_type const& getTransparency() const;

		/*!
		 * \remarks 获取混合模式
		 * \return 
		*/
		String const& getBlendMode() const;

		/*!
		 * \remarks 获取图元正反面数据
		 * \return 
		*/
		bool const& getTwoSide() const;

		/*!
		 * \remarks 获取纹理贴图的数量
		 * \return 
		*/
		uint8 const& getTextureCount() const;

		/*!
		 * \remarks 获取所有纹理贴图的名字
		 * \return 
		 * \param String
		*/
		STL_VECTOR(String) const& getTextureNames() const;

	private:
		/*!
		 * \remarks 设置后台线程的future
		 * \return 
		 * \param std::future<bool> & fut
		*/
		void setFuture(std::future<bool>& fut);

		/*!
		 * \remarks 载入
		 * \return 
		*/
		bool load();

		/*!
		 * \remarks 判断是否已经载入
		 * \return 
		*/
		bool isLoaded() const;

	private:
		Vector3 mAmbient;
		Vector3 mDiffuse;
		Vector3 mSpecular;
		real_type mShininess;
		real_type mShininessStrength;
		real_type mTransparency;
		String mBlendMode;
		bool mTwoSide = false;
		uint8 mTextureCount = 0;
		STL_VECTOR(String) mTextureNames;
		mutable std::recursive_mutex mRecursiveMutex;
		mutable std::future<bool> mNakedFuture;
		mutable bool mIsLoaded = false;
	};
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

#endif//_URM_MATERIAL_H_

//对类似资源的载入，应该把后台载入的code给抽象成为一个loader出来，谁需要载入行为，就给谁一个loader。