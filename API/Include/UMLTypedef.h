/*!
 * \brief
 * 数学库的类型别名。
 * \file UMLTypedef.h
 *
 * \author Su Yang
 *
 * \date 2016/12/03
 */
#ifndef _UML_TYPEDEF_H_
#define _UML_TYPEDEF_H_

#ifndef _UML_CONFIG_H_
#include "UMLConfig.h"
#endif

#ifdef UML_HIERARCHICAL_COMPILE

namespace ung
{
	template<typename T>
	class Consts;

	template<typename T>
	class MathImpl;

	template<typename T>
	class Vec2;

	template<typename T>
	class Vec3;

	template<typename T>
	class Vec4;

	template<typename T>
	class Mat2;

	template<typename T>
	class Mat3;

	template<typename T>
	class Mat4;

	template<typename T>
	class Quater;

	template<typename T>
	class SQTImpl;

	using Constants = Consts<real_type>;
	using Math = MathImpl<real_type>;
	using Vector2 = Vec2<real_type>;
	using Vector3 = Vec3<real_type>;
	using Vector4 = Vec4<real_type>;
	using Matrix2 = Mat2<real_type>;
	using Matrix3 = Mat3<real_type>;
	using Matrix4 = Mat4<real_type>;
	using Quaternion = Quater<real_type>;
	using SQT = SQTImpl<real_type>;
}//namespace ung

#endif//UML_HIERARCHICAL_COMPILE

#endif//_UML_TYPEDEF_H_