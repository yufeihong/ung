/*!
 * \brief
 * 空间管理器接口。
 * \file UIFISpatialManager.h
 *
 * \author Su Yang
 *
 * \date 2017/03/27
 */
#ifndef _UIF_ISPATIALMANAGER_H_
#define _UIF_ISPATIALMANAGER_H_

#ifndef _UIF_CONFIG_H_
#include "UIFConfig.h"
#endif

#ifdef UIF_HIERARCHICAL_COMPILE

namespace ung
{
	class Camera;

	/*!
	 * \brief
	 * 空间管理器基类
	 * \class ISpatialManager
	 *
	 * \author Su Yang
	 *
	 * \date 2017/03/27
	 *
	 * \todo
	 */
	class UngExport ISpatialManager : public MemoryPool<ISpatialManager>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		ISpatialManager() = default;

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~ISpatialManager() = default;

		/*!
		 * \remarks 获取创建该空间管理器的工厂
		 * \return 
		*/
		virtual ISpatialManagerFactory* getCreator() const = 0;

		/*!
		 * \remarks 获取空间管理器工厂的标识
		 * \return 
		*/
		virtual String const& getFactoryIdentify() const = 0;

		/*!
		 * \remarks 获取空间管理器工厂的掩码
		 * \return 
		*/
		virtual SceneTypeMask getFactoryMask() const = 0;

		/*!
		 * \remarks 获取空间管理器的名字
		 * \return 
		*/
		virtual String const& getManagerName() const = 0;

		/*!
		 * \remarks 创建节点
		 * \return 
		 * \param ISpatialNode * parent
		*/
		virtual ISpatialNode* createNode(ISpatialNode* parent) = 0;

		/*!
		 * \remarks 销毁节点
		 * \return 
		 * \param ISpatialNode * node
		*/
		virtual void destroyNode(ISpatialNode* node) = 0;

		/*!
		 * \remarks 获取空间树的根节点
		 * \return 
		*/
		virtual ISpatialNode* getRootNode() const = 0;

		/*!
		 * \remarks 将对象放置到空间管理器中
		 * \return 返回对象所关联的节点
		 * \param StrongIObjectPtr objectPtr
		*/
		virtual ISpatialNode* placeObject(StrongIObjectPtr objectPtr) = 0;

		/*!
		 * \remarks 将对象从空间管理器中拿走
		 * \return 
		 * \param StrongIObjectPtr objectPtr
		*/
		virtual void takeAwayObject(StrongIObjectPtr objectPtr) = 0;

		/*!
		 * \remarks 创建射线场景查询
		 * \return 
		 * \param Ray const & ray
		*/
		virtual StrongISceneQueryPtr createRaySceneQuery(Ray const& ray) = 0;

		/*!
		 * \remarks 创建球体场景查询
		 * \return 
		 * \param Sphere const & sphere
		*/
		virtual StrongISceneQueryPtr createSphereSceneQuery(Sphere const& sphere) = 0;

		/*!
		 * \remarks 创建AABB场景查询
		 * \return 
		 * \param AABB const & box
		*/
		virtual StrongISceneQueryPtr createAABBSceneQuery(AABB const& box) = 0;

		/*!
		 * \remarks 创建摄像机场景查询
		 * \return 
		 * \param Camera const & camera
		*/
		virtual StrongISceneQueryPtr createCameraSceneQuery(Camera const& camera) = 0;
	};
}//namespace ung

#endif//UIF_HIERARCHICAL_COMPILE

#endif//_UIF_ISPATIALMANAGER_H_