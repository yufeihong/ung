/*!
 * \brief
 * UTL的配置文件。
 * \file UTLConfig.h
 *
 * \author Su Yang
 *
 * \date 2016/01/27
 */
#ifndef _UNG_UTL_CONFIG_H_
#define _UNG_UTL_CONFIG_H_

#ifndef _INCLUDE_BOOST_TYPETRAITS_HPP_
#include "boost/type_traits.hpp"
#define _INCLUDE_BOOST_TYPETRAITS_HPP_
#endif

#ifndef _INCLUDE_BOOST_ITERATORFACADE_HPP_
#include "boost/iterator/iterator_facade.hpp"
#define _INCLUDE_BOOST_ITERATORFACADE_HPP_
#endif

#ifndef _INCLUDE_BOOST_REVERSEITERATOR_HPP_
#include "boost/iterator/reverse_iterator.hpp"
#define _INCLUDE_BOOST_REVERSEITERATOR_HPP_
#endif

#ifndef _INCLUDE_BOOST_NEXTPRIOR_HPP_
#include "boost/next_prior.hpp"
#define _INCLUDE_BOOST_NEXTPRIOR_HPP_
#endif

#ifndef _INCLUDE_BOOST_CONCEPTCHECK_HPP_
#include "boost/concept_check.hpp"
#define _INCLUDE_BOOST_CONCEPTCHECK_HPP_
#endif

#ifndef _INCLUDE_BOOST_ITERATORCONCEPTS_HPP_
#include "boost/iterator/iterator_concepts.hpp"
#define _INCLUDE_BOOST_ITERATORCONCEPTS_HPP_
#endif

#ifndef _INCLUDE_BOOST_ENABLEIF_HPP_
#include "boost/utility/enable_if.hpp"
#define _INCLUDE_BOOST_ENABLEIF_HPP_
#endif

#ifndef _INCLUDE_BOOST_RANGE_HPP_
#include "boost/range.hpp"
#define _INCLUDE_BOOST_RANGE_HPP_
#endif

#ifndef _INCLUDE_BOOST_HASTYPE_HPP_
#include "boost/tti/has_type.hpp"
#define _INCLUDE_BOOST_HASTYPE_HPP_
#endif

#ifndef _INCLUDE_BOOST_MOVECORE_HPP_
#include "boost/move/core.hpp"																				//BOOST_MOVABLE_BUT_NOT_COPYABLE(TYPE)
#define _INCLUDE_BOOST_MOVECORE_HPP_
#endif

#ifndef _INCLUDE_BOOST_MPLIF_HPP_
#include "boost/mpl/if.hpp"
#define _INCLUDE_BOOST_MPLIF_HPP_
#endif

#ifndef _INCLUDE_BOOST_INDIRECTITERATOR_HPP_
#include "boost/iterator/indirect_iterator.hpp"
#define _INCLUDE_BOOST_INDIRECTITERATOR_HPP_
#endif

#ifndef _INCLUDE_BOOST_SMARTPTR_HPP_
#include "boost/smart_ptr.hpp"
#define _INCLUDE_BOOST_SMARTPTR_HPP_
#endif

#ifndef _INCLUDE_BOOST_ISCONVERTIBLE_HPP_
#include "boost/type_traits/is_convertible.hpp"
#define _INCLUDE_BOOST_ISCONVERTIBLE_HPP_
#endif

#ifndef _INCLUDE_BOOST_HASH_HPP_
#include "boost/functional/hash.hpp"
#define _INCLUDE_BOOST_HASH_HPP_
#endif

//#include <iterator>

#ifndef _INCLUDE_STLIB_MEMORY_
#include <memory>
#define _INCLUDE_STLIB_MEMORY_
#endif

#ifndef _INCLUDE_STLIB_SCOPEDALLOCATOR_
#include <scoped_allocator>
#define _INCLUDE_STLIB_SCOPEDALLOCATOR_
#endif

#ifndef _INCLUDE_STLIB_ALGORITHM_
#include <algorithm>
#define _INCLUDE_STLIB_ALGORITHM_
#endif

#ifndef _INCLUDE_STLIB_TYPETRAITS_
#include <type_traits>
#define _INCLUDE_STLIB_TYPETRAITS_
#endif

#pragma warning(disable : 4308)

#ifdef _DEBUG
#define UTL_DEBUGMODE 1
#else
#define UTL_DEBUGMODE 0
#endif

namespace ung
{
	namespace utl
	{
		//精确宽度
		using int8 = boost::int8_t;					//有符号的8位整数
		using uint8 = boost::uint8_t;				//无符号的8位整数
		using int16 = boost::int16_t;				//有符号的16位整数
		using uint16 = boost::uint16_t;			//无符号的16位整数
		using int32 = boost::int32_t;				//有符号的32位整数
		using uint32 = boost::uint32_t;			//无符号的32位整数
		using int64 = boost::int64_t;				//有符号的64位整数
		using uint64 = boost::uint64_t;			//无符号的64位整数

		//最少具有这么多位，并且是CPU处理速度最快的类型
		using intfast8 = boost::int_fast8_t;
		using uintfast8 = boost::uint_fast8_t;
		using intfast16 = boost::int_fast16_t;
		using uintfast16 = boost::uint_fast16_t;
		using intfast32 = boost::int_fast32_t;
		using uintfast32 = boost::uint_fast32_t;
		using intfast64 = boost::int_fast64_t;
		using uintfast64 = boost::uint_fast64_t;

		//字：一个字为两个字节。(通过无符号的整数定义)
		using word16 = boost::uint16_t;
		using word32 = boost::uint32_t;
		using dword = boost::uint32_t;

		//编译器支持的最大整数
		using int_max = boost::intmax_t;
		using uint_max = boost::uintmax_t;

#ifdef _WIN64
		using usize = uint64;
		using ssize = int64;
		using ufast = uintfast64;
		using sfast = intfast64;
#else
		using usize = uint32;
		using ssize = int32;
		using ufast = uintfast32;
		using sfast = intfast32;
#endif
	}//namespace utl
}//namespace ung
#endif//_UNG_UTL_CONFIG_H_