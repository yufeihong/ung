/*!
 * \brief
 * 基础类库。
 * 客户代码包含这个头文件。
 * \file UFC.h
 *
 * \author Su Yang
 *
 * \date 2016/04/20
 */
#ifndef _UFC_H_
#define _UFC_H_

#ifndef _UFC_ENABLE_H_
#include "UFCEnable.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#ifndef _UFC_FSTRING_H_
#include "UFCFString.h"
#endif

#ifndef _UFC_STRINGUTILITIES_H_
#include "UFCStringUtilities.h"
#endif

#ifndef _UFC_DELETE_H_
#include "UFCDelete.h"
#endif

#ifndef _UFC_NUMERICCAST_H_
#include "UFCNumericCast.h"
#endif

#ifndef _UFC_POLYMORPHICCAST_H_
#include "UFCPolymorphicCast.h"
#endif

#ifndef _UFC_EXCEPTION_H_
#include "UFCException.h"
#endif

#ifndef _UFC_NUMBOUNDS_H_
#include "UFCNumBounds.h"
#endif

#ifndef _UFC_LOGMANAGER_H_
#include "UFCLogManager.h"
#endif

#ifndef _UFC_FILESYSTEM_H_
#include "UFCFilesystem.h"
#endif

#ifndef _UFC_TIMER_H_
#include "UFCTimer.h"
#endif

#ifndef _UFC_PERFORMANCE_H_
#include "UFCPerformance.h"
#endif

#ifndef _UFC_CHOOSECONTAINER_H_
#include "UFCChooseContainer.h"
#endif

#ifndef _UFC_PARSER_H_
#include "UFCParser.h"
#endif

#ifndef _UFC_UNIQUEID_H_
#include "UFCUniqueID.h"
#endif

#ifndef _UFC_ABSTRACTFACTORY_H_
#include "UFCAbstractFactory.h"
#endif

#ifndef _UFC_GENERICOBJECTFACTORY_H_
#include "UFCGenericObjectFactory.h"
#endif

#ifndef _UFC_PLUGIN_H_
#include "UFCPlugin.h"
#endif

#ifndef _UFC_PLUGINMANAGER_H_
#include "UFCPluginManager.h"
#endif

#ifndef _UFC_STREAMAUX_H_
#include "UFCStreamAux.h"
#endif

#ifndef _UFC_FILTER_H_
#include "UFCFilter.h"
#endif

#ifndef _UFC_STREAM_H_
#include "UFCStream.h"
#endif

#ifndef _UFC_PACk_H_
#include "UFCPack.h"
#endif

#ifndef _UFC_UNPACKHELPERINFO_H_
#include "UFCUnpackHelperInfo.h"
#endif

#ifndef _UFC_UNPACK_H_
#include "UFCUnpack.h"
#endif

#ifndef _UFC_OBJECTPOOL_H_
#include "UFCObjectPool.h"
#endif

#ifndef _UFC_UTILITIES_H_
#include "UFCUtilities.h"
#endif

#ifndef _UFC_METAFUNCTION_H_
#include "UFCMetaFunction.h"
#endif

#ifndef _UFC_GENERICFUNCTOR_H_
#include "UFCGenericFunctor.h"
#endif

//#ifndef _UFC_COLORVALUE_H_
//#include "UFCColorValue.h"
//#endif

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_H_