/*!
 * \brief
 * 通用物理接口基类。
 * \file UPEBase.h
 *
 * \author Su Yang
 *
 * \date 2017/03/31
 */
#ifndef _UPE_BASE_H_
#define _UPE_BASE_H_

#ifndef _UPE_CONFIG_H_
#include "UPEConfig.h"
#endif

#ifdef UPE_HIERARCHICAL_COMPILE

#ifndef _UPE_INTERFACE_H_
#include "UPEInterface.h"
#endif

namespace ung
{
	//临时
	class WeakObjectPtr
	{};

	/*!
	 * \brief
	 * 通用物理接口基类
	 * \class PhysicsBase
	 *
	 * \author Su Yang
	 *
	 * \date 2017/03/31
	 *
	 * \todo
	 */
	class PhysicsBase : public IPhysics
	{
	public:
		PhysicsBase() = default;

		virtual ~PhysicsBase() = default;

		virtual void initialize() override
		{
		}

		virtual void update() override
		{
		}

		virtual void addSphere(real_type radius, WeakObjectPtr object) override
		{
		}

		virtual void addBox(Vector3 const& min, Vector3 const& max, WeakObjectPtr object) override
		{
		}
	};
}

#endif//UPE_HIERARCHICAL_COMPILE

#endif//_UPE_BASE_H_