/*!
 * \brief
 * 物理库。
 * 客户代码包含这个头文件。
 * \file UPE.h
 *
 * \author Su Yang
 *
 * \date 2016/11/24
 */
#ifndef _UPE_H_
#define _UPE_H_

#ifndef _UPE_ENABLE_H_
#include "UPEEnable.h"
#endif

#ifdef UPE_HIERARCHICAL_COMPILE

#ifndef _UPE_UTILITIES_H_
#include "UPEUtilities.h"
#endif

#ifndef _UPE_MOVABLE_H_
#include "UPEMovable.h"
#endif

#ifndef _UPE_TRANSFORMATIONHOOK_H_
#include "UPETransformationHook.h"
#endif

#ifndef _UPE_COLLISIONBOX_H_
#include "UPECollisionBox.h"
#endif

#ifndef _UPE_RIGIDBODY_H_
#include "UPERigidBody.h"
#endif

#ifndef _UPE_DEBUGDRAWER_H_
#include "UPEDebugDrawer.h"
#endif

#ifndef _UPE_GEOMETRY_H_
#include "UPEGeometry.h"
#endif

#ifndef _UPE_LINESEGMENT_H_
#include "UPELineSegment.h"
#endif

#ifndef _UPE_TRIANGLE_H_
#include "UPETriangle.h"
#endif

#ifndef _UPE_QUADRILATERAL_H_
#include "UPEQuadrilateral.h"
#endif

#ifndef _UPE_RAY_H_
#include "UPERay.h"
#endif

#ifndef _UPE_SPHERE_H_
#include "UPESphere.h"
#endif

#ifndef _UPE_AABB_H_
#include "UPEAABB.h"
#endif

#ifndef _UPE_PLANE_H_
#include "UPEPlane.h"
#endif

#ifndef _UPE_PLANEBOUNDEDVOLUME_H_
#include "UPEPlaneBoundedVolume.h"
#endif

#ifndef _UPE_CAPSULE_H_
#include "UPECapsule.h"
#endif

#ifndef _UPE_LOZENGE_H_
#include "UPELozenge.h"
#endif

#ifndef _UPE_CONE_H_
#include "UPECone.h"
#endif

#ifndef _UPE_CYLINDER_H_
#include "UPECylinder.h"
#endif

#ifndef _UPE_RECTANGLE_H_
#include "UPERectangle.h"
#endif

#ifndef _UPE_COLLISIONDETECTION_H_
#include "UPECollisionDetection.h"
#endif

#endif//UPE_HIERARCHICAL_COMPILE

#endif//_UPE_H_