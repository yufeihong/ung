/*!
 * \brief
 * 对图像的封装。
 * \file URMImage.h
 *
 * \author Su Yang
 *
 * \date 2016/11/13
 */
#ifndef _URM_IMAGE_H_
#define _URM_IMAGE_H_

#ifndef _URM_CONFIG_H_
#include "URMConfig.h"
#endif

#ifdef URM_HIERARCHICAL_COMPILE

#ifndef _URM_RESOURCE_H_
#include "URMResource.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 对图像的一个封装。
	 * \class Image
	 *
	 * \author Su Yang
	 *
	 * \date 2016/11/13
	 *
	 * \todo
	 */
	class Image : public Resource,public std::enable_shared_from_this<Image>
	{
	public:
		enum ImageFileType : uint8
		{
			IFT_UNKNOWN,IFT_BMP,IFT_PNG,IFT_DDS,IFT_TGA,IFT_JPEG,IFT_XPM,IFT_ICO,IFT_TIFF,IFT_PSD,IFT_GIF,IFT_HDR,IFT_RAW
		};

		enum PixelBitType : uint8
		{
			PBT_UNKNOWN,
			PBT_BITMAP,										//standard image:1,4,8,16,24,32 bit
			PBT_UINT16,										//unsigned 16 bit
			PBT_INT16,										//signed 16 bit
			PBT_UINT32,										//unsigned 32 bit
			PBT_INT32,										//signed 32 bit
			PBT_FLOAT,										//32 bit IEEE floating point
			PBT_DOUBLE,									//64 bit IEEE floating point
			PBT_COMPLEX,									//2 x 64 bit IEEE floating point
			PBT_RGB16,										//3 x 16 bit
			PBT_RGBA16,										//4 x 16 bit
			PBT_RGBF,											//3 x 32 bit IEEE floating point
			PBT_RGBAF										//4 x 32 bit IEEE floating point
		};

		enum ColorType : uint8
		{
			CT_UNKNOWN,CT_RGB, CT_RGBA
		};

		/*!
		 * \remarks 创建具体资源的回调函数
		 * \return 
		 * \param String const & fullName 包含路径
		*/
		static std::shared_ptr<Resource> creatorCallback(String const& fullName);

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param String const& fullName 包含路径
		*/
		Image(String const& fullName);

		/*!
		 * \remarks 移动构造函数
		 * \return 
		 * \param Image&& im
		*/
		Image(Image&& im) noexcept;

		/*!
		 * \remarks 移动赋值运算符
		 * \return 
		 * \param Image&& im
		*/
		Image& operator=(Image&& im) noexcept;

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~Image();

		/*!
		 * \remarks 把load任务递送至线程池
		 * \return 
		*/
		virtual void build() override;

		/*!
		 * \remarks 获取image文件的类型
		 * \return 
		*/
		ImageFileType getImageFileType();

		/*!
		 * \remarks 获取image的像素类型
		 * \return 
		*/
		PixelBitType getPixelBitType();

		/*!
		 * \remarks 获取颜色类型
		 * \return 
		*/
		ColorType getColorType();

		/*!
		 * \remarks 载入，默认从Memory加载(UNG负责从磁盘到内存的映射)
		 * \return 
		 * \param bool fromMemory false,从File加载
		*/
		bool load(bool fromMemory = true);

		/*!
		 * \remarks 让Image对象持有future
		 * \return 
		 * \param std::future<bool>& fut
		*/
		void setFuture(std::future<bool>& fut);

		/*!
		 * \remarks 是否已经载入
		 * \return 
		*/
		bool isLoaded();

		/*!
		 * \remarks 存储
		 * \return 
		 * \param ImageFileType fileType 目标文件类型
		 * \param String const & fileName 目标文件名
		*/
		void save(ImageFileType fileType,String const& fileName);

		/*!
		 * \remarks 获取一个像素的bits
		 * \return 
		*/
		uint8 getBPP();

		/*!
		 * \remarks 获取image的宽度，单位为像素
		 * \return the width of the bitmap in pixel units.
		*/
		usize getWidthInPixels();

		/*!
		 * \remarks 获取image的高度，单位为像素
		 * \return the height of the bitmap in pixel units.
		*/
		usize getHeightInPixels();

		/*!
		 * \remarks 获取image的宽度，单位为字节
		 * \return the width of the bitmap in bytes.
		*/
		usize getWidthInBytes();

		/*!
		 * \remarks 获取一个像素的大小，单位为字节
		 * \return the number of bytes per pixel(3 for 24-bit or 4 for 32-bit)
		*/
		uint8 getPixelSizeInBytes();

		/*!
		 * \remarks 获取image的宽度(rounded to the next 32-bit boundary)，单位为字节
		 * \return 
		*/
		usize getPitchInBytes();

		/*!
		 * \remarks image是否含有alpha通道
		 * \return 
		*/
		bool isTransparent();

		/*!
		 * \remarks 获取指向data bits的指针
		 * \return 
		*/
		uint8* getData();

		/*!
		 * \remarks 水平翻转
		 * \return 
		*/
		void flipHorizontal();

		/*!
		 * \remarks 垂直翻转
		 * \return 
		*/
		void flipVertical();

	private:
		/*!
		 * \remarks 从磁盘载入
		 * \return 
		*/
		void loadFromFile();

		/*!
		 * \remarks 从内存载入
		 * \return 
		*/
		void loadFromMemory();

	private:
		class Impl;
		std::shared_ptr<Impl> mImpl;																		//std::unique_ptr要求完全类
		mutable std::recursive_mutex mRecursiveMutex;												//保护mImpl
		std::future<bool> mNakedFuture;
	};
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

#endif//_URM_IMAGE_H_

/*
用法：
1：主线程：创建一个Image imageObject。
2：主线程：imageObject.build(true/false);
3：主线程：imageObject.isLoaded();
isLoaded()越晚调用越好，否则太早调用，会产生没必要的block。
*/