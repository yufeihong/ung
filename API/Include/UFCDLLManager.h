///*!
//* \brief
//* DLL管理器
//* \file PILDLLManager.h
//*
//* \author Su Yang
//*
//* \date 2016/05/15
//*/
//
//#ifndef _UNG_PIL_DLLMANAGER_H_
//#define _UNG_PIL_DLLMANAGER_H_
//
//#ifndef _UNG_PIL_INCLUDE_H_
//#include "PILInclude.h"
//#endif
//
//#ifndef _UNG_PIL_SINGLETON_H_
//#include "PILSingleton.h"
//#endif
//
//namespace ung
//{
//	class DLL;
//
//	/*!
//	 * \brief
//	 * DLL管理器
//	 * \class DLLManager
//	 *
//	 * \author Su Yang
//	 *
//	 * \date 2016/05/15
//	 *
//	 * \todo
//	 */
//	class _UngExport DLLManager: public Singleton<DLLManager>
//	{
//	public:
//		/*!
//		 * \remarks 默认构造函数
//		 * \return 
//		*/
//		DLLManager() = default;
//
//		/*!
//		 * \remarks 析构函数
//		 * \return 
//		*/
//		~DLLManager();
//	
//		/*!
//		 * \remarks load动态链接库
//		 * \return 
//		 * \param const String & filename 扩展名可以忽略
//		*/
//		DLL* load(const String& filename);
//
//		/*!
//		 * \remarks unload动态链接库
//		 * \return 
//		*/
//		void unload();
//
//	private:
//		std::map<String, DLL*> mDLLs;
//	};
//}//namespace ung
//#endif//_UNG_PIL_DLLMANAGER_H_