/*!
 * \brief
 * 渲染窗口接口
 * \file UIFIWindow.h
 *
 * \author Su Yang
 *
 * \date 2017/04/29
 */
#ifndef _UIF_IWINDOW_H_
#define _UIF_IWINDOW_H_

#ifndef _UIF_CONFIG_H_
#include "UIFConfig.h"
#endif

#ifdef UIF_HIERARCHICAL_COMPILE

#ifndef _UIF_IRENDERTARGET_H_
#include "UIFIRenderTarget.h"
#endif

#ifndef WIN32_LEAN_AND_MEAN
#if UNG_PLATFORM == UNG_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#if !defined(NOMINMAX) && defined(_MSC_VER)
#define NOMINMAX																					//required to stop windows.h messing up std::min
#endif
#include <windows.h>
#endif
#endif

#ifndef _INCLUDE_BOOST_TRIBOOL_HPP_
#include "boost/logic/tribool.hpp"
#define _INCLUDE_BOOST_TRIBOOL_HPP_
#endif

namespace ung
{
	/*!
	 * \brief
	 * 渲染窗口接口
	 * \class IWindow
	 *
	 * \author Su Yang
	 *
	 * \date 2017/04/29
	 *
	 * \todo
	 */
	class UngExport IWindow : public IRenderTarget
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		IWindow() = default;

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~IWindow() = default;

		/*!
		 * \remarks 获取窗口类名字
		 * \return 
		*/
		virtual wchar_t const* getClassName() const PURE;

		/*!
		 * \remarks 获取窗口的句柄
		 * \return 
		*/
		virtual HWND getHandle() const PURE;

		/*!
		 * \remarks 获取标题
		 * \return 
		*/
		virtual String const& getTitle() const PURE;

		/*!
		 * \remarks 是否为全屏
		 * \return 
		*/
		virtual bool isFullScreen() const PURE;

		/*!
		 * \remarks 是否垂直同步
		 * \return 
		*/
		virtual bool isVerticalSync() const PURE;

		/*!
		 * \remarks 获取窗口的宽
		 * \return 
		*/
		virtual uint32 getWidth() const PURE;

		/*!
		 * \remarks 获取窗口的高
		 * \return 
		*/
		virtual uint32 getHeight() const PURE;

		/*!
		 * \remarks 设置宽
		 * \return 
		 * \param uint32 newWidth
		*/
		virtual void setWidth(uint32 newWidth) PURE;

		/*!
		 * \remarks 设置高
		 * \return 
		 * \param uint32 newHeight
		*/
		virtual void setHeight(uint32 newHeight) PURE;

		/*!
		 * \remarks 获取last宽
		 * \return 
		*/
		virtual uint32 getLastWidth() const PURE;

		/*!
		 * \remarks 获取last高
		 * \return 
		*/
		virtual uint32 getLastHeight() const PURE;

		/*!
		 * \remarks 更新last宽高
		 * \return 
		*/
		virtual void updateLastWH() PURE;

		/*!
		 * \remarks build style
		 * \return 
		*/
		virtual void buildStyle() PURE;

		/*!
		 * \remarks 获取全屏style
		 * \return 
		*/
		virtual uint32 getFullScreenStyle() const PURE;

		/*!
		 * \remarks 获取窗口style
		 * \return 
		*/
		virtual uint32 getWindowedStyle() const PURE;

		/*!
		 * \remarks 获取style
		 * \return 
		*/
		virtual uint32 getStyle() const PURE;

		/*!
		 * \remarks 获取窗口所归属的显示器
		 * \return 
		*/
		virtual HMONITOR getMonitor() const PURE;

		/*!
		 * \remarks 是否为primary
		 * \return 
		*/
		virtual bool isPrimary() const PURE;

		/*!
		 * \remarks 创建窗口
		 * \return 
		*/
		virtual bool create() PURE;

		/*!
		 * \remarks 销毁窗口
		 * \return 
		*/
		virtual void destroy() PURE;

		/*!
		 * \remarks 设置窗口的最小化/最大化状态(和全屏没关系)
		 * \return false_value:mined,true_value:maxed,indeterminate_value:normal
		 * \param boost::tribool state
		*/
		virtual void setMinMaxState(boost::tribool state) PURE;

		/*!
		 * \remarks 获取窗口的最小化/最大化状态(和全屏没关系)
		 * \return false_value:mined,true_value:maxed,indeterminate_value:normal
		*/
		virtual boost::tribool getMinMaxState() const PURE;

		/*!
		 * \remarks 窗口是否处于normal状态(和全屏没关系)
		 * \return 
		*/
		virtual bool isNormalState() const PURE;
	};
}//namespace ung

#endif//UIF_HIERARCHICAL_COMPILE

#endif//_UIF_IWINDOW_H_