/*!
 * \brief
 * 用于配置内部代码。
 * \file URMConfig.h
 *
 * \author Su Yang
 *
 * \date 2016/11/13
 */
#ifndef _URM_CONFIG_H_
#define _URM_CONFIG_H_

#ifndef _URM_ENABLE_H_
#include "URMEnable.h"
#endif

#ifdef URM_HIERARCHICAL_COMPILE

//其它库的头文件
#ifndef _UBS_H_
#include "UBS.h"
#endif

#ifndef _UTM_H_
#include "UTM.h"
#endif

#ifndef _UMM_H_
#include "UMM.h"
#endif

#ifndef _UFC_H_
#include "UFC.h"
#endif

#ifndef _UML_H_
#include "UML.h"
#endif

#ifndef _INCLUDE_STLIB_MUTEX_
#include <mutex>
#define _INCLUDE_STLIB_MUTEX_
#endif

#ifndef _INCLUDE_STLIB_FUTURE_
#include <future>
#define _INCLUDE_STLIB_FUTURE_
#endif

#endif//URM_HIERARCHICAL_COMPILE

#endif//_URM_CONFIG_H_