/*!
 * \brief
 * 自定义物理系统
 * \file UPECustom.h
 *
 * \author Su Yang
 *
 * \date 2017/03/31
 */
#ifndef _UPE_CUSTOM_H_
#define _UPE_CUSTOM_H_

#ifndef _UPE_CONFIG_H_
#include "UPEConfig.h"
#endif

#ifdef UPE_HIERARCHICAL_COMPILE

#ifndef _UPE_BASE_H_
#include "UPEBase.h"
#endif

namespace ung
{
	class PhysicsCustom : public PhysicsBase
	{
	public:
		PhysicsCustom();

		virtual ~PhysicsCustom();

		virtual void initialize() override;

		virtual void update() override;

		virtual void addSphere(real_type radius, WeakObjectPtr object) override;

		virtual void addBox(Vector3 const& min, Vector3 const& max, WeakObjectPtr object) override;
	};
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

#endif//_UPE_CUSTOM_H_