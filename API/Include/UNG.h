/*!
 * \brief
 * 客户代码包含这个头文件。
 * \file UNG.h
 *
 * \author Su Yang
 *
 * \date 2017/03/29
 */
#ifndef _UNG_H_
#define _UNG_H_

#ifndef _UNG_ENABLE_H_
#include "UNGEnable.h"
#endif

#ifdef UNG_HIERARCHICAL_COMPILE

#ifndef _UNG_ROOT_H_
#include "UNGRoot.h"
#endif

#endif//UNG_HIERARCHICAL_COMPILE

#endif//_UNG_H_

/*
引擎初始化方法：
auto& ung = Root::getInstance();
ung.init();
以上两个函数，必须按顺序在最前端调用。
*/