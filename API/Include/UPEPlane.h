/*!
 * \brief
 * 几何对象-平面
 * \file UPEPlane.h
 *
 * \author Su Yang
 *
 * \date 2016/05/22
 */
#ifndef _UPE_PLANE_H_
#define _UPE_PLANE_H_

#ifndef _UPE_CONFIG_H_
#include "UPEConfig.h"
#endif

#ifdef UPE_HIERARCHICAL_COMPILE

#ifndef _UPE_GEOMETRY_H_
#include "UPEGeometry.h"
#endif

namespace ung
{
	class Triangle;
	class AABB;

	/*!
	 * \brief
	 * Plane
	 * \class Plane
	 *
	 * \author Su Yang
	 *
	 * \date 2016/05/22
	 *
	 * \todo
	 */
	class UngExport Plane : public Geometry
	{
	public:
		/*!
		 * \remarks 默认构造函数，穿过坐标系原点，位于xz平面，法线为正Y轴
		 * \return 
		*/
		Plane();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~Plane();

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param const Vector3 & n 法向量
		 * \param const real_type d 原点到平面的有符号距离为：-d
		*/
		Plane(const Vector3& n, const real_type d);

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param real_type nx 法线的三个分量
		 * \param real_type ny
		 * \param real_type nz
		 * \param real_type d 原点到平面的有符号距离为：-d
		*/
		Plane(real_type nx, real_type ny, real_type nz, real_type d);

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param const Vector3& n 法线(不要求是单位向量)
		 * \param const Vector3& p 位于平面上的一个点
		*/
		Plane(const Vector3& n, const Vector3& p);

		/*!
		 * \remarks 构造函数，生成平面的法线方向，按照三个顶点顺序来适用右手法则
		 * \return 
		 * \param const Vector3 & p0 平面上的三个点(要求三个点不能共线)
		 * \param const Vector3 & p1
		 * \param const Vector3 & p2
		*/
		Plane(const Vector3& p0, const Vector3& p1, const Vector3& p2);

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param Triangle const & tri
		*/
		explicit Plane(Triangle const& tri);

		/*!
		 * \remarks 拷贝构造函数
		 * \return 
		 * \param const Plane & p
		*/
		Plane(const Plane& p);

		/*!
		 * \remarks 拷贝赋值运算符
		 * \return 
		 * \param const Plane & p
		*/
		Plane& operator=(const Plane& p);

		/*!
		 * \remarks 两个平面是否相等
		 * \return 
		 * \param const Plane & p
		*/
		bool operator==(const Plane& p) const;

		/*!
		 * \remarks 两个平面是否不相等
		 * \return 
		 * \param const Plane & p
		*/
		bool operator!=(const Plane& p) const;

		/*!
		 * \remarks 获取该平面的法线
		 * \return 
		*/
		const Vector3 getN() const;

		/*!
		 * \remarks 获取平面方程的参数d
		 * \return 如果平面的法向量的模为1，则d = n · p0给出了坐标系原点到该平面最短的有符号距离。
		*/
		const real_type getD() const;

		/*!
		 * \remarks 坐标系原点到平面的有符号距离
		 * \return 
		*/
		const real_type getSignedDistanceFromOriginToPlane() const;

		/*!
		 * \remarks 计算一个给定向量在该平面上的投影向量
		 * \return 
		 * \param const Vector3 & q
		*/
		Vector3 projectionVector(const Vector3& q) const;
		
		/*!
		 * \remarks 将一个4x4矩阵所代表的变换给应用到该平面上
		 * \return 
		 * \param Matrix4 const & mat4
		*/
		void transform(Matrix4 const& mat4);

	private:
		/*!
		 * \remarks 对法线进行规范化，同时重新计算d
		 * \return 
		*/
		void recalculate();

		/*
		n · (p - p0) = 0
		n · p - d = 0
		其中d = n · p0

		mN是上式中的法向量n。
		mD是上式中的常量d。
		mD也是：
		ax + by + cz = d
		中的d，其中[x,y,z]是mN，[a,b,c]是平面上的一点p0
		*/
		Vector3 mN{ 0.0, 1.0, 0.0 };
		/*
		d除以n的长度，再乘以-1.0，为坐标系原点到该平面的有符号距离。
		如果原点位于平面的背面则d为正，也就是说，-d为负。
		*/
		real_type mD = 0.0;
	};
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

#endif//_UPE_PLANE_H_