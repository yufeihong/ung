/*!
 * \brief
 * 统计
 * \file UNGStatistics.h
 *
 * \author Su Yang
 *
 * \date 2017/06/06
 */
#ifndef _UNG_STATISTICS_H_
#define _UNG_STATISTICS_H_

#ifndef _UNG_CONFIG_H_
#include "UNGConfig.h"
#endif

#ifdef UNG_HIERARCHICAL_COMPILE

namespace ung
{
	/*!
	 * \brief
	 * 统计
	 * \class Statistics
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/06
	 *
	 * \todo
	 */
	class UngExport Statistics : public Singleton<Statistics>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		Statistics();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~Statistics();

		/*!
		 * \remarks 更新
		 * \return 
		 * \param real_type dt 单位:毫秒
		*/
		void update(real_type dt);

		/*!
		 * \remarks 获取FPS信息
		 * \return 
		*/
		real_type getFPS() const;

	private:
		/*!
		 * \remarks 更新FPS统计数据
		 * \return 
		 * \param real_type dt 单位:毫秒
		*/
		void _updateFPS(real_type dt);

	private:
		real_type mFPS;
	};
}//namespace ung

#endif//UNG_HIERARCHICAL_COMPILE

#endif//_UNG_STATISTICS_H_