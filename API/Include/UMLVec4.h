/*!
 * \brief
 * 4维向量
 * \file UMLVec4.h
 *
 * \author Su Yang
 *
 * \date 2016/06/15
 */
#ifndef _UML_VEC4_H_
#define _UML_VEC4_H_

#ifndef _UML_CONFIG_H_
#include "UMLConfig.h"
#endif

#ifdef UML_HIERARCHICAL_COMPILE

#ifndef _UML_VEC3_H_
#include "UMLVec3.h"
#endif

#ifndef _UML_MAT4_H_
#include "UMLMat4.h"
#endif

#ifndef _UML_SQT_H_
#include "UMLSQT.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 4维向量
	 * \class Vec4
	 *
	 * \author Su Yang
	 *
	 * \date 2016/06/15
	 *
	 * \todo
	 */
	template<typename T>
	class Vec4 : public MemoryPool<Vec4<T>>
	{
		/*!
		 * \remarks scaler + Vec4
		 * \return 
		 * \param const T scaler
		 * \param const Vec4 & v
		*/
		friend Vec4 operator+(const T scaler, const Vec4& v);

		/*!
		 * \remarks scaler * Vec4
		 * \return 
		 * \param const T scaler
		 * \param const Vec4 & v
		*/
		friend Vec4 operator*(const T scaler, const Vec4& v);

		/*!
		 * \remarks 打印Vec4
		 * \return 
		 * \param std::ostream & o
		 * \param const Vec4 & v
		*/
		friend std::ostream& operator<<(std::ostream& o, const Vec4& v)
		{
			o << "Vector4(" << v.x << "," << v.y << "," << v.z << "," << v.w << ")";
			return o;
		}

	public:
		using math_type = MathImpl<T>;

		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		Vec4() = default;

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~Vec4() = default;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param const T xx
		 * \param const T yy
		 * \param const T zz
		 * \param const T ww
		*/
		Vec4(const T xx, const T yy, const T zz, const T ww) :
			x(xx),
			y(yy),
			z(zz),
			w(ww)
		{
		}

		/*!
		 * \remarks 构造函数，四个分量均设置为参数所给定的标量
		 * \return 
		 * \param const T scalar
		*/
		explicit Vec4(const T scalar) :
			x(scalar),
			y(scalar),
			z(scalar),
			w(scalar)
		{
		}

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param const T * const ar
		 * \param const uint32 sz
		*/
		Vec4(const T *const ar, const uint32 sz)
		{
			BOOST_ASSERT(sz == 4);

			x = ar[0];
			y = ar[1];
			z = ar[2];
			w = ar[3];
		}

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param const T(&ar)[4] 对一个大小为4个元素的数组的常量引用
		*/
		explicit Vec4(const T(&ar)[4]) :
			x(ar[0]),
			y(ar[1]),
			z(ar[2]),
			w(ar[3])
		{
		}

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param const Vec2<T> &v2
		 * \param T zz
		 * \param T ww
		*/
		Vec4(const Vec2<T>& v2,T zz,T ww) :
			x(v2.x),
			y(v2.y),
			z(zz),
			w(ww)
		{
		}

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param const Vec3<T>& v3
		 * \param T ww
		*/
		Vec4(const Vec3<T>& v3,T ww) :
			x(v3.x),
			y(v3.y),
			z(v3.z),
			w(ww)
		{
		}

		/*!
		 * \remarks 拷贝构造函数
		 * \return 
		 * \param const Vec4 & v
		*/
		Vec4(const Vec4 &v) :
			x(v.x),
			y(v.y),
			z(v.z),
			w(v.w)
		{
		}

		/*!
		 * \remarks 拷贝赋值运算符
		 * \return 
		 * \param const Vec4 & v
		*/
		Vec4& operator=(const Vec4& v)
		{
			if (this != &v)
			{
				x = v.x;
				y = v.y;
				z = v.z;
				w = v.w;
			}

			return *this;
		}

		/*!
		 * \remarks 移动构造函数
		 * \return 
		 * \param Vec4 & & v
		*/
		Vec4(Vec4&& v) noexcept :
			x(v.x),
			y(v.y),
			z(v.z),
			w(v.w)
		{
		}

		/*!
		 * \remarks 移动赋值运算符
		 * \return 
		 * \param Vec4 & & v
		*/
		Vec4& operator=(Vec4&& v) noexcept
		{
			if (this != &v)
			{
				x = std::move(v.x);
				y = std::move(v.y);
				z = std::move(v.z);
				w = std::move(v.w);
			}

			return *this;
		}

		/*!
		 * \remarks 重载[]运算符
		 * \return 元素的引用
		 * \param const usize i
		*/
		T& operator [](const usize i)
		{
			BOOST_ASSERT(i >= 0 && i <= 3);
			
			return *(&x + i);
		}

		/*!
		 * \remarks 重载[]运算符，常量对象调用
		 * \return 元素的常量引用
		 * \param const usize i
		*/
		const T& operator [](const usize i) const
		{
			BOOST_ASSERT(i >= 0 && i <= 3);
			
			return *(&x + i);
		}

		/*!
		 * \remarks 获取x的地址
		 * \return 
		*/
		T* getAddress()
		{
			return &x;
		}

		/*!
		 * \remarks 获取x的地址
		 * \return 
		*/
		T const* getAddress() const
		{
			return &x;
		}

		/*!
		 * \remarks 重载==运算符
		 * \return 
		 * \param const Vec4 & v
		*/
		bool operator==(const Vec4& v) const
		{
			return(math_type::isEqual(x, v.x) && math_type::isEqual(y, v.y) && math_type::isEqual(z, v.z) && math_type::isEqual(w, v.w));
		}

		/*!
		 * \remarks 重载!=运算符
		 * \return 
		 * \param const Vec4 & v
		*/
		bool operator!=(const Vec4& v) const
		{
			return !(operator==(v));
		}

		/*!
		 * \remarks 重载<运算符
		 * \return 
		 * \param const Vec4 & v
		*/
		bool operator<(const Vec4& v) const
		{
			if (x < v.x && y < v.y && x < v.z && w < v.w)
			{
				return true;
			}

			return false;
		}

		/*!
		 * \remarks 重载>运算符
		 * \return 
		 * \param const Vec4 & v
		*/
		bool operator>(const Vec4& v) const
		{
			if (x > v.x && y > v.y && z > v.z && w > v.w)
			{
				return true;
			}

			return false;
		}

		/*!
		 * \remarks 重载-运算符
		 * \return 
		*/
		Vec4 operator-() const
		{
			return Vec4(-x, -y, -z, -w);
		}

		/*!
		 * \remarks 重载+运算符
		 * \return 
		 * \param const Vec3 & v
		*/
		Vec4 operator+(const Vec3<T>& v) const
		{
			return Vec4(x + v.x, y + v.y, z + v.z, w);
		}

		/*!
		 * \remarks 重载+运算符
		 * \return 
		 * \param const Vec4 & v
		*/
		Vec4 operator+(const Vec4& v) const
		{
			return Vec4(x + v.x, y + v.y, z + v.z, w + v.w);
		}

		/*!
		 * \remarks 重载-运算符
		 * \return 
		 * \param const Vec4 & v
		*/
		Vec4 operator-(const Vec4& v) const
		{
			return Vec4(x - v.x, y - v.y, z - v.z, w - v.w);
		}

		/*!
		 * \remarks 重载-运算符
		 * \return 
		 * \param const Vec3 & v
		*/
		Vec4 operator-(const Vec3<T>& v) const
		{
			return Vec4(x - v.x, y - v.y, z - v.z, w);
		}

		/*!
		 * \remarks 重载*运算符
		 * \return 
		 * \param const Vec4 & v
		*/
		Vec4 operator*(const Vec4& v) const
		{
			return Vec4(x * v.x, y * v.y, z * v.z, w * v.w);
		}

		/*!
		 * \remarks 重载/运算符
		 * \return 
		 * \param const Vec4 & v
		*/
		Vec4 operator/(const Vec4& v) const
		{
			BOOST_ASSERT(!math_type::isEqual(v.x, 0.0) && !math_type::isEqual(v.y, 0.0) && !math_type::isEqual(v.z, 0.0) && !math_type::isEqual(v.w, 0.0));

			return Vec4(x / v.x, y / v.y, z / v.z, w / v.w);
		}

		/*!
		 * \remarks 重载+=运算符
		 * \return 
		 * \param const Vec4 & v
		*/
		Vec4& operator+=(const Vec4& v)
		{
			x += v.x;
			y += v.y;
			z += v.z;
			w += v.w;

			return *this;
		}

		/*!
		 * \remarks 重载-=运算符
		 * \return 
		 * \param const Vec4 & v
		*/
		Vec4& operator-=(const Vec4& v)
		{
			x -= v.x;
			y -= v.y;
			z -= v.z;
			w -= v.w;

			return *this;
		}

		/*!
		 * \remarks 重载*=运算符
		 * \return 
		 * \param const Vec4 & v
		*/
		Vec4& operator*=(const Vec4& v)
		{
			x *= v.x;
			y *= v.y;
			z *= v.z;
			w *= v.w;

			return *this;
		}

		/*!
		 * \remarks 重载/=运算符
		 * \return 
		 * \param const Vec4 & v
		*/
		Vec4& operator/=(const Vec4& v)
		{
			BOOST_ASSERT(!math_type::isEqual(v.x, 0.0) && !math_type::isEqual(v.y, 0.0) && !math_type::isEqual(v.z, 0.0) && !math_type::isEqual(v.w, 0.0));

			x /= v.x;
			y /= v.y;
			z /= v.z;
			w /= v.w;

			return *this;
		}

		/*!
		 * \remarks Vec4 + scaler
		 * \return 
		 * \param const T scaler
		*/
		Vec4 operator+(const T scaler) const
		{
			return Vec4{x + scaler,y + scaler,z + scaler,w + scaler};
		}

		/*!
		 * \remarks 重载+=运算符
		 * \return 
		 * \param const T scaler
		*/
		Vec4& operator+=(const T scaler)
		{
			x += scaler;
			y += scaler;
			z += scaler;
			w += scaler;

			return *this;
		}

		/*!
		 * \remarks Vec4 - scaler
		 * \return 
		 * \param const T scaler
		*/
		Vec4 operator-(const T scaler) const
		{
			return Vec4{x - scaler,y - scaler,z - scaler,w - scaler};
		}

		/*!
		 * \remarks 重载-=运算符
		 * \return 
		 * \param const T scaler
		*/
		Vec4& operator-=(const T scaler)
		{
			x -= scaler;
			y -= scaler;
			z -= scaler;
			w -= scaler;

			return *this;
		}

		/*!
		 * \remarks Vec4 * scaler
		 * \return 
		 * \param const T scaler
		*/
		Vec4 operator*(const T scaler) const
		{
			return Vec4{ x * scaler,y * scaler,z * scaler,w * scaler };
		}

		/*!
		 * \remarks 重载*=运算符
		 * \return 
		 * \param const T scaler
		*/
		Vec4& operator*=(const T scaler)
		{
			x *= scaler;
			y *= scaler;
			z *= scaler;
			w *= scaler;

			return *this;
		}

		/*!
		 * \remarks Vec4 / scaler
		 * \return 
		 * \param const T scaler
		*/
		Vec4 operator/(const T scaler) const
		{
			BOOST_ASSERT(!math_type::isZero(scaler));

			T inv = 1.0 / scaler;

			return operator*(inv);
		}

		/*!
		 * \remarks 重载/=运算符
		 * \return 
		 * \param const T scaler
		*/
		Vec4& operator/=(const T scaler)
		{
			BOOST_ASSERT(!math_type::isZero(scaler));

			T inv = 1.0 / scaler;
			
			return operator*=(inv);

			return *this;
		}

		/*!
		 * \remarks Vec4 * Mat4
		 * \return 
		 * \param const Mat4<T> & m4
		*/
		Vec4 operator*(const Mat4<T>& m4) const
		{
#if UNG_USE_SIMD
			__declspec (align(16)) float v[] = { x,y,z,w };
			//把内存中的float数组载入__m128(即SSE寄存器)
			__m128 v4 = _mm_load_ps(v);

			auto row0 = m4.getRow(0);
			auto row1 = m4.getRow(1);
			auto row2 = m4.getRow(2);
			auto row3 = m4.getRow(3);

			//强制声明16字节对齐，若略去这个指令，则程序运行时会崩溃。
			__declspec (align(16)) float r0[] = { row0.x,row0.y,row0.z,row0.w };
			__declspec (align(16)) float r1[] = {row1.x,row1.y,row1.z,row1.w};
			__declspec (align(16)) float r2[] = {row2.x,row2.y,row2.z,row2.w};
			__declspec (align(16)) float r3[] = {row3.x,row3.y,row3.z,row3.w};

			__m128 a = _mm_load_ps(r0);
			__m128 b = _mm_load_ps(r1);
			__m128 c = _mm_load_ps(r2);
			__m128 d = _mm_load_ps(r3);

			__m128 result;

			//4个float值相乘
			result = _mm_mul_ps(_mm_replicate_x_ps(v4), a);
			result = _mm_madd_ps(_mm_replicate_y_ps(v4), b, result);
			result = _mm_madd_ps(_mm_replicate_z_ps(v4), c, result);
			result = _mm_madd_ps(_mm_replicate_w_ps(v4), d, result);

			__declspec (align(16)) float out[4];

			//把__m128变量存储至float数组
			_mm_store_ps(out,result);

			return Vec4(out[0],out[1],out[2],out[3]);
#else
			Vec4 outVec{ *this };
			outVec *= m4;

			return outVec;
#endif
		}

		/*!
		 * \remarks 重载*=运算符
		 * \return 
		 * \param const Mat4<T> & m4
		*/
		Vec4& operator*=(const Mat4<T>& m4)
		{
			T xx = x;
			T yy = y;
			T zz = z;
			T ww = w;

			x = xx * m4.m[0][0] + yy * m4.m[1][0] + zz * m4.m[2][0] + ww * m4.m[3][0];
			y = xx * m4.m[0][1] + yy * m4.m[1][1] + zz * m4.m[2][1] + ww * m4.m[3][1];
			z = xx * m4.m[0][2] + yy * m4.m[1][2] + zz * m4.m[2][2] + ww * m4.m[3][2];
			w = xx * m4.m[0][3] + yy * m4.m[1][3] + zz * m4.m[2][3] + ww * m4.m[3][3];

			return *this;
		}

		/*!
		 * \remarks Vec4 * Quater
		 * \return 
		 * \param const Quater<T> & q
		*/
		Vec4 operator*(const Quater<T>& q) const
		{
			return q * (*this);
		}

		/*!
		 * \remarks Vec4 *= Quater
		 * \return 
		 * \param const Quater<T> & q
		*/
		Vec4& operator*=(const Quater<T>& q)
		{
			Vec4 temp = q * (*this);

			swap(temp);

			return *this;
		}

		/*!
		 * \remarks Vec4 * SQT
		 * \return 
		 * \param SQTImpl<T> const & sqt
		*/
		Vec4 operator*(SQTImpl<T> const& sqt) const
		{
			Vec4 outVec4;

			//旋转
			outVec4 = sqt.getRotate() * (*this);

			//缩放(仅投影时改变w)
			T s = sqt.getScale();
			outVec4.x *= s;
			outVec4.y *= s;
			outVec4.z *= s;

			//平移(不平移向量)
			if (!math_type::isZero(w))
			{
				outVec4 += Vec4{sqt.getTranslate(),0.0};
			}
			
			return outVec4;
		}

		/*!
		 * \remarks Vec4 *= SQT
		 * \return 
		 * \param SQTImpl<T> const & sqt
		*/
		Vec4& operator*=(SQTImpl<T> const& sqt)
		{
			Vec4 temp = (*this) * sqt;

			swap(temp);

			return *this;
		}

		/*!
		 * \remarks 交换
		 * \return 
		 * \param Vec4 & other
		*/
		void swap(Vec4& other)
		{
			std::swap(x, other.x);
			std::swap(y, other.y);
			std::swap(z, other.z);
			std::swap(w, other.w);
		}

		/*!
		 * \remarks 返回一个具有Vec4的x,y,z分量的Vec3
		 * \return 
		*/
		Vec3<T> toVector3()
		{
			Vec3<T> v3{ x,y,z };

			return v3;
		}

		/*
		3D点[x,y,z]的齐次坐标为:[x,y,z,w].
		w分量,用于齐次化坐标.要将其次坐标转换为常规坐标,必须除以w.
		w = 0时表示"无限远点",它描述了一个方向而不是一个位置.
		w分量能够"开关"4x4矩阵的平移部分.
		*/
		/*!
		 * \remarks 当向量处于齐次空间时，设置向量到齐次空间中的w = 1三维平面上
		 * \return 
		*/
		void toHomogeneous()
		{
			if (!math_type::isEqual(w, 1.0))
			{
				BOOST_ASSERT(!math_type::isEqual(w,0.0));
				T inv = 1.0 / w;
				operator*=(inv);
				w = 1.0;
			}
		}

	public:
		T x = 0.0;
		T y = 0.0;
		T z = 0.0;
		T w = 0.0;
	};

	template<typename T>
	Vec4<T> operator+(const T scaler, const Vec4<T>& v)
	{
		return Vec4(scaler + v.x, scaler + v.y, scaler + v.z, scaler + v.w);
	}

	template<typename T>
	Vec4<T> operator*(const T scaler, const Vec4<T>& v)
	{
		return Vec4(scaler * v.x, scaler * v.y, scaler * v.z, scaler * v.w);
	}
}//namespace ung

#endif//UML_HIERARCHICAL_COMPILE

#endif//_UML_VEC4_H_