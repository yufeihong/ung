/*!
 * \brief
 * 浮点数typedefs。
 * \file UBSFloat.h
 *
 * \author Su Yang
 *
 * \date 2016/06/15
 */
#ifndef _UBS_FLOAT_H_
#define _UBS_FLOAT_H_

#ifndef _UBS_CONFIG_H_
#include "UBSConfig.h"
#endif

#ifdef UBS_HIERARCHICAL_COMPILE

/*
For best results, <boost/cstdfloat.hpp> should be #included before other headers that define generic code making use of standard
library functions defined in <cmath>.
*/
#ifndef _INCLUDE_BOOST_CSTDFLOAT_HPP_
#include "boost/cstdfloat.hpp"
#define _INCLUDE_BOOST_CSTDFLOAT_HPP_
#endif

#ifndef _INCLUDE_BOOST_MPLIF_HPP_
#include "boost/mpl/if.hpp"
#define _INCLUDE_BOOST_MPLIF_HPP_
#endif

#ifndef _INCLUDE_BOOST_TYPETRAITS_HPP_
#include "boost/type_traits.hpp"
#define _INCLUDE_BOOST_TYPETRAITS_HPP_
#endif

namespace ung
{
	/*
	The total number of bits.
	根据小数位数来推断浮点数total位数
	digits：字符和整数：不含正负号之位数；浮点数：radix的位数
	radix：整数：表述式的底（base），几乎总是2；浮点数：指数表述式的底（base）
	64
	*/
	const int floatTotalBits =
		(std::numeric_limits<boost::floatmax_t>::digits == 113) ? 128 :
		(std::numeric_limits<boost::floatmax_t>::digits == 64) ? 80 :
		(std::numeric_limits<boost::floatmax_t>::digits == 53) ? 64 :
		(std::numeric_limits<boost::floatmax_t>::digits == 24) ? 32 :
		(std::numeric_limits<boost::floatmax_t>::digits == 11) ? 16 :
		0;

	/*
	The number of 'guaranteed' decimal digits
	可以保证的小数位数
	digits10：十进制数字的个数（只有is_bounded成立才有意义）
	15
	*/
	const int floatDigits10 = std::numeric_limits<boost::floatmax_t>::digits10;

	/*
	The maximum number of possibly significant decimal digits.
	小数位数的可能最大数量
	max_digits10：必要的十进制数位个数，确保不同的数值总是不同（对所有浮点数类型都有意义）
	17
	*/
	const int floatMaxDigits10 = std::numeric_limits<boost::floatmax_t>::max_digits10;

//测试都支持哪些widths
#if defined(BOOST_FLOAT16_C)
	using float16 = boost::float16_t;
	using floatfast16 = boost::float_fast16_t;
#endif

#if defined(BOOST_FLOAT32_C)
	using float32 = boost::float32_t;
	using floatfast32 = boost::float_fast32_t;
#endif

#if defined(BOOST_FLOAT64_C)
	using float64 = boost::float64_t;
	using floatfast64 = boost::float_fast64_t;
#endif

#if defined(BOOST_FLOAT80_C)
	using float80 = boost::float80_t;
	using floatfast80 = boost::float_fast80_t;
#endif

#if defined(BOOST_FLOAT128_C)
	using float128 = boost::float128_t;
	using floatfast128 = boost::float_fast128_t;
#endif

#if defined(BOOST_FLOATMAX_C)
	using floatmax = boost::floatmax_t;
#endif

	using float_type = floatfast32;
	using double_type = floatfast64;

#ifdef UNG_CLOSE_DOUBLE
	using real_type = float_type;
#define UNG_USE_DOUBLE 0
#else
	using real_type = double_type;
	#define UNG_USE_DOUBLE 1
#endif
}//namespace ung

#endif//UBS_HIERARCHICAL_COMPILE

#endif//_UBS_FLOAT_H_