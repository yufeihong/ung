/*!
 * \brief
 * 场景对象工厂。
 * \file USMObjectFactory.h
 *
 * \author Su Yang
 *
 * \date 2016/12/13
 */
#ifndef _USM_OBJECTFACTORY_H_
#define _USM_OBJECTFACTORY_H_

#ifndef _USM_CONFIG_H_
#include "USMConfig.h"
#endif

#ifdef USM_HIERARCHICAL_COMPILE

#ifndef _INCLUDE_TINYXML2_H_
#include "tinyxml2.h"
#define _INCLUDE_TINYXML2_H_
#endif

namespace ung
{
	/*!
	 * \brief
	 * 场景对象工厂。(通过解析XML文件，来构建一个由一些components组装起来的Object。)
	 * \class ObjectFactory
	 *
	 * \author Su Yang
	 *
	 * \date 2016/12/13
	 *
	 * \todo
	 */
	class UngExport ObjectFactory : public MemoryPool<ObjectFactory>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		ObjectFactory();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~ObjectFactory();

		/*!
		 * \remarks 创建对象
		 * \return 
		 * \param StrongISceneManagerPtr sm 所创建的对象，其归属的场景管理器
		 * \param String const & objectDataFileFullName
		 * \param String const & objectName
		*/
		StrongIObjectPtr createObject(StrongISceneManagerPtr sm,String const& objectDataFileFullName,String const& objectName);

	protected:
		/*!
		 * \remarks 创建组件
		 * \return 
		 * \param tinyxml2::XMLElement* pComponentElement
		*/
		virtual StrongIObjectComponentPtr createComponent(tinyxml2::XMLElement* pComponentElement);

	protected:
		/*
		泛型工厂:template <typename BaseType, typename Key,class Hasher = std::hash>
		IObjectComponent:组件的基类
		String:xml中组件的名字
		在GenericObjectFactory中，将组件的名字作为Key，来索引到创建组件的creator函数
		*/
		GenericObjectFactory<IObjectComponent, String> mComponentFactory;
	};
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE

#endif//_USM_OBJECTFACTORY_H_