/*!
 * \brief
 * 封装WGL
 * \file UUTWgl.h
 *
 * \author Su Yang
 *
 * \date 2016/11/16
 */
#ifndef _UUT_WGL_H_
#define _UUT_WGL_H_

#ifndef _UUT_CONFIG_H_
#include "UUTConfig.h"
#endif

#ifdef UUT_HIERARCHICAL_COMPILE

namespace ung
{
	typedef void(*ChangeSizeType)(int, int);
	typedef void(*RenderType)();

	/*!
	 * \brief
	 * 封装WGL
	 * \class WGL
	 *
	 * \author Su Yang
	 *
	 * \date 2016/11/16
	 *
	 * \todo
	 */
	class UngExport WGL : public MemoryPool<WGL>
	{
	public:
		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param uint32 width
		 * \param uint32 height
		 * \param wchar_t * title
		 * \param bool fullScreen
		*/
		WGL(uint32 width,uint32 height,wchar_t* title,bool fullScreen = false);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~WGL();

		/*!
		 * \remarks 选择像素格式，创建窗口
		 * \return 
		*/
		void createWindow();

		/*!
		 * \remarks 销毁窗口
		 * \return 
		*/
		void destroyWindow();

		/*!
		 * \remarks 设置回调函数
		 * \return 
		 * \param ChangeSizeType changeSize
		 * \param RenderType render
		*/
		void setCallback(ChangeSizeType changeSize,RenderType render);

		/*!
		 * \remarks 进入循环
		 * \return 
		*/
		void enterLoop();

		/*!
		 * \remarks 获取窗口句柄
		 * \return 
		*/
		HWND getHwnd();

	public:
		static bool& getRenderingFlag();
		static ChangeSizeType getChangeSizeFunction();
		static RenderType getRenderFunction();

	private:
		uint32 mWidth;
		uint32 mHeight;
		bool mFullScreen;
		wchar_t* mTitle;
		HINSTANCE mHinstance;
		HWND mHwnd;
		HDC mHdc;
		HGLRC mHglrc;
		uint8 mMajor;
		uint8 mMinor;

	private:
		static bool mRenderingFlag;
		static ChangeSizeType mChangeSizeFunction;
		static RenderType mRenderFunction;
	};
}//namespace ung

#endif//UUT_HIERARCHICAL_COMPILE

#endif//_UUT_WGL_H_