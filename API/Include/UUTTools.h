/*!
 * \brief
 * 工具集。
 * \file UUTTools.h
 *
 * \author Su Yang
 *
 * \date 2016/11/13
 */
#ifndef _UUT_TOOLS_H_
#define _UUT_TOOLS_H_

#ifndef _UUT_CONFIG_H_
#include "UUTConfig.h"
#endif

#ifdef UUT_HIERARCHICAL_COMPILE

namespace ung
{
	UNG_C_LINK_BEGIN;

	/*!
	 * \remarks 设置顶点shader中的顶点属性索引值。仅用于不支持layout的情况下，且必须于program link之前调用。
	 * \return 
	 * \param GLuint programHandle
	 * \param std::initializer_list<const char * > lst
	*/
	void uutSetAttribLocation(GLuint programHandle,std::initializer_list<const char*> lst);

	/*!
	 * \remarks 判断给定的扩展名，是否被当前的显卡所支持
	 * \return 
	 * \param const char * extension
	*/
	bool uutIsExtSupported(const char* extension);

	/*!
	 * \remarks 画线
	 * \return 
	 * \param const Vector3 & from
	 * \param const Vector3 & to
	 * \param const Vector3 & color
	*/
	void uutDrawLine(const Vector3 &from, const Vector3 &to, const Vector3 &color);

	/*!
	 * \remarks Check for existing instance of the same window.(在创建窗口之后，应用程序初始化之前调用)
	 * \return 
	 * \param const wchar_t* title
	*/
	bool uutIsOnlyInstance(const wchar_t* title);

	/*!
	 * \remarks 检查运行时所需的磁盘空间(用于存储游戏，从DVD-ROM中缓存数据，或者其它临时文件等)
	 * \return 
	 * \param const unsigned __int64 diskSpaceNeeded 单位为：字节，默认需要1GB
	*/
	bool uutCheckStorage(const unsigned __int64 diskSpaceNeeded = 1073741824);

	/*!
	 * \remarks 检查system memory和virtual memory
	 * \return 
	 * \param const unsigned __int64 physicalRAMNeeded 单位为：字节，默认需要2GB
	 * \param const unsigned __int64 virtualRAMNeeded 单位为：字节，默认需要1GB
	*/
	bool uutCheckMemory(const unsigned __int64 physicalRAMNeeded = 2147483648,
		const unsigned __int64 virtualRAMNeeded = 1073741824);

	/*!
	 * \remarks 检查所需的最小CPU速度
	 * \return 
	 * \param unsigned long cpuSpeedNeeded 默认为2.0 GHz
	*/
	bool uutCheckCPUSpeed(unsigned long cpuSpeedNeeded = 2000);

	UNG_C_LINK_END;
}//namespace ung

#endif//UUT_HIERARCHICAL_COMPILE

#endif//_UUT_TOOLS_H_