#ifndef _UFC_GENLINEARHIERARCHY_H_
#define _UFC_GENLINEARHIERARCHY_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#ifndef _UFC_TYPELIST_H_
#include "UFCTypeList.h"
#endif

namespace ung
{
	namespace utp
	{
		template<typename TList,template <typename AtomicType, typename Base> class Unit,typename Root = EmptyType>
		class GenLinearHierarchy;

		template<typename T1,typename T2,template <typename, typename> class Unit,typename Root>
		class GenLinearHierarchy<TypeList<T1, T2>, Unit, Root> : public Unit< T1, GenLinearHierarchy<T2, Unit, Root> >
		{
		};

		template<typename T,template <typename, typename> class Unit,typename Root>
		class GenLinearHierarchy<TypeList<T, NullType>, Unit, Root> : public Unit<T, Root>
		{
		};

		template<template <typename, typename> class Unit,typename Root>
		class GenLinearHierarchy<NullType, Unit, Root> : public Root
		{
		};
	}//namespace utp
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_GENLINEARHIERARCHY_H_