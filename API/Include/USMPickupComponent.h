/*!
 * \brief
 * 可拾取组件。
 * \file USMPickupComponent.h
 *
 * \author Su Yang
 *
 * \date 2017/02/11
 */
#ifndef _USM_PICKUPCOMPONENT_H_
#define _USM_PICKUPCOMPONENT_H_

#ifndef _USM_CONFIG_H_
#include "USMConfig.h"
#endif

#ifdef USM_HIERARCHICAL_COMPILE

#ifndef _USM_OBJECTCOMPONENT_H_
#include "USMObjectComponent.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 可拾取组件
	 * \class PickupComponent
	 *
	 * \author Su Yang
	 *
	 * \date 2017/02/11
	 *
	 * \todo
	 */
	class UngExport PickupComponent : public ObjectComponent
	{
	public:
		using WeakObjectPtr = std::weak_ptr<Object>;

		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		PickupComponent() = delete;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param String const & name
		*/
		PickupComponent(String const& name);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~PickupComponent();

		virtual void apply(WeakObjectPtr pObject) = 0;
	};
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE

#endif//_USM_PICKUPCOMPONENT_H_