/*!
 * \brief
 * 几何体。基类。
 * \file UPEGeometry.h
 *
 * \author Su Yang
 *
 * \date 2016/11/24
 */
#ifndef _UPE_GEOMETRY_H_
#define _UPE_GEOMETRY_H_

#ifndef _UPE_CONFIG_H_
#include "UPEConfig.h"
#endif

#ifdef UPE_HIERARCHICAL_COMPILE

namespace ung
{
	/*!
	 * \brief
	 * 几何体基类。
	 * \class Geometry
	 *
	 * \author Su Yang
	 *
	 * \date 2016/11/24
	 *
	 * \todo
	 */
	class UngExport Geometry : public MemoryPool<Geometry>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		Geometry();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~Geometry();
	};
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

#endif//_UPE_GEOMETRY_H_