/*!
 * \brief
 * ������ڴ��
 * \file UMMObject.h
 *
 * \author Su Yang
 *
 * \date 2016/04/09
 */
#ifndef _UMM_OBJECT_H_
#define _UMM_OBJECT_H_

#ifndef _UMM_CONFIG_H_
#include "UMMConfig.h"
#endif

#ifdef UMM_HIERARCHICAL_COMPILE

#ifndef _INCLUDE_BOOST_OBJECTPOOL_HPP_
#include "boost/pool/object_pool.hpp"
#define _INCLUDE_BOOST_OBJECTPOOL_HPP_
#endif

/*
using namespace boost;
*/

#endif//UMM_HIERARCHICAL_COMPILE

#endif//_UMM_OBJECT_H_