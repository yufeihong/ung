/*!
 * \brief
 * 凸状平面包围体(凸多面体)(每个面的法线都朝向外)
 * \file UPEPlaneBoundedVolume.h
 *
 * \author Su Yang
 *
 * \date 2017/04/03
 */
#ifndef _UPE_PLANEBOUNDEDVOLUME_H_
#define _UPE_PLANEBOUNDEDVOLUME_H_

#ifndef _UPE_CONFIG_H_
#include "UPEConfig.h"
#endif

#ifdef UPE_HIERARCHICAL_COMPILE

#ifndef _UIF_IBOUNDINGVOLUME_H_
#include "UIFIBoundingVolume.h"
#endif

namespace ung
{
	class Plane;
	class AABB;
	class Sphere;

	/*!
	 * \brief
	 * 凸状平面包围体
	 * \class PlaneBoundedVolume
	 *
	 * \author Su Yang
	 *
	 * \date 2017/04/03
	 *
	 * \todo
	 */
	class UngExport PlaneBoundedVolume : public IBoundingVolume
	{
	public:
		using planes_type = STL_VECTOR(std::shared_ptr<Plane>);

		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		PlaneBoundedVolume();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~PlaneBoundedVolume();

		/*!
		 * \remarks 添加平面
		 * \return 
		 * \param std::shared_ptr<Plane> planePtr
		*/
		void addPlane(std::shared_ptr<Plane> planePtr);

		/*!
		 * \remarks 是否包含参数所指定的球体(相交也算包含)
		 * \return 
		 * \param Sphere const & sphere
		*/
		bool contain(Sphere const& sphere);

		/*!
		 * \remarks 获取凸状平面包围体的世界空间的包围盒
		 * \return 
		*/
		AABB getWorldAABB() const;

		/*!
		 * \remarks 获取迭代器
		 * \return 
		*/
		std::pair<planes_type::iterator, planes_type::iterator> getIterator();

		/*!
		 * \remarks 获取常量迭代器
		 * \return 
		*/
		std::pair<planes_type::const_iterator, planes_type::const_iterator> getIterator() const;

	private:
		planes_type mPlanes;
	};
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

#endif//_UPE_PLANEBOUNDEDVOLUME_H_