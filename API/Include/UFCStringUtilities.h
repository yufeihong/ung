/*!
 * \brief
 * 字符串工具集
 * \file UFCStringUtilities.h
 *
 * \author Su Yang
 *
 * \date 2016/04/15
 */
#ifndef _UFC_STRINGUTILITIES_H_
#define _UFC_STRINGUTILITIES_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#ifndef _UFC_CHOOSECONTAINER_H_
#include "UFCChooseContainer.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 字符串工具集
	 * \class StringUtilities
	 *
	 * \author Su Yang
	 *
	 * \date 2016/04/15
	 *
	 * \todo
	 */
	class UngExport StringUtilities
	{
	public:
		StringUtilities() = delete;
		~StringUtilities() = delete;

		StringUtilities(const StringUtilities&) = delete;
		StringUtilities& operator=(const StringUtilities&) = delete;

		enum IsType
		{
			istype_space			= std::ctype_base::space,				//字符是否为空格或制表符
			istype_alnum			= std::ctype_base::alnum,				//字符是否为字母和数字
			istype_alpha			= std::ctype_base::alpha,				//字符是否为字母
			istype_cntrl			= std::ctype_base::cntrl,				//字符是否为控制字符
			istype_digit			= std::ctype_base::digit,				//字符是否为十进制数字
			istype_graph			= std::ctype_base::graph,				//字符是否为图形字符
			istype_lower			= std::ctype_base::lower,				//字符是否为小写字符
			istype_print			= std::ctype_base::print,				//字符是否为可打印字符
			istype_punct			= std::ctype_base::punct,				//字符是否为标点符号
			istype_upper			= std::ctype_base::upper,				//字符是否为大写字符
			istype_xdigit			= std::ctype_base::xdigit				//字符是否为十六进制数字
		};

		/*
		Case Conversion:大小写转换.
		*/
		static void toUpper(String& s);
		static String toUpperCopy(const String& s);
		static void toLower(String& s);
		static String toLowerCopy(const String& s);

		/*
		Trimming:修剪.
		*/
		/*
		Remove leading spaces from a string.
		Remove trailing spaces from a string.
		mask:istype_alpha | istype_punct
		*/
		static void trimLeft(String& s);
		static void trimRight(String& s);
		static void trimLeftIf(String& s, uint16 mask);
		static void trimRightIf(String& s, uint16 mask);
		static String trimLeftCopy(const String& s);
		static String trimRightCopy(const String& s);
		static String trimLeftCopyIf(const String& s, uint16 mask);
		static String trimRightCopyIf(const String& s, uint16 mask);
		/*
		Remove leading and trailing spaces from a string.
		*/
		static void trimBoth(String& s);
		static void trimBothIf(String& s, uint16 mask);
		static String trimBothCopy(const String& s);
		static String trimBothCopyIf(const String& s, uint16 mask);

		/*
		Predicates:谓词,判断式.
		Check if a string is a prefix of the other one.
		Check if a string is a suffix of the other one.
		*/
		static bool startWithSensitive(const String& s, const String& with);		//敏感
		static bool endWithSensitive(const String& s, const String& with);
		static bool startWithInSensitive(const String& s, const String& with);		//不敏感
		static bool endWithInSensitive(const String& s, const String& with);
		/*
		Check if a string is contained of the other one.
		*/
		static bool containSensitive(const String& s, const String& con);
		static bool containInSensitive(const String& s, const String& con);
		/*
		Check if two strings are equal.
		*/
		static bool equalSensitive(const String& s1, const String& s2);
		static bool equalInSensitive(const String& s1, const String& s2);
		/*
		Check if a string is lexicographically less(按字典顺序,小于) than another one.
		*/
		static bool lessSensitive(const String& s1, const String& s2);
		static bool lessInSensitive(const String& s1, const String& s2);
		/*
		Check if all elements of a string satisfy the given predicate.
		检测一个字符串中的所有元素是否都满足指定的判断式.
		s:如果只想检测s中的某个子区间的话,可以这样:s.substr(0,5)
		*/
		static bool allCheck(const String& s, uint16 pre);

		/*
		Find algorithms.查找.
		Find the first/last occurrence(出现) of a string in the input.
		*/
		static uint32 findLeftSensitive(String& s, const String& f);		//返回找到的pos
		static uint32 findLeftInSensitive(String& s, const String& f);
		static uint32 findRightSensitive(String& s, const String& f);
		static uint32 findRightInSensitive(String& s, const String& f);
		/*
		Find the nth (zero-indexed) occurrence of a string in the input.
		查找字符串在输入中的第N次出现的位置(从0开始计数).
		*/
		static uint32 findNthSensitive(String& s, const String& f,uint8 n);
		static uint32 findNthInSensitive(String& s, const String& f,uint8 n);
		/*
		Retrieve the head/tail of a string.
		获取一个字符串开头N个字符的子串,相当于substr(0,n).
		*/
		static String getLeftN(String& s, uint8 n);
		static String getRightN(String& s, uint8 n);
		/*
		Find first matching token in the string.
		*/
		static String getFirstMatchToken(String& s, IsType type);

		/*
		替换与删除.
		*/
		/*
		替换/删除一个字符串在输入中的第一次出现.
		*/
		static void replaceLeftSensitive(String& s, const String& search, const String& replace);
		static String replaceLeftCopySensitive(String& s, const String& search, const String& replace);
		static void replaceLeftInSensitive(String& s, const String& search, const String& replace);
		static String replaceLeftCopyInSensitive(String& s, const String& search, const String& replace);

		static void eraseLeftSensitive(String& s, const String& search);
		static String eraseLeftCopySensitive(String& s, const String& search);
		static void eraseLeftInSensitive(String& s, const String& search);
		static String eraseLeftCopyInSensitive(String& s, const String& search);

		static void replaceRightSensitive(String& s, const String& search, const String& replace);
		static String replaceRightCopySensitive(String& s, const String& search, const String& replace);
		static void replaceRightInSensitive(String& s, const String& search, const String& replace);
		static String replaceRightCopyInSensitive(String& s, const String& search, const String& replace);

		static void eraseRightSensitive(String& s, const String& search);
		static String eraseRightCopySensitive(String& s, const String& search);
		static void eraseRightInSensitive(String& s, const String& search);
		static String eraseRightCopyInSensitive(String& s, const String& search);
		/*
		Replace/Erase the nth (zero-indexed) occurrence of a string in the input.
		替换/删除一个字符串在输入中的第n次出现(从0开始计数).
		*/
		static void replaceNthSensitive(String& s, const String& search, uint8 n,const String& replace);
		static String replaceNthCopySensitive(String& s, const String& search, uint8 n, const String& replace);
		static void replaceNthInSensitive(String& s, const String& search, uint8 n, const String& replace);
		static String replaceNthCopyInSensitive(String& s, const String& search, uint8 n, const String& replace);

		static void eraseNthSensitive(String& s, const String& search, uint8 n);
		static String eraseNthCopySensitive(String& s, const String& search, uint8 n);
		static void eraseNthInSensitive(String& s, const String& search, uint8 n);
		static String eraseNthCopyInSensitive(String& s, const String& search, uint8 n);
		/*
		Replace/Erase the all occurrences of a string in the input.
		替换/删除一个字符串在输入中的所有出现.
		*/
		static void replaceAllSensitive(String& s, const String& search, const String& replace);
		static String replaceAllCopySensitive(String& s, const String& search,const String& replace);
		static void replaceAllInSensitive(String& s, const String& search,const String& replace);
		static String replaceAllCopyInSensitive(String& s, const String& search,const String& replace);

		static void eraseAllSensitive(String& s, const String& search);
		static String eraseAllCopySensitive(String& s, const String& search);
		static void eraseAllInSensitive(String& s, const String& search);
		static String eraseAllCopyInSensitive(String& s, const String& search);
		/*
		Replace/Erase the head of the input.
		替换/删除输入的开头的N个字符.
		*/
		static void replaceLeftN(String& s, uint8 n, const String& replace);
		static String replaceLeftNCopy(String& s, uint8 n, const String& replace);
		static void eraseLeftN(String& s, uint8 n);
		static String eraseLeftNCopy(String& s, uint8 n);

		static void replaceRightN(String& s, uint8 n, const String& replace);
		static String replaceRightNCopy(String& s, uint8 n, const String& replace);
		static void eraseRightN(String& s, uint8 n);
		static String eraseRightNCopy(String& s, uint8 n);

		/*
		Split.分割.
		*/
		static void splitToList(STL_LIST(String)& result, String& input, const char* pred);
		static void splitToVector(STL_VECTOR(String)& result, String& input, const char* pred);
		static void splitToDeque(STL_DEQUE(String)& result, String& input, const char* pred);

		/*
		Join.合并.
		把存储在容器中的字符串连接成一个新的字符串,并且可以指定连接的分隔符.
		*/
		static String joinFromList(STL_LIST(String) &source, const char *separator);
		static String joinFromVector(STL_VECTOR(String) &source, const char *separator);
		static String joinFromDeque(STL_DEQUE(String) &source, const char *separator);
	};

	/*!
	 * \brief
	 * 字符串转换
	 * \class LexicalCast
	 *
	 * \author Su Yang
	 *
	 * \date 2016/04/15
	 *
	 * \todo
	 */
	class UngExport LexicalCast
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		LexicalCast() = default;

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~LexicalCast() = default;

		/*!
		 * \remarks string-int
		 * \return 
		 * \param const String & s
		*/
		static int32 stringToInt(const String& s);

		/*!
		 * \remarks string-float
		 * \return 
		 * \param const String & s
		*/
		static float stringToFloat(const String& s);

		/*!
		 * \remarks string-double
		 * \return 
		 * \param const String & s
		*/
		static double stringToDouble(const String& s);

		/*!
		 * \remarks int-string
		 * \return 
		 * \param const int32 i
		*/
		static String intToString(const int32 i);

		/*!
		 * \remarks double-string
		 * \return 
		 * \param const double r
		*/
		static String doubleToString(const double r);

		/*!
		 * \remarks char-wchar_t(需要手动释放delete[] sink)
		 * \return 
		 * \param const char * source
		 * \param wchar_t * & sink
		*/
		static bool convert(const char* source, wchar_t*& sink);

		/*!
		 * \remarks wchar_t-char(需要手动释放delete[] sink)
		 * \return 
		 * \param const wchar_t * source
		 * \param char * & sink
		*/
		static bool convert(const wchar_t* source, char*& sink);

		/*!
		 * \remarks string-wstring
		 * \return 
		 * \param String const & source
		 * \param StringW & sink
		*/
		static bool convert(String const& source, StringW& sink);

		/*!
		 * \remarks wstring-string
		 * \return 
		 * \param StringW const & source
		 * \param String & sink
		*/
		static bool convert(StringW const& source, String& sink);

		/*!
		 * \remarks char-wstring
		 * \return 
		 * \param const char * source
		 * \param StringW & sink
		*/
		static bool convert(const char* source, StringW& sink);

		/*!
		 * \remarks wstring-char(需要手动释放delete[] sink)
		 * \return 
		 * \param StringW const & source
		 * \param char * & sink
		*/
		static bool convert(StringW const& source, char*& sink);

		/*!
		 * \remarks wchar_t-string
		 * \return 
		 * \param const wchar_t * source
		 * \param String & sink
		*/
		static bool convert(const wchar_t* source, String& sink);

		/*!
		 * \remarks string-wchar_t(需要手动释放delete[] sink)
		 * \return 
		 * \param String const & source
		 * \param wchar_t * & sink
		*/
		static bool convert(String const& source, wchar_t*& sink);
	};
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_STRINGUTILITIES_H_