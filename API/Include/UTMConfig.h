#ifndef _UTM_CONFIG_H_
#define _UTM_CONFIG_H_

#ifndef _UTM_ENABLE_H_
#include "UTMEnable.h"
#endif

#ifdef UTM_HIERARCHICAL_COMPILE

#ifndef _UBS_H_
#include "UBS.h"
#endif

#endif//UTM_HIERARCHICAL_COMPILE

#endif//_UTM_CONFIG_H_