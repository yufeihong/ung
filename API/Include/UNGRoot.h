/*!
 * \brief
 * UNG root
 * \file UNGRoot.h
 *
 * \author Su Yang
 *
 * \date 2017/03/29
 */
#ifndef _UNG_ROOT_H_
#define _UNG_ROOT_H_

#ifndef _UNG_CONFIG_H_
#include "UNGConfig.h"
#endif

#ifdef UNG_HIERARCHICAL_COMPILE

namespace ung
{
	class Statistics;
	enum class RenderSystemType : uint8;

	/*!
	 * \brief
	 * UNG Root
	 * \class Root
	 *
	 * \author Su Yang
	 *
	 * \date 2017/03/29
	 *
	 * \todo
	 */
	class UngExport Root : public Singleton<Root>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		Root();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~Root();

		//-------------------------------------------------------------------------

		/*!
		 * \remarks 初始化引擎。
		 * \return 
		*/
		void init();

		/*!
		 * \remarks 循环
		 * \return 
		*/
		void run();

		/*!
		 * \remarks 更新
		 * \return 
		 * \param real_type dt 单位:毫秒
		*/
		void update(real_type dt);

		/*!
		 * \remarks 渲染
		 * \return 
		 * \param real_type dt 单位:毫秒
		*/
		void render(real_type dt);

		/*!
		 * \remarks 关闭引擎。
		 * \return 
		*/
		void close();

		//-------------------------------------------------------------------------

		/*!
		 * \remarks 设置上一帧所消耗的时间(单位:毫秒)
		 * \return 
		 * \param real_type dt
		*/
		void setElapsedTime(real_type dt);

		/*!
		 * \remarks 获取上一帧所消耗的时间(单位:毫秒)
		 * \return 
		*/
		real_type getElapsedTime();

		//-------------------------------------------------------------------------

		/*!
		 * \remarks 创建特定空间管理器类型的场景管理器
		 * \return 返回引用，是避免客户代码直接delete指针，从而让容器中的元素成为悬指针
		 * \param String const & name
		*/
		template<SceneType ST>
		StrongISceneManagerPtr createSceneManager(String const& name = EMPTY_STRING)
		{
			//配置析构器
			StrongISceneManagerPtr pSM(UNG_NEW SceneManager<ST>(name),
				//此处的参数必须为ISceneManager*类型
				[this](ISceneManager* p)
			{
				auto name = p->getName();

#if UNG_DEBUGMODE
				//std::cout << "析构场景管理器:" << name << std::endl;
#endif

				auto findRet = mSceneManagers.find(name);
				BOOST_ASSERT(findRet != mSceneManagers.end());

				auto ret = mSceneManagers.erase(name);
				BOOST_ASSERT(ret == 1);

				UNG_DEL(p);
			});

			mSceneManagers[pSM->getName()] = pSM;

			return pSM;
		}

		/*!
		 * \remarks 销毁参数所指定的场景管理器
		 * \return 
		 * \param String const & name
		*/
		void destroySceneManager(String const& name);

		/*!
		 * \remarks 获取参数所指定的场景管理器
		 * \return 
		 * \param String const & name
		*/
		StrongISceneManagerPtr getSceneManager(String const& name) const;

		//-------------------------------------------------------------------------

		/*!
		 * \remarks 添加渲染系统
		 * \return 
		 * \param String const & name
		 * \param IRenderSystem * rs
		*/
		void addRenderSystem(String const& name, IRenderSystem* rs);

		/*!
		 * \remarks 移除渲染系统
		 * \return 
		 * \param String const & name
		*/
		void removeRenderSystem(String const& name);

		/*!
		 * \remarks 获取参数所指定的渲染系统
		 * \return 
		 * \param String const & name
		*/
		IRenderSystem* getRenderSystem(String const& name) const;

		/*!
		 * \remarks 获取当前渲染系统
		 * \return 
		*/
		IRenderSystem* getCurrentRenderSystem() const;

		/*!
		 * \remarks 获取当前渲染系统的类型
		 * \return 
		*/
		RenderSystemType const& getCurrentRenderSystemType() const;

		//-------------------------------------------------------------------------

		/*!
		 * \remarks 获取日志管理器
		 * \return 
		*/
		LogManager& getLogManager();

#if UNG_DEBUGMODE
		/*!
		 * \remarks 获取内存检测管理器
		 * \return 
		*/
		BigDog& getBigDogManager();
#endif

		/*!
		 * \remarks 获取平台信息管理器
		 * \return 
		*/
		Platform& getPlatformManager();

		/*!
		 * \remarks 获取文件系统管理器
		 * \return 
		*/
		Filesystem& getFilesystemManager();

		/*!
		 * \remarks 获取插件管理器
		 * \return 
		*/
		PluginManager& getPluginManager();

		/*!
		 * \remarks 获取消息管理器
		 * \return 
		*/
		EventManager& getEventManager();

		/*!
		 * \remarks 获取空间管理器的枚举器
		 * \return 
		*/
		SpatialManagerEnumerator& getSpatialManagerEnumerator();

		/*!
		 * \remarks 获取统计实例
		 * \return 
		*/
		Statistics& getStatistics();

	private:
		STL_UNORDERED_MAP(String, StrongISceneManagerPtr) mSceneManagers;
		STL_UNORDERED_MAP(String, IRenderSystem*) mRenderSystems;
		IRenderSystem* mCurrentRenderSystem;
		real_type mElapsedTime;
	};
}//namespace ung

#endif//UNG_HIERARCHICAL_COMPILE

#endif//_UNG_ROOT_H_