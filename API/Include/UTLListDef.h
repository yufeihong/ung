/*!
 * \brief
 * 链表容器的函数定义。
 * \file UTLListDef.h
 *
 * \author Su Yang
 *
 * \date 2016/01/28
 */
#ifndef _UNG_UTL_LIST_DEF_H_
#define _UNG_UTL_LIST_DEF_H_

#ifndef _UNG_UTL_LIST_H_
#include "UTLList.h"
#endif

namespace ung
{
	namespace utl
	{
		template<typename ElementType>
		ListNode<ElementType>::ListNode() :
		mPrev(nullptr),
		mNext(nullptr),
		mData(value_type())
		{
		}

		template<typename ElementType>
		ListNode<ElementType>::ListNode(value_type const& data) :
			mPrev(nullptr),
			mNext(nullptr),
			mData(data)
		{
		}

		template<typename ElementType>
		ListNode<ElementType>::ListNode(value_type&& data) :
			mData(std::move(data))
		{

		}

		template<typename ElementType>
		ListNode<ElementType>::ListNode(self_type const& right) :
			mPrev(nullptr),
			mNext(nullptr),
			mData(right.mData)
		{
		}

		template<typename ElementType>
		void ListNode<ElementType>::operator=(self_type const& right)
		{
			if (this == &right)
			{
				return;
			}

			//mPrev = right.mPrev;
			//mNext = right.mNext;
			mData = right.mData;
		}

		template<typename ElementType>
		ListNode<ElementType>::~ListNode()
		{
			priv_destructor(boost::has_trivial_destructor<ElementType>());
		}

		template<typename ElementType>
		void ListNode<ElementType>::priv_destructor(boost::true_type &)
		{
			priv_delete(boost::is_pointer<ElementType>());
		}

		template<typename ElementType>
		void ListNode<ElementType>::priv_destructor(boost::false_type &)
		{
			mData.~ElementType();
		}

		template<typename ElementType>
		void ListNode<ElementType>::priv_delete(boost::true_type &)
		{
			if(mData)
			{
				delete(mData);
				mData = nullptr;
			}
		}

		template<typename ElementType>
		void ListNode<ElementType>::priv_delete(boost::false_type &)
		{
		}

		//-------------------------------------------------------------------------

		template<typename Alloc>
		ListAllocatorHolder<Alloc>::ListAllocatorHolder() noexcept(boost::has_nothrow_default_constructor<Alloc>::value) :
			Alloc(),
			mHead(nullptr),
			mTail(nullptr),
			mSize(size_type())
		{
			mHead = allocate(1);
			::new(mHead) value_type();
			mHead->mPrev = nullptr;

			mTail = allocate(1);
			::new(mTail) value_type();
			mTail->mPrev = mHead;
			mTail->mNext = nullptr;

			mHead->mNext = mTail;
		}

		template<typename Alloc>
		ListAllocatorHolder<Alloc>::ListAllocatorHolder(size_type initialSize) :
			ListAllocatorHolder(initialSize, element_type())
		{
		}

		template<typename Alloc>
		ListAllocatorHolder<Alloc>::ListAllocatorHolder(size_type initialSize, element_type const& elementValue) :
			ListAllocatorHolder()
		{
			ssize signedSize = initialSize;
			while (signedSize-- > 0)
			{
				insert(mTail, elementValue);
			}
		}

		template<typename Alloc>
		ListAllocatorHolder<Alloc>::ListAllocatorHolder(self_type const& right) :
			ListAllocatorHolder()
		{
			pointer begin = right.mHead->mNext;
			pointer end = right.mTail;
			while (begin != end)
			{
				insert(mTail, *begin);
				begin = begin->mNext;
			}
		}

		template<typename Alloc>
		typename ListAllocatorHolder<Alloc>::self_type& ListAllocatorHolder<Alloc>::operator=(self_type const& right)
		{
			if (this == &right)
			{
				return *this;
			}

			if (right.mSize == 0)
			{
				decrementSize(mHead->mNext, mSize);
				return *this;
			}

			//先释放
			ssize minValue = std::min(mSize, right.mSize);
			pointer curr = mHead->mNext;
			while (minValue-- > 0)
			{
				curr->~ListNode();
				curr = curr->mNext;
			}

			ssize sizeDif = mSize - right.mSize;

			if (sizeDif < 0)
			{
				incrementSize(mTail, -sizeDif);
			}

			ssize count = right.mSize;
			curr = mHead->mNext;
			pointer source = right.mHead->mNext;
			while (count-- > 0)
			{
				element_type tmp = source->mData;
				place(curr, std::move(tmp));

				curr = curr->mNext;
				source = source->mNext;
			}

			decrementSize(curr, mSize - right.mSize);

			return *this;
		}

		template<typename Alloc>
		ListAllocatorHolder<Alloc>::ListAllocatorHolder(self_type&& right) :
			ListAllocatorHolder()
		{
			pointer begin = right.mHead->mNext;
			pointer end = right.mTail;
			while (begin != end)
			{
				insert(mTail, *begin);
				begin = begin->mNext;
			}
		}

		template<typename Alloc>
		typename ListAllocatorHolder<Alloc>::self_type& ListAllocatorHolder<Alloc>::operator=(self_type&& right)
		{
			if (this == &right)
			{
				return *this;
			}

			if (right.mSize == 0)
			{
				decrementSize(mHead->mNext, mSize);
				return *this;
			}

			//先释放
			ssize minValue = std::min(mSize, right.mSize);
			pointer curr = mHead->mNext;
			while (minValue-- > 0)
			{
				curr->~ListNode();
				curr = curr->mNext;
			}

			ssize sizeDif = mSize - right.mSize;

			if (sizeDif < 0)
			{
				incrementSize(mTail, -sizeDif);
			}

			ssize count = right.mSize;
			curr = mHead->mNext;
			pointer source = right.mHead->mNext;
			while (count-- > 0)
			{
				place(curr, std::move(source->mData));

				curr = curr->mNext;
				source = source->mNext;
			}

			decrementSize(curr, mSize - right.mSize);

			return *this;
		}

		template<typename Alloc>
		template<class iteratorT>
		ListAllocatorHolder<Alloc>::ListAllocatorHolder(iteratorT first, iteratorT last, typename boost::enable_if<std::_Is_iterator<iteratorT>, void>::type*) :
			ListAllocatorHolder()
		{
			while (first != last)
			{
				insert(mTail,*first++);
			}
		}

		template<typename Alloc>
		ListAllocatorHolder<Alloc>::ListAllocatorHolder(std::initializer_list<element_type> il) :
			ListAllocatorHolder(il.begin(),il.end())
		{
		}

		template<typename Alloc>
		template<class iteratorT>
		void ListAllocatorHolder<Alloc>::assign(iteratorT first, iteratorT last,
			typename boost::enable_if<std::_Is_iterator<iteratorT>, void>::type* = 0)
		{
			usize sz = last - first;

			if (sz == 0)
			{
				decrementSize(mHead->mNext, sz);
				return *this;
			}

			ssize minValue = std::min(mSize, sz);
			pointer curr = mHead->mNext;
			while (minValue-- > 0)
			{
				curr->~ListNode();
				curr = curr->mNext;
			}

			ssize sizeDif = mSize - sz;

			if (sizeDif < 0)
			{
				incrementSize(mTail, -sizeDif);
			}

			ssize count = sz;
			curr = mHead->mNext;
			pointer source = first
			while (count-- > 0)
			{
				place(curr, *source);

				curr = curr->mNext;
				++source;
			}

			decrementSize(curr, mSize - sz);
		}

		template<typename Alloc>
		void ListAllocatorHolder<Alloc>::assign(std::initializer_list<element_type> il)
		{
			assign(il.begin(), il.end());
		}

		template<typename Alloc>
		void ListAllocatorHolder<Alloc>::assign(size_type count, element_type const& elementValue)
		{
			if (count == 0)
			{
				decrementSize(mHead->mNext, mSize);
				return *this;
			}

			ssize minValue = std::min(mSize, count);
			pointer curr = mHead->mNext;
			while (minValue-- > 0)
			{
				curr->~ListNode();
				curr = curr->mNext;
			}

			ssize sizeDif = mSize - count;

			if (sizeDif < 0)
			{
				incrementSize(mTail, -sizeDif);
			}

			ssize ct = count;
			curr = mHead->mNext;
			while (ct-- > 0)
			{
				place(curr, elementValue);

				curr = curr->mNext;
			}

			decrementSize(curr, mSize - count);
		}

		template<typename Alloc>
		ListAllocatorHolder<Alloc>::~ListAllocatorHolder()
		{
			decrementSize(mHead->mNext,mSize);
			mHead->~ListNode();
			deallocate(mHead,1);
			mTail->~ListNode();
			deallocate(mTail, 1);
			mHead = mTail = nullptr;
		}

		template<typename Alloc>
		typename ListAllocatorHolder<Alloc>::pointer const& ListAllocatorHolder<Alloc>::head() const
		{
			return mHead;
		}

		template<typename Alloc>
		typename ListAllocatorHolder<Alloc>::pointer const& ListAllocatorHolder<Alloc>::tail() const
		{
			return mTail;
		}

		template<typename Alloc>
		typename ListAllocatorHolder<Alloc>::size_type const& ListAllocatorHolder<Alloc>::size() const
		{
			return mSize;
		}

		template<typename Alloc>
		void ListAllocatorHolder<Alloc>::incrementSize(pointer where, size_type increments = 1)
		{
			mSize += increments;
			ssize signedSize = increments;
			while (signedSize-- > 0)
			{
				pointer prev = where->mPrev;
				pointer next = where;

				pointer newNode = allocate(1);
				newNode->mPrev = prev;
				prev->mNext = newNode;
				newNode->mNext = next;
				next->mPrev = newNode;

				where = newNode;
			}
		}

		/*
		从where开始删除，总共删除decrements个（包括where在内）。返回被删除的最后一个节点其后面紧跟着的那个有效节点（或者尾节点）。
		*/
		template<typename Alloc>
		typename ListAllocatorHolder<Alloc>::pointer ListAllocatorHolder<Alloc>::decrementSize(pointer where, size_type decrements = 1)
		{
			ssize counter = decrements;

			while (counter-- > 0)
			{
				pointer prev = where->mPrev;
				pointer next = where->mNext;
				prev->mNext = next;
				next->mPrev = prev;
				where->~ListNode();
				deallocate(where, 1);
				where = next;
			}

			mSize -= decrements;

			return where;
		}

		template<typename Alloc>
		void ListAllocatorHolder<Alloc>::resize(size_type newSize)
		{
			resize(newSize, element_type());
		}

		template<typename Alloc>
		void ListAllocatorHolder<Alloc>::resize(size_type newSize,element_type const& elementValue)
		{
			if (newSize > mSize)
			{
				insert(mTail, newSize - mSize,elementValue);
			}
			else
			{
				decrementSize(mHead->mNext + newSize, mSize - newSize);
			}
		}

		template<typename Alloc>
		void ListAllocatorHolder<Alloc>::swap(self_type& right)
		{
			std::swap(mHead, right.mHead);
			std::swap(mTail, right.mTail);
			std::swap(mSize, right.mSize);
		}

		template<typename Alloc>
		typename ListAllocatorHolder<Alloc>::pointer 
			ListAllocatorHolder<Alloc>::insert(pointer where, element_type const& elementValue)
		{
			element_type tmp = elementValue;
			return insert(where, std::move(tmp));
		}

		template<typename Alloc>
		typename ListAllocatorHolder<Alloc>::pointer
			ListAllocatorHolder<Alloc>::insert(pointer where, element_type&& elementValue)
		{
			incrementSize(where);

			//在where指向的节点前面插入。
			return place(where->mPrev, std::move(elementValue));
		}

		template<typename Alloc>
		template<typename... Arguments>
		typename ListAllocatorHolder<Alloc>::pointer
			ListAllocatorHolder<Alloc>::insert(pointer where, Arguments&&... args)
		{
			value_type tmp((args)...);
			return insert(where, std::move(tmp.mData));
		}

		template<typename Alloc>
		template<typename iteratorT>
		typename boost::enable_if<std::_Is_iterator<iteratorT>, typename ListAllocatorHolder<Alloc>::pointer>::type
			ListAllocatorHolder<Alloc>::insert(pointer where, iteratorT first, iteratorT last,bool moveFlag)
		{
			pointer holder = where;
			while (first != last)
			{
				if (moveFlag)
				{
					holder = insert(holder, std::move(*first));
				}
				else
				{
					holder = insert(holder, *first);
				}
				++first;
			}

			return holder;
		}

		template<typename Alloc>
		typename ListAllocatorHolder<Alloc>::pointer
			ListAllocatorHolder<Alloc>::insert(pointer where, size_type count, const element_type& elementVal)
		{
			pointer holder = where;
			ssize c = count;
			while (--c)
			{
				holder = pirv_insert(holder, elementVal);
			}

			return holder;
		}

		/*
		在where指向的节点上build数据。
		*/
		template<typename Alloc>
		typename ListAllocatorHolder<Alloc>::pointer
			ListAllocatorHolder<Alloc>::place(pointer where, element_type&& elementValue)
		{
			::new(&where->mData) element_type(std::move(elementValue));

			//返回指针指向最后插入的node。
			return where;
		}
	}//namespace utl
}//namespace ung

#endif//_UNG_UTL_LIST_DEF_H_

/*
线性表---链式描述：
在链式描述中，线性表的元素在内存中的存储位置是随机的。每个元素都有一个明确的指针或链（指针和链是一个意思）指向线性表的下一个元素的位置（即地址）。
在基于数组的描述中，元素的地址是由数学公式决定的。而在链式描述中，元素的地址是随机分布的。
在链式描述中，数据对象实例的每一个元素都用一个单元或节点来描述。每一个节点都明确包含另一个相关节点的位置信息，这个信息称为链（link）或指针(pointer)。
链表是节点的集合，节点中存储着数据并链接到其它的节点。
节点的定义中用到了自身，因为数据成员mNext指向刚刚定义的，同样类型的节点。包含这种数据成员的对象称为自我引用对象。
*/