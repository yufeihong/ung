/*!
 * \brief
 * 消息系统。
 * 客户代码包含这个头文件。
 * \file UES.h
 *
 * \author Su Yang
 *
 * \date 2016/12/25
 */
#ifndef _UES_H_
#define _UES_H_

#ifndef _UES_ENABLE_H_
#include "UESEnable.h"
#endif

#ifdef UES_HIERARCHICAL_COMPILE

#ifndef _UES_BINDER_H_
#include "UESBinder.h"
#endif

#ifndef _UES_EVENTTYPE_H_
#include "UESEventType.h"
#endif

#ifndef _UES_IEVENTDATA_H_
#include "UESIEventData.h"
#endif

#ifndef _UES_EVENTDATA_H_
#include "UESEventData.h"
#endif

#ifndef _UES_EVENTMANAGER_H_
#include "UESEventManager.h"
#endif

#endif//UES_HIERARCHICAL_COMPILE

#endif//_UES_H_