/*!
 * \brief
 * 分配器。
 * \file UTLAllocator.h
 *
 * \author Su Yang
 *
 * \date 2016/01/27
 */
#ifndef _UNG_UTL_ALLOCATOR_H_
#define _UNG_UTL_ALLOCATOR_H_

#ifndef _UNG_UTL_CONFIG_H_
#include "UTLConfig.h"
#endif

namespace ung
{
	namespace utl
	{
		/*
		必须提供一下性质：
		1:一个value_type类型定义，它只是用来代表被传入的template参数类型。
		2：一个构造函数。
		3：一个template构造函数，用来在类型有所改变时复制内部状态。注意，template构造函数并不会抑制copy构造函数。
		4：一个成员函数allocate()，用来提供新的内存。
		5：一个成员函数deallocate(),用来释放不再用到的内存。
		6：构造函数和析构函数（如果必要的话），用来初始化、复制、清理内部状态。
		7：:操作符==和!=。
		无须提供construct()或destroy()，因为它们的默认实现（使用placement new初始化内存，以及明白调用析构函数以完成清理工作）通常已经足够了。
		*/
		/*!
		 * \brief
		 * UTL容器默认所使用的分配器。
		 * 容器通过对全局new和delete进行包装来管理容器所使用的内存空间。
		 * \class Allocator
		 *
		 * \author Su Yang
		 *
		 * \date 2016/01/27
		 *
		 * \todo
		 */
		template<typename T,typename ElementType = T>
		class Allocator
		{
		public:
			using value_type = T;//typename boost::remove_const<T>::type;
			using element_type = ElementType;//typename boost::remove_const<ElementType>::type;
			using allocator_type = Allocator;
			using pointer = typename boost::add_pointer<value_type>::type;
			BOOST_STATIC_ASSERT(boost::is_pointer<pointer>::value);
			using const_pointer = typename boost::add_pointer<typename boost::add_const<value_type>::type>::type;
			BOOST_STATIC_ASSERT(boost::is_same<const_pointer, value_type const*>::value);
			using reference = typename boost::add_lvalue_reference<value_type>::type;
			using const_reference = typename boost::add_lvalue_reference<typename boost::add_const<value_type>::type>::type;
			BOOST_STATIC_ASSERT(boost::is_same<const_reference,value_type const&>::value);
			using reference_r = typename boost::add_rvalue_reference<value_type>::type;
			BOOST_STATIC_ASSERT(boost::is_rvalue_reference<reference_r>::value);
			BOOST_STATIC_ASSERT(boost::is_same<reference_r,value_type&&>::value);
			using size_type = usize;
			using difference_type = ssize;

			/*!
			 * \remarks 默认构造函数
			 * \return 
			*/
			Allocator() noexcept
			{
			}

			/*!
			 * \remarks 拷贝构造函数
			 * \return 
			 * \param Allocator<U> const &
			*/
			template<typename U>
			Allocator(Allocator<U> const&) noexcept
			{
			}

			/*!
			* \remarks 分配内存空间
			* \return pointer 返回一个指向类型T的指针
			* \param usize num 申请的数量
			*/
			pointer allocate(usize num)
			{
				if(!num)
				{
					return nullptr;
				}
#ifdef _DEBUG
				usize typeSize = sizeof(value_type);
#endif
				return static_cast<pointer>(::operator new(sizeof(value_type) * num));
			}

			/*!
			 * \remarks 析构器
			 * \return void
			 * \param pointer p 被析构的指针
			 * \param usize num 被析构的对象数量
			*/
			void deallocate(pointer p, usize num)
			{
				if (num)
				{
					::operator delete(p);
				}
			}
		};
		/*!
		 * \remarks 比较两个不同类型的析构器是否相等
		 * \return bool
		 * \param Allocator<T1> const &
		 * \param Allocator<T2> const &
		*/
		template<typename T1,typename T2>
		bool operator==(Allocator<T1> const&, Allocator<T2> const&) noexcept
		{
			return true;
		}

		/*!
		 * \remarks 比较两个析构器是否不等
		 * \return bool
		 * \param Allocator<T1> const &
		 * \param Allocator<T2> const &
		*/
		template<typename T1, typename T2>
		bool operator!=(Allocator<T1> const&, Allocator<T2> const&) noexcept
		{
			return false;
		}

		/*!
		 * \remarks 交换两个析构器
		 * \return void
		 * \param Allocator<T1> const &
		 * \param Allocator<T2> const &
		*/
		template<typename T1,typename T2>
		void swap(Allocator<T1> const&, Allocator<T2> const&) noexcept
		{
		}
	}//namespace utl
}//namespace ung

#endif//_UNG_UTL_ALLOCATOR_H_

/*
借由Allocator，容器和算法的元素存储方式得以被参数化。

allocator基本操作：
a.allocate(num):
为num个元素分配内存。
a.construct(p,val):
将p所指的元素初始化为val。
a.destroy(p):
销毁p所指的元素。
a.deallocate(p,num):
回收p所指的“可容纳num个元素”的内存空间。

写一个自己的allocator并不很难。最重要的问题是你如何分配和回收空间，剩下的大多有默认行为并且往往已由C++11提供。
*/

/*
STL六大组件的交互关系：
Container通过Allocator取得数据存储空间。
Algorithm通过Iterator存取Container内容。
Functor可以协助Algorithm完成不同的策略变化。
Adapter可以修饰或套接Functor。
*/