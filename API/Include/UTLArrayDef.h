/*!
 * \brief
 * Array的函数定义。
 * \file UTLArrayDef.h
 *
 * \author Su Yang
 *
 * \date 2016/01/27
 */
#ifndef _UNG_UTL_ARRAY_DEF_H_
#define _UNG_UTL_ARRAY_DEF_H_

#ifndef _UNG_UTL_CONFIG_H_
#include "UTLConfig.h"
#endif

#ifndef _UNG_UTL_ARRAY_H_
#include "UTLArray.h"
#endif

#ifndef _INCLUDE_BOOST_ADDRESSOF_HPP_
#include "boost/utility/addressof.hpp"
#define _INCLUDE_BOOST_ADDRESSOF_HPP_
#endif

namespace ung
{
	namespace utl
	{
		template<typename T,ssize SIZE>
		typename Array<T, SIZE>::iterator Array<T, SIZE>::begin()
		{
			/*
			addressof:
			addressof是对取地址操作(&)的增强，因为C++允许重载operator&，所以有时候operator&会被程序员“欺骗”，但addressof总能够获取到操作对象
			的真实地址。
			有的时候某些类甚至把operator&声明为私有的，通常这种情况下无法使用&来获得变量的地址，但addressof不受影响。
			*/
			return iterator(boost::addressof(mContainer[0]));
		}
		
		template<typename T, ssize SIZE>
		typename Array<T, SIZE>::const_iterator Array<T, SIZE>::begin() const
		{
			return const_iterator(boost::addressof(mContainer[0]));
		}
		
		template<typename T, ssize SIZE>
		typename Array<T, SIZE>::const_iterator Array<T, SIZE>::cbegin() const
		{
			return const_iterator(mContainer);
		}
		
		template<typename T, ssize SIZE>
		typename Array<T, SIZE>::reverse_iterator Array<T, SIZE>::rbegin()
		{
			return reverse_iterator(end());
		}
		
		template<typename T, ssize SIZE>
		typename Array<T, SIZE>::const_reverse_iterator Array<T, SIZE>::rbegin() const
		{
			return const_reverse_iterator(end());
		}

		template<typename T, ssize SIZE>
		typename Array<T, SIZE>::const_reverse_iterator Array<T, SIZE>::crbegin() const
		{
			return const_reverse_iterator(end());
		}
		
		template<typename T, ssize SIZE>
		typename Array<T, SIZE>::iterator Array<T, SIZE>::end()
		{
			return iterator(boost::addressof(mContainer[SIZE]));
		}

		template<typename T, ssize SIZE>
		typename Array<T, SIZE>::const_iterator Array<T, SIZE>::end() const
		{
			return const_iterator(boost::addressof(mContainer[SIZE]));
		}

		template<typename T, ssize SIZE>
		typename Array<T, SIZE>::const_iterator Array<T, SIZE>::cend() const
		{
			return const_iterator(boost::addressof(mContainer[SIZE]));
		}

		template<typename T, ssize SIZE>
		typename Array<T, SIZE>::reverse_iterator Array<T, SIZE>::rend()
		{
			return reverse_iterator(begin());
		}

		template<typename T, ssize SIZE>
		typename Array<T, SIZE>::const_reverse_iterator Array<T, SIZE>::rend() const
		{
			return const_reverse_iterator(begin());
		}

		template<typename T, ssize SIZE>
		typename Array<T, SIZE>::const_reverse_iterator Array<T, SIZE>::crend() const
		{
			return const_reverse_iterator(begin());
		}
		
		template<typename T, ssize SIZE>
		typename Array<T, SIZE>::reference Array<T, SIZE>::operator[](size_type i)
		{
			BOOST_ASSERT_MSG(i < SIZE, "out of range");
			return mContainer[i];
		}
		
		template<typename T, ssize SIZE>
		typename Array<T, SIZE>::const_reference Array<T, SIZE>::operator[](size_type i) const
		{
			BOOST_ASSERT_MSG(i < SIZE, "out of range");
			return mContainer[i];
		}

		template<typename T, ssize SIZE>
		typename Array<T, SIZE>::reference Array<T, SIZE>::at(size_type i)
		{
			rangeCheck(i);
			return mContainer[i];
		}

		template<typename T, ssize SIZE>
		typename Array<T, SIZE>::const_reference Array<T, SIZE>::at(size_type i) const
		{
			rangeCheck(i);
			return mContainer[i];
		}
		
		template<typename T, ssize SIZE>
		typename Array<T, SIZE>::reference Array<T, SIZE>::front()
		{
			return mContainer[0];
		}

		template<typename T, ssize SIZE>
		typename Array<T, SIZE>::const_reference Array<T, SIZE>::front() const
		{
			return mContainer[0];
		}

		template<typename T, ssize SIZE>
		typename Array<T, SIZE>::reference Array<T, SIZE>::back()
		{
			return mContainer[SIZE - 1];
		}

		template<typename T, ssize SIZE>
		typename Array<T, SIZE>::const_reference Array<T, SIZE>::back() const
		{
			return mContainer[SIZE - 1];
		}
		
		template<typename T, ssize SIZE>
		typename Array<T, SIZE>::size_type Array<T, SIZE>::size()
		{
			return SIZE;
		}

		template<typename T, ssize SIZE>
		bool Array<T, SIZE>::empty()
		{
			return false;
		}

		template<typename T, ssize SIZE>
		typename Array<T, SIZE>::size_type Array<T, SIZE>::max_size()
		{
			return SIZE;
		}

		template<typename T, ssize SIZE>
		typename Array<T, SIZE>::pointer Array<T, SIZE>::data()
		{
			return mContainer;
		}

		template<typename T, ssize SIZE>
		typename Array<T, SIZE>::const_pointer Array<T, SIZE>::data() const
		{
			return mContainer;
		}

		template<typename T, ssize SIZE>
		typename Array<T, SIZE>::pointer Array<T, SIZE>::c_array()
		{
			return mContainer;
		}

		template<typename T, ssize SIZE>
		void Array<T, SIZE>::fill(const_reference v)
		{
			std::fill_n(mContainer, size(), v);
		}

		template<typename T, ssize SIZE>
		void Array<T, SIZE>::assign(const_reference v)
		{
			fill(v);
		}

		template<typename T, ssize SIZE>
		void Array<T, SIZE>::swap(Array<T, SIZE>& other)
		{
			for (size_type i = 0; i < SIZE; ++i)
			{
				boost::swap(mContainer[i], other.mContainer[i]);
			}
		}

		template<typename T, ssize SIZE>
		void Array<T, SIZE>::rangeCheck(size_type i)
		{
			if (i >= size())
			{
				//抛出异常
				std::out_of_range e("Array<>: index out of range");
				boost::throw_exception(e);
			}
		}
	}//namespace utl
}//namespace ung

#endif//_UNG_UTL_ARRAY_DEF_H_
