/*!
 * \brief
 * Vec3的定义。
 * \file UMLVec3Def.h
 *
 * \author Su Yang
 *
 * \date 2017/03/15
 */
#ifndef _UML_VEC3DEF_H_
#define _UML_VEC3DEF_H_

#ifndef _UML_CONFIG_H_
#include "UMLConfig.h"
#endif

#ifdef UML_HIERARCHICAL_COMPILE

#ifndef _UML_VEC3_H_
#include "UMLVec3.h"
#endif

//保存当前的编译器警告状态
#pragma warning(push)

//不是所有的控件路径都返回值
#pragma warning(disable : 4715)

namespace ung
{
	template<typename T>
	const Vec3<T> Vec3<T>::ZERO(0, 0, 0);
	template<typename T>
	const Vec3<T> Vec3<T>::UNIT_X(1, 0, 0);
	template<typename T>
	const Vec3<T> Vec3<T>::UNIT_Y(0, 1, 0);
	template<typename T>
	const Vec3<T> Vec3<T>::UNIT_Z(0, 0, 1);
	template<typename T>
	const Vec3<T> Vec3<T>::NEGATIVE_UNIT_X(-1, 0, 0);
	template<typename T>
	const Vec3<T> Vec3<T>::NEGATIVE_UNIT_Y(0, -1, 0);
	template<typename T>
	const Vec3<T> Vec3<T>::NEGATIVE_UNIT_Z(0, 0, -1);
	template<typename T>
	const Vec3<T> Vec3<T>::UNIT_SCALE(1, 1, 1);

	template<typename T>
	Vec3<T>::Vec3() :
		x(0.0),
		y(0.0),
		z(0.0)
	{
	}

	template<typename T>
	Vec3<T>::Vec3(const T& scaler) :
		Vec3(scaler, scaler, scaler)
	{
	}

	template<typename T>
	Vec3<T>::Vec3(const T& xx, const T& yy, const T& zz) :
		x(xx),
		y(yy),
		z(zz)
	{
	}

	template<typename T>
	Vec3<T>::Vec3(const T* const ar, const uint32 sz)
	{
		BOOST_ASSERT(sz == 3);
		x = ar[0];
		y = ar[1];
		z = ar[2];
	}

	template<typename T>
	Vec3<T>::Vec3(const T(&ar)[3]) :
		x(ar[0]),
		y(ar[1]),
		z(ar[2])
	{
	}

	template<typename T>
	Vec3<T>::Vec3(Vec2<T> const& v2, T zz) :
		x(v2.x),
		y(v2.y),
		z(zz)
	{
	}

	template<typename T>
	Vec3<T>::Vec3(const Vec3 &v) :
		x(v.x),
		y(v.y),
		z(v.z)
	{
	}

	template<typename T>
	Vec3<T>& Vec3<T>::operator=(const Vec3& v)
	{
		x = v.x;
		y = v.y;
		z = v.z;

		return *this;
	}

	template<typename T>
	Vec3<T>::Vec3(Vec3 && v) noexcept :
		x(v.x),
		y(v.y),
		z(v.z)
	{
	}

	template<typename T>
	Vec3<T>& Vec3<T>::operator=(Vec3 && v) noexcept
	{
		if (this != &v)
		{
			x = v.x;
			y = v.y;
			z = v.z;
		}

		return *this;
	}

	template<typename T>
	T& Vec3<T>::operator[](const usize i)
	{
		BOOST_ASSERT(i >= 0 && i <= 2);
		
		return *(&x + i);
	}

	template<typename T>
	const T& Vec3<T>::operator[](const usize i) const
	{
		BOOST_ASSERT(i >= 0 && i <= 2);
		
		return *(&x + i);
	}

	template<typename T>
	T* Vec3<T>::getAddress()
	{
		return &x;
	}

	template<typename T>
	T const* Vec3<T>::getAddress() const
	{
		return &x;
	}

	template<typename T>
	bool Vec3<T>::operator<(const Vec3& v) const
	{
		if (x < v.x && y < v.y && x < v.z)
		{
			return true;
		}

		return false;
	}

	template<typename T>
	bool Vec3<T>::operator>(const Vec3& v) const
	{
		if (x > v.x && y > v.y && x > v.z)
		{
			return true;
		}

		return false;
	}

	template<typename T>
	bool Vec3<T>::operator<=(const Vec3& v) const
	{
		return !(operator>(v));
	}

	template<typename T>
	bool Vec3<T>::operator>=(const Vec3& v) const
	{
		return !(operator<(v));
	}

	/*
	在几何学中，如果两个向量的长度和方向均相同，那么这两个向量相等。
	在代数学中，如果两个向量维数相同并且相应的分量也相等，则二者相等。
	*/
	template<typename T>
	bool Vec3<T>::operator==(const Vec3& v) const
	{
		return(math_type::isEqual(x,v.x) && math_type::isEqual(y,v.y) && math_type::isEqual(z,v.z));
	}

	template<typename T>
	bool Vec3<T>::operator!=(const Vec3& v) const
	{
		return !(operator==(v));
	}

	/*
	关于对构造函数的调用说明：
	1:
	return Vec3{x + v.x,y + v.y,z + v.z};
	只调用了一次构造函数
	2:
	return std::move(Vec3{x + v.x,y + v.y,z + v.z});
	调用了一次构造函数，一次移动构造函数
	3:
	Vec3<T> outVec3;
	outVec3.x = x + v.x;
	outVec3.y = y + v.y;
	outVec3.z = z + v.z;
	return outVec3;
	调用了一次默认构造函数，一次移动构造函数
	4:
	Vec3<T> outVec3;
	outVec3.x = x + v.x;
	outVec3.y = y + v.y;
	outVec3.z = z + v.z;
	return std::move(outVec3);
	调用了一次默认构造函数，一次移动构造函数

	分析：
	3和4一样，所以当返回一个局部对象的拷贝时，没有必要显式地使用std::move()。
	2在效率上，和3,4相同。
	1效率最高。
	结论：
	当在一个函数中要返回一个局部对象的拷贝时，直接return object_type{...};
	*/
	template<typename T>
	Vec3<T> Vec3<T>::operator+(const Vec3& v) const
	{
		return Vec3{x + v.x,y + v.y,z + v.z};
	}

	template<typename T>
	Vec3<T>& Vec3<T>::operator+=(const Vec3& v)
	{
		x += v.x;
		y += v.y;
		z += v.z;

		return *this;
	}

	template<typename T>
	Vec3<T> Vec3<T>::operator+(const T& scaler) const
	{
		return Vec3{x + scaler,y + scaler,z + scaler};
	}

	template<typename T>
	Vec3<T>& Vec3<T>::operator+=(const T& scaler)
	{
		x += scaler;
		y += scaler;
		z += scaler;

		return *this;
	}

	template<typename T>
	Vec3<T> Vec3<T>::operator-(const Vec3& v) const
	{
		return Vec3{x - v.x,y - v.y,z - v.z};
	}

	template<typename T>
	Vec3<T>& Vec3<T>::operator-=(const Vec3& v)
	{
		x -= v.x;
		y -= v.y;
		z -= v.z;

		return *this;
	}

	template<typename T>
	Vec3<T> Vec3<T>::operator-(const T& scaler) const
	{
		return Vec3{ x - scaler,y - scaler,z - scaler };
	}

	template<typename T>
	Vec3<T>& Vec3<T>::operator-=(const T& scaler)
	{
		x -= scaler;
		y -= scaler;
		z -= scaler;

		return *this;
	}

	template<typename T>
	Vec3<T> Vec3<T>::operator*(const Vec3& v) const
	{
		return Vec3{x * v.x,y * v.y,z * v.z};
	}

	template<typename T>
	Vec3<T>& Vec3<T>::operator*=(const Vec3& v)
	{
		x *= v.x;
		y *= v.y;
		z *= v.z;

		return *this;
	}

	template<typename T>
	Vec3<T> Vec3<T>::operator*(const T& scaler) const
	{
		return Vec3{x * scaler,y * scaler,z * scaler};
	}

	template<typename T>
	Vec3<T>& Vec3<T>::operator*=(const T& scaler)
	{
		x *= scaler;
		y *= scaler;
		z *= scaler;

		return *this;
	}

	template<typename T>
	Vec3<T> Vec3<T>::operator*(Mat3<T> const& mat3) const
	{
		return Vec3{x * mat3.m[0][0] + y * mat3.m[1][0] + z * mat3.m[2][0],
		x * mat3.m[0][1] + y * mat3.m[1][1] + z * mat3.m[2][1],
		x * mat3.m[0][2] + y * mat3.m[1][2] + z * mat3.m[2][2]};
	}

	template<typename T>
	Vec3<T>& Vec3<T>::operator*=(Mat3<T> const& mat3)
	{
		Vec3 outVec3{};

		outVec3.x = x * mat3.m[0][0] + y * mat3.m[1][0] + z * mat3.m[2][0];
		outVec3.y = x * mat3.m[0][1] + y * mat3.m[1][1] + z * mat3.m[2][1];
		outVec3.z = x * mat3.m[0][2] + y * mat3.m[1][2] + z * mat3.m[2][2];

		swap(outVec3);

		return *this;
	}

	template<typename T>
	Vec3<T> Vec3<T>::operator*(Mat4<T> const& mat4) const
	{
		return Vec3{ x * mat4.m[0][0] + y * mat4.m[1][0] + z * mat4.m[2][0],
		x * mat4.m[0][1] + y * mat4.m[1][1] + z * mat4.m[2][1],
		x * mat4.m[0][2] + y * mat4.m[1][2] + z * mat4.m[2][2] };
	}

	template<typename T>
	Vec3<T>& Vec3<T>::operator*=(Mat4<T> const& mat4)
	{
		Vec3 outVec3{};

		outVec3.x = x * mat4.m[0][0] + y * mat4.m[1][0] + z * mat4.m[2][0];
		outVec3.y = x * mat4.m[0][1] + y * mat4.m[1][1] + z * mat4.m[2][1];
		outVec3.z = x * mat4.m[0][2] + y * mat4.m[1][2] + z * mat4.m[2][2];

		swap(outVec3);

		return *this;
	}

	template<typename T>
	Vec3<T> Vec3<T>::operator*(Quater<T> const& q) const
	{
		return Vec3{q * (*this)};
	}

	template<typename T>
	Vec3<T>& Vec3<T>::operator*=(Quater<T> const& q)
	{
		Vec3<T> temp = q * (*this);

		swap(temp);

		return *this;
	}

	template<typename T>
	Vec3<T> Vec3<T>::operator*(SQTImpl<T> const& sqt) const
	{
		Vec3 outVec3;

		//旋转
		outVec3 = sqt.getRotate() * (*this);
		//缩放
		outVec3 *= sqt.getScale();

		//忽略平移

		return std::move(outVec3);
	}

	template<typename T>
	Vec3<T>& Vec3<T>::operator*=(SQTImpl<T> const& sqt)
	{
		Vec3 temp = (*this) * sqt;

		swap(temp);

		return *this;
	}

	template<typename T>
	Vec3<T> Vec3<T>::operator/(const Vec3& v) const
	{
		BOOST_ASSERT(!math_type::isZero(v.x) && 
			!math_type::isZero(v.y) && 
			!math_type::isZero(v.z));

		return Vec3{x / v.x,y / v.y,z / v.z};
	}

	template<typename T>
	Vec3<T>& Vec3<T>::operator/=(const Vec3& v)
	{
		BOOST_ASSERT(!math_type::isEqual(v.x,0.0) && !math_type::isEqual(v.y,0.0) && !math_type::isEqual(v.z,0.0));

		x /= v.x;
		y /= v.y;
		z /= v.z;

		return *this;
	}

	template<typename T>
	Vec3<T> Vec3<T>::operator/(const T& scaler) const
	{
		BOOST_ASSERT(!math_type::isZero(scaler));

		T inv = 1.0 / scaler;

		return operator*(inv);
	}

	template<typename T>
	Vec3<T>& Vec3<T>::operator/=(const T& scaler)
	{
		BOOST_ASSERT(!math_type::isZero(scaler));

		T inv = 1.0 / scaler;
		x *= inv;
		y *= inv;
		z *= inv;

		return *this;
	}

	template<typename T>
	Vec3<T> Vec3<T>::operator-() const
	{
		return Vec3(-x, -y, -z);
	}

	template<typename T>
	void Vec3<T>::setZero()
	{
		x = y = z = 0.0;
	}

	template<typename T>
	void Vec3<T>::swap(Vec3& other)
	{
		std::swap(x, other.x);
		std::swap(y, other.y);
		std::swap(z, other.z);
	}

	template<typename T>
	T Vec3<T>::length() const
	{
		return math_type::calSqrt(x * x + y * y + z * z);
	}

	template<typename T>
	T Vec3<T>::squaredLength() const
	{
		/*
		向量自身的点积结果为向量长度的平方。
		*/
		return (x * x + y * y + z * z);
	}

	template<typename T>
	T Vec3<T>::distance(const Vec3& p) const
	{
		return (*this - p).length();
	}

	template<typename T>
	T Vec3<T>::squaredDistance(const Vec3& p) const
	{
		return (*this - p).squaredLength();
	}

	/*
	u · v = ux * vx + uy * vy + uz * vz = s
	上述公式并不具有明显的几何意义。但由余弦定理，可以发现u · v = |u| * |v| * cosθ，即两个向量的点积等于二者夹角的余弦再乘以两个向量的模的乘积。由此可知，
	如果u和v都是单位向量，则u · v就等于u，v夹角的余弦。
	1，若s = 0，则u和v垂直
	2，若s > 0，则两向量之间的夹角小于90°
	3，若s < 0，则两向量之间的夹角大于90°
	4，若s = 1，则u和v平行
	*/
	template<typename T>
	T Vec3<T>::dotProduct(const Vec3& v) const
	{
		return (x * v.x + y * v.y + z * v.z);
	}

	template<typename T>
	const Vec3<T> Vec3<T>::projectionComponent(const Vec3& v) const
	{
		BOOST_ASSERT(!isZeroLength());
		BOOST_ASSERT(!v.isZeroLength());

		//如果向量v是单位向量
		if (v.isUnitLength())
		{
			//返回当前向量和向量v的点积(这是一个标量，代表了投影向量的长度)的结果，然后给这个标量乘以单位向量v，以使其具有方向。
			return dotProduct(v) * v;
		}

		//向量v不是单位向量
		T len2 = v.squaredLength();
		BOOST_ASSERT(len2 != 0.0);

		//必须要除去向量v的长度
		return dotProduct(v) * v / len2;
	}

	template<typename T>
	const Vec3<T> Vec3<T>::perpendicularComponent(const Vec3& v) const
	{
		return *this - projectionComponent(v);
	}

	/*
	向量的规范化（normalizing）就是使向量的模变为1，即变为单位向量。
	可以通过将向量的每个分量都除以该向量的模来实现向量的规范化。
	*/
	template<typename T>
	void Vec3<T>::normalize()
	{
		BOOST_ASSERT(!isZeroLength());

		//如果当前向量原本就是单位向量
		if (isUnitLength())
		{
			return;
		}

		auto len = length();

		BOOST_ASSERT(!math_type::isEqual(len,0.0));

		auto invLen = 1.0 / len;
		x *= invLen;
		y *= invLen;
		z *= invLen;

		BOOST_ASSERT(isUnitLength());
	}

	template<typename T>
	Vec3<T> Vec3<T>::normalizeCopy() const
	{
		Vec3 ret = *this;
		ret.normalize();

		return ret;
	}

	template<typename T>
	Vec3<T> Vec3<T>::midPoint(const Vec3& p) const
	{
		return Vec3((x + p.x) * 0.5, (y + p.y) * 0.5, (z + p.z) * 0.5);
	}

	template<typename T>
	void Vec3<T>::setFloor(const Vec3& v)
	{
		if (v.x < x)
		{
			x = v.x;
		}
		if (v.y < y)
		{
			y = v.y;
		}
		if (v.z < z)
		{
			z = v.z;
		}
	}

	template<typename T>
	void Vec3<T>::setCeil(const Vec3& v)
	{
		if (v.x > x)
		{
			x = v.x;
		}
		if (v.y > y)
		{
			y = v.y;
		}
		if (v.z > z)
		{
			z = v.z;
		}
	}

	template<typename T>
	Vec3<T> Vec3<T>::perpendicular() const
	{
		Vec3 ret = this->crossProductUnit(Vec3(1, 0, 0));
		//如果与x轴平行
		if(ret.isParallel(Vec3(1, 0, 0)))
		{
			ret = this->crossProductUnit(Vec3(0, 1, 0));
		}

		ret.normalize();

		return ret;
	}

	/*
	叉积（cross product）的结果是另一个向量。
	如果取向量u和v的叉积，运算所得的向量p与u，v彼此正交（orthogonal，是垂直perpendicular的同义语）。
	叉积的运算规则如下：
	p = u x v = [(uy * vz - uz * vy),(uz * vx - ux * vz),(ux * vy - uy * vx)]
	向量-p也与向量u，v同时正交。
	u x v = -(v x u)，表明叉积不具备交换性。
	右手法则：如果右手手指沿着第一个向量向第二个向量的方向弯曲，拇指的指向就是这两个向量叉积的方向。
	|u x v| = |u||v|sinθ，其中θ为u和v之间的夹角。
	*/
	template<typename T>
	Vec3<T> Vec3<T>::crossProduct(const Vec3& v) const
	{
		return Vec3(y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x);
	}

	template<typename T>
	Vec3<T> Vec3<T>::crossProductUnit(const Vec3& v) const
	{
		auto ret = crossProduct(v);

		//如果叉积结果表明当前向量与给定向量不是互相平行
		if (!ret.isZeroLength())
		{
			//对叉积结果进行规范化
			ret.normalize();
		}

		return ret;
	}

	template<typename T>
	Vec3<T> Vec3<T>::reflect(const Vec3& normal) const
	{
		BOOST_ASSERT(!isZeroLength());
		BOOST_ASSERT(!normal.isZeroLength());

		Vec3 Nn = normal.normalizeCopy();

		//当前向量在法线上的投影分向量
		auto vecProj = projectionComponent(Nn);
		//让投影分向量延长为其自身的2倍
		auto doubleVecProj = vecProj * 2.0;
		//用延长后的投影向量减去当前向量
		auto ret = doubleVecProj - *this;

		ret.normalize();

		return ret;
	}

	template<typename T>
	bool Vec3<T>::isParallel(const Vec3& v) const
	{
		BOOST_ASSERT(!isZeroLength());
		BOOST_ASSERT(!v.isZeroLength());

		auto curN = normalizeCopy();
		Vec3 vN = v.normalizeCopy();

		//两个向量平行，那么其点积为1.0
		auto dotRet = curN.dotProduct(vN);

		if (math_type::isEqual(dotRet,1.0))
		{
			//再次确认，如果两个向量平行，那么其叉积长度为0.0
			BOOST_ASSERT(math_type::isEqual((curN.crossProductUnit(vN)).squaredLength(), 0.0));

			return true;
		}

		return false;
	}

	template<typename T>
	Radian Vec3<T>::angleBetween(const Vec3& v) const
	{
		BOOST_ASSERT(!isZeroLength());
		BOOST_ASSERT(!v.isZeroLength());

		auto curUnit = this->normalizeCopy();
		auto vUnit = v.normalizeCopy();

		auto dotRet = curUnit.dotProduct(vUnit);

		return Radian(math_type::calACos(dotRet));
	}

	template<typename T>
	bool Vec3<T>::isZeroLength() const
	{
		return math_type::isEqual(squaredLength(), 0.0);
	}

	template<typename T>
	bool Vec3<T>::isUnitLength() const
	{
		return math_type::isEqual(squaredLength(), 1.0);
	}

	template<typename T>
	Vec3<T> Vec3<T>::primaryAxis() const
	{
		T absx = math_type::calAbs(x);
		T absy = math_type::calAbs(y);
		T absz = math_type::calAbs(z);
		if (absx > absy)
		{
			if (absx > absz)
			{
				return x > 0 ? Vec3(1.0, 0.0, 0.0) : Vec3(-1.0, 0.0, 0.0);
			}
			else
			{
				return z > 0 ? Vec3(0.0, 0.0, 1.0) : Vec3(0.0, 0.0, -1.0);
			}
		}
		else//absy >= absx
		{
			if (absy > absz)
			{
				return y > 0 ? Vec3(0.0, 1.0, 0.0) : Vec3(0.0, -1.0, 0.0);
			}
			else
			{
				return z > 0 ? Vec3(0.0, 0.0, 1.0) : Vec3(0.0, 0.0, -1.0);
			}
		}
	}

	template<typename T>
	Vec3<T> Vec3<T>::lerp(Vec3 const& start, Vec3 const& end, T const& t)
	{
		Vec3 outVec3 = (1.0 - t) * start + t * end;

		return outVec3;
	}
}//namespace ung

//恢复原先的警告状态
#pragma warning(pop)

#endif//UML_HIERARCHICAL_COMPILE

#endif//_UML_VEC3DEF_H_

 /*
 叉积仅可应用于3D向量.
 当点积和叉积在一起时,叉积优先计算:a . b X c = a . (b X c),因为点积返回一个标量,同时标量和向量间不能叉乘.
 运算a . (b X c)称作三重积.

 叉乘不满足交换律.事实上,它满足反交换律:a X b = -(b X a).
 叉乘也不满足结合律.(a X b) X c != a X (b X c).
 标量乘法对叉积的结合律:k(a X b) = (ka) X b = a X (kb)
 叉积对向量加法的分配律:a X (b + c) = a X b + a X c
 任意向量与自身的叉乘等于零向量:a X a = 0
 向量与另一向量的叉乘再点乘该向量本身等于0:a . (a X b) = 0

 |P X Q| = |P| * |Q| * sin(alpha) = 向量P和Q组成的平行四边形的面积.
 因此,对于顶点为v1,v2和v3的任意一个三角形,利用下面的公式可以计算出其面积A:
 A = 0.5 * |(v2 - v1) X (v3 - v1)|
 叉积得到的非零向量一定垂直于进行叉积运算的两个向量,但满足该要求的向量的方向有两种.
 叉积的方向,遵循右手法则.

 如果P,Q平行或任意一个为0,则P X Q = 0.叉积对零向量的解释为:零向量平行于任意其他向量.注意,这和点积的解释不同,点积的解释是和
 任意其他向量垂直.

 斜对称矩阵(Skew Symmetric Matrix):
 对角元素都是0,关于对角元素对称位置上的元素恰好为相反数.M(ij) = -M(ji)
 本身加上转置等于零矩阵.

 斜对称矩阵可以用来表达叉乘:
 a X b = c,那么可以构造一个斜对称矩阵S,使得a * S = c
 设a:(ax,ay,az),b(bx,by,bz),那么c为:
 cx = ay * bz - az * by;
 cy = az * bx - ax * bz;
 cz = ax * by - ay * bx;
 接着,我们把c给重新布局一下:
 cx = ax *    0     + ay *    bz   + az * (-by);
 cy = ax * (-bz)  + ay *     0    + az *   bx;
 cz = ax *    by    + ay * (-bx)  + az *    0;
 因为a和S进行相乘的时候,ax每次都是和S的每一列的第一个元素进行相乘,所以S的第一行为:0   -bz   by,同理,
 这样,就可以构造S为:
 0		-bz		by
 bz	0			-bx
 -by	bx			0
 经验证a * S = c
 经验证,发现斜对称矩阵的行列式为0,所以其没有逆矩阵.

 这就相当于把一个向量a和向量b的叉乘给转换成了向量a和矩阵S的相乘.也可以说,用一个矩阵替换了一个向量.
 */

 /*
 operators库使用泛型编程的"基类链"技术解决了多重继承的问题,这种技术通过模板把多继承转换为链式的单继承.

 boost::equality_comparable:要求提供==,可自动实现 !=
 boost::less_than_comparable : 要求提供 < ,可自动实现 >,<=,>=
 boost::addable : 要求提供 += ,可自动实现 +
 boost::subtractable : 要求提供 -= ,可自动实现 -
 boost::incrementable : 要求提供前置 ++,可自动实现后置 ++
 boost::decrementable : 要求提供前置 -- ,可自动实现后置 --
 boost::equivalent : 要求提供 < ,可自动实现 ==的等价语义

 class Vec3 : boost::equality_comparable<Vec3>,
 boost::less_than_comparable<Vec3>,
 boost::addable<Vec3>,
 boost::subtractable<Vec3>
 或者
 class Vec3 : boost::equality_comparable<Vec3,
 boost::less_than_comparable<Vec3,
 boost::addable<Vec3,
 boost::subtractable<Vec3>>>>

 Vec3作为boost::equality_comparable的模板类型参数,只是用来实现内部的比较操作符,用作操作符函数的类型,没有任何继承关系.
 模板类型参数必须是子类自身,特别是当子类本身也是个模板类的时候,例如假设:
 template<typename T>
 class Vec3
 {
 }
 那么就应该这样写:
 template<typename T>
 class Vec3 : boost::equality_comparable<Vec3<T>>,因为只有Vec3<T>才是模板类Vec3的全名.

 复合运算
 boost::totally_ordered:全序概念,组合了equality_comparable和less_than_comparable
 boost::additive:可加减概念,组合了addable和subtractable
 boost::multiplicative:可乘除概念,组合了multipliable和dividable
 boost::arithmetic:算术运算概念,组合了additive和multiplicative
 boost::unit_stoppable:可步进概念,组合了incrementable和decrementable
 一般情况下,类型T继承totally_ordered<T>,再定义<,==操作符即可获得完全的比较运算功能,能够用于标准容器和算法.
 继承arithmetic和arithmetic2,再定义+=,-=,*=,/*.即可实现+,-,*,/。但是这会导致对象增加4字节的数据。
 */

/*
变换：transformation
向量的两个重要属性是长度（也称为大小或模）和方向。
因为向量的属性中不含有位置（location）信息，所以两个向量只要长度和方向相同，无论其起点是否相同，我们认为二者相等。
当某一向量的起始端与坐标原点重合时，我们称该向量处于标准位置。
R^3的标准基向量：i,j,k，其方向分别与坐标系中的x,y,z轴一致，且长度均为1：
i = (1,0,0)
j = (0,1,0)
k = (0,0,1)
长度为1的向量称为单位向量（unit vector）。
向量的数学运算除了叉积（cross product）外都可以推广至任意维数的向量，其中叉积只在R^3中有定义。
向量减法：u - v。返回一个自v的末端指向u的末端的向量。如果我们把u和v的分量理解为点的坐标，便可使用向量减法求得自一点指向另一点的向量。
*/