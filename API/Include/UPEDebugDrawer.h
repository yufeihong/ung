/*!
 * \brief
 * draw debug information.
 * \file UPEDebugDrawer.h
 *
 * \author Su Yang
 *
 * \date 2016/12/04
 */
#ifndef _UPE_DEBUGDRAWER_H_
#define _UPE_DEBUGDRAWER_H_

#ifndef _UPE_CONFIG_H_
#include "UPEConfig.h"
#endif

#ifdef UPE_HIERARCHICAL_COMPILE

namespace ung
{
	/*!
	 * \brief
	 * edges of objects, the points of collision, the depth of penetration(渗透，穿透), or more.
	 * \class DebugDrawer
	 *
	 * \author Su Yang
	 *
	 * \date 2016/12/04
	 *
	 * \todo
	 */
	class DebugDrawer : public btIDebugDraw
	{
	public:
		/*!
		 * \remarks 设置debug mode
		 * \return 
		 * \param int debugMode
		*/
		virtual UngExport void setDebugMode(int debugMode) override;

		/*!
		 * \remarks 获取debug mode
		 * \return 
		*/
		virtual UngExport int getDebugMode() const override;
		
		/*!
		 * \remarks 在两个接触点之间画线
		 * \return 
		 * \param const btVector3 & pointOnB
		 * \param const btVector3 & normalOnB
		 * \param btScalar distance
		 * \param int lifeTime
		 * \param const btVector3 & color
		*/
		virtual UngExport void drawContactPoint(const btVector3 &pointOnB, const btVector3 &normalOnB,
			btScalar distance, int lifeTime, const btVector3 &color) override;

		/*!
		 * \remarks 画线
		 * \return 
		 * \param const btVector3 & from
		 * \param const btVector3 & to
		 * \param const btVector3 & color
		*/
		virtual UngExport void drawLine(const btVector3 &from, const btVector3 &to, const btVector3 &color) override;
		
		/*!
		 * \remarks override
		 * \return 
		 * \param const char * warningString
		*/
		virtual UngExport void reportErrorWarning(const char* warningString) override;

		/*!
		 * \remarks override
		 * \return 
		 * \param const btVector3 & location
		 * \param const char * textString
		*/
		virtual UngExport void draw3dText(const btVector3 &location, const char* textString) override;
		
		/*!
		 * \remarks 
		 * \return 
		 * \param int flag
		*/
		void UngExport toggleDebugFlag(int flag);
		
	protected:
		int mDebugMode;
	};
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

#endif//_UPE_DEBUGDRAWER_H_