/*!
 * \brief
 * 定位new。
 * \file UTLConstruct.h
 *
 * \author Su Yang
 *
 * \date 2016/02/22
 */
#ifndef _UNG_UTL_CONSTRUCT_H_
#define _UNG_UTL_CONSTRUCT_H_

#ifndef _UNG_UTL_CONFIG_H_
#include "UTLConfig.h"
#endif

namespace ung
{
	namespace utl
	{
		template<typename T>
		inline void construct(T* p)
		{
			::new ((void*)p) T();
		}

		/*!
		 * \remarks 就地构造
		 * \return 
		 * \param T1 * p
		 * \param T2 const & value
		*/
		template<typename T1,typename T2>
		inline void construct(T1* p, T2 const& value)
		{
			::new ((void*)p) T1(value);
		}

		/*!
		 * \remarks 就地emplace构造，
		 * \return 
		 * \param T * p
		 * \param Arguments & & ... args
		*/
		template<typename T, typename... Arguments>
		inline void construct(T* p, Arguments&&... args)
		{
			::new ((void*)p) T((args)...);
		}

		/*!
		 * \remarks 析构
		 * \return 
		 * \param T * p
		*/
		template<typename T>
		inline void destroy(T* p)
		{
			p->~T();
		}

		/*!
		 * \remarks 析构给定范围中的每一个元素
		 * \return 
		 * \param ForwardIterator first
		 * \param ForwardIterator last
		*/
		template <typename ForwardIterator>
		inline void destroy(ForwardIterator first, ForwardIterator last)
		{
			/*
			iterator_value<I>:返回迭代器所指的值类型。
			*/
			destroy_(first, last, boost::iterator_value<first>::type);
		}

		template <typename ForwardIterator, typename T>
		inline void destroy_(ForwardIterator first, ForwardIterator last, T*)
		{
			using trivial_destructor = boost::has_trivial_destructor<T>;
			destroy_aux_(first, last, trivial_destructor());
		}

		template <typename ForwardIterator>
		void destroy_aux_(ForwardIterator first, ForwardIterator last, boost::false_type)
		{
			for (; first != last; ++first)
			{
				destroy(&*first);
			}
		}

		template <typename ForwardIterator>
		inline void destroy_aux_(ForwardIterator, ForwardIterator, boost::true_type)
		{
		}
	}//namespace utl
}//namespace ung

#endif//_UNG_UTL_CONSTRUCT_H_