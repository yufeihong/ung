/*!
 * \brief
 * 元函数
 * \file UFCMetaFunction.h
 *
 * \author Su Yang
 *
 * \date 2016/04/24
 */
#ifndef _UFC_METAFUNCTION_H_
#define _UFC_METAFUNCTION_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#ifndef _INCLUDE_BOOST_POINTEE_HPP_
#include "boost/pointee.hpp"
#define _INCLUDE_BOOST_POINTEE_HPP_
#endif

namespace ung
{
	namespace utp
	{
		/*!
		 * \brief
		 * 用于获取一个编译期静态常量
		 * \class StaticConstValue
		 *
		 * \author Su Yang
		 *
		 * \date 2017/03/20
		 *
		 * \todo
		 */
		template<typename T,T v>						//非类型实参
		struct StaticConstValue
		{
			static T const value = v;
		};

		/*!
		 * \brief
		 * 把一个常整数映射为一个型别，Int2Type<0>不同于Int2Type<1>，用于静态分派
		 * \class Int2Type
		 *
		 * \author Su Yang
		 *
		 * \date 2016/04/24
		 *
		 * \todo
		 */
		template <int v>
		struct Int2Type
		{
			enum { value = v };
		};

		/*!
		 * \brief
		 * 型别代表物，不同型别足以区分各个Type2Type实体，用于传给重载函数
		 * \class Type2Type
		 *
		 * \author Su Yang
		 *
		 * \date 2016/04/24
		 *
		 * \todo
		 */
		template <typename T>
		struct Type2Type
		{
			typedef T OriginalType;
		};

		/*!
		 * \brief
		 * 根据一个bool变量来选择两个型别中的某一个
		 * Invocation: Select<flag, T, U>::result
		 * flag is a compile-time boolean constant,T and U are types,result evaluates to T if flag is true, and to U otherwise
		 * \class Select
		 *
		 * \author Su Yang
		 *
		 * \date 2016/04/24
		 *
		 * \todo
		 */
		template <bool flag, typename T, typename U>
		struct Select
		{
			typedef T result;
		};

		template <typename T, typename U>
		struct Select<false, T, U>
		{
			typedef U result;
		};

		/*!
		 * \brief
		 * Return true if two given types are the same
		 * Invocation: SameType<T, U>::value
		 * T and U are types,Result evaluates to true if U == T (types equal)
		 * \class IsSameType
		 *
		 * \author Su Yang
		 *
		 * \date 2016/04/24
		 *
		 * \todo
		 */
		template <typename T, typename U>
		struct IsSameType
		{
			enum { value = false };
		};

		template <typename T>
		struct IsSameType<T, T>
		{
			enum { value = true };
		};

		namespace
		{
			/*!
			 * \brief
			 * 保证sizeof(Small) < sizeof(Big)
			 * \class ConversionHelper
			 *
			 * \author Su Yang
			 *
			 * \date 2016/04/24
			 *
			 * \todo
			 */
			template <class T, class U>
			struct ConversionHelper
			{
				typedef char Small;
				struct Big
				{
					char dummy[2];
				};
				static Big Test(...);
				static Small Test(U);
				static T MakeT();																							//稻草人函数
			};
		}

		/*!
		 * \brief
		 * 侦测可转换性，从T到U
		 * 用法：(T and U are types):
		 * a) Conversion<T, U>::exists
		 * returns (at compile time) true if there is an implicit conversion from T to U (example: Derived to Base)
		 * b) Conversion<T, U>::exists2Way
		 * returns (at compile time) true if there are both conversions from T to U and from U to T (example: int to char and back)
		 * c) Conversion<T, U>::sameType
		 * returns (at compile time) true if T and U represent the same type
		 * Caveat(警告):might not work if T and U are in a private inheritance hierarchy
		 * \class Conversion
		 *
		 * \author Su Yang
		 *
		 * \date 2016/04/24
		 *
		 * \todo
		 */
		template <class T, class U>
		struct Conversion
		{
			typedef ConversionHelper<T, U> H;
			enum { exists = sizeof(typename H::Small) == sizeof((H::Test(H::MakeT()))) };
			enum { exists2Way = exists && Conversion<U, T>::exists };
			enum { sameType = false };
		};

		template <class T>
		struct Conversion<T, T>
		{
			enum { exists = 1, exists2Way = 1, sameType = 1 };
		};

		template <class T>
		struct Conversion<void, T>
		{
			enum { exists = 0, exists2Way = 0, sameType = 0 };
		};

		template <class T>
		struct Conversion<T, void>
		{
			enum { exists = 0, exists2Way = 0, sameType = 0 };
		};

		template <>
		struct Conversion<void, void>
		{
		public:
			enum { exists = 1, exists2Way = 1, sameType = 1 };
		};

		/*!
		 * \brief
		 * 侦测继承性
		 * 用法: SuperSubclass<B, D>::value where B and D are types
		 * 如果D是public继承自B，或二者是同一型别，那么返回true
		 * Caveat: might not work if T and U are in a private inheritance hierarchy
		 * \class SuperSubclass
		 *
		 * \author Su Yang
		 *
		 * \date 2016/04/24
		 *
		 * \todo
		 */
		template <class T, class U>
		struct SuperSubclass
		{
			enum
			{
				value = (Conversion<const volatile U*, const volatile T*>::exists && !Conversion<const volatile T*, const volatile void*>::sameType)
			};

			//Dummy enum to make sure that both classes are fully defined.
			enum { dontUseWithIncompleteTypes = (sizeof(T) == sizeof(U)) };
		};

		template <>
		struct SuperSubclass<void, void>
		{
			enum { value = false };
		};

		template <class U>
		struct SuperSubclass<void, U>
		{
			enum
			{
				value = (Conversion<const volatile U*, const volatile void*>::exists && !Conversion<const volatile void*, const volatile void*>::sameType)
			};

			//Dummy enum to make sure that both classes are fully defined.
			enum { dontUseWithIncompleteTypes = (0 == sizeof(U)) };
		};

		template <class T>
		struct SuperSubclass<T, void>
		{
			enum
			{
				value = (Conversion<const volatile void*, const volatile T*>::exists && !Conversion<const volatile T*, const volatile void*>::sameType)
			};

			//Dummy enum to make sure that both classes are fully defined.
			enum { dontUseWithIncompleteTypes = (sizeof(T) == 0) };
		};

		/*!
		 * \brief
		 * Invocation: SuperSubclassStrict<B, D>::value where B and D are types
		 * Returns true if B is a public base of D
		 * Caveat: might not work if T and U are in a private inheritance hierarchy
		 * \class SuperSubclassStrict
		 *
		 * \author Su Yang
		 *
		 * \date 2016/04/24
		 *
		 * \todo
		 */
		template<class T, class U>
		struct SuperSubclassStrict
		{
			enum
			{
				value = (Conversion<const volatile U*, const volatile T*>::exists &&
								!Conversion<const volatile T*, const volatile void*>::sameType &&
								!Conversion<const volatile T*, const volatile U*>::sameType)
			};

			//Dummy enum to make sure that both classes are fully defined.
			enum { dontUseWithIncompleteTypes = (sizeof(T) == sizeof(U)) };
		};

		template<>
		struct SuperSubclassStrict<void, void>
		{
			enum { value = false };
		};

		template<class U>
		struct SuperSubclassStrict<void, U>
		{
			enum
			{
				value = (Conversion<const volatile U*, const volatile void*>::exists &&
								!Conversion<const volatile void*, const volatile void*>::sameType &&
								!Conversion<const volatile void*, const volatile U*>::sameType)
			};

			//Dummy enum to make sure that both classes are fully defined.
			enum { dontUseWithIncompleteTypes = (0 == sizeof(U)) };
		};

		template<class T>
		struct SuperSubclassStrict<T, void>
		{
			enum
			{
				value = (Conversion<const volatile void*, const volatile T*>::exists &&
								!Conversion<const volatile T*, const volatile void*>::sameType &&
								!Conversion<const volatile T*, const volatile void*>::sameType)
			};

			//Dummy enum to make sure that both classes are fully defined.
			enum { dontUseWithIncompleteTypes = (sizeof(T) == 0) };
		};

		/*
		Invocation: UFC_SUPERSUBCLASS(B, D) where B and D are types
		Returns true if B is a public base of D, or if B and D are aliases of the same type
		Caveat(警告): might not work if T and U are in a private inheritance hierarchy
		*/
		#define UFC_SUPERSUBCLASS(B, D) utp::SuperSubclass<B,D>::value

		/*
		Invocation: UFC_SUPERSUBCLASS_STRICT(B, D) where B and D are types
		Returns true if B is a public base of D
		Caveat: might not work if T and U are in a private inheritance hierarchy
		*/
		#define UFC_SUPERSUBCLASS_STRICT(B, D) utp::SuperSubclassStrict<B,D>::value

		/*!
		 * \brief
		 * 是否是std::shared_ptr
		 * \class isSharedPtr
		 *
		 * \author Su Yang
		 *
		 * \date 2016/11/18
		 *
		 * \todo
		 */
		template <typename T>
		struct isSharedPtr : boost::mpl::false_
		{
		};

		template <typename T>
		struct isSharedPtr<std::shared_ptr<T>> : boost::mpl::true_
		{
		};
		/*
		用法：
		class Dummy
		{
		};

		template<typename T>
		void check(T)
		{
			std::cout << std::boolalpha << isSharedPtr<T>::value << std::endl;
		}

		int main()
		{
			Dummy dummy;
			Dummy* dummyPtr = &dummy;
			std::shared_ptr<Dummy> sharedDummy1(new Dummy);
			std::shared_ptr<Dummy> sharedDummy2 = std::make_shared<Dummy>();

			check(dummy);								//false
			check(dummyPtr);							//false
			check(sharedDummy1);					//true
			check(sharedDummy2);					//true

			return 0;
		}
		*/

		/*!
		 * \brief
		 * 是否是std::weak_ptr
		 * \class isWeakPtr
		 *
		 * \author Su Yang
		 *
		 * \date 2016/11/18
		 *
		 * \todo
		 */
		template <typename T>
		struct isWeakPtr : boost::mpl::false_
		{
		};

		template <typename T>
		struct isWeakPtr<std::weak_ptr<T>> : boost::mpl::true_
		{
		};

		/*!
		 * \brief
		 * 是否是std::unique_ptr
		 * \class isUniquePtr
		 *
		 * \author Su Yang
		 *
		 * \date 2016/11/18
		 *
		 * \todo
		 */
		template <typename T>
		struct isUniquePtr : boost::false_type
		{
		};

		template <typename T>
		struct isUniquePtr<std::unique_ptr<T>> : boost::true_type
		{
		};

		/*!
		 * \brief
		 * 是否是标准库智能指针
		 * \class isSmartPtr
		 *
		 * \author Su Yang
		 *
		 * \date 2017/04/22
		 *
		 * \todo
		 */
		template <typename T>
		struct isSmartPtr : boost::false_type
		{
		};

		template <typename T>
		struct isSmartPtr<std::shared_ptr<T>> : boost::true_type
		{
		};

		template <typename T>
		struct isSmartPtr<std::weak_ptr<T>> : boost::true_type
		{
		};

		template <typename T>
		struct isSmartPtr<std::unique_ptr<T>> : boost::true_type
		{
		};

		/*!
		 * \brief
		 * 获取标准模板库智能指针所引用的实际类型
		 * \class SharedType
		 *
		 * \author Su Yang
		 *
		 * \date 2017/03/08
		 *
		 * \todo
		 */
		template<typename T>
		struct SharedType
		{
			//T必须是std::shared_ptr
			BOOST_STATIC_ASSERT(utp::isSharedPtr<T>::value);

			using element_type = typename T::_Mybase::element_type;

			BOOST_STATIC_ASSERT(boost::is_same<element_type,
				typename boost::pointee<T>::type>::value);
		};

		template<typename T>
		struct WeakType
		{
			//T必须是std::weak_ptr
			BOOST_STATIC_ASSERT(utp::isWeakPtr<T>::value);

			using element_type = typename T::_Mybase::element_type;

			BOOST_STATIC_ASSERT(boost::is_same<element_type,
				typename boost::pointee<T>::type>::value);
		};

		template<typename T>
		struct UniqueType
		{
			//T必须是std::unique_ptr
			BOOST_STATIC_ASSERT(utp::isUniquePtr<T>::value);

			using element_type = typename T::element_type;

			BOOST_STATIC_ASSERT(boost::is_same<element_type,
				typename boost::pointee<T>::type>::value);
		};

		template<typename T>
		struct SmartType
		{
			BOOST_STATIC_ASSERT(utp::isSharedPtr<T>::value || utp::isWeakPtr<T>::value || 
													utp::isUniquePtr<T>::value);

			using element_type = typename boost::conditional<utp::isUniquePtr<T>::value,
													typename T::element_type,
													typename T::_Mybase::element_type
													>::type;

			BOOST_STATIC_ASSERT(boost::is_same<element_type,
				typename boost::pointee<T>::type>::value);
		};

		/*!
		 * \brief
		 * 最佳的参数类型
		 * \class ParameterType
		 *
		 * \author Su Yang
		 *
		 * \date 2017/04/22
		 *
		 * \todo
		 */
		template<typename T>
		struct ParameterType
		{
			/*
			如果T是基本类型，左值引用，普通指针，成员变量指针，函数指针，成员函数指针，智能指针，那么返回T
			否则返回T const&
			*/

			using type = typename boost::conditional < boost::is_fundamental<T>::value ||
																				boost::is_lvalue_reference<T>::value ||
																				boost::is_pointer<T>::value ||
																				isSmartPtr<T>::value || 
																				boost::is_member_function_pointer<T>::value || 
																				boost::is_member_object_pointer<T>::value,
				T,
				typename boost::conditional<boost::is_const<T>::value,
														typename boost::add_lvalue_reference<T>::type,
														typename boost::add_lvalue_reference<typename boost::add_const<T>::type>::type
				>::type
			>::type;
		};
	}//namespace utp
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_METAFUNCTION_H_