/*!
 * \brief
 * 场景查询
 * \file USMSceneQuery.h
 *
 * \author Su Yang
 *
 * \date 2017/04/03
 */
#ifndef _USM_SCENEQUERY_H_
#define _USM_SCENEQUERY_H_

#ifndef _USM_CONFIG_H_
#include "USMConfig.h"
#endif

#ifdef USM_HIERARCHICAL_COMPILE

#ifndef _UIF_SCENEQUERY_H_
#include "UIFISceneQuery.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 场景查询
	 * \class SceneQuery
	 *
	 * \author Su Yang
	 *
	 * \date 2017/04/03
	 *
	 * \todo
	 */
	class UngExport SceneQuery : public SceneQueryBase
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		SceneQuery() = default;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param ISpatialManager* spatialManager
		*/
		SceneQuery(ISpatialManager* spatialManager);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~SceneQuery();

		/*!
		 * \remarks 设置某一个实例的掩码
		 * \return 
		 * \param uint32 mask
		*/
		virtual void setInstanceMask(uint32 mask) override;

		/*!
		 * \remarks 获取某一个实例的掩码
		 * \return 
		*/
		virtual uint32 getInstanceMask() override;

		/*!
		 * \remarks 设置某一类型的掩码
		 * \return 
		 * \param uint32 mask
		*/
		virtual void setTypeMask(uint32 mask) override;

		/*!
		 * \remarks 获取某一类型的掩码
		 * \return 
		*/
		virtual uint32 getTypeMask() override;

	protected:
		ISpatialManager* mSpatialManager;
		uint32 mInstanceMask;
		uint32 mTypeMask;
	};//end class SceneQuery

	//--------------------------------------------------------------

	/*!
	 * \brief
	 * 单一场景查询结果
	 * \class SingleSceneQueryResults
	 *
	 * \author Su Yang
	 *
	 * \date 2017/04/03
	 *
	 * \todo
	 */
	class SingleSceneQueryResults : public SceneQueryResultsBase
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		SingleSceneQueryResults();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~SingleSceneQueryResults() = default;

		/*!
		 * \remarks 获取单一场景查询结果对象
		 * \return 
		*/
		virtual WeakIObjectPtr getSingleObject() const override;

		/*!
		 * \remarks 获取射线的距离
		 * \return 
		*/
		virtual real_type getRayDistance() const override;

		/*!
		 * \remarks 清空查询结果
		 * \return 
		*/
		virtual void clearResults() override;

		/*!
		 * \remarks 重载<运算符
		 * \return 
		 * \param SingleSceneQueryResults const & other
		*/
		bool operator<(SingleSceneQueryResults const& other);

	//private:
		WeakIObjectPtr mObject;
		real_type mDistance;
	};

	/*!
	 * \brief
	 * 单一场景查询
	 * \class SingleSceneQuery
	 *
	 * \author Su Yang
	 *
	 * \date 2017/04/03
	 *
	 * \todo
	 */
	class UngExport SingleSceneQuery : public SceneQuery
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		SingleSceneQuery() = default;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param ISpatialManager* spatialManager
		*/
		SingleSceneQuery(ISpatialManager* spatialManager);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~SingleSceneQuery();

		/*!
		 * \remarks 执行查询
		 * \return 
		*/
		virtual ISceneQueryResults* execute() = 0;

		/*!
		 * \remarks 获取最近一次的查询结果
		 * \return 
		*/
		virtual ISceneQueryResults* getResults() override;

		/*!
		 * \remarks 清空查询结果
		 * \return 
		*/
		virtual void clearResults() override;

	protected:
		SingleSceneQueryResults* mResults;
	};//end class SingleSceneQuery

	//--------------------------------------------------------------

	/*!
	 * \brief
	 * 范围场景查询结果
	 * \class RangeSceneQueryResults
	 *
	 * \author Su Yang
	 *
	 * \date 2017/04/03
	 *
	 * \todo
	 */
	class RangeSceneQueryResults : public SceneQueryResultsBase
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		RangeSceneQueryResults() = default;

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~RangeSceneQueryResults() = default;

		/*!
		 * \remarks 获取范围查询结果对象
		 * \return 
		 * \param STL_LIST(WeakIObjectPtr)& objects
		*/
		virtual void getRangeObjects(STL_LIST(WeakIObjectPtr)& objects) const override;

		/*!
		 * \remarks 清空查询结果
		 * \return 
		*/
		virtual void clearResults() override;

		STL_LIST(WeakIObjectPtr) mObjects;
	};

	/*!
	 * \brief
	 * 范围场景查询
	 * \class RangeSceneQuery
	 *
	 * \author Su Yang
	 *
	 * \date 2017/04/03
	 *
	 * \todo
	 */
	class UngExport RangeSceneQuery : public SceneQuery
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		RangeSceneQuery() = default;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param ISpatialManager* spatialManager
		*/
		RangeSceneQuery(ISpatialManager* spatialManager);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~RangeSceneQuery();

		/*!
		 * \remarks 执行查询
		 * \return 
		*/
		virtual ISceneQueryResults* execute() = 0;

		/*!
		 * \remarks 获取最近一次的查询结果
		 * \return 
		*/
		virtual ISceneQueryResults* getResults() override;

		/*!
		 * \remarks 清空查询结果
		 * \return 
		*/
		virtual void clearResults() override;

	protected:
		RangeSceneQueryResults* mResults;
	};//end class RangeSceneQuery
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE

#endif//_USM_SCENEQUERY_H_