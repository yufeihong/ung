/*!
 * \brief
 * 对Bullet的Motion State的封装。用于获取对象的变换信息。
 * \file UPETransformationHook.h
 *
 * \author Su Yang
 *
 * \date 2016/12/03
 */
#ifndef _UPE_TRANSFORMATIONHOOK_H_
#define _UPE_TRANSFORMATIONHOOK_H_

#ifndef _UPE_CONFIG_H_
#include "UPEConfig.h"
#endif

#ifdef UPE_HIERARCHICAL_COMPILE

namespace ung
{
	/*!
	 * \brief
	 * 封装在bullet中，对象的变换信息。
	 * \class TransformationHook
	 *
	 * \author Su Yang
	 *
	 * \date 2016/12/03
	 *
	 * \todo
	 */
	class TransformationHook : public btDefaultMotionState
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		TransformationHook() = delete;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param const btTransform & transform
		*/
		UngExport TransformationHook(const btTransform &transform);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual UngExport ~TransformationHook();

		/*!
		 * \remarks 获取对象的transformation matrix，以便于渲染系统去使用。
		 * \return 
		 * \param real_type* transform
		*/
		UngExport void grabWorldTransform(real_type* transform);

		/*!
		 * \remarks 获取对象的transformation matrix，以便于渲染系统去使用。
		 * \return 
		 * \param Matrix4 & transform
		*/
		UngExport void grabWorldTransform(Matrix4& transform);
	};
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

#endif//_UPE_TRANSFORMATIONHOOK_H_