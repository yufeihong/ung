/*!
 * \brief
 * 场景查询接口。
 * \file UIFISceneQuery.h
 *
 * \author Su Yang
 *
 * \date 2017/04/03
 */
#ifndef _UIF_SCENEQUERY_H_
#define _UIF_SCENEQUERY_H_

#ifndef _UIF_CONFIG_H_
#include "UIFConfig.h"
#endif

#ifdef UIF_HIERARCHICAL_COMPILE

#ifndef _UIF_SCENEQUERY_H_
#include "UIFISceneQuery.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 场景查询结果接口
	 * \class ISceneQueryResults
	 *
	 * \author Su Yang
	 *
	 * \date 2017/04/16
	 *
	 * \todo
	 */
	class UngExport ISceneQueryResults : public MemoryPool<ISceneQueryResults>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		ISceneQueryResults() = default;

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~ISceneQueryResults() = default;

		/*!
		 * \remarks 获取单一查询的对象
		 * \return 
		*/
		virtual WeakIObjectPtr getSingleObject() const = 0;

		/*!
		 * \remarks 获取射线的距离
		 * \return 
		*/
		virtual real_type getRayDistance() const = 0;

		/*!
		 * \remarks 获取范围查询的对象
		 * \return 
		 * \param STL_LIST(WeakIObjectPtr)& objects
		*/
		virtual void getRangeObjects(STL_LIST(WeakIObjectPtr)& objects) const = 0;

		/*!
		 * \remarks 清空查询结果
		 * \return 
		*/
		virtual void clearResults() = 0;
	};

	/*!
	 * \brief
	 * 场景查询结果基类
	 * \class SceneQueryResultsBase
	 *
	 * \author Su Yang
	 *
	 * \date 2017/04/16
	 *
	 * \todo
	 */
	class UngExport SceneQueryResultsBase : public ISceneQueryResults
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		SceneQueryResultsBase() = default;

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~SceneQueryResultsBase() = default;

		/*!
		 * \remarks 获取单一查询的对象
		 * \return 
		*/
		virtual WeakIObjectPtr getSingleObject() const override
		{
			return WeakIObjectPtr{};
		}

		/*!
		 * \remarks 获取射线的距离
		 * \return 
		*/
		virtual real_type getRayDistance() const override
		{
			return real_type{};
		}

		/*!
		 * \remarks 获取范围查询的对象
		 * \return 
		 * \param STL_LIST(WeakIObjectPtr)& objects
		*/
		virtual void getRangeObjects(STL_LIST(WeakIObjectPtr)& objects) const override
		{
		}

		/*!
		 * \remarks 清空查询结果
		 * \return 
		*/
		virtual void clearResults() override
		{
		}
	};

	/*!
	 * \brief
	 * 场景查询接口
	 * \class ISceneQuery
	 *
	 * \author Su Yang
	 *
	 * \date 2017/04/03
	 *
	 * \todo
	 */
	class UngExport ISceneQuery : public MemoryPool<ISceneQuery>,public std::enable_shared_from_this<ISceneQuery>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		ISceneQuery() = default;

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~ISceneQuery() = default;

		/*!
		 * \remarks 设置某一个实例的掩码
		 * \return 
		 * \param uint32 mask
		*/
		virtual void setInstanceMask(uint32 mask) = 0;

		/*!
		 * \remarks 获取某一个实例的掩码
		 * \return 
		*/
		virtual uint32 getInstanceMask() = 0;

		/*!
		 * \remarks 设置某一类型的掩码
		 * \return 
		 * \param uint32 mask
		*/
		virtual void setTypeMask(uint32 mask) = 0;

		/*!
		 * \remarks 获取某一类型的掩码
		 * \return 
		*/
		virtual uint32 getTypeMask() = 0;

		/*!
		 * \remarks 执行查询
		 * \return 
		*/
		virtual ISceneQueryResults* execute() = 0;

		/*!
		 * \remarks 获取最近一次的查询结果
		 * \return 
		*/
		virtual ISceneQueryResults* getResults() = 0;

		/*!
		 * \remarks 清空查询结果
		 * \return 
		*/
		virtual void clearResults() = 0;
	};//end class ISceneQuery

	//-----------------------------------------------------

	/*!
	 * \brief
	 * 场景查询基类
	 * \class SceneQueryBase
	 *
	 * \author Su Yang
	 *
	 * \date 2017/04/03
	 *
	 * \todo
	 */
	class UngExport SceneQueryBase : public ISceneQuery
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		SceneQueryBase() = default;

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~SceneQueryBase() = default;

		/*!
		 * \remarks 设置某一个实例的掩码
		 * \return 
		 * \param uint32 mask
		*/
		virtual void setInstanceMask(uint32 mask) override
		{
		}

		/*!
		 * \remarks 获取某一个实例的掩码
		 * \return 
		*/
		virtual uint32 getInstanceMask() override
		{
			return uint32();
		}

		/*!
		 * \remarks 设置某一类型的掩码
		 * \return 
		 * \param uint32 mask
		*/
		virtual void setTypeMask(uint32 mask) override
		{
		}

		/*!
		 * \remarks 获取某一类型的掩码
		 * \return 
		*/
		virtual uint32 getTypeMask() override
		{
			return uint32();
		}

		/*!
		 * \remarks 执行查询
		 * \return 
		*/
		virtual ISceneQueryResults* execute() = 0;

		/*!
		 * \remarks 获取最近一次的查询结果
		 * \return 
		*/
		virtual ISceneQueryResults* getResults() override
		{
			return nullptr;
		}

		/*!
		 * \remarks 清空查询结果
		 * \return 
		*/
		virtual void clearResults() override
		{
		}
	};
}//namespace ung

#endif//UIF_HIERARCHICAL_COMPILE

#endif//_UIF_SCENEQUERY_H_