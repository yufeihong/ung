/*!
 * \brief
 * Single linked list，单向链表
 * \file UTLSingleList.h
 *
 * \author Su Yang
 *
 * \date 2016/02/26
 */
#ifndef _UNG_UTL_SINGLELIST_H_
#define _UNG_UTL_SINGLELIST_H_

#ifndef _UNG_UTL_CONFIG_H_
#include "UTLConfig.h"
#endif

#ifndef _UNG_UTL_ALLOCATOR_H_
#include "UTLAllocator.h"
#endif

#ifndef _UNG_UTL_CONSTRUCT_H_
#include "UTLConstruct.h"
#endif

#ifndef _UNG_UTL_SINGLELIST_ITERATOR_H_
#include "UTLSingleListIterator.h"
#endif

#ifndef _UNG_UTL_TYPETRAITS_H_
#include "UTLTypeTraits.h"
#endif

namespace ung
{
	namespace utl
	{
		/*!
		 * \brief
		 * 单向链表的节点基本结构
		 * \class slist_node_base_
		 *
		 * \author Su Yang
		 *
		 * \date 2016/02/26
		 *
		 * \todo
		 */
		struct slist_node_base_ 
		{
			slist_node_base_* next;
		};

		/*!
		 * \brief
		 * 单向链表的节点结构
		 * \class slist_node_
		 *
		 * \author Su Yang
		 *
		 * \date 2016/02/26
		 *
		 * \todo
		 */
		template<typename T>
		struct slist_node_ : public slist_node_base_
		{
			T data;
		};

		/*!
		 * \remarks 已知某一节点，插入新节点于其后
		 * \return 返回指向新节点的指针
		 * \param slist_node_base_ * prev_node
		 * \param slist_node_base_ * new_node
		*/
		inline slist_node_base_* slist_make_link_(slist_node_base_* prev_node, slist_node_base_* new_node)
		{
			//令new节点的下一节点为prev节点的下一节点
			new_node->next = prev_node->next;
			//令prev节点的下一节点指向new节点
			prev_node->next = new_node;

			return new_node;
		}

		/*!
		 * \remarks 找到node的前面一个节点
		 * \return 
		 * \param slist_node_base_ * head
		 * \param const slist_node_base_ * node
		*/
		inline slist_node_base_* slist_previous_(slist_node_base_* head,const slist_node_base_* node)
		{
			while (head && head->next != node)
			{
				head = head->next;
			}

			return head;
		}

		/*!
		 * \remarks 找到node的前面一个节点
		 * \return 
		 * \param const slist_node_base_ * head 底层const
		 * \param const slist_node_base_ * node
		*/
		inline const slist_node_base_* slist_previous_(const slist_node_base_* head,const slist_node_base_* node)
		{
			while (head && head->next != node)
			{
				head = head->next;
			}

			return head;
		}

		/*!
		 * \remarks 把区间(before_first,before_last)所表示的元素插入到pos后面
		 * \return 
		 * \param slist_node_base_ * pos
		 * \param slist_node_base_ * before_first 如果是整个单向链表的话，before_first代表head
		 * \param slist_node_base_ * before_last 如果是整个单向链表的话，before_last代表0前面的那个元素
		*/
		inline void slist_splice_after_(slist_node_base_* pos,slist_node_base_* before_first,slist_node_base_* before_last)
		{
			if (pos != before_first && pos != before_last)
			{
				slist_node_base_* first = before_first->next;
				slist_node_base_* after = pos->next;
				before_first->next = before_last->next;
				pos->next = first;
				before_last->next = after;
			}
		}

		/*!
		 * \remarks 把h后面的所有元素插入到pos之后
		 * \return 
		 * \param slist_node_base_ * pos
		 * \param slist_node_base_ * h
		*/
		inline void slist_splice_after_(slist_node_base_* pos, slist_node_base_* h)
		{
			slist_node_base_* before_last = slist_previous_(h, 0);	//找到h所代表的单向链表的最后一个元素(0之前的那个元素)
			if (before_last != h)
			{
				slist_node_base_* after = pos->next;
				pos->next = h->next;
				h->next = 0;
				before_last->next = after;
			}
		}

		/*!
		 * \remarks 获得逆序
		 * \return 
		 * \param slist_node_base_ * node_
		*/
		inline slist_node_base_* slist_reverse_(slist_node_base_* node_)
		{
			slist_node_base_* result = node_;
			node_ = node_->next;
			result->next = 0;
			while (node_)
			{
				slist_node_base_* next_ = node_->next;
				node_->next = result;
				result = node_;
				node_ = next_;
			}

			return result;
		}

		/*!
		 * \brief
		 * 单向链表的基类。
		 * \class slist_base_
		 *
		 * \author Su Yang
		 *
		 * \date 2016/02/27
		 *
		 * \todo
		 */
		template <typename T, typename Alloc>
		struct slist_base_
		{
			/*!
			 * \remarks 默认构造函数
			 * \return 
			*/
			slist_base_()
			{
				head.next = 0;
			}

			/*!
			 * \remarks 析构函数
			 * \return 
			*/
			~slist_base_()
			{
				erase_after_(&head, 0);
			}

		protected:
			using slist_node_Allocator = Allocator<slist_node_<T>>;
			slist_node_Allocator nodeAllocator;

			/*!
			 * \remarks 分配一个节点
			 * \return 
			*/
			slist_node_<T>* get_node()
			{
				return nodeAllocator.allocate(1);
			}

			/*!
			 * \remarks 释放一个节点
			 * \return 
			 * \param slist_node_<T> * p
			*/
			void put_node(slist_node_<T>* p)
			{
				nodeAllocator.deallocate(p, 1);
			}

			/*!
			 * \remarks 删除pos后面的元素
			 * \return 
			 * \param slist_node_base_ * pos
			*/
			slist_node_base_* erase_after_(slist_node_base_* pos)
			{
				slist_node_<T>* n = (slist_node_<T>*) (pos->next);
				slist_node_base_* nn = n->next;
				pos->next = nn;
				destroy(&n->data);
				put_node(n);

				return nn;
			}
			/*!
			 * \remarks 删除区间所表示范围中元素
			 * \return 
			 * \param slist_node_base_ * before_first 如果是整个单向链表的话，那么before_first表示head
			 * \param slist_node_base_ * last 如果是整个单向链表的话，那么last表示0
			*/
			slist_node_base_* erase_after_(slist_node_base_* before_first, slist_node_base_* last)
			{
				slist_node_<T>* cur = (slist_node_<T>*) (before_first->next);
				while (cur != last)
				{
					slist_node_<T>* tmp = cur;
					cur = (slist_node_<T>*) cur->next;
					destroy(&tmp->data);
					put_node(tmp);
				}
				before_first->next = last;

				return last;
			}

		protected:
			slist_node_base_ head;				//头部，注意：不是指针
		};

		/*!
		 * \brief
		 * 单向链表
		 * \class SList
		 *
		 * \author Su Yang
		 *
		 * \date 2016/02/27
		 *
		 * \todo 整理接口
		 */
		template<typename T,typename Alloc = Allocator<T>>
		class SList : private slist_base_<T, Alloc>
		{
		private:
			using base_type = slist_base_<T, Alloc>;

		public:
			using value_type = T;
			using pointer = value_type*;
			using const_pointer = value_type const*;
			using reference = value_type&;
			using const_reference = value_type const&;
			using difference_type = std::ptrdiff_t;

			using iterator = SListIterator<T, T&, T*>;
			using const_iterator = SListIterator<T, T const&, T const*>;
			using reverse_iterator = std::reverse_iterator<iterator>;
			using const_reverse_iterator = std::reverse_iterator<const_iterator>;

			/*!
			 * \remarks 默认构造函数
			 * \return 
			*/
			SList() :
				base_type()
			{
			}

			/*!
			 * \remarks 构造函数
			 * \return 
			 * \param usize n 数量
			 * \param const_reference x 值
			*/
			SList(usize n, const_reference x) :
				base_type()
			{
				construct_n_(n, x);
			}

			/*!
			 * \remarks 构造函数
			 * \return 
			 * \param usize n 数量
			*/
			explicit SList(usize n) :
				SList(n, value_type())
			{
			}

			///*!
			//* \remarks 构造函数
			//* \return 
			//* \param const_iterator first
			//* \param const_iterator last
			//*/
			//SList(const_iterator first, const_iterator last) :
			//	base_type()
			//{
			//	insert_range_after_(&this->head, first, last);
			//}

			/*!
			 * \remarks 构造函数，from[first,last)
			 * \return 
			 * \param Iter first
			 * \param Iter last
			*/
			template<typename Iter,class = typename std::enable_if<std::_Is_iterator<Iter>::value,void>::type>
			SList(Iter first, Iter last) :
				base_type()
			{
				construct_range_(first, last);
			}

			/*!
			 * \remarks 构造函数，from initializer_list
			 * \return 
			 * \param std::initializer_list<value_type> il
			*/
			SList(std::initializer_list<value_type> il) :
				base_type()
			{
				insert_after(before_begin(), il.begin(), il.end());
			}

			/*!
			 * \remarks 赋值运算符，from initializer_list
			 * \return 
			 * \param std::initializer_list<value_type> il
			*/
			SList& operator=(std::initializer_list<value_type> il)
			{
				assign(il.begin(), il.end());

				return *this;
			}

			///*!
			//* \remarks 构造函数
			//* \return 
			//* \param const_pointer first
			//* \param const_pointer last
			//*/
			//SList(const_pointer first, const_pointer last) :
			//	base_type()
			//{
			//	insert_range_after_(&this->head, first, last);
			//}

			/*!
			 * \remarks 拷贝构造函数
			 * \return 
			 * \param SList const & x
			*/
			SList(SList const& x) :
				base_type()
			{
				construct_range_(x.begin(), x.end());
			}

			/*!
			 * \remarks 移动构造函数
			 * \return 
			 * \param SList && right
			*/
			SList(SList&& right) noexcept :
				base_type()
			{
				swap(right);
			}

			/*!
			 * \remarks 拷贝，移动赋值运算符
			 * \return 
			 * \param SList right
			*/
			SList& operator=(SList right) noexcept
			{
				swap(right);

				return *this;
			}

			/*!
			 * \remarks 析构函数
			 * \return 
			*/
			virtual ~SList()
			{
			}

			/*!
			 * \remarks 赋值
			 * \return 
			 * \param usize n 数量
			 * \param const_reference val 值
			*/
			void assign(usize n, const_reference val)
			{
				assign_(n, val);
			}

			/*!
			 * \remarks 赋值
			 * \return 
			 * \param std::initializer_list<value_type> il 初始化列表
			*/
			void assign(std::initializer_list<value_type> il)
			{
				assign(il.begin(), il.end());
			}

			/*!
			 * \remarks 赋值
			 * \return 
			 * \param Iter first
			 * \param Iter last
			*/
			template<class Iter>
			typename std::enable_if<std::_Is_iterator<Iter>::value,void>::type assign(Iter first, Iter last)
			{
				clear();
				insert_after(before_begin(), first, last);
			}

			/*!
			 * \remarks 在第一个元素之前插入一个由args创建的对象
			 * \return 
			 * \param Arguments && ... args
			*/
			template<typename... Arguments>
			void emplace_front(Arguments&&... args)
			{
				insert_after(before_begin(), std::forward<Arguments>(args)...);
			}

			/*!
			 * \remarks 在where插入一个由args创建的对象
			 * \return 返回指向新元素的迭代器
			 * \param const_iterator where
			 * \param Arguments && ... args
			*/
			template<class... Arguments>
			iterator emplace_after(const_iterator where, Arguments&&... args)
			{
				return iterator(insert_after_(where, std::forward<Arguments>(args)...));
			}

			/*!
			 * \remarks 获取指向head的迭代器
			 * \return 
			*/
			iterator before_begin()
			{
				return iterator((list_node*)&this->head);
			}

			/*!
			 * \remarks 获取指向head的常量迭代器，常量对象调用
			 * \return 
			*/
			const_iterator before_begin() const
			{
				return const_iterator((list_node*)&this->head);
			}

			/*!
			 * \remarks 获取指向head的常量迭代器
			 * \return 
			*/
			const_iterator cbefore_begin() const
			{
				return const_iterator((list_node*)&this->head);
			}

			/*!
			 * \remarks 获取指向第一个元素的迭代器
			 * \return 
			*/
			iterator begin()
			{
				return iterator((list_node*)head.next);
			}

			/*!
			 * \remarks 获取指向第一个元素的常量迭代器，常量对象调用
			 * \return 
			*/
			const_iterator begin() const
			{
				return const_iterator((list_node*)this->head.next);
			}

			/*!
			 * \remarks 获取指向第一个元素的常量迭代器
			 * \return 
			*/
			const_iterator cbegin() const
			{
				return const_iterator((list_node*)this->head.next);
			}

			/*!
			 * \remarks 获取指向最后一个元素下一位置的迭代器
			 * \return 
			*/
			iterator end()
			{
				/*
				因为源代码：using iterator = SListIterator<T,T&,T*>
				所以有：SListIterator<T,T&,T*>(0)，产生一个临时对象，引发ctor
				因为：SListIterator(list_node* x) : slist_iterator_base_(x) {}
				而导致基础类的构造：slist_iterator_base_(0)
				因为：
				struct slist_iterator_base_
				{
					slist_iterator_base_(slist_node_base_* x) :
						node(x)
					{
					}

					//指向节点基本结构
					slist_node_base_* node;
				};
				而导致：node(0)
				*/
				return iterator(0);
			}

			/*!
			 * \remarks 获取指向最后一个元素下一位置的常量迭代器，常量对象调用
			 * \return 
			*/
			const_iterator end() const
			{
				return const_iterator(0);
			}

			/*!
			* \remarks 获取指向最后一个元素下一位置的常量迭代器
			* \return
			*/
			const_iterator cend() const
			{
				return const_iterator(0);
			}

			/*!
			 * \remarks 获取元素数量
			 * \return 
			*/
			usize size() const
			{
				slist_node_base_* node = head.next;
				usize result = 0;
				for (; node != 0; node = node->next)
				{
					++result;
				}

				return result;
			}

			/*!
			 * \remarks 判断是否为空
			 * \return 
			*/
			bool empty() const
			{
				return head.next == 0;
			}

			/*!
			 * \remarks 交换
			 * \return 
			 * \param SList & x
			*/
			void swap(SList& x) noexcept
			{
				/*
				或者：std::swap(this->head.next, x.head.next);
				*/
				list_node_base* tmp = head.next;
				head.next = x.head.next;
				x.head.next = tmp;
			}

			/*!
			 * \remarks 获取第一个元素的引用
			 * \return 
			*/
			reference front()
			{
				return ((list_node*)head.next)->data;
			}

			/*!
			 * \remarks 获取第一个元素的常量引用，常量对象调用
			 * \return 
			*/
			const_reference front() const
			{
				return ((list_node*) this->head.next)->data;
			}

			/*!
			 * \remarks 在第一个元素之前压入
			 * \return 
			 * \param const_reference x
			*/
			void push_front(const_reference x)
			{
				insert_after_(before_begin(),x);
			}

			/*!
			 * \remarks 在第一个元素之前压入
			 * \return 
			 * \param value_type && val 右值引用
			*/
			void push_front(value_type&& val)
			{
				insert_after_(before_begin(), std::forward<value_type>(val));
			}

			/*!
			 * \remarks 删除第一个元素
			 * \return 
			*/
			void pop_front()
			{
				list_node* node = (list_node*)head.next;
				head.next = node->next;
				destroy_node(node);
			}

			/*!
			 * \remarks 在pos后面插入
			 * \return 
			 * \param iterator pos
			 * \param const_reference x
			*/
			iterator insert_after(iterator pos, const_reference x)
			{
				return iterator(insert_after_(pos, x));
			}

			/*!
			 * \remarks 在pos后面插入
			 * \return 
			 * \param iterator pos
			 * \param value_type && x 右值引用
			*/
			iterator insert_after(iterator pos, value_type&& x)
			{
				return iterator(insert_after_(pos, std::forward<value_type>(x)));
			}

			/*!
			 * \remarks 在pos后面插入
			 * \return 
			 * \param iterator pos
			*/
			iterator insert_after(iterator pos)
			{
				return insert_after(pos, value_type());
			}

			/*!
			 * \remarks 在pos后面插入
			 * \return 
			 * \param iterator pos
			 * \param usize n
			 * \param const value_type & x
			*/
			void insert_after(iterator pos, usize n, const value_type& x)
			{
				insert_n_after_(pos.node, n, x);
			}

			///*!
			//* \remarks 在pos后面插入
			//* \return 
			//* \param iterator pos
			//* \param const_iterator first
			//* \param const_iterator last
			//*/
			//void insert_after(iterator pos,const_iterator first, const_iterator last)
			//{
			//	insert_range_after_(pos.node, first, last);
			//}

			/*!
			* \remarks insert [first, last) at where
			* \return
			* \param const_iterator where
			* \param Iter first
			* \param Iter last
			*/
			template<class Iter>
			typename std::enable_if<std::_Is_iterator<Iter>::value, iterator>::type insert_after(const_iterator where, Iter first, Iter last)
			{
				return insert_range_after_(where, first, last, Iter_cat(first));
			}

			///*!
			//* \remarks 在pos后面插入
			//* \return 
			//* \param iterator pos
			//* \param const value_type * first
			//* \param const value_type * last
			//*/
			//void insert_after(iterator pos,const value_type* first, const value_type* last)
			//{
			//	insert_range_after_(pos.node, first, last);
			//}

			/*!
			 * \remarks 在pos后面插入
			 * \return 
			 * \param const_iterator where
			 * \param std::initializer_list<value_type> il
			*/
			iterator insert_after(const_iterator pos,std::initializer_list<value_type> il)
			{
				return insert_after(pos, il.begin(), il.end());
			}

			/*!
			 * \remarks 在pos前面插入
			 * \return 
			 * \param iterator pos
			 * \param const_reference x
			*/
			iterator insert(iterator pos, const_reference x)
			{
				return iterator(insert_after_(slist_previous_(&this->head, pos.node), x));
			}

			/*!
			 * \remarks 在pos前面插入
			 * \return 
			 * \param iterator pos
			*/
			iterator insert(iterator pos)
			{
				return iterator(insert_after_(slist_previous_(&this->head,pos.node),value_type()));
			}

			/*!
			 * \remarks 在pos前面插入
			 * \return 
			 * \param iterator pos
			 * \param usize n 数量
			 * \param const value_type & x 值
			*/
			void insert(iterator pos, usize n, const value_type& x)
			{
				insert_n_after_(slist_previous_(&this->head, pos.node),n, x);
			}

			/*!
			 * \remarks 在pos前面插入
			 * \return 
			 * \param iterator pos
			 * \param const_iterator first
			 * \param const_iterator last
			*/
			void insert(iterator pos, const_iterator first, const_iterator last)
			{
				insert_range_after_(slist_previous_(&this->head, pos.node),first, last);
			}

			/*!
			 * \remarks 在pos前面插入
			 * \return 
			 * \param iterator pos
			 * \param const value_type * first
			 * \param const value_type * last
			*/
			void insert(iterator pos, const value_type* first,const value_type* last)
			{
				insert_range_after_(slist_previous_(&this->head, pos.node),first, last);
			}

			/*!
			 * \remarks 删除pos后面的元素
			 * \return 
			 * \param iterator pos
			*/
			iterator erase_after(iterator pos)
			{
				return iterator((list_node*) this->erase_after_(pos.node));
			}

			/*!
			 * \remarks 删除区间(before_first,last)范围中的元素
			 * \return 
			 * \param iterator before_first
			 * \param iterator last
			*/
			iterator erase_after(iterator before_first, iterator last)
			{
				return iterator((list_node*) this->erase_after_(before_first.node, last.node));
			}

			/*!
			 * \remarks 删除pos指向的元素
			 * \return 
			 * \param iterator pos
			*/
			iterator erase(iterator pos)
			{
				return (list_node*) this->erase_after_(slist_previous_(&this->head, pos.node));
			}

			/*!
			 * \remarks 删除区间[first,last)范围中的元素
			 * \return 
			 * \param iterator first
			 * \param iterator last
			*/
			iterator erase(iterator first, iterator last)
			{
				return (list_node*) this->erase_after_(slist_previous_(&this->head, first.node), last.node);
			}

			/*!
			 * \remarks 清空单向链表
			 * \return 
			*/
			void clear()
			{
				this->erase_after_(&this->head, 0);
			}

			/*!
			 * \remarks 调整单向链表的大小
			 * \return 
			 * \param usize len 新大小
			 * \param const_reference x 填充值
			*/
			void resize(usize len, const_reference x)
			{
				list_node_base* cur = &this->head;
				while (cur->next != 0 && len > 0)
				{
					--len;
					cur = cur->next;
				}

				if (cur->next)
				{
					this->erase_after_(cur, 0);
				}
				else
				{
					insert_n_after_(cur, len, x);
				}
			}

			/*!
			 * \remarks 调整单向链表的大小
			 * \return 
			 * \param usize new_size 新大小
			*/
			void resize(usize new_size)
			{
				resize(new_size, value_type());
			}

			/*!
			* \remarks 移动x对象中的所有元素至*this,插入到pos后面。Complexity:linear in x.size()
			* \return
			* \param const_iterator pos The position in the destination forward_list after which to insert.
			* \param slist & x
			*/
			void splice_after(const_iterator pos, SList& x)
			{
				if (this == &x || x.empty())
				{
					return;
				}

				slist_splice_after_(pos.node, &x.head);
			}

			/*!
			* \remarks 移动x对象中的所有元素至*this,插入到pos后面。Complexity:linear in x.size()
			* \return
			* \param const_iterator pos The position in the destination forward_list after which to insert.
			* \param SList && x
			*/
			void splice_after(const_iterator pos, SList&& x)
			{
				splice_after(pos, (SList&)x);
			}

			/*!
			 * \remarks splice x(first, first + 2)至pos后面
			 * \return 
			 * \param const_iterator pos
			 * \param SList & x
			 * \param const_iterator first
			*/
			void splice_after(const_iterator pos, SList& x, const_iterator first)
			{
				const_iterator after = first;
				if (first == x.end() || ++after == x.end())
				{
					return;
				}
				else
				{
					if (this != &x || (pos != first && pos != after))
					{
						slist_splice_after_(pos.node, first, first.node->next->next);
					}
				}
			}

			/*!
			 * \remarks splice x[first, first + 1)至pos后面
			 * \return 
			 * \param const_iterator pos
			 * \param SList && x
			 * \param const_iterator first
			*/
			void splice_after(const_iterator pos, SList&& x,const_iterator first)
			{
				splice_after(pos, (SList&)x, first,slist_previous_(&x.head,first));
			}

			/*!
			 * \remarks splice x[first, last)至pos后面
			 * \return 
			 * \param const_iterator pos
			 * \param SList & x
			 * \param const_iterator first
			 * \param const_iterator last
			*/
			void splice_after(const_iterator pos, SList& x, const_iterator first, const_iterator last)
			{
				const_iterator after = first;
				if (first == x.end())
				{
					return;
				}
				else if (this != &x || pos != first)
				{
					slist_splice_after_(pos.node, slist_previous_(&x.head, first),last);
				}
			}

			/*!
			 * \remarks splice x[first, last)至pos后面
			 * \return 
			 * \param const_iterator pos
			 * \param SList & & x
			 * \param const_iterator first
			 * \param const_iterator last
			*/
			void splice_after(const_iterator pos, SList&& x, const_iterator first, const_iterator last)
			{
				splice_after(pos, (SList&)x, first, last);
			}

			/*!
			 * \remarks 获得逆序
			 * \return 
			*/
			void reverse()
			{
				if (this->head.next)
				{
					this->head.next = slist_reverse_(this->head.next);
				}
			}

			/*!
			 * \remarks 调用erase删除掉与给定值相等的每个元素
			 * \return 
			 * \param const_reference val
			*/
			void remove(const_reference val)
			{
				list_node_base* cur = &this->head;
				while (cur && cur->next)
				{
					if (((list_node*)cur->next)->data == val)
					{
						this->erase_after_(cur);
					}
					else
					{
						cur = cur->next;
					}
				}
			}

			/*!
			 * \remarks 删除满足谓词的每个元素
			 * \return 
			 * \param Pred pd
			*/
			template<typename Pred>
			void remove_if(Pred pd)
			{
				iterator it = before_begin();

				for (iterator first = begin(); first != end(); )
				{
					if (pd(*first))
					{
						first = erase_after_(it);
					}
					else
					{
						++it;
						++first;
					}
				}
			}

			/*!
			 * \remarks 调用erase删除同一个值得连续拷贝
			 * \return 
			*/
			void unique()
			{
				/*
				list_node_base* cur = this->head.next;
				if (cur)
				{
					while (cur->next)
					{
						if (((list_node*)cur)->data == ((list_node*)(cur->next))->data)
						{
							this->erase_after_(cur);
						}
						else
						{
							cur = cur->next;
						}
					}
				}
				*/
				unique(std::equal_to<>());
			}

			/*!
			 * \remarks 删除满足为此的每一个previous元素
			 * \return 
			 * \param Pred pd
			*/
			template<typename Pred>
			void unique(Pred pd)
			{
				iterator first = begin();
				if (first != end())
				{	//worth doing
					iterator after = first;
					for (++after; after != end();)
					{
						if (pd(*first, *after))
						{
							after = erase_after(first);
						}
						else
						{
							first = after++;
						}
					}
				}
			}

			/*!
			 * \remarks 将来自x的元素合并入*this。元素将从x中删除。在合并之后x变为空。
			 * \return 
			 * \param SList & x
			*/
			void merge(SList& x)
			{
				list_node_base* n1 = &this->head;
				while (n1->next && x.head.next)
				{
					if (((list_node*)x.head.next)->data < ((list_node*)n1->next)->data)
					{
						slist_splice_after_(n1, &x.head, x.head.next);
					}
					n1 = n1->next;
				}

				if (x.head.next)
				{
					n1->next = x.head.next;
					x.head.next = 0;
				}
			}

			/*!
			 * \remarks merge in elements from right, both ordered by operator<
			 * \return 
			 * \param SList && x
			*/
			void merge(SList&& x)
			{
				merge((SList&)x);
			}

			/*!
			 * \remarks 使用<排序元素
			 * \return 
			*/
			void sort()
			{
				if (this->head.next && this->head.next->next)
				{
					SList carry;
					SList counter[64];
					int fill = 0;
					while (!empty())
					{
						slist_splice_after_(&carry.head,&this->head, this->head.next);
						int i = 0;
						while (i < fill && !counter[i].empty())
						{
							counter[i].merge(carry);
							carry.swap(counter[i]);
							++i;
						}
						carry.swap(counter[i]);
						if (i == fill)
						{
							++fill;
						}
					}

					for (int i = 1; i < fill; ++i)
					{
						counter[i].merge(counter[i - 1]);
					}
					this->swap(counter[fill - 1]);
				}
			}

		private:
			using list_node = slist_node_<T>;
			using list_node_base = slist_node_base_;
			using iterator_base = slist_iterator_base_;

			/*!
			 * \remarks 分配并构造一个节点
			 * \return 
			*/
			list_node* create_node()
			{
				list_node* node = get_node();
				try
				{
					construct(&node->data);
					node->next = 0;
				}
				catch (...)
				{
					put_node(node);
					throw;
				}
				
				return node;
			}

			/*!
			 * \remarks 分配并构造一个节点
			 * \return 
			 * \param const_reference x
			*/
			list_node* create_node(const_reference x)
			{
				list_node* node = get_node();
				try
				{
					construct(&node->data, x);
					node->next = 0;
				}
				catch (...)
				{
					put_node(node);
					throw;
				}

				return node;
			}

			/*!
			 * \remarks 析构并释放一个节点
			 * \return 
			 * \param list_node * node
			*/
			void destroy_node(list_node* node)
			{
				//将元素析构
				destroy(&node->data);
				//释放空间
				put_node(node);
			}

			/*!
			 * \remarks 构造，from n * val
			 * \return 
			 * \param usize n 数量
			 * \param const_reference val 值
			*/
			void construct_n_(usize n,const_reference val)
			{
				try
				{
					insert_n_after_(before_begin().node, n, val);
				}
				catch (...)
				{
					clear();
					throw;
				}
			}

			/*!
			* \remarks 构造，from[first,last)
			* \return
			* \param Iter first
			* \param Iter last
			*/
			template<class Iter>
			void construct_range_(Iter first, Iter last)
			{
				try
				{
					insert_range_after_(before_begin(), first, last, Iter_cat(first));
				}
				catch (...)
				{
					clear();
					throw;
				}
			}

			/*!
			 * \remarks 在pos后面插入
			 * \return 指向新元素的指针
			 * \param const_iterator where
			 * \param Arguments && ... args
			*/
			template<typename... Arguments>
			list_node* insert_after_(const_iterator where, Arguments&&... args)
			{
				return (list_node*)(slist_make_link_(where.node, create_node(std::forward<Arguments>(args)...)));
			}

			/*!
			 * \remarks 在pos后面插入n个值为x的元素
			 * \return 
			 * \param list_node_base * pos
			 * \param usize n
			 * \param const_reference x
			*/
			void insert_n_after_(list_node_base* pos,usize n, const_reference x)
			{
				for (usize i = 0; i < n; ++i)
				{
					pos = slist_make_link_(pos, create_node(x));
				}
			}

			/*!
			* \remarks insert [first, last) after where, input iterators
			* \return
			* \param const_iterator where
			* \param Iter first
			* \param Iter last
			* \param std::input_iterator_tag
			*/
			template<class Iter>
			iterator insert_range_after_(const_iterator where, Iter first, Iter last, std::input_iterator_tag)
			{
				usize num = 0;
				const_iterator after = where;

				try
				{
					for (; first != last; ++after, ++first, ++num)
					{
						insert_after_(iterator((list_node*)after.node), *first);
					}
				}
				catch (...)
				{
					for (; 0 < num; --num)
					{
						erase_after_(where.node);
					}

					throw;
				}

				return iterator((list_node*)after.node);
			}

			/*!
			 * \remarks insert [first, last) after where, forward iterators
			 * \return 
			 * \param const_iterator where
			 * \param Iter first
			 * \param Iter last
			 * \param forward_iterator_tag
			*/
			template<class Iter>
			iterator insert_range_after_(const_iterator where,Iter first, Iter last, std::forward_iterator_tag)
			{
				Iter next = first;
				const_iterator after = where;

				try
				{
					for (; first != last; ++after, ++first)
					{
						insert_after_(after, *first);
					}
				}
				catch (...)
				{
					for (; next != first; ++next)
					{
						erase_after_(where.node);
					}

					throw;
				}

				return iterator((list_node*)after.node);
			}

			/*!
			 * \remarks 用n个val来替换*this中的元素值
			 * \return 
			 * \param usize n
			 * \param const_reference val
			*/
			void assign_(usize n, const_reference val)
			{
				list_node_base* prev = &head;
				list_node* node = (list_node*) this->head.next;
				for (; node != 0 && n > 0; --n)
				{
					node->data = val;
					prev = node;
					node = (list_node*)node->next;
				}
				if (n > 0)
				{
					insert_n_after_(prev, n, val);
				}
				else
				{
					this->erase_after_(prev, 0);
				}
			}
		};

		/*!
		 * \remarks 交换
		 * \return 
		 * \param SList<T，Alloc> & left
		 * \param SList<T，Alloc> & right
		*/
		template<typename T, typename Alloc>
		inline void swap(SList<T, Alloc>& left, SList<T, Alloc>& right) noexcept
		{
			left.swap(right);
		}

		/*!
		 * \remarks 判断两个单向链表是否相等
		 * \return 
		 * \param const SList<T,Alloc> & left
		 * \param const SList<T,Alloc> & right
		*/
		template <typename T, typename Alloc>
		inline bool operator==(SList<T, Alloc> const& left, SList<T, Alloc> const& right)
		{
			return (std::equal(left.begin(), left.end(), right.begin(), right.end()));
		}

		/*!
		 * \remarks 不等
		 * \return 
		 * \param const SList<T,Alloc> & left
		 * \param const SList<T,Alloc> & right
		*/
		template <typename T, typename Alloc>
		inline bool operator!=(SList<T, Alloc> const& left, SList<T, Alloc> const& right)
		{
			return !(left == right);
		}

		/*!
		 * \remarks 判断前一个单向链表是否小于后一个
		 * \return 
		 * \param const SList<T,Alloc> & left
		 * \param const SList<T,Alloc> & right
		*/
		template <typename T, typename Alloc>
		inline bool operator<(SList<T, Alloc> const& left, SList<T, Alloc> const& right)
		{
			return std::lexicographical_compare(left.begin(), left.end(), right.begin(), right.end());
		}

		/*!
		 * \remarks 大于
		 * \return 
		 * \param const SList<T,Alloc> & left
		 * \param const SList<T,Alloc> & right
		*/
		template <typename T, typename Alloc>
		inline bool operator>(SList<T, Alloc> const& left, SList<T, Alloc> const& right)
		{
			return right < left;
		}

		/*!
		 * \remarks 小于等于
		 * \return 
		 * \param const SList<T,Alloc> & left
		 * \param const SList<T,Alloc> & right
		*/
		template <typename T, typename Alloc>
		inline bool operator<=(SList<T, Alloc> const& left, SList<T, Alloc> const& right)
		{
			return !(right < left);
		}

		/*!
		 * \remarks 大于等于
		 * \return 
		 * \param const SList<T,Alloc> & left
		 * \param const SList<T,Alloc> & right
		*/
		template <typename T, typename Alloc>
		inline bool operator>=(SList<T, Alloc> const& left, SList<T, Alloc> const& right)
		{
			return !(left < right);
		}
	}//namespace utl
}//namespace ung

#endif//_UNG_UTL_SINGLELIST_H_