#ifndef _UTM_TASK_H_
#define _UTM_TASK_H_

#ifndef _UTM_CONFIG_H_
#include "UTMConfig.h"
#endif

#ifdef UTM_HIERARCHICAL_COMPILE

namespace ung
{
	class Task : boost::noncopyable
	{
	public:
		Task() = default;

		template<typename F>
		Task(F&& f) :
			//mImpl(std::make_unique<Impl<F>>(std::move(f)))
			mImpl(new Impl<F>(std::move(f)))
		{
			BOOST_ASSERT(mImpl);
		}

		/*!
		 * \remarks 移动构造函数
		 * \return 
		 * \param Task && other
		*/
		Task(Task&& other) noexcept :
			mImpl(std::move(other.mImpl))
		{
			//other.mImpl.reset(nullptr);

			BOOST_ASSERT(mImpl);
		}

		/*!
		 * \remarks 移动赋值运算符
		 * \return 
		 * \param Task && other
		*/
		Task& operator=(Task&& other) noexcept
		{
			if (this != &other)
			{
				mImpl = std::move(other.mImpl);

				//other.mImpl.reset(nullptr);

				BOOST_ASSERT(mImpl);
			}

			return *this;
		}

		/*!
		 * \remarks 调用运算符
		 * \return 
		*/
		void operator()()
		{
			BOOST_ASSERT(mImpl);

			mImpl->call();
		}

	private:
		/*
		BaseImpl的作用：便于把Impl给包装到智能指针中。
		*/
		class BaseImpl
		{
		public:
			BaseImpl() = default;

			virtual ~BaseImpl()
			{
			}

			virtual void call() = 0;
		};//class baseImpl

		template<typename F>
		class Impl : public BaseImpl
		{
		public:
			Impl(F&& f) :
				mFunc(std::move(f))
			{
			}

			virtual ~Impl()
			{
			}

			virtual void call() override
			{
				mFunc();
			}

			F mFunc;
		};//class Impl

		std::unique_ptr<BaseImpl> mImpl;
	};
}//namespace ung

#endif//UTM_HIERARCHICAL_COMPILE

#endif//_UTM_TASK_H_