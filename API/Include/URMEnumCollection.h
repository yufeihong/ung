/*!
 * \brief
 * 枚举合集
 * \file URMEnumCollection.h
 *
 * \author Su Yang
 *
 * \date 2017/05/10
 */
#ifndef _URM_ENUMCOLLECTION_H_
#define _URM_ENUMCOLLECTION_H_

#ifndef _URM_CONFIG_H_
#include "URMConfig.h"
#endif

#ifdef URM_HIERARCHICAL_COMPILE

namespace ung
{
	/*
	In general,32-bit pixel formats are used for the display and back buffer surfaces.
	Most graphics cards should support the first six formats(PF_16BITS_X1R5G5B5,
	PF_16BITS_R5G6B5,PF_24BITS_R8G8B8,PF_32BITS_X8R8G8B8,PF_32BITS_A8R8G8B8,
	PF_8BITS_A8) in some way without a problem.
	In general,you should always check to see if the hardware supports a particular format 
	before using it.
	PF_8BITS_A8,PF_16BITS_FLOAT_R16,and PF_32BITS_FLOAT_R32:
	These kinds of formats are useful for just storing single numbers in a surface.
	We can even generalize data to the multi-component surface formats like 
	PF_32BITS_X8R8G8B8.With PF_32BITS_X8R8G8B8,we could encode a point or vector(x,y,z) 
	in the color components (red,green,blue) of the surface.
	*/
	enum class PixelFormat : uint32
	{
		PF_UNKNOWN,

		/*
		Specifies a 16-bit pixel format where,starting from the leftmost bit,1 bit is not used,5 bits 
		are allocated for red,5 bits are allocated for green,and 5 bits are allocated for blue.
		*/
		PF_16BITS_X1R5G5B5,

		/*
		Specifies a 16-bit pixel format where,starting from the leftmost bit,5 bits are allocated for 
		red,6 bits are allocated for green,and 5 bits are allocated for blue.
		*/
		PF_16BITS_R5G6B5,

		/*
		Specifies a 24-bit pixel format where,starting from the leftmost bit,8 bits are allocated for 
		red,8 bits are allocated for green,and 8 bits are allocated for blue.
		*/
		PF_24BITS_R8G8B8,

		/*
		Specifies a 32-bit pixel format where,starting from the leftmost bit,8 bits are not used,8 
		bits are allocated for red,8 bits are allocated for green, and 8 bits are allocated for blue.
		*/
		PF_32BITS_X8R8G8B8,

		/*
		Specifies a 32-bit pixel format where,starting from the leftmost bit,8 bits are allocated for 
		alpha,8 bits are allocated for red,8 bits are allocated for green,and 8 bits are allocated for 
		blue.
		*/
		PF_32BITS_A8R8G8B8,

		/*
		Specifies an 8-bit alpha-only pixel format.
		*/
		PF_8BITS_A8,

		/*
		Specifies a 16-bit red floating-point pixel format.
		*/
		PF_16BITS_FLOAT_R16,

		/*
		Specifies a 64-bit floating-point pixel format.Starting from the leftmost bit,16 bits are 
		allocated for alpha,16 bits are allocated for blue,16 bits are allocated for green,and 16 
		bits are allocated for red.
		*/
		PF_64BITS_FLOAT_A16B16G16R16,

		/*
		Specifies a 32-bit red floating-point pixel format.
		*/
		PF_32BITS_FLOAT_R32,

		/*
		Specifies a 128-bit floating-point pixel format.Starting from the leftmost bit,32 bits are 
		allocated for alpha,32 bits are allocated for blue,32 bits are allocated for green,and 32 
		bits are allocated for red.
		*/
		PF_128BITS_FLOAT_A32B32G32R32
	};

	//Vertex element semantics,used to identify the meaning of vertex buffer contents
	enum class VertexElementSemantic : uint8
	{
		VES_POSITION = 1,										//Position,3 reals per vertex
		VES_BLEND_WEIGHTS = 2,								//Blending weights
		VES_BLEND_INDICES = 3,								//Blending indices
		VES_NORMAL = 4,											//Normal,3 reals per vertex
		VES_DIFFUSE = 5,											//Diffuse colors
		VES_TEXTURE_COORDINATES = 6,					//Texture coordinates
		VES_BINORMAL = 7,										//Binormal (Y axis if normal is Z)
		VES_TANGENT = 8,										//Tangent (X axis if normal is Z)
		VES_POSITION_HCS = 9,								//其次裁剪空间的Position
		VES_COUNT = 9
	};

	enum class VertexElementType : uint8
	{
		VET_FLOAT1 = 1,
		VET_FLOAT2 = 2,
		VET_FLOAT3 = 3,
		VET_FLOAT4 = 4,
		VET_COLOR = 5,
		VET_COLOR_ARGB = 6,							//D3D style compact color
		VET_COLOR_ABGR = 7							//GL style compact color
	};

	enum class IndexType : uint8
	{
		IT_16BIT,
		IT_32BIT
	};

	/*
	Pools cannot be mixed for different objects contained within one resource (mip levels in a 
	mipmap) and,when a pool is chosen,it cannot be changed.
	*/
	enum class ResourcePoolType : uint32
	{
		/*
		通常，用于动态缓冲区。
		Specify that a resource are placed in the memory pool most appropriate for the set of 
		usages requested for the given resource.This is usually video memory,including both local 
		video memory and accelerated graphics port (AGP) memory.
		The RPT_DEFAULT pool is separate from RPT_MANAGED and RPT_SYSTEM,and it specifies 
		that the resource is placed in the preferred memory for device access.Note that 
		RPT_DEFAULT never indicates that either RPT_MANAGED or RPT_SYSTEM should be 
		chosen as the memory pool type for this resource.
		Resources in the default pool do not persist through transitions between the lost and 
		operational states of the device.These resources must be released before calling reset and 
		must then be re-created.
		When creating resources with RPT_DEFAULT,if video card memory is already committed,
		managed resources will be evicted to free enough memory to satisfy the request.
		Dynamic textures can be locked, even if they are created in RPT_DEFAULT.
		Textures placed in the RPT_DEFAULT pool cannot be locked unless they are dynamic 
		textures.
		*/
		RPT_DEFAULT,

		/*
		通常，用于静态缓冲区。
		Specify a managed resource.Managed resources persist through transitions between the 
		lost and operational states of the device.These resources exist both in video and system 
		memory.A managed resource will be copied into video memory when it is needed during 
		rendering.
		Only the system-memory copy is directly modified.UNG copies your changes to 
		driver-accessible memory as needed.
		However,if the device must be destroyed and re-created, all resources must be re-created.
		VB_USAGE_DYNAMIC和RPT_MANAGED不兼容，不能一起使用。
		Dynamic textures不能使用RPT_MANAGED。

		Applications should use RPT_MANAGED for most static resources because this saves the 
		application from having to deal with lost devices. (Managed resources are restored by the 
		runtime.) This is especially beneficial for unified memory architecture (UMA) systems. 
		Other dynamic resources are not a good match for RPT_MANAGED.In fact,index buffers 
		and vertex buffers cannot be created using RPT_MANAGED together with 
		VB_USAGE_DYNAMIC.
		*/
		RPT_MANAGED,

		/*
		Resources are placed in memory that is not typically accessible by the device.This 
		memory allocation consumes system RAM but does not reduce pageable RAM.These 
		resources do not need to be recreated when a device is lost.Resources in this pool can be 
		locked and can be used as the source for a operation to a memory resource created with 
		RPT_DEFAULT.
		*/
		RPT_SYSTEM,

		/*
		Resources are placed in system RAM and do not need to be recreated when a device is 
		lost.These resources are not bound by device size or format restrictions.Because of this,
		these resources cannot be accessed by the device nor set as textures or render targets.
		However,these resources can always be created,locked,and copied.

		不适用于顶点缓冲区。
		*/
		RPT_SCRATCH
	};

	//frame buffer types.要与整数做&运算，不能为限定作用域。
	enum FrameBufferType : uint32
	{
		FBT_COLOR = 0x1,
		FBT_DEPTH = 0x2,
		FBT_STENCIL = 0x4
	};

	enum ShaderType : uint32
	{
		ST_VERT = 1,
		ST_FRAG = 2
	};
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

#endif//_URM_ENUMCOLLECTION_H_