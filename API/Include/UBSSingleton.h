/*!
 * \brief
 * 单件。
 * \file UBSSingleton.h
 *
 * \author Su Yang
 *
 * \date 2016/01/31
 */
#ifndef _UBS_SINGLETON_H_
#define _UBS_SINGLETON_H_

#ifndef _UBS_CONFIG_H_
#include "UBSConfig.h"
#endif

#ifdef UBS_HIERARCHICAL_COMPILE

#ifndef _INCLUDE_BOOST_THREAD_ONCE_HPP_
#include "boost/thread/once.hpp"
#define _INCLUDE_BOOST_THREAD_ONCE_HPP_
#endif

namespace ung
{
	/*!
	 * \brief
	 * 线程安全的单件类。
	 * \class Singleton
	 *
	 * \author Su Yang
	 *
	 * \date 2016/01/31
	 *
	 * \todo
	 */
	template<typename T>
	class Singleton : boost::noncopyable					//单件类不从内存池(MemoryPool)继承
	{
	public:
		using inst_type = T;

		/*!
		 * \remarks 获取实例。线程安全。
		 * \return inst_type&
		*/
		static inst_type& getInstance()
		{
			boost::call_once(mOnceFlag, init);

			return init();
		}

		void* operator new(size_t size) = delete;
		void* operator new[](size_t elementsTotalSize) = delete;

	protected:
		Singleton()
		{
		}

		virtual ~Singleton()
		{
		}

	private:
		static inst_type& init()
		{
			static inst_type obj;

			return obj;
		}

		static boost::once_flag mOnceFlag;
	};

	template<typename T>
	boost::once_flag Singleton<T>::mOnceFlag;
}//namespace ung

#endif//UBS_HIERARCHICAL_COMPILE

#endif//_UBS_SINGLETON_H_