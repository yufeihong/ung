/*!
 * \brief
 * 事件(事件数据)接口。
 * \file UESIEventData.h
 *
 * \author Su Yang
 *
 * \date 2016/12/25
 */
#ifndef _UES_IEVENTDATA_H_
#define _UES_IEVENTDATA_H_

#ifndef _UES_CONFIG_H_
#include "UESConfig.h"
#endif

#ifdef UES_HIERARCHICAL_COMPILE

#ifndef _UES_EVENTTYPE_H_
#include "UESEventType.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 消息(消息数据)接口(Ride along with the event)
	 * \class IEventData
	 *
	 * \author Su Yang
	 *
	 * \date 2016/12/25
	 *
	 * \todo
	 */
	class UngExport IEventData : public MemoryPool<IEventData>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		IEventData() = default;

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~IEventData();

		/*!
		 * \remarks 获取该数据所跟随的消息类型
		 * \return 
		*/
		virtual EventType const& getEventType() const = 0;

		/*!
		 * \remarks 获取时间戳
		 * \return 
		*/
		virtual real_type getTimeStamp() const = 0;
	};
}//namespace ung

#endif//UES_HIERARCHICAL_COMPILE

#endif//_UES_IEVENTDATA_H_