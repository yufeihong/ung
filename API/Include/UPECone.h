/*!
 * \brief
 * 圆锥体
 * \file UPECone.h
 *
 * \author Su Yang
 *
 * \date 2017/04/08
 */
#ifndef _UPE_CONE_H_
#define _UPE_CONE_H_

#ifndef _UPE_CONFIG_H_
#include "UPEConfig.h"
#endif

#ifdef UPE_HIERARCHICAL_COMPILE

#ifndef _UIF_IBOUNDINGVOLUME_H_
#include "UIFIBoundingVolume.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 圆锥体
	 * \class Cone
	 *
	 * \author Su Yang
	 *
	 * \date 2017/04/08
	 *
	 * \todo
	 */
	class UngExport Cone : public IBoundingVolume
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		Cone() = default;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param Vector3 const & top 顶尖位置
		 * \param Vector3 const & dir 方向向量
		 * \param real_type h 高度
		 * \param Radian halfAngle 半角
		*/
		Cone(Vector3 const& top, Vector3 const& dir, real_type h, Radian halfAngle);

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param Vector3 const & top 顶尖位置
		 * \param Vector3 const & dir 方向向量
		 * \param real_type h 高度
		 * \param Degree halfAngle 半角
		*/
		Cone(Vector3 const& top, Vector3 const& dir, real_type h, Degree halfAngle);

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param Vector3 const & top 顶尖位置
		 * \param Vector3 const & dir 方向向量
		 * \param real_type h 高度
		 * \param real_type radius 底面半径
		*/
		Cone(Vector3 const& top, Vector3 const& dir, real_type h, real_type radius);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~Cone() = default;

		/*!
		 * \remarks 获取顶尖位置
		 * \return 
		*/
		Vector3 const& getTop() const;

		/*!
		 * \remarks 获取方向向量(单位向量)
		 * \return 
		*/
		Vector3 const& getDir() const;

		/*!
		 * \remarks 获取高度
		 * \return 
		*/
		real_type getHeight() const;

		/*!
		 * \remarks 获取半角(弧度)
		 * \return 
		*/
		Radian getHalfAngle() const;

		//

		/*!
		 * \remarks 获取底面半径
		 * \return 
		*/
		real_type getRadius() const;

	private:
		Vector3 mTop;
		Vector3 mDir;
		real_type mHeight;
		Radian mHalfAngle;
		real_type mRadius;
	};
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

#endif//_UPE_CONE_H_