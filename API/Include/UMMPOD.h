/*!
 * \brief
 * POD�ڴ��
 * \file UMMPOD.h
 *
 * \author Su Yang
 *
 * \date 2016/04/09
 */
#ifndef _UMM_POD_H_
#define _UMM_POD_H_

#ifndef _UMM_CONFIG_H_
#include "UMMConfig.h"
#endif

#ifdef UMM_HIERARCHICAL_COMPILE

#ifndef _INCLUDE_BOOST_POOL_HPP_
#include "boost/pool/pool.hpp"
#define _INCLUDE_BOOST_POOL_HPP_
#endif

#ifndef _INCLUDE_BOOST_SINGLETONPOOL_HPP_
#include "boost/pool/singleton_pool.hpp"
#define _INCLUDE_BOOST_SINGLETONPOOL_HPP_
#endif

/*
using namespace boost;
*/

#endif//UMM_HIERARCHICAL_COMPILE

#endif//_UMM_POD_H_