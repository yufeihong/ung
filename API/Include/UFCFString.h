/*!
 * \brief
 * �����ַ���
 * \file UFCFString.h
 *
 * \author Su Yang
 *
 * \date 2016/04/15
 */
#ifndef _UFC_FSTRING_H_
#define _UFC_FSTRING_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#ifndef _INCLUDE_BOOST_HASH_HPP_
#include "boost/functional/hash.hpp"
#define _INCLUDE_BOOST_HASH_HPP_
#endif

namespace ung
{
	/*!
	 * \brief
	 * �����ַ���
	 * \class FString
	 *
	 * \author Su Yang
	 *
	 * \date 2016/04/15
	 *
	 * \todo
	 */
	class UngExport FString : public MemoryPool<FString>
	{
	public:
		typedef size_t size_type;

		explicit FString(String str) :
			mComparison(boost::hash<String>()(str)),
			mString(str)
		{
		}

		const size_type mComparison;
		const String mString;
	};

	//inline size_t hashFString(const FString& sf)
	//{
	//	return sf.mComparison;
	//}

	class FStringHasher
	{
	public:
		size_t operator()(const FString& fs) const
		{
			return fs.mComparison;
		}
	};

	class FStringEqual
	{
	public:
		bool operator()(const FString& s1, const FString& s2) const
		{
			return s1.mComparison == s2.mComparison && s1.mString == s2.mString;
		}
	};

	//inline bool operator==(const FString& s1, const FString& s2)
	//{
	//	return s1.mComparison == s2.mComparison && s1.mString == s2.mString;
	//}

	//inline bool operator!=(const FString& s1, const FString& s2)
	//{
	//	return !operator==(s1,s2);
	//}
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_FSTRING_H_