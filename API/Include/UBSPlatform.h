/*!
 * \brief
 * 平台。
 * \file UBSPlatform.h
 *
 * \author Su Yang
 *
 * \date 2017/03/12
 */
#ifndef _UBS_PLATFORM_H_
#define _UBS_PLATFORM_H_

#ifndef _UBS_CONFIG_H_
#include "UBSConfig.h"
#endif

#ifdef UBS_HIERARCHICAL_COMPILE

#ifndef _UBS_MACRO_H_
#include "UBSMacro.h"
#endif

#ifndef _UBS_SINGLETON_H_
#include "UBSSingleton.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 输出平台信息。
	 * \class Platform
	 *
	 * \author Su Yang
	 *
	 * \date 2016/02/01
	 *
	 * \todo
	 */
	class UngExport Platform : public Singleton<Platform>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		Platform();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~Platform();

		/*!
		 * \remarks 当前平台是否是大端
		 * \return bool
		*/
		bool isBigEndian();

		/*!
		 * \remarks 输出CPU信息
		 * \return void
		*/
		void printProcessor();

		/*!
		 * \remarks 输出大小端信息
		 * \return void
		*/
		void printEndianness();
	};
}//namespace ung

#endif//UBS_HIERARCHICAL_COMPILE

#endif//_UBS_PLATFORM_H_