/*!
 * \brief
 * Log相关头文件的预编译。在微软的编译器中,必须确保这个头文件在CPP中被第一个包含。在.h文件中不包含这个预编译头文件.
 * \file UFCLogPrecompiled.h
 *
 * \author Su Yang
 *
 * \date 2016/01/31
 */
#ifndef _UFC_LOGPRECOMPILED_H_
#define _UFC_LOGPRECOMPILED_H_

#include "boost/log/expressions/predicates/has_attr.hpp"
#include "boost/log/expressions/formatters.hpp"
#include "boost/phoenix/operator.hpp"
#include "boost/log/sources/severity_logger.hpp"
#include "boost/log/sources/record_ostream.hpp"
#include "boost/log/sinks/sync_frontend.hpp"
#include "boost/log/sinks/text_ostream_backend.hpp"
#include "boost/log/sinks/text_file_backend.hpp"
#include "boost/log/support/date_time.hpp"
#include "boost/log/utility/setup/common_attributes.hpp"

#endif//_UFC_LOGPRECOMPILED_H_