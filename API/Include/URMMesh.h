/*!
 * \brief
 * 对网格信息的一个封装，支持多线程安全。
 * \file URMMesh.h
 *
 * \author Su Yang
 *
 * \date 2016/11/13
 */
#ifndef _URM_MESH_H_
#define _URM_MESH_H_

#ifndef _URM_CONFIG_H_
#include "URMConfig.h"
#endif

#ifdef URM_HIERARCHICAL_COMPILE

#ifndef _URM_RESOURCE_H_
#include "URMResource.h"
#endif

#ifndef _URM_SUBMESH_H_
#include "URMSubMesh.h"
#endif

#ifndef _URM_MESHPARSER_H_
#include "URMMeshParser.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 对网格信息的一个封装，支持线程安全。
	 * \class Mesh
	 *
	 * \author Su Yang
	 *
	 * \date 2016/11/13
	 *
	 * \todo
	 */
	class Mesh : public Resource,public std::enable_shared_from_this<Mesh>
	{
	public:
		/*!
		 * \remarks 创建具体资源的回调函数
		 * \return 
		 * \param String const & fullName 包含路径
		*/
		static std::shared_ptr<Resource> creatorCallback(String const& fullName);

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param String const & fullName 包含路径
		*/
		Mesh(String const& fullName);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~Mesh();

		/*!
		 * \remarks 把load任务递送至线程池
		 * \return 
		*/
		virtual void build() override;

		/*!
		 * \remarks 获取数据形式
		 * \return 
		*/
		String const& getDataFormat() const;

		/*!
		 * \remarks 获取所适应的坐标系
		 * \return 
		*/
		String const& getCoordSystem() const;

		/*!
		 * \remarks 获取三角形数量
		 * \return 
		*/
		usize const getFaceCount() const;

		/*!
		 * \remarks 获取顶点数量
		 * \return 
		*/
		usize const getVertexCount() const;

		/*!
		 * \remarks 获取SubMesh数量
		 * \return 
		*/
		usize const getSubMeshCount() const;

		/*!
		 * \remarks 获取包含position信息的容器
		 * \return 
		 * \param Vector3
		*/
		STL_VECTOR(Vector3) const& getPositions() const;

		/*!
		 * \remarks 获取包含tangent信息的容器
		 * \return 
		 * \param Vector4
		*/
		STL_VECTOR(Vector4) const& getTangents() const;

		/*!
		 * \remarks 获取包含bitangent信息的容器
		 * \return 
		 * \param Vector3
		*/
		STL_VECTOR(Vector3) const& getBitangents() const;

		/*!
		 * \remarks 获取包含normal信息的容器
		 * \return 
		 * \param Vector3
		*/
		STL_VECTOR(Vector3) const& getNormals() const;

		/*!
		 * \remarks 获取包含uv信息的容器
		 * \return 
		 * \param Vector2
		*/
		STL_VECTOR(Vector2) const& getUVs() const;

		/*!
		 * \remarks 获取所有的子mesh
		 * \return 
		 * \param std::shared_ptr<SubMesh>
		*/
		STL_VECTOR(std::shared_ptr<SubMesh>) const& getSubMeshs() const;

		/*!
		 * \remarks 获取给定子mesh的索引数量
		 * \return 
		 * \param uint32 subMeshIndex
		*/
		uint32 getIndicesCount(uint32 subMeshIndex) const;

		/*!
		 * \remarks 获取给定子mesh的所有索引
		 * \return 
		 * \param uint32
		*/
		STL_VECTOR(uint32) const& getIndices(uint32 subMeshIndex) const;

		/*!
		 * \remarks 获取图元的组织模式
		 * \return 
		 * \param uint32 subMeshIndex
		*/
		String const& getTrianglesMode(uint32 subMeshIndex) const;

	private:
		/*!
		 * \remarks 加锁，执行load
		 * \return 
		*/
		bool load();

		/*!
		 * \remarks 创建一个SubMesh
		 * \return 
		*/
		std::shared_ptr<SubMesh> createSubMesh();

		/*!
		 * \remarks 设置future
		 * \return 
		 * \param std::future<bool> & fut
		*/
		void setFuture(std::future<bool>&& fut);

		/*!
		 * \remarks 等待future
		 * \return 
		*/
		bool isLoaded() const;

	private:
		String mDataFormat;																//XML,JSON,INFO,BINARY
		String mCoordSystem;																//OpenGL/Direct3D
		usize mFaceCount = 0;
		usize mVertexCount = 0;
		usize mSubMeshCount = 0;
		STL_VECTOR(Vector3) mPositions;
		STL_VECTOR(Vector4) mTangents;
		STL_VECTOR(Vector3) mBitangents;					//临时
		STL_VECTOR(Vector3) mNormals;
		STL_VECTOR(Vector2) mUVs;
		STL_VECTOR(std::shared_ptr<SubMesh>) mSubMeshs;
		mutable std::recursive_mutex mRecursiveMutex;
		mutable std::future<bool> mNakedFuture;
		//mutable std::shared_future<bool> mNakedFuture;
		mutable bool mIsLoaded = false;
	};
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

#endif//_URM_MESH_H_