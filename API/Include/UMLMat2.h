/*!
 * \brief
 * 2x2矩阵
 * \file UMLMat2.h
 *
 * \author Su Yang
 *
 * \date 2016/06/15
 */
#ifndef _UML_MAT2_H_
#define _UML_MAT2_H_

#ifndef _UML_CONFIG_H_
#include "UMLConfig.h"
#endif

#ifdef UML_HIERARCHICAL_COMPILE

namespace ung
{
	/*!
	 * \brief
	 * 2x2矩阵
	 * \class Mat2
	 *
	 * \author Su Yang
	 *
	 * \date 2016/06/15
	 *
	 * \todo
	 */
	template<typename T>
	class Mat2 : public MemoryPool<Mat2<T>>
	{
	public:
		using element_type = T;
		using math_type = MathImpl<element_type>;

		/*!
		 * \remarks 打印Mat2
		 * \return 
		 * \param std::ostream & o
		 * \param const Mat2 & mat
		*/
		friend std::ostream& operator<<(std::ostream& o, const Mat2& mat)
		{
			o << "Mat2(" << mat.m[0][0] << "," << mat.m[0][1] << "," 
				<< mat.m[1][0] << "," << mat.m[1][1] << ")";

			return o;
		}

		/*!
		 * \remarks scalar * Mat2
		 * \return 
		 * \param T scalar
		 * \param Mat2 const &
		*/
		friend Mat2 operator*(T scalar, Mat2 const& mat2)
		{
			return mat2 * scalar;
		}

	public:
		/*
		默认构造函数(单位矩阵)
		*/
		Mat2()
		{
			//单位矩阵
			m[0][0] = m[1][1] = 1.0;
			m[0][1] = m[1][0] = 0.0;
		}

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~Mat2() = default;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param T m00
		 * \param T m01
		 * \param T m10
		 * \param T m11
		*/
		Mat2(T m00, T m01, T m10, T m11)
		{
			m[0][0] = m00;
			m[0][1] = m01;
			m[1][0] = m10;
			m[1][1] = m11;
		}

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param Vec2<T> const & row1
		 * \param Vec2<T> const & row2
		*/
		Mat2(Vec2<T> const& row1, Vec2<T> const& row2)
		{
			m[0][0] = row1.x;
			m[0][1] = row1.y;

			m[1][0] = row2.x;
			m[1][1] = row2.y;
		}

		/*!
		 * \remarks 拷贝构造函数
		 * \return 
		 * \param const Mat2 & other
		*/
		Mat2(const Mat2& other)
		{
			memcpy(m, other.m, 4 * sizeof(T));
		}

		/*!
		 * \remarks 拷贝赋值运算符
		 * \return 
		 * \param const Mat2 & other
		*/
		Mat2& operator=(const Mat2& other)
		{
			if (this != &other)
			{
				memcpy(m, other.m, 4 * sizeof(T));
			}

			return *this;
		}

		/*!
		 * \remarks 移动构造函数
		 * \return 
		 * \param Mat2 & & other
		*/
		Mat2(Mat2&& other) noexcept
		{
			for (uint8 r = 0; r < 2; ++r)
			{
				for (uint8 c = 0; c < 2; ++c)
				{
					m[r][c] = other[r][c];
				}
			}
		}

		/*!
		 * \remarks 移动赋值运算符(不会修改被赋值对象的内存地址)
		 * \return 
		 * \param Mat2 & & other
		*/
		Mat2& operator=(Mat2&& other) noexcept
		{
			if (this != &other)
			{
				for (uint8 r = 0; r < 2; ++r)
				{
					for (uint8 c = 0; c < 2; ++c)
					{
						m[r][c] = other[r][c];
					}
				}
			}

			return *this;
		}

		/*!
		 * \remarks []运算符
		 * \return 返回的是副本，不是引用
		 * \param usize iRow
		*/
		const Vec2<T> operator[](usize iRow) const
		{
			BOOST_ASSERT(iRow >= 0 && iRow <= 1);

			return Vec2<T>(m[iRow]);
		}

		/*!
		 * \remarks []运算符
		 * \return 返回的是副本，不是引用
		 * \param usize iRow
		*/
		Vec2<T> operator[](usize iRow)
		{
			BOOST_ASSERT(iRow >= 0 && iRow <= 1);

			return Vec2<T>(m[iRow]);
		}

		/*!
		 * \remarks 重载()
		 * \return 拷贝
		 * \param usize row
		 * \param usize col
		*/
		T operator()(usize row, usize col) const
		{
			BOOST_ASSERT(row >= 0 && row <= 1);
			BOOST_ASSERT(col >= 0 && col <= 1);

			return m[row][col];
		}

		/*!
		 * \remarks 重载()
		 * \return 引用
		 * \param usize row
		 * \param usize col
		*/
		T& operator()(usize row, usize col)
		{
			BOOST_ASSERT(row >= 0 && row <= 1);
			BOOST_ASSERT(col >= 0 && col <= 1);

			return m[row][col];
		}

		/*!
		 * \remarks 获取参数所指定的元素
		 * \return 引用(注意：auto m00 = m3.getElement(0, 0);auto推断出来的m00的类型是T，而不是T&，必须写成auto&)
		 * \param usize r
		 * \param usize c
		*/
		T& getElement(usize r, usize c)
		{
			BOOST_ASSERT(r >= 0 && r < 2);
			BOOST_ASSERT(c >= 0 && c < 2);

			return m[r][c];
		}

		/*!
		 * \remarks 获取参数所指定的元素
		 * \return 常量引用
		 * \param usize r
		 * \param usize c
		*/
		T const& getElement(usize r, usize c) const
		{
			BOOST_ASSERT(r >= 0 && r < 2);
			BOOST_ASSERT(c >= 0 && c < 2);

			return m[r][c];
		}

		/*!
		 * \remarks 获取00的地址
		 * \return 
		*/
		T* getAddress()
		{
			//多维数组在内存中是连续分布的
			return &m[0][0];;
		}

		/*!
		 * \remarks 获取00的地址
		 * \return 
		*/
		T const* getAddress() const
		{
			return &m[0][0];;
		}

		/*!
		 * \remarks 判断当前矩阵是否等于给定矩阵
		 * \return 
		 * \param const Mat2 & other
		*/
		bool operator==(const Mat2& other) const
		{
			for (usize iRow = 0; iRow < 2; iRow++)
			{
				for (usize iCol = 0; iCol < 2; iCol++)
				{
					if (!(math_type::isEqual(m[iRow][iCol], other.m[iRow][iCol])))
					{
						return false;
					}
				}
			}

			return true;
		}

		/*!
		 * \remarks 判断当前矩阵是否不等于给定矩阵
		 * \return 
		 * \param const Mat2 & other
		*/
		bool operator!=(const Mat2& other) const
		{
			return !(operator==(other));
		}

		/*!
		 * \remarks Mat2 * scalar
		 * \return 
		 * \param T scalar
		*/
		Mat2 operator*(T scalar) const
		{
			Mat2 outMat{ *this };
			outMat *= scalar;

			return outMat;
		}

		/*!
		 * \remarks 当前矩阵*=给定标量
		 * \return 
		 * \param T scalar
		*/
		Mat2& operator*=(T scalar)
		{
			for (uint8 iRow = 0; iRow < 2; iRow++)
			{
				for (uint8 iCol = 0; iCol < 2; iCol++)
				{
					m[iRow][iCol] = m[iRow][iCol] * scalar;
				}
			}

			return *this;
		}

		/*!
		 * \remarks Mat2 * Mat2
		 * \return 
		 * \param Mat2 const & other
		*/
		Mat2 operator*(Mat2 const& other) const
		{
			Mat2 outMat{ *this };
			outMat *= other;

			return outMat;
		}

		/*!
		 * \remarks 当前矩阵*=给定矩阵
		 * \return 
		 * \param Mat2 const & other
		*/
		Mat2& operator*=(Mat2 const& other)
		{
			Mat2 R{};

			for (uint8 iRow = 0; iRow < 2; iRow++)
			{
				for (uint8 iCol = 0; iCol < 2; iCol++)
				{
					R.m[iRow][iCol] = m[iRow][0] * other.m[0][iCol] + m[iRow][1] * other.m[1][iCol];
				}
			}

			//移动赋值
			*this = std::move(R);

			return *this;
		}

		/*!
		 * \remarks Mat2 / scalar
		 * \return 
		 * \param T scalar
		*/
		Mat2 operator/(T scalar) const
		{
			Mat3 outMat{ *this };
			outMat /= scalar;

			return outMat;
		}

		/*!
		 * \remarks 当前矩阵/=给定标量
		 * \return 
		 * \param T scalar
		*/
		Mat2& operator/=(T scalar)
		{
			T invScalar = 1.0 / scalar;

			return operator*=(invScalar);
		}

		/*!
		 * \remarks 如果所有非对角线元素都为0,那么称这种矩阵为对角矩阵.单位矩阵是一种特殊的对角矩阵.
		 * \return 
		*/
		void identity()
		{
			m[0][0] = 1.0;
			m[0][1] = 0.0;
			m[1][0] = 0.0;
			m[1][1] = 1.0;
		}

		/*!
		 * \remarks 计算行列式.在任意方阵中都存在一个标量,称作该方阵的行列式.如果矩阵行列式为0,那么该矩阵包含投影.如果为负,那么该矩阵包含镜像.
		 * \return 
		*/
		T determinant() const
		{
			return m[0][0] * m[1][1] - m[0][1] * m[1][0];
		}

		/*!
		 * \remarks 计算转置矩阵(不改变当前矩阵)
		 * \return 
		 * \param Mat2& outMat
		*/
		void transpose(Mat2& outMat) const
		{
			for (usize iRow = 0; iRow < 2; iRow++)
			{
				for (usize iCol = 0; iCol < 2; iCol++)
				{
					outMat.m[iRow][iCol] = m[iCol][iRow];
				}
			}
		}

		/*!
		 * \remarks 转置，改变了当前矩阵
		 * \return 
		*/
		void transpose()
		{
			std::swap(m[0][1], m[1][0]);
		}

		/*!
		 * \remarks 计算逆矩阵.矩阵转置的逆等于它的逆的转置.矩阵乘积的逆等于矩阵的逆的相反顺序的乘积.
		 * \return 
		 * \param Mat2 & invMat
		*/
		bool inverse(Mat2& invMat) const
		{
			auto det = determinant();

			if (math_type::isEqual(det, 0.0))
			{
				invMat = ZERO;
				return false;
			}

			T invDet = 1.0 / det;

			invMat.m[0][0] = m[1][1] * invDet;
			invMat.m[0][1] = -m[0][1] * invDet;
			invMat.m[1][0] = -m[1][0] * invDet;
			invMat.m[1][1] = m[0][0] * invDet;;
		}

		/*!
		 * \remarks 获取给定行
		 * \return 返回的是拷贝
		 * \param const usize iRow
		*/
		Vec3<T> getRow(const usize iRow) const
		{
			BOOST_ASSERT(iRow < 2);
			return Vec3<T>(m[iRow][0], m[iRow][1]);
		}

		/*!
		 * \remarks 获取给定列
		 * \return 返回的是拷贝
		 * \param const usize iCol
		*/
		Vec3<T> getColumn(const usize iCol) const
		{
			BOOST_ASSERT(iCol < 2);
			return Vec3<T>(m[0][iCol], m[1][iCol]);
		}

		/*!
		 * \remarks 设置给定行
		 * \return 
		 * \param const usize iRow
		 * \param const Vec2<T> & vec
		*/
		void setRow(const usize iRow, const Vec2<T>& vec)
		{
			BOOST_ASSERT(iRow < 2);
			m[iRow][0] = vec.x;
			m[iRow][1] = vec.y;
		}

		/*!
		 * \remarks 设置给定列
		 * \return 
		 * \param const usize iCol
		 * \param const Vec2<T> & vec
		*/
		void setColumn(const usize iCol, const Vec2<T>& vec)
		{
			BOOST_ASSERT(iCol < 2);
			m[0][iCol] = vec.x;
			m[1][iCol] = vec.y;
		}

		/*!
		 * \remarks 沿坐标轴,均匀缩放.
		 * \return 
		 * \param T r
		*/
		Mat2& setUniformScale(T r)
		{
			m[0][0] = m[1][1] = r;
			return *this;
		}

	public:
		T m[2][2];

		static const Mat2 ZERO;
		static const Mat2 IDENTITY;
	};

	template<typename T>
	const Mat2<T> Mat2<T>::ZERO(0, 0, 0, 0);
	template<typename T>
	const Mat2<T> Mat2<T>::IDENTITY(1, 0, 0, 1);
}//namespace ung

#endif//UML_HIERARCHICAL_COMPILE

#endif//_UML_MAT2_H_