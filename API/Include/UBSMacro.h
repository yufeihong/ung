/*!
 * \brief
 * 宏。
 * \file UBSMacro.h
 *
 * \author Su Yang
 *
 * \date 2017/03/12
 */
#ifndef _UBS_MACRO_H_
#define _UBS_MACRO_H_

#ifndef _UBS_CONFIG_H_
#include "UBSConfig.h"
#endif

#ifdef UBS_HIERARCHICAL_COMPILE

#ifndef _INCLUDE_STLIB_TYPEINFO_
#include <typeinfo>
#define _INCLUDE_STLIB_TYPEINFO_
#endif

#ifdef NDEBUG
#define UNG_DEBUGMODE 0
#else
#define UNG_DEBUGMODE 1
#endif

#ifdef UNG_EXPORTS
#define UngExport BOOST_SYMBOL_EXPORT
#else
#define UngExport BOOST_SYMBOL_IMPORT	
#endif

//异常和使用dynamic_cast强制转换的类，必须用BOOST_SYMBOL_VISIBLE声明。
#define UNG_SYMBOL_VISIBLE BOOST_SYMBOL_VISIBLE

#ifdef __cplusplus
#define UNG_C_LINK_BEGIN extern "C" {
#define UNG_C_LINK_END }
#else
#define UNG_C_LINK_BEGIN
#define UNG_C_LINK_END
#endif

#define UNG_STDCALL __stdcall

/*
平台：UNG_PLATFORM
CPU架构：UNG_CPU
字节对齐：UNG_SIMD_ALIGNMENT
CPU地址总线宽度：UNG_ARCH_TYPE
编译器：UNG_COMPILER
编译器版本：UNG_COMPILER_VER
字节序：UNG_ENDIAN
接口导出和导入：_UngExport
调试模式：UNG_DEBUGMODE
是否支持UNICODE：UNG_UNICODE_SUPPORT
*/

//平台
#define UNG_PLATFORM_WIN32 1
#define UNG_PLATFORM_LINUX 2
#define UNG_PLATFORM_APPLE 3

#if defined(__WIN32__) || defined(_WIN32)
#define UNG_PLATFORM UNG_PLATFORM_WIN32
#endif

//----------------------------------------------------

//CPU架构
#define UNG_CPU_UNKNOWN 0
#define UNG_CPU_X86 1
#define UNG_CPU_PPC 2

/*
_MSC_VER:
VS2008:1500(9.0)
VS2010:1600(10.0)
VS2012:1700(11.0)
VS2013:1800(12.0)
VS2015:1900(14.0)
*/
#if (defined(_MSC_VER) && (defined(_M_IX86) || defined(_M_X64))) || (defined(__GNUC__) && (defined(__i386__) || defined(__x86_64__)))
#define UNG_CPU UNG_CPU_X86
#else
#define UNG_CPU UNG_CPU_UNKNOWN
#endif

//----------------------------------------------------

/*
16字节对齐的对象，只可置于为16倍数的地址(即地址最低有效半字节为0x0)
即，地址最低有效半字节必须是“对齐字节数”的整数倍。
半字节，在计算机中，通常将8位二进制数称为字节，而把4位二进制数称为半字节。
比如0x6A341174，其最低有效半字节为0x4。
*/
#if UNG_CPU == UNG_CPU_X86
#define UNG_BYTESALIGNED 8
#define UNG_SIMD_ALIGNMENT 16
#endif

//----------------------------------------------------

//CPU地址总线宽度
#define UNG_ARCHITECTURE_32 1
#define UNG_ARCHITECTURE_64 2

#if defined(__x86_64__) || defined(_M_X64) || defined(__powerpc64__) || defined(__alpha__) || defined(__ia64__) || defined(__s390__) || defined(__s390x__)
#define UNG_ARCH_TYPE UNG_ARCHITECTURE_64
#else
#define UNG_ARCH_TYPE UNG_ARCHITECTURE_32
#endif

//----------------------------------------------------

//编译器
#define UNG_COMPILER_MSVC 1
#define UNG_COMPILER_GNUC 2
#define UNG_COMPILER_BORL 3
#define UNG_COMPILER_WINSCW 4
#define UNG_COMPILER_GCCE 5

#if defined(_MSC_VER)
#define UNG_COMPILER UNG_COMPILER_MSVC
#define UNG_COMPILER_VER _MSC_VER
#else
#pragma error "No known compiler."
#endif

//----------------------------------------------------

/*
字节序:就是数据在内存中的存放顺序.

Little-Endian:就是低位字节排放在内存的低地址端,高位字节排放在内存的高地址端.
Big-Endian:就是高位字节排放在内存的低地址端,低位字节排放在内存的高地址端.

x86系列则采用little endian方式存储数据.
*/
#define UNG_ENDIAN_LITTLE 1
#define UNG_ENDIAN_BIG 2

#ifdef UNG_CONFIG_BIG_ENDIAN
#define UNG_ENDIAN UNG_ENDIAN_BIG
#else
#define UNG_ENDIAN UNG_ENDIAN_LITTLE
#endif

/*
需要在主程序的工程属性中设置.lib的寻找目录，在链接器的输入中设置.lib
如果需要跟进调试的话，需要.pdb文件
*/

//----------------------------------------------------

//是否支持UNICODE
#define UNG_UNICODE_SUPPORT 1

//----------------------------------------------------

//检测禁用的RTTI(Runtime type information)
#ifdef BOOST_NO_RTTI
#error "Disabled RTTI."
#endif

#define PURE = 0
#define OVER override

//----------------------------------------------------

#if UNG_DEBUGMODE
#define UNG_PRINT_ONE_LINE(s)				std::cout << s << std::endl
#else
#define UNG_PRINT_ONE_LINE(s)
#endif

#if UNG_DEBUGMODE
//可以传入一个对象，也可以传入一个类型
#define UNG_PRINT_TYPE_NAME(stuff)		UNG_PRINT_ONE_LINE(typeid(stuff).name())
#else
#define UNG_PRINT_TYPE_NAME(stuff)
#endif

//下个版本再修复
#define UNG_NEXT_VERSION_REPAIR

//临时代码
#define UNG_TEMPORARY_CODE

#endif//UBS_HIERARCHICAL_COMPILE

#endif//_UBS_MACRO_H_