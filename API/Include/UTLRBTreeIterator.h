/*!
 * \brief
 * 红黑树迭代器。
 * \file UTLRBTreeIterator.h
 *
 * \author Su Yang
 *
 * \date 2016/03/17
 */
#ifndef _UNG_UTL_RBTREEITERATOR_H_
#define _UNG_UTL_RBTREEITERATOR_H_

#ifndef _UNG_UTL_CONFIG_H_
#include "UTLConfig.h"
#endif

namespace ung
{
	namespace utl
	{
		struct rbtree_node_base;
		template<typename T>
		struct rbtree_node;

		using RBTreeIteratorTag = std::bidirectional_iterator_tag;

		template<typename T, typename Ref, typename Ptr>
		class RBTreeIterator : public boost::iterator_facade<RBTreeIterator<T, Ref, Ptr>, T, RBTreeIteratorTag>
		{
			friend class boost::iterator_core_access;

		public:
			using value_type = T;
			using self_type = RBTreeIterator<value_type,Ref,Ptr>;
			using pointer = typename iterator_facade::pointer;
			using reference = typename iterator_facade::reference;
			using difference_type = typename iterator_facade::difference_type;
			using iterator_category = RBTreeIteratorTag;
			using iterator = RBTreeIterator<value_type, value_type&, value_type*>;
			using const_iterator = RBTreeIterator<value_type, value_type const&, value_type const*>;

			using node_base_ptr = rbtree_node_base*;
			using node_ptr = rbtree_node<value_type>*;

			/*!
			 * \remarks 默认构造函数
			 * \return 
			*/
			RBTreeIterator() :
				mNode(nullptr)
			{
			}

			/*!
			 * \remarks 构造函数
			 * \return 
			 * \param node_ptr x
			*/
			explicit RBTreeIterator(node_ptr x) :
				mNode(x)
			{
			}

			/*!
			 * \remarks 拷贝构造函数
			 * \return 
			 * \param self_type const & other
			*/
			RBTreeIterator(self_type const& other) :
				mNode(other.mNode)
			{
			}

			/*!
			 * \remarks 拷贝赋值运算符
			 * \return void
			 * \param self_type const & other 
			*/
			void operator=(self_type const& other)
			{
				mNode = other.mNode;
			}

			node_base_ptr mNode;			//用来与容器之间产生一个连接关系(make a reference)

		private:
			reference dereference() const
			{
				return node_ptr(mNode)->mValue;
			}

			void increment()
			{
				//如果有右子节点
				if (mNode->mRight)
				{
					//就向右走
					mNode = mNode->mRight;
					//然后一直往左子树走到底，便是解答
					while (mNode->mLeft)
					{
						mNode = mNode->mLeft;
					}
				}
				//没有右子节点
				else
				{
					//找出父节点
					node_base_ptr y = mNode->mParent;
					//如果现行节点本身是个右子节点，就一直上溯，直到“不为右子节点”止
					while (mNode == y->mRight)
					{
						mNode = y;
						y = y->mParent;
					}

					/*
					若此时的右子节点不等于此时的父节点，此时的父节点便是解答，否则此时的mNode是解答
					这是为了应付一种情况：我们欲寻找根节点的下一节点，而恰巧根节点无右子节点
					*/
					if (mNode->mRight != y)
					{
						mNode = y;
					}
				}
			}

			bool equal(self_type const& other) const
			{
				return mNode == other.mNode;
			}

			void decrement()
			{
				//如果是红节点，且父节点的父节点等于自己，右子节点便是解答
				//发生于mNode为Header时（亦即mNode为end()时），注意：Header的右子节点即rightMost，指向整棵树的max节点
				if (mNode->mColor == rbtree_red && mNode->mParent->mParent == mNode)
				{
					mNode = mNode->mRight;
				}
				//如果有左子节点
				else if (mNode->mLeft)
				{
					//令y指向左子节点
					node_base_ptr y = mNode->mLeft;
					//当y有右子节点时
					while (y->mRight)
					{
						//一直往右子节点走到底
						y = y->mRight;
					}
					//最后便是解答
					mNode = y;
				}
				//既非根节点，亦无左子节点
				else
				{
					//找出父节点
					node_base_ptr y = mNode->mParent;
					//当现行节点身为左子节点时，一直交替往上走，直到现行节点不为左子节点
					while (mNode == y->mLeft)
					{
						mNode = y;
						y = y->mParent;
					}
					//此时之父节点便是解答
					mNode = y;
				}
			}
		};
	}//namespace utl
}//namespace ung

#endif//_UNG_UTL_RBTREEITERATOR_H_