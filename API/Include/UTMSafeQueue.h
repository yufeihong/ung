#ifndef _UTM_SAFEQUEUE_H_
#define _UTM_SAFEQUEUE_H_

#ifndef _UTM_CONFIG_H_
#include "UTMConfig.h"
#endif

#ifdef UTM_HIERARCHICAL_COMPILE

#ifndef _INCLUDE_STLIB_MUTEX_
#include <mutex>
#define _INCLUDE_STLIB_MUTEX_
#endif

namespace ung
{
	template<typename T>
	class SafeQueue
	{
	public:
		SafeQueue() = default;

		~SafeQueue() = default;

		SafeQueue(SafeQueue&) = delete;

		SafeQueue& operator=(SafeQueue&) = delete;

		void push(T value)
		{
			std::shared_ptr<T> elem(std::make_shared<T>(std::move(value)));

			{
				std::lock_guard<std::mutex> lg(mMutex);
				mQueue.push(std::move(elem));
			}

			//不保证哪个线程被通知到。
			mCondition.notify_one();
		}

		/*
		试图从队列中弹出值，但总是立即返回，即便没有能获取到值。
		*/
		bool tryPop(T& valueRef)
		{
			std::lock_guard<std::mutex> lg(mMutex);
			//注意：这里不能调用SafeQueue::empty()，否则会死锁，因为互斥体不是递归的。
			if (mQueue.empty())
			{
				return false;
			}

			valueRef = std::move(*mQueue.front());
			mQueue.pop();

			return true;
		}

		/*
		如果没有获取到值，那么返回nullptr
		*/
		std::shared_ptr<T> tryPop()
		{
			std::lock_guard<std::mutex> lg(mMutex);
			if (mQueue.empty())
			{
				return std::shared_ptr<T>();
			}

			std::shared_ptr<T> ptrT = mQueue.front();
			mQueue.pop();

			return ptrT;
		}

		/*
		一直等待，直到有值可以获取。
		*/
		void waitAndPop(T& valueRef)
		{
			std::unique_lock<std::mutex> ul(mMutex);
			//为空的话，empty返回false，整个lambda表达式返回true。
			mCondition.wait(lk, [this] {return !(this->mQueue.empty()); });

			value = std::move(*mQueue.front());
			mQueue.pop();
		}

		std::shared_ptr<T> waitAndPop()
		{
			std::unique_lock<std::mutex> ul(mMutex);
			mCondition.wait(lk, [this] {return !(this->mQueue.empty()); });

			std::shared_ptr<T> ptrT = mQueue.front();
			mQueue.pop();

			return ptrT;
		}

		bool empty() const
		{
			std::lock_guard<std::mutex> lg(mMutex);
			return mQueue.empty();
		}

	private:
		mutable std::mutex mMutex;
		std::queue<std::shared_ptr<T>> mQueue;
		std::condition_variable mCondition;
	};
}//namespace ung

#endif//UTM_HIERARCHICAL_COMPILE

#endif//_UTM_SAFEQUEUE_H_

/*
为了避免有关接口固有的竞争条件，所以需要将front()和pop()给组合到单个函数调用中去。这个问题，使用递归锁是无法解决的，比如：
线程1原本的想法是：拿到第一个元素的拷贝后，删除第一个元素
队列的当前状态为[1,2]：
线程1：
int value = myQueue.front();										//value为1
队列的当前状态为[1,2]
线程2：
myQueue.pop();
队列的当前状态为[2]
线程1：
myQueue.pop();
队列的当前状态为[]，而线程1原本期待的操作完状态为：[2]
这种问题，加入你在pop接口中，添加了一个递归锁，你顶多也就是给empty接口也添加一个递归锁，然后在pop接口中调用empty。
所以，必须将front()和pop()给组合到单个函数调用中去。
*/

/*
当一个元素进入队列时，如果不止一个线程处于等待状态，那么只有一个线程会被通知到而被唤醒。然而，如果被唤醒的线程在waitAndPop()中引发了异常，例如，
构造std::shared_ptr<>的时候，那么就没有线程将被唤醒。解决方案：
1，通知all，代价就是当最后它们发现队列为空的时候，其中的大部分线程都要重新进入睡眠状态。
2，当引发异常时，让waitAndPop()再次通知一个等待中的线程，这样另外一个线程就可以被唤醒。
3，将std::shared_ptr<>的初始化放到push()中，并且队列元素存储为std::shared_ptr<T>。将队列内的智能指针给进行复制是不会引发异常的，这样waitAndPop()
就安全了。数据由std::shared_ptr<>来持有，有一个额外的好处：可以在push()的锁外面完成实例的分配，因为内存分配通常是很昂贵的操作，这种方式就有助于提高
队列的性能，因为它减少了持有互斥元的时间，让其他线程可以在队列上执行操作。
*/