/*!
 * \brief
 * MultiMap
 * \file UTLMultiMap.h
 *
 * \author Su Yang
 *
 * \date 2016/04/06
 */
#ifndef _UNG_UTL_MULTIMAP_H_
#define _UNG_UTL_MULTIMAP_H_

#ifndef _UNG_UTL_CONFIG_H_
#include "UTLConfig.h"
#endif

#ifndef _UNG_UTL_ALLOCATOR_H_
#include "UTLAllocator.h"
#endif

#ifndef _UNG_UTL_RBTREE_H_
#include "UTLRBTree.h"
#endif

#ifndef _UNG_UTL_TYPETRAITS_H_
#include "UTLTypeTraits.h"
#endif

namespace ung
{
	namespace utl
	{
		/*!
		 * \brief
		 * MultiMap
		 * \class MultiMap
		 *
		 * \author Su Yang
		 *
		 * \date 2016/04/06
		 *
		 * \todo
		 */
		template<typename Key,typename T,typename Compare = std::less<Key>,typename Alloc = Allocator<rbtree_node<std::pair<Key const,T>>>>
		class MultiMap
		{
		public:
			using key_type = Key;
			using data_type = T;
			using mapped_type = T;
			using value_type = std::pair<Key const, T>;
			using key_compare = Compare;
			using self_type = MultiMap<key_type, data_type, key_compare, Alloc>;

			/*!
			 * \brief
			 * Functor for comparing two element values
			 * \class value_compare
			 *
			 * \author Su Yang
			 *
			 * \date 2016/04/06
			 *
			 * \todo
			 */
			class value_compare
			{
				friend class MultiMap<Key, T, Compare, Alloc>;

			public:
				using first_argument_type = value_type;
				using second_argument_type = value_type;
				using result_type = bool;

				value_compare(key_compare predicate) :
					comp(predicate)
				{
				}

				bool operator()(const value_type& x, const value_type& y) const
				{
					return comp(x.first, y.first);
				}

			protected:
				key_compare mComp;
			};//end class value_compare

		private:
			using rep_type = RBTree<key_type,value_type,ung::utl::select1st<value_type>,key_compare,Alloc>;

		public:
			using pointer = typename rep_type::pointer;
			using const_pointer = typename rep_type::const_pointer;
			using reference = typename rep_type::reference;
			using const_reference = typename rep_type::const_reference;
			using iterator = typename rep_type::iterator;
			using const_iterator = typename rep_type::const_iterator;
			using reverse_iterator = typename rep_type::reverse_iterator;
			using const_reverse_iterator = typename rep_type::const_reverse_iterator;
			using size_type = typename rep_type::size_type;
			using difference_type = typename rep_type::difference_type;

			/*!
			 * \remarks 默认构造函数
			 * \return 
			*/
			MultiMap() :
				mTree(key_compare())
			{
			}

			/*!
			 * \remarks 构造函数
			 * \return 
			 * \param key_compare const & comp
			*/
			explicit MultiMap(key_compare const& comp) :
				mTree(comp)
			{
			}

			/*!
			 * \remarks 构造函数
			 * \return 
			 * \param Iter first
			 * \param Iter last
			*/
			template<typename Iter>
			MultiMap(Iter first, Iter last) :
				mTree(key_compare())
			{
				mTree.insertEqual(first, last);
			}

			/*!
			 * \remarks 构造函数
			 * \return 
			 * \param Iter first
			 * \param Iter last
			 * \param key_compare const & comp
			*/
			template<typename Iter>
			MultiMap(Iter first, Iter last,key_compare const& comp) :
				mTree(comp)
			{
				mTree.insertEqual(first, last);
			}

			/*!
			 * \remarks 拷贝构造函数
			 * \return 
			 * \param self_type const & x
			*/
			MultiMap(self_type const& x) :
				mTree(x.mTree)
			{
			}

			/*!
			 * \remarks 拷贝赋值运算符
			 * \return 
			 * \param self_type const & x
			*/
			self_type& operator=(self_type const& x)
			{
				mTree = x.mTree;

				return *this;
			}

			/*!
			 * \remarks 移动构造函数
			 * \return 
			 * \param self_type && x
			*/
			MultiMap(self_type&& x) noexcept :
				mTree(std::move(x.mTree))
			{
			}

			/*!
			 * \remarks 移动赋值运算符
			 * \return 
			 * \param self_type && x
			*/
			self_type& operator=(self_type&& x) noexcept
			{
				mTree = std::move(x.mTree);

				return *this;
			}

			MultiMap(std::initializer_list<value_type> il) :
				mTree(key_compare())
			{
				mTree.insert(il.begin(), il.end());
			}

			MultiMap(std::initializer_list<value_type> il,const key_compare& Pred) :
				mTree(Pred)
			{
				mTree.insert(il.begin(), il.end());
			}

			self_type& operator=(std::initializer_list<value_type> il)
			{
				mTree.clear();
				mTree.insert(il.begin(), il.end());

				return *this;
			}

			iterator begin()
			{
				return mTree.begin();
			}

			const_iterator begin() const
			{
				return mTree.begin();
			}

			iterator end()
			{
				return mTree.end();
			}

			const_iterator end() const
			{
				return mTree.end();
			}

			reverse_iterator rbegin()
			{
				return mTree.rbegin();
			}

			const_reverse_iterator rbegin() const
			{
				return mTree.rbegin();
			}

			reverse_iterator rend()
			{
				return mTree.rend();
			}

			const_reverse_iterator rend() const
			{
				return mTree.rend();
			}

			bool empty() const
			{
				return mTree.empty();
			}

			size_type size() const
			{
				return mTree.size();
			}

			/*!
			 * \remarks Find element matching k or insert with default mapped
			 * \return 
			 * \param key_type const & k
			*/
			mapped_type& operator[](key_type const& k)
			{
				/*
				value_type(k, data_type()):
				根据键值和实值做出一个元素，由于实值未知，所以产生一个与实值型别相同的暂时对象替代。
				insert(value_type(k, data_type()))：
				将该元素插入到map里面去。插入操作返回一个pair，其第一个元素是个迭代器，指向插入妥当的新元素，或指向插入失败点（键值重复）的旧元素。注意，如果
				下标操作符作为左值运用（通常表示要填加新元素），我们正好以此“实值待填”的元素将位置卡好。如果下标操作符作为右值运用（通常表示要根据键值取其实值），
				此时的插入操作所返回的pair的第一元素（是个迭代器）恰指向键值符合的旧元素。
				(insert(value_type(k, data_type()))).first：
				取插入操作所返回的pair的第一元素，是个迭代器，指向被插入的元素。
				*((insert(value_type(k, data_type()))).first)：
				解引用该迭代器，获得一个map元素，是一个由键值和实值组成的pair。
				(*((insert(value_type(k, data_type()))).first)).second：
				取其第二元素，即为实值。注意，这个实值以by reference方式传递，所以它作为左值或右值都可以。
				*/
				return (*((insert(value_type(k, data_type()))).first)).second;
			}

			mapped_type& operator[](key_type&& k)
			{
				return (*((insert(value_type(std::move(k), data_type()))).first)).second;
			}

			/*!
			 * \remarks Find element matching k
			 * \return 
			 * \param const key_type & k
			*/
			mapped_type& at(const key_type& k)
			{
				iterator where = mTree.lowerBound(k);

				return where->second;
			}

			const mapped_type& at(const key_type& k) const
			{
				const_iterator where = mTree.lowerBound(k);

				return where->second;
			}

			void swap(self_type& x)
			{
				mTree.swap(x.mTree);
			}

			std::pair<iterator, bool> insert(value_type const& x)
			{
				return mTree.insertEqual(x);
			}

			iterator insert(iterator position, value_type const& x)
			{
				return mTree.insertEqual(position, x);
			}

			template<typename Iter>
			void insert(Iter first, Iter last)
			{
				mTree.insertEqual(first, last);
			}

			void erase(iterator position)
			{
				mTree.erase(position);
			}

			size_type erase(key_type const& x)
			{
				return mTree.erase(x);
			}

			void erase(iterator first, iterator last)
			{
				mTree.erase(first, last);
			}

			void clear()
			{
				mTree.clear();
			}

			iterator find(key_type const& x)
			{
				return mTree.find(x);
			}

			const_iterator find(key_type const& x) const
			{
				return mTree.find(x);
			}

			size_type count(key_type const& x) const
			{
				return mTree.count(x);
			}

			iterator lowerBound(key_type const& x)
			{
				return mTree.lowerBound(x);
			}

			const_iterator lowerBound(key_type const& x) const
			{
				return mTree.lowerBound(x);
			}

			iterator upperBound(key_type const& x)
			{
				return mTree.upperBound(x);
			}

			const_iterator upperBound(key_type const& x) const
			{
				return mTree.upperBound(x);
			}

			std::pair<iterator, iterator> equalRange(key_type const& x)
			{
				return mTree.equalRange(x);
			}

			std::pair<const_iterator, const_iterator> equalRange(key_type const& x) const
			{
				return mTree.equalRange(x);
			}

		private:
			rep_type mTree;

			friend bool operator==(MultiMap const&, MultiMap const&);
			friend bool operator<(MultiMap const&, MultiMap const&);
			friend void swap(MultiMap&, MultiMap&);
		};

		template<typename Key,typename T,typename Compare,typename Alloc>
		inline bool operator==(MultiMap<Key, T, Compare, Alloc> const& x, MultiMap<Key, T, Compare, Alloc> const& y)
		{
			return x.mTree == y.mTree;
		}

		template<typename Key, typename T, typename Compare, typename Alloc>
		inline bool operator<(MultiMap<Key, T, Compare, Alloc> const& x, MultiMap<Key, T, Compare, Alloc> const& y)
		{
			return x.mTree < y.mTree;
		}

		template<typename Key, typename T, typename Compare, typename Alloc>
		inline void swap(MultiMap<Key, T, Compare, Alloc>& x,MultiMap<Key, T, Compare, Alloc>& y)
		{
			x.swap(y);
		}
	}//namespace utl
}//namespace ung

#endif//_UNG_UTL_MULTIMAP_H_