/*!
 * \brief
 * 内存分配器
 * \file UMMAllocator.h
 *
 * \author Su Yang
 *
 * \date 2016/04/10
 */
#ifndef _UMM_ALLOCATOR_H_
#define _UMM_ALLOCATOR_H_

#ifndef _UMM_CONFIG_H_
#include "UMMConfig.h"
#endif

#ifdef UMM_HIERARCHICAL_COMPILE

#ifndef _UMM_MACRO_H_
#include "UMMMacro.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 分配释放辅助
	 * \class AllocatorHelper
	 *
	 * \author Su Yang
	 *
	 * \date 2017/05/30
	 *
	 * \todo
	 */
	class AllocatorHelper
	{
	public:
		using size_type = size_t;

		/*!
		 * \remarks 检查地址是否对齐
		 * \return 
		 * \param void * address
		*/
		static void checkAddress(void* address);

		/*!
		 * \remarks 对齐分配
		 * \return 
		 * \param size_type size
		*/
		static void* mallocAligned(size_type bytes);

		/*!
		 * \remarks 对齐释放
		 * \return 
		 * \param void * memblock
		*/
		static void freeAligned(void* memblock);

		/*!
		* \remarks 将参数所要求的bytes上调至UMM_BYTESALIGNED的倍数
		* \return
		* \param size_type bytes
		*/
		static size_type roundUp(size_type bytes);
	};
}//namespace ung

namespace ung
{
	/*!
	* \brief
	* 第一级配置器(不主动调用，而是由第二级配置器根据情况来调用)
	* 考虑到小型区块所可能造成的内存破碎问题，这里设计了双层级配置器。
	* 客户要记得调用static void(*set_malloc_handler(void(*f)()))()来设置自己的内存不足处理例程。
	* \class MallocAllocator
	*
	* \author Su Yang
	*
	* \date 2016/04/10
	*
	* \todo
	*/
	class MallocAllocator
	{
	public:
		using size_type = size_t;

		/*!
		 * \remarks 从内存条获取空间
		 * \return 
		 * \param size_type size
		*/
		static void* allocate(size_type size);

		/*!
		 * \remarks 归还空间给内存条
		 * \return 
		 * \param void* p
		 * \param size_type
		*/
		static void deallocate(void* p, size_type size);

		/*!
		* \remarks Clients必须调用这个仿真C++的set_new_handler()，来指定自己的out-of-memory handler，不能直接运用C++ new_handler机制，因为不是使用::operator new来配置内存的
		* \return 
		*/
		static void(*set_malloc_handler(void(*f)()))();

	private:
		/*!
		 * \remarks 启动out-of-memory handler模块。当无法从内存条中取得要求大小的空间时，代码逻辑进入out-of-memory handler模块。
		 * \return 
		 * \param size_type size
		*/
		static void* oom_malloc(size_type size);

		//这是一个函数指针的声明，用于持有客户set的"内存不足处理的例程"
		static void (*oom_malloc_handler)();
	};//end class MallocAllocator

	//-------------------------------------------------------------------------------------------------------------------------

	/*!
	* \brief
	* 第二级配置器
	* 第二级配置器视情况采用不同的策略：
	* a，当配置区块超过UMM_LEVEL_TWO_MAX_BYTES bytes时，视之为“足够大”，便调用第一级配置器。
	* b，当配置区块小于等于UMM_LEVEL_TWO_MAX_BYTES bytes时，视之为“过小”，采用内存池方式，而不再求助于第一级配置器。此法称之为次层配置（sub-allocation）
	* 第二级配置器维护了UMM_FREELISTS_COUNT个链表，来负责UMM_FREELISTS_COUNT种小型区块的配置能力。
	* \class PoolAllocator
	*
	* \author Su Yang
	*
	* \date 2016/04/10
	*
	* \todo
	*/
	class PoolAllocator
	{
	public:
		using size_type = size_t;

		/*!
		 * \remarks 配置空间。其中包含level策略的判断。
		 * \return 
		 * \param size_type& bytes 这里的引用，是为了把对齐后的字节数能够让BigDog在new时收集
		*/
		UngExport static void* allocate(size_type& bytes);

		/*!
		 * \remarks 释放空间。其中包含level策略的判断。
		 * \return 
		 * \param void * p
		 * \param size_type bytes
		*/
		UngExport static void deallocate(void* p, size_type bytes);

	/*
	private说明符之后的成员可以被类的成员函数访问,但是不能被使用该类的客户代码访问.
	private部分封装了(即隐藏了)类的实现细节.
	*/
	private:
		/*
		自由链表的节点构造
		从第一字段来看，obj_type对象为一个指针，指向相同形式的另一个obj_type对象
		从第二字段来看，obj_type对象为一个指针，指向实际的区块
		*/
		union obj_type
		{
			union obj_type* mLink;
			char mData[1];
		};

		/*!
		* \remarks 根据区块的大小，决定使用第n号自由链表，n从0起算
		* \return 
		* \param size_type bytes
		*/
		static  size_type calFreeListsIndex(size_type bytes);

		/*!
		 * \remarks 根据区块的大小，决定使用第n号自由链表，n从0起算，计算n号索引的起始地址
		 * \return n号索引的起始地址
		 * \param size_type bytes
		*/
		static obj_type* volatile* chooseFreeList(size_type bytes);

		/*!
		* \remarks 为free list重新补充空间，新的空间取自内存池
		* \return 
		* \param size_type size 这个free list的区块的字节大小
		*/
		static void* refill(size_type size);

		/*!
		* \remarks 从内存池中配置一大块空间给free list使用，空间大小为可容纳num个大小为size的区块，如果配置num个区块有所不便，num可能会降低
		* \return 
		* \param size_type size 区块字节大小，已经上调至UMM_BYTESALIGNED的倍数
		* \param size_t& num 区块的数量
		*/
		static char* getSpace(size_type size, size_t& num);

		static char* sStart;																							//内存池起始位置
		static char* sEnd;																							//内存池结束位置
		//用于记录从内存条成功获取的所有空间补充给内存池来使用的总量
		static size_type sHeapSize;

		/*
		free list本身并不包含内存空间，只是用其指针去指向一个实际的内存空间，这个实际的内存空间位于内存池中
		每个空闲列表都具有固定大小的格子，UMM_FREELISTS_COUNT个固定的大小分别为：
		0:8,1:16,2:24,3:32,4:40,5:48,6:56,7:64,8:72,9:80,10:88,11:96,12:104,13:112,14:120,15:128...
		free list的索引每增加1，其小格子的固定大小就增加UMM_BYTESALIGNED
		*/
		static obj_type* volatile sFreeLists[UMM_FREELISTS_COUNT];								//UMM_FREELISTS_COUNT个自由链表
	};//end class PoolAllocator

	//-------------------------------------------------------------------------------------------------------------------------

	/*!
	* \brief
	* 可用于容器的标准配置器(对内存池的包装)，使配置器的配置单位从bytes转为元素的大小
	* 必须提供的性质：
	* 一个value_type
	* 一个构造函数
	* 一个template构造函数，用来在类型有所改变时复制内部状态。注意：template构造函数并不会抑制copy构造函数
	* 一个成员函数allocate()
	* 一个成员函数deallocate()
	* 操作符==和!=
	* 无须提供construct()或destroy()。
	* \class ContainerAllocator
	*
	* \author Su Yang
	*
	* \date 2016/04/11
	*
	* \todo
	*/
	template<typename T>
	class ContainerAllocator
	{
	public:
		using value_type = T;

		using size_type = size_t;

		/*!
		 * \remarks 不抛出异常的默认构造函数
		 * \return 
		*/
		ContainerAllocator() noexcept
		{
		}

		/*!
		 * \remarks 不抛出异常的拷贝构造函数
		 * \return 
		 * \param ContainerAllocator<U> const &
		*/
		template<typename U>
		ContainerAllocator(ContainerAllocator<U> const&) noexcept
		{
		}

		/*!
		 * \remarks 分配
		 * \return 
		 * \param size_type num
		*/
		T* allocate(size_type num)
		{
			if (num == 0)
			{
				return nullptr;
			}

			auto size = num * sizeof(value_type);
			auto p = static_cast<T*>(PoolAllocator::allocate(size));

			return p;
		}

		/*!
		 * \remarks 释放
		 * \return 
		 * \param T* p
		 * \param size_type num
		*/
		void deallocate(T* p, size_type num)
		{
			if (num != 0)
			{
				auto size = num * sizeof(value_type);
				PoolAllocator::deallocate(p, size);
			}
		}
	};//end class ContainerAllocator

	/*!
	 * \remarks 重载==运算符
	 * \return 
	 * \param ContainerAllocator<T> const &
	 * \param ContainerAllocator<U> const &
	*/
	template<typename T,typename U>
	bool operator==(ContainerAllocator<T> const&, ContainerAllocator<U> const&) noexcept
	{
		return true;
	}

	/*!
	 * \remarks 重载!=运算符
	 * \return 
	 * \param ContainerAllocator<T> const &
	 * \param ContainerAllocator<U> const &
	*/
	template<typename T, typename U>
	bool operator!=(ContainerAllocator<T> const&, ContainerAllocator<U> const&) noexcept
	{
		return false;
	}

	//-------------------------------------------------------------------------------------------------------------------------

	/*!
	 * \brief
	 * 用于Boost库的指针容器的克隆分配器
	 * 要求在定义指针容器的.h文件中include容器中元素的头文件。
	 * \class BoostPointerContainerClone
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/07
	 *
	 * \todo
	 */
	template<typename T>
	class BoostPointerContainerClone
	{
	public:
		using value_type = T;

		/*!
		 * \remarks boost指针容器的克隆分配器的固定接口
		 * \return 
		 * \param const T& r
		*/
		static T* allocate_clone(const T& r)
		{
			/*
			不能通过new来调用拷贝构造函数来实现，因为指针容器中可能存储的是抽象类。
			所以，只能通过定义一个clone接口来实现拷贝构造函数的功能。
			比如：virtual T* clone() const PURE;
			*/
			T* pClone = r.clone();

			return pClone;
		}

		/*!
		 * \remarks boost指针容器的克隆分配器的固定接口
		 * \return 
		 * \param const T* r
		*/
		static void deallocate_clone(const T* r)
		{
			UNG_DEL(r);
		}
	};//end class BoostPointerContainerClone

	//-------------------------------------------------------------------------------------------------------------------------

	/*!
	 * \brief
	 * 应用于无法从MemoryPool<>继承的类。(要求具有默认构造函数)
	 * \class AdapterAllocator
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/09
	 *
	 * \todo
	 */
	class AdapterAllocator
	{
	public:
		/*!
		 * \remarks 分配
		 * \return 
		*/
		template<typename T>
		static T* allocate()
		{
			auto size = sizeof(T);

			auto p = static_cast<T*>(PoolAllocator::allocate(size));

			//通过定位new来进行默认初始化(这样就调用了T的默认构造函数)
			new (p) T();

			return p;
		}

		/*!
		 * \remarks 分配数组
		 * \return 
		 * \param uint32 count
		*/
		template<typename T>
		static T* allocate(uint32 count)
		{
			auto size = sizeof(T) * count;

			auto p = static_cast<T*>(PoolAllocator::allocate(size));

			auto pp = p;
			for (uint32 i = 0; i < count; ++i)
			{
				//通过定位new来进行默认初始化(这样就调用了T的默认构造函数)
				new (pp++) T();
			}

			return p;
		}

		/*!
		 * \remarks 释放
		 * \return 
		 * \param void * p
		*/
		template<typename T>
		static void deallocate(void* p)
		{
			if (!p)
			{
				return;
			}

			//调用析构函数
			static_cast<T*>(p)->~T();

			PoolAllocator::deallocate(p, sizeof(T));

#if UNG_DEBUGMODE
			BigDog::getInstance().doDelete(p);
#endif

			p = nullptr;
		}

		/*!
		 * \remarks 释放数组
		 * \return 
		 * \param void * p
		 * \param uint32 count
		*/
		template<typename T>
		static void deallocate(void* p,uint32 count)
		{
			if (!p)
			{
				return;
			}

			auto pp = static_cast<T*>(p);
			for (uint32 i = 0; i < count; ++i)
			{
				//调用析构函数
				(pp++)->~T();
			}

			PoolAllocator::deallocate(p, sizeof(T) * count);

#if UNG_DEBUGMODE
			BigDog::getInstance().doDelete(p);
#endif

			p = nullptr;
		}

#if UNG_DEBUGMODE
		/*!
		 * \remarks 分配
		 * \return 
		*/
		template<typename T>
		static T* allocate(std::string const& info)
		{
			auto size = sizeof(T);

			auto p = static_cast<T*>(PoolAllocator::allocate(size));

			//通过定位new来进行默认初始化(这样就调用了T的默认构造函数)
			new (p) T();

			BigDog::getInstance().doNew(p, size, info);

			return p;
		}

		/*!
		 * \remarks 分配数组
		 * \return 
		 * \param uint32 count
		*/
		template<typename T>
		static T* allocate(uint32 count,std::string const& info)
		{
			auto size = sizeof(T) * count;

			auto p = static_cast<T*>(PoolAllocator::allocate(size));

			auto pp = p;
			for (uint32 i = 0; i < count; ++i)
			{
				//通过定位new来进行默认初始化(这样就调用了T的默认构造函数)
				new (pp++) T();
			}

			BigDog::getInstance().doNew(p, size, info);

			return p;
		}
#endif
	};//end class AdapterAllocator
}//namespace ung

#endif//UMM_HIERARCHICAL_COMPILE

#endif//_UMM_ALLOCATOR_H_