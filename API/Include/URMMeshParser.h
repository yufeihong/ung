/*!
 * \brief
 * 解析mesh文件。
 * \file URMMeshParser.h
 *
 * \author Su Yang
 *
 * \date 2016/11/13
 */
#ifndef _URM_MESHPARSER_H_
#define _URM_MESHPARSER_H_

#ifndef _URM_CONFIG_H_
#include "URMConfig.h"
#endif

#ifdef URM_HIERARCHICAL_COMPILE

#ifndef _UFC_PARSER_H_
#include "UFCParser.h"
#endif

namespace ung
{
	class Material;
	class Mesh;

	/*!
	 * \brief
	 * 解析mesh文件。
	 * \class MeshParser
	 *
	 * \author Su Yang
	 *
	 * \date 2016/11/13
	 *
	 * \todo
	 */
	class MeshParser : public PropertyTreeParser
	{
	public:
		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param String const & fileName
		*/
		MeshParser(String const& fileName);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~MeshParser();

		/*!
		 * \remarks 获取材质的数量
		 * \return 
		*/
		uint32 getMaterialCount();

		/*!
		 * \remarks 获取mesh的数量
		 * \return 
		*/
		uint32 getMeshCount();

		/*!
		 * \remarks 读取材质
		 * \return 
		 * \param ufast index
		 * \param std::shared_ptr<Material> materialPtr
		*/
		void readMaterial(ufast index,std::shared_ptr<Material> materialPtr);

		/*!
		 * \remarks 读取mesh
		 * \return 
		 * \param std::shared_ptr<Mesh> meshPtr
		*/
		void readMesh(std::shared_ptr<Mesh> meshPtr);

	private:
		//用于存放Materials节点中的所有Material节点
		STL_VECTOR(boost::property_tree::ptree) mMaterialsVec;
		//用于存放Meshs节点中的所有Mesh节点
		STL_VECTOR(boost::property_tree::ptree) mMeshsVec;
	};
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

#endif//_URM_MESHPARSER_H_