/*!
 * \brief
 * 实用工具包。主要包含对freeglut，glfw和wgl的封装。
 * 客户代码包含这个头文件。
 * \file UUT.h
 *
 * \author Su Yang
 *
 * \date 2016/11/9
 */
#ifndef _UUT_H_
#define _UUT_H_

#ifndef _UUT_ENABLE_H_
#include "UUTEnable.h"
#endif

#ifdef UUT_HIERARCHICAL_COMPILE

#ifndef _UUT_TOOLS_H_
#include "UUTTools.h"
#endif

#ifndef _UUT_WGL_H_
#include "UUTWgl.h"
#endif

#ifndef _UUT_WINDOW_H_
#include "UUTWindow.h"
#endif

#ifndef _UUT_FREEGLUTAPPLICATION_H_
#include "UUTFreeGlutApplication.h"
#endif

#ifndef _UUT_GLPROGRAM_H_
#include "UUTGLProgram.h"
#endif

#ifndef _UUT_GLSHADERMANAGER_H_
#include "UUTGLShaderManager.h"
#endif

#endif//UUT_HIERARCHICAL_COMPILE

#endif//_UUT_H_