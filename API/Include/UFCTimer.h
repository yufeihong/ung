/*!
 * \brief
 * Timer。
 * \file UFCTimer.h
 *
 * \author Su Yang
 *
 * \date 2016/02/01
 */
#ifndef _UFC_TIMER_H_
#define _UFC_TIMER_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#ifndef _INCLUDE_STLIB_CHRONO_
#include <chrono>
#define _INCLUDE_STLIB_CHRONO_
#endif

#ifndef _INCLUDE_STLIB_RATIO_
#include <ratio>
#define _INCLUDE_STLIB_RATIO_
#endif

namespace ung
{
	/*!
	 * \brief
	 * Timer。
	 * \class Timer
	 *
	 * \author Su Yang
	 *
	 * \date 2016/02/01
	 *
	 * \todo
	 */
	template<typename CLOCK = std::chrono::steady_clock,typename DURATION = std::chrono::duration<real_type,std::ratio<1,1000>>>
	class Timer
	{
	public:
		/*!
		 * \remarks 构造函数
		 * \return 
		*/
		Timer() :
			mStartTime(CLOCK::now())
		{
		}

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~Timer() = default;

		/*!
		 * \remarks 计时器重启
		 * \return void
		*/
		void restart()
		{
			mStartTime = CLOCK::now();
		}

		/*!
		 * \remarks 获取逝去的时间
		 * \return real_type
		*/
		real_type elapsed() const
		{
			/*
			count():Retrieves the number of clock ticks in the time interval.
			*/
			return DURATION(CLOCK::now() - mStartTime).count();
		}

	private:
		typename CLOCK::time_point mStartTime;
	};
	
	typedef Timer<std::chrono::steady_clock, std::chrono::duration<real_type, std::ratio<1,1000>>> SteadyClock_ms;
	typedef Timer<std::chrono::steady_clock, std::chrono::duration<real_type, std::ratio<1>>> SteadyClock_s;
	typedef Timer<std::chrono::steady_clock, std::chrono::duration<real_type, std::ratio<60>>> SteadyClock_m;
	typedef Timer<std::chrono::steady_clock, std::chrono::duration<real_type, std::ratio<3600>>> SteadyClock_h;
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_TIMER_H_
