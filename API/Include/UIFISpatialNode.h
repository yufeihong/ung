/*!
 * \brief
 * 空间管理器节点接口。
 * \file UIFISpatialNode.h
 *
 * \author Su Yang
 *
 * \date 2016/11/25
 */
#ifndef _UIF_ISPATIALNODE_H_
#define _UIF_ISPATIALNODE_H_

#ifndef _UIF_CONFIG_H_
#include "UIFConfig.h"
#endif

#ifdef UIF_HIERARCHICAL_COMPILE

namespace ung
{
	/*!
	 * \brief
	 * 空间管理器节点接口。
	 * \class ISpatialNode
	 *
	 * \author Su Yang
	 *
	 * \date 2016/11/25
	 *
	 * \todo
	 */
	class UngExport ISpatialNode : public MemoryPool<ISpatialNode>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		ISpatialNode() = default;

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~ISpatialNode() = default;

		/*!
		 * \remarks 获取父节点
		 * \return 
		*/
		virtual ISpatialNode* getParent() const = 0;

		/*!
		 * \remarks 获取当前节点的深度
		 * \return 
		*/
		virtual int32 getDepth() const = 0;

		/*!
		 * \remarks 关联一个对象到当前节点
		 * \return 
		 * \param StrongIObjectPtr objectPtr
		*/
		virtual void attachObject(StrongIObjectPtr objectPtr) = 0;

		/*!
		 * \remarks 将一个对象脱离当前节点
		 * \return 
		 * \param StrongIObjectPtr objectPtr
		*/
		virtual void detachObject(StrongIObjectPtr objectPtr) = 0;

		/*!
		 * \remarks 创建一个孩子节点
		 * \return 
		*/
		virtual ISpatialNode* createChild() = 0;

		/*!
		 * \remarks 创建多个孩子节点
		 * \return 
		*/
		virtual void createChildren() = 0;
	};
}//namespace ung

#endif//UIF_HIERARCHICAL_COMPILE

#endif//_UIF_ISPATIALNODE_H_