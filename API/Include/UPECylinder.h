/*!
 * \brief
 * 圆柱体
 * \file UPECylinder.h
 *
 * \author Su Yang
 *
 * \date 2017/04/11
 */
#ifndef _UPE_CYLINDER_H_
#define _UPE_CYLINDER_H_

#ifndef _UPE_CONFIG_H_
#include "UPEConfig.h"
#endif

#ifdef UPE_HIERARCHICAL_COMPILE

#ifndef _UIF_IBOUNDINGVOLUME_H_
#include "UIFIBoundingVolume.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 圆柱体
	 * \class Cylinder
	 *
	 * \author Su Yang
	 *
	 * \date 2017/04/11
	 *
	 * \todo
	 */
	class UngExport Cylinder : public IBoundingVolume
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		Cylinder() = delete;

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~Cylinder() = default;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param Vector3 const & topCenter
		 * \param Vector3 const & bottomCenter
		 * \param real_type radius
		*/
		Cylinder(Vector3 const& topCenter, Vector3 const& bottomCenter, real_type radius);

		/*!
		 * \remarks 获取顶面圆心
		 * \return 
		*/
		Vector3 const& getTopCenter() const;

		/*!
		 * \remarks 获取底面圆心
		 * \return 
		*/
		Vector3 const& getBottomCenter() const;

		/*!
		 * \remarks 获取半径
		 * \return 
		*/
		real_type getRadius() const;

	private:
		Vector3 mTopCenter;
		Vector3 mBottomCenter;
		real_type mRadius;
	};
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

#endif//_UPE_CYLINDER_H_