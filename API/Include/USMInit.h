/*!
 * \brief
 * 场景管理器的初始化。
 * \file USMInit.h
 *
 * \author Su Yang
 *
 * \date 2016/11/24
 */
#ifndef _USM_INIT_H_
#define _USM_INIT_H_

#ifndef _USM_CONFIG_H_
#include "USMConfig.h"
#endif

#ifdef USM_HIERARCHICAL_COMPILE

namespace ung
{
	UNG_C_LINK_BEGIN;

	UngExport void UNG_STDCALL usmInit();

	UNG_C_LINK_END;
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE

#endif//_USM_INIT_H_