/*!
 * \brief
 * 日志。
 * \file UFCLog.h
 *
 * \author Su Yang
 *
 * \date 2016/01/31
 */
#ifndef _UFC_LOG_H_
#define _UFC_LOG_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

namespace ung
{
	enum LOG_LEVEL_TYPE
	{
		LLT_NORMAL,
		LLT_NOTIFICATION,
		LLT_WARNING,
		LLT_ERROR,
		LLT_CRITICAL
	};

	/*!
	 * \brief
	 * 记录引擎事件，用户不直接创建Log以及调用Log接口，而是通过LogManager使用
	 * Release模式下，severity >= LLT_WARNING才会产生log文件
	 * 不要从MemoryPool继承，因为是在main之前构造的。
	 * \class LogImpl
	 *
	 * \author Su Yang
	 *
	 * \date 2016/01/31
	 *
	 * \todo
	 */
	class LogImpl
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		LogImpl();
		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~LogImpl();

		/*!
		 * \remarks 记录事件。用户不直接调用这个方法，这个方法会在LogManager中被调用。
		 * \return void
		 * \param String& mes 标准库string
		 * \param LOG_LEVEL_TYPE level 
		*/
		void logRecord(String& mes,LOG_LEVEL_TYPE level);
		/*!
		 * \remarks 同上。用户不直接调用这个方法，这个方法会在LogManager中被调用。
		 * \return void
		 * \param const char* mes C字符串
		 * \param LOG_LEVEL_TYPE level 
		*/
		void logRecord(const char* mes,LOG_LEVEL_TYPE level);
		/*!
		 * \remarks 记录异常事件。用户不直接调用这个方法，这个方法会在LogManager中被调用。
		 * \return void
		 * \param String& mes 标准库string
		 * \param LOG_LEVEL_TYPE level 
		*/
		void logException(String& mes,LOG_LEVEL_TYPE level);
		/*!
		 * \remarks 同上。用户不直接调用这个方法，这个方法会在LogManager中被调用。
		 * \return void
		 * \param const char* mes C字符串
		 * \param LOG_LEVEL_TYPE level 
		*/
		void logException(const char* mes,LOG_LEVEL_TYPE level);

	private:
		class Impl;
		std::shared_ptr<Impl> mImplPtr;
	};
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_LOG_H_