/*!
 * \brief
 * 资源管理器。多线程安全。
 * 客户代码包含这个头文件。
 * \file URM.h
 *
 * \author Su Yang
 *
 * \date 2016/11/13
 */
#ifndef _URM_H_
#define _URM_H_

#ifndef _URM_ENABLE_H_
#include "URMEnable.h"
#endif

#ifdef URM_HIERARCHICAL_COMPILE

#ifndef _URM_INIT_H_
#include "URMInit.h"
#endif

#ifndef _URM_RESOURCE_H_
#include "URMResource.h"
#endif

#ifndef _URM_RESOURCEFACTORY_H_
#include "URMResourceFactory.h"
#endif

#ifndef _URM_PACKRESOURCE_H_
#include "URMPackResource.h"
#endif

#ifndef _URM_RESOURCEPOOL_H_
#include "URMResourcePool.h"
#endif

#ifndef _URM_MATERIAL_H_
#include "URMMaterial.h"
#endif

#ifndef _URM_PIXELRECTANGLE_H_
#include "URMPixelRectangle.h"
#endif

#ifndef _URM_ENUMCOLLECTION_H_
#include "URMEnumCollection.h"
#endif

#endif//URM_HIERARCHICAL_COMPILE

#endif//_URM_H_