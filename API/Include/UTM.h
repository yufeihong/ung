/*!
 * \brief
 * 线程管理器。
 * 客户代码包含这个头文件。
 * \file UTM.h
 *
 * \author Su Yang
 *
 * \date 2016/04/16
 */
#ifndef _UTM_H_
#define _UTM_H_

#ifndef _UTM_ENABLE_H_
#include "UTMEnable.h"
#endif

#ifdef UTM_HIERARCHICAL_COMPILE

#ifndef _UTM_THREADPOOL_H_
#include "UTMThreadPool.h"
#endif

#ifndef _UTM_FUTUREPOOL_H_
#include "UTMFuturePool.h"
#endif

#endif//UTM_HIERARCHICAL_COMPILE

#endif//_UTM_H_