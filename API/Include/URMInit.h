/*!
 * \brief
 * 资源管理器库的初始化
 * \file URMInit.h
 *
 * \author Su Yang
 *
 * \date 2016/11/18
 */
#ifndef _URM_INIT_H_
#define _URM_INIT_H_

#ifndef _URM_CONFIG_H_
#include "URMConfig.h"
#endif

#ifdef URM_HIERARCHICAL_COMPILE

namespace ung
{
	UNG_C_LINK_BEGIN;

	UngExport void UNG_STDCALL urmInit();

	UNG_C_LINK_END;
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

#endif//_URM_INIT_H_