/*!
 * \brief
 * 用于配置内部代码。
 * \file UUTConfig.h
 *
 * \author Su Yang
 *
 * \date 2016/11/13
 */
#ifndef _UUT_CONFIG_H_
#define _UUT_CONFIG_H_

#ifndef _UUT_ENABLE_H_
#include "UUTEnable.h"
#endif

#ifdef UUT_HIERARCHICAL_COMPILE

//glew
#ifndef _INCLUDE_GL_GLEW_H_
#include "GL/glew.h"
#define _INCLUDE_GL_GLEW_H_
#endif

/*
glewExperimental = GL_TRUE;
当环境创建失败的时候(比如：版本号错误)，这里就会失败。
glew初始化失败的原因一般是在建立OpenGL context之前去初始化了。
*/
#define GLEW_INIT					\
GLenum err = glewInit();			\
if (GLEW_OK != err)					\
{												\
	exit(EXIT_FAILURE);				\
}

//freeglut
#ifndef _INCLUDE_GL_FREEGLUT_H_
#define FREEGLUT_STATIC
#include "GL/freeglut.h"
#define _INCLUDE_GL_FREEGLUT_H_
#endif

//glfw
/*
1,Do not include the OpenGL header yourself, as GLFW does this for you in a platform-independent way
2,Do not include windows.h or other platform-specific headers unless you plan on using those APIs yourself
3,If you do need to include such headers, include them before the GLFW header and it will detect this
*/
#ifndef _INCLUDE_GL_GLFW_H_
#include "glfw3.h"
#define _INCLUDE_GL_GLFW_H_
#endif

//windows gl
#ifndef _INCLUDE_GL_WGLEW_H_
#include "GL/wglew.h"
#define _INCLUDE_GL_WGLEW_H_
#endif

//其它库的头文件
#ifndef _UFC_H_
#include "UFC.h"
#endif

#ifndef _UML_H_
#include "UML.h"
#endif

//STL库的头文件
#ifndef _INCLUDE_STLIB_INITIALIZERLIST_
#include <initializer_list>
#define _INCLUDE_STLIB_INITIALIZERLIST_
#endif

#endif//UUT_HIERARCHICAL_COMPILE

#endif//_UUT_CONFIG_H_