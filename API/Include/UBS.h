/*!
 * \brief
 * 基本。
 * 客户代码包含这个头文件。
 * \file UBS.h
 *
 * \author Su Yang
 *
 * \date 2017/03/12
 */
#ifndef _UBS_H_
#define _UBS_H_

#ifndef _UBS_ENABLE_H_
#include "UBSEnable.h"
#endif

#ifdef UBS_HIERARCHICAL_COMPILE

/*
如果已包含了整个包含文件，那么将省去打开和解析该文件的无益成。
这种技术在Linux下几乎没有什么效果，可能是因为GNU编译器和Linux磁盘缓存的结合营造了一个更加高效的环境所至。
*/

//标准库的头文件
#ifndef _INCLUDE_STLIB_IOSTREAM_
#include <iostream>
#define _INCLUDE_STLIB_IOSTREAM_
#endif

#ifndef _INCLUDE_STLIB_ARRAY_
#include <array>
#define _INCLUDE_STLIB_ARRAY_
#endif

#ifndef _INCLUDE_STLIB_VECTOR_
#include <vector>
#define _INCLUDE_STLIB_VECTOR_
#endif

#ifndef _INCLUDE_STLIB_LIST_
#include <list>
#define _INCLUDE_STLIB_LIST_
#endif

#ifndef _INCLUDE_STLIB_FORWARDLIST_
#include <forward_list>
#define _INCLUDE_STLIB_FORWARDLIST_
#endif

#ifndef _INCLUDE_STLIB_QUEUE_
#include <queue>
#define _INCLUDE_STLIB_QUEUE_
#endif

#ifndef _INCLUDE_STLIB_DEQUE_
#include <deque>
#define _INCLUDE_STLIB_DEQUE_
#endif

#ifndef _INCLUDE_STLIB_SET_
#include <set>
#define _INCLUDE_STLIB_SET_
#endif

#ifndef _INCLUDE_STLIB_MAP_
#include <map>
#define _INCLUDE_STLIB_MAP_
#endif

#ifndef _INCLUDE_STLIB_UNORDEREDSET_
#include <unordered_set>
#define _INCLUDE_STLIB_UNORDEREDSET_
#endif

#ifndef _INCLUDE_STLIB_UNORDEREDMAP_
#include <unordered_map>
#define _INCLUDE_STLIB_UNORDEREDMAP_
#endif

#ifndef _INCLUDE_STLIB_STACK_
#include <stack>
#define _INCLUDE_STLIB_STACK_
#endif

#ifndef _INCLUDE_STLIB_TUPLE_
#include <tuple>
#define _INCLUDE_STLIB_TUPLE_
#endif

#ifndef _INCLUDE_STLIB_MEMORY_
#include <memory>
#define _INCLUDE_STLIB_MEMORY_
#endif

//boost库头文件
#ifndef _INCLUDE_BOOST_ASSERT_HPP_
#include "boost/assert.hpp"
#define _INCLUDE_BOOST_ASSERT_HPP_
#endif

#ifndef _INCLUDE_BOOST_STATICASSERT_HPP_
#include "boost/static_assert.hpp"
#define _INCLUDE_BOOST_STATICASSERT_HPP_
#endif

#ifndef _INCLUDE_BOOST_CONFIG_HPP_
#include "boost/config.hpp"
#define _INCLUDE_BOOST_CONFIG_HPP_
#endif

#ifndef _INCLUDE_BOOST_NONCOPYABLE_HPP_
#include "boost/noncopyable.hpp"
#define _INCLUDE_BOOST_NONCOPYABLE_HPP_
#endif

//UBS
#ifndef _UBS_MACRO_H_
#include "UBSMacro.h"
#endif

#ifndef _UBS_WARNING_H_
#include "UBSWarning.h"
#endif

#ifndef _UBS_INT_H_
#include "UBSInt.h"
#endif

#ifndef _UBS_FLOAT_H_
#include "UBSFloat.h"
#endif

#ifndef _UBS_STRING_H_
#include "UBSString.h"
#endif

#ifndef _UBS_INIT_H_
#include "UBSInit.h"
#endif

#ifndef _UBS_SINGLETON_H_
#include "UBSSingleton.h"
#endif

#ifndef _UBS_PLATFORM_H_
#include "UBSPlatform.h"
#endif

#ifndef _UBS_FORWARDTYPEDEFINE_H_
#include "UBSForwardTypeDefine.h"
#endif

#endif//UBS_HIERARCHICAL_COMPILE

#endif//_UBS_H_