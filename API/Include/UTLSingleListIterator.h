/*!
 * \brief
 * 单向链表的迭代器。
 * \file UTLSingleListIterator.h
 *
 * \author Su Yang
 *
 * \date 2016/02/26
 */
#ifndef _UNG_UTL_SINGLELIST_ITERATOR_H_
#define _UNG_UTL_SINGLELIST_ITERATOR_H_

#ifndef _UNG_UTL_CONFIG_H_
#include "UTLConfig.h"
#endif

namespace ung
{
	namespace utl
	{
		struct slist_node_base_;

		/*!
		 * \brief
		 * 单向链表的迭代器基本结构
		 * \class slist_iterator_base_
		 *
		 * \author Su Yang
		 *
		 * \date 2016/02/26
		 *
		 * \todo
		 */
		struct slist_iterator_base_
		{
			/*!
			 * \remarks 构造函数
			 * \return 
			 * \param slist_node_base_ * x
			*/
			slist_iterator_base_(slist_node_base_* x) :
				node(x)
			{
			}

			/*!
			 * \remarks 重载!=运算符
			 * \return 
			 * \param slist_iterator_base_ const & x
			*/
			bool operator!=(slist_iterator_base_ const& x) const
			{
				return node != x.node;
			}

			//指向节点基本结构
			slist_node_base_* node;
		};

		template<typename T>
		struct slist_node_;

		using SListIteratorTag = std::forward_iterator_tag;

		/*!
		 * \brief
		 * 单向链表迭代器。
		 * \class SListIterator
		 *
		 * \author Su Yang
		 *
		 * \date 2016/02/27
		 *
		 * \todo
		 */
		template<typename T,typename Ref,typename Ptr>
		struct SListIterator : public slist_iterator_base_,public boost::iterator_facade<SListIterator<T,Ref,Ptr>, T, SListIteratorTag>
		{
			friend class boost::iterator_core_access;

			using iterator = SListIterator<T,T&,T*>;
			using const_iterator = SListIterator<T, T const&, T const*>;
			
			using self_type = SListIterator<T, Ref, Ptr>;
			using value_type = T;
			using pointer = typename iterator_facade::pointer;
			using reference = typename iterator_facade::reference;
			using difference_type = typename iterator_facade::difference_type;
			using iterator_category = SListIteratorTag;

			using list_node = slist_node_<T>;

			/*!
			 * \remarks 默认构造函数
			 * \return 
			*/
			SListIterator() :
				slist_iterator_base_(0)
			{
			}

			/*!
			 * \remarks 构造函数
			 * \return 
			 * \param list_node * x
			*/
			SListIterator(list_node* x) :
				slist_iterator_base_(x)
			{
			}

			/*!
			 * \remarks 构造函数
			 * \return 
			 * \param iterator const & x
			*/
			SListIterator(iterator const& x) :
				slist_iterator_base_(x.node)
			{
			}

		private:
			reference dereference() const
			{
				return ((list_node*)node)->data;
			}

			void increment()
			{
				node = node->next;
			}

			bool equal(self_type const& right) const
			{
				return node == right.node;
			}

			//difference_type distance_to(self_type const& right) const;
		};
	}//namespace utl
}//namespace ung

#endif//_UNG_UTL_SINGLELIST_ITERATOR_H_