/*!
 * \brief
 * 双端队列迭代器。
 * \file UTLDequeIterator.h
 *
 * \author Su Yang
 *
 * \date 2016/02/21
 */
#ifndef _UNG_UTL_DEQUEITERATOR_H_
#define _UNG_UTL_DEQUEITERATOR_H_

#ifndef _UNG_UTL_CONFIG_H_
#include "UTLConfig.h"
#endif

namespace ung
{
	namespace utl
	{
		/*!
		 * \remarks 这里获取的是缓冲区中可以放置的元素的个数
		 * \return 
		 * \param usize BufSize
		 * \param usize elemSize
		*/
		static usize bufSize(usize BufSize,usize elemSize)
		{
			/*
			如果BufSize不为0，传回BufSize，表示buffer size由用户自定义
			如果BufSize为0，表示buffer size使用默认值，那么如果元素大小小于512字节，传回512/元素大小，如果不小于512字节，传回1
			*/
			return BufSize != 0 ? BufSize : (elemSize < 512 ? usize(512 / elemSize) : usize(1));
		}

		using DequeIteratorTag = std::random_access_iterator_tag;

		/*!
		 * \brief
		 * 双端队列迭代器。
		 * \class DequeIterator
		 *
		 * \author Su Yang
		 *
		 * \date 2016/02/22
		 *
		 * \todo
		 */
		template<typename T,usize BufSize>
		class DequeIterator : public boost::iterator_facade<DequeIterator<T, BufSize>, T, DequeIteratorTag>
		{
			friend class boost::iterator_core_access;

		public:
			using value_type = T;
			using self_type = DequeIterator<value_type, BufSize>;
			using base_type = boost::iterator_facade<self_type, value_type, DequeIteratorTag>;
			using iterator_category = DequeIteratorTag;

			using reference = value_type&;
			using pointer = value_type*;
			using map_pointer = pointer*;
			using difference_type = std::ptrdiff_t;

			/*!
			 * \remarks 默认构造函数
			 * \return 
			*/
			DequeIterator() :
				mCur(nullptr),
				mFirst(nullptr),
				mLast(nullptr),
				mNode(nullptr),
			{
			}

		private:
			/*!
			 * \remarks 设置迭代器和map节点的关系
			 * \return 
			 * \param map_pointer newNode
			*/
			void setNode(map_pointer newNode)
			{
				mNode = newNode;
				mFirst = *newNode;
				mLast = mFirst + difference_type(bufSize(BufSize,sizeof(value_type)));
			}

			reference dereference() const
			{
				return *mCur;
			}

			void increment()
			{
				++mCur;
				if (mCur == mLast)
				{
					setNode(mNode + 1);
					mCur = mFirst;
				}
			}

			bool equal(self_type const& other) const
			{
				return mCur == other.mCur;
			}

			void decrement()
			{
				if (mCur == mFirst)
				{
					setNode(mNode - 1);
					mCur = mLast;
				}
				--mCur;
			}

			difference_type distance_to(self_type const& other) const
			{
				return difference_type(bufSize(BufSize, sizeof(value_type))) * (other.mNode - mNode - 1) + (mLast - mCur) + (other.mCur - other.mFirst);
			}

			void advance(difference_type n)
			{
				if (n > 0)
				{
					if (mLast - mCur >= n)
					{
						mCur += n;
					}
					else
					{
						n -= mLast - mCur;
						usize nodeOffset = n / bufSize(BufSize, sizeof(value_type)) + 1;
						setNode(nodeOffset);
						n -= (nodeOffset - 1) * bufSize(BufSize, sizeof(value_type));
						mCur += n;
					}
				}
				else
				{
					n = -n;
					if (mCur - mFirst >= n)
					{
						mCur -= n;
					}
					else
					{
						n -= mCur - mFirst;
						usize nodeOffset = n / bufSize(BufSize, sizeof(value_type)) + 1;
						setNode(-nodeOffset);
						n -= (nodeOffset - 1) * bufSize(BufSize, sizeof(value_type));
						mCur -= n;
					}
				}
			}

		//private:
			pointer mCur;
			pointer mFirst;
			pointer mLast;				//指向缓冲区最后一个元素的下一个位置
			map_pointer mNode;
		};
	}//namespace utl
}//namespace ung

#endif//_UNG_UTL_DEQUEITERATOR_H_