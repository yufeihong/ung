/*!
 * \brief
 * 前置类型定义。
 * \file UIFForwardTypeDefine.h
 *
 * \author Su Yang
 *
 * \date 2017/03/07
 */
#ifndef _UIF_FORWARDTYPEDEFINE_H_
#define _UIF_FORWARDTYPEDEFINE_H_

#ifndef _UIF_CONFIG_H_
#include "UIFConfig.h"
#endif

#ifdef UIF_HIERARCHICAL_COMPILE

namespace ung
{
	class ISceneNode;
	using StrongISceneNodePtr = std::shared_ptr<ISceneNode>;
	using WeakISceneNodePtr = std::weak_ptr<ISceneNode>;

	class IObject;
	using StrongIObjectPtr = std::shared_ptr<IObject>;
	using WeakIObjectPtr = std::weak_ptr<IObject>;

	class IObjectComponent;
	using StrongIObjectComponentPtr = std::shared_ptr<IObjectComponent>;
	using WeakIObjectComponentPtr = std::weak_ptr<IObjectComponent>;

	using SceneTypeMask = uint8;

	using ComponentID = UniqueID;

	class ISceneManager;
	using StrongISceneManagerPtr = std::shared_ptr<ISceneManager>;
	using WeakISceneManagerPtr = std::weak_ptr<ISceneManager>;

	class ISceneQuery;
	using StrongISceneQueryPtr = std::shared_ptr<ISceneQuery>;
	using WeakISceneQueryPtr = std::weak_ptr<ISceneQuery>;

	class ISpatialManager;
	class ISpatialManagerFactory;
	class ISpatialNode;
	
	class ISceneQueryResults;

	class IRenderSystem;
	class IRenderTarget;
	class IWindow;
}//namespace ung

#endif//UIF_HIERARCHICAL_COMPILE

#endif//_UIF_FORWARDTYPEDEFINE_H_