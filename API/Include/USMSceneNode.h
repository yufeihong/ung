/*!
 * \brief
 * 场景图节点。用于表达逻辑信息。
 * \file USMSceneNode.h
 *
 * \author Su Yang
 *
 * \date 2016/11/25
 */
#ifndef _USM_SCENENODE_H_
#define _USM_SCENENODE_H_

#ifndef _USM_CONFIG_H_
#include "USMConfig.h"
#endif

#ifdef USM_HIERARCHICAL_COMPILE

#ifndef _UIF_ISCENENODE_H_
#include "UIFISceneNode.h"
#endif

#ifndef _USM_SCENEMANAGER_H_
#include "USMSceneManager.h"
#endif

#ifndef _INCLUDE_STLIB_FUNCTIONAL_
#include <functional>
#define _INCLUDE_STLIB_FUNCTIONAL_
#endif

namespace ung
{
	/*!
	 * \brief
	 * 场景图节点
	 * \class SceneNode
	 *
	 * \author Su Yang
	 *
	 * \date 2016/11/25
	 *
	 * \todo
	 */
	class UngExport SceneNode : public ISceneNode
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		SceneNode() = delete;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param WeakISceneNodePtr parentPtr
		 * \param String const& name 场景节点的名字(名字必须唯一)
		*/
		SceneNode(WeakISceneNodePtr parentPtr, String const& name);

	public:
		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~SceneNode();

		/*!
		 * \remarks 获取当前节点的名字
		 * \return 
		*/
		virtual String const& getName() const override;

		/*!
		 * \remarks 获取当前节点的父节点。
		 * \return 
		*/
		virtual WeakISceneNodePtr getParent() const override;

		/*!
		 * \remarks 渲染前行为
		 * \return 
		*/
		virtual void preRender() override;

		/*!
		 * \remarks 渲染
		 * \return 
		*/
		virtual void render() override;

		/*!
		 * \remarks 渲染后行为
		 * \return 
		*/
		virtual void postRender() override;

		//------------------------------------------------------------------------------------------

		/*!
		 * \remarks 获取所有的（直接）孩子数量
		 * \return 
		*/
		virtual uint32 getChildrenCount() override;

		/*!
		 * \remarks 获取所有的（直接和非直接）孩子数量
		 * \return 
		*/
		virtual uint32 getFamilyCount() override;

		/*!
		 * \remarks 从父节点脱离
		 * \return 
		*/
		virtual void detachFromParent() override;

		/*!
		 * \remarks 关联到父节点
		 * \return 
		 * \param StrongISceneNodePtr parentPtr
		*/
		virtual void attachToParent(StrongISceneNodePtr parentPtr) override;

		/*
		用法：
		class PrintName
		{
		public:
			void operator()(StrongSceneNodePtr p) const
			{
				std::cout << p->getName() << ",";
			}
		};
		root->preTraverseDepthFirst<PrintName>();
		*/
		/*!
		 * \remarks 深度优先遍历(前序:先父后子)
		 * \return 
		*/
		template<typename F>
		void preTraverseDepthFirst()
		{
			auto beg = mChildren.begin();
			auto end = mChildren.end();

			while (beg != end)
			{
				F()(*beg);

				(*beg)->preTraverseDepthFirst<F>();

				++beg;
			}
		}

		/*!
		 * \remarks 深度优先遍历(后序:先子后父)
		 * \return 
		*/
		template<typename F>
		void postTraverseDepthFirst()
		{
			auto beg = mChildren.begin();
			auto end = mChildren.end();

			while (beg != end)
			{
				(*beg)->postTraverseDepthFirst<F>();

				F()(*beg);

				++beg;
			}
		}

		/*
		用法：
		std::function<void(StrongSceneNodePtr)> fn = [](StrongSceneNodePtr p)
		{
			std::cout << p->getName() << "," << std::endl;
		};
		或
		auto fn = [](StrongSceneNodePtr p)
		{
			std::cout << p->getName() << "," << std::endl;
		};
		root->preTraverseDepthFirst(fn);
		*/
		/*!
		 * \remarks 深度优先遍历(前序:先父后子)
		 * \return 
		 * \param std::function<void(StrongSceneNodePtr)> fn
		*/
		void preTraverseDepthFirst(std::function<void(StrongSceneNodePtr)> fn);

		/*!
		 * \remarks 深度优先遍历(后序:先子后父)
		 * \return 
		 * \param std::function<void(StrongSceneNodePtr)> fn
		*/
		void postTraverseDepthFirst(std::function<void(StrongSceneNodePtr)> fn);

		//------------------------------------------------------------------------------------------

		/*!
		 * \remarks 关联对象
		 * \return 
		 * \param StrongIObjectPtr objectPtr
		*/
		virtual void attachObject(StrongIObjectPtr objectPtr) override;

		/*!
		 * \remarks 分离对象
		 * \return 
		 * \param StrongIObjectPtr objectPtr
		 * \param bool remove true:从容器中移除对象
		*/
		virtual void detachObject(StrongIObjectPtr objectPtr,bool remove = true) override;

		/*!
		 * \remarks 获取(直接)关联在该节点下的对象数量
		 * \return 
		*/
		virtual uint32 getAttachedObjectCount() override;

		/*!
		 * \remarks 获取(直接和非直接)关联在该节点下的对象数量
		 * \return 
		*/
		virtual uint32 getAttachedAllObjectCount() override;

		//------------------------------------------------------------------------------------------

		/*!
		 * \remarks 必须update后才可正常使用
		 * \return 
		*/
		virtual void update() override;

		/*!
		 * \remarks 获取可运动的属性对象
		 * \return 
		*/
		virtual Movable& getMovableAttribute() override;

		/*!
		 * \remarks 获取可运动的属性对象
		 * \return 
		*/
		virtual Movable const& getMovableAttribute() const override;

		/*!
		 * \remarks 存储子节点
		 * \return 
		 * \param StrongISceneNodePtr childPtr
		*/
		virtual void saveChild(StrongISceneNodePtr childPtr) override;

		/*!
		 * \remarks 移除子节点
		 * \return 
		 * \param StrongISceneNodePtr childPtr
		*/
		virtual void removeChild(StrongISceneNodePtr childPtr) override;

		/*!
		 * \remarks 设置父节点
		 * \return 
		 * \param WeakISceneNodePtr parentPtr
		*/
		virtual void setParent(WeakISceneNodePtr parentPtr) override;

		/*!
		 * \remarks 该场景节点是否为根节点
		 * \return 
		*/
		virtual bool isRoot() const override;

		/*!
		 * \remarks 获取节点所关联的直接对象的数量
		 * \return 
		*/
		virtual uint32 getObjectsSize() const override;

		/*!
		 * \remarks 通知所有的子孙节点需要更新
		 * \return 
		*/
		virtual void notifyDescendant() override;

		/*!
		 * \remarks 所关联的直接对象需要更新
		 * \return 
		*/
		virtual void notifyObjects() override;

		/*!
		 * \remarks 重新计算该节点的world
		 * \return 
		*/
		virtual void buildWorld() override;

	//private:
		using objects_aggregates_type = STL_UNORDERED_MAP(String,StrongIObjectPtr);
		ObjectPool<objects_aggregates_type> mObjects;

		//该场景节点的所有子节点(使用list是为了有序，这样便于遍历时能够按照一定的严格顺序)
		STL_LIST(StrongISceneNodePtr) mChildren;

		WeakISceneNodePtr mParent;

		String mName;

		Movable mMovableAttribute;
	};
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE

#endif//_USM_SCENENODE_H_

/*
SceneNode的强引用，仅由其父节点持有。SceneManager中的节点池持有的是弱引用。

创建节点的两种方式：
1：在场景管理器中进行创建。
2：创建当前节点的子节点。
*/