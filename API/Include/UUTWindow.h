/*!
 * \brief
 * 对创建窗口的一个封装。应用泛型抽象工厂模式。
 * \file UUTWindow.h
 *
 * \author Su Yang
 *
 * \date 2016/11/13
 */
#ifndef _UUT_WINDOW_H_
#define _UUT_WINDOW_H_

#ifndef _UUT_CONFIG_H_
#include "UUTConfig.h"
#endif

#ifdef UUT_HIERARCHICAL_COMPILE

namespace ung
{
	//hooks类型
	typedef void(*changeSizeType)(int, int);
	typedef void(*renderType)();
	typedef void(*destroyWindowType)();

	/*!
	 * \brief
	 * 窗口包装器基类
	 * \class WindowWrapper
	 *
	 * \author Su Yang
	 *
	 * \date 2016/11/16
	 *
	 * \todo
	 */
	class UngExport WindowWrapper
	{
	public:
		virtual ~WindowWrapper();

		/*!
		 * \remarks 使用freeglut来创建一个窗口
		 * \return 
		 * \param int argc
		 * \param char * * argv
		 * \param int width
		 * \param int height
		 * \param const char * title
		 * \param int major
		 * \param int minor
		*/
		virtual void createWindow(int argc, char** argv, int width, int height, const char* title, int major, int minor);

		/*!
		 * \remarks 设置freeglut的三个回掉函数：改变窗口大小，渲染，销毁窗口
		 * \return
		 * \param changeSizeType changeSize
		 * \param renderType render
		 * \param destroyWindowType destroyWindow
		*/
		virtual void setCallback(changeSizeType changeSize, renderType render, destroyWindowType destroyWindow);

		/*!
		 * \remarks freeglut进入循环
		 * \return
		*/
		virtual void enterLoop();

		/*!
		 * \remarks 使用glfw来创建一个窗口
		 * \return
		 * \param GLFWwindow * & window
		 * \param int width
		 * \param int height
		 * \param const char * title
		 * \param int major
		 * \param int minor
		*/
		virtual void createWindow(GLFWwindow*& window, int width, int height, const char* title, int major, int minor);
	};

	/*!
	 * \brief
	 * freeglut族系。freeglut的退出应用程序不经过main()函数的return 0;有些内存会释放不了。
	 * \class FreeglutWrapper
	 *
	 * \author Su Yang
	 *
	 * \date 2016/11/16
	 *
	 * \todo
	 */
	class UngExport FreeglutWrapper : public WindowWrapper
	{
	public:
		/*!
		 * \remarks 创建窗口
		 * \return 
		 * \param int argc
		 * \param char * * argv
		 * \param int width
		 * \param int height
		 * \param const char * title
		 * \param int major
		 * \param int minor
		*/
		virtual void createWindow(int argc, char** argv, int width, int height, const char* title, int major, int minor) override;

		/*!
		 * \remarks 设置回调函数
		 * \return 
		 * \param changeSizeType changeSize
		 * \param renderType render
		 * \param destroyWindowType destroyWindow
		*/
		virtual void setCallback(changeSizeType changeSize, renderType render, destroyWindowType destroyWindow) override;

		/*!
		 * \remarks 进入循环
		 * \return 
		*/
		virtual void enterLoop() override;
	};

	/*!
	 * \brief
	 * glfw族系
	 * \class GlfwWrapper
	 *
	 * \author Su Yang
	 *
	 * \date 2016/11/16
	 *
	 * \todo
	 */
	class UngExport GlfwWrapper : public WindowWrapper
	{
	public:
		/*!
		 * \remarks 创建窗口
		 * \return 
		 * \param GLFWwindow * & window
		 * \param int width
		 * \param int height
		 * \param const char * title
		 * \param int major
		 * \param int minor
		*/
		virtual void createWindow(GLFWwindow*& window, int width, int height, const char* title, int major, int minor) override;
	};

	//----------------------------------------------------------------------

	//抽象工厂
	typedef AbstractFactory<TYPELIST_1(WindowWrapper)> WindowWrapperFactory;

	//具象工厂
	typedef ConcreteFactory<WindowWrapperFactory, AbstractFactoryUnit,
		TYPELIST_1(FreeglutWrapper)> FreeglutFactory;

	//具象工厂
	typedef ConcreteFactory<WindowWrapperFactory, AbstractFactoryUnit,
		TYPELIST_1(GlfwWrapper)> GlfwFactory;
}//namespace ung

#endif//UUT_HIERARCHICAL_COMPILE

#endif//_UUT_WINDOW_H_