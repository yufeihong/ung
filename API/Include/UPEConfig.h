/*!
 * \brief
 * 配置文件
 * \file UPEConfig.h
 *
 * \author Su Yang
 *
 * \date 2016/06/17
 */
#ifndef _UPE_CONFIG_H_
#define _UPE_CONFIG_H_

#ifndef _UPE_ENABLE_H_
#include "UPEEnable.h"
#endif

#ifdef UPE_HIERARCHICAL_COMPILE

//其它库的头文件
#ifndef _UMM_H_
#include "UMM.h"
#endif

#ifndef _UFC_H_
#include "UFC.h"
#endif

#ifndef _UML_H_
#include "UML.h"
#endif

#ifndef _UUT_H_
#include "UUT.h"
#endif

#if UNG_USE_DOUBLE
#define BT_USE_DOUBLE_PRECISION								//bullet库使用double
#endif

#ifndef _INCLUDE_BULLET_BTSCALAR_H_
#include "LinearMath/btScalar.h"
#define _INCLUDE_BULLET_BTSCALAR_H_
#endif

#ifndef _INCLUDE_BULLET_VECTOR3_H_
#include "LinearMath/btVector3.h"
#define _INCLUDE_BULLET_VECTOR3_H_
#endif

#ifndef _INCLUDE_BULLET_COLLISIONCOMMON_H_
#include "btBulletCollisionCommon.h"
#define _INCLUDE_BULLET_COLLISIONCOMMON_H_
#endif

#ifndef _INCLUDE_BULLET_BTRIGIDBODY_H_
#include "BulletDynamics/Dynamics/btRigidBody.h"
#define _INCLUDE_BULLET_BTRIGIDBODY_H_
#endif

#ifndef _INCLUDE_BULLET_IDEBUGDRAWER_H_
#include "LinearMath/btIDebugDraw.h"
#define _INCLUDE_BULLET_IDEBUGDRAWER_H_
#endif

#ifndef _UIF_FORWARDTYPEDEFINE_H_
#include "UIFForwardTypeDefine.h"
#endif

#endif//UPE_HIERARCHICAL_COMPILE

#endif//_UPE_CONFIG_H_