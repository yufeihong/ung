/*!
 * \brief
 * 日志管理器。多线程安全。
 * \file UFCLogManager.h
 *
 * \author Su Yang
 *
 * \date 2016/01/31
 */
#ifndef _UFC_LOGMANAGER_H_
#define _UFC_LOGMANAGER_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#ifndef _UFC_LOG_H_
#include "UFCLog.h"
#endif

#ifndef _UFC_STRINGUTILITIES_H_
#include "UFCStringUtilities.h"						//宏需要这个头文件
#endif

namespace ung
{
	/*!
	 * \brief
	 * 日志管理器。多线程安全。
	 * \class LogManager
	 *
	 * \author Su Yang
	 *
	 * \date 2016/01/31
	 *
	 * \todo
	 */
	class UngExport LogManager
	{
	private:
		struct Helper
		{
			/*
			这个构造函数什么也不做，只是确保getInstance()在main()之前被调用。
			*/
			Helper()
			{
				LogManager::getInstance();
			}

			inline void doNothing() const
			{
			}
		};

		static Helper mHelper;

	private:
		/*!
		 * \remarks 默认构造函数，在main开始之前被调用
		 * \return 
		*/
		LogManager();

	public:
		/*!
		 * \remarks 析构函数，在main()中的return 0;之后被调用
		 * \return 
		*/
		~LogManager();

	public:
		static LogManager& getInstance()
		{
			/*
			obj在getInstance()第一次被调用时进行了构造。
			*/
			static LogManager obj;

			/*
			mHelper被隐式实例化。
			因为mHelper是一个静态成员，所以其构造函数在main()之前被调用。
			其构造函数中包含了一个对getInstance()的调用，所以这就确保了getInstance()在main()之前被调用。
			下面这行代码什么也不做，只是强制mHelper对象的构造函数在main()之前被调用。
			*/
			mHelper.doNothing();

			return obj;
		}

		/*!
		 * \remarks 记录事件。用户不直接调用这个方法，用户使用宏。
		 * \return void
		 * \param String& mes 标准库string
		 * \param LOG_LEVEL_TYPE level 默认参数为LLT_NORMAL
		*/
		void logRecord(String& mes,LOG_LEVEL_TYPE level = LLT_NORMAL);

		/*!
		 * \remarks 同上。用户不直接调用这个方法，用户使用宏。
		 * \return void
		 * \param const char* mes C字符串
		 * \param LOG_LEVEL_TYPE level 默认参数为LLT_NORMAL
		*/
		void logRecord(const char* mes,LOG_LEVEL_TYPE level = LLT_NORMAL);

		/*!
		 * \remarks 记录异常。用户不直接调用这个方法，用户使用宏。
		 * \return void
		 * \param String& mes 标准库string
		 * \param LOG_LEVEL_TYPE level 默认参数为LLT_CRITICAL
		*/
		void logException(String& mes,LOG_LEVEL_TYPE level = LLT_CRITICAL);

		/*!
		 * \remarks 同上。用户不直接调用这个方法，用户使用宏。
		 * \return void
		 * \param const char* mes C字符串
		 * \param LOG_LEVEL_TYPE level 默认参数为LLT_CRITICAL
		*/
		void logException(const char* mes,LOG_LEVEL_TYPE level = LLT_CRITICAL);

	private:
		LogImpl mLog;
	};
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_LOGMANAGER_H_