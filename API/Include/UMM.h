/*!
 * \brief
 * 内存管理器。支持STL中的容器。
 * 客户代码包含这个头文件。
 * \file UMM.h
 *
 * \author Su Yang
 *
 * \date 2016/06/16
 */
#ifndef _UMM_H_
#define _UMM_H_

#ifndef _UMM_ENABLE_H_
#include "UMMEnable.h"
#endif

#ifdef UMM_HIERARCHICAL_COMPILE

#ifndef _UMM_POD_H_
#include "UMMPOD.h"
#endif

#ifndef _UMM_OBJECT_H_
#include "UMMObject.h"
#endif

#ifndef _UMM_ALLOCATOR_H_
#include "UMMAllocator.h"
#endif

#ifndef _UMM_MACRO_H_
#include "UMMMacro.h"
#endif

#ifndef _UMM_POOL_H_
#include "UMMPool.h"
#endif

#ifndef _UMM_BIGDOG_H_
#include "UMMBigDog.h"
#endif

#ifndef _INCLUDE_BOOST_LEXICALCAST_HPP_
#include "boost/lexical_cast.hpp"
#define _INCLUDE_BOOST_LEXICALCAST_HPP_
#endif

#endif//UMM_HIERARCHICAL_COMPILE

#endif//_UMM_H_