/*!
 * \brief
 * 颜色值
 * \file UFCColorValue.h
 *
 * \author Su Yang
 *
 * \date 2017/05/17
 */
#ifndef _UFC_COLORVALUE_H_
#define _UFC_COLORVALUE_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

namespace ung
{
	using RGBA = uint32;
	using ARGB = uint32;
	using ABGR = uint32;
	using BGRA = uint32;

	/*!
	 * \brief
	 * 颜色值
	 * \class ColorValue
	 *
	 * \author Su Yang
	 *
	 * \date 2017/05/17
	 *
	 * \todo
	 */
	template<typename T>
	class ColorValue : public MemoryPool<ColorValue<T>>
	{
		/*!
		 * \remarks T* ColorValue
		 * \return 
		 * \param const T s
		 * \param const ColorValue& right
		*/
		friend ColorValue operator*(const T s,const ColorValue& right)
		{
			ColorValue prod;

			prod.r = s * right.r;
			prod.g = s * right.g;
			prod.b = s * right.b;
			prod.a = s * right.a;

			return prod;
		}

	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		 * \param T rr
		 * \param T gg
		 * \param T bb
		 * \param T aa
		*/
		explicit ColorValue(T rr = 1.0, T gg = 1.0, T bb = 1.0, T aa = 1.0);

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param RGBA color
		*/
		ColorValue(RGBA color);

		/*!
		 * \remarks set color
		 * \return 
		 * \param const RGBA val
		*/
		void setRGBA(const RGBA val);

        /*!
         * \remarks set color
         * \return 
         * \param const ARGB val
        */
        void setARGB(const ARGB val);

        /*!
         * \remarks set color
         * \return 
         * \param const BGRA val
        */
        void setBGRA(const BGRA val);

        /*!
         * \remarks set color
         * \return 
         * \param const ABGR val
        */
        void setABGR(const ABGR val);

		/*!
		 * \remarks Retrieves color
		 * \return 
		*/
		RGBA getRGBA() const;

		/*!
		 * \remarks Retrieves color
		 * \return 
		*/
		ARGB getARGB() const;

        /*!
         * \remarks Retrieves color
         * \return 
        */
        BGRA getBGRA() const;

        /*!
         * \remarks Retrieves color
         * \return 
        */
        ABGR getABGR() const;

		/*!
		 * \remarks Clamps color value to the range [0.0,1.0]
		 * \return 
		*/
		void clamp();

		/*!
		 * \remarks Clamps color value to the range [0.0,1.0]
		 * \return 拷贝
		*/
		ColorValue clampCopy() const;

		/*!
		 * \remarks Array accessor operator
		 * \return 副本
		 * \param const size_t i
		*/
		T operator[](const size_t i) const;

		/*!
		 * \remarks Array accessor operator
		 * \return 引用
		 * \param const size_t i
		*/
		T& operator[](const size_t i);

		/*!
		 * \remarks Pointer accessor for direct copying
		 * \return 
		*/
		T* ptr();

		/*!
		 * \remarks Pointer accessor for direct copying
		 * \return 
		*/
		const T* ptr() const;

		/*!
		 * \remarks ColorValue + ColorValue
		 * \return 
		 * \param const ColorValue & right
		*/
		ColorValue operator+(const ColorValue& right) const;

		/*!
		 * \remarks ColorValue - ColorValue
		 * \return 
		 * \param const ColorValue & right
		*/
		ColorValue operator-(const ColorValue& right) const;

		/*!
		 * \remarks ColorValue * T
		 * \return 
		 * \param const T s
		*/
		ColorValue operator*(const T s) const;

		/*!
		 * \remarks ColorValue * ColorValue
		 * \return 
		 * \param const ColorValue & right
		*/
		ColorValue operator*(const ColorValue& right) const;

		/*!
		 * \remarks ColorValue / ColorValue
		 * \return 
		 * \param const ColorValue & right
		*/
		ColorValue operator/(const ColorValue& right) const;

		/*!
		 * \remarks ColorValue / T
		 * \return 
		 * \param const T s
		*/
		ColorValue operator/(const T s) const;

		/*!
		 * \remarks ColorValue += ColorValue
		 * \return 
		 * \param const ColorValue & right
		*/
		ColorValue& operator+=(const ColorValue& right);

		/*!
		 * \remarks ColorValue -= ColorValue
		 * \return 
		 * \param const ColorValue & right
		*/
		ColorValue& operator-=(const ColorValue& right);

		/*!
		 * \remarks ColorValue *= T
		 * \return 
		 * \param const T s
		*/
		ColorValue& operator*=(const T s);

		/*!
		 * \remarks ColorValue /= T
		 * \return 
		 * \param const T s
		*/
		ColorValue& operator/=(const T s);

		/*!
		 * \remarks ColorValue == ColorValue
		 * \return 
		 * \param const ColorValue & right
		*/
		bool operator==(const ColorValue& right) const;

		/*!
		 * \remarks ColorValue != ColorValue
		 * \return 
		 * \param const ColorValue & right
		*/
		bool operator!=(const ColorValue& right) const;

	public:
		T r, g, b, a;
	};

	//显式实例化的声明
	extern template class ColorValue<float>;
	extern template class ColorValue<double>;

	//与显式实例化没有关系，仅类型别名
	using color128 = ColorValue<float>;
	using color256 = ColorValue<double>;

#if UNG_USE_256_BITS_COLOR
	using real_color = color256;
#else
	using real_color = color128;
#endif

#define UNG_COLOR_ZERO			real_color(0.0, 0.0, 0.0, 0.0)
#define UNG_COLOR_BLACK			real_color(0.0, 0.0, 0.0)
#define UNG_COLOR_WHITE			real_color(1.0, 1.0, 1.0)
#define UNG_COLOR_RED				real_color(1.0, 0.0, 0.0)
#define UNG_COLOR_GREEN			real_color(0.0, 1.0, 0.0)
#define UNG_COLOR_BLUE				real_color(0.0, 0.0, 1.0)

	//宏参数为：[0,255]，uint32 -> uint32
#define UNG_ARGB(a,r,g,b)		((uint32)((((a)&0xff)<<24)|(((r)&0xff)<<16)|(((g)&0xff)<<8)|((b)&0xff)))
#define UNG_RGBA(r,g,b,a)		UNG_ARGB(a,r,g,b)
#define UNG_XRGB(r,g,b)			UNG_ARGB(0xff,r,g,b)

#define UNG_AYUV(a,y,u,v)		UNG_ARGB(a,y,u,v)
#define UNG_XYUV(y,u,v)			UNG_ARGB(0xff,y,u,v)

	//宏参数为：[0.0,1.0]，128/256 -> uint32
#define UNG_COLORVALUE_TO_RGBA(r,g,b,a)			UNG_RGBA((uint32)((r)*255.0),(uint32)((g)*255.0),(uint32)((b)*255.0),(uint32)((a)*255.0))

	//宏参数为：[0,255]，uint32 -> 128/256
#define UNG_RGBA_TO_COLORVALUE(r,g,b,a)			real_color(((r & 0xff) / 255.0),((g & 0xff) / 255.0),((b & 0xff) / 255.0),((a & 0xff) / 255.0))
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_COLORVALUE_H_