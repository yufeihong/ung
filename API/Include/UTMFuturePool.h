#ifndef _UTM_FUTUREPOOL_H_
#define _UTM_FUTUREPOOL_H_

#ifndef _UTM_CONFIG_H_
#include "UTMConfig.h"
#endif

#ifdef UTM_HIERARCHICAL_COMPILE

#ifndef _INCLUDE_BOOST_ANY_HPP_
#include "boost/any.hpp"
#define _INCLUDE_BOOST_ANY_HPP_
#endif

#ifndef _INCLUDE_STLIB_FUTURE_
#include <future>
#define _INCLUDE_STLIB_FUTURE_
#endif

namespace ung
{
	class FutureWrapperBase
	{
	public:
		FutureWrapperBase() = default;

		virtual ~FutureWrapperBase()
		{
		}
	};

	template<typename T>
	class FutureWrapper : public FutureWrapperBase
	{
	public:
		using result_type = T;

		FutureWrapper(std::future<T>& fut)
		{
			std::shared_ptr<std::future<T>> sp(new std::future<T>(std::move(fut)));
			mAny = std::move(sp);
		}

		boost::any mAny;
	};//class FutureWrapper

	class FuturePool : boost::noncopyable
	{
	public:
		UngExport static FuturePool& getInstance()
		{
			std::call_once(mOnceFlag, init);

			return init();
		}

		template<typename T>
		void put(std::string const& objectName,T&& t)
		{
			std::shared_ptr<T> p(&t);

			mPool[objectName] = p;
		}

		void get(std::string const& objectName)
		{
			boost::any p = mPool[objectName];
		}

	private:
		FuturePool() = default;

		virtual ~FuturePool()
		{
		}

		static FuturePool& init()
		{
			static FuturePool obj;

			return obj;
		}

		static std::once_flag mOnceFlag;
		//std::string:objectName(比如:模型文件名,纹理文件名等)
		std::unordered_map<std::string, std::shared_ptr<FutureWrapperBase>> mPool;
	};//class FuturePool
}//namespace ung

#endif//UTM_HIERARCHICAL_COMPILE

#endif//_UTM_FUTUREPOOL_H_