/*!
 * \brief
 * 检测内存泄露
 * \file UMMBigDog.h
 *
 * \author Su Yang
 *
 * \date 2017/03/10
 */
#ifndef _UMM_BIGDOG_H_
#define _UMM_BIGDOG_H_

#ifndef _UMM_CONFIG_H_
#include "UMMConfig.h"
#endif

#ifdef UMM_HIERARCHICAL_COMPILE

#ifndef _UMM_MACRO_H_
#include "UMMMacro.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 内存检测
	 * \class BigDog
	 *
	 * \author Su Yang
	 *
	 * \date 2017/03/10
	 *
	 * \todo
	 */
	class UngExport BigDog : public Singleton<BigDog>
	{
	public:
		using size_type = size_t;

		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		BigDog();

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		virtual ~BigDog();

		/*!
		 * \remarks 在new时调用
		 * \return 
		 * \param void * address
		 * \param size_type size
		 * \param std::string const & info
		*/
		static void doNew(void* address,size_type size,std::string const& info);

		/*!
		 * \remarks 在delete时调用
		 * \return 
		 * \param void * address
		*/
		static void doDelete(void* address);

		//-------------------------------------------------------------------------------------

	public:
		/*!
		 * \remarks 增加第一级配置器总共从内存条中成功获取空间的总次数
		 * \return 
		*/
		static void addLevelFirstMallocTimes();

		/*!
		 * \remarks 增加第一级配置器总共从内存条中成功获取的空间的总量
		 * \return 
		 * \param size_type size
		 * \param bool flag 标记获取和释放
		*/
		static void addLevelFirstHeapSize(size_type size,bool flag);

		/*!
		 * \remarks 增加第二级配置器总共从内存条中成功获取大块空间来补充给内存池的总次数
		 * \return 
		*/
		static void addLevelSecondMallocTimes();

		/*!
		 * \remarks 增加第二级配置器总共从内存条成功获取的所有空间补充给内存池来使用的总量
		 * \return 
		 * \param size_type size
		*/
		static void addLevelSecondHeapSize(size_type size);

		/*!
		 * \remarks 每当占用了某个索引的空闲列表的一个小格子，就递增一次
		 * \return 
		 * \param size_type freelistIndex
		*/
		static void addFreeListsTouched(size_type freelistIndex);

		/*!
		 * \remarks 记录增加了多少个小格子到某个空闲列表
		 * \return 
		 * \param size_type freelistIndex
		 * \param size_type num
		*/
		static void addFreeListsCellNum(size_type freelistIndex,size_type num);

		/*!
		 * \remarks 记录某个空闲列表中的小格子被占用了
		 * \return 
		 * \param size_type freelistIndex
		*/
		static void substractFreeListsCellNum(size_type freelistIndex);

		/*!
		 * \remarks 设置内存池剩余的大小
		 * \return 
		 * \param size_type size
		*/
		static void setPoolRemaining(size_type size);

		/*!
		 * \remarks 打印整个内存池的使用明细
		 * \return 
		*/
		static void reportWholePool();

	private:
		/*
		如果代码连续运行好几天，下面这些变量有可能会爆表。
		*/

		//下面的这四个静态数据成员，其记录的是整个内存池的使用情况，而不是某个类对内存池的使用情况
		//用于记录第一级配置器：总共从内存条中成功获取空间的总次数
		static size_type sLevelFirstMallocTimes;
		//用于记录第一级配置器：总共从内存条中成功获取的空间的总量
		static size_type sLevelFirstHeapSize;

		//用于记录第二级配置器：总共从内存条中成功获取大块空间来补充给内存池的总次数
		static size_type sLevelSecondMallocTimes;
		//用于记录第二级配置器：总共从内存条中成功获取的所有空间补充给内存池来使用的总量
		static size_type sLevelSecondHeapSize;

		//用于记录Free Lists的分配次数明细(只记录曾经被占用过的小格子，而当对象从小格子中释放时，这里的记录并不减少)
		static size_type sRecordTimesArray2[UMM_FREELISTS_COUNT];

		//用于记录每个Free List中未被占用的小格子数量
		static size_type sRecordCellNum[UMM_FREELISTS_COUNT];

		//用于记录内存池的剩余大小
		static size_type sPoolRemaining;

	private:
		/*
		void*:new分配的内存地址
		std::string,调用new时的文件名，函数名和行数信息
		*/
		using statistics_type = std::unordered_map<void*, std::string>;
		static statistics_type mStatistics;
	};
}//namespace ung

#endif//UMM_HIERARCHICAL_COMPILE

#endif//_UMM_BIGDOG_H_