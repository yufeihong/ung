#ifndef _UFC_PERFORMANCE_H_
#define _UFC_PERFORMANCE_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#ifndef _UFC_TIMER_H_
#include "UFCTimer.h"
#endif

namespace ung
{
	template<typename F>
	class Performance : public Singleton<Performance<F>>
	{
	public:
		real_type functionCostTime(F&& func)
		{
			mTimer.restart();

			func();

			return mTimer.elapsed();
		}

	private:
		SteadyClock_ms mTimer;
	};
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_PERFORMANCE_H_