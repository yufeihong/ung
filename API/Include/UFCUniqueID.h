/*!
 * \brief
 * 独一无二的ID
 * \file UFCUniqueID.h
 *
 * \author Su Yang
 *
 * \date 2016/04/15
 */
#ifndef _UFC_UNIQUEID_H_
#define _UFC_UNIQUEID_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#ifndef _INCLUDE_BOOST_UUID_HPP_
#include "boost/uuid/uuid.hpp"
#define _INCLUDE_BOOST_UUID_HPP_
#endif

#ifndef _INCLUDE_BOOST_UUIDGENERATORS_HPP_
#include "boost/uuid/uuid_generators.hpp"
#define _INCLUDE_BOOST_UUIDGENERATORS_HPP_
#endif

#ifndef _INCLUDE_BOOST_UUIDIO_HPP_
#include "boost/uuid/uuid_io.hpp"
#define _INCLUDE_BOOST_UUIDIO_HPP_
#endif

#ifndef _INCLUDE_BOOST_HASH_HPP_
#include "boost/functional/hash.hpp"
#define _INCLUDE_BOOST_HASH_HPP_
#endif

namespace ung
{
	/*!
	 * \brief
	 * 独一无二的ID
	 * \class UniqueID
	 *
	 * \author Su Yang
	 *
	 * \date 2016/04/15
	 *
	 * \todo
	 */
	class UngExport UniqueID : public MemoryPool<UniqueID>
	{
	public:
		/*!
		 * \remarks 构造函数
		 * \return 
		*/
		UniqueID() :
			mID(boost::uuids::random_generator()())
		{
		}

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~UniqueID()
		{
		}

		/*!
		 * \remarks 拷贝构造函数
		 * \return 
		 * \param UniqueID const & rhs
		*/
		UniqueID(UniqueID const& rhs) noexcept :
			mID(rhs.mID)
		{
		}

		/*!
		 * \remarks 拷贝赋值运算符
		 * \return 
		 * \param UniqueID const & rhs
		*/
		UniqueID& operator=(UniqueID const& rhs) noexcept
		{
			if (this != &rhs)
			{
				mID = rhs.mID;
			}

			return *this;
		}

		/*!
		 * \remarks 获取哈希值
		 * \return 
		*/
		size_t getHashValue() const
		{
			return boost::hash<boost::uuids::uuid>()(mID);
		}

		/*!
		 * \remarks 是否相等
		 * \return 
		 * \param UniqueID const & rhs
		*/
		bool operator==(UniqueID const& rhs) const
		{
			return mID == rhs.mID;
		}

		/*!
		 * \remarks 重载<运算符
		 * \return 
		 * \param UniqueID const & rhs
		*/
		bool operator<(UniqueID const& rhs) const
		{
			return mID < rhs.mID;
		}

		String toString()
		{
			return boost::uuids::to_string(mID);
		}

		boost::uuids::uuid mID;
	};

	/*!
	 * \brief
	 * 用于容器
	 * \class UniqueIDHasher
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/17
	 *
	 * \todo
	 */
	struct UniqueIDHasher : std::unary_function<UniqueID, size_t>
	{
		size_t operator()(UniqueID const& uid) const
		{
			return boost::hash<boost::uuids::uuid>()(uid.mID);
		}
	};

	/*!
	 * \brief
	 * 用于容器
	 * \class UniqueIDEqual
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/17
	 *
	 * \todo
	 */
	struct UniqueIDEqual : std::binary_function<UniqueID, UniqueID, bool>
	{
		bool operator()(UniqueID const& left,UniqueID const& right) const
		{
			return left.mID == right.mID;
		}
	};
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_UNIQUEID_H_

/*
UUID是一个128位的数字（16字节），可以创建全球唯一的标识符。
只要你想唯一地标识一个实体，就可以使用UUID。
UUID被设计为没有构造函数，可以像POD数据类型一样使用。
在数量庞大的UUID中有一个特殊的全零值nil，它表示一个无效的UUID，成员函数is_nil()可以检测uuid是否是nil。

using some_type = std::unordered_map<UniqueID,T,UniqueIDHasher,UniqueIDEqual,
																ContainerAllocator<std::pair<const UniqueID,T>>>;
*/