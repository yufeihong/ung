/*!
 * \brief
 * 枚举合集
 * \file UIFEnumCollection.h
 *
 * \author Su Yang
 *
 * \date 2017/06/17
 */
#ifndef _UIF_ENUMCOLLECTION_H_
#define _UIF_ENUMCOLLECTION_H_

#ifndef _UIF_CONFIG_H_
#include "UIFConfig.h"
#endif

#ifdef UIF_HIERARCHICAL_COMPILE

namespace ung
{
	/*
	默认为CM_CW:把顺时针面给cull掉
	*/
	enum class CullMode : uint32
	{
		CM_CW = 1,
		CM_CCW = 2
	};
}//namespace ung

#endif//UIF_HIERARCHICAL_COMPILE

#endif//_UIF_ENUMCOLLECTION_H_