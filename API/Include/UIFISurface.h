/*!
 * \brief
 * 表面接口
 * \file UIFISurface.h
 *
 * \author Su Yang
 *
 * \date 2017/04/29
 */
#ifndef _UIF_ISURFACE_H_
#define _UIF_ISURFACE_H_

#ifndef _UIF_CONFIG_H_
#include "UIFConfig.h"
#endif

#ifdef UIF_HIERARCHICAL_COMPILE

namespace ung
{
	enum class LockSurfaceFlag : uint32
	{
		LSF_NONE = 0x00000000,

		/*
		The application discards all memory within the locked region.
		For vertex and index buffers,the entire buffer will be discarded.
		This option is only valid when the resource is created with dynamic usage.
		*/
		LSF_DISCARD = 0x00002000,

		/*
		Allows an application to gain back CPU cycles if the driver cannot lock the surface 
		immediately.If this flag is set and the driver cannot lock the surface immediately,the lock 
		call will return D3DERR_WASSTILLDRAWING.This flag can only be used when locking a 
		surface created using CreateOffscreenPlainSurface,CreateRenderTarget,or 
		CreateDepthStencilSurface.This flag can also be used with a back buffer.
		*/
		LSF_DONOTWAIT = 0x00004000,

		/*
		By default,a lock on a resource adds a dirty region to that resource.This option prevents 
		any changes to the dirty state of the resource.Applications should use this option when 
		they have additional information about the set of regions changed during the lock 
		operation.
		*/
		LSF_NO_DIRTY_UPDATE = 0x00008000,

		/*
		Indicates that memory that was referred to in a drawing call since the last lock without 
		this flag will not be modified during the lock.This can enable optimizations when the 
		application is appending data to a resource.Specifying this flag enables the driver to 
		return immediately if the resource is in use,otherwise,the driver must finish using the 
		resource before returning from locking.
		*/
		LSF_NOOVERWRITE = 0x00001000,

		/*
		The default behavior of a video memory lock is to reserve a system-wide critical section,
		guaranteeing that no display mode changes will occur for the duration of the lock.This 
		option causes the system-wide critical section not to be held for the duration of the lock.
		The lock operation is time consuming,but can enable the system to perform other duties,
		such as moving the mouse cursor.This option is useful for long-duration locks,such as the 
		lock of the back buffer for software rendering that would otherwise adversely affect 
		system responsiveness.
		*/
		LSF_NOSYSLOCK = 0x00000800,

		/*
		The application will not write to the buffer.This enables resources stored in non-native 
		formats to save the recompression step when unlocking.
		*/
		LSF_READONLY = 0x00000010
	};

	class PixelRectangle;

	/*!
	 * \brief
	 * 表面接口(offscreen plain surface)
	 * \class IOffscreenPlainSurface
	 *
	 * \author Su Yang
	 *
	 * \date 2017/04/29
	 *
	 * \todo
	 */
	class UngExport IOffscreenPlainSurface : public MemoryPool<IOffscreenPlainSurface>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		IOffscreenPlainSurface() = default;

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~IOffscreenPlainSurface() = default;

		/*!
		 * \remarks 获取宽度
		 * \return 像素个数
		*/
		virtual uint32 getWidth() const PURE;

		/*!
		 * \remarks 获取高度
		 * \return 像素个数
		*/
		virtual uint32 getHeight() const PURE;

		/*!
		 * \remarks 获取像素格式
		 * \return 
		*/
		virtual PixelFormat getFormat() const PURE;

		/*!
		 * \remarks 获取颜色深度
		 * \return 
		*/
		virtual uint32 getColorDepth() const PURE;

		/*!
		 * \remarks 锁定一个区域
		 * \return 
		 * \param PixelRectangle const& rect 如果是默认构造的话，则表示整个表面
		 * \param LockSurfaceFlag lsFlag
		 * \param uint32& pitch
		*/
		virtual uint32* lock(PixelRectangle const& rect, LockSurfaceFlag lsFlag,uint32& pitch) PURE;

		/*!
		 * \remarks 解锁
		 * \return 
		*/
		virtual void unlock() PURE;
	};
}//namespace ung

#endif//UIF_HIERARCHICAL_COMPILE

#endif//_UIF_ISURFACE_H_