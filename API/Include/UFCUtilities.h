/*!
 * \brief
 * 基础工具集。
 * \file UFCUtilities.h
 *
 * \author Su Yang
 *
 * \date 2017/03/09
 */
#ifndef _UFC_UTILITIES_H_
#define _UFC_UTILITIES_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#ifndef _UFC_FILESYSTEM_H_
#include "UFCFilesystem.h"
#endif

namespace ung
{
	/*!
	 * \remarks 从一个weak_ptr得到一个shared_ptr
	 * \return 
	 * \param std::weak_ptr<T> pWeakPtr
	*/
	template <typename T>
	std::shared_ptr<T> ungGetStrongPtr(std::weak_ptr<T> pWeakPtr)
	{
		/*
		The reason for this is that a weak_ptr cannot be dereferenced directly,it must always be 
		converted to a shared_ptr before being used.
		*/

		if (!pWeakPtr.expired())
		{
			return std::shared_ptr<T>(pWeakPtr);
		}
		else
		{
			return std::shared_ptr<T>{};
		}
	}

	UNG_C_LINK_BEGIN;

	/*!
	 * \remarks 在系统内存中分配参数所指定字节的缓冲区(地址对齐)
	 * \return 
	 * \param uint32 bytes 缓冲区大小，单位：字节
	*/
	UngExport uint8* UNG_STDCALL ungMallocAlignedBuffer(uint32 bytes);

	/*!
	 * \remarks 释放通过ungMallocAlignedBuffer()函数所获得的指针
	 * \return 
	 * \param uint8* pBuffer
	*/
	UngExport void UNG_STDCALL ungFreeAlignedBuffer(uint8* pBuffer);

	/*!
	 * \remarks 检查给定文件的全名是否有效
	 * \return 
	 * \param String const& fileFullName
	*/
	UngExport void UNG_STDCALL ungCheckFileFullName(String const& fileFullName);

	/*!
	 * \remarks 内存地址转换为字符串
	 * \return 
	 * \param void* address
	 * \param String& outString
	*/
	UngExport void UNG_STDCALL ungAddressToString(void* address,String& outString);

	/*!
	 * \remarks 交换颜色的RB分量
	 * \return 
	 * \param uint32 & color
	*/
	UngExport void UNG_STDCALL ungSwapColorRB(uint32& color);

	/*!
	 * \remarks left |= ev;
	 * \return 
	 * \param uint32 & left
	 * \param uint32 ev
	*/
	UngExport void UNG_STDCALL ungEnumCombinationJoin(uint32& left,uint32 ev);

	/*!
	 * \remarks left &= ~ev;
	 * \return 
	 * \param uint32 & left
	 * \param uint32 ev
	*/
	UngExport void UNG_STDCALL ungEnumCombinationRemove(uint32& left,uint32 ev);

	/*!
	 * \remarks left & ev
	 * \return 
	 * \param uint32 & left
	 * \param uint32 ev
	*/
	UngExport bool UNG_STDCALL ungEnumCombinationHave(uint32& left,uint32 ev);

	UNG_C_LINK_END;
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_UTILITIES_H_