/*!
 * \brief
 * 散列表迭代器
 * \file UTLHashTableIterator.h
 *
 * \author Su Yang
 *
 * \date 2016/04/07
 */
#ifndef _UNG_UTL_HASHTABLEITERATOR_H_
#define _UNG_UTL_HASHTABLEITERATOR_H_

#ifndef _UNG_UTL_CONFIG_H_
#include "UTLConfig.h"
#endif

namespace ung
{
	namespace utl
	{
		template <typename Value>
		struct HashTableNode;

		template <typename Value, typename Key, typename HashFcn,typename ExtractKey, typename EqualKey, typename Alloc>
		class HashTable;

		using HashTableIteratorTag = std::forward_iterator_tag;

		/*!
		 * \brief
		 * 散列表迭代器
		 * \class HashTableIterator
		 *
		 * \author Su Yang
		 *
		 * \date 2016/04/07
		 *
		 * \todo
		 */
		template<typename Value, typename Key, typename HashFcn,typename ExtractKey, typename EqualKey, typename Alloc>
		class HashTableIterator : public boost::iterator_facade<HashTableIterator<Value, Key, HashFcn,ExtractKey,EqualKey,Alloc>, Value, HashTableIteratorTag>
		{
			friend class boost::iterator_core_access;

		public:
			using value_type = Value;
			/*
			self_type,pointer和reference有可能是普通的，也有可能是const的，根据具体的模板参数Value是否包含const修饰词来定。
			iterator_facade里面的仅仅只是在定义value_type的时候，把模板参数Value身上的const修饰词给remove掉了，它的reference和pointer并没有remove掉const修饰词。
			所以说，假如模板参数Value为const sfast，那么reference就是const sfast&，pointer就是const sfast*。
			*/
			using self_type = HashTableIterator<value_type, Key, HashFcn,ExtractKey,EqualKey,Alloc>;
			using pointer = typename iterator_facade::pointer;
			using reference = typename iterator_facade::reference;
			using difference_type = typename iterator_facade::difference_type;
			using iterator_category = HashTableIteratorTag;
			using size_type = usize;
			using node_type = HashTableNode<value_type>;
			using node_ptr = typename HashTableNode<value_type>::pointer;
			using table_type = HashTable<value_type, Key, HashFcn,ExtractKey,EqualKey,Alloc>;

			/*!
			 * \remarks 默认构造函数
			 * \return 
			*/
			HashTableIterator() :
				mCur(nullptr),
				mHashTable(nullptr)
			{
			}

			HashTableIterator(node_ptr n, table_type* tab) :
				mCur(n),
				mHashTable(tab)
			{
			}

			/*!
			 * \remarks 拷贝构造函数
			 * \return 
			 * \param self_type const & other
			*/
			HashTableIterator(self_type const& other) :
				mCur(other.mCur),
				mHashTable(other.mHashTable)
			{
			}

			/*!
			 * \remarks 拷贝赋值运算符
			 * \return self_type&
			 * \param self_type const & other 
			*/
			self_type& operator=(self_type const& other)
			{
				mCur = other.mCur;
				mHashTable = other.mHashTable;

				return *this;
			}

			node_ptr mCur;													//迭代器当前所指向的节点
			table_type* mHashTable;

		private:
			reference dereference() const
			{
				return mCur->mValue;
			}

			void increment()
			{
				const node_type* old = mCur;
				mCur = mCur->mNext;

				if (!mCur)
				{
					size_type bkt = mHashTable->calPos(old->mValue);
					while (!mCur && ++bkt < mHashTable->mBuckets.size())
					{
						mCur = mHashTable->mBuckets[bkt];
					}
				}
			}

			bool equal(self_type const& other) const
			{
				return mCur == other.mCur;
			}
		};

		template<typename Value, typename Key, typename HashFcn,typename ExtractKey, typename EqualKey, typename Alloc>
		class HashTableConstIterator : public boost::iterator_facade<HashTableConstIterator<Value, Key, HashFcn,ExtractKey,EqualKey,Alloc>, const Value, HashTableIteratorTag>
		{
			friend class boost::iterator_core_access;

		public:
			using value_type = Value;
			using self_type = HashTableConstIterator<value_type, Key, HashFcn,ExtractKey,EqualKey,Alloc>;
			using iterator = HashTableIterator<value_type,Key, HashFcn, ExtractKey, EqualKey, Alloc>;//<typename boost::remove_const<value_type>::type, Key, HashFcn, ExtractKey, EqualKey, Alloc>;
			using pointer = typename iterator_facade::pointer;
			using reference = typename iterator_facade::reference;
			using difference_type = typename iterator_facade::difference_type;
			using iterator_category = HashTableIteratorTag;
			using size_type = usize;
			using node_type = HashTableNode<value_type>;
			using table_type = HashTable<value_type, Key, HashFcn,ExtractKey,EqualKey,Alloc>;

			/*!
			 * \remarks 默认构造函数
			 * \return 
			*/
			HashTableConstIterator() :
				mCur(nullptr),
				mHashTable(nullptr)
			{
			}

			/*!
			 * \remarks 构造函数
			 * \return 
			 * \param node_ptr n
			 * \param HashTable const* tab
			*/
			HashTableConstIterator(node_type const* n, table_type const* tab) :
				mCur(n),
				mHashTable(tab)
			{
			}

			/*!
			 * \remarks 通过非常量迭代器来构造常量迭代器
			 * \return 
			 * \param iterator const& it
			*/
			HashTableConstIterator(iterator const& it) :
				mCur(it.mCur),
				mHashTable(it.mHashTable)
			{
			}

			/*!
			 * \remarks 拷贝构造函数
			 * \return 
			 * \param self_type const & other
			*/
			HashTableConstIterator(self_type const& other) :
				mCur(other.mCur),
				mHashTable(other.mHashTable)
			{
			}

			/*!
			 * \remarks 拷贝赋值运算符
			 * \return self_type&
			 * \param self_type const & other 
			*/
			self_type& operator=(self_type const& other)
			{
				mCur = other.mCur;
				mHashTable = other.mHashTable;

				return *this;
			}

			node_type const* mCur;
			table_type const* mHashTable;

		private:
			reference dereference() const
			{
				return mCur->mValue;
			}

			void increment()
			{
				node_type const* old = mCur;
				mCur = mCur->mNext;

				if (!mCur)
				{
					size_type bkt = mHashTable->calPos(old->mValue);
					while (!mCur && ++bkt < mHashTable->mBuckets.size())
					{
						mCur = mHashTable->mBuckets[bkt];
					}
				}
			}

			bool equal(self_type const& other) const
			{
				return mCur == other.mCur;
			}
		};
	}//namespace utl
}//namespace ung

#endif//_UNG_UTL_HASHTABLEITERATOR_H_