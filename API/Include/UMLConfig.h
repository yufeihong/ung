/*!
 * \brief
 * 配置文件
 * \file UMLConfig.h
 *
 * \author Su Yang
 *
 * \date 2016/06/15
 */
#ifndef _UML_CONFIG_H_
#define _UML_CONFIG_H_

#ifndef _UML_ENABLE_H_
#include "UMLEnable.h"
#endif

#ifdef UML_HIERARCHICAL_COMPILE

//其它库的头文件
#ifndef _UBS_H_
#include "UBS.h"
#endif

#ifndef _UTM_H_
#include "UTM.h"
#endif

#ifndef _UMM_H_
#include "UMM.h"
#endif

#ifndef _UFC_H_
#include "UFC.h"
#endif

#ifndef _INCLUDE_BOOST_OPERATORS_HPP_
#include "boost/operators.hpp"
#define _INCLUDE_BOOST_OPERATORS_HPP_
#endif

#if UNG_USE_SIMD
#ifndef _INCLUDE_STLIB_XMMINTRIN_
#include <xmmintrin.h>
#define _INCLUDE_STLIB_XMMINTRIN_
#endif

#define SHUFFLE_PARAM(x,y,z,w)								\
((x) | ((y) << 2) | ((z) << 4) | ((w) << 6))

#define _mm_replicate_x_ps(v)								\
_mm_shuffle_ps((v),(v),SHUFFLE_PARAM(0,0,0,0))

#define _mm_replicate_y_ps(v)								\
_mm_shuffle_ps((v),(v),SHUFFLE_PARAM(1,1,1,1))

#define _mm_replicate_z_ps(v)								\
_mm_shuffle_ps((v),(v),SHUFFLE_PARAM(2,2,2,2))

#define _mm_replicate_w_ps(v)								\
_mm_shuffle_ps((v),(v),SHUFFLE_PARAM(3,3,3,3))

#define _mm_madd_ps(a,b,c)									\
_mm_add_ps(_mm_mul_ps((a),(b)),(c))
#endif

//double->float
#pragma warning(disable:4244)
#pragma warning(disable:4305)

#endif//UML_HIERARCHICAL_COMPILE

#endif//_UML_CONFIG_H_

/*
UNG的约定：
右手坐标系，Y轴向上
行为主矩阵
三角形顶点环绕正方向为逆时针counter-clockwise。
旋转正方向为从轴的负端点向正端点看，顺时针。即，右手握住旋转轴，大拇指指向旋转轴的正端点，其余四指的环绕方向为顺时针。
*/