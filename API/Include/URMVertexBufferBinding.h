///*!
// * \brief
// * 绑定缓存
// * \file URMVertexBufferBinding.h
// *
// * \author Su Yang
// *
// * \date 2017/05/31
// */
//#ifndef _URM_VERTEXBUFFERBINDING_H_
//#define _URM_VERTEXBUFFERBINDING_H_
//
//#ifndef _URM_CONFIG_H_
//#include "URMConfig.h"
//#endif
//
//#ifdef URM_HIERARCHICAL_COMPILE
//
//#ifndef _UFC_STLWRAPPER_H_
//#include "UFCSTLWrapper.h"
//#endif
//
//namespace ung
//{
//	class VertexBuffer;
//
//	/*!
//	 * \brief
//	 * 
//	 * \class VertexBufferBinding
//	 *
//	 * \author Su Yang
//	 *
//	 * \date 2017/05/31
//	 *
//	 * \todo
//	 */
//	class UngExport VertexBufferBinding : public MemoryPool<VertexBufferBinding>
//	{
//	public:
//		//using VertexBufferBindingUnMap = UnorderedMapWrapper<uint32,VertexBuffer*>;
//		//using BindingIndexUnMap = UnorderedMapWrapper<uint32, uint32>;
//
//	public:
//		/*!
//		 * \remarks 默认构造函数
//		 * \return 
//		*/
//		VertexBufferBinding();
//
//		/*!
//		 * \remarks 虚析构函数
//		 * \return 
//		*/
//		virtual ~VertexBufferBinding();
//
//		/*!
//		 * \remarks 把一个顶点缓存和一个索引给关联起来
//		 * \return 
//		 * \param uint32 index
//		 * \param VertexBuffer* buffer
//		*/
//		virtual void setBinding(uint32 index,VertexBuffer* buffer);
//
//		/*!
//		 * \remarks Removes an existing binding
//		 * \return 
//		 * \param uint32 index
//		*/
//		virtual void unsetBinding(uint32 index);
//
//		/*!
//		 * \remarks Removes all the bindings
//		 * \return 
//		*/
//		virtual void unsetAllBindings();
//
//		/*!
//		 * \remarks Gets a read-only version of the buffer bindings
//		 * \return 
//		*/
//		virtual const VertexBufferBindingUnMap& getBindings() const;
//
//		/*!
//		 * \remarks Gets the buffer bound to the given source index
//		 * \return 
//		 * \param uint32 index
//		*/
//		virtual VertexBuffer const* getBuffer(uint32 index) const;
//
//		/*!
//		 * \remarks Gets whether a buffer is bound to the given source index
//		 * \return 
//		 * \param uint32 index
//		*/
//		virtual bool isBufferBound(uint32 index) const;
//
//		/*!
//		 * \remarks 
//		 * \return 
//		*/
//		virtual usize getBufferCount() const;
//
//		/*!
//		 * \remarks Gets the highest index which has already been set,plus 1
//		 * \return 
//		*/
//		virtual uint32 getNextIndex() const;
//
//		/*!
//		 * \remarks Gets the last bound index.
//		 * \return 
//		*/
//		virtual uint32 getLastBoundIndex() const;
//
//		/*!
//		 * \remarks Check whether any gaps in the bindings
//		 * \return 
//		*/
//		virtual bool hasGaps() const;
//
//		/*!
//		 * \remarks Remove any gaps in the bindings,If this bindings is already being used with a VertexDeclaration,you will need to alter that too
//		 * \return 
//		 * \param BindingIndexMap & bindingIndexMap To be retrieve the binding index map that used to translation old index to new index
//		*/
//		//virtual void closeGaps(BindingIndexUnMap& bindingIndexMap);
//
//	protected:
//		mutable uint32 mHighIndex = 0;
//		//VertexBufferBindingUnMap mBindings;
//	};
//}//namespace ung
//
//#endif//URM_HIERARCHICAL_COMPILE
//
//#endif//_URM_VERTEXBUFFERBINDING_H_