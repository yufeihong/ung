/*!
 * \brief
 * 渲染目标接口
 * \file UIFIRenderTarget.h
 *
 * \author Su Yang
 *
 * \date 2017/04/29
 */
#ifndef _UIF_IRENDERTARGET_H_
#define _UIF_IRENDERTARGET_H_

#ifndef _UIF_CONFIG_H_
#include "UIFConfig.h"
#endif

#ifdef UIF_HIERARCHICAL_COMPILE

namespace ung
{
	/*!
	 * \brief
	 * 渲染目标接口
	 * \class IRenderTarget
	 *
	 * \author Su Yang
	 *
	 * \date 2017/04/29
	 *
	 * \todo
	 */
	class UngExport IRenderTarget : public MemoryPool<IRenderTarget>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		IRenderTarget() = default;

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~IRenderTarget() = default;
	};
}//namespace ung

#endif//UIF_HIERARCHICAL_COMPILE

#endif//_UIF_IRENDERTARGET_H_