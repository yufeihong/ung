/*!
 * \brief
 * �¼�����
 * \file UESEventType.h
 *
 * \author Su Yang
 *
 * \date 2016/12/25
 */
#ifndef _UES_EVENTTYPE_H_
#define _UES_EVENTTYPE_H_

#ifndef _UES_CONFIG_H_
#include "UESConfig.h"
#endif

#ifdef UES_HIERARCHICAL_COMPILE

namespace ung
{
	using EventType = uint32;
}//namespace ung

#endif//UES_HIERARCHICAL_COMPILE

#endif//_UES_EVENTTYPE_H_