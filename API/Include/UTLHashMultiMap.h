/*!
 * \brief
 * HashMultiMap
 * \file UTLHashMultiMap.h
 *
 * \author Su Yang
 *
 * \date 2016/04/09
 */
#ifndef _UNG_UTL_HASHMULTIMAP_H_
#define _UNG_UTL_HASHMULTIMAP_H_

#ifndef _UNG_UTL_CONFIG_H_
#include "UTLConfig.h"
#endif

#ifndef _UNG_UTL_HASHTABLE_H_
#include "UTLHashTable.h"
#endif

#ifndef _UNG_UTL_TYPETRAITS_H_
#include "UTLTypeTraits.h"
#endif

namespace ung
{
	namespace utl
	{
		/*!
		 * \brief
		 * HashMultiMap
		 * \class HashMultiMap
		 *
		 * \author Su Yang
		 *
		 * \date 2016/04/09
		 *
		 * \todo
		 */
		template <typename Key, typename T, typename HashFcn = boost::hash<Key>, typename EqualKey = std::equal_to<Key>,typename Alloc = Allocator<HashTableNode<std::pair<Key const,T>>>>
		class HashMultiMap
		{
		private:
			typedef HashTable<std::pair<const Key, T>, Key, HashFcn,select1st<std::pair<const Key, T> >, EqualKey, Alloc> table_type;
			table_type mTable;

		public:
			typedef typename table_type::key_type key_type;
			typedef T data_type;
			typedef T mapped_type;
			typedef typename table_type::value_type value_type;
			typedef typename table_type::hasher hasher;
			typedef typename table_type::key_equal key_equal;

			typedef typename table_type::size_type size_type;
			typedef typename table_type::difference_type difference_type;
			typedef typename table_type::pointer pointer;
			typedef typename table_type::const_pointer const_pointer;
			typedef typename table_type::reference reference;
			typedef typename table_type::const_reference const_reference;

			typedef typename table_type::iterator iterator;
			typedef typename table_type::const_iterator const_iterator;

			hasher getHasher() const
			{
				return mTable.getHasher();
			}

			key_equal getEqual() const
			{
				return mTable.getEqual();
			}

			HashMultiMap() :
				mTable(100, hasher(), key_equal())
			{
			}

			explicit HashMultiMap(size_type n) :
				mTable(n, hasher(), key_equal())
			{
			}

			HashMultiMap(size_type n, const hasher& hf) :
				mTable(n, hf, key_equal())
			{
			}

			HashMultiMap(size_type n, const hasher& hf, const key_equal& eql) :
				mTable(n, hf, eql)
			{
			}

			template <class InputIterator>
			HashMultiMap(InputIterator first, InputIterator last) :
				mTable(100, hasher(), key_equal())
			{
				mTable.insertEqual(first, last);
			}

			template <class InputIterator>
			HashMultiMap(InputIterator first, InputIterator last, size_type n) :
				mTable(n, hasher(), key_equal())
			{
				mTable.insertEqual(first, last);
			}

			template <class InputIterator>
			HashMultiMap(InputIterator first, InputIterator last, size_type n,const hasher& hf) :
				mTable(n, hf, key_equal())
			{
				mTable.insertEqual(first, last);
			}

			template <class InputIterator>
			HashMultiMap(InputIterator first, InputIterator last, size_type n,const hasher& hf, const key_equal& eql) :
				mTable(n, hf, eql)
			{
				mTable.insertEqual(first, last);
			}

			size_type size() const
			{
				return mTable.size();
			}

			size_type max_size() const
			{
				return mTable.max_size();
			}

			bool empty() const
			{
				return mTable.empty();
			}

			void swap(HashMultiMap& hs)
			{
				mTable.swap(hs.mTable);
			}

			iterator begin()
			{
				return mTable.begin();
			}

			iterator end()
			{
				return mTable.end();
			}

			const_iterator begin() const
			{
				return mTable.begin();
			}

			const_iterator end() const
			{
				return mTable.end();
			}

			std::pair<iterator, bool> insert(const value_type& obj)
			{
				return mTable.insertEqual(obj);
			}

			template <class InputIterator>
			void insert(InputIterator first, InputIterator last)
			{
				mTable.insertEqual(first, last);
			}

			std::pair<iterator, bool> insertNoresize(const value_type& obj)
			{
				return mTable.insertUniqueNoResize(obj);
			}

			iterator find(const key_type& key)
			{
				return mTable.find(key);
			}

			const_iterator find(const key_type& key) const
			{
				return mTable.find(key);
			}

			T& operator[](const key_type& key)
			{
				return mTable.findOrInsert(value_type(key, T())).second;
			}

			size_type count(const key_type& key) const
			{
				return mTable.count(key);
			}

			std::pair<iterator, iterator> equalRange(const key_type& key)
			{
				return mTable.equalRange(key);
			}

			std::pair<const_iterator, const_iterator> equalRange(const key_type& key) const
			{
				return mTable.equalRange(key);
			}

			size_type erase(const key_type& key)
			{
				return mTable.erase(key);
			}

			void erase(iterator it)
			{
				mTable.erase(it);
			}

			void erase(iterator first, iterator last)
			{
				mTable.erase(first, last);
			}

			void clear()
			{
				mTable.clear();
			}

			void resize(size_type hint)
			{
				mTable.resize(hint);
			}

			size_type bucketCount() const
			{
				return mTable.bucketCount();
			}

			size_type maxBucketCount() const
			{
				return mTable.maxBucketCount();
			}

			size_type elemsNumInBucket(size_type n) const
			{
				return mTable.elemsNumInBucket(n);
			}

			template<typename Key, typename T, typename HashFcn, typename EqualKey,typename Alloc>
			friend bool operator== (const HashMultiMap<Key, T, HashFcn, EqualKey,Alloc>&,const HashMultiMap<Key, T, HashFcn, EqualKey,Alloc>&);

			template<typename Key, typename T, typename HashFcn, typename EqualKey, typename Alloc>
			friend bool operator!= (const HashMultiMap<Key, T, HashFcn, EqualKey, Alloc>&, const HashMultiMap<Key, T, HashFcn, EqualKey, Alloc>&);

			template <typename Key, typename T, typename HashFcn, typename EqualKey, typename Alloc>
			friend void swap(HashMultiMap<Key, T, HashFcn, EqualKey, Alloc>& hm1, HashMultiMap<Key, T, HashFcn, EqualKey, Alloc>& hm2);
		};

		template <typename Key, typename T, typename HashFcn, typename EqualKey,typename Alloc>
		inline bool operator==(const HashMultiMap<Key, T, HashFcn, EqualKey,Alloc>& hm1,const HashMultiMap<Key, T, HashFcn, EqualKey,Alloc>& hm2)
		{
			return hm1.mTable == hm2.mTable;
		}

		template <typename Key, typename T, typename HashFcn, typename EqualKey,typename Alloc>
		inline bool operator!=(const HashMultiMap<Key, T, HashFcn, EqualKey,Alloc>& hm1,const HashMultiMap<Key, T, HashFcn, EqualKey,Alloc>& hm2)
		{
			return !(hm1 == hm2);
		}

		template <typename Key, typename T, typename HashFcn, typename EqualKey,typename Alloc>
		inline void swap(HashMultiMap<Key, T, HashFcn, EqualKey,Alloc>& hm1,HashMultiMap<Key, T, HashFcn, EqualKey,Alloc>& hm2)
		{
			hm1.swap(hm2);
		}
	}//namespace utl
}//namespace ung

#endif//_UNG_UTL_HASHMULTIMAP_H_