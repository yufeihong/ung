/*!
 * \brief
 * 空间管理器工厂基类
 * \file USMSpatialManagerFactoryBase.h
 *
 * \author Su Yang
 *
 * \date 2017/03/27
 */
#ifndef _USM_SPATIALMANAGERFACTORYBASE_H_
#define _USM_SPATIALMANAGERFACTORYBASE_H_

#ifndef _USM_CONFIG_H_
#include "USMConfig.h"
#endif

#ifdef USM_HIERARCHICAL_COMPILE

#ifndef _UIF_ISPATIALMANAGERFACTORY_H_
#include "UIFISpatialManagerFactory.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 空间管理器工厂基类
	 * \class SpatialManagerFactory
	 *
	 * \author Su Yang
	 *
	 * \date 2017/03/27
	 *
	 * \todo
	 */
	class UngExport SpatialManagerFactoryBase : public ISpatialManagerFactory
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		SpatialManagerFactoryBase() = default;

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~SpatialManagerFactoryBase() = default;

		/*!
		 * \remarks 获取工厂的标识
		 * \return 
		*/
		virtual String const& getIdentity() const override;

		/*!
		 * \remarks 获取工厂的标识
		 * \return 
		*/
		virtual SceneTypeMask getMask() const override;

		/*!
		 * \remarks 创建一个空间管理器
		 * \return 
		 * \param String const & instanceName 空间管理器的名字
		*/
		virtual ISpatialManager* createInstance(String const& instanceName) override;

		/*!
		 * \remarks 销毁一个空间管理器
		 * \return 
		 * \param ISpatialManager * instancePtr 指向空间管理器的指针
		*/
		virtual void destroyInstance(ISpatialManager* instancePtr) override;
	};
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE

#endif//_USM_SPATIALMANAGERFACTORYBASE_H_