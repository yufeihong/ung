/*!
 * \brief
 * 过滤器
 * \file UFCFilter.h
 *
 * \author Su Yang
 *
 * \date 2016/12/20
 */
#ifndef _UFC_FILTER_H_
#define _UFC_FILTER_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

 //过滤器辅助类
#ifndef _INCLUDE_BOOST_AGGREGATE_HPP_
#include "boost/iostreams/filter/aggregate.hpp"
#define _INCLUDE_BOOST_AGGREGATE_HPP_
#endif

namespace ung
{
	/*
	aggregate_filter:
	是一个两用过滤器辅助类，一次性接收所有的数据。
	这意味着只要使用它就既可以实现输入过滤器也可以实现输出过滤器
	*/
	/*!
	 * \brief
	 * 默认过滤器
	 * \class DefaultFilter
	 *
	 * \author Su Yang
	 *
	 * \date 2016/12/18
	 *
	 * \todo
	 */
	template<typename Ch>
	class DefaultFilter : public boost::iostreams::aggregate_filter<Ch>
	{
	public:
		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~DefaultFilter()
		{
		}

	private:
		/*
		纯虚函数do_filter()用于实现过滤器的核心功能。
		它有两个参数，src是过滤器接收到的所有字符，我们可以对这些字符进行任意的处理，然后把处理结果添加到dest，这两个参数都存储
		在std::vector<Ch>中。
		处理后的结果存储在dest中，dest也就代表了最终sink的内容。
		*/
		/*!
		 * \remarks 实现过滤器的核心功能
		 * \return 
		 * \param const vector_type & src
		 * \param vector_type & dest
		*/
		virtual void do_filter(const vector_type& src, vector_type& dest) override
		{
			//dest不能为空
			std::copy(src.cbegin(), src.cend(), std::back_inserter(dest));
		}
	};
	//给过滤器增加管道能力。1:过滤器模板参数的数量
	BOOST_IOSTREAMS_PIPABLE(DefaultFilter, 1)
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_FILTER_H_