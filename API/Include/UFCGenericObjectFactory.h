/*!
 * \brief
 * 泛型对象工厂。
 * \file UFCGenericObjectFactory.h
 *
 * \author Su Yang
 *
 * \date 2016/12/13
 */
#ifndef _UFC_GENERICOBJECTFACTORY_H_
#define _UFC_GENERICOBJECTFACTORY_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#ifndef _UFC_CHOOSECONTAINER_H_
#include "UFCChooseContainer.h"
#endif

namespace
{
	/*!
	 * \remarks 构造一个用于创建对象的函数
	 * \return 
	*/
	template <typename BaseType, typename SubType>
	BaseType* genericObjectCreator()
	{
		using dispatch_type = typename boost::mpl::if_c <
			boost::has_new_operator<SubType>::value,
			boost::true_type, boost::false_type > ::type;

		return _genericObjectCreator<BaseType,SubType>(dispatch_type());
	}

	/*!
	 * \remarks 如果SubType重载了new运算符
	 * \return 
	 * \param boost::true_type
	*/
	template <typename BaseType, typename SubType>
	BaseType* _genericObjectCreator(boost::true_type)
	{
		return UNG_NEW SubType;
	}

	/*!
	 * \remarks 如果SubType没有重载new运算符
	 * \return 
	 * \param boost::false_type
	*/
	template <typename BaseType, typename SubType>
	BaseType* _genericObjectCreator(boost::false_type)
	{
		return GLOBAL_NEW SubType;
	}
}//namespace

namespace ung
{
	/*!
	 * \brief
	 * 泛型对象工厂。
	 * \class GenericObjectFactory
	 *
	 * \author Su Yang
	 *
	 * \date 2016/12/13
	 *
	 * \todo
	 */
	template <typename BaseType, typename Key,template<typename> class Hasher = std::hash>
	class GenericObjectFactory : public MemoryPool<GenericObjectFactory<BaseType,Key,Hasher>>
	{
		//创建对象的函数类型
		typedef BaseType* (*ObjectCreatorType)();

	public:
		/*!
		 * \remarks 注册用于创建对象的函数
		 * \return 
		 * \param Key key 对象ID的类型
		*/
		template <typename SubType>
		bool registerCreator(Key key)
		{
			auto findIt = mCreators.find(key);
			if (findIt == mCreators.end())
			{
				//没有找到，那么插入
				mCreators[key] = &genericObjectCreator<BaseType, SubType>;

				return true;
			}

			return false;
		}

		/*!
		 * \remarks 创建对象
		 * \return 
		 * \param Key key
		*/
		BaseType* create(Key key)
		{
			auto findIt = mCreators.find(key);
			if (findIt != mCreators.end())
			{
				//找到了，那么创建对象
				ObjectCreatorType pFunc = findIt->second;
				//调用用于创建对象的函数
				return pFunc();
			}

			return nullptr;
		}

	private:
		//把要创建的类型和其创建函数关联起来
		STL_UNORDERED_MAP_H(Key, ObjectCreatorType,Hasher<Key>) mCreators;
	};
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_GENERICOBJECTFACTORY_H_