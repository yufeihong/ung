/*!
 * \brief
 * 2维向量
 * \file UMLVec2.h
 *
 * \author Su Yang
 *
 * \date 2016/06/15
 */
#ifndef _UML_VEC2_H_
#define _UML_VEC2_H_

#ifndef _UML_CONFIG_H_
#include "UMLConfig.h"
#endif

#ifdef UML_HIERARCHICAL_COMPILE

namespace ung
{
	class Radian;

	/*!
	 * \brief
	 * 2维向量
	 * \class Vec2
	 *
	 * \author Su Yang
	 *
	 * \date 2016/06/15
	 *
	 * \todo
	 */
	template<typename T>
	class Vec2 : public MemoryPool<Vec2<T>>
	{
		/*!
		 * \remarks 标量乘以向量
		 * \return 
		 * \param T s
		 * \param Vec2 v
		*/
		friend Vec2 operator*(T s, Vec2 v)
		{
			return Vec2(s * v.x, s * v.y);
		}

		/*!
		 * \remarks 打印向量
		 * \return 
		 * \param std::ostream & o
		 * \param const Vec2<T> & v
		*/
		friend std::ostream& operator<<(std::ostream& o, const Vec2<T>& v)
		{
			o << "Vector2(" << v.x << "," << v.y << ")";
			return o;
		}

	public:
		using element_type = T;
		using math_type = MathImpl<element_type>;

		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		Vec2() = default;

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~Vec2() = default;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param const T& scaler
		*/
		explicit Vec2(const T& scaler);

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param const T & xx
		 * \param const T & yy
		*/
		Vec2(const T& xx, const T& yy);

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param const T* const ar 指向const T的常量指针
		 * \param const uint32 sz 数组大小
		*/
		Vec2(const T *const ar, const uint32 sz);

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param const T(&ar)[2] 数组的引用
		*/
		explicit Vec2(const T(&ar)[2]);
		
		/*!
		 * \remarks 拷贝构造函数
		 * \return 
		 * \param const Vec2 & v
		*/
		Vec2(const Vec2 &v);

		/*!
		 * \remarks 拷贝赋值运算符
		 * \return 
		 * \param const Vec2 & v
		*/
		Vec2& operator=(const Vec2& v);

		/*!
		 * \remarks 移动构造函数
		 * \return 
		 * \param Vec2 && v
		*/
		Vec2(Vec2 &&v) noexcept;

		/*!
		 * \remarks 移动赋值运算符
		 * \return 
		 * \param Vec2 && v
		*/
		Vec2& operator=(Vec2 &&v) noexcept;

		/*!
		 * \remarks 重载[]运算符
		 * \return 元素的引用
		 * \param const usize i
		*/
		T& operator [](const usize i);

		/*!
		 * \remarks 重载[]运算符，常量对象调用
		 * \return 元素的常量引用
		 * \param const usize i
		*/
		const T& operator [](const usize i) const;

		/*!
		 * \remarks 获取x的地址
		 * \return 
		*/
		T* getAddress();

		/*!
		 * \remarks 获取x的地址
		 * \return 
		*/
		T const* getAddress() const;

		/*!
		 * \remarks 重载==运算符
		 * \return 
		 * \param const Vec2 & v
		*/
		bool operator==(const Vec2& v) const;

		/*!
		 * \remarks 重载!=运算符
		 * \return 
		 * \param const Vec2 & v
		*/
		bool operator!=(const Vec2& v) const;

		/*!
		 * \remarks 重载<运算符
		 * \return 
		 * \param const Vec2 & v
		*/
		bool operator<(const Vec2& v) const;

		/*!
		 * \remarks 重载>运算符
		 * \return 
		 * \param const Vec2 & v
		*/
		bool operator>(const Vec2& v) const;

		/*!
		 * \remarks 重载-运算符
		 * \return 
		*/
		Vec2 operator-() const;

		/*!
		 * \remarks 重载+运算符
		 * \return 
		 * \param const Vec2 & v
		*/
		Vec2 operator+(const Vec2& v) const;

		/*!
		 * \remarks 重载+运算符
		 * \return 
		 * \param const T & s
		*/
		Vec2 operator+(const T& s) const;

		/*!
		 * \remarks 重载-运算符
		 * \return 
		 * \param const Vec2 & v
		*/
		Vec2 operator-(const Vec2& v) const;

		/*!
		 * \remarks 重载-运算符
		 * \return 
		 * \param const T & s
		*/
		Vec2 operator-(const T& s) const;

		/*!
		 * \remarks 重载*运算符
		 * \return 
		 * \param const T & s
		*/
		Vec2 operator*(const T& s) const;

		/*!
		 * \remarks Vec2 * Vec2
		 * \return 
		 * \param const Vec2 & v
		*/
		Vec2 operator*(const Vec2& v) const;

		/*!
		 * \remarks 重载/运算符
		 * \return 
		 * \param const T & s
		*/
		Vec2 operator/(const T& s) const;

		/*!
		 * \remarks Vec2 / Vec2
		 * \return 
		 * \param const Vec2 & v
		*/
		Vec2 operator/(const Vec2& v) const;

		/*!
		 * \remarks 重载+=运算符
		 * \return 
		 * \param const Vec2 & v
		*/
		Vec2& operator+=(const Vec2& v);

		/*!
		 * \remarks 重载+=运算符
		 * \return 
		 * \param const T & scaler
		*/
		Vec2& operator+=(const T& scaler);

		/*!
		 * \remarks 重载-=运算符
		 * \return 
		 * \param const Vec2 & v
		*/
		Vec2& operator-=(const Vec2& v);

		/*!
		 * \remarks 重载-=运算符
		 * \return 
		 * \param const T & scaler
		*/
		Vec2& operator-=(const T& scaler);

		/*!
		 * \remarks 重载*=运算符
		 * \return 
		 * \param const T & scaler
		*/
		Vec2& operator*=(const T& scaler);

		/*!
		 * \remarks 重载/=运算符
		 * \return 
		 * \param const T & scaler
		*/
		Vec2& operator/=(const T& scaler);

		/*!
		 * \remarks 设置两个元素都为0
		 * \return 
		*/
		void setZero();

		/*!
		 * \remarks 交换
		 * \return 
		 * \param Vec2 & other
		*/
		void swap(Vec2& other);

		/*!
		 * \remarks 计算长度
		 * \return 
		*/
		T length() const;

		/*!
		 * \remarks 计算长度的平方
		 * \return 
		*/
		T squaredLength() const;

		/*!
		 * \remarks 计算两点之间的距离
		 * \return 
		 * \param const Vec2 & v
		*/
		T distance(const Vec2& v) const;

		/*!
		 * \remarks 计算两点之间距离的平方
		 * \return 
		 * \param const Vec2 & v
		*/
		T squaredDistance(const Vec2& v) const;

		/*!
		 * \remarks 点积
		 * \return 
		 * \param const Vec2 & v
		*/
		T dotProduct(const Vec2& v) const;

		/*!
		 * \remarks 计算当前向量到另一个向量的投影分量
		 * \return 
		 * \param const Vec2 & v
		*/
		const Vec2 projectionComponent(const Vec2& v) const;

		/*!
		 * \remarks 计算向量P相对于向量Q的垂直分量
		 * \return 
		 * \param const Vec2 & v
		*/
		const Vec2 perpendicularComponent(const Vec2& v) const;

		/*!
		 * \remarks 使向量的长度为1
		 * \return 
		*/
		void normalize();

		/*!
		 * \remarks 获取当前向量normalize的一个拷贝，不改变当前向量
		 * \return 
		*/
		Vec2 normalizeCopy() const;

		/*!
		 * \remarks 计算两点的中点
		 * \return 
		 * \param const Vec2 & v
		*/
		Vec2 midPoint(const Vec2& v) const;

		/*!
		 * \remarks 获取小值
		 * \return 
		 * \param const Vec2 & v
		*/
		void setFloor(const Vec2& v);

		/*!
		 * \remarks 获取大值
		 * \return 
		 * \param const Vec2 & v
		*/
		void setCeil(const Vec2& v);

		/*!
		 * \remarks 产生一个垂直于该向量的向量
		 * \return 
		*/
		Vec2 perpendicular() const;

		/*!
		 * \remarks 判断当前向量的长度是否为0
		 * \return 
		*/
		bool isZeroLength() const;

		/*!
		 * \remarks 判断当前向量的长度是否为1
		 * \return 
		*/
		bool isUnitLength() const;

		/*!
		 * \remarks 计算当前向量，相对于法线的反射向量，要求当前向量和法线,均为单位向量
		 * \return 
		 * \param const Vec2 & normal
		*/
		Vec2 reflect(const Vec2& normal) const;

		/*!
		 * \remarks 计算两个向量的夹角
		 * \return 弧度
		 * \param const Vec2 & v
		 * \param bool normalized
		*/
		Radian angleBetween(const Vec2& v, bool normalized = false) const;

		/*!
		 * \remarks 获取主轴
		 * \return 
		*/
		Vec2 primaryAxis() const;

	public:
		/*
		当定义一个Vec2对象时,比如:
		Vec2 v2;
		会首先进入默认构造函数,然后跳转到这里.
		*/
		T x{ 0.0 };
		T y{ 0.0 };

		static const Vec2 ZERO;
	};

	/*
	一些编译器允许在尚无友元函数的初始声明的情况下就调用它.不过即使你的编译器支持这种行为,最好还是提供一个独立的函数声明.这样即使你更换了
	一个有这种强制要求的编译器,也不必改变代码.
	*/
	//Vec2 operator+(const T scaler, const Vec2& v);
	//Vec2 operator+(const Vec2& v, const T scaler);
	//Vec2 operator-(const T scaler, const Vec2& v);
	//Vec2 operator-(const Vec2& v, const T scaler);
	/*
	标量与向量相乘,结果将得到一个向量,与原向量平行,但长度不同或方向相反.
	*/
	//Vec2 operator*(const T scaler, const Vec2& v);
	//template<typename T>
	//std::ostream& operator<<(std::ostream& o, const Vec2<T>& v);

	//typedef Vec2<real_type> Vector2;
	//typedef Vec2<real_type> Vector2;
}//namespace ung

#ifndef _UML_VEC2DEF_H_
#include "UMLVec2Def.h"
#endif

#endif//UML_HIERARCHICAL_COMPILE

#endif//_UML_VEC2_H_