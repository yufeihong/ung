/*!
 * \brief
 * 对象池。
 * \file UFCObjectPool.h
 *
 * \author Su Yang
 *
 * \date 2017/03/08
 */
#ifndef _UFC_OBJECTPOOL_H_
#define _UFC_OBJECTPOOL_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#ifndef _INCLUDE_BOOST_MPLIF_HPP_
#include "boost/mpl/if.hpp"
#define _INCLUDE_BOOST_MPLIF_HPP_
#endif

#ifndef _INCLUDE_BOOST_CONCEPTCHECK_HPP_
#include "boost/concept_check.hpp"
#define _INCLUDE_BOOST_CONCEPTCHECK_HPP_
#endif

#ifndef _UFC_METAFUNCTION_H_
#include "UFCMetaFunction.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 对象池。
	 * \class ObjectPool
	 *
	 * \author Su Yang
	 *
	 * \date 2017/03/10
	 *
	 * \todo
	 */
	template<typename C>
	class ObjectPool
	{
	public:
		using con_type = C;
		using key_type = typename con_type::key_type;
		using mapped_type = typename con_type::mapped_type;

		//是否是关联容器
		BOOST_CONCEPT_ASSERT((boost::AssociativeContainer<con_type>));
		//是否是键-值关联类型，即映射
		BOOST_CONCEPT_ASSERT((boost::PairAssociativeContainer<con_type>));
		//是否不允许重复键
		BOOST_CONCEPT_ASSERT((boost::UniqueAssociativeContainer<con_type>));

		//mapped_type必须是std::shared_ptr或者std::weak_ptr
		BOOST_STATIC_ASSERT((utp::isSharedPtr<mapped_type>::value || 
			utp::isWeakPtr<mapped_type>::value) 
			&& "mapped_type must be a strong reference.");

		//智能指针所引用的实际类型
		using element_type = typename utp::SmartType<mapped_type>::element_type;

		//mapped_type是否是std::shared_ptr强引用
		using is_strong_type = typename boost::mpl::if_c<
			utp::isSharedPtr<mapped_type>::value,
			boost::true_type,boost::false_type>::type;

		//如果is_strong_type为真的话，那么strong_type就是mapped_type，否则为std::shared_ptr<element_type>
		using strong_type = typename boost::conditional<
			boost::is_same<is_strong_type,boost::true_type>::value,
			mapped_type,std::shared_ptr<element_type>>::type;

		//如果is_strong_type为真的话，那么weak_type就是std::weak_ptr<element_type>，否则为mapped_type
		using weak_type = typename boost::mpl::if_c <
			boost::is_same<is_strong_type,boost::true_type>::value,
			std::weak_ptr<element_type>,mapped_type> ::type;

		//迭代器
		using iterator_type = typename con_type::iterator;
		using const_iterator_type = typename con_type::const_iterator;

	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		ObjectPool() = delete;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param String const & name
		*/
		explicit ObjectPool(String const& name) :
			mName(name.empty() ? UniqueID().toString() : name)
		{
		}

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~ObjectPool()
		{
#if UNG_DEBUGMODE
			auto name = mName;
#endif

			if (!empty())
			{
				clear();
			}
		}

		//-------------------------------------------------------------------------------

		/*!
		 * \remarks 查找对象
		 * \return 如果没有找到的话，会返回一个空的智能指针
		 * \param key_type const & k
		*/
		mapped_type find(key_type const& k)
		{
			return _find(k, is_strong_type());
		}

		/*!
		 * \remarks 存储对象
		 * \return 
		 * \param key_type const & k
		 * \param mapped_type m
		*/
		void save(key_type const& k,mapped_type m)
		{
			_save(k,m,is_strong_type());
		}

		/*!
		 * \remarks 移除对象
		 * \return 
		 * \param key_type const & k
		*/
		void remove(key_type const& k)
		{
			//根据is_strong_type为真或者为假来匹配所要调用的函数
			_remove(k,is_strong_type());
		}

		/*!
		 * \remarks 获取强引用
		 * \return 
		 * \param key_type const& k
		*/
		strong_type getStrong(key_type const& k)
		{
			return _getStrong(k, is_strong_type());
		}

		/*!
		 * \remarks 获取弱引用
		 * \return 
		 * \param key_type const& k
		*/
		weak_type getWeak(key_type const& k)
		{
			return _getWeak(k, is_strong_type());
		}

		/*!
		 * \remarks 获取对子类的强引用
		 * \return 
		 * \param key_type const& k
		*/
		template<typename sub_mapped_type>
		std::shared_ptr<sub_mapped_type> getSubStrong(key_type const& k)
		{
			//首先给定子类型和基类型应该是不同的类型
			BOOST_STATIC_ASSERT(!boost::is_same<mapped_type,sub_mapped_type>::value);
			//其次基类型和给定的子类型必须构成父子关系
			BOOST_STATIC_ASSERT(boost::is_base_of<mapped_type,sub_mapped_type>::value);

			return _getSubStrong<sub_mapped_type>(k,is_strong_type());
		}

		/*!
		 * \remarks 获取对子类的弱引用
		 * \return 
		 * \param key_type const& k
		*/
		template<typename sub_mapped_type>
		std::weak_ptr<sub_mapped_type> getSubWeak(key_type const& k)
		{
			return _getSubWeak<sub_mapped_type>(k,is_strong_type());
		}

		//-------------------------------------------------------------------------------

		/*!
		 * \remarks 对象池是否为空
		 * \return 
		*/
		bool empty() const
		{
			return mPool.empty();
		}

		/*!
		 * \remarks 对象池的大小
		 * \return 
		*/
		uint32 size() const
		{
			return mPool.size();
		}

		/*!
		 * \remarks 清空对象池
		 * \return 
		*/
		void clear()
		{
			if (mPool.empty())
			{
				return;
			}

			mPool.clear();
		}

		//-------------------------------------------------------------------------------

		/*!
		 * \remarks 迭代器
		 * \return 
		*/
		iterator_type begin()
		{
			return mPool.begin();
		}

		/*!
		 * \remarks 迭代器
		 * \return 
		*/
		iterator_type end()
		{
			return mPool.end();
		}

	private:
		mapped_type _find(key_type const& k,boost::true_type)
		{
			auto findRet = mPool.find(k);

			if (findRet == mPool.end())
			{
				//没有找到
				return std::move(mapped_type{});
			}
			else
			{
				//找到了
				return (*findRet).second;
			}
		}

		mapped_type _find(key_type const& k, boost::false_type)
		{
			//得到的是一个弱引用
			auto findRet = mPool.find(k);

			if (findRet == mPool.end())
			{
				//没有找到
				return std::move(mapped_type{});
			}
			else
			{
				//找到了
				auto wp = (*findRet).second;

				//如果已经过期，那么就移除掉(注意：这里不能调用ObjectPool::remove())
				if (wp.expired())
				{
					auto eraseRet = mPool.erase(k);
					BOOST_ASSERT(eraseRet == 1);

					//然后返回一个生成的空weak_ptr
					return std::move(mapped_type{});
				}

				//到这里，说明找到了，并且没有过期，那么可以返回
				return wp;
			}
		}

		void _save(key_type const& k,mapped_type m,boost::true_type)
		{
			//先查找给定的key是否已经存在
			if (find(k))
			{
				//抛出异常
				UNG_EXCEPTION("ObjectPool does not support multi same keys.");
			}

			mPool.insert({ k,m });
		}

		void _save(key_type const& k, mapped_type m, boost::false_type)
		{
			//先查找给定的key是否已经存在
			auto wp = find(k);

			//找到了，而且没有过期
			if (!wp.expired())
			{
				//抛出异常
				UNG_EXCEPTION("ObjectPool does not support multi same keys.");
			}

			mPool.insert({ k,m });
		}

		void _remove(key_type const& k,boost::true_type)
		{
			auto removeRet = mPool.erase(k);
			BOOST_ASSERT(removeRet == 1);
		}

		void _remove(key_type const& k, boost::false_type)
		{
			auto removeRet = mPool.erase(k);
			BOOST_ASSERT(removeRet == 1);
		}

		strong_type _getStrong(key_type const& k, boost::true_type)
		{
			auto findRet = find(k);
			BOOST_ASSERT(findRet);

			return findRet;
		}

		strong_type _getStrong(key_type const& k, boost::false_type)
		{
			//得到的是一个弱引用
			auto findRet = find(k);

			/*
			对std::weak_ptr的使用说明：
			std::weak_ptr<int> wp;			//一个empty weak_ptr

			if (wp)									//编译错误：无法转换为bool
			{
				int a = 0;
			}

			auto a = wp.expired();				//这个empty weak_ptr的expired()为true
			*/

			BOOST_ASSERT(!findRet.expired());

			return findRet.lock();
		}

		weak_type _getWeak(key_type const& k, boost::true_type)
		{
			//得到的是一个强引用
			auto findRet = find(k);
			BOOST_ASSERT(!findRet.expired());

			return findRet;
		}

		weak_type _getWeak(key_type const& k, boost::false_type)
		{
			//得到的是一个弱引用
			auto findRet = find(k);
			BOOST_ASSERT(!findRet.expired());

			return findRet;
		}

		template<typename sub_mapped_type>
		std::shared_ptr<sub_mapped_type> _getSubStrong(key_type const& k,boost::true_type)
		{
			auto findRet = find(k);

			//如果找到了
			if (findRet)
			{
				//向下类型转换(std::static_pointer_cast:Static cast to shared_ptr.)
				std::shared_ptr<sub_mapped_type> pSub(std::static_pointer_cast<sub_mapped_type>(findRet));
				BOOST_ASSERT(pSub);

				return pSub;
			}
			//如果没有找到
			else
			{
				return std::shared_ptr<sub_mapped_type>{};
			}
		}

		template<typename sub_mapped_type>
		std::shared_ptr<sub_mapped_type> _getSubStrong(key_type const& k, boost::false_type)
		{
			//得到的是弱引用
			auto findRet = find(k);

			//如果找到了
			if (!findRet.expired())
			{
				//从弱引用得到强引用
				auto strongRet = findRet.lock();
				BOOST_ASSERT(strongRet);

				std::shared_ptr<sub_mapped_type> pSub(std::static_pointer_cast<sub_mapped_type>(strongRet));
				BOOST_ASSERT(pSub);

				return pSub;
			}
			//如果没有找到
			else
			{
				return std::shared_ptr<sub_mapped_type>{};
			}
		}

		template<typename sub_mapped_type>
		std::weak_ptr<sub_mapped_type> _getSubWeak(key_type const& k, boost::true_type)
		{
			//得到的是强引用
			auto findRet = find(k);

			//如果找到了
			if (findRet)
			{
				std::weak_ptr<sub_mapped_type> pSub(std::static_pointer_cast<sub_mapped_type>(findRet));
				BOOST_ASSERT(!pSub.expired());

				return pSub;
			}
			//如果没有找到
			else
			{
				return std::weak_ptr<sub_mapped_type>{};
			}
		}

		template<typename sub_mapped_type>
		std::weak_ptr<sub_mapped_type> _getSubWeak(key_type const& k, boost::false_type)
		{
			//得到的是弱引用
			auto findRet = find(k);

			//如果找到了
			if (!findRet.expired())
			{
				//从弱引用得到强引用
				auto strongRet = findRet.lock();
				BOOST_ASSERT(strongRet);

				std::weak_ptr<sub_mapped_type> pSub(std::static_pointer_cast<sub_mapped_type>(strongRet));
				BOOST_ASSERT(!pSub.expired());

				return pSub;
			}
			//如果没有找到
			else
			{
				return std::weak_ptr<sub_mapped_type>{};
			}
		}

	private:
		String mName;
		//只有对象池可以长期持有一个对象的强引用，任何客户代码只能持有弱引用或在一个局部域中持有强引用
		con_type mPool;
	};
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_OBJECTPOOL_H_