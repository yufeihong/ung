/*!
 * \brief
 * 矩形
 * \file UPERectangle.h
 *
 * \author Su Yang
 *
 * \date 2017/05/04
 */
#ifndef _UPE_RECTANGLE_H_
#define _UPE_RECTANGLE_H_

#ifndef _UPE_CONFIG_H_
#include "UPEConfig.h"
#endif

#ifdef UPE_HIERARCHICAL_COMPILE

#ifndef _UPE_GEOMETRY_H_
#include "UPEGeometry.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 矩形
	 * \class Rectangle
	 *
	 * \author Su Yang
	 *
	 * \date 2017/05/04
	 *
	 * \todo
	 */
	class UngExport Rectangle : public Geometry
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		Rectangle() = delete;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param Vector2 const & min
		 * \param Vector2 const & max
		*/
		Rectangle(Vector2 const& min, Vector2 const& max);

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param Vector2 const & min
		 * \param real_type width
		 * \param real_type height
		*/
		Rectangle(Vector2 const& min, real_type width, real_type height);

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param real_type minx
		 * \param real_type miny
		 * \param real_type maxx
		 * \param real_type maxy
		*/
		Rectangle(real_type minx, real_type miny, real_type maxx, real_type maxy);

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~Rectangle() = default;

	private:
		Vector2 mMin;
		Vector2 mMax;
	};
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

#endif//_UPE_RECTANGLE_H_