/*!
 * \brief
 * Dynamic Linking Library
 * \file UFCDLL.h
 *
 * \author Su Yang
 *
 * \date 2016/05/15
 */
#ifndef _UFC_DLL_H_
#define _UFC_DLL_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

struct HINSTANCE__;
using hInstance = HINSTANCE__*;

#if UNG_PLATFORM == UNG_PLATFORM_WIN32
#define DLL_HANDLE hInstance
#define DLL_LOAD(p) LoadLibraryExA(p,nullptr,LOAD_WITH_ALTERED_SEARCH_PATH)
#define DLL_GETSYM(p1,p2) GetProcAddress(p1,p2)
#define DLL_UNLOAD(p1) !FreeLibrary(p1)
#endif

namespace ung
{
	/*!
	 * \brief
	 * 运行时显式载入动态链接库
	 * \class DLL
	 *
	 * \author Su Yang
	 *
	 * \date 2016/05/15
	 *
	 * \todo
	 */
	class UngExport DLL : public MemoryPool<DLL>
	{
	public:
		/*!
		 * \remarks 不允许无参构造
		 * \return 
		*/
		DLL() = delete;

		/*!
		 * \remarks 不能直接调用
		 * \return 
		 * \param const String & name
		*/
		DLL(const String& name);

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		virtual ~DLL();

		/*!
		 * \remarks 载入动态链接库
		 * \return 
		*/
		void load();

		/*!
		 * \remarks 卸载动态链接库
		 * \return 
		*/
		void unload();

		/*!
		 * \remarks 获取动态链接库的名字
		 * \return 
		*/
		const String& getName() const;

		/*!
		 * \remarks 获取给定符号的地址
		 * \return 返回一个句柄，或者nullptr
		 * \param const String & strName
		*/
		void* getSymbol(const String& strName) const noexcept;

	protected:
		String getLastLoadingError();
		String mName;
		DLL_HANDLE mHInstance;
	};
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_DLL_H_