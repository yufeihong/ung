/*!
 * \brief
 * Object
 * \file USMObject.h
 *
 * \author Su Yang
 *
 * \date 2016/11/17
 */
#ifndef _USM_OBJECT_H_
#define _USM_OBJECT_H_

#ifndef _USM_CONFIG_H_
#include "USMConfig.h"
#endif

#ifdef USM_HIERARCHICAL_COMPILE

#ifndef _UIF_IOBJECT_H_
#include "UIFIObject.h"
#endif

#ifndef _USM_SCENEMANAGER_H_
#include "USMSceneManager.h"
#endif

#ifndef _INCLUDE_TINYXML2_H_
#include "tinyxml2.h"
#define _INCLUDE_TINYXML2_H_
#endif

namespace ung
{
	class SpatialNode;

	/*!
	 * \brief
	 * Object(由一些component组合(组装)起来的东西。如果没有这些component的话，Object就是一个空盒子。)
	 * 所有的Objects都是通过工厂来创建的。
	 * 所有的Objects都是通过XML数据文件来定义的。XML文件中对各种组件进行了配置，并且设置了组件的默认值。
	 * 注意：该类为final，不可继承，也不必继承。
	 * \class Object
	 *
	 * \author Su Yang
	 *
	 * \date 2016/12/14
	 *
	 * \todo
	 */
	class UngExport Object final : public IObject
	{
		friend class ObjectFactory;

		template<SceneType ST>
		friend class SceneManager;

		friend class SceneNode;

	private:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		Object() = delete;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param StrongISceneManager smPtr 该对象所属的场景管理器
		 * \param String const & objectName
		*/
		Object(StrongISceneManagerPtr smPtr, String const& objectName = EMPTY_STRING);

	public:
		/*!
		 * \remarks 析构函数(这个类，不设计为“被继承，被改写”，因为一切的“变化，不同”都来自于组件。)
		 * 公开该接口的原因是为了让std::shared_ptr可以触发。
		 * \return 
		*/
		virtual ~Object();

	public:
		/*!
		 * \remarks 获取对象的名字
		 * \return 
		*/
		virtual String const& getName() const override;

		/*!
		 * \remarks 关联对象到场景图节点
		 * \return 
		 * \param StrongISceneNodePtr sceneNodePtr
		*/
		virtual void attachToSceneNode(StrongISceneNodePtr sceneNodePtr) override;

		/*!
		 * \remarks 对象脱离其所关联的场景图节点
		 * \return 
		 * \param bool remove true:将对象从场景节点的容器中移除
		*/
		virtual void detachFromSceneNode(bool remove = true) override;

		/*!
		 * \remarks 对象是否已经关联到场景图中的一个节点
		 * \return 
		*/
		virtual bool isAttachSceneNode() const override;

		/*!
		 * \remarks 获取对象所关联的场景图节点
		 * \return 
		*/
		virtual WeakISceneNodePtr getAttachedNode() const override;

	private:
		/*!
		 * \remarks 销毁对象，当不再需要该对象时，必须手工调用该函数，以便于触发组件的析构函数，
		 * 从而释放组件对该对象的“强引用”，否则将无法触发该对象自己的析构函数。
		 * 已经在SceneManager::destroyObject()中调用了。客户代码要销毁对象时只需通过该接口即可。
		 * \return 
		*/
		void destroy();

		/*!
		 * \remarks 设置对象所关联的场景节点(同时更新对象与空间管理器的关系)。该函数仅可被SceneNode调用
		 * \return 
		 * \param WeakISceneNodePtr sceneNodePtr
		*/
		void setAttachedSceneNode(WeakISceneNodePtr sceneNodePtr);

	public:
		/*!
		 * \remarks 在添加组件之前，进行一些初始化工作
		 * \return 
		 * \param tinyxml2::XMLElement* pRoot
		*/
		virtual void preInit(tinyxml2::XMLElement* pRoot) override;

		/*!
		 * \remarks 在添加组件之后，进行一些初始化工作
		 * \return 
		*/
		virtual void postInit() override;

		/*!
		 * \remarks 添加组件
		 * \return 
		 * \param StrongIObjectComponentPtr pComponent
		*/
		virtual void addComponent(StrongIObjectComponentPtr pComponent) override;

		/*!
		 * \remarks 获取给定类型的组件，模板参数为组件的某一个具体类，用于从基类down-cast
		 * \return 返回“弱引用”，所以在使用返回值之前必须检查其是否已经过期
		 * \param String const& componentName 组件的名字
		*/
		template <class SubComponentType>
		std::weak_ptr<SubComponentType> getComponent(String const& componentName)
		{
			return mComponents.getSubWeak<SubComponentType>(componentName);
		}

		/*!
		 * \remarks 便捷接口，获取TransformComponent的一个强引用
		 * \return 
		*/
		std::shared_ptr<IMovable> getTransformComponent();

		/*!
		 * \remarks 更新
		 * \return 
		*/
		virtual void update() override;

		/*!
		 * \remarks 调整对象在空间管理器树中的位置
		 * \return 
		*/
		virtual void adjustmentHangInSpatial() override;

		/*!
		 * \remarks 获取对象所关联的空间管理器树节点
		 * \return 
		*/
		virtual ISpatialNode* getAttachedSpatialNode() const override;

		/*!
		 * \remarks 获取对象的包围体的半径
		 * \return 
		*/
		virtual real_type getRadius() const override;

		/*!
		 * \remarks 获取对象的AABB包围体(对象模型空间)
		 * \return 
		*/
		virtual AABB const& getAABB() const override;

		/*!
		 * \remarks 获取对象的AABB包围体(世界空间)
		 * \return 
		*/
		virtual AABB getWorldAABB() override;

	private:
		String mName;
		WeakISceneNodePtr mAttachedSceneNode;
		//String:组件的名字，这里设计为：对象和组件互相持有对方的强引用
		using component_aggregates_type = STL_UNORDERED_MAP(String, StrongIObjectComponentPtr);
		ObjectPool<component_aggregates_type> mComponents;

		/*
		对象的强引用分别被各种ObjectComponent，SceneNode和SceneManager持有。

		创建对象和销毁对象的唯一方式：
		在场景管理器中进行创建和销毁。
		*/
		bool mManualCallDestroyFlag;

		//该对象所属的场景管理器
		WeakISceneManagerPtr mSceneManager;

		//该对象所关联的空间管理器节点
		ISpatialNode* mSpatialNode;

		AABB mBox;
	};
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE

#endif//_USM_OBJECT_H_