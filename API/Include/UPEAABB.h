/*!
 * \brief
 * 几何对象-AABB
 * \file UPEAABB.h
 *
 * \author Su Yang
 *
 * \date 2016/05/22
 */
#ifndef _UPE_AABB_H_
#define _UPE_AABB_H_

#ifndef _UPE_CONFIG_H_
#include "UPEConfig.h"
#endif

#ifdef UPE_HIERARCHICAL_COMPILE

#ifndef _UIF_IBOUNDINGVOLUME_H_
#include "UIFIBoundingVolume.h"
#endif

namespace ung
{
	class Sphere;
	class Plane;

	/*!
	 * \brief
	 * AABB
	 * \class AABB
	 *
	 * \author Su Yang
	 *
	 * \date 2016/05/22
	 *
	 * \todo
	 */
	class UngExport AABB : public IBoundingVolume
	{
		/*!
		 * \remarks 对AABB进行旋转
		 * \return 返回一个临时的旋转后的AABB，不要对这个AABB再次进行旋转
		 * \param AABB const & box 模型空间的AABB
		 * \param Matrix3 const & mat3
		*/
		friend UngExport AABB operator*(AABB const& box,Matrix3 const& mat3);

		/*!
		 * \remarks 对AABB进行平移和旋转
		 * \return 返回一个临时的变换后的AABB，不要对这个AABB再次进行旋转
		 * \param AABB const & box 模型空间的AABB
		 * \param Matrix4 const & mat4
		*/
		friend UngExport AABB operator*(AABB const& box,Matrix4 const& mat4);

		/*!
		 * \remarks 对AABB进行旋转
		 * \return 返回一个临时的旋转后的AABB，不要对这个AABB再次进行旋转
		 * \param AABB const & box 模型空间的AABB
		 * \param Quaternion const & rot
		*/
		friend UngExport AABB operator*(AABB const& box,Quaternion const& rot);

		/*!
		 * \remarks 输出AABB数据
		 * \return 
		 * \param std::ostream & o
		 * \param const AABB & aabb
		*/
		friend UngExport std::ostream& operator<<(std::ostream& o, const AABB& aabb);

	public:
		/*!
		 * \remarks 默认构造函数(构造一个空的AABB)
		 * \return 
		*/
		AABB();

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		virtual ~AABB();

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param const Vector3 & min
		 * \param const Vector3 & max
		*/
		AABB(const Vector3& min, const Vector3& max);

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param real_type minx
		 * \param real_type miny
		 * \param real_type minz
		 * \param real_type maxx
		 * \param real_type maxy
		 * \param real_type maxz
		*/
		AABB(real_type minx, real_type miny, real_type minz, real_type maxx, real_type maxy, real_type maxz);

		/*!
		 * \remarks 拷贝构造函数
		 * \return 
		 * \param const AABB & box
		*/
		AABB(const AABB& box);

		/*!
		 * \remarks 拷贝赋值运算符
		 * \return 
		 * \param const AABB & box
		*/
		AABB& operator=(const AABB& box);

		/*!
		 * \remarks 移动构造函数
		 * \return 
		 * \param AABB & & box
		*/
		AABB(AABB&& box) noexcept;

		/*!
		 * \remarks 移动赋值运算符
		 * \return 
		 * \param AABB & & box
		*/
		AABB& operator=(AABB&& box) noexcept;

		/*!
		 * \remarks 设置为空
		 * \return 
		*/
		void setEmpty();

		/*!
		 * \remarks 是否为空
		 * \return 
		*/
		bool isEmpty() const;

		/*!
		 * \remarks 获取min
		 * \return 
		*/
		const Vector3& getMin() const;

		/*!
		 * \remarks 获取min
		 * \return 
		*/
		Vector3& getMin();

		/*!
		 * \remarks 获取max
		 * \return 
		*/
		const Vector3& getMax() const;

		/*!
		 * \remarks 获取max
		 * \return 
		*/
		Vector3& getMax();

		/*!
		 * \remarks 获取中心
		 * \return 
		*/
		Vector3 getCenter() const;

		/*!
		 * \remarks 获取size
		 * \return 
		*/
		Vector3 getSize() const;

		/*!
		 * \remarks 获取一半size
		 * \return 
		*/
		Vector3 getHalfSize() const;

		/*!
		 * \remarks 获取半径
		 * \return 
		*/
		real_type getRadius() const;

		/*!
		 * \remarks 设置min
		 * \return 
		 * \param const Vector3 & min
		*/
		void setMin(const Vector3& min);

		/*!
		 * \remarks 设置min
		 * \return 
		 * \param real_type minx
		 * \param real_type miny
		 * \param real_type minz
		*/
		void setMin(real_type minx, real_type miny, real_type minz);

		/*!
		 * \remarks 设置max
		 * \return 
		 * \param const Vector3 & max
		*/
		void setMax(const Vector3& max);

		/*!
		 * \remarks 设置max
		 * \return 
		 * \param real_type maxx
		 * \param real_type maxy
		 * \param real_type maxz
		*/
		void setMax(real_type maxx, real_type maxy, real_type maxz);

		/*!
		 * \remarks 获取参数所指定的点
		 * (0:+++,1:++-,2:+-+,3:+--,4:-++,5:-+-,6:--+,7:---)
		 * \return 
		 * \param uint32 index
		*/
		Vector3 getIndexedCorner(uint32 index) const;

		/*!
		 * \remarks 缩放(会累计)
		 * \return 
		 * \param const Vector3 & s
		*/
		void scale(const Vector3& s);

		/*!
		 * \remarks 把参数AABB对象给融合到当前AABB对象中.
		 * \return 
		 * \param const AABB & obj
		*/
		void merge(const AABB& obj);

		/*!
		 * \remarks 融合一个点.
		 * \return 
		 * \param const Vector3 & point
		*/
		void merge(const Vector3& point);

		/*!
		 * \remarks 检测给定AABB是否在包围盒里面.
		 * \return 
		 * \param const AABB & other
		*/
		bool contain(const AABB& other) const;

		/*!
		 * \remarks 判断是否相等
		 * \return 
		 * \param const AABB & rhs
		*/
		bool operator==(const AABB& rhs) const;

		/*!
		 * \remarks 判断是否不等
		 * \return 
		 * \param const AABB & rhs
		*/
		bool operator!= (const AABB& rhs) const;

	private:
		Vector3 mMin = Vector3(0.0, 0.0, 0.0);
		Vector3 mMax{ 0.0, 0.0, 0.0 };

	public:
#if UNG_DEBUGMODE
		bool mRotated = false;
#endif
	};

	UngExport AABB operator*(AABB const& box,Matrix3 const& mat3);

	UngExport AABB operator*(AABB const& box,Matrix4 const& mat4);

	UngExport AABB operator*(AABB const& box,Quaternion const& rot);

	UngExport std::ostream& operator<<(std::ostream& o, const AABB& aabb);
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

#endif//_UPE_AABB_H_