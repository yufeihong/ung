/*!
 * \brief
 * 插件管理器
 * \file UFCPluginManager.h
 *
 * \author Su Yang
 *
 * \date 2016/05/17
 */
#ifndef _UFC_PLUGINMANAGER_H_
#define _UFC_PLUGINMANAGER_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#ifndef _UFC_CHOOSECONTAINER_H_
#include "UFCChooseContainer.h"
#endif

namespace ung
{
	class Plugin;
	class DLL;

	/*!
	 * \brief
	 * 插件管理器
	 * \class PluginManager
	 *
	 * \author Su Yang
	 *
	 * \date 2016/05/17
	 *
	 * \todo
	 */
	class UngExport PluginManager : public Singleton<PluginManager>
	{
	public:
		/*!
		 * \remarks 构造函数
		 * \return 
		*/
		PluginManager();

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~PluginManager();

		/*!
		 * \remarks load参数所给定的插件
		 * \return 
		 * \param const String & pluginFile 不包含路径，可以不包含后缀名
		*/
		void load(const String& pluginFile);

		/*!
		 * \remarks load所有的插件
		 * \return 
		 * \param const String & pluginsfile 不包含路径
		*/
		void loadAll(const String& pluginsFile);

		/*!
		 * \remarks unload所有的插件
		 * \return 
		*/
		void unloadAll();

		/*!
		 * \remarks 安装
		 * \return 
		 * \param Plugin * plugin
		*/
		void install(Plugin* plugin);

		/*!
		 * \remarks 卸载
		 * \return 
		 * \param Plugin * plugin
		*/
		void uninstall(Plugin* plugin);

	private:
		STL_VECTOR(DLL*) mDLLs;
		STL_VECTOR(Plugin*) mPlugins;
	};
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_PLUGINMANAGER_H_