/*!
 * \brief
 * 资源池。
 * \file URMResourcePool.h
 *
 * \author Su Yang
 *
 * \date 2016/11/17
 */
#ifndef _URM_RESOURCEPOOL_H_
#define _URM_RESOURCEPOOL_H_

#ifndef _URM_CONFIG_H_
#include "URMConfig.h"
#endif

#ifdef URM_HIERARCHICAL_COMPILE

namespace ung
{
	class Resource;

	/*!
	 * \brief
	 * 资源池。
	 * \class ResourcePool
	 *
	 * \author Su Yang
	 *
	 * \date 2016/11/17
	 *
	 * \todo
	 */
	class ResourcePool : public Singleton<ResourcePool>
	{
		using pointer = std::shared_ptr<Resource>;

	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		ResourcePool();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~ResourcePool();

		/*!
		 * \remarks 把一个Resource给放入到pool中
		 * \return 
		 * \param String const & fullName
		 * \param pointer ptr
		*/
		void put(String const& fullName, pointer ptr);

		/*!
		 * \remarks 根据full name来获取一个Resource的指针
		 * \return 
		 * \param String const& fullName
		*/
		pointer take(String const& fullName);

		/*!
		 * \remarks 从池中移除给定Resource
		 * \return 
		 * \param String const & fullName
		*/
		void erase(String const& fullName);

		/*!
		 * \remarks 从池中移除给定Resource
		 * \return 
		 * \param pointer ptr
		*/
		void erase(pointer ptr);

		/*!
		 * \remarks 获取当前pool中的Resource数量
		 * \return 
		*/
		uint32 getCount() const;

	private:
		//String:包含路径的full name
		STL_UNORDERED_MAP(String, pointer) mPool;
	};
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

#endif//_URM_RESOURCEPOOL_H_

//least recently used(LRU)