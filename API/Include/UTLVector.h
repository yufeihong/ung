/*!
 * \brief
 * UTL的Vector容器。
 * \file UTLVector.h
 *
 * \author Su Yang
 *
 * \date 2016/01/28
 */
#ifndef _UNG_UTL_VECTOR_H_
#define _UNG_UTL_VECTOR_H_

#ifndef _UNG_UTL_CONFIG_H_
#include "UTLConfig.h"
#endif

#ifndef _UNG_UTL_ALLOCATOR_H_
#include "UTLAllocator.h"
#endif

#ifndef _UNG_UTL_VECTORITERATOR_H_
#include "UTLVectorIterator.h"
#endif

namespace ung
{
	namespace utl
	{
		/*!
		 * \brief
		 * Vector容器的内存分配器包装。
		 * \class VectorAllocatorHolder
		 *
		 * \author Su Yang
		 *
		 * \date 2016/01/28
		 *
		 * \todo
		 */
		template<typename Alloc>
		class VectorAllocatorHolder : public Alloc
		{
		private:
			/*
			This macro marks a type as movable but not copyable, disabling copy construction and assignment. The user will need to write a 
			move constructor/assignment as explained in the documentation to fully write a movable but not copyable class.
			*/
			//BOOST_MOVABLE_BUT_NOT_COPYABLE(VectorAllocatorHolder)

		public:
			using allocator_type = typename Alloc::allocator_type;
			using value_type = typename Alloc::value_type;
			using pointer = typename Alloc::pointer;
			using size_type = typename Alloc::size_type;

			/*!
			 * \remarks 默认构造函数
			 * \return  
			*/
			VectorAllocatorHolder() noexcept(boost::has_nothrow_default_constructor<Alloc>::value);
			/*!
			 * \remarks 构造函数
			 * \return  
			 * \param size_type initialSize 初始空间大小
			*/
			explicit VectorAllocatorHolder(size_type initialSize);
			/*!
			 * \remarks 拷贝构造函数
			 * \return  
			 * \param VectorAllocatorHolder<Alloc> const & right 
			*/
			VectorAllocatorHolder(VectorAllocatorHolder<Alloc> const& right);
			/*!
			 * \remarks 拷贝赋值运算符
			 * \return VectorAllocatorHolder<Alloc>&
			 * \param VectorAllocatorHolder<Alloc> const & right 
			*/
			VectorAllocatorHolder<Alloc>& operator=(VectorAllocatorHolder<Alloc> const& right);
			/*!
			 * \remarks 移动构造函数
			 * \return 
			 * \param VectorAllocatorHolder<Alloc> && right 
			*/
			VectorAllocatorHolder(VectorAllocatorHolder<Alloc>&& right);
			/*!
			 * \remarks 移动赋值运算符
			 * \return VectorAllocatorHolder<Alloc>&
			 * \param VectorAllocatorHolder<Alloc> && right 
			*/
			VectorAllocatorHolder<Alloc>& operator=(VectorAllocatorHolder<Alloc>&& right);
			/*!
			 * \remarks 析构函数
			 * \return 
			*/
			~VectorAllocatorHolder() noexcept;

			/*!
			 * \remarks 调整空间大小
			 * \return void
			 * \param size_type newCapacity 
			*/
			void adjustCapacity(size_type newCapacity);
			/*!
			 * \remarks 缩小空间大小至元素的数量
			 * \return void
			*/
			void shrink();

			/*!
			 * \remarks 获取指向容器首元素的指针
			 * \return pointer const&
			*/
			pointer const& start() const;
			/*!
			 * \remarks 获取容器中元素的数量
			 * \return size_type const&
			*/
			size_type const& size() const;
			/*!
			 * \remarks 增加元素的数量
			 * \return void
			 * \param size_type step 
			*/
			void incrementSize(size_type step = 1);
			/*!
			 * \remarks 减少元素的数量
			 * \return void
			 * \param size_type step 
			*/
			void decrementSize(size_type step = 1);
			/*!
			 * \remarks 获取容器的空间大小
			 * \return size_type const&
			*/
			size_type const& capacity() const;

			/*!
			 * \remarks 交换两个容器的内存空间
			 * \return void
			 * \param VectorAllocatorHolder<Alloc> & right 
			*/
			void swap(VectorAllocatorHolder<Alloc>& right);

		private:
			pointer mStart;
			size_type mSize;
			size_type mCapacity;
		};

		/*!
		 * \brief
		 * UTL的Vector容器。
		 * \class Vector
		 *
		 * \author Su Yang
		 *
		 * \date 2016/01/28
		 *
		 * \todo
		 */
		template<typename T,typename Alloc = Allocator<T>>
		class Vector
		{
			friend void swap(Vector<T, Alloc>& left, Vector<T, Alloc>& right);
			friend bool operator==(const Vector<T, Alloc>& left, const Vector<T, Alloc>& right);
			friend bool operator!=(const Vector<T, Alloc>& left, const Vector<T, Alloc>& right);
			friend bool operator<(const Vector<T, Alloc>& left, const Vector<T, Alloc>& right);
			friend bool operator>(const Vector<T, Alloc>& left, const Vector<T, Alloc>& right);
			friend bool operator<=(const Vector<T, Alloc>& left, const Vector<T, Alloc>& right);
			friend bool operator>=(const Vector<T, Alloc>& left, const Vector<T, Alloc>& right);

		public:
			/*
			types:
			*/
			using value_type = typename Alloc::value_type;
			using allocator_type = typename Alloc::allocator_type;
			using allocator_holder = VectorAllocatorHolder<Alloc>;
			using pointer = typename Alloc::pointer;
			using const_pointer = typename Alloc::const_pointer;
			using reference = typename Alloc::reference;
			using const_reference = typename Alloc::const_reference;
			using reference_r = typename Alloc::reference_r;
			using size_type = typename Alloc::size_type;
			using difference_type = typename Alloc::difference_type;
			using self_type = Vector<value_type,Alloc>;
			using self_reference = typename boost::add_lvalue_reference<self_type>::type;
			using self_const_reference = typename boost::add_const<self_reference>::type;
			using self_reference_r = typename boost::add_rvalue_reference<self_type>::type;
			typedef VectorIterator<value_type> iterator;
			typedef VectorConstIterator<value_type> const_iterator;									//value_type是const的，不能被修改。迭代器可以移动。
			typedef std::reverse_iterator<iterator> reverse_iterator;
			typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

			/*
			construct,copy,destroy:
			*/
			/*!
			 * \remarks 默认构造函数
			 * \return 
			*/
			Vector() noexcept;
			/*!
			 * \remarks 构造函数
			 * \return 
			 * \param size_type initialSize 初始大小
			*/
			explicit Vector(size_type initialSize);
			/*!
			 * \remarks 构造函数
			 * \return 
			 * \param size_type initialSize 初始大小
			 * \param const_reference val 初始元素值
			*/
			Vector(size_type initialSize, const_reference val);
			/*!
			 * \remarks 拷贝构造函数
			 * \return 
			 * \param self_const_reference right 
			*/
			Vector(self_const_reference right);
			/*!
			 * \remarks 拷贝赋值运算符
			 * \return self_reference
			 * \param self_const_reference right 
			*/
			self_reference operator=(self_const_reference right);
			/*!
			 * \remarks 移动构造函数
			 * \return 
			 * \param self_reference_r right 
			*/
			Vector(self_reference_r right);
			/*!
			 * \remarks 移动赋值运算符
			 * \return self_reference
			 * \param self_reference_r right 
			*/
			self_reference operator=(self_reference_r right);
			BOOST_TTI_HAS_TYPE(iterator_category)
			/*!
			 * \remarks 构造函数
			 * \return 
			 * \param iteratorT first 指向区间首元素的迭代器
			 * \param iteratorT last 指向区间尾后位置的迭代器
			 * \param typename boost::enable_if<has_type_iterator_category<iteratorT>>::type * 用于函数重载裁决
			*/
			template<typename iteratorT>
			Vector(iteratorT first, iteratorT last,typename boost::enable_if<has_type_iterator_category<iteratorT>>::type* = 0);
			/*!
			 * \remarks 构造函数
			 * \return 
			 * \param std::initializer_list<value_type> il 初始化列表
			*/
			Vector(std::initializer_list<value_type> il);
			/*!
			 * \remarks 赋值运算符
			 * \return self_reference
			 * \param std::initializer_list<value_type> il 初始化列表
			*/
			self_reference operator=(std::initializer_list<value_type> il);
			/*!
			 * \remarks 赋值
			 * \return void
			 * \param iteratorT first 指向区间首元素的迭代器
			 * \param iteratorT last 指向区间尾后位置的迭代器
			 * \param typename boost::enable_if<has_type_iterator_category<iteratorT>>::type * 用于函数重载裁决
			*/
			template <typename iteratorT>
			void assign(iteratorT first, iteratorT last, typename boost::enable_if<has_type_iterator_category<iteratorT>>::type* = 0);
			/*!
			 * \remarks 同上
			 * \return void
			 * \param std::initializer_list<value_type> il 初始化列表
			*/
			void assign(std::initializer_list<value_type> il);
			/*!
			 * \remarks 同上
			 * \return void
			 * \param size_type count 数量
			 * \param const_reference val 常量引用
			*/
			void assign(size_type count, const_reference val);

			/*
			iterators:
			*/
			/*!
			 * \remarks 获取指向容器首元素的迭代器
			 * \return iterator
			*/
			iterator begin();
			/*!
			 * \remarks 同上
			 * \return const_iterator 返回常量迭代器，常量对象调用
			*/
			const_iterator begin() const;
			/*!
			 * \remarks 同上
			 * \return const_iterator 返回常量迭代器
			*/
			const_iterator cbegin() const;
			/*!
			 * \remarks 返回指向容器尾后位置的迭代器
			 * \return reverse_iterator
			*/
			reverse_iterator rbegin();
			/*!
			 * \remarks 同上
			 * \return const_reverse_iterator 返回常量迭代器，常量对象调用
			*/
			const_reverse_iterator rbegin() const;
			/*!
			 * \remarks 同上
			 * \return const_reverse_iterator 返回常量迭代器
			*/
			const_reverse_iterator crbegin() const;
			/*!
			 * \remarks 返回指向容器尾后位置的迭代器
			 * \return iterator
			*/
			iterator end();
			/*!
			 * \remarks 同上
			 * \return const_iterator 返回常量迭代器，常量对象调用
			*/
			const_iterator end() const;
			/*!
			 * \remarks 同上
			 * \return const_iterator 返回常量迭代器
			*/
			const_iterator cend() const;
			/*!
			 * \remarks 获取指向容器首元素的迭代器
			 * \return reverse_iterator
			*/
			reverse_iterator rend();
			/*!
			 * \remarks 同上
			 * \return const_reverse_iterator 返回常量迭代器，常量对象调用
			*/
			const_reverse_iterator rend() const;
			/*!
			 * \remarks 同上
			 * \return const_reverse_iterator 返回常量迭代器
			*/
			const_reverse_iterator crend() const;
			/*
			间接迭代器：
			boost::indirect_iterator把一个迭代器进行适配，在执行operator*时再多执行一次解引用操作（即再执行一次operator*），适合用于查看保存指针，
			智能指针或者迭代器的容器。也仅能用于元素是指针的容器。
			工厂函数make_indirect_iterator()。
			*/
			/*!
			 * \remarks 在执行operator*时再多执行一次解引用操作（即再执行一次operator*），仅能用于元素是指针的容器
			 * \return auto
			*/
			auto ibegin() const ->decltype(boost::make_indirect_iterator(begin().getPointer()))
			{
				return boost::make_indirect_iterator(begin().getPointer());
			}

			/*!
			 * \remarks 同上
			 * \return auto
			*/
			auto iend() const ->decltype(boost::make_indirect_iterator(end().getPointer()))
			{
				return boost::make_indirect_iterator(end().getPointer());
			}

			/*
			无法在编译器从shared_ptr中获取到其所引用的类型。
			auto cibegin(typename boost::enable_if <boost::is_pointer<value_type>,void> ::type* = 0) const
			auto cibegin() const
				->decltype(boost::make_indirect_iterator<typename remove_shared_pointer<value_type>::type const, value_type const*>
					(cbegin().getPointer()))
			{
				return boost::make_indirect_iterator<typename remove_shared_pointer<value_type>::type const, value_type const*>
					(cbegin().getPointer());
			}

			template<class = typename boost::enable_if<boost::is_base_of<std::_Ptr_base<typename value_type::element_type>,
				value_type> ::value, void>::type>
				auto ciend() const ->decltype(boost::make_indirect_iterator<value_type::element_type const, value_type const*>(cend().getPointer()))
			{
				return boost::make_indirect_iterator<value_type::element_type const, value_type const*>(cend().getPointer());
			}
			*/

			/*
			capacity:
			*/
			/*!
			 * \remarks 获取容器是否为空
			 * \return bool
			*/
			bool empty() const;
			/*!
			 * \remarks 获取容器中元素的数量
			 * \return size_type
			*/
			size_type size() const;
			/*!
			 * \remarks 重新设置容器中元素的数量
			 * \return void
			 * \param size_type newSize 
			*/
			void resize(size_type newSize);
			/*!
			 * \remarks 不重新分配内存空间的话，容器可以保存多少元素
			 * \return size_type
			*/
			size_type capacity() const;
			/*!
			 * \remarks 分配至少能容纳newSize个元素的内存空间
			 * \return void
			 * \param size_type newSize 
			*/
			void reserve(size_type newSize);
			/*!
			 * \remarks 将capacity()减少为与size()相同大小
			 * \return void
			*/
			void shrink_to_fit();

			/*
			element access:
			*/
			/*!
			 * \remarks 获取容器首元素的引用
			 * \return reference
			*/
			reference front();
			/*!
			 * \remarks 获取容器首元素的常量引用，常量对象调用
			 * \return const_reference
			*/
			const_reference front() const;
			/*!
			 * \remarks 获取容器尾元素的引用，不是尾后
			 * \return reference
			*/
			reference back();
			/*!
			 * \remarks 同上
			 * \return const_reference 返回常量引用，常量对象调用
			*/
			const_reference back() const;
			/*!
			 * \remarks 重载[]运算符
			 * \return reference 返回容器元素的引用
			 * \param size_type pos 
			*/
			reference operator[](size_type pos);

			/*!
			 * \remarks 同上
			 * \return const_reference 返回容器元素的常量引用，常量对象调用
			 * \param size_type pos 
			*/
			const_reference operator[](size_type pos) const;
			/*!
			 * \remarks 获取给定位置元素的引用
			 * \return reference
			 * \param size_type pos 
			*/
			reference at(size_type pos);
			/*!
			 * \remarks 同上
			 * \return const_reference 返回元素的常量引用，常量对象调用
			 * \param size_type pos 
			*/
			const_reference at(size_type pos) const;

			/*
			data access:
			*/
			/*!
			 * \remarks 获取指向容器首元素的指针
			 * \return pointer
			*/
			pointer data();
			/*!
			 * \remarks 同上
			 * \return const_pointer 返回常量指针，常量对象调用
			*/
			const_pointer data() const;

			/*
			modifiers:
			*/
			/*!
			 * \remarks 在容器的尾后位置通过args来创建一个元素
			 * \return void
			 * \param Arguments & & ... args 
			*/
			template<typename... Arguments>
			void emplace_back(Arguments&&... args);
			/*!
			 * \remarks 在指定位置前面通过args来创建一个元素
			 * \return iterator
			 * \param const_iterator where 
			 * \param Arguments && ... args 
			*/
			template<typename... Arguments>
			iterator emplace(const_iterator where, Arguments&&... args);

			/*!
			 * \remarks 在给定位置前面插入一定数量一定值的元素
			 * \return iterator
			 * \param const_iterator where 
			 * \param size_type count 
			 * \param const_reference val 
			*/
			iterator insert(const_iterator where, size_type count, const_reference val);
			/*!
			 * \remarks 在给定位置前面插入一个元素
			 * \return iterator
			 * \param const_iterator where 
			 * \param const_reference val 
			*/
			iterator insert(const_iterator where, const_reference val);
			/*!
			 * \remarks 同上
			 * \return iterator
			 * \param const_iterator where 
			 * \param reference_r val 右值引用
			*/
			iterator insert(const_iterator where, reference_r val);
			/*!
			 * \remarks 把区间所表示的元素，插入到给定位置的前面
			 * \return ::type 用于函数重载裁决
			 * \param const_iterator where 
			 * \param iteratorT first 
			 * \param iteratorT last 
			*/
			template<typename iteratorT>
			typename boost::enable_if<has_type_iterator_category<iteratorT>, iteratorT>::type
			insert(const_iterator where, iteratorT first, iteratorT last);
			/*!
			 * \remarks 把初始化列表中的元素插入到给定位置的前面
			 * \return iterator
			 * \param const_iterator where 
			 * \param std::initializer_list<value_type> il 
			*/
			iterator insert(const_iterator where, std::initializer_list<value_type> il);

			/*!
			 * \remarks 在容器的尾后位置压入一个元素
			 * \return void
			 * \param const_reference val 
			*/
			void push_back(const_reference val);
			/*!
			 * \remarks 同上
			 * \return void
			 * \param reference_r val 右值引用
			*/
			void push_back(reference_r val);
			/*!
			 * \remarks 把容器的最后一个元素给删除掉
			 * \return void
			*/
			void pop_back();

			/*!
			 * \remarks 删除迭代器所指向位置的元素
			 * \return iterator 返回迭代器指向被删除的位置
			 * \param const_iterator where 
			*/
			iterator erase(const_iterator where);
			/*!
			 * \remarks 删除容器中区间所表示范围的元素
			 * \return iterator 返回第一个被删除的元素位置
			 * \param const_iterator first 
			 * \param const_iterator last 
			*/
			iterator erase(const_iterator first, const_iterator last);

			/*!
			 * \remarks 清空容器
			 * \return void
			*/
			void clear();

			/*!
			 * \remarks 交换两个容器
			 * \return void
			 * \param self_reference right 
			*/
			void swap(self_reference right);

#if UTL_DEBUGMODE
			void printTypes()
			{
				std::cout << "value_type:" << typeid(value_type).name() << std::endl;

				std::cout << "pointer:" << typeid(pointer).name() << std::endl;
				std::cout << "const_pointer:" << typeid(const_pointer).name() << std::endl;

				std::cout << "reference:" << typeid(reference).name() << std::endl;
				std::cout << "const_reference:" << typeid(const_reference).name() << std::endl;
			}
#endif

		private:
			
			/*
			指向自定义类的指针。
			指向内置类型的指针。
			内置类型。
			*/
			void priv_resize_destructor(size_type newSize, boost::true_type);
			/*
			自定义类对象。
			智能指针。
			*/
			void priv_resize_destructor(size_type newSize, boost::false_type);
			/*
			普通指针。
			*/
			void priv_resize_delete(size_type newSize, boost::true_type);
			/*
			内置类型对象。
			*/
			void priv_resize_delete(size_type newSize, boost::false_type);

		private:
			allocator_holder mHolder;
		};
		/*!
		 * \remarks 交换两个容器
		 * \return void
		 * \param Vector<T，Alloc> & left 
		 * \param Vector<T，Alloc> & right 
		*/
		template<typename T,typename Alloc>
		void swap(Vector<T,Alloc>& left, Vector<T, Alloc>& right);

		/*!
		 * \remarks 判断两个容器是否相等
		 * \return bool
		 * \param const Vector<T，Alloc> & left 
		 * \param const Vector<T，Alloc> & right 
		*/
		template<typename T, typename Alloc>
		bool operator==(const Vector<T, Alloc>& left, const Vector<T, Alloc>& right);

		/*!
		 * \remarks 不等
		 * \return bool
		 * \param const Vector<T，Alloc> & left 
		 * \param const Vector<T，Alloc> & right 
		*/
		template<typename T,typename Alloc>
		bool operator!=(const Vector<T, Alloc>& left, const Vector<T, Alloc>& right);

		/*!
		 * \remarks 小于
		 * \return bool
		 * \param const Vector<T，Alloc> & left 
		 * \param const Vector<T，Alloc> & right 
		*/
		template<typename T,typename Alloc>
		bool operator<(const Vector<T, Alloc>& left, const Vector<T, Alloc>& right);

		/*!
		 * \remarks 大于
		 * \return bool
		 * \param const Vector<T，Alloc> & left 
		 * \param const Vector<T，Alloc> & right 
		*/
		template<typename T,typename Alloc>
		bool operator>(const Vector<T, Alloc>& left, const Vector<T, Alloc>& right);

		/*!
		 * \remarks 小于等于
		 * \return bool
		 * \param const Vector<T，Alloc> & left 
		 * \param const Vector<T，Alloc> & right 
		*/
		template<typename T,typename Alloc>
		bool operator<=(const Vector<T, Alloc>& left, const Vector<T, Alloc>& right);

		/*!
		 * \remarks 大于等于
		 * \return bool
		 * \param const Vector<T，Alloc> & left 
		 * \param const Vector<T，Alloc> & right 
		*/
		template<typename T,typename Alloc>
		bool operator>=(const Vector<T, Alloc>& left, const Vector<T, Alloc>& right);
	}//namespace utl
}//namespace ung

#endif//_UNG_UTL_VECTOR_H_

#include "UTLVectorDef.h"

/*
函数与参数：
int func(int a,int b)
{
	return a + b;
}
c = func(2,3);
a,b是函数func的形参(formal parameter)，2,3是分别与a，b对应的实参（actual parameter）。
形参a，b实际上是传值参数（value parameter）。在运行时，函数func执行前，把实参复制给形参。复制过程是由形参类型的复制构造函数（copy constructor）
来完成的。
当函数运行结束时，形参类型的析构函数（destructor）负责释放形式参数。

template<typename T>
T func2(T const& a,T const& b)
{
	return a + b;
}
参数类型是一个变量，它的值由编译器来决定。
常量引用（const reference），这种模式指明的引用参数不能被函数修改。

返回值：
一个函数可以返回一个值、一个引用或一个常量引用。

异常：
异常是表示程序出现错误的信息。比如除数为0，这是一个错误，对这个错误，虽然C++检查不出来，但是硬件会检查出来，并抛出一个异常。
catch(...)
{
}
捕捉所有异常，不管是什么类型。
如果一个异常被抛出，那么try块的正常运行停止，程序进入第一个能够捕捉到这种异常类型的catch块。在这个catch块执行完之后，其它的catch块就被忽略了。如果
没有一个catch块能够与抛出的异常类型相对应，那么异常就会跨越嵌入在try块里的层次结构，寻找在层次结构中能够处理这个异常的第一个catch块。如果该异常没
有被任何catch块捕捉，那么程序非正常停止。

二维数组：
当形参是一个二维数组时，必须指定第二维的大小。

h.setValue(42);
h是调用setValue的调用对象。在编写函数setValue的代码时，我们有办法访问调用该函数的对象，因此，不需要把调用对象的名称放入参数表中。

int getValue() const;
关键字const指明这些函数不会改变调用对象的值。这种函数称为常量函数（constant function）。
this指向调用对象，*this就是调用对象。

保护性类成员：
保护性成员类似于私有成员，区别在于派生函数可以访问基类的保护性成员。
优秀的软件工程设计原则要求数据成员是私有的。通过成员函数，派生类可以间接访问基类的私有数据成员。

定义一个异常类：
class illegalParameterValue
{
public:
	illegalParameterValue()
	{
		message("something");
	}

	illegalParameterValue(char* mes)
	{
		message = mes;
	}

	void outputMessage()
	{
		cout << message << endl;
	}

private:
	string message;
};

递归函数：
递归函数（recursive function）或方法自己调用自己。在直接递归（direct recursion）中，递归函数f的代码包含了调用f的语句，而在间接递归（indirect recursion）
中，递归函数f调用了函数g，g又调用了函数h，如此进行下去，直至又调用了f。
递归函数有一个基础部分和一个递归部分。递归部分的每一次应用都使我们更接近基础部分。
C++的递归函数：
正确的递归函数必须包含基础部分。每一次递归调用，其参数值都比上一次的参数值要小，从而重复调用递归函数使参数值达到基础部分的值。
计算n!的递归函数：
基础部分是n <= 1。
int factorial(int n)
{
	if(n <= 1)
	{
		return 1;
	}
	else
	{
		return n * factorial(n - 1)
	}
}
考虑factorial(2)的计算过程。为了计算在else语句中的表达式2 * factorial(1)，将factorial(2)的计算挂起，然后调用factorial(1)。当factorial(2)的计算被挂起
时，程序状态（即局部变量和传值形参的值、与引用形参绑定的值、代码执行位置等）被保留在递归栈中。当factorial(1)的计算结束时，程序状态恢复。factorial(1)
的返回值是1。接下来继续计算factorial(2)，即计算2 * 1。
n个元素的排列个数是n!。

标准模板库：
C++标准模板库（STL）是一个容器、适配器、迭代器、函数对象（也称仿函数）和算法的集合。

黑盒法（black box method）考查的是程序功能，而不是实际的代码。
白盒法（white box method）考查的是程序代码。

程序性能分析：
我们用程序性能（program performance）来指一个程序对内存和时间的需求。
一个程序的空间复杂度（space complexity）是指该程序的运行所需内存的大小。
时间复杂度（time complexity）是指运行程序所需要的时间。

程序所需要的空间主要由一下部分构成：
1:指令空间。
指令空间是指编译之后的程序指令所需要的存储空间。
2：数据空间。
数据空间是指所有常量和变量值所需要的存储空间。它由两个部分构成：
a，常量和简单变量所需要的存储空间。
b，动态数组和动态类实例等动态对象所需要的空间。
3:环境栈空间。
环境栈用来保存暂停的函数和方法在恢复运行时所需要的信息。例如，函数f调用了函数g，那么我们至少要保存在函数g结束时函数f继续执行的指令地址。

每当一个函数被调用时，下面的数据将被保存在环境栈中：
1：返回地址。
2：正在调用的函数的所有局部变量的值以及形式参数的值。

渐进记法：
1:常量。
longn:对数。
n:线性。
nlogn:n倍对数。
n^2:平方。
n^3:立方。
2^n:指数。
n!:阶乘。
1 < logn < n < nlogn < n^2 < n^3 < 2^n < n!

性能测量：
计算机内存是有等级之分的，例如，L1高速缓存、L2高速缓存和主存。
简单计算机模型：
ALU:算术和逻辑单元。
二级缓存的大小不足1MB，一级缓存的大小是几十KB，寄存器的数量在8和32之间。程序开始运行时，所有数据都在主存。
要执行一个算术运算，例如加法，首先把相加的数据从主存移到寄存器，然后把寄存器的数据相加，最后把结果写入主存。
我们把寄存器的数据相加所需要的时间作为一个周期。把一级缓存的数据送到一个寄存器所需要的时间是两个周期。如果需要的数据没有在一级缓存，而是在二级缓存，
即一级缓存未命中，那么把需要的数据从二级缓存送到一级缓存和寄存器需要10个周期。当需要的数据没有在二级缓存，即二级缓存未命中时，把需要的数据从主存
复制到二级缓存、一级缓存和寄存器需要100个周期。我们把写操作，甚至向主存的写操作，算作一个周期，因为不需要等到写操作完成之后再进行下一个操作。

缓存未命中对运行时间的影响：
a = b + c;
load b;
load c;
add;
store c;
load操作把数据送到寄存器，store操作把相加后的结果送到主存。add和store操作共需要两个周期。两个load操作可能需要4个周期至200个周期不等，这取决于数
据是否在缓存中，即缓存是否命中。因此，语句a = b + c所需要的总时间从6个周期到202个周期不等。
为了减少缓存未命中的数量，从而减少程序的运行时间，计算机采用了一些策略，比如，把最近需要处理的数据预载到缓存中，当出现一个缓存未命中时，把需要的数
据和相邻字节中的数据装入缓存中。当连续的计算机操作使用的是相邻字节的数据时，这个策略很有效。

数组：
如果我们总是按一个乘法因子来增加数组长度，那么实施一系列线性表的操作所需要的时间与不用改变数组长度时相比，至多增加一个常数因子。

一个迭代器（iterator）是一个指针，指向对象的一个元素。

vector:
STL提供了一个基于数组的类vector。
*/

/*
指定了操作的项被称为抽象数据类型（ADT）。

数据及其相关操作的结合被称为数据封装。

消息传递相当于传统语言中的函数调用。

公有继承意味着基类的公有成员和受保护成员，在派生类中依然分别是公有成员和受保护成员。
在受保护的继承中，基类的公有成员和受保护成员在派生类中都变成了受保护成员。
对于私有继承，基类的公有成员和受保护成员在派生类中都变成了私有成员。

class Base
{};
class Derived1Level1 : public virtual Base
{};
class Derived2Level1 : public virtual Base
{};
class Derived3Level2 : public Derived1Level1,public Derived2Level1
{};
virtual,意味着Derived3Level2仅包含Base里成员函数的一个副本。

在OOP中，多态性指的是用同样的函数名称表示多个函数，而这些函数是不同对象的成员。

由于内存位置是连续的，所以向量中的元素可以随机访问。
*/

/*
enable_if:
它使用SFINAE原则，可以在编译期启用或禁用特定的泛型代码。
enable_if主要用来解决函数模板或类模板的重载解析问题，允许函数模板或类模板仅针对某些特定类型有效，即依据条件启用或禁用某些特化形式。
enable_if<>的类摘要：
template<bool B,typename T = void>
struct enable_if_c
{
	typedef T type;
};
template<typename T>
struct enable_if_c<false,T>			//对false特化，无::type返回
{
};
template<typename Cond,typename T = void>
struct enable_if : public enable_if_c<Cond::value,T>			//计算元函数Cond
{
};
enable_if<>使用元函数转发技术，计算条件元函数Cond的值，再交给enable_if_c<>。如果条件为true，那么enable_if<>/enable_if_c<>将返回类型T，否则
enable_if<>/enable_if_c<>将是一个无返回的元函数。
disable_if<>与enable_if<>相似，但在语义上是反义词，即条件Cond成立时无返回。
处理函数重载时编译器要构造所有同名函数的集合，再从中选择一个最恰当的函数。当存在函数模板时，如果函数模板可以被模板实参推演实例化，那么它就是一个
候选函数。反之，如果某个参数或返回值类型无效导致推演失败无法实例化，那么这个函数模板则不是候选函数，编译器也不会认为是一个编译错误。这就是著名的
SFINAE原则，即“替代失败不是错误”（substitution failure is not an error）。
应用于函数模板：
作为模板推演时的控制条件，enable_if通常需要配合type_traits或者mpl使用来检查类型T是否满足某些条件，可以放在函数模板参数列表的最末尾用作缺省参数，
或者是用作返回值，两种形式的效果是相同的，但有的时候只能使用一种形式，比如用于构造函数和析构函数时没有返回值，用于操作符重载时不能变动参数的数量。
下面的代码示范了enable_if的用法，这个print()函数仅在类型是整数时才生效：
template<typename T>
T print(T x,typename enable_if<is_integral<T>>::type* = 0)			//整数时启用
{
	cout << "int:" << x << endl;
	return x;
}
代码中enable_if作为函数print()的缺省参数出现，声明了一个无名指针参数，默认值是空指针。这样，当编译器进行模板实例化时，如果T不是整数，那么enable_if<>
将不会返回任何类型，导致实例化失败，从而使这个print()被禁用。
enable_if的返回值用法如下，效果与缺省参数的形式相同：
template<typename T>
typename enable_if<is_integral<T>,T>::type print(T x)
{
	...
}
注意在这里我们向enable_if<>传递了第二个元参数T，因为如果不这么做的话enable_if<>的返回值将是void，不符合函数的签名，这与enable_if<>的缺省参数
用法略微有些不同(当然enable_if<>的缺省参数用法也可以使用enable_if<is_integral<T>,T>的形式，但因为指针参数并不被实际使用，因此默认的void类型
就可以正常工作了)。第二个元参数也不一定必须是T，我们也可以在这里再进行元计算，比如使用promote<T>提升T的范围。
使用disable_if<>可以禁止函数的实例化，例如不允许print()操作类类型：
template<typename T>
typename disable_if<is_class<T>,T>::type			//T是class时禁用
print(T x)
{
	...
}

应用于类模板：
enable_if的启用或禁用类模板偏特化的用法与函数模板用法类似，它需要为类的模板参数列表增加一个额外缺省参数，缺省值是void，然后再使用enable_if来偏特化。
template<typename T,typename Enable = void>
class Dog
{
	...
};
然后使用enable_if偏特化：
template<typename T>
class Dog<T,typename enable_if<is_arithmetic<T>>::type>
{
	...
};
在这里Dog使用enable_if对int,real_type等算术类型进行了偏特化。很显然，enable_if使得模板偏特化的应用范围更大了，可以针对某一些而不是某一个特定的类型
偏特化，简单的偏特化相当于使用is_same<>:
//对string类型特化
template<typename T>
class Dog<T,typename enable_if<is_same<T,string>>::type>
{
	...
};

lazy_enable_if:
enable_if库还提供另外四个功能类似的lazy版本，它们与同名的版本没有太多的不同，只是要求类型T必须有一个内部的::type类型定义。
lazy_enable_if<>和lazy_enable_if_c<>的定义如下：
template<bool B,typaname T>
struct lazy_enable_if_c
{
	typedef typename T::type type;			//注意这里
};

template<typename T>
struct lazy_enable_if_c<false,T>
{
};

template<typename Cond,typename T>
struct lazy_enable_if : public lazy_enable_if_c<Cond::value,T>
{
};
相对于enable_if来说，lazy_enable_if对类型T增加了更强的约束，如果需要类型有内嵌的type类型，那么就使用它。
*/

/*
range:
迭代器通常很少单独使用，在处理容器或者字符串的时候我们常常要用一对指定了首末位置的迭代器。另外一些时候，我们只想操作容器里的一部分元素，但标准库
并没有提供这样的接口，只能拷贝到一个临时容器，增加了不必要的运行成本。
range库在迭代器和容器之上抽象出了“区间”（range）的概念，简化了对算法和容器的操作。

区间（range）是一个比较宽松的概念，它基于迭代器和容器，但要求却比容器低的多，不需要容纳元素，只含有区间的两个首末端点位置，像是一个容器的“视图”，
可以简单地想象为一个迭代器的pair(std::pair<I,I>)。
区间都是左闭右开的，可以用成员函数begin()/end()或者自由函数begin()/end()获得其两个端点。
range库目前内建支持的有：
1：标准容器和boost容器。
2：迭代器的std::pair。
3：原生数组和字符串。
4：range库自带的iterator_range及其子类。

元函数has_range_iterator<>可以判断一个类型是否是区间：
BOOST_STATIC_ASSERT(has_range_iterator<vector<int>>::value);
BOOST_STATIC_ASSERT(has_range_iterator<string>::value);
typedef boost::array<char,5> array_5;
BOOST_STATIC_ASSERT(has_range_iterator<array_5>::value);
typedef pair<int*,int*> pair_t;
BOOST_STATIC_ASSERT(has_range_iterator<pair_t>::value);
char a[] = "range"																						//原生数组
BOOST_STATIC_ASSERT(has_range_iterator<decltype(a)>::value);
BOOST_STATIC_ASSERT(!has_range_iterator<char*>::value);							//指针类型不是区间
range库完全基于boost新式迭代器概念，区间也可以分为单遍区间、前向区间、双向区间和随机访问区间，所以可以使用概念检查类来更精确地判断类型。

区间元函数：
类似iterator_traits<>，range库提供若干元函数用来获取区间的特征：
1:range_iterator<R>:
返回区间的迭代器类型。
2:range_value<R>:
返回区间的值类型。
3:range_reference<R>:
返回区间的引用类型。
4:range_pointer<R>:
返回区间的指针类型。
5:range_category<R>:
返回区间的迭代器分类。
6:range_size<R>:
返回区间的长度类型（无符号整数）。
7:range_difference<R>:
返回区间的距离类型（有符号整数）。
8:range_reverse_iterator<R>:
返回区间的逆向迭代器（仅对双向区间）。

区间操作函数：
range库抽象了对区间的操作，给出了若干类似容器的操作：
1:begin():
返回区间的起点。
2:end():
返回区间的终点。
3:rbegin():
返回逆向区间的起点（双向区间）。
4:rend():
返回逆向区间的终点（双向区间）。
5:empty():
判断区间是否为空。
6:distance():
返回区间两端的距离。
7:size():
返回区间的大小（随机访问区间）。
begin(),end()等四个操作另有前缀const_的版本。
distance()的计算方法是std::distance(begin(r),end(r)),返回类型是有符号类型range_difference<R>::type。
size()是直接end(r) - begin(r)，返回类型是无符号类型range_size<R>::type,只能用于随机访问区间。
因为容器、迭代器的pair和数组都满足区间的概念，所以我们可以用这些区间操作函数写出统一处理它们的代码，不必纠结于类型是否有成员函数begin()/end()。

区间算法：
range库在名字空间boost里提供了所有标准算法的区间版本，但它们并不在头文件"boost/range.hpp"里，而是对应标准库算法分成了三个头文件：
1:"boost/range/algorithm.hpp":
C++中的所有标准算法。
2:"boost/range/algorithm_ext.hpp":
一些扩展算法，如erase、itoa。
3:"boost/range/numeric.hpp":
数值标准算法。
这些区间算法的参数除了把两个迭代器改为一个区间外，与标准算法基本相同。

区间算法的返回类型：
通常情况下区间算法与标准版本的返回类型相同（很多时候算法的返回值都被忽略），但有的区间算法的重载形式却可以返回一个区间类型，能够搭配其它算法使用。
这些算法又可以分为两类，第一类算法返回原区间，第二类算法使用一个模板参数定制返回的区间。
返回原区间的算法（包括_copy版本）：
1:fill/fill_n
2:generate/generate_n
3:inplace_merge
4:random_shuffle
5:replace/replace_if
6:reverse/rotate
7:sort/stable_sort
8:partial_sort/nth_element
9:make_heap/sort_heap/push_heap/pop_heap
这些算法处理整个区间，然后再返回原区间。由于区间类型里含有两个首末位置的迭代器，所以能够直接被其它区间算法所利用。
定制返回区间的算法：
第二类算法都与某种形式查找/分割操作有关（包括_copy版本)：
1:find/find_if
2:find_first_of/find_end/adjacent_find
3:search/search_n
4:remove/remove_if
5:unique
6:partition/stable_partition
7:lower_bound/upper_bound
8:min_element/max_element
这些算法的标准版本返回一个处理后的迭代器位置，我们可以将这个迭代器位置称为found，依据found位置可以得到两个常用的子区间：
[boost::begin(rng),found):起点到返回位置的区间。
[found,boost::end(rng)):返回位置到终点的区间。
除了found位置，range库还定义了boost::next(found)和boost::prior(found)位置，这五个位置（含起点和终点）如下所示：
+--------------+-----+-----+---------------+
begin           prior   found  next               end
range库在boost名字空间定义了一个枚举类型range_return_value，作为这些区间算法的模板参数，来元计算定制返回类型：
enum range_return_value
{
	return_found,									//返回found位置，同标准算法
	return_next,									//返回next位置
	return_prior,									//返回prior位置
	return_begin_found,						//返回区间[begin,found)
	return_begin_next,							//返回区间[begin,next)
	return_begin_prior,							//返回区间[begin,prior)
	return_found_end,							//返回区间[found,end)
	return_next_end,								//返回区间[next,end)
	return_prior_end,							//返回区间[prior,end)
	return_begin_end							//返回区间[begin,end)
};
//find算法的定制返回类型重载，其它算法类似
template<range_return_value re,typename SinglePassRange,typename Value>
range_return<const SinglePassRange,re>::type						//元计算返回类型
find(const SinglePassRange& rng,const Value& val);

iterator_range:
区间概念本质上就是一对迭代器，可以用std::pair<I,I>来表示，但std::pair太过于简单，也不能明确地表述区间的含义，所以range库提供了一个专门的区间工具
类：iterator_range。
iterator_range同std::pair<I,I>类似，封装了两个迭代器，但它的功能更丰富，接口像是一个标准容器，但非常轻量级。
辅助函数：
iterator_range可以直接从两个迭代器或者一个区间/容器来构造：
vector<int> v(10);
typedef iterator_range<vector<int>::iterator> vec_range;			//区间类型定义
//从容器构造一个区间
vec_range r1(v);

int a[10];
typedef iterator_range<int*> int_range;
//从两个迭代器构造区间
int_range r2(a,a + 5);
直接创建iterator_range需要手工指定迭代器类型，很多时候显得很麻烦，所以range库提供工厂函数make_iterator_range()，它有三种重载形式：
template<typename IteratorT>
iterator_range<IteratorT> make_iterator_range(IteratorT Begin,IteratorT End);		//两个迭代器区间构造

template<typename ForwardRange>
iterator_range<range_iterator<ForwardRange>::type> make_iterator_range(ForwardRange& r);		//区间或容器构造区间

template<typename Range>
iterator_range<range_iterator<Range>::type> make_iterator_range(Range& r,							//区间
																range_difference<Range>::type advance_begin,		//起点
																range_difference<Range>::type advance_end);		//终点
vector<int> v(10);
auto r1 = make_iterator_range(v);
BOOST_STATIC_ASSERT(has_range_iterator<decltype(r1)>::value);
int a[10];
auto r2 = make_iterator_range(a,a + 5);
auto r3 = make_iterator_range(a,1,-1);				//指定迭代器的位置
BOOST_STATIC(r3.size() == 8);
range库另外还有一个函数模板copy_range,它可以把区间里的元素拷贝到一个新的容器：
template<typename SeqT,typename Range>
SeqT copy_range(const Range& r);

区间工具：
基于iterator_range，range库实现了几个很有用的工具类和函数。
sub_range:
sub_range是iterator_range的子类，它的主要目的是表示一个子区间，更接近容器。
counting_range:
counting_range()函数返回一个计数迭代器的区间，类似C++11的itoa算法。
istream_range:
istream_range()函数，封装了输入流迭代器std::istream_iterator,它可以把输入流的输入过程转化成一个区间。
irange:
irange()，它使用iterator_facade定义了两个可递增的整数迭代器integer_iterator和integer_iterator_with_step，然后使用irange()函数产生递增的整数区间。
它很类似counting_range，但多了指定步长的功能。

区间适配器：
类似迭代器的适配器，range库也提供了区间适配器，可以把一个区间适配成另一个区间。由于区间的能力基于迭代器，因此变动迭代器也就变动了区间，使区间不仅
只能表达元素的范围，更可以处理元素。
同迭代器类似，区间适配器使用boost::copy来驱动数据通过区间和迭代器流动，而且由于区间的自表达特性，它还重载了operator()，支持类似UNIX的管道操作符
的形式连接多个区间，每个区间都可以执行一定的工作。
适配器列表：
1:adjacent_filtered(P):
使用谓词P过滤两个邻接的元素。
2:copied(n,m):
取子区间[n,m),只能用于随机访问区间。
3:filtered(P):
使用谓词P过滤元素。
4:indexed(i):
为迭代器添加一个从i开始的索引号。
5:indirected:
类似indirect_iterator,多一次解引用操作。
6:map_keys:
取出map容器里的key。
7:map_values:
取出map容器里的value。
8:replaced(x,y):
把区间里的x替换为y。
9:replaced_if(P,v):
把区间里符合谓词P的元素替换为v。
10:reversed:
逆序区间。
11:sliced(n,m):
同copied。
12:strided(n):
在区间上以步长n跳跃前进。
13:tokenized():
使用boost::regex正则处理。
14:transformed(F):
类似std::transform算法，用F处理每一个元素。
15:uniqued:
过滤掉相邻重复的元素。
区间适配器的用法相当简单，可以把原始区间想象成一个数据源，经过适配器后得到处理的数据，最后再由算法来处理这些数据。

自定义区间类型：
range库支持标准容器、boost容器、迭代器的pair等类型，并且提供了区间工具类iterator_range和sub_range，大多数情况下已经能够满足我们的需要，但有的
时候我们想让自己编写的类也能够搭配区间算法使用，这时就要求自定义类型满足区间的概念。
可以有三种方式来实现一个区间：
1：定义为iterator_range的子类。
2：提供成员函数begin()/end()和内部的类型定义iterator/const_iterator。
3：如果不能修改类型，那么可以为类型重载range_begin()和range_end()，返回区间的端点，同时还要重载（特化）boost名字空间里的元函数range_const_iterator<>
和range_mutable_iterator<>。

连接区间：
join()函数。
注意join()函数并不要求被连接的两个区间是相邻的，也就是说它可以把两个有间隔的区间连接成一个逻辑上连续的区间。
*/