/*!
 * \brief
 * checked delete
 * \file UFCDelete.h
 *
 * \author Su Yang
 *
 * \date 2016/04/16
 */
#ifndef _UFC_DELETE_H_
#define _UFC_DELETE_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#ifndef _INCLUDE_BOOST_CHECKEDDELETE_HPP_
#include "boost/checked_delete.hpp"
#define _INCLUDE_BOOST_CHECKEDDELETE_HPP_
#endif

namespace ung
{
	/*!
	 * \brief
	 * 函数对象，删除普通指针
	 * \class Delete
	 *
	 * \author Su Yang
	 *
	 * \date 2016/04/16
	 *
	 * \todo
	 */
	class Delete
	{
	public:
		typedef void result_type;

		template<typename T>
		void operator()(T*& p) const
		{
			boost::checked_delete(p);
			p = nullptr;
		}
	};

	/*!
	 * \brief
	 * 函数对象，删除指向数组的指针
	 * \class DeleteArray
	 *
	 * \author Su Yang
	 *
	 * \date 2016/04/16
	 *
	 * \todo
	 */
	class DeleteArray
	{
	public:
		typedef void result_type;

		template<typename T>
		void operator()(T*& p) const
		{
			boost::checked_array_delete(p);
			p = nullptr;
		}
	};

#define UFC_DELETE(p) Delete()(p)
#define UFC_DELETE_ARRAY(p) DeleteArray()(p)
#define UFC_DELETE_R Delete()																						//_R:删除器
#define UFC_DELETE_ARRAY_R DeleteArray()
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_DELETE_H_