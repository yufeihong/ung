/*!
 * \brief
 * video buffer
 * \file URMVideoBuffer.h
 *
 * \author Su Yang
 *
 * \date 2017/05/30
 */
#ifndef _URM_VIDEOBUFFER_H_
#define _URM_VIDEOBUFFER_H_

#ifndef _URM_CONFIG_H_
#include "URMConfig.h"
#endif

#ifdef URM_HIERARCHICAL_COMPILE

#ifndef _UIF_IVIDEOBUFFER_H_
#include "UIFIVideoBuffer.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * video buffer
	 * \class VideoBuffer
	 *
	 * \author Su Yang
	 *
	 * \date 2017/05/30
	 *
	 * \todo
	 */
	class UngExport VideoBuffer : public IVideoBuffer
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		VideoBuffer();

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param uint32 usage
		 * \param bool useShadowBuffer
		*/
		VideoBuffer(uint32 usage, bool useShadowBuffer);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~VideoBuffer();

		/*!
		 * \remarks Lock the entire buffer for reading / writing(lock整个表面)
		 * \return 
		 * \param LockOptions options
		*/
		virtual void* lock(LockOptions options) override;

		/*!
		 * \remarks Lock the buffer for reading / writing(如果要lock整个表面的话，offset和length应该都设置为0)
		 * \return 
		 * \param uint32 offset
		 * \param uint32 length 要lock的区域的size，单位：字节
		 * \param LockOptions options
		*/
		virtual void* lock(uint32 offset, uint32 length, LockOptions options) override;

		/*!
		 * \remarks Releases the lock on this buffer
		 * 当缓存处于locked的状态，这个时候去切换video mode，会抛出异常，必须重新upload数据，如果
		 * 要100%确保数据不会lost，那么就使用readData和writeData。
		 * \return 
		*/
		virtual void unlock() override;

		/*!
		 * \remarks Reads data from the buffer and places it in the memory pointed to by pDest
		 * \return 
		 * \param uint32 offset
		 * \param uint32 length
		 * \param void* pDest
		*/
		virtual void readData(uint32 offset,uint32 length,void* pDest) override;

		/*!
		 * \remarks Copy data from another buffer into this one
		 * \return 
		 * \param IVideoBuffer& srcBuffer 源，不能为通过VB_USAGE_WRITE_ONLY创建的
		 * \param uint32 srcOffset
		 * \param uint32 dstOffset
		 * \param uint32 length
		 * \param bool discardWholeBuffer If true,will discard the entire contents of this buffer before copying
		*/
		virtual void copyData(IVideoBuffer& srcBuffer, uint32 srcOffset, uint32 dstOffset, uint32 length,bool discardWholeBuffer = true) override;

		/*!
		 * \remarks Copy all data from another buffer into this one
		 * \return 
		 * \param IVideoBuffer& srcBuffer
		*/
		virtual void copyData(IVideoBuffer& srcBuffer) override;

		/*!
		 * \remarks Updates the real buffer from the shadow buffer,if required.
		 * \return 
		*/
		virtual void updateFromShadow() override;

		/*!
		 * \remarks 获取缓存的大小，单位：字节
		 * \return 
		*/
		virtual uint32 getSizeInBytes() const override;

		/*!
		 * \remarks 获取创建缓存时的Usage flags
		 * \return 
		*/
		virtual uint32 getUsage() const override;

		/*!
		 * \remarks 这个缓存是否有shadow
		 * \return 
		*/
		virtual bool hasShadowBuffer() const override;

		/*!
		 * \remarks 当前的状态是否为locked
		 * \return 
		*/
		virtual bool isLocked() const override;

	protected:
		uint32 mUsage;
		bool mUseShadowBuffer;
		bool mShadowUpdated;
		bool mIsLocked;
		IVideoBuffer* mShadowBuffer;
		uint32 mSizeInBytes = 0;
		uint32 mLockStart = 0;
		uint32 mLockSize = 0;
	};
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

#endif//_URM_VIDEOBUFFER_H_