/*!
 * \brief
 * 全局配置文件
 * \file UBSGlobalConfig.h
 *
 * \author Su Yang
 *
 * \date 2017/03/22
 */
#ifndef _UBS_GLOBALCONFIG_H_
#define _UBS_GLOBALCONFIG_H_

//在include UNG头文件之前定义下列宏

//关闭双精度浮点数
//#define UNG_CLOSE_DOUBLE
//关闭SIMD
//#define UNG_CLOSE_SIMD

//使用双精度(256位)颜色值
//#define UNG_USE_256_BITS_COLOR

//使用DX渲染接口
//#define UNG_RENDER_API_DX

#endif//_UBS_GLOBALCONFIG_H_