/*!
 * \brief
 * 抽象基类，插件
 * \file UFCPlugin.h
 *
 * \author Su Yang
 *
 * \date 2016/04/20
 */
#ifndef _UFC_PLUGIN_H_
#define _UFC_PLUGIN_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

namespace ung
{
	/*!
	 * \brief
	 * 插件API
	 * \class Plugin
	 *
	 * \author Su Yang
	 *
	 * \date 2016/04/20
	 *
	 * \todo
	 */
	class UngExport Plugin : public MemoryPool<Plugin>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		Plugin();

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		virtual ~Plugin();

		/*!
		 * \remarks 获取插件的名字
		 * \return 
		*/
		virtual const char* getName() const = 0;

		/*!
		 * \remarks 安装插件
		 * \return 
		*/
		virtual void install() = 0;

		/*!
		 * \remarks 卸载插件
		 * \return 
		*/
		virtual void uninstall() = 0;
	};
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_PLUGIN_H_