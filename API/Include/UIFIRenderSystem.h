/*!
 * \brief
 * 渲染系统接口
 * \file UIFIRenderSystem.h
 *
 * \author Su Yang
 *
 * \date 2017/04/29
 */
#ifndef _UIF_IRENDERSYSTEM_H_
#define _UIF_IRENDERSYSTEM_H_

#ifndef _UIF_CONFIG_H_
#include "UIFConfig.h"
#endif

#ifdef UIF_HIERARCHICAL_COMPILE

#ifndef _UIF_ENUMCOLLECTION_H_
#include "UIFEnumCollection.h"
#endif

#ifndef _URM_ENUMCOLLECTION_H_
#include "URMEnumCollection.h"
#endif

namespace ung
{
	class Viewport;
	class IOffscreenPlainSurface;
	class Shader;
	class Camera;

	//渲染系统类型
	enum class RenderSystemType : uint8
	{
		RST_D3D = 1,
		RST_GL = 2
	};

	/*!
	 * \brief
	 * 渲染系统接口
	 * \class IRenderSystem
	 *
	 * \author Su Yang
	 *
	 * \date 2017/05/10
	 *
	 * \todo
	 */
	class UngExport IRenderSystem : public MemoryPool<IRenderSystem>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		IRenderSystem() = default;

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~IRenderSystem() = default;

		/*!
		 * \remarks 初始化
		 * \return 
		*/
		virtual void initialize() PURE;

		/*!
		 * \remarks 是否已经初始化了
		 * \return 
		*/
		virtual bool isInitialized() const PURE;

		/*!
		 * \remarks 获取渲染系统类型
		 * \return 
		*/
		virtual RenderSystemType getType() const PURE;

		/*!
		 * \remarks 渲染前的准备工作
		 * \return 
		*/
		virtual void prepare() PURE;

		/*!
		 * \remarks 设置窗口/全屏
		 * \return 
		 * \param bool windowed true:窗口
		*/
		virtual void setWindowed(bool windowed) PURE;

		/*!
		 * \remarks 获取是否为窗口
		 * \return true:窗口
		*/
		virtual bool getWindowed() const PURE;

		/*!
		 * \remarks 设置back buffer的宽高
		 * \return 
		 * \param uint32 width
		 * \param uint32 height
		*/
		virtual void setBackBufferWH(uint32 width, uint32 height) PURE;

		/*!
		 * \remarks 获取back buffer的宽高
		 * \return 
		*/
		virtual std::pair<uint32, uint32> getBackBufferWH() const PURE;

        /*!
		 * \remarks Get the native color type for this rendersystem.
         * \return 
        */
        virtual VertexElementType getNativeColorType() const PURE;

		/*!
		 * \remarks 设置back buffer的像素格式
		 * \return 
		 * \param PixelFormat pf
		*/
		virtual void setBackBufferPixelFormat(PixelFormat pf) PURE;

		/*!
		 * \remarks 获取back buffer的像素格式
		 * \return 
		*/
		virtual PixelFormat getBackBufferPixelFormat() const PURE;

		/*!
		 * \remarks 创建一个offscreen plain表面
		 * \return 
		 * \param uint32 width 单位：像素个数
		 * \param uint32 height
		 * \param PixelFormat format
		 * \param ResourcePoolType pt
		*/
		virtual IOffscreenPlainSurface* createOffscreenPlainSurface(uint32 width,uint32 height,PixelFormat format, ResourcePoolType pt = ResourcePoolType::RPT_DEFAULT) PURE;

		/*!
		 * \remarks 创建窗口
		 * \return 
		 * \param String const & title
		 * \param bool isFullScreen
		 * \param uint32 width
		 * \param uint32 height
		 * \param bool primary
		*/
		virtual bool createWindow(String const& title, bool isFullScreen, uint32 width, uint32 height,bool primary = false) PURE;

		/*!
		 * \remarks 关闭渲染系统
		 * \return 
		*/
		virtual void shutdown() PURE;

		/*!
		 * \remarks 设备是否丢失
		 * \return 
		*/
		virtual bool isDeviceLost() PURE;

		/*!
		 * \remarks 处理窗口大小变化的消息
		 * \return 
		*/
		virtual void onSizeChanged() PURE;

		/*!
		 * \remarks 获取active window
		 * \return 
		*/
		virtual IWindow* getActiveWindow() PURE;

		/*!
		 * \remarks 在窗口模式和全屏模式之间切换
		 * \return 
		*/
		virtual void switchBetweenWindowedAndFullScreen() PURE;

		/*!
		 * \remarks 更新
		 * \return 
		 * \param real_type dt 单位:毫秒
		*/
		virtual void update(real_type dt) PURE;

		/*!
		 * \remarks 渲染一帧
		 * \return 
		 * \param real_type deltaTime 单位：毫秒
		*/
		virtual void renderOneFrame(real_type deltaTime) PURE;

		/*!
		 * \remarks 创建摄像机
		 * \return 
		 * \param String const & name
		 * \param Vector3 const & pos
		 * \param Vector3 const & lookat
		 * \param real_type aspect
		*/
		virtual Camera* createCamera(String const& name,Vector3 const& pos, Vector3 const& lookat, real_type aspect) PURE;

		/*!
		 * \remarks 删除摄像机
		 * \return 
		 * \param String const & name
		*/
		virtual void removeCamera(String const& name) PURE;

		/*!
		 * \remarks 设置主摄像机
		 * \return 
		 * \param Camera * cam
		*/
		virtual void setMainCamera(Camera* cam) PURE;

		/*!
		 * \remarks 获取主摄像机
		 * \return 
		*/
		virtual Camera* getMainCamera() const PURE;

		/*!
		 * \remarks 获取参数所指定的摄像机
		 * \return 
		 * \param String const & name
		*/
		virtual Camera* getCamera(String const& name) const PURE;

		/*!
		 * \remarks 设置视口变换
		 * \return 
		 * \param Viewport * vp
		*/
		virtual void setViewPort(Viewport* vp) PURE;

		/*!
		 * \remarks 创建顶点shader
		 * \return 
		 * \param String const & fullName
		*/
		virtual Shader* createVertexShader(String const& fullName) PURE;

		/*!
		 * \remarks 创建片段shader
		 * \return 
		 * \param String const & fullName
		*/
		virtual Shader* createPixelShader(String const& fullName) PURE;

		/*!
		 * \remarks 使用shader
		 * \return 
		 * \param String const & shaderName
		*/
		virtual void useShader(String const& shaderName) PURE;

		/*!
		 * \remarks 设置默认的渲染状态
		 * \return 
		*/
		virtual void setDefaultRenderStates() PURE;

		/*!
		 * \remarks 设置cull mode
		 * \return 
		 * \param CullMode cm
		*/
		virtual void setRenderStateCullMode(CullMode cm = CullMode::CM_CW) PURE;
	};
}//namespace ung

#endif//UIF_HIERARCHICAL_COMPILE

#endif//_UIF_IRENDERSYSTEM_H_