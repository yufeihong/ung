/*!
 * \brief
 * 数学常用计算
 * \file UMLMath.h
 *
 * \author Su Yang
 *
 * \date 2016/06/15
 */
#ifndef _UML_MATH_H_
#define _UML_MATH_H_

#ifndef _UML_CONFIG_H_
#include "UMLConfig.h"
#endif

#ifdef UML_HIERARCHICAL_COMPILE

#ifndef _UML_ASM_H_
#include "UMLASM.h"
#endif

#ifndef _UML_CONSTS_H_
#include "UMLConsts.h"
#endif

#ifndef _UML_ANGLE_H_
#include "UMLAngle.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 数学常用计算
	 * \class MathImpl
	 *
	 * \author Su Yang
	 *
	 * \date 2016/06/15
	 *
	 * \todo
	 */
	template<typename T>
	class MathImpl
	{
	public:
		using type = T;

		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		MathImpl() = delete;

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~MathImpl() = delete;

		/*!
		 * \remarks 绝对值
		 * \return 
		 * \param type value
		*/
		static type calAbs(type value)
		{
			return type(std::fabs(value));
		}

		/*!
		 * \remarks 平方
		 * \return 
		 * \param type r
		*/
		static type calSqr(type r)
		{
			return r * r;
		}

		/*!
		 * \remarks 开平方根
		 * \return 
		 * \param type r
		*/
		static type calSqrt(type r)
		{
			return type(std::sqrt(r));
		}

		/*!
		 * \remarks 计算平方根的倒数
		 * \return 
		 * \param type r
		*/
		static type calInvSqrt(type r)
		{
			return ASM::asm_invSqrt(r);
		}

		/*!
		 * \remarks 获取小于或等于最大整数的浮点值(往左取整)
		 * \return 2.8->2.0,-2.8->-3.0
		 * \param type r
		*/
		static type calFloor(type r)
		{
			return std::floor(r);
		}

		/*!
		 * \remarks 大于或等于最小整数的浮点值(往右取整)
		 * \return 2.8->3.0,-2.8->-2.0
		 * \param type r
		*/
		static type calCeil(type r)
		{
			return std::ceil(r);
		}

		/*!
		 * \remarks 计算r的exp次幂
		 * \return 
		 * \param type r
		 * \param double exp
		*/
		static type calPow(type r, double exp)
		{
			return std::pow(r, exp);
		}

		/*!
		 * \remarks 计算以2为底的对数。
		 * \return 
		 * \param type r
		*/
		static type calLog2(type r)
		{
			BOOST_ASSERT(r > 0.0);

			return std::log2(r);
		}

		/*!
		 * \remarks 计算以e为底的对数。
		 * \return 
		 * \param type r
		*/
		static type calLogE(type r)
		{
			BOOST_ASSERT(r > 0.0);

			return std::log(r);
		}

		/*!
		 * \remarks 计算以10为底的对数。
		 * \return 
		 * \param type r
		*/
		static type calLog10(type r)
		{
			BOOST_ASSERT(r > 0.0);

			return std::log10(r);
		}

		/*!
		 * \remarks 获取较大的数
		 * \return 
		 * \param type r1
		 * \param type r2
		*/
		static type calMax(type r1, type r2)
		{
			return std::max(r1, r2);
		}

		/*!
		 * \remarks 获取较小的数
		 * \return 
		 * \param type r1
		 * \param type r2
		*/
		static type calMin(type r1, type r2)
		{
			return std::min(r1, r2);
		}

		/*!
		 * \remarks 约束至一个范围
		 * \return 
		 * \param const type & val
		 * \param const type & min
		 * \param const type & max
		*/
		static type calClamp(const type& val, const type& min, const type& max)
		{
			BOOST_ASSERT(min <= max);

			return std::max(std::min(val, max), min);
		}

		/*!
		 * \remarks 计算sin
		 * \return 
		 * \param Radian r 弧度
		*/
		static type calSin(Radian r)
		{
			return ASM::asm_sin(r.getRadian());
		}

		/*!
		 * \remarks 计算sin
		 * \return 
		 * \param Degree d 度
		*/
		static type calSin(Degree d)
		{
			return calSin(d.toRadian());
		}

		/*!
		 * \remarks 计算cos
		 * \return 
		 * \param Radian r 弧度
		*/
		static type calCos(Radian r)
		{
			return ASM::asm_cos(r.getRadian());
		}

		/*!
		 * \remarks 计算cos
		 * \return 
		 * \param Degree d度
		*/
		static type calCos(Degree d)
		{
			return calCos(d.toRadian());
		}

		/*!
		 * \remarks 计算正切
		 * \return 
		 * \param Radian r 弧度
		*/
		static type calTan(Radian r)
		{
			return type(std::tan(r.getRadian()));
		}

		/*!
		 * \remarks 计算正切
		 * \return 
		 * \param Degree d 度
		*/
		static type calTan(Degree d)
		{
			return calTan(d.toRadian());
		}

		/*!
		 * \remarks 计算余切
		 * \return 
		 * \param Radian r 弧度
		*/
		static type calCot(Radian r)
		{
			return 1.0 / calTan(r);
		}

		/*!
		 * \remarks 计算余切
		 * \return 
		 * \param Degree d 度
		*/
		static type calCot(Degree d)
		{
			return calCot(d);
		}

		/*!
		 * \remarks 计算arcSin
		 * \return 弧度
		 * \param real_type s
		*/
		static Radian calASin(real_type s)
		{
			calClamp(s, -1.0, 1.0);

			return Radian(std::asin(s));
		}

		/*!
		 * \remarks 计算arcCos
		 * \return 弧度
		 * \param real_type s
		*/
		static Radian calACos(real_type s)
		{
			calClamp(s, -1.0, 1.0);

			/*
			acos 函数返回反余弦参数x在范围 0 到PI之间的弧度.
			默认情况下,如果x小于–1或大于1,acos返回未定义值.
			*/
			return Radian(std::acos(s));
		}

		/*!
		 * \remarks 判断两个浮点数是否相等
		 * \return 
		 * \param type a
		 * \param type b
		 * \param type tolerance std::numeric_limits<type>::epsilon()不足以表达经过类似于normalize后的误差
		*/
		static bool isEqual(type a,type b,type tolerance = Consts<type>::ZERO)
		{
			type diff = std::fabs(a - b);
			
			if (diff <= tolerance)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/*!
		 * \remarks 判断参数所给浮点数是否为0.0
		 * \return 
		 * \param type r
		 * \param type tolerance
		*/
		static bool isZero(type r,type tolerance = Consts<type>::ZERO)
		{
			return isEqual(r, 0.0, tolerance);
		}

		/*!
		 * \remarks 判断参数所给浮点数是否为1.0
		 * \return 
		 * \param type r
		 * \param type tolerance
		*/
		static bool isUnit(type r,type tolerance = Consts<type>::ZERO)
		{
			return isEqual(r, 1.0, tolerance);
		}

		/*!
		 * \remarks 小于
		 * \return 
		 * \param type a
		 * \param type b
		 * \param type tolerance
		*/
		static bool isLessThan(type a, type b, type tolerance = Consts<type>::ZERO)
		{
			if (a < b + tolerance)
			{
				return true;
			}

			return false;
		}

		/*!
		 * \remarks 大于
		 * \return 
		 * \param type a
		 * \param type b
		 * \param type tolerance
		*/
		static bool isGreaterThan(type a, type b, type tolerance = Consts<type>::ZERO)
		{
			if (a + tolerance > b)
			{
				return true;
			}

			return false;
		}

		/*!
		 * \remarks 小于等于
		 * \return 
		 * \param type a
		 * \param type b
		 * \param type tolerance
		*/
		static bool isLessThanEqual(type a,type b,type tolerance = Consts<type>::ZERO)
		{
			if (a <= b + tolerance)
			{
				return true;
			}

			return false;
		}

		/*!
		 * \remarks 大于等于
		 * \return 
		 * \param type a
		 * \param type b
		 * \param type tolerance
		*/
		static bool isGreaterThanEqual(type a, type b, type tolerance = Consts<type>::ZERO)
		{
			if (a + tolerance >= b)
			{
				return true;
			}

			return false;
		}

		/*!
		 * \remarks 度转换为弧度
		 * \return 
		 * \param type degree
		*/
		static type degreeToRadian(type degree)
		{
			return degree * (Consts<type>::PI / 180.0);
		}

		/*!
		 * \remarks 弧度转换为度
		 * \return 
		 * \param type radian
		*/
		static type radianToDegree(type radian)
		{
			return radian * (180.0 / Consts<type>::PI);
		}

		static const type POS_INFINITY;
		static const type NEG_INFINITY;
	};
}//namespace ung

#ifndef _UML_MATH_DEF_H_
#include "UMLMathDef.h"
#endif

#endif//UML_HIERARCHICAL_COMPILE

#endif//_UML_MATH_H_