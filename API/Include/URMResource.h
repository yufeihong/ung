/*!
 * \brief
 * 资源基类。支持多线程安全。
 * \file URMResource.h
 *
 * \author Su Yang
 *
 * \date 2016/11/17
 */
#ifndef _URM_RESOURCE_H_
#define _URM_RESOURCE_H_

#ifndef _URM_CONFIG_H_
#include "URMConfig.h"
#endif

#ifdef URM_HIERARCHICAL_COMPILE

namespace ung
{
	/*!
	 * \brief
	 * 资源基类。支持多线程安全。
	 * \class Resource
	 *
	 * \author Su Yang
	 *
	 * \date 2016/11/17
	 *
	 * \todo
	 */
	class Resource : public MemoryPool<Resource>,boost::noncopyable
	{
	public:
		/*!
		 * \remarks 默认构造函数，用户不调用
		 * \return 
		*/
		Resource();

		/*!
		 * \remarks 构造函数，用户不调用
		 * \return 
		 * \param String const & fullName 包含路径名
		 * \param String const & resourceType 资源的类型(具体类的类名)
		*/
		Resource(String const& fullName,String const& resourceType);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~Resource();

		/*!
		 * \remarks 把load任务送入后台线程
		 * \return 
		*/
		virtual void build() = 0;

		/*!
		 * \remarks 获取资源包含路径的名字
		 * \return 
		*/
		String const& getFullName() const;

		/*!
		 * \remarks 获取资源的类型
		 * \return 
		*/
		String const& getType() const;

		/*!
		 * \remarks 获取资源文件的扩展名
		 * \return 
		*/
		String const& getExtension() const;

		/*!
		 * \remarks 获取资源文件的大小。单位：字节
		 * \return 
		*/
		uint32 const& getSize() const;

	protected:
		/*!
		 * \remarks 映射文件，然后读取文件到一个属性树中
		 * \return 
		 * \param void* treePtr 避免在.h文件中include
		*/
		void readFile(void* treePtr);

	private:
		const String mFullName;
		const String mType;
		String mExtension;
		uint32 mSize;
	};
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

#endif//_URM_RESOURCE_H_