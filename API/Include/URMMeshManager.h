/*!
 * \brief
 * mesh管理器，支持多线程安全。
 * \file URMMeshManager.h
 *
 * \author Su Yang
 *
 * \date 2016/11/13
 */
#ifndef _URM_MESHMANAGER_H_
#define _URM_MESHMANAGER_H_

#ifndef _URM_CONFIG_H_
#include "URMConfig.h"
#endif

#ifdef URM_HIERARCHICAL_COMPILE

#ifndef _URM_MESH_H_
#include "URMMesh.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * mesh管理器，支持多线程安全。
	 * \class MeshManager
	 *
	 * \author Su Yang
	 *
	 * \date 2016/11/13
	 *
	 * \todo
	 */
	class MeshManager : public Singleton<MeshManager>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		MeshManager();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~MeshManager();

		/*!
		 * \remarks 创建一个mesh对象，并放入容器中，但是没有load，如果重复创建则引发异常
		 * \return 
		 * \param String const & meshName 仅文件名，不包含路径
		*/
		std::shared_ptr<Mesh> createMesh(String const& meshName);

		/*!
		 * \remarks 部署mesh的后台载入
		 * \return 
		 * \param std::shared_ptr<Mesh> meshPtr
		*/
		void buildMesh(std::shared_ptr<Mesh> meshPtr);

		/*!
		 * \remarks 获取一个已经创建的mesh对象，如果之前没有创建这个mesh则引发异常
		 * \return 
		 * \param String const & meshName
		*/
		std::shared_ptr<Mesh> getMesh(String const& meshName);

		/*!
		 * \remarks 获取一个已经创建的mesh对象，如果之前没有创建这个mesh则引发异常
		 * \return 
		 * \param String const & meshName
		*/
		std::shared_ptr<const Mesh> getMesh(String const& meshName) const;

		/*!
		 * \remarks 获取mesh的数量
		 * \return 
		*/
		usize getMeshCount() const;

	private:
		/*!
		 * \remarks 把mesh给存储到容器中
		 * \return 
		 * \param String const & meshName
		 * \param std::shared_ptr<Mesh> meshPtr
		*/
		void addMesh(String const& meshName,std::shared_ptr<Mesh> meshPtr);

		/*!
		 * \remarks 从容器中删除给定名字的mesh
		 * \return 
		 * \param String const & meshName
		*/
		void removeMesh(String const& meshName);

	private:
		STL_UNORDERED_MAP(String,std::shared_ptr<Mesh>) mMeshs;
	};
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

#endif//_URM_MESHMANAGER_H_