/*!
 * \brief
 * 包含范围检查的数值转型
 * \file UFCNumericCast.h
 *
 * \author Su Yang
 *
 * \date 2017/06/17
 */
#ifndef _UFC_NUMERICCAST_H_
#define _UFC_NUMERICCAST_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#ifndef _INCLUDE_BOOST_NUMERIC_CAST_HPP_
#include "boost/numeric/conversion/cast.hpp"
#define _INCLUDE_BOOST_NUMERIC_CAST_HPP_
#endif

#ifndef _INCLUDE_BOOST_TYPETRAITS_HPP_
#include "boost/type_traits.hpp"
#define _INCLUDE_BOOST_TYPETRAITS_HPP_
#endif

namespace ung
{
	/*!
	 * \remarks 包含范围检查的数值转型
	 * \return 
	 * \param Source n
	*/
	template<typename Target,typename Source>
	Target numCast(Source n)
	{
		BOOST_STATIC_ASSERT(boost::is_arithmetic<Source>::value);
		BOOST_STATIC_ASSERT(boost::is_arithmetic<Target>::value);

		return boost::numeric_cast<Target>(n);
	}
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_NUMERICCAST_H_

/*
只能执行对数字类型的转型is_arithmetic<T>::value == true

如果数字转型后可以被正确地容纳到目标类型那么一切OK，否则会抛出异常。可以完全替代static_cast进行
数字转型，带来更好的安全性。
*/