/*!
 * \brief
 * UTL的数组容器。
 * \file UTLArray.h
 *
 * \author Su Yang
 *
 * \date 2016/01/27
 */
#ifndef _UNG_UTL_ARRAY_H_
#define _UNG_UTL_ARRAY_H_

#ifndef _UNG_UTL_CONFIG_H_
#include "UTLConfig.h"
#endif

#ifndef _UNG_UTL_ARRAYITERATOR_H_
#include "UTLArrayIterator.h"
#endif

namespace ung
{
	namespace utl
	{
		/*!
		 * \brief
		 * 数组容器。
		 * \class Array
		 *
		 * \author Su Yang
		 *
		 * \date 2016/01/27
		 *
		 * \todo
		 */
		template<typename T,ssize SIZE>
		class Array
		{
			BOOST_STATIC_ASSERT_MSG(SIZE > 0,"array's SIZE should be greater than 0.");

			BOOST_CONCEPT_ASSERT((boost::Integer<ssize>));
			BOOST_CONCEPT_ASSERT((boost::SignedInteger<ssize>));

		public:
			using value_type = T;
			using size_type = usize;
			using pointer = typename boost::add_pointer<value_type>::type;									//T*
			BOOST_STATIC_ASSERT(boost::is_pointer<pointer>::value);
			using const_pointer = value_type const*;
			using reference = typename boost::add_lvalue_reference<value_type>::type;					//T&
			static_assert(boost::is_lvalue_reference<reference>::value,"reference should be a left reference.");
			using const_reference = value_type const&;
			using difference_type = ssize;
			using self_type = Array<value_type, SIZE>;
			typedef ArrayIterator<value_type> iterator;
			//新式迭代器概念检查
			BOOST_CONCEPT_ASSERT((boost_concepts::ReadableIteratorConcept<iterator>));									//可读迭代器
			BOOST_CONCEPT_ASSERT((boost_concepts::WritableIteratorConcept<iterator,value_type>));					//可写迭代器
			BOOST_CONCEPT_ASSERT((boost_concepts::SwappableIteratorConcept<iterator>));								//可交换迭代器
			BOOST_CONCEPT_ASSERT((boost_concepts::LvalueIteratorConcept<iterator>));										//左值迭代器
			BOOST_CONCEPT_ASSERT((boost_concepts::IncrementableIteratorConcept<iterator>));							//可递增迭代器
			BOOST_CONCEPT_ASSERT((boost_concepts::SinglePassIteratorConcept<iterator>));								//单遍迭代器
			BOOST_CONCEPT_ASSERT((boost_concepts::ForwardTraversalConcept<iterator>));								//前向迭代器
			BOOST_CONCEPT_ASSERT((boost_concepts::BidirectionalTraversalConcept<iterator>));							//双向迭代器
			BOOST_CONCEPT_ASSERT((boost_concepts::RandomAccessTraversalConcept<iterator>));						//随机访问遍历迭代器
			typedef ArrayIterator<typename boost::add_const<value_type>::type> const_iterator;							//<T const>
			BOOST_CONCEPT_ASSERT((boost_concepts::ReadableIteratorConcept<const_iterator>));						//可读迭代器
			//BOOST_CONCEPT_ASSERT((boost_concepts::WritableIteratorConcept<const_iterator, value_type>));		//可写迭代器
			//BOOST_CONCEPT_ASSERT((boost_concepts::SwappableIteratorConcept<const_iterator>));					//可交换迭代器
			BOOST_CONCEPT_ASSERT((boost_concepts::LvalueIteratorConcept<const_iterator>));							//左值迭代器
			BOOST_CONCEPT_ASSERT((boost_concepts::IncrementableIteratorConcept<const_iterator>));					//可递增迭代器
			BOOST_CONCEPT_ASSERT((boost_concepts::SinglePassIteratorConcept<const_iterator>));						//单遍迭代器
			BOOST_CONCEPT_ASSERT((boost_concepts::ForwardTraversalConcept<const_iterator>));						//前向迭代器
			BOOST_CONCEPT_ASSERT((boost_concepts::BidirectionalTraversalConcept<const_iterator>));					//双向迭代器
			BOOST_CONCEPT_ASSERT((boost_concepts::RandomAccessTraversalConcept<const_iterator>));				//随机访问遍历迭代器
			//typedef std::reverse_iterator<iterator> reverse_iterator;
			//typedef std::reverse_iterator<const_iterator> const_reverse_iterator;
			/*
			boost::reverse_iterator把一个迭代器适配成可以逆序遍历的逆向迭代器。
			reverse_iterator提供函数make_reverse_iterator()，可以轻松创建逆向迭代器。
			*/
			typedef boost::reverse_iterator<iterator> reverse_iterator;
			typedef boost::reverse_iterator<const_iterator> const_reverse_iterator;

			/*!
			 * \remarks 获取指向容器首元素的迭代器
			 * \return iterator
			*/
			iterator begin();												//迭代器
			/*!
			 * \remarks 获取指向容器首元素的常量迭代器，常量对象调用
			 * \return const_iterator
			*/
			const_iterator begin() const;
			/*!
			 * \remarks 获取指向容器首元素的常量迭代器
			 * \return const_iterator
			*/
			const_iterator cbegin() const;
			/*!
			 * \remarks 获取指向容器尾后的迭代器
			 * \return reverse_iterator
			*/
			reverse_iterator rbegin();
			/*!
			 * \remarks 获取指向容器尾后的常量迭代器，常量对象调用
			 * \return const_reverse_iterator
			*/
			const_reverse_iterator rbegin() const;
			/*!
			 * \remarks 获取指向容器尾后的常量迭代器
			 * \return const_reverse_iterator
			*/
			const_reverse_iterator crbegin() const;
			/*!
			 * \remarks 获取指向容器尾后的迭代器
			 * \return iterator
			*/
			iterator end();
			/*!
			 * \remarks 获取指向容器尾后的常量迭代器，常量对象调用
			 * \return const_iterator
			*/
			const_iterator end() const;
			/*!
			 * \remarks 获取指向容器尾后的常量迭代器
			 * \return const_iterator
			*/
			const_iterator cend() const;
			/*!
			 * \remarks 获取指向容器首元素的迭代器
			 * \return reverse_iterator
			*/
			reverse_iterator rend();
			/*!
			 * \remarks 获取指向容器首元素的常量迭代器，常量对象调用
			 * \return const_reverse_iterator
			*/
			const_reverse_iterator rend() const;
			/*!
			 * \remarks 获取指向容器首元素的常量迭代器
			 * \return const_reverse_iterator
			*/
			const_reverse_iterator crend() const;

			/*!
			 * \remarks 重载[]运算符
			 * \return reference 返回对容器元素的引用
			 * \param size_type i
			*/
			reference operator[](size_type i);						//重载operator[]
			/*!
			 * \remarks 重载[]运算符，常量对象调用
			 * \return const_reference 返回对容器元素的常量引用
			 * \param size_type i
			*/
			const_reference operator[](size_type i) const;
			/*!
			 * \remarks 获取索引位置的容器元素的引用
			 * \return reference 返回对容器元素的引用
			 * \param size_type i
			*/
			reference at(size_type i);									//取元素值，有范围检查
			/*!
			 * \remarks 获取索引位置的容器元素的引用，常量对象调用
			 * \return const_reference 返回对容器元素的常量引用
			 * \param size_type i
			*/
			const_reference at(size_type i) const;

			/*!
			 * \remarks 获取容器的头元素
			 * \return reference 返回对容器头元素的引用
			*/
			reference front();											//访问前后元素
			/*!
			 * \remarks 获取容器的头元素，常量对象调用
			 * \return const_reference 返回对容器头元素的常量引用
			*/
			const_reference front() const;
			/*!
			 * \remarks 获取容器的尾元素
			 * \return reference 返回对容器尾元素的引用
			*/
			reference back();
			/*!
			 * \remarks 获取容器的尾元素，常量对象调用
			 * \return const_reference 返回对容器尾元素的常量引用
			*/
			const_reference back() const;

			/*!
			 * \remarks 获取容器的大小
			 * \return size_type
			*/
			static size_type size();										//容量操作
			/*!
			 * \remarks 获取容器是否为空
			 * \return bool
			*/
			static bool empty();
			/*!
			 * \remarks 获取容器的大小，请使用size()接口
			 * \return size_type
			*/
			static size_type max_size();

			/*!
			 * \remarks 获取指向容器头元素的指针
			 * \return pointer
			*/
			pointer data();												//指针形式直接访问元素
			/*!
			 * \remarks 获取指向容器头元素的常量指针，常量对象调用
			 * \return const_pointer
			*/
			const_pointer data() const;
			/*!
			 * \remarks 获取指向容器头元素的指针，同data()接口
			 * \return pointer
			*/
			pointer c_array();

			/*!
			 * \remarks 用一个值来填充容器
			 * \return void
			 * \param const_reference v
			*/
			void fill(const_reference v);										//赋值操作
			/*!
			 * \remarks 用一个值来填充容器，同fill()接口
			 * \return void
			 * \param const_reference v
			*/
			void assign(const_reference v);
			/*!
			 * \remarks 交换两个不同类型的容器元素
			 * \return void
			 * \param Array<value_type,param SIZE> & other
			*/
			void swap(Array<value_type, SIZE>& other);					//交换操作

			/*!
			 * \remarks 越界检查
			 * \return void
			 * \param size_type i
			*/
			static void rangeCheck(size_type i);

			T mContainer[SIZE];
		};
		typedef Array<int, 1> ArrayTestType;
		//元函数has_range_iterator<>可以判断一个类型是否是区间
		BOOST_STATIC_ASSERT(boost::has_range_iterator<ArrayTestType>::value);									//Array<int,1>是区间
		//区间概念检查
		BOOST_CONCEPT_ASSERT((boost::SinglePassRangeConcept<ArrayTestType>));								//Array<int,1>是单遍区间
		BOOST_CONCEPT_ASSERT((boost::ForwardRangeConcept<ArrayTestType>));									//Array<int,1>是前向区间
		BOOST_CONCEPT_ASSERT((boost::WriteableForwardRangeConcept<ArrayTestType>));					//Array<int,1>是可写前向区间
		BOOST_CONCEPT_ASSERT((boost::BidirectionalRangeConcept<ArrayTestType>));							//Array<int,1>是双向区间
		BOOST_CONCEPT_ASSERT((boost::WriteableBidirectionalRangeConcept<ArrayTestType>));				//Array<int,1>是可写双向区间
		BOOST_CONCEPT_ASSERT((boost::RandomAccessRangeConcept<ArrayTestType>));						//Array<int,1>是随机访问区间
		BOOST_CONCEPT_ASSERT((boost::WriteableRandomAccessRangeConcept<ArrayTestType>));			//Array<int,1>是可写随机访问区间
		//获取区间的特征
		//1:返回区间的迭代器类型
		BOOST_STATIC_ASSERT(boost::is_same<boost::range_iterator<ArrayTestType>::type, ArrayTestType::iterator>::value);
		BOOST_STATIC_ASSERT(boost::is_same<boost::range_iterator<const ArrayTestType>::type, ArrayTestType::const_iterator>::value);
		//2:返回区间的值类型
		BOOST_STATIC_ASSERT(boost::is_same<boost::range_value<ArrayTestType>::type, ArrayTestType::value_type>::value);
		//3:返回区间的引用类型
		BOOST_STATIC_ASSERT(boost::is_same<boost::range_reference<ArrayTestType>::type, ArrayTestType::reference>::value);
		//4:返回区间的指针类型
		BOOST_STATIC_ASSERT(boost::is_same<boost::range_pointer<ArrayTestType>::type, ArrayTestType::pointer>::value);
		//5:返回区间的迭代器分类
		BOOST_STATIC_ASSERT(boost::is_same<boost::range_category<ArrayTestType>::type, ArrayTestType::iterator::iterator_category>::value);
		//6:返回区间的长度类型
		BOOST_STATIC_ASSERT(boost::is_same<boost::range_size<ArrayTestType>::type, ArrayTestType::size_type>::value);
		//7:返回区间的距离类型
		BOOST_STATIC_ASSERT(boost::is_same<boost::range_difference<ArrayTestType>::type, ArrayTestType::iterator::difference_type>::value);
		//8:返回区间的逆向迭代器（仅对双向区间）
		BOOST_STATIC_ASSERT(boost::is_same<boost::range_reverse_iterator<ArrayTestType>::type, ArrayTestType::reverse_iterator>::value);
		BOOST_STATIC_ASSERT(boost::is_same<boost::range_reverse_iterator<const ArrayTestType>::type, ArrayTestType::const_reverse_iterator>::value);
	}//namespace utl
}//namespace ung

#include "UTLArrayDef.h"

#endif//_UNG_UTL_ARRAY_H_

/*
concept_check:
泛型编程中使用的是“静态多态”，它在语义上经常要求类型具有某种“特征”或者满足某种“条件”。例如有内嵌的类型定义，固定名字的成员函数，支持迭代操作，这些
要求通常被称为“概念(concept)”。
概念检查的基本工具是宏BOOST_CONCEPT_ASSERT，它的用法很像静态断言BOOST_STATIC_ASSERT，可以用在任何域（scope）：函数域，类域，名字空间
域，如果概念检查不通过则导致编译错误。但BOOST_CONCEPT_ASSERT与静态断言也是有区别的，不能在宏中使用逻辑运算符(!,&&等)，还需要注意的是宏的
参数必须要用括号括起来，也就是说使用双重括号，像这样：BOOST_CONCEPT_ASSERT((some_check_class));
concept_check库提供了大量的概念检查类用来检查类型是否符合某个概念，这与type_traits库有些类似，只是type_traits的检查更偏重于C++的类型系统，而
concept_check库更偏重于类型的功能属性。

concept_check库里用于概念检查的元函数，可分为如下六个类别：
1：基本的概念检查				：检查整数类型、拷贝构造、缺省构造、赋值函数等基本的概念。
2：函数对象概念检查				：检查函数对象相关的概念。
3：标准迭代器概念检查			：检查标准库的五种迭代器分类概念。
4：新式迭代器概念检查			：检查boost定义的九种迭代器分类概念。
5：容器概念检查					：检查容器相关的概念。
6：区间概念检查					：检查区间相关的概念。

基本概念检查：
1:Integer<T>:
检查T是否是内建的整数类型，相当于is_integral<T>。
2:SignedInteger<T>:
检查T是否是内建的有符号整数类型，相当于is_signed<T>。
3:UnsignedInteger<T>:
检查T是否是内建的无符号整数类型，相当于is_unsigned<T>。
4:Convertible<X,Y>:
检查X是否可转换为Y，相当于is_convertible<X,Y>。
5:Assignable<T>:
检查T是否是可赋值的，要求有赋值操作符operator=。
6:SGIAssignable<T>:
检查T是否符合SGI赋值概念，要求有拷贝构造函数和operator=。
7:DefaultConstructible<T>:
检查T是否有缺省构造函数。
8:CopyConstructible<T>:
检查T是否有拷贝构造函数。
9:EqualityComparable<T>:
检查T是否可以进行相等比较，即定义了operator==。
10:LessThanComparable<T>:
检查T是否可以进行小于比较，即定义了operator<。
11:Comparable<T>:
检查T是否是可以进行所有关系运算，即定义了<,>,<=和>=。

函数对象概念检查:
函数对象概念检查主要基于标准库的函数对象定义，它们的模板参数较复杂，除了输入要检查的函数对象类型外，还依情况需要输入返回值类型和参数类型。
F：函数对象类型。R：返回值类型。A和B分别是两个参数类型。
1:Generator<F,R>:
检查F是否是无参函数对象。
2:UnaryFunction<F,R,A>:
检查F是否是单参函数对象。
3:BinaryFunction<F,R,A,B>:
检查F是否是双参函数对象。
4:UnaryPredicate<F,A>:
检查F是否是单参谓词(返回bool类型)。
5:BinaryPredicate<F,A,B>:
检查F是否是双参谓词。
6:Const_BinaryPredicate<F,A,B>:
检查F是否是const双参谓词。
7:AdaptableGenerator<F,R>:
检查F是否是有内嵌result_type定义的无参函数对象，因而可以被bind等函数对象适配器使用。
8:AdaptableUnaryFunction<F,R,A>:
检查F是否是有内嵌result_type定义的单参函数对象。
9:AdaptableBianryFunction<F,R,A,B>:
检查F是否是有内嵌result_type定义的双参函数对象。
10:AdaptablePredicate<F,A>:
检查F是否是有内嵌result_type定义的单参谓词。
11:AdaptableBinaryPredicate<F,A,B>:
检查F是否是有内嵌result_type定义的双参谓词。

标准迭代器概念检查:
标准迭代器概念检查类还定义了value_type,reference,pointer等内部类型，等价于std::iterator_traits<>。
1:InputIterator<I>:
检查I是否是输入迭代器。
2:OutputIterator<I,T>:
检查I是否是输出类型T的输出迭代器。
3:ForwardIterator<I>:
检查I是否是前向迭代器。
4:Mutable_ForwardIterator<I>:
检查I是否是可变前向迭代器（即可修改，支持*i++ = *i操作）。
5:BidirectionalIterator<I>:
检查I是否是双向迭代器。
6:Mutable_BidirectionalIterator<I>:
检查I是否是可变双向迭代器。
7:RandomAccessIterator<I>:
检查I是否是随机访问迭代器。
8:Mutable_RandomAccessIterator<I>:
检查I是否是可变随机访问迭代器。

新式迭代器概念检查：
1:ReadableIteratorConcept<I>:
检查I是否是可读迭代器。
2:WritableIteratorConcept<I,T>:
检查I是否是可写迭代器。
3:SwappableIteratorConcept<I>:
检查I是否是可交换迭代器。
4:LvalueIteratorConcept<I>:
检查I是否是左值迭代器。
5:IncrementableIteratorConcept<I>:
检查I是否是可递增迭代器。
6:SinglePassIteratorConcept<I>:
检查I是否是单遍迭代器。
7:ForwardTraversalConcept<I>:
检查I是否是前向迭代器。
8:BidirectionalTraversalConcept<I>:
检查I是否是双向迭代器。
9:RandomAccessTraversalConcept<I>:
检查I是否是随机访问遍历迭代器。

容器概念检查：
容器概念检查类检查是否符合标准库的容器定义，也就是说是否具有begin(),end(),empty(),size()等成员函数，是否有若干容器必备的内嵌类型定义。
这些容器概念检查类都有内部的value_type,reference等概念满足的类型定义，因此也可以把它们当作是容器的traits元函数。
基本的容器概念检查类如下：
1:Container<C>:
检查C是否满足标准容器定义。
2:Mutable_Container<C>:
检查C是否满足可变容器定义（即可以修改元素的值）。
3:ForwardContainer<C>:
检查C是否可以前向迭代。
4:Mutable_ForwardContainer<C>:
检查C是否满足可变前向迭代容器定义。
5:ReversibleContainer<C>:
检查C是否可以逆向迭代。
6:Mutable_ReversibleContainer<C>:
检查C是否满足可变逆向迭代容器定义。
7:RandomAccessContainer<C>:
检查C是否满足随机访问容器定义。
8:Mutable_RandomAccessContainer<C>:
检查C是否满足可变随机访问容器定义。

检查容器的序列类型：
1:Sequence<C>:
检查C是否是线性序列容器，如vector,deque。
2:FrontInsertionSequence<C>:
检查C是否支持序列头插入操作。
3:BackInsertionSequence<C>:
检查C是否支持序列尾插入操作。
4:AssociativeContainer<C>:
检查C是否是关联容器。
5:UniqueAssociativeContainer<C>:
检查C是否不允许重复键。
6:MultipleAssociativeContainer<C>:
检查C是否允许重复键。
7:SimpleAssociativeContainer<C>:
检查C是否键即值，即集合类型。
8:PairAssociativeContainer<C>:
检查C是否是键-值关联类型，即映射。
9:SortedAssociativeContainer<C>:
检查C是否是有序的。

区间概念检查：
区间概念检查类检查是否符合区间的概念-类似容器，但要求比容器要低一些，更接近迭代器概念。
1:SinglePassRangeConcept<R>:
检查R是否是单遍区间。
2:ForwardRangeConcept<R>:
检查R是否是前向区间。
3:WriteableForwardRangeConcept<R>:
检查R是否是可写前向区间。
4:BidirectionalRangeConcept<R>:
检查R是否是双向区间。
5:WriteableBidirectionalRangeConcept<R>:
检查R是否是可写双向区间。
6:RandomAccessRangeConcept<R>:
检查R是否是随机访问区间。
7:WriteableRandomAccessRangeConcept<R>:
检查R是否是可写随机访问区间。
*/