/*!
 * \brief
 * UTL的链表容器。
 * \file UTLList.h
 *
 * \author Su Yang
 *
 * \date 2016/01/27
 */
#ifndef _UNG_UTL_LIST_H_
#define _UNG_UTL_LIST_H_

#ifndef _UNG_UTL_CONFIG_H_
#include "UTLConfig.h"
#endif

#ifndef _UNG_UTL_ALLOCATOR_H_
#include "UTLAllocator.h"
#endif

#ifndef _UNG_UTL_LISTITERATOR_H_
#include "UTLListIterator.h"
#endif

namespace ung
{
	namespace utl
	{
		/*!
		 * \brief
		 * 链表结点的数据结构。
		 * \class ListNode
		 *
		 * \author Su Yang
		 *
		 * \date 2016/01/27
		 *
		 * \todo
		 */
		template<typename ElementType>
		class ListNode
		{
			/*!
			 * \remarks 友元函数，重载<<输出运算符
			 * \return friend std::ostream&
			 * \param ostream & o 
			 * \param ListNode<ElementType> const & ln 
			*/
			friend std::ostream& operator<<(std::ostream& o, ListNode<ElementType> const& ln)
			{
				return o << ln.mData;
			}

		public:
			using value_type = ElementType;
			using node_pointer = typename boost::add_pointer<ListNode<value_type>>::type;
			BOOST_STATIC_ASSERT(boost::is_pointer<node_pointer>::value);
			using self_type = ListNode<value_type>;

			/*!
			 * \remarks 默认构造函数
			 * \return 
			*/
			ListNode();
			/*!
			 * \remarks 构造函数
			 * \return 
			 * \param value_type const & data 容器元素类型的值
			*/
			ListNode(value_type const& data);
			/*!
			 * \remarks 构造函数
			 * \return 
			 * \param value_type && data 容器元素类型的右值
			*/
			ListNode(value_type&& data);
			/*!
			 * \remarks 拷贝构造函数
			 * \return 
			 * \param self_type const & right
			*/
			ListNode(self_type const& right);
			/*!
			 * \remarks 拷贝赋值运算符
			 * \return void
			 * \param self_type const & right 
			*/
			void operator=(self_type const& right);
			/*!
			 * \remarks 析构函数
			 * \return 
			*/
			~ListNode();

			node_pointer mPrev;
			node_pointer mNext;
			value_type mData;

		private:
			void priv_destructor(boost::true_type&);
			void priv_destructor(boost::false_type&);
			void priv_delete(boost::true_type&);
			void priv_delete(boost::false_type&);

			BOOST_STATIC_ASSERT(boost::is_class<ListNode<ElementType>>::value);
		};
		
		/*!
		 * \brief
		 * 链表内存空间分配器的包装类。
		 * \class ListAllocatorHolder
		 *
		 * \author Su Yang
		 *
		 * \date 2016/01/27
		 *
		 * \todo
		 */
		template<typename Alloc>
		class ListAllocatorHolder : public Alloc
		{
		public:
			using allocator_type = typename Alloc::allocator_type;
			using value_type = typename Alloc::value_type;
			using element_type = typename Alloc::element_type;
			using pointer = typename Alloc::pointer;
			using const_pointer = typename Alloc::const_pointer;
			using size_type = typename Alloc::size_type;
			using self_type = ListAllocatorHolder<Alloc>;

			/*!
			 * \remarks 构造函数
			 * \return 
			*/
			ListAllocatorHolder() noexcept(boost::has_nothrow_default_constructor<Alloc>::value);
			/*!
			 * \remarks 构造函数
			 * \return 
			 * \param size_type initialSize 初始空间大小
			*/
			explicit ListAllocatorHolder(size_type initialSize);
			/*!
			 * \remarks 构造函数
			 * \return 
			 * \param size_type initialSize 初始空间大小
			 * \param element_type const & elementValue 元素值
			*/
			ListAllocatorHolder(size_type initialSize, element_type const& elementValue);

			/*!
			 * \remarks 拷贝构造函数
			 * \return 
			 * \param self_type const & right 
			*/
			ListAllocatorHolder(self_type const& right);
			/*!
			 * \remarks 拷贝赋值运算符
			 * \return self_type&
			 * \param self_type const & right 
			*/
			self_type& operator=(self_type const& right);
			/*!
			 * \remarks 移动构造函数
			 * \return 
			 * \param self_type & & right 
			*/
			ListAllocatorHolder(self_type&& right);
			/*!
			 * \remarks 移动赋值运算符
			 * \return self_type&
			 * \param self_type & & right 
			*/
			self_type& operator=(self_type&& right);
			/*
			参数修饰词：
			如果为iteratorT const&的话，就不能执行++first的遍历。
			如果为iteratorT&的话，就会改变作为实参的迭代器变量。
			所以，这里参数应该按值传递。
			*/
			/*!
			 * \remarks 构造函数
			 * \return 
			 * \param iteratorT first 指向区间头元素的迭代器
			 * \param iteratorT last 指向区间尾后的迭代器
			 * \param typename boost::enable_if<std::_Is_iterator<iteratorT>param void>::type * 用于函数重载裁决
			*/
			template<class iteratorT>
			ListAllocatorHolder(iteratorT first, iteratorT last, typename boost::enable_if<std::_Is_iterator<iteratorT>, void>::type* = 0);

			/*!
			 * \remarks 构造函数
			 * \return 
			 * \param std::initializer_list<element_type> il 初始化列表
			*/
			ListAllocatorHolder(std::initializer_list<element_type> il);

			/*!
			 * \remarks 赋值，区间
			 * \return void
			 * \param iteratorT first 
			 * \param iteratorT last 
			 * \param typename boost::enable_if<std::_Is_iterator<iteratorT>,void>::type * 重载裁决
			*/
			template<class iteratorT>
			void assign(iteratorT first, iteratorT last, typename boost::enable_if<std::_Is_iterator<iteratorT>, void>::type* = 0);

			/*!
			 * \remarks 赋值，统一初始化
			 * \return void
			 * \param std::initializer_list<element_type> il 
			*/
			void assign(std::initializer_list<element_type> il);

			/*!
			 * \remarks 赋值
			 * \return void
			 * \param size_type count 元素数量
			 * \param element_type const & elementValue 元素值
			*/
			void assign(size_type count, element_type const& elementValue);
			/*!
			 * \remarks 析构函数
			 * \return 
			*/
			~ListAllocatorHolder() noexcept;

			/*!
			 * \remarks 获取指向容器头结点的指针，头结点不包含有效的元素数据
			 * \return pointer const&
			*/
			pointer const& head() const;
			/*!
			 * \remarks 获取指向容器尾结点的指针，尾结点不包含有效的元素数据
			 * \return pointer const&
			*/
			pointer const& tail() const;
			/*!
			 * \remarks 获取容器在内存空间的元素数量
			 * \return size_type const&
			*/
			size_type const& size() const;
			/*!
			 * \remarks 增加元素数量
			 * \return void
			 * \param pointer where 在where之前增加
			 * \param size_type increments 
			*/
			void incrementSize(pointer where, size_type increments = 1);
			/*!
			 * \remarks 减少元素数量
			 * \return pointer
			 * \param pointer where 
			 * \param size_type decrements where指向的元素将被删除掉
			*/
			pointer decrementSize(pointer where, size_type decrements = 1);

			/*!
			 * \remarks 重新设置大小
			 * \return void
			 * \param size_type newSize 
			*/
			void resize(size_type newSize);

			/*!
			 * \remarks 重新设置大小
			 * \return void
			 * \param size_type newSize 
			 * \param element_type const & elementValue 要填充的元素值
			*/
			void resize(size_type newSize,element_type const& elementValue);

			/*!
			 * \remarks 交换内存空间，包括表示数量的数据
			 * \return void
			 * \param self_type & right 
			*/
			void swap(self_type& right);

			/*!
			 * \remarks 插入元素
			 * \return pointer 返回指向插入结点的迭代器
			 * \param pointer where 在where之前插入结点
			 * \param element_type & & elementValue 右值
			*/
			pointer insert(pointer where, element_type&& elementValue);
			/*!
			 * \remarks 同上，左值
			 * \return pointer
			 * \param pointer where 
			 * \param element_type const & elementValue 
			*/
			pointer insert(pointer where, element_type const& elementValue);
			/*!
			 * \remarks 同上，初始化列表
			 * \return pointer
			 * \param pointer where 
			 * \param Arguments & & ... args 
			*/
			template<typename... Arguments>
			pointer insert(pointer where, Arguments&&... args);

			/*!
			 * \remarks 同上，迭代器区间
			 * \return ::type
			 * \param const_iterator where 
			 * \param iteratorT first 
			 * \param iteratorT last 
			*/
			template<typename iteratorT>
			typename boost::enable_if<std::_Is_iterator<iteratorT>, pointer>::type
			insert(pointer where, iteratorT first, iteratorT last,bool moveFlag = false);

			/*!
			 * \remarks 同上，插入count个相同的值
			 * \return pointer
			 * \param pointer where 
			 * \param size_type count 
			 * \param const element_type & elementVal 
			*/
			pointer insert(pointer where, size_type count, const element_type& elementVal);

			/*!
			 * \remarks 同上，右值，定位new，不产生分配空间动作
			 * \return pointer
			 * \param pointer where 
			 * \param element_type & & elementValue 
			*/
			pointer place(pointer where, element_type&& elementValue);

		private:
			pointer mHead;
			pointer mTail;
			size_type mSize;
		};

		/*!
		 * \brief
		 * UTL的链表容器
		 * \class List
		 *
		 * \author Su Yang
		 *
		 * \date 2016/01/28
		 *
		 * \todo
		 */
		template<typename T,typename Alloc = Allocator<ListNode<T>,T>>
		class List
		{
		public:
			using element_type = typename Alloc::element_type;
			using value_type = typename Alloc::value_type;
			using allocator_type = typename Alloc::allocator_type;
			using allocator_holder = ListAllocatorHolder<Alloc>;
			using pointer = typename Alloc::pointer;
			using const_pointer = typename Alloc::const_pointer;
			using reference = typename Alloc::reference;
			using const_reference = typename Alloc::const_reference;
			using reference_r = typename Alloc::reference_r;
			using size_type = typename Alloc::size_type;
			using difference_type = typename Alloc::difference_type;
			using self_type = List<element_type, Alloc>;
			typedef ListIterator<value_type, element_type> iterator;
			BOOST_STATIC_ASSERT(std::_Is_iterator<iterator>::value);
			typedef ListIterator<const value_type, const element_type> const_iterator;
			typedef boost::reverse_iterator<iterator> reverse_iterator;
			typedef boost::reverse_iterator<const_iterator> const_reverse_iterator;

		private:
			allocator_holder mHolder;
			
		public:
			/*!
			 * \remarks 默认构造函数
			 * \return  
			*/
			List():
				mHolder(allocator_holder())
			{
			}

			/*!
			 * \remarks 构造函数
			 * \return  
			 * \param size_type count 数量
			*/
			explicit List(size_type count) :
				mHolder(allocator_holder(count))
			{
			}

			/*!
			 * \remarks 构造函数
			 * \return  
			 * \param size_type count 数量
			 * \param const element_type & elementVal 元素值
			*/
			List(size_type count, const element_type& elementVal) :
				mHolder(allocator_holder(count, elementVal))
			{
			}

			/*!
			 * \remarks 拷贝构造函数
			 * \return  
			 * \param self_type const & right 
			*/
			List(self_type const& right) :
				mHolder(allocator_holder(right.mHolder))
			{
			}

			/*!
			 * \remarks 拷贝赋值运算符
			 * \return self_type& 
			 * \param self_type const & right 
			*/
			self_type& operator=(self_type const& right)
			{
				if (this != &right)
				{
					mHolder = right.mHolder;
				}

				return *this;
			}

			/*!
			 * \remarks 移动构造函数
			 * \return  
			 * \param self_type && right 
			*/
			List(self_type&& right) :
				mHolder(allocator_holder(std::move(right.mHolder)))
			{
			}

			/*!
			 * \remarks 移动赋值运算符
			 * \return self_type& 
			 * \param self_type && right 
			*/
			self_type& operator=(self_type&& right)
			{
				if (this != &right)
				{
					mHolder = std::move(right.mHolder);
				}

				return *this;
			}

			/*!
			 * \remarks 构造函数
			 * \return  
			 * \param iteratorT first 指向区间头的迭代器
			 * \param iteratorT last 指向区间尾后的迭代器
			 * \param typename boost::enable_if<std::_Is_iterator<iteratorT>param void>::type * 用于函数重载裁决
			*/
			template<class iteratorT>
			List(iteratorT first, iteratorT last, typename boost::enable_if<std::_Is_iterator<iteratorT>, void>::type* = 0) :
				mHolder(allocator_holder(first, last))
			{
			}

			/*!
			 * \remarks 构造函数
			 * \return 
			 * \param std::initializer_list<element_type> il 初始化列表
			*/
			List(std::initializer_list<element_type> il) :
				mHolder(allocator_holder(il))
			{
			}

			/*!
			 * \remarks 重载赋值运算符
			 * \return self_type&
			 * \param std::initializer_list<element_type> il 
			*/
			self_type& operator=(std::initializer_list<element_type> il)
			{
				assign(il);

				return *this;
			}
				
			/*!
			 * \remarks 赋值，统一初始化
			 * \return void
			 * \param std::initializer_list<element_type> il 
			*/
			void assign(std::initializer_list<element_type> il)
			{
				mHolder.assign(il);
			}

			/*!
			 * \remarks 赋值，区间
			 * \return ::type
			 * \param iteratorT first 
			 * \param iteratorT last 
			*/
			template<class iteratorT>
			typename boost::enable_if<std::_Is_iterator<iteratorT>,void>::type
			assign(iteratorT first, iteratorT last)
			{
				mHolder.assign(first, last);
			}

			/*!
			 * \remarks 赋值
			 * \return void
			 * \param size_type count 数量
			 * \param const element_type & elementVal 元素值
			*/
			void assign(size_type count, const element_type& elementVal)
			{
				mHolder.assign(count, elementVal);
			}


			~List() noexcept
			{
				mHolder.~ListAllocatorHolder();
			}

			//-------------------------------------------------

			/*!
			 * \remarks 获取指向容器首元素的迭代器
			 * \return iterator 
			*/
			iterator begin()
			{
				return mHolder.head()->mNext;
			}

			/*!
			 * \remarks 获取指向容器首元素的常量迭代器，常量对象调用
			 * \return const_iterator 
			*/
			const_iterator begin() const
			{
				return cbegin();
			}

			/*!
			 * \remarks 获取指向容器首元素的常量迭代器
			 * \return const_iterator 
			*/
			const_iterator cbegin() const
			{
				return mHolder.head()->mNext;
			}

			/*!
			 * \remarks 获取指向容器尾后的迭代器
			 * \return reverse_iterator 
			*/
			reverse_iterator rbegin()
			{
				return (reverse_iterator(end()));
			}
			
			/*!
			 * \remarks 获取指向容器首尾后的常量迭代器，常量对象调用
			 * \return const_reverse_iterator 
			*/
			const_reverse_iterator rbegin() const
			{
				return (const_reverse_iterator(end()));
			}

			/*!
			 * \remarks 获取指向容器尾后的常量迭代器
			 * \return const_reverse_iterator 
			*/
			const_reverse_iterator crbegin() const
			{
				return (rbegin());
			}

			/*!
			 * \remarks 获取指向容器尾后的迭代器
			 * \return iterator 
			*/
			iterator end()
			{
				return mHolder.tail();
			}

			/*!
			 * \remarks 获取指向容器尾后的常量迭代器，常量对象调用
			 * \return const_iterator 
			*/
			const_iterator end() const
			{
				return cend();
			}

			/*!
			 * \remarks 获取指向容器尾后的常量迭代器
			 * \return const_iterator 
			*/
			const_iterator cend() const
			{
				return mHolder.tail();
			}

			/*!
			 * \remarks 获取指向容器首元素的迭代器
			 * \return reverse_iterator 
			*/
			reverse_iterator rend()
			{
				return (reverse_iterator(begin()));
			}

			/*!
			 * \remarks 获取指向容器首元素的常量迭代器，常量对象调用
			 * \return const_reverse_iterator 
			*/
			const_reverse_iterator rend() const
			{
				return (const_reverse_iterator(begin()));
			}

			/*!
			 * \remarks 获取指向容器首元素的常量迭代器
			 * \return const_reverse_iterator 
			*/
			const_reverse_iterator crend() const
			{
				return (rend());
			}

			//-------------------------------------------------

			/*!
			 * \remarks 在容器首元素之前创建一个由args构造的结点
			 * \return void 
			 * \param Arguments && ... args 
			*/
			template<typename... Arguments>
			void emplace_front(Arguments&&... args)
			{
				mHolder.insert(begin(), std::forward(<Arguments>(args)...));
			}

			/*!
			 * \remarks 在容器的尾后位置创建一个由args构造的结点
			 * \return void 
			 * \param Arguments && ... args 
			*/
			template<typename... Arguments>
			void emplace_back(Arguments&&... args)
			{
				mHolder.insert(end(), std::forward(<Arguments>(args)...));
			}

			/*!
			 * \remarks 在容器中where位置之前创建一个由args构造的结点
			 * \return iterator 返回指向新插入结点的迭代器
			 * \param const_iterator where 
			 * \param Arguments && ... args 
			*/
			template<typename... Arguments>
			iterator emplace(const_iterator where, Arguments&&... args)
			{
				return mHolder.insert(where.getPointer(), std::forward<Arguments>(args)...);
			}

			/*!
			 * \remarks 在容器中where位置之前插入一个结点
			 * \return iterator 返回指向新插入结点的迭代器
			 * \param const_iterator where 
			 * \param element_type && elementVal 右值
			*/
			iterator insert(const_iterator where, element_type&& elementVal)
			{
				/*
				注意：迭代器对象调用自己的成员函数和调用其所引用的对象的成员函数的区别。
				*/
				return mHolder.insert(where.getPointer(), elementVal);
			}

			/*!
			 * \remarks 同上，初始化列表
			 * \return iterator 
			 * \param const_iterator where 
			 * \param std::initializer_list<element_type> il 
			*/
			iterator insert(const_iterator where,std::initializer_list<element_type> il)
			{
				return insert(where, il.begin(), il.end());
			}

			/*!
			 * \remarks 同上，左值引用
			 * \return iterator 
			 * \param const_iterator where 
			 * \param const element_type & elementVal 
			*/
			iterator insert(const_iterator where, const element_type& elementVal)
			{
				return mHolder.insert(where.getPointer(), elementVal);
			}
			
			/*!
			 * \remarks 同上
			 * \return iterator 
			 * \param const_iterator where 
			 * \param size_type count 数量
			 * \param const element_type & elementVal 
			*/
			iterator insert(const_iterator where,size_type count, const element_type& elementVal)
			{
				return mHolder.insert(where,count,elementVal);
			}
			
			/*!
			 * \remarks 同上，区间
			 * \return ::type 用于函数重载裁决
			 * \param const_iterator where 
			 * \param iteratorT first 
			 * \param iteratorT last 
			*/
			template<typename iteratorT>
			typename boost::enable_if<std::_Is_iterator<iteratorT>,iterator>::type
			insert(const_iterator where, iteratorT first, iteratorT last)
			{
				return mHolder.insert(where,first,last);
			}

			/*!
			 * \remarks 在容器首元素之前压入一个元素
			 * \return void
			 * \param const element_type & elementVal 左值引用
			*/
			void push_front(const element_type& elementVal)
			{
				mHolder.insert(begin(),elementVal);
			}

			/*!
			 * \remarks 在容器首元素之前压入一个元素
			 * \return void
			 * \param element_type && elementVal 右值引用
			*/
			void push_front(element_type&& elementVal)
			{
				mHolder.insert(begin(),std::forward<element_type>(elementVal));
			}

			/*!
			 * \remarks 在容器尾元素之后(尾后处)压入一个元素
			 * \return void
			 * \param const T & elementVal 左值引用
			*/
			void push_back(const T& elementVal)
			{
				mHolder.insert(end(),elementVal);
			}

			/*!
			 * \remarks 在容器尾元素之后(尾后处)压入一个元素
			 * \return void
			 * \param element_type && elementVal 右值引用
			*/
			void push_back(element_type&& elementVal)
			{
				mHolder.insert(end(),std::forward<element_type>(elementVal));
			}

			/*!
			 * \remarks 返回mutable序列的第一个元素
			 * \return reference
			*/
			reference front()
			{
				return (*begin());
			}

			/*!
			 * \remarks 返回nonmutable序列的第一个元素
			 * \return const_reference
			*/
			const_reference front() const
			{
				return (*begin());
			}

			/*!
			 * \remarks 返回mutable序列的最后一个元素
			 * \return reference
			*/
			reference back()
			{
				return (*(--end()));
			}

			/*!
			 * \remarks 返回nonmutable序列的最后一个元素
			 * \return const_reference
			*/
			const_reference back() const
			{
				return (*(--end()));
			}

			//------------------------------------------------------------------

			/*!
			 * \remarks 获取元素数量
			 * \return size_type
			*/
			size_type size() const noexcept
			{
				return mHolder.size();
			}

			/*!
			 * \remarks 容器是否为空
			 * \return bool
			*/
			bool empty() const noexcept
			{
				return mHolder.size() == 0;
			}

			//--------------------------------------------------------------------

			/*!
			 * \remarks 重新设置容器大小
			 * \return void
			 * \param size_type newSize 
			*/
			void resize(size_type newSize)
			{
				mHolder.resize(newSize);
			}

			/*!
			 * \remarks 重新设置容器大小
			 * \return void
			 * \param size_type _Newsize 
			 * \param const element_type& elementVal 填充值
			*/
			void resize(size_type newSize, const element_type& elementVal)
			{
				mHolder.resize(newSize, elementVal);
			}

			//-----------------------------------------------------------------------------

			/*!
			 * \remarks 删除迭代器所指向的元素
			 * \return std::iterator 返回一个指向被删元素之后元素的迭代器
			 * \param const_iterator where 
			*/
			iterator erase(const_iterator where)
			{
				return mHolder.decrementSize(where);
			}

			/*!
			 * \remarks 删除迭代器区间所指向的元素
			 * \return std::iterator 返回一个指向被删元素之后元素的迭代器
			 * \param const_iterator first 
			 * \param const_iterator last 
			*/
			iterator erase(const_iterator first, const_iterator last)
			{
				iterator it;
				while (first != last)
				{
					it = erase(first++);
				}

				return it;
			}

			/*!
			 * \remarks 删除容器中的所有元素
			 * \return void
			*/
			void clear() noexcept
			{
				erase(cbegin(), cend());
			}

			/*!
			 * \remarks 删除容器的首元素
			 * \return void
			*/
			void pop_front()
			{
				erase(begin());
			}

			/*!
			 * \remarks 删除容器的最后一个有效元素
			 * \return void
			*/
			void pop_back()
			{
				erase(--end());
			}

			//-----------------------------------------------------------------------------

			/*!
			 * \remarks 拼接
			 * \return void
			 * \param const_iterator where 
			 * \param self_type & right 
			*/
			void splice(const_iterator where, self_type& right)
			{
				mHolder.insert(where,right.begin,right.end())
			}

			/*!
			 * \remarks 同上
			 * \return void
			 * \param const_iterator where 
			 * \param self_type && right 右值引用
			*/
			void splice(const_iterator where, self_type&& right)
			{
				mHolder.insert(where,right.begin,right.end(),true)
			}

			/*!
			 * \remarks 拼接，只拼接一个元素
			 * \return void
			 * \param const_iterator where 
			 * \param self_type & right 
			 * \param const_iterator first
			*/
			void splice(const_iterator where, self_type& right,const_iterator first)
			{
				assert(first != right.end());
				mHolder.insert(where,first,first + 1)
			}

			/*!
			 * \remarks 同上
			 * \return void
			 * \param const_iterator where 
			 * \param self_type && right 右值引用
			 * \param const_iterator first 
			*/
			void splice(const_iterator where, self_type&& right,const_iterator first)
			{
				assert(first != right.end());
				mHolder.insert(where, first, first + 1,true)
			}

			void splice(const_iterator where,self_type& right, const_iterator first, const_iterator last)
			{
				assert(first != last && this != &right);
				mHolder.insert(where, first, last)
			}

			void splice(const_iterator where,self_type&& right, const_iterator first, const_iterator last)
			{
				assert(first != last && this != &right);
				mHolder.insert(where, first, last,true)
			}

			//----------------------------------------------------------------------------------------------

			//void merge(self_type& right);
			//void merge(self_type&& right);
			//template<class Pred>
			//void merge(self_type& right, Pred p);
			//template<class Pred>
			//void merge(self_type&& right, Pred p);

			//-----------------------------------------------------------------------------------------------

			void swap(self_type& right)
			{
				mHolder.swap(right);
			}

			//void remove(const element_type& elementVal);
			//template<class Pred>
			//void remove_if(Pred p);

			//void unique();
			//template<class Pred>
			//void unique(Pred p);

			//void sort();
			//template<class Pred>
			//void sort(Pred p);

			//void reverse() noexcept;

		};
	}//namespace utl
}//namespace ung

#include "UTLListDef.h"

#endif//_UNG_UTL_LIST_H_