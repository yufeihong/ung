/*!
 * \brief
 * 空间管理器基类。
 * \file USMSpatialManagerBase.h
 *
 * \author Su Yang
 *
 * \date 2017/03/27
 */
#ifndef _USM_SPATIALMANAGERBASE_H_
#define _USM_SPATIALMANAGERBASE_H_

#ifndef _USM_CONFIG_H_
#include "USMConfig.h"
#endif

#ifdef USM_HIERARCHICAL_COMPILE

#ifndef _UIF_ISPATIALMANAGER_H_
#include "UIFISpatialManager.h"
#endif

namespace ung
{
	class Camera;

	/*!
	 * \brief
	 * 空间管理器基类
	 * \class SpatialManagerBase
	 *
	 * \author Su Yang
	 *
	 * \date 2017/03/27
	 *
	 * \todo
	 */
	class UngExport SpatialManagerBase : public ISpatialManager
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		SpatialManagerBase() = default;

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~SpatialManagerBase() = default;

		/*!
		 * \remarks 获取创建该空间管理器的工厂
		 * \return 
		*/
		virtual ISpatialManagerFactory* getCreator() const override;

		/*!
		 * \remarks 获取空间管理器工厂的标识
		 * \return 
		*/
		virtual String const& getFactoryIdentify() const override;

		/*!
		 * \remarks 获取空间管理器工厂的掩码
		 * \return 
		*/
		virtual SceneTypeMask getFactoryMask() const override;

		/*!
		 * \remarks 获取空间管理器的名字
		 * \return 
		*/
		virtual String const& getManagerName() const override;

		/*!
		 * \remarks 创建节点
		 * \return 
		 * \param ISpatialNode * parent
		*/
		virtual ISpatialNode* createNode(ISpatialNode* parent) override;

		/*!
		 * \remarks 销毁节点
		 * \return 
		 * \param ISpatialNode * node
		*/
		virtual void destroyNode(ISpatialNode* node) override;

		/*!
		 * \remarks 获取空间树的根节点
		 * \return 
		*/
		virtual ISpatialNode* getRootNode() const override;

		/*!
		 * \remarks 将对象放置到空间管理器中
		 * \return 返回对象所关联的节点
		 * \param StrongIObjectPtr objectPtr
		*/
		virtual ISpatialNode* placeObject(StrongIObjectPtr objectPtr) override;

		/*!
		 * \remarks 将对象从空间管理器中拿走
		 * \return 
		 * \param StrongIObjectPtr objectPtr
		*/
		virtual void takeAwayObject(StrongIObjectPtr objectPtr) override;

		/*!
		 * \remarks 创建射线场景查询
		 * \return 
		 * \param Ray const & ray
		*/
		virtual StrongISceneQueryPtr createRaySceneQuery(Ray const& ray) override;

		/*!
		 * \remarks 创建球体场景查询
		 * \return 
		 * \param Sphere const & sphere
		*/
		virtual StrongISceneQueryPtr createSphereSceneQuery(Sphere const& sphere) override;

		/*!
		 * \remarks 创建AABB场景查询
		 * \return 
		 * \param AABB const & box
		*/
		virtual StrongISceneQueryPtr createAABBSceneQuery(AABB const& box) override;

		/*!
		 * \remarks 创建摄像机场景查询
		 * \return 
		 * \param Camera const & camera
		*/
		virtual StrongISceneQueryPtr createCameraSceneQuery(Camera const& camera) override;
	};
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE

#endif//_USM_SPATIALMANAGERBASE_H_