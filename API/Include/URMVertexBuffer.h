/*!
 * \brief
 * vertex buffer
 * \file URMVertexBuffer.h
 *
 * \author Su Yang
 *
 * \date 2017/05/30
 */
#ifndef _URM_VERTEXBUFFER_H_
#define _URM_VERTEXBUFFER_H_

#ifndef _URM_CONFIG_H_
#include "URMConfig.h"
#endif

#ifdef URM_HIERARCHICAL_COMPILE

#ifndef _URM_VIDEOBUFFER_H_
#include "URMVideoBuffer.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * vertex buffer(Act as a source of geometry data for rendering objects)
	 * \class VertexBuffer
	 *
	 * \author Su Yang
	 *
	 * \date 2017/05/30
	 *
	 * \todo
	 */
	class UngExport VertexBuffer : public VideoBuffer
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		VertexBuffer();

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param uint32 vertexSize 一个顶点的大小，单位：字节
		 * \param uint32 numVertices 顶点数量
		 * \param uint32 usage
		 * \param bool useShadowBuffer If set to true,this buffer will be 'shadowed' by one stored 
		 * in system memory rather than GPU or AGP memory.You should set this flag if you 
		 * intend to read data back from the vertex buffer,because reading data from a buffer in 
		 * the GPU or AGP memory is very expensive,and is in fact impossible if you specify 
		 * VB_USAGE_WRITE_ONLY for the main buffer.If you use this option,all reads and writes 
		 * will be done to the shadow buffer,and the shadow buffer will be synchronised with the 
		 * real buffer at an appropriate time.
		*/
		VertexBuffer(uint32 vertexSize, uint32 numVertices,uint32 usage, bool useShadowBuffer);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~VertexBuffer();

		/*!
		 * \remarks 获取一个顶点的大小。单位：字节
		 * \return 
		*/
		uint32 getVertexSize() const;

		/*!
		 * \remarks 获取顶点的数量
		 * \return 
		*/
		uint32 getVerticesNum() const;

	protected:
		uint32 mVertexSize = 0;
		uint32 mVerticesNum = 0;
	};
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

#endif//_URM_VERTEXBUFFER_H_

//用智能指针包装一下，以便于对vertex buffer的shared.