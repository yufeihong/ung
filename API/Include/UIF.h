/*!
 * \brief
 * 接口层
 * 客户代码包含这个头文件。
 * \file UIF.h
 *
 * \author Su Yang
 *
 * \date 2017/04/01
 */
#ifndef _UIF_H_
#define _UIF_H_

#ifndef _UIF_ENABLE_H_
#include "UIFEnable.h"
#endif

#ifdef UIF_HIERARCHICAL_COMPILE

#ifndef _UIF_IMOVABLE_H_
#include "UIFIMovable.h"
#endif

#ifndef _UIF_IOBJECT_H_
#include "UIFIObject.h"
#endif

#ifndef _UIF_IOBJECTCOMPONENT_H_
#include "UIFIObjectComponent.h"
#endif

#ifndef _UIF_ISCENENODE_H_
#include "UIFISceneNode.h"
#endif

#ifndef _UIF_ISCENEMANAGER_H_
#include "UIFISceneManager.h"
#endif

#ifndef _UIF_ISPATIALNODE_H_
#include "UIFISpatialNode.h"
#endif

#ifndef _UIF_ISPATIALMANAGER_H_
#include "UIFISpatialManager.h"
#endif

#ifndef _UIF_ISPATIALMANAGERFACTORY_H_
#include "UIFISpatialManagerFactory.h"
#endif

#ifndef _UIF_IBOUNDINGVOLUME_H_
#include "UIFIBoundingVolume.h"
#endif

#ifndef _UIF_SCENEQUERY_H_
#include "UIFISceneQuery.h"
#endif

#ifndef _UIF_ISCENEQUERYFACTORY_H_
#include "UIFISceneQueryFactory.h"
#endif

#ifndef _UIF_IRENDERSYSTEM_H_
#include "UIFIRenderSystem.h"
#endif

//#ifndef _UIF_IPIXELFORMAT_H_
//#include "UIFIPixelFormat.h"
//#endif

#ifndef _UIF_ISURFACE_H_
#include "UIFISurface.h"
#endif

#ifndef _UIF_IRENDERTARGET_H_
#include "UIFIRenderTarget.h"
#endif

#ifndef _UIF_IWINDOW_H_
#include "UIFIWindow.h"
#endif

#endif//UIF_HIERARCHICAL_COMPILE

#endif//_UIF_H_