/*!
 * \brief
 * 汇编代码计算常用数学运算
 * \file UMLASM.h
 *
 * \author Su Yang
 *
 * \date 2016/06/15
 */
#ifndef _UML_ASM_H_
#define _UML_ASM_H_

#ifndef _UML_CONFIG_H_
#include "UMLConfig.h"
#endif

#ifdef UML_HIERARCHICAL_COMPILE

namespace ung
{
	/*!
	 * \brief
	 * 汇编代码计算常用数学运算
	 * \class ASM
	 *
	 * \author Su Yang
	 *
	 * \date 2016/06/15
	 *
	 * \todo
	 */
	class ASM
	{
	public:
		/*!
		 * \remarks 计算sin
		 * \return 
		 * \param real_type r
		*/
		static real_type asm_sin(real_type r)
		{
#if  UNG_COMPILER == UNG_COMPILER_MSVC &&  UNG_ARCH_TYPE == UNG_ARCHITECTURE_32 && UNG_CPU == UNG_CPU_X86
			__asm
			{
				fld r
				fsin
			}
#else
			return sin(r);
#endif
		}

		/*!
		 * \remarks 计算cos
		 * \return 
		 * \param real_type r
		*/
		static real_type asm_cos(real_type r)
		{
#if  UNG_COMPILER == UNG_COMPILER_MSVC &&  UNG_ARCH_TYPE == UNG_ARCHITECTURE_32 && UNG_CPU == UNG_CPU_X86
			__asm
			{
				fld r
				fcos
			}
#else
			return cos(r);
#endif
		}

		/*!
		 * \remarks 对r进行开平方,然后再进行倒数
		 * \return 
		 * \param real_type r
		*/
		static real_type asm_invSqrt(real_type r)
		{
#if  UNG_COMPILER == UNG_COMPILER_MSVC &&  UNG_ARCH_TYPE == UNG_ARCHITECTURE_32 && UNG_CPU == UNG_CPU_X86
			__asm
			{
				fld1
				fld r
				fsqrt
				fdiv
			}
#else
			return 1.0 / sqrt(r);
#endif
		}
	};
}//namespace ung

#endif//UML_HIERARCHICAL_COMPILE

#endif//_UML_ASM_H_