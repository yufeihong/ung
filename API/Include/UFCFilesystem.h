/*!
 * \brief
 * 文件系统。
 * 在Linux中，一切（几乎一切）都是文件。
 * \file UFCFilesystem.h
 *
 * \author Su Yang
 *
 * \date 2016/01/29
 */
#ifndef _UFC_FILESYSTEM_H_
#define _UFC_FILESYSTEM_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#ifndef _UFC_CHOOSECONTAINER_H_
#include "UFCChooseContainer.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 封装不同平台文件系统的路径。
	 * \class Filesystem
	 *
	 * \author Su Yang
	 *
	 * \date 2016/01/29
	 *
	 * \todo
	 */
	class UngExport Filesystem : public Singleton<Filesystem>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		Filesystem();

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~Filesystem() = default;

		/*!
		 * \remarks 路径或文件是否存在
		 * \return bool
		 * \param char const* 文件fullname
		*/
		bool isExists(char const*) const;

		/*!
		 * \remarks 是否是一个目录
		 * \return bool
		 * \param char const* 路径名
		*/
		bool isDirectory(char const*) const;

		/*!
		 * \remarks 是否是一个普通文件
		 * \return bool
		 * \param char const * 文件fullname
		*/
		bool isRegularFile(char const*) const;

		/*!
		 * \remarks 获取普通文件的大小，单位为字节
		 * \return uintmax
		 * \param char const* 文件fullname
		*/
		uintmax getFileSize(char const*) const;

		/*!
		 * \remarks 获取路径下所有文件的filename，对路径进行迭代
		 * \return std::shared_ptr<STL_VECTOR(String)> 指向存放字符串的容器的指针
		 * \param char const* 路径名
		*/
		std::shared_ptr<STL_VECTOR(String)> listFileNameRecursive(char const*) const;

		/*!
		 * \remarks 获取路径下所有文件的fullname，对路径进行迭代
		 * \return std::shared_ptr<STL_VECTOR(String)> 指向存放字符串的容器的指针
		 * \param char const* 路径名
		*/
		std::shared_ptr<STL_VECTOR(String)> listFileFullNameRecursive(char const*) const;

		/*!
		 * \remarks 路径是否为空
		 * \return bool
		 * \param char const * 路径名
		*/
		bool isEmpty(char const*) const;
		/*!
		 * \remarks 是否是绝对路径
		 * \return bool
		 * \param char const * 路径名
		*/
		bool isAbsolute(char const*) const;
		/*!
		 * \remarks 是否包含根路径
		 * \return bool
		 * \param char const * 文件fullname或路径名
		*/
		bool hasRootPath(char const*) const;
		/*!
		 * \remarks 是否包含父路径
		 * \return bool
		 * \param char const * 文件fullname或路径名
		*/
		bool hasParentPath(char const*) const;
		/*!
		 * \remarks 是否包含文件名
		 * \return bool
		 * \param char const * 文件fullname或路径名
		*/
		bool hasFileName(char const*) const;
		/*!
		 * \remarks 是否包含文件基本名
		 * \return bool
		 * \param char const * 文件fullname或路径名
		*/
		bool hasStem(char const*) const;
		/*!
		 * \remarks 是否包含文件扩展名
		 * \return bool
		 * \param char const * 文件fullname或路径名
		*/
		bool hasExtension(char const*) const;

		/*!
		 * \remarks 获取根目录
		 * \return String
		 * \param char const * 文件fullname或路径名
		*/
		String getRootPath(char const*) const;

		/*!
		 * \remarks 获取父目录
		 * \return String
		 * \param char const * 文件fullname或路径名
		*/
		String getParentPath(char const*) const;

		/*!
		 * \remarks 获取文件名
		 * \return void
		 * \param char const * 文件fullname
		*/
		String getFileName(char const*) const;

		/*!
		 * \remarks 获取文件名,不含扩展名
		 * \return String
		 * \param char const * 文件fullname
		*/
		String getFileStem(char const*) const;

		/*!
		 * \remarks 获取文件扩展名
		 * \return String
		 * \param char const * 文件fullname
		*/
		String getFileExtension(char const*) const;

		/*!
		 * \remarks 获取路径下的磁盘可用大小，单位为字节。
		 * \return uintmax
		 * \param char const * 路径名
		*/
		uintmax getSpaceFree(char const*) const;

		/*!
		 * \remarks 创建目录，可以一次创建多级目录。
		 * \return bool
		 * \param char const * 路径名
		*/
		bool createDir(char const*) const;

		/*!
		 * \remarks 处理路径：1，小写。2：使用"/"分隔符。3：以"./"或"../"开头。4：/目录以"/"结尾
		 * \return 
		 * \param String & str
		*/
		void processDirSeparator(String& str);
	};
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_FILESYSTEM_H_