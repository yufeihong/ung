/*!
 * \brief
 * 空间管理器的枚举器
 * \file USMSpatialManagerEnumerator.h
 *
 * \author Su Yang
 *
 * \date 2017/03/27
 */
#ifndef _USM_SPATIALMANAGERENUMERATOR_H_
#define _USM_SPATIALMANAGERENUMERATOR_H_

#ifndef _USM_CONFIG_H_
#include "USMConfig.h"
#endif

#ifdef USM_HIERARCHICAL_COMPILE

namespace ung
{
	/*!
	 * \brief
	 * 空间管理器的枚举器
	 * \class SpatialManagerEnumerator
	 *
	 * \author Su Yang
	 *
	 * \date 2017/03/27
	 *
	 * \todo
	 */
	class UngExport SpatialManagerEnumerator : public Singleton<SpatialManagerEnumerator>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		SpatialManagerEnumerator();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~SpatialManagerEnumerator();

		/*!
		 * \remarks 添加空间管理器工厂
		 * \return 
		 * \param ISpatialManagerFactory * factoryPtr
		*/
		void addFactory(ISpatialManagerFactory* factoryPtr);

		/*!
		 * \remarks 移除空间管理器工厂
		 * \return 
		 * \param ISpatialManagerFactory * factoryPtr
		*/
		void removeFactory(ISpatialManagerFactory* factoryPtr);

		/*!
		 * \remarks 创建参数所给定的空间管理器
		 * \return 
		 * \param String const & factoryIdentity 用于identify工厂
		 * \param String const & instanceName
		*/
		ISpatialManager* createSpatialManager(String const& factoryIdentity, String const& instanceName = EMPTY_STRING);

		/*!
		 * \remarks 创建参数所给定的空间管理器
		 * \return 
		 * \param SceneTypeMask mask 用于identify工厂
		 * \param String const & instanceName
		*/
		ISpatialManager* createSpatialManager(SceneTypeMask mask, String const& instanceName = EMPTY_STRING);

		/*!
		 * \remarks 销毁参数所给定的空间管理器
		 * \return 
		 * \param ISpatialManager * spatialManagerPtr
		*/
		void destroySpatialManager(ISpatialManager* spatialManagerPtr);

		/*!
		 * \remarks 获取参数所给定的空间管理器
		 * \return 
		 * \param String const & instanceName
		*/
		ISpatialManager* getSpatialManager(String const& instanceName) const;

		/*!
		 * \remarks 判断参数所给定的空间管理器是否存在
		 * \return 
		 * \param String const & instanceName
		*/
		bool hasSpatialManager(String const& instanceName) const;

		/*!
		 * \remarks 判断参数所给定的空间管理器是否存在
		 * \return 
		 * \param ISpatialManagerFactory * factory
		*/
		bool hasSpatialManager(ISpatialManagerFactory* factory) const;

	private:
		STL_UNORDERED_MAP(String,ISpatialManagerFactory*) mSpatialManagerFactorys;
		STL_UNORDERED_MAP(String,ISpatialManager*) mSpatialManagers;
	};
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE

#endif//_USM_SPATIALMANAGERENUMERATOR_H_