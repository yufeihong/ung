/*!
 * \brief
 * 刚体。
 * \file UPERigidBody.h
 *
 * \author Su Yang
 *
 * \date 2016/12/03
 */
#ifndef _UPE_RIGIDBODY_H_
#define _UPE_RIGIDBODY_H_

#ifndef _UPE_CONFIG_H_
#include "UPEConfig.h"
#endif

#ifdef UPE_HIERARCHICAL_COMPILE

namespace ung
{
	/*!
	 * \brief
	 * 刚体。
	 * \class RigidBody
	 *
	 * \author Su Yang
	 *
	 * \date 2016/12/03
	 *
	 * \todo
	 */
	class RigidBody : public btRigidBody
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		RigidBody() = delete;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param real_type mass
		 * \param btMotionState * transformationPtr
		 * \param btCollisionShape * collisionShapePtr
		*/
		UngExport RigidBody(real_type mass, btMotionState* transformationPtr, btCollisionShape* collisionShapePtr);
	
		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual UngExport ~RigidBody();
	};
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

#endif//_UPE_RIGIDBODY_H_