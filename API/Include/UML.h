/*!
 * \brief
 * 数学库。
 * 客户代码包含这个头文件。
 * \file UML.h
 *
 * \author Su Yang
 *
 * \date 2016/06/16
 */
#ifndef _UML_H_
#define _UML_H_

#ifndef _UML_ENABLE_H_
#include "UMLEnable.h"
#endif

#ifdef UML_HIERARCHICAL_COMPILE

#ifndef _UML_CONSTS_H_
#include "UMLConsts.h"
#endif

#ifndef _UML_MATH_H_
#include "UMLMath.h"
#endif

#ifndef _UML_ANGLE_H_
#include "UMLAngle.h"
#endif

#ifndef _UML_VEC2_H_
#include "UMLVec2.h"
#endif

#ifndef _UML_VEC3_H_
#include "UMLVec3.h"
#endif

#ifndef _UML_VEC4_H_
#include "UMLVec4.h"
#endif

#ifndef _UML_MAT2_H_
#include "UMLMat2.h"
#endif

#ifndef _UML_MAT3_H_
#include "UMLMat3.h"
#endif

#ifndef _UML_MAT4_H_
#include "UMLMat4.h"
#endif

#ifndef _UML_QUATER_H_
#include "UMLQuater.h"
#endif

#ifndef _UML_SQT_H_
#include "UMLSQT.h"
#endif

#ifndef _UML_TYPEDEF_H_
#include "UMLTypedef.h"
#endif

#ifndef _UML_UTILITIES_H_
#include "UMLUtilities.h"
#endif

#endif//UML_HIERARCHICAL_COMPILE

#endif//_UML_H_