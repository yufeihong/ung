/*!
 * \brief
 * 流辅助。
 * \file UFCStreamAux.h
 *
 * \author Su Yang
 *
 * \date 2016/12/20
 */
#ifndef _UFC_STREAMAUX_H_
#define _UFC_STREAMAUX_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

 //算法
#pragma warning(disable:4244)
#include "boost/iostreams/copy.hpp"

namespace ung
{
	/*!
	 * \remarks 拷贝，但是不关闭
	 * \return 
	 * \param Source src
	 * \param Sink snk
	 * \param std::streamsize bufferSize
	*/
	template<typename Source,typename Sink>
	std::streamsize copy_not_close_impl(Source src, Sink snk,std::streamsize bufferSize)
	{
		using namespace boost::iostreams;

		//调用函数对象copy_operation。copy了，但是没有close。
		return detail::copy_operation<Source, Sink>(src, snk, bufferSize)();
	}

	/*!
	 * \remarks source和sink都不是标准的stream或stream buffer
	 * \return 
	 * \param const Source & src
	 * \param const Sink & snk
	 * \param std::streamsize bufferSize
	*/
	template<typename Source, typename Sink>
	std::streamsize copy_not_close(const Source& src, const Sink& snk, std::streamsize bufferSize =
		default_device_buffer_size
		BOOST_IOSTREAMS_DISABLE_IF_STREAM(Source)
		BOOST_IOSTREAMS_DISABLE_IF_STREAM(Sink))
	{
		using namespace boost::iostreams;

		typedef typename char_type_of<Source>::type char_type;

		return copy_not_close_impl(detail::resolve<input, char_type>(src), detail::resolve<output, char_type>(snk), bufferSize);
	}

	/*!
	 * \remarks sink不是一个标准的stream或stream buffer
	 * \return 
	 * \param Source & src
	 * \param const Sink & snk
	 * \param std::streamsize bufferSize
	*/
	template<typename Source, typename Sink>
	std::streamsize copy_not_close(Source& src, const Sink& snk, std::streamsize bufferSize =
		default_device_buffer_size
		BOOST_IOSTREAMS_ENABLE_IF_STREAM(Source)
		BOOST_IOSTREAMS_DISABLE_IF_STREAM(Sink))
	{
		using namespace boost::iostreams;

		typedef typename char_type_of<Source>::type char_type;

		return copy_not_close_impl(detail::wrap(src), detail::resolve<output, char_type>(snk), bufferSize);
	}

	/*!
	 * \remarks source不是一个标准的stream或stream buffer
	 * \return 
	 * \param const Source & src
	 * \param Sink & snk
	 * \param std::streamsize bufferSize
	*/
	template<typename Source, typename Sink>
	std::streamsize copy_not_close(const Source& src, Sink& snk, std::streamsize bufferSize =
		default_device_buffer_size
		BOOST_IOSTREAMS_DISABLE_IF_STREAM(Source)
		BOOST_IOSTREAMS_ENABLE_IF_STREAM(Sink))
	{
		using namespace boost::iostreams;

		typedef typename char_type_of<Source>::type char_type;

		return copy_not_close_impl(detail::resolve<input, char_type>(src), detail::wrap(snk), bufferSize);
	}

	/*!
	 * \remarks source和sink都是一个标准的stream或stream buffer
	 * \return 
	 * \param Source & src
	 * \param Sink & snk
	 * \param std::streamsize bufferSize
	*/
	template<typename Source, typename Sink>
	std::streamsize copy_not_close(Source& src, Sink& snk, std::streamsize bufferSize =
		default_device_buffer_size
		BOOST_IOSTREAMS_ENABLE_IF_STREAM(Source)
		BOOST_IOSTREAMS_ENABLE_IF_STREAM(Sink))
	{
		using namespace boost::iostreams;

		return copy_not_close_impl(detail::wrap(src), detail::wrap(snk), bufferSize);
	}

	/*!
	 * \remarks 关闭流，设备
	 * \return 
	 * \param Source& src
	 * \param Sink& snk
	*/
	template<typename Source, typename Sink>
	void close_now(Source& src,Sink& snk)
	{
		using namespace boost::iostreams;

		detail::call_close_all(src)();
		detail::call_close_all(snk)();
	}

	/*!
	 * \remarks 关闭流，设备
	 * \return 
	 * \param T& t
	*/
	template<typename T>
	void close_one_now(T& t)
	{
		using namespace boost::iostreams;

		detail::call_close_all(t)();
	}
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_STREAMAUX_H_