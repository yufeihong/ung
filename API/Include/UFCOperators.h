﻿/*!
* \brief
* 抽象工厂
* \file UFCOperators.h
*
* \author Su Yang
*
* \date 2018/02/04
*/

#ifndef _UFC_OPERATORS_H_
#define _UFC_OPERATORS_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

namespace ung
{
	namespace operators_detail
	{
		/**
		 * \brief 
		 * \tparam T 
		 */
		template<typename T>
		struct EmptyBaseClassChaining
		{
		};
	}//namespace operators_detail

	/**
	 * \brief 
	 * Requirements:lefthand side(左侧) < righthand side
	 * \tparam T 
	 * \tparam U 
	 * \tparam B 
	 */
	template<typename T, typename B = operators_detail::EmptyBaseClassChaining<T>>
	struct LessThanComparable1 : B
	{
		friend bool operator>(T const& lhs,T const& rhs)
		{
			return rhs < lhs;
		}

		friend bool operator <=(T const& lhs, T const& rhs)
		{
			return !static_cast<bool>(rhs < lhs);
		}

		friend bool operator >=(T const& lhs, T const& rhs)
		{
			return !static_cast<bool>(lhs < rhs);
		}
	};//end struct LessThanComparable1

	/**
	 * \brief 
	 * Requirements:t < u,t > u
	 * \tparam T 
	 * \tparam U 
	 * \tparam B 
	 */
	template<typename T,typename U,typename B = operators_detail::EmptyBaseClassChaining<T>>
	struct LessThanComparable2 : B
	{
		/*
		 * Friend functions defined in a class are implicitly inline.
		 */

		/**
		 * \brief 
		 * \param t 
		 * \param u 
		 * \return 
		 */
		friend bool operator<=(T const& t,U const& u)
		{
			return !static_cast<bool>(t > u);
		}

		/**
		 * \brief 
		 * \param t 
		 * \param u 
		 * \return 
		 */
		friend bool operator>=(T const& t, U const& u)
		{
			return !static_cast<bool>(t < u);
		}

		/**
		 * \brief 
		 * \param u 
		 * \param t 
		 * \return 
		 */
		friend bool operator<=(U const& u,T const& t)
		{
			return !static_cast<bool>(t < u);
		}

		/**
		 * \brief 
		 * \param u 
		 * \param t 
		 * \return 
		 */
		friend bool operator>=(U const& u, T const& t)
		{
			return !static_cast<bool>(t > u);
		}

		friend bool operator>(U const& u,T const& t)
		{
			return t < u;
		}

		friend bool operator<(U const& u, T const& t)
		{
			return t > u;
		}
	};//end struct LessThanComparable2

	template<typename T,typename B = operators_detail::EmptyBaseClassChaining<T>>
	struct equalityComparable1 : B
	{
		friend bool operator!=(T const& lhs,T const& rhs)
		{
			return !static_cast<bool>(lhs == rhs);
		}
	};//end struct equalityComparable1

	template<typename T,typename U,typename B = operators_detail::EmptyBaseClassChaining<T>>
	struct equalityComparable2 : B
	{
		friend bool operator==(U const& u,T const& t)
		{
			return t == u;
		}

		friend bool operator!=(T const& t,U const& u)
		{
			return !static_cast<bool>(t == u);
		}

		friend bool operator!=(U const& u,T const& t)
		{
			return !static_cast<bool>(t == u);
		}
	};//end struct equalityComparable2

	/*
	 * requires the compiler to implement the NRVO.
	 */

	/*
	 * commutative(可交换的)
	 */
	template<typename T, typename B = operators_detail::EmptyBaseClassChaining<T>>
	struct addable1 : B
	{
		friend T operator+(T const& lhs, T const& rhs)
		{
			T nrv{ lhs };

			nrv += rhs;

			return nrv;
		}
	};//end struct addable1

	template<typename T, typename U, typename B = operators_detail::EmptyBaseClassChaining<T>>
	struct addable2 : B
	{
		friend T operator+(T const& t, U const& u)
		{
			T nrv{ t };

			nrv += u;

			return nrv;
		}

		friend T operator+(U const& u, T const& t)
		{
			T nrv{ t };

			nrv += u;

			return nrv;
		}
	};//end struct addable2

	template<typename T, typename B = operators_detail::EmptyBaseClassChaining<T>>
	struct multipliable1 : B
	{
		friend T operator*(T const& lhs, T const& rhs)
		{
			T nrv{ lhs };

			nrv *= rhs;

			return nrv;
		}
	};//end struct multipliable1

	template<typename T, typename U, typename B = operators_detail::EmptyBaseClassChaining<T>>
	struct multipliable2 : B
	{
		friend T operator*(T const& t, U const& u)
		{
			T nrv{ t };

			nrv *= u;

			return nrv;
		}

		friend T operator*(U const& u, T const& t)
		{
			T nrv{ t };

			nrv *= u;

			return nrv;
		}
	};//end struct multipliable2

	template<typename T, typename B = operators_detail::EmptyBaseClassChaining<T>>
	struct xorable1 : B
	{
		friend T operator^(T const& lhs, T const& rhs)
		{
			T nrv{ lhs };

			nrv ^= rhs;

			return nrv;
		}
	};//end struct xorable1

	template<typename T, typename U, typename B = operators_detail::EmptyBaseClassChaining<T>>
	struct xorable2 : B
	{
		friend T operator^(T const& t, U const& u)
		{
			T nrv{ t };

			nrv ^= u;

			return nrv;
		}

		friend T operator^(U const& u, T const& t)
		{
			T nrv{ t };

			nrv ^= u;

			return nrv;
		}
	};//end struct xorable2

	template<typename T, typename B = operators_detail::EmptyBaseClassChaining<T>>
	struct andable1 : B
	{
		friend T operator&(T const& lhs, T const& rhs)
		{
			T nrv{ lhs };

			nrv &= rhs;

			return nrv;
		}
	};//end struct andable1

	template<typename T, typename U, typename B = operators_detail::EmptyBaseClassChaining<T>>
	struct andable2 : B
	{
		friend T operator&(T const& t, U const& u)
		{
			T nrv{ t };

			nrv &= u;

			return nrv;
		}

		friend T operator&(U const& u, T const& t)
		{
			T nrv{ t };

			nrv &= u;

			return nrv;
		}
	};//end struct andable2

	template<typename T, typename B = operators_detail::EmptyBaseClassChaining<T>>
	struct orable1 : B
	{
		friend T operator|(T const& lhs, T const& rhs)
		{
			T nrv{ lhs };

			nrv |= rhs;

			return nrv;
		}
	};//end struct orable1

	template<typename T, typename U, typename B = operators_detail::EmptyBaseClassChaining<T>>
	struct orable2 : B
	{
		friend T operator|(T const& t, U const& u)
		{
			T nrv{ t };

			nrv |= u;

			return nrv;
		}

		friend T operator|(U const& u, T const& t)
		{
			T nrv{ t };

			nrv |= u;

			return nrv;
		}
	};//end struct orable2

	/*
	 * non-commutative(不可交换的)
	 */
	template<typename T, typename B = operators_detail::EmptyBaseClassChaining<T>>
	struct subtractable1 : B
	{
		friend T operator-(T const& lhs, T const& rhs)
		{
			T nrv{ lhs };

			nrv -= rhs;

			return nrv;
		}
	};//end struct subtractable1

	template<typename T, typename U, typename B = operators_detail::EmptyBaseClassChaining<T>>
	struct subtractable2 : B
	{
		friend T operator-(T const& t,U const& u)
		{
			T nrv{ t };

			nrv -= u;

			return nrv;
		}
	};//end struct subtractable2

	template<typename T, typename B = operators_detail::EmptyBaseClassChaining<T>>
	struct dividable1 : B
	{
		friend T operator/(T const& lhs, T const& rhs)
		{
			T nrv{ lhs };

			nrv /= rhs;

			return nrv;
		}
	};//end struct dividable1

	template<typename T, typename U, typename B = operators_detail::EmptyBaseClassChaining<T>>
	struct dividable2 : B
	{
		friend T operator/(T const& t, U const& u)
		{
			T nrv{ t };

			nrv /= u;

			return nrv;
		}
	};//end struct dividable2

	template<typename T, typename B = operators_detail::EmptyBaseClassChaining<T>>
	struct modable1 : B
	{
		friend T operator%(T const& lhs, T const& rhs)
		{
			T nrv{ lhs };

			nrv %= rhs;

			return nrv;
		}
	};//end struct modable1

	template<typename T, typename U, typename B = operators_detail::EmptyBaseClassChaining<T>>
	struct modable2 : B
	{
		friend T operator%(T const& t, U const& u)
		{
			T nrv{ t };

			nrv %= u;

			return nrv;
		}
	};//end struct modable2

	template<typename T, typename B = operators_detail::EmptyBaseClassChaining<T>>
	struct incrementable : B
	{
		/**
		 * \brief 后置++
		 * \param t 
		 * \return 
		 */
		friend T operator++(T& t,int)
		{
			T nrv{ t };

			++t;

			return nrv;
		}
	};//struct incrementable

	template<typename T, typename B = operators_detail::EmptyBaseClassChaining<T>>
	struct decrementable : B
	{
		/**
		 * \brief 后置--
		 * \param t 
		 * \return 
		 */
		friend T operator--(T& t, int)
		{
			T nrv{ t };

			--t;

			return nrv;
		}
	};//struct decrementable

	/*
	 * Iterator operator classes
	 */
	//template<typename T, typename P, typename B = operators_detail::EmptyBaseClassChaining<T>>
	//struct dereferenceable : B
	//{
	//	P operator->() const
	//	{
	//		return std::addressof(*static_cast<const T&>(*this));
	//	}
	//};//struct dereferenceable

	template<typename T,typename B = operators_detail::EmptyBaseClassChaining<T>>
	struct leftShiftable1 : B
	{
		friend T operator<<(T const& lhs,T const& rhs)
		{
			T nrv{ lhs };

			nrv <<= rhs;

			return nrv;
		}
	};//end struct leftShiftable1

	template<typename T,typename U,typename B = operators_detail::EmptyBaseClassChaining<T>>
	struct leftShiftable2 : B
	{
		friend T operator<<(T const& t,U const& u)
		{
			T nrv{ t };

			nrv <<= u;

			return nrv;
		}
	};//end struct leftShiftable2

	template<typename T, typename B = operators_detail::EmptyBaseClassChaining<T>>
	struct rightShiftable1 : B
	{
		friend T operator>>(T const& lhs, T const& rhs)
		{
			T nrv{ lhs };

			nrv >>= rhs;

			return nrv;
		}
	};//end struct rightShiftable1

	template<typename T, typename U, typename B = operators_detail::EmptyBaseClassChaining<T>>
	struct rightShiftable2 : B
	{
		friend T operator>>(T const& t, U const& u)
		{
			T nrv{ t };

			nrv >>= u;

			return nrv;
		}
	};//end struct rightShiftable2

	template<typename T,typename B = operators_detail::EmptyBaseClassChaining<T>>
	struct equivalent1 : B
	{
		friend bool operator==(T const& lhs,T const& rhs)
		{
			return !static_cast<bool>(lhs < rhs) && !static_cast<bool>(rhs < lhs);
		}
	};//end struct equivalent1

	template<typename T,typename U,typename B = operators_detail::EmptyBaseClassChaining<T>>
	struct equivalent2 : B
	{
		friend bool operator==(T const& t,U const& u)
		{
			return !static_cast<bool>(t < u) && !static_cast<bool>(u < t);
		}
	};//end struct equivalent2
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_OPERATORS_H_