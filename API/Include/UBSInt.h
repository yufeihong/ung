/*!
 * \brief
 * 整数
 * \file UBSInt.h
 *
 * \author Su Yang
 *
 * \date 2016/06/15
 */
#ifndef _UBS_INT_H_
#define _UBS_INT_H_

#ifndef _UBS_CONFIG_H_
#include "UBSConfig.h"
#endif

#ifdef UBS_HIERARCHICAL_COMPILE

#ifndef _INCLUDE_BOOST_CSTDINT_HPP_
#include "boost/cstdint.hpp"
#define _INCLUDE_BOOST_CSTDINT_HPP_
#endif

#ifndef _INCLUDE_BOOST_INTEGERTRAITS_HPP_
#include "boost/integer_traits.hpp"
#define _INCLUDE_BOOST_INTEGERTRAITS_HPP_
#endif

namespace ung
{
	//精确宽度
	using int8 = boost::int8_t;													//有符号的8位整数
	using uint8 = boost::uint8_t;												//无符号的8位整数
	using int16 = boost::int16_t;												//有符号的16位整数
	using uint16 = boost::uint16_t;											//无符号的16位整数
	using int32 = boost::int32_t;												//有符号的32位整数
	using uint32 = boost::uint32_t;											//无符号的32位整数
	using int64 = boost::int64_t;												//有符号的64位整数
	using uint64 = boost::uint64_t;											//无符号的64位整数

	//最少具有这么多位，并且是CPU处理速度最快的类型
	using intfast8 = boost::int_fast8_t;
	using uintfast8 = boost::uint_fast8_t;
	using intfast16 = boost::int_fast16_t;
	using uintfast16 = boost::uint_fast16_t;
	using intfast32 = boost::int_fast32_t;
	using uintfast32 = boost::uint_fast32_t;
	using intfast64 = boost::int_fast64_t;
	using uintfast64 = boost::uint_fast64_t;

	//编译器支持的最大整数
	using intmax = boost::intmax_t;
	using uintmax = boost::uintmax_t;

#ifdef _WIN64
	using ufast = uintfast64;
	using sfast = intfast64;
	using usize = uint64;
	using ssize = int64;
#else
	using ufast = uintfast32;
	using sfast = intfast32;
	using usize = uint32;
	using ssize = int32;
#endif

//是否支持128位整数
#ifdef BOOST_HAS_INT128
	using int128 = boost::int128_type;
	using uint128 = boost::uint128_type;
#endif

//是否支持64位整数
#ifdef BOOST_NO_LONG_LONG
#error "Ung requires at least int64 support."
#endif
}//namespace ung

#endif//UBS_HIERARCHICAL_COMPILE

#endif//_UBS_INT_H_