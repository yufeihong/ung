/*!
 * \brief
 * 事件管理器。
 * \file UESEventManager.h
 *
 * \author Su Yang
 *
 * \date 2016/12/25
 */
#ifndef _UES_EVENTMANAGER_H_
#define _UES_EVENTMANAGER_H_

#ifndef _UES_CONFIG_H_
#include "UESConfig.h"
#endif

#ifdef UES_HIERARCHICAL_COMPILE

#ifndef _UES_EVENTTYPE_H_
#include "UESEventType.h"
#endif

#ifndef _UES_IEVENTDATA_H_
#include "UESIEventData.h"
#endif

#ifndef _UES_BINDER_H_
#include "UESBinder.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 消息管理器
	 * \class EventManager
	 *
	 * \author Su Yang
	 *
	 * \date 2016/12/25
	 *
	 * \todo 添加一个线程安全的队列
	 */
	class UngExport EventManager : public Singleton<EventManager>
	{
	public:
		using event_pointer = std::shared_ptr<IEventData>;
		using delegate_type = fastdelegate::FastDelegate1<event_pointer>;
		using queue_type = std::list<event_pointer>;
		using listener_list = std::list<delegate_type>;
		using listener_list_map = std::map<EventType, listener_list>;

	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		EventManager();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~EventManager();

		/*!
		 * \remarks 注册一个委托函数，当给定消息类型触发时调用
		 * \return 
		 * \param delegate_type const& delegateFunctor 委托函数
		 * \param EventType const& et
		*/
		virtual void addListener(delegate_type const& delegateFunctor,EventType const& et);

		/*!
		 * \remarks 移除给定的委托函数和消息类型对
		 * \return 
		 * \param delegate_type const& delegateFunctor 委托函数
		 * \param EventType const & et
		*/
		virtual void removeListener(delegate_type const& delegateFunctor,EventType const& et);

		/*!
		 * \remarks 绕过消息队列，直接触发一个具体的消息
		 * \return 
		 * \param event_pointer const& evt
		*/
		virtual void triggerEvent(event_pointer const& evt) const;

		/*!
		 * \remarks Queue up一个消息，以便于下一帧时去触发这个消息
		 * \return 
		 * \param event_pointer const& evt
		*/
		virtual void queueEven(event_pointer const& evt);

		/*!
		 * \remarks 处理队列中的所有消息
		 * \return 
		*/
		virtual void update();

	private:
		std::array<queue_type, 2> mQueue;
		uint8 mActiveQueue;
		listener_list_map mMap;
	};
}//namespace ung

#endif//UES_HIERARCHICAL_COMPILE

#endif//_UES_EVENTMANAGER_H_