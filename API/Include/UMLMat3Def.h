/*!
 * \brief
 * Mat3的定义。
 * \file UMLMat3Def.h
 *
 * \author Su Yang
 *
 * \date 2017/03/15
 */
#ifndef _UML_MAT3DEF_H_
#define _UML_MAT3DEF_H_

#ifndef _UML_CONFIG_H_
#include "UMLConfig.h"
#endif

#ifdef UML_HIERARCHICAL_COMPILE

#ifndef _UML_MAT3_H_
#include "UMLMat3.h"
#endif

#ifndef _UML_MATH_H_
#include "UMLMath.h"
#endif

#ifndef _UML_CONSTS_H_
#include "UMLConsts.h"
#endif

namespace ung
{
	template<typename T>
	const Mat3<T> Mat3<T>::ZERO(0, 0, 0, 0, 0, 0, 0, 0, 0);
	template<typename T>
	const Mat3<T> Mat3<T>::IDENTITY(1, 0, 0, 0, 1, 0, 0, 0, 1);

	template<typename T>
	Mat3<T>::Mat3()
	{
		for (auto &row : m)
		{
			for (auto &e : row)
			{
				e = 0.0;
			}
		}
		//单位矩阵
		m[0][0] = m[1][1] = m[2][2] = 1.0;

		/*
		也可以使用标准库函数begin和end.
		for(auto p = begin(ia); p != end(ia); ++p)			//p指向ia的第一个数组
		{
		for(auto q = begin(*p); q != end(*p); ++q)		//q指向内层数组的首元素
		std::cout << *q <<' ';

		std::cout << std::endl;
		}
		*/
	}

	template<typename T>
	Mat3<T>::Mat3(T m00, T m01, T m02,
							T m10, T m11, T m12,
							T m20, T m21, T m22)
	{
		m[0][0] = m00;
		m[0][1] = m01;
		m[0][2] = m02;
		m[1][0] = m10;
		m[1][1] = m11;
		m[1][2] = m12;
		m[2][0] = m20;
		m[2][1] = m21;
		m[2][2] = m22;
	}

	template<typename T>
	Mat3<T>::Mat3(const T(*const ar)[3], const uint32 rowSize)
	{
		BOOST_ASSERT(rowSize == 3);
		
		memcpy(m, ar, 9 * sizeof(T));
	}

	template<typename T>
	Mat3<T>::Mat3(const T *const ar, const uint32 sz)
	{
		BOOST_ASSERT(sz == 9);
		memcpy(m, ar, 9 * sizeof(T));
	}

	template<typename T>
	Mat3<T>::Mat3(const T(&ar)[9])
	{
		memcpy(m, ar, 9 * sizeof(T));
	}

	template<typename T>
	Mat3<T>::Mat3(const T(&ar)[3][3])
	{
		memcpy(m, ar, 9 * sizeof(T));
	}

	template<typename T>
	Mat3<T>::Mat3(Vec3<T> const& row1, Vec3<T> const& row2, Vec3<T> const& row3)
	{
		m[0][0] = row1.x;
		m[0][1] = row1.y;
		m[0][2] = row1.z;

		m[1][0] = row2.x;
		m[1][1] = row2.y;
		m[1][2] = row2.z;

		m[2][0] = row3.x;
		m[2][1] = row3.y;
		m[2][2] = row3.z;
	}

	template<typename T>
	Mat3<T>::Mat3(const Mat3& other)
	{
		memcpy(m, other.m, 9 * sizeof(T));
	}

	template<typename T>
	Mat3<T>& Mat3<T>::operator=(const Mat3& other)
	{
		if (this != &other)
		{
			memcpy(m, other.m, 9 * sizeof(T));
		}

		return *this;
	}

	template<typename T>
	Mat3<T>::Mat3(Mat3 && other) noexcept
	{
		for (uint8 r = 0; r < 3; ++r)
		{
			for (uint8 c = 0; c < 3; ++c)
			{
				m[r][c] = other[r][c];
			}
		}
	}

	template<typename T>
	Mat3<T>& Mat3<T>::operator=(Mat3 && other) noexcept
	{
		if (this != &other)
		{
			for (uint8 r = 0; r < 3; ++r)
			{
				for (uint8 c = 0; c < 3; ++c)
				{
					m[r][c] = other[r][c];
				}
			}
		}

		return *this;
	}

	template<typename T>
	const Vec3<T> Mat3<T>::operator[](usize iRow) const
	{
		BOOST_ASSERT(iRow >= 0 && iRow <= 2);

		return Vec3<T>(m[iRow]);
	}

	template<typename T>
	Vec3<T> Mat3<T>::operator[](usize iRow)
	{
		BOOST_ASSERT(iRow >= 0 && iRow <= 2);

		return Vec3<T>(m[iRow]);
	}

	template<typename T>
	T Mat3<T>::operator()(usize row, usize col) const
	{
		BOOST_ASSERT(row >= 0 && row <= 2);
		BOOST_ASSERT(col >= 0 && col <= 2);

		return m[row][col];
	}

	template<typename T>
	T& Mat3<T>::operator()(usize row, usize col)
	{
		BOOST_ASSERT(row >= 0 && row <= 2);
		BOOST_ASSERT(col >= 0 && col <= 2);

		return m[row][col];
	}

	template<typename T>
	T& Mat3<T>::getElement(usize r, usize c)
	{
		BOOST_ASSERT(r >= 0 && r < 3);
		BOOST_ASSERT(c >= 0 && c < 3);

		return m[r][c];
	}

	template<typename T>
	T const& Mat3<T>::getElement(usize r, usize c) const
	{
		BOOST_ASSERT(r >= 0 && r < 3);
		BOOST_ASSERT(c >= 0 && c < 3);

		return m[r][c];
	}

	template<typename T>
	T* Mat3<T>::getAddress()
	{
		//多维数组在内存中是连续分布的
		return &m[0][0];;
	}

	template<typename T>
	T const* Mat3<T>::getAddress() const
	{
		return &m[0][0];;
	}

	template<typename T>
	Vec3<T> Mat3<T>::getScale() const
	{
		return Vec3<T>(m[0][0],m[1][1],m[2][2]);
	}

	template<typename T>
	bool Mat3<T>::operator== (const Mat3& other) const
	{
		for (usize iRow = 0; iRow < 3; iRow++)
		{
			for (usize iCol = 0; iCol < 3; iCol++)
			{
				if (!(math_type::isEqual(m[iRow][iCol],other.m[iRow][iCol])))
				{
					return false;
				}
			}
		}

		return true;
	}

	template<typename T>
	bool Mat3<T>::operator!= (const Mat3& other) const
	{
		return !(operator==(other));
	}

	template<typename T>
	Mat3<T> Mat3<T>::operator*(T scalar) const
	{
		Mat3 outMat{*this};
		outMat *= scalar;

		return outMat;
	}

	template<typename T>
	Mat3<T>& Mat3<T>::operator*=(T scalar)
	{
		for (uint8 iRow = 0; iRow < 3; iRow++)
		{
			for (uint8 iCol = 0; iCol < 3; iCol++)
			{
				m[iRow][iCol] = m[iRow][iCol] * scalar;
			}
		}

		return *this;
	}

	template<typename T>
	Mat3<T> Mat3<T>::operator*(Mat3 const& other) const
	{
		Mat3 outMat{ *this };
		outMat *= other;

		return outMat;
	}

	template<typename T>
	Mat3<T>& Mat3<T>::operator*=(const Mat3& other)
	{
		Mat3<T> R{};

		for (uint8 iRow = 0; iRow < 3; iRow++)
		{
			for (uint8 iCol = 0; iCol < 3; iCol++)
			{
				R.m[iRow][iCol] = m[iRow][0] * other.m[0][iCol] + m[iRow][1] * other.m[1][iCol] + m[iRow][2] * other.m[2][iCol];
			}
		}

		//移动赋值
		*this = std::move(R);

		return *this;
	}

	template<typename T>
	Mat3<T> Mat3<T>::operator/(T scalar) const
	{
		Mat3 outMat{ *this };
		outMat /= scalar;

		return outMat;
	}

	template<typename T>
	Mat3<T>& Mat3<T>::operator/=(T scalar)
	{
		T invScalar = 1.0 / scalar;

		return operator*=(invScalar);
	}

	template<typename T>
	void Mat3<T>::identity()
	{
		m[0][0] = 1.0;
		m[0][1] = 0.0;
		m[0][2] = 0.0;
		m[1][0] = 0.0;
		m[1][1] = 1.0;
		m[1][2] = 0.0;
		m[2][0] = 0.0;
		m[2][1] = 0.0;
		m[2][2] = 1.0;
	}

	/*
	从矩阵中任意选择一行或一列,对该行或列中的每个元素,都乘以对应的代数余子式.这些乘积的和就是矩阵的行列式.
	将坐标轴向量放入到矩阵的一，二，三行中，此时矩阵为正交矩阵。求该矩阵的行列式，若其值为1，那么代表了
	右手坐标系，若其值为-1，那么代表了左手坐标系。
	旋转矩阵的行列式为1，反射矩阵的行列式为-1。

	矩阵的转置矩阵，其行列式和原矩阵相等。
	互换两行或两列，行列式为其原值的负。
	矩阵A和矩阵B的乘积的行列式，等于各自行列式的乘积。

	对于一个n维空间的nxn行列式，分别代表1D，2D和3D空间的长度，面积和体积。
	*/
	template<typename T>
	T Mat3<T>::determinant() const
	{
		/*
		代数余子式
		注意:余子式是一个矩阵,而代数余子式是一个标量.
		*/
		T cofactor00 = m[1][1] * m[2][2] - m[1][2] * m[2][1];
		T cofactor01 = m[1][2] * m[2][0] - m[1][0] * m[2][2];		//0 + 1产生负
		T cofactor02 = m[1][0] * m[2][1] - m[1][1] * m[2][0];

		T det = m[0][0] * cofactor00 + m[0][1] * cofactor01 + m[0][2] * cofactor02;

		return det;
	}

	template<typename T>
	void Mat3<T>::transpose(Mat3& outMat) const
	{
		for (usize iRow = 0; iRow < 3; iRow++)
		{
			for (usize iCol = 0; iCol < 3; iCol++)
			{
				outMat.m[iRow][iCol] = m[iCol][iRow];
			}
		}
	}

	template<typename T>
	void Mat3<T>::transpose()
	{
		using std::swap;

		swap(m[0][1], m[1][0]);
		swap(m[0][2], m[2][0]);
		swap(m[1][2], m[2][1]);
	}

	/*
	对于一个n X n矩阵M而言,如果存在一个矩阵M(-1),使MM(-1) = M(-1)M = I,则称矩阵M是可逆的.矩阵M(-1)叫做M的逆矩阵.
	并不是每个矩阵都可逆,没有逆矩阵的矩阵叫做奇异矩阵.
	例如,任何有一行或一列为0的矩阵就是奇异矩阵.那么对于任意矩阵,如果有一行是其他行的线性组合,则这个矩阵是奇异的.
	n X n矩阵M当且仅当detM != 0时是可逆的.
	*/
	template<typename T>
	bool Mat3<T>::inverse(Mat3& inverseMat) const
	{
		/*
		矩阵的逆 = 标准伴随矩阵 / 矩阵的行列式
		M的"标准伴随矩阵",定义为M的代数余子式矩阵的转置矩阵.
		下面计算当前矩阵的代数余子式矩阵,同时已经进行了转置,所以这个矩阵就是标准伴随矩阵.
		*/
		inverseMat.m[0][0] = m[1][1] * m[2][2] - m[1][2] * m[2][1];
		inverseMat.m[0][1] = m[0][2] * m[2][1] - m[0][1] * m[2][2];
		inverseMat.m[0][2] = m[0][1] * m[1][2] - m[0][2] * m[1][1];
		inverseMat.m[1][0] = m[1][2] * m[2][0] - m[1][0] * m[2][2];
		inverseMat.m[1][1] = m[0][0] * m[2][2] - m[0][2] * m[2][0];
		inverseMat.m[1][2] = m[0][2] * m[1][0] - m[0][0] * m[1][2];
		inverseMat.m[2][0] = m[1][0] * m[2][1] - m[1][1] * m[2][0];
		inverseMat.m[2][1] = m[0][1] * m[2][0] - m[0][0] * m[2][1];
		inverseMat.m[2][2] = m[0][0] * m[1][1] - m[0][1] * m[1][0];

		//计算行列式
		T det = m[0][0] * inverseMat.m[0][0] + m[0][1] * inverseMat.m[1][0] + m[0][2] * inverseMat.m[2][0];

		if (math_type::isEqual(det,0.0))
		{
			inverseMat = ZERO;
			return false;
		}

		T invDet = 1.0 / det;
		for (usize iRow = 0; iRow < 3; iRow++)
		{
			for (usize iCol = 0; iCol < 3; iCol++)
			{
				inverseMat.m[iRow][iCol] *= invDet;
			}
		}

		return true;
	}

	template<typename T>
	void Mat3<T>::orthogonalization()
	{
		/*
		使用施密特正交化.
		认为当前矩阵为M = [m0|m1|m2]形式,正交化后输出的矩阵为Q = [q0|q1|q2]形式.
		那么:
		q0 = m0 / |m0|
		q1 = (m1 - (q0.m1)q0) / |m1 - (q0.m1)q0|
		q2 = (m2 - (q0.m2)q0 - (q1.m2)q1) / |m2 - (q0.m2)q0 - (q1.m2)q1|
		其中.表示点积.
		*/

		//q0
		T invLen = math_type::calInvSqrt(m[0][0] * m[0][0] + m[1][0] * m[1][0] + m[2][0] * m[2][0]);
		
		m[0][0] *= invLen;
		m[1][0] *= invLen;
		m[2][0] *= invLen;

		//q1
		T dot0 = m[0][0] * m[0][1] + m[1][0] * m[1][1] + m[2][0] * m[2][1];

		m[0][1] -= dot0*m[0][0];
		m[1][1] -= dot0*m[1][0];
		m[2][1] -= dot0*m[2][0];

		invLen = math_type::calInvSqrt(m[0][1] * m[0][1] + m[1][1] * m[1][1] + m[2][1] * m[2][1]);

		m[0][1] *= invLen;
		m[1][1] *= invLen;
		m[2][1] *= invLen;

		//q2
		T dot1 = m[0][1] * m[0][2] + m[1][1] * m[1][2] + m[2][1] * m[2][2];
		dot0 = m[0][0] * m[0][2] + m[1][0] * m[1][2] + m[2][0] * m[2][2];

		m[0][2] -= dot0*m[0][0] + dot1*m[0][1];
		m[1][2] -= dot0*m[1][0] + dot1*m[1][1];
		m[2][2] -= dot0*m[2][0] + dot1*m[2][1];

		invLen = math_type::calInvSqrt(m[0][2] * m[0][2] + m[1][2] * m[1][2] + m[2][2] * m[2][2]);

		m[0][2] *= invLen;
		m[1][2] *= invLen;
		m[2][2] *= invLen;
	}

	template<typename T>
	Vec3<T> Mat3<T>::getRow(const usize iRow) const
	{
		BOOST_ASSERT(iRow < 3);
		return Vec3<T>(m[iRow][0], m[iRow][1], m[iRow][2]);
	}

	template<typename T>
	Vec3<T> Mat3<T>::getColumn(const usize iCol) const
	{
		BOOST_ASSERT(iCol < 3);
		return Vec3<T>(m[0][iCol],m[1][iCol],m[2][iCol]);
	}

	template<typename T>
	void Mat3<T>::setRow(const usize iRow, const Vec3<T>& vec)
	{
		BOOST_ASSERT(iRow < 3);
		m[iRow][0] = vec.x;
		m[iRow][1] = vec.y;
		m[iRow][2] = vec.z;
	}

	template<typename T>
	void Mat3<T>::setColumn(const usize iCol, const Vec3<T>& vec)
	{
		BOOST_ASSERT(iCol < 3);
		m[0][iCol] = vec.x;
		m[1][iCol] = vec.y;
		m[2][iCol] = vec.z;
	}

	template<typename T>
	Mat3<T>& Mat3<T>::setUniformScale(T r)
	{
		m[0][0] = m[1][1] = m[2][2] = r;
		return *this;
	}

	template<typename T>
	Mat3<T> operator* (T scalar, const Mat3<T>& mat)
	{
		Mat3 prod;
		for (usize iRow = 0; iRow < 3; iRow++)
		{
			for (usize iCol = 0; iCol < 3; iCol++)
			{
				prod[iRow][iCol] = scalar * mat.m[iRow][iCol];
			}
		}

		return prod;
	}

	template<typename T>
	Mat3<T> Mat3<T>::makeScaleAnyDirection(const Vec3<T>& n, T k)
	{
		Mat3 outMat{};

		auto dir{ n };
		if (!dir.isUnitLength())
		{
			dir.normalize();
		}

		T kMinusOne = k - 1.0;

		outMat.m[0][0] = 1 + kMinusOne * dir.x * dir.x;
		outMat.m[0][1] = kMinusOne * dir.x * dir.y;
		outMat.m[0][2] = kMinusOne * dir.x * dir.z;
		outMat.m[1][0] = outMat.m[0][1];
		outMat.m[1][1] = 1 + kMinusOne * dir.y * dir.y;
		outMat.m[1][2] = kMinusOne * dir.y * dir.z;
		outMat.m[2][0] = outMat.m[0][2];
		outMat.m[2][1] = outMat.m[1][2];
		outMat.m[2][2] = 1 + kMinusOne * dir.z * dir.z;

		return std::move(outMat);
	}

	template<typename T>
	void Mat3<T>::setRotateX(Radian r)
	{
		T cosValue = math_type::calCos(r);
		T sinValue = math_type::calSin(r);

		m[0][0] = 1.0;
		m[0][1] = 0.0;
		m[0][2] = 0.0;
		m[1][0] = 0.0;
		m[1][1] = cosValue;
		m[1][2] = sinValue;
		m[2][0] = 0.0;
		m[2][1] = sinValue * -1.0;
		m[2][2] = cosValue;
	}

	template<typename T>
	void Mat3<T>::setRotateX(Degree d)
	{
		setRotateX(d.toRadian());
	}

	/*
	旋转矩阵的逆矩阵与其转置相等，具备这样特点的矩阵称为正交矩阵。
	*/
	template<typename T>
	Mat3<T> Mat3<T>::makeRotateX(Radian r)
	{
		Mat3 outMat{};
		outMat.setRotateX(r);

		return std::move(outMat);
	}

	template<typename T>
	Mat3<T> Mat3<T>::makeRotateX(Degree d)
	{
		Mat3 outMat{};
		outMat.setRotateX(d.toRadian());

		return std::move(outMat);
	}

	template<typename T>
	void Mat3<T>::setRotateY(Radian r)
	{
		T cosValue = math_type::calCos(r);
		T sinValue = math_type::calSin(r);

		m[0][0] = cosValue;
		m[0][1] = 0.0;
		m[0][2] = sinValue * -1.0;
		m[1][0] = 0.0;
		m[1][1] = 1.0;
		m[1][2] = 0.0;
		m[2][0] = sinValue;
		m[2][1] = 0.0;
		m[2][2] = cosValue;
	}

	template<typename T>
	void Mat3<T>::setRotateY(Degree d)
	{
		setRotateY(d.toRadian());
	}

	template<typename T>
	Mat3<T> Mat3<T>::makeRotateY(Radian r)
	{
		Mat3 outMat{};
		outMat.setRotateY(r);

		return std::move(outMat);
	}

	template<typename T>
	Mat3<T> Mat3<T>::makeRotateY(Degree d)
	{
		Mat3 outMat{};
		outMat.setRotateY(d.toRadian());

		return std::move(outMat);
	}

	template<typename T>
	void Mat3<T>::setRotateZ(Radian r)
	{
		//矩阵以行为主，那么没一行，就代表了旋转后的x,y,z轴

		T cosValue = math_type::calCos(r);
		T sinValue = math_type::calSin(r);

		m[0][0] = cosValue;
		m[0][1] = sinValue;
		m[0][2] = 0.0;
		m[1][0] = sinValue * -1.0;
		m[1][1] = cosValue;
		m[1][2] = 0.0;
		m[2][0] = 0.0;
		m[2][1] = 0.0;
		m[2][2] = 1.0;
	}

	template<typename T>
	void Mat3<T>::setRotateZ(Degree d)
	{
		setRotateZ(d.toRadian());
	}

	template<typename T>
	Mat3<T> Mat3<T>::makeRotateZ(Radian r)
	{
		Mat3 outMat{};
		outMat.setRotateZ(r);

		return std::move(outMat);
	}

	template<typename T>
	Mat3<T> Mat3<T>::makeRotateZ(Degree d)
	{
		Mat3 outMat{};
		outMat.setRotateZ(d.toRadian());

		return std::move(outMat);
	}

	template<typename T>
	void Mat3<T>::setRotateAnyAxis(const Vec3<T>& n, Radian alpha)
	{
		auto dir{ n };
		if (!dir.isUnitLength())
		{
			dir.normalize();
		}

		T cosValue = math_type::calCos(alpha);
		T sinValue = math_type::calSin(alpha);
		T oneMinusCos = 1.0 - cosValue;

		T x = dir.x, y = dir.y, z = dir.z;
		T x2 = x * x;
		T y2 = y * y;
		T z2 = z * z;
		T xy = x * y;
		T xz = x * z;
		T yz = y * z;

		m[0][0] = x2 * oneMinusCos + cosValue;
		m[0][1] = xy * oneMinusCos + z * sinValue;
		m[0][2] = xz * oneMinusCos - y * sinValue;
		m[1][0] = xy * oneMinusCos - z * sinValue;
		m[1][1] = y2 * oneMinusCos + cosValue;
		m[1][2] = yz * oneMinusCos + x * sinValue;
		m[2][0] = xz * oneMinusCos + y * sinValue;
		m[2][1] = yz * oneMinusCos - x * sinValue;
		m[2][2] = z2 * oneMinusCos + cosValue;

		//该矩阵公式已验证
	}

	template<typename T>
	void Mat3<T>::setRotateAnyAxis(const Vec3<T>& n, Degree d)
	{
		setRotateAnyAxis(n,d.toRadian());
	}

	template<typename T>
	Mat3<T> Mat3<T>::makeRotateAnyAxis(const Vec3<T>& n, Radian alpha)
	{
		Mat3 outMat{};
		outMat.setRotateAnyAxis(n,alpha);

		return std::move(outMat);
	}

	template<typename T>
	Mat3<T> Mat3<T>::makeRotateAnyAxis(const Vec3<T>& n, Degree d)
	{
		Mat3 outMat{};
		outMat.setRotateAnyAxis(n, d.toRadian());

		return std::move(outMat);
	}

	template<typename T>
	void Mat3<T>::extractAxisAngle(Vec3<T>& axis, Radian& alpha) const
	{
		T traceValue = m[0][0] + m[1][1] + m[2][2];
		T cosValue = 0.5 * (traceValue - 1.0);
		alpha = math_type::calACos(cosValue);

		if (alpha > 0.0)
		{
			if (alpha < Consts<T>::PI)
			{
				axis.x = m[1][2] - m[2][1];
				axis.y = m[2][0] - m[0][2];
				axis.z = m[0][1] - m[1][0];
				axis.normalize();
			}
			else//== PI
			{
				T halfInverse;
				if (m[0][0] >= m[1][1])
				{
					if (m[0][0] >= m[2][2])
					{
						//r00最大
						axis.x = 0.5 * math_type::calSqrt(m[0][0] - m[1][1] - m[2][2] + 1.0);
						halfInverse = 0.5 / axis.x;
						axis.y = halfInverse * m[1][0];
						axis.z = halfInverse * m[2][0];
					}
					else
					{
						//r22最大
						axis.z = 0.5 * math_type::calSqrt(m[2][2] - m[0][0] - m[1][1] + 1.0);
						halfInverse = 0.5 / axis.z;
						axis.x = halfInverse * m[2][0];
						axis.y = halfInverse * m[2][1];
					}
				}
				else
				{
					//r11 > r00
					if (m[1][1] >= m[2][2])
					{
						//r11最大
						axis.y = 0.5 * math_type::calSqrt(m[1][1] - m[0][0] - m[2][2] + 1.0);
						halfInverse = 0.5 / axis.y;
						axis.x = halfInverse * m[1][0];
						axis.z = halfInverse * m[2][1];
					}
					else
					{
						//r22最大
						axis.z = 0.5 * math_type::calSqrt(m[2][2] - m[0][0] - m[1][1] + 1.0);
						halfInverse = 0.5 / axis.z;
						axis.x = halfInverse * m[2][0];
						axis.y = halfInverse * m[2][1];
					}
				}
			}
		}
		else//== 0
		{
			axis.x = 1.0;
			axis.y = 0.0;
			axis.z = 0.0;
		}
	}

	template<typename T>
	void Mat3<T>::extractAxisAngle(Vec3<T>& axis, Degree& d) const
	{
		Radian r{};
		extractAxisAngle(axis,r);

		d = r.toDegree();
	}

	template<typename T>
	void Mat3<T>::setRotateFromQuaternion(const Quater<T>& q)
	{
		/*
		行为主的公式：
		1.0 - twoY2 - twoZ2		twoXY + twoWZ				twoXZ - twoWY
		twoXY - twoWZ				1.0 - twoX2 - twoZ2		twoYZ + twoWX
		twoXZ + twoWY				twoYZ - twoWX				1.0 - twoX2 - twoY2
		*/
		T twoX2 = 2.0 * q.x * q.x;
		T twoY2 = 2.0 * q.y * q.y;
		T twoZ2 = 2.0 * q.z * q.z;

		T twoWX = 2.0 * q.w * q.x;
		T twoWY = 2.0 * q.w * q.y;
		T twoWZ = 2.0 * q.w * q.z;
		T twoXY = 2.0 * q.x * q.y;
		T twoXZ = 2.0 * q.x * q.z;
		T twoYZ = 2.0 * q.y * q.z;

		m[0][0] = 1.0 - twoY2 - twoZ2;
		m[0][1] = twoXY + twoWZ;
		m[0][2] = twoXZ - twoWY;

		m[1][0] = twoXY - twoWZ;
		m[1][1] = 1.0 - twoX2 - twoZ2;
		m[1][2] = twoYZ + twoWX;

		m[2][0] = twoXZ + twoWY;
		m[2][1] = twoYZ - twoWX;
		m[2][2] = 1.0 - twoX2 - twoY2;
	}

	template<typename T>
	Quater<T> Mat3<T>::getQuaternionOfRotate() const
	{
		Quater<T> quat{};

		T q[4];

		//轨迹
		T trace = m[0][0] + m[1][1] + m[2][2];

		//检测主轴
		if (trace > 0.0)
		{
			T s = math_type::calSqrt(trace + 1.0);
			q[3] = s * 0.5;

			T t = 0.5 / s;
			q[0] = (m[1][2] - m[2][1]) * t;
			q[1] = (m[2][0] - m[0][2]) * t;
			q[2] = (m[0][1] - m[1][0]) * t;
		}
		else
		{
			//主轴为负
			uint32 i = 0;
			if (m[1][1] > m[0][0])
			{
				i = 1;
			}

			if (m[2][2] > m[i][i])
			{
				i = 2;
			}

			static const uint32 next[3] = { 1,2,0 };

			uint32 j = next[i];
			uint32 k = next[j];

			T s = math_type::calSqrt((m[i][i] - (m[j][j] + m[k][k])) + 1.0);

			q[i] = s * 0.5;

			T t;
			if (!math_type::isZero(s))
			{
				t = 0.5 / s;
			}
			else
			{
				t = s;
			}

			q[3] = (m[j][k] - m[k][j]) * t;
			q[j] = (m[i][j] + m[j][i]) * t;
			q[k] = (m[i][k] + m[k][i]) * t;
		}

		quat.w = q[3];
		quat.x = q[0];
		quat.y = q[1];
		quat.z = q[2];

		quat.normalize();

		BOOST_ASSERT(quat.isUnit());

		return quat;
	}

	template<typename T>
	Mat3<T> Mat3<T>::makeOrthogonal(const Vec3<T>& n)
	{
		return makeScaleAnyDirection(n, 0.0);
	}

	template<typename T>
	Mat3<T> Mat3<T>::makeReflectAnyPlane(const Vec3<T>& n)
	{
		return makeScaleAnyDirection(n, -1.0);
	}
}//namespace ung

#endif//UML_HIERARCHICAL_COMPILE

#endif//_UML_MAT3DEF_H_

 /*
 矩阵主要用来描述两个坐标系统间的关系.

 向量是标量的数组,矩阵则是向量的数组.

 方阵能描述任意线性变换.Mat3用于通过矩阵来实现线性变换.
 线性变换的一个重要性质就是不包括平移.线性变换满足:F(a + b) = F(a) + F(b)和F(ka) = kF(a).
 仿射变换是指线性变换后接着平移.任何具有形式v1 = v0 * M + b的变换都是放射变换.
 因此,仿射变换的集合是线性变换的超集,任何线性变换都是仿射变换,但不是所有仿射变换都是线性变换.
 线性变换包括:旋转,缩放,投影,镜像(反射),切变等.
 */

/*
向量在几何上可以被解释为一系列与轴平行的位移.
如,向量[1,-3,4]被解释为位移[1,0,0],随后位移[0,-3,0],最后位移[0,0,4].
将这个位移序列解释成向量的加法:
[1,0,0] + [0,-3,0] + [0,0,4] = [1,-3,4]
一般来说,任意向量v都能写为"扩展"形式:
[x,0,0] + [0,y,0] + [0,0,z] = [x,y,z] = v
也可以写为这样:
x[1,0,0] + y[0,1,0] + z[0,0,1] = [x,y,z] = v									//式1
我们发现左边的3个单位向量就是x,y,z轴.
那也就是说向量可以用3个坐标轴(基向量)的线性组合来表达.						//如果一组向量互相垂直,这组向量被认为是正交基.如果它们都是单位向量,
																								//则称它们为标准正交基.
这次,分别将p,q和r定义为指向+x,+y和+z方向的单位向量,那么:
v = xp + yq + zr(将向量表示为基向量的线性组合)
现在,向量v就被表示成向量p,q,r的线性变换了.向量p,q,r称作基向量.这里基向量是笛卡尔坐标轴,但事实上,一个坐标系能用任意3个基向量
定义,当然这三个向量要线性无关(也就是不在同一平面上).

以p,q,r为行构建一个3x3矩阵M:
	   [p]    [px  py pz]
M = |q| = |qx  qy qz|
	   [r]     [rx  ry  rz]
这样,我们就可以将矩阵解释为基向量的集合.

用一个向量乘以该矩阵,得到:
			 [px py pz]
[x,y,z] * |qx qy qz| = [x * px + y * qx + z * rx,x * py + y * qy + z * ry,x * pz + y * qz + z * rz]		//式2
			 [rx  ry  rz]
							= [xp,yq,zr]和之前的v一样.																				//式3

这里理解"和之前的v一样"的时候,请看式1,式1中x[1,0,0]这里的[1,0,0]中第一个1是px的1,第二个0是qx的0,第三个0是
rx的0.所以x * px + y * qx + z * rx = x * px,同理式2中的第二和第三分量分别为y * py和z * rz,这样式2就成为了:
[x,y,z],之前的v也是[x,y,z],所以一样.但是式2的左边也同样是用[x,y,z]乘以了一个矩阵,结果依然等于[x,y,z],那是因为
这个矩阵仅仅只是三个坐标轴所代表的基向量的集合.
所以我们说,如果把矩阵的行解释为坐标系的基向量,那么乘以该矩阵就相当于执行了一次坐标转换.
矩阵,只是用一种紧凑的方式来表达坐标转换所需的数学运算.

接下来,我们看一下基向量乘以任意矩阵M时的情况:
										[m11 m12 m13]
[1 0 0] * |m21 m22 m23| = [m11 m12 m13]
										[m31 m32 m33]

										[m11 m12 m13]
[0 1 0] * |m21 m22 m23| = [m21 m22 m23]
										[m31 m32 m33]

										[m11 m12 m13]
[0 0 1] * |m21 m22 m23| = [m31 m32 m33]
										[m31 m32 m33]

发现,用基向量[1 0 0]乘以M时,结果是M的第一行.其他两行也有同样的结果.
故,说矩阵的每一行都能解释为转换后的基向量.这样我们就可以给出一个期望的变换(如旋转,缩放等),然后构造一个矩阵代表此变换(可以通过想象
变换后的坐标系的基向量来想象矩阵).我们所要做的一切就是计算基向量的变换,然后将变换后的基向量填入矩阵.
所以说,要看一个矩阵的实质变换是什么,只要把矩阵的每个行拿出来,组成3个基向量,就知道了这个矩阵把当前坐标系(比如[1 0 0],[0 1 0],
[0 0 1])究竟给变换到了一个什么样的坐标系(什么样的3个基向量构成的新坐标系).
零向量乘以任何矩阵仍然得到零向量.因此,方阵所代表的线性变换的原点和原坐标系的原点一致.变换不包含原点.

有些情况下需要进行物体变换,另外一些情况下则需要进行坐标系变换(比如,用枪设计一辆汽车的时候,把子弹的弹道给变换到汽车的物体坐标系中).
将物体变换一个量等价于将描述这个物体的坐标系变换一个相反的量.当有多个变换时,则需要以相反的顺序变换相反的量.

谨记:
1:一个坐标系能用任意3个基向量定义.
2:向量可以表示为基向量的线性组合.
3:把矩阵的行解释为坐标系的基向量,那么乘以该矩阵就相当于执行了一次坐标转换.
4:矩阵的每一行都能解释为转换后的基向量.

2D中的旋转(限制为只绕原点旋转):
原本坐标系的基向量p0为[1,0]和q0[0,1].
变换后坐标系的基向量为p1和q1.
2D中绕原点的旋转只有一个参数:角度θ,它描述了旋转量.根据下图所示:
						q0 = [0,1]
						  |
q1[-sinθ,cosθ]\    |       /p1[cosθ,sinθ]
					 \   |      /
					  \  |    /
					   \ |  /
						\|/
--------------------------------p0 = [1,0]
						 |
我们知道旋转后的两个基向量的值为p1[cosθ,sinθ]和q1[-sinθ,cosθ].
那么,我们知道"矩阵的每一行都能解释为转换后的基向量",所以我们就可以构造这个变换的矩阵了:
		   [p1]     [cosθ,  sinθ]
R(θ) = |    | = |                |
		   [q1]     [-sinθ,cosθ]

-------------------------------------------------------------------------------------------------

2D旋转矩阵:
						y
						|
				 Q     |        P
				  \     |       /
				   \    |      /
				 r  \   |    / r
					 \  |  /
					   \|/
-----------------------------------------x
						|
向量P的长度为r.
向量P和x轴的夹角为α.
向量P围绕原点旋转β角度并定义为Q,同样Q的长度也为r.

P的坐标可按照如下方式加以定义:
Px = r * cos(α),Py = r * sin(α)
依据上式,同理Q的坐标可按照如下方式计算:
Qx = r * cos(α + β),Qy = r * sin(α + β)

有三角函数公式:
cos(α+β) = cos(α) * cos(β) - sin(α) * sin(β)
sin(α+β) = sin(α) * cos(β) + cos(α) * sin(β)

所以:
Qx = r * (cos(α) * cos(β) - sin(α) * sin(β))
	 = r * cos(α) * cos(β) - r * sin(α) * sin(β)
	 = Px * cos(β) - Py * sin(β)															//式1
Qy = r * (sin(α) * cos(β) + cos(α) * sin(β))
	 = r * sin(α) * cos(β) + r * cos(α) * sin(β)
	 = r * cos(α) * sin(β) + r * sin(α) * cos(β)//前后换一下
	 = Px * sin(β) + Py * cos(β)															//式2

这样根据式1和式2就可以整合为矩阵乘法的形式:
			  [cos(β)   sin(β)]
(Px,Py) * |                      |  = (Qx,Qy)
			  [-sin(β)  cos(β)]
这个2x2矩阵就表示2D旋转矩阵.

----------------------------------------------------------------------------------------

2D中的缩放矩阵(沿着坐标轴缩放):
2D中有两个缩放因子,kx和ky.
原坐标系中的两个基向量p0和q0为:[1,0],[0,1].
那么应用缩放因子后,也就是变换后的坐标系中的两个对应的基向量p1和q1为:
p1 = kx * p0 = kx * [1,0] = [kx,0]
q1 = ky * q0 = ky * [0,1] = [0,ky]
这样,我们就可以用变换后坐标系中的这两个基向量来构建缩放矩阵了:
				[p1]    [kx 0]
S(kx,ky) = |   | = |      |
				[q1]    [ky 0]

2D中沿任意方向缩放
设n为平行于缩放方向的单位向量,k为缩放因子.
我们需要推导出一个表达式,给定向量v0,可以通过v,n和k来计算v1.
为了做到这一点,将v0分解为两个分量v0Parallel和v0Perpendicular,分别平行于n和垂直于n.
因为v0Perpendicular垂直于n,它不会被缩放操作影响.因此,v1 = v1Parallel + v0Perpendicular.那么怎么样得到v1Parallel呢?由于
v0Parallel平行于缩放方向,所以v1Parallel = k * v0Parallel.
总结已知向量,并进行代换,得到:
v0 = v0Parallel + v0Perpendicular
v0Parallel = (v . n) * n
v1Perpendicular = v0Perpendicular
					   = v0 - v0Parallel
					   = v - (v . n) * n
v1Parallel = k * v0Parallel
			   = k * (v . n) * n
v1 = v1Perpendicular + v1Parallel
	= v - (v . n) * n + k * (v . n) * n
	= v + (k - 1) * (v . n) * n
这样我们就计算出来了这个公式.接下来,我们就用原坐标系中的两个基向量p0[1,0]和q0[0,1]来带入这个公式中的v0,就可以知道原坐标系中的两个
基向量p0和q0经过变换后的结果,然后用变换后的p1和q1来作为矩阵的行,这样,我们就可以构建矩阵出来了.
			 [p1]     [1 + (k - 1) * n.x * n.x     (k - 1) * n.x * n.y       ]
S(n,k) = |    | = |                                                                     |
			 [q1]     [(k - 1) * n.x * n.y            1 + (k - 1) * n.y * n.y]
*/

/*
切变
切变是一种坐标系"扭曲"变换,非均匀地拉伸它.
切变的时候角度会发生变化,但令人惊奇的是面积和体积却保持不变.
基本思想是将某一坐标的乘积加到另一个上.例如,2D中,将y乘以某个因子然后加到x上,得到x1 = x0 + s * y0.如下图所示:
					y
					| s
					|---/---------------/
					|   /                    /
					|  /                    /
					| /                    /
					|/                    /
					----------------------------x
实现这个切变变换的矩阵为:
			[1 0]
Hx(s) = |     |
			[s 1]
记法Hx的意义是x坐标根据坐标y被切变,参数s控制着切变的方向和量.
			[1 s]
Hy(s) = |     |
			[0 1]

3D中的切变方法是取出一个坐标,乘以不同因子,再加到其他两个坐标上.
记法Hxy的意义是x,y坐标被坐标z改变.公式如下:
				[1 0 0]
Hxy(s,t) = |0 1 0|
				[s  t 1]

				[1 0 0]
Hxz(s,t) = |s  1 t|
				[0 0 1]

				[1 s  t]
Hyz(s,t) = |0 1 0|
				[0 0 1]
*/

 /*
 向量v和矩阵mat进行相乘时的写法:v * mat.可以看出,向量为行向量,进行相乘时为:1x3的行向量 * 3x3的矩阵 = 1x3的行向量.
 矩阵以行为主,即:第一行代表x轴,第二行代表y轴,第三行代表z轴.

 设向量v,矩阵A和B,那么v * A * B计算得到retV.如果把v放到矩阵的右边,如何才能得到相等的retV?
 方法为:Bt * At * v = retV.
 */
 /*
 在右手坐标系中,从旋转轴的正端点看向负端点,认为逆时针旋转为正方向.
 下面矩阵的每一行,都是原坐标系的三个标准正交基经过旋转后,而形成的新标准正交基.
 RX =  1        0           0
		  0     cos(t)     sin(t)
		  0     -sin(t)    cos(t)

 RY =  cos(t)    0     -sin(t)
		   0         1       0
		  sin(t)     0     cos(t)

 RZ =  cos(t)    sin(t)     0
		  -sin(t)    cos(t)    0
			0          0          1
 */

 /*
 使用范围for语句处理多维数组.这种语句可以遍历容器或其它序列的所有元素.
 序列:拥有能返回迭代器的begin和end成员.
 范围for语句中,预存了end()的值,一旦在序列中添加(删除)元素,end函数的值就可能变得无效了.
 这种语句遍历给定序列中的每个元素,并对序列中的每个值执行某种操作,其语法形式是:
 for(declaration : expression)
 statement
 其中,expression部分是一个对象,用于表示一个序列.declaration部分负责定义一个变量,该变量将被用于访问序列中的基础元素.每次迭代,
 declaration部分的变量会被初始化为expression部分的下一个元素值.
 虽然我们通常称new T[]分配的内存为"动态数组",但这种叫法某种程度上有些误导.当用new分配一个数组时,我们并未得到一个数组类型的对象,
 而是得到一个数组元素类型的指针.
 由于分配的内存并不是一个数组类型,因此不能对动态数组调用begin或end.这些函数使用数组维度来返回指向首元素和尾后元素的指针.出于相同
 的原因,也不能用范围for语句来处理(所谓的)动态数组中的元素.
 要记住,我们所说的动态数组并不是数组类型,这是很重要的.

 这里我们要改变元素的值,所以把控制变量row和e声明成了引用类型.其实,还有一个深层次的原因促使我们这么做.
 将外层循环的控制变量声明成了引用类型,这是为了避免数组被自动转成指针.假设不用引用类型,则循环如下述形式:
 for (auto row : ia)
	for (auto e : row)
		std::cout << col << std::endl;
 程序将无法通过编译.
 这是因为row不是引用类型,所以编译器初始化row时会自动将这些数组形式的元素(和其他类型的数组一样)转换成指向该数组内首元素的指针.
 这样得到的row的类型就是int*,显然内层的循环就不合法了,编译器将试图在一个int*内遍历.
 谨记:要使用范围for语句处理多维数组,除了最内层的循环外,其他所有循环的控制变量都应该是引用类型.
 */

 /*
 int ia[3][4];//大小为3的数组,每个元素是含有4个整数的数组
 int(*p)[4] = ia;//p指向含有4个整数的数组
 p = &ia[2];//p指向ia的尾元素
 我们首先明确(*p)意味着p是一个指针.接着观察右边发现,指针p所指的是一个维度为4的数组;再观察左边知道,数组中的元素是整数.因此,p就
 是指向含有4个整数的数组的指针.

 当程序使用多维数组的名字时,也会自动将其转换成指向数组首元素的指针.
 因为多维数组实际上是数组的数组,所以由多维数组名转换得来的指针实际上是指向第一个内层数组的指针.
 和所有数组一样,当将多维数组传递给函数时,真正传递的是指向数组首元素的指针.因为我们处理的是数组的数组,所以首元素本身就是一个数组,
 指针就是一个指向数组的指针.数组第二维(以及后面所有维度)的大小都是数组类型的一部分,不能省略:
 void print(int(*matrix)[3], int rowSize);		//matrix指向数组的首元素,该数组的元素是由3个整数构成的数组
 再一次强调,*matrix两端的括号必不可少:
 int *matrix[3];		//3个指针构成的数组.
 int(*matrix)[3];		//指向含有3个整数的数组的指针.
 */

 /*
 正交变换
 正交变换的基本思想是轴保持互相垂直,而且不进行缩放变换.
 平移,旋转和镜像(反射)是仅有的正交变换.

 正交矩阵
 若方阵M是正交的,则当且仅当M与它的转置矩阵T的乘积等于单位矩阵.
 我们知道矩阵乘以它的逆矩阵等于单位矩阵,所以,如果一个矩阵是正交的,那么它的转置矩阵等于它的逆矩阵.
 正交矩阵的行列式为1或-1.
 所有正交矩阵都是仿射和可逆的.
 旋转和镜像(反射)矩阵是正交的.

 矩阵正交化:
 有时可能会遇到略微违反了正交性的矩阵.例如,浮点数运算的累计错误.
 这种情况下需要做矩阵正交化,来得到一个正交矩阵.
 */

/*
Axis-Angle to Matrix(绕任意轴旋转r弧度).

第一种推导:
用单位向量n描述旋转轴,n指向屏幕内.,alpha描述旋转量.
我们想得到满足下面条件的矩阵R(n,alpha)
pR(n,alpha) = q
q是向量p绕轴n旋转后的向量.
我们的想法是在垂直于n的平面中解决这个问题.为了做到这一点,将p分解为两个分量:pParallel(平行)和pPerpendicular(垂直),分别平行于n
和垂直于n,并有p = pParallel + pPerpendicular.
因为pParallel平行于n,所以绕n旋转不会影响它.故只要计算出pPerpendicular绕n旋转后的qPerpendicular,就能得到:
q = pParallel + qPerpendicular.
为了计算qPerpendicular,我们构造向量pParallel,pPerpendicular和临时向量w.
w是同时垂直于pParallel和pPerpendicular的向量.它的长度和pPerpendicular的相同.
w和pPerpendicular同在垂直于n的平面中.
w是pPerpendicular绕n旋转90度的结果,由n X(叉积) pPerpendicular可以得到.

												  /--------------pPerpendicular
									pParallel /| \
												/ |   \
											   /  |    \qPerpendicular
											n/   |      \				向量p:从n起始,pPerpendicular结束.
												  w					向量q:从n起始,qPerpendicular结束

现在,q垂直于n的分量可以表示为:
//pPerpendicular和w为基向量,这个表达式是把qPerpendicular给展开为"扩展"形式
qPerpendicular = pPerpendicular * cos(alpha) + w * sin(alpha)
接下来替换pPerpendicular和w:
pParallel = (p.n)n
pPerpendicular = p - pParallel = p - (p.n)n
w = n X pPerpendicular
	= n X (p - pParallel)
	= n X p - n X pParallel
	= n X p - 0
	= n X p
那么,qPerpendicular = pPerpendicular * cos(alpha) + w * sin(alpha)
								= (p - (p.n)n) * cos(alpha) + (n X p) * sin(alpha)

带入q = pParallel + qPerpendicular,有:
q = (p.n)n + (p - (p.n)n) * cos(alpha) + (n X p) * sin(alpha)
至此,已经得到q与p,n和alpha的关系公式了,可以用它来计算变换后的基向量并构造矩阵.
我们知道向量p的三个基向量是:[1,0,0],[0,1,0],[0,0,1]
那么把第一个基向量带入到上式就得到了矩阵的第一行为:
[nx * nx * (1 - cos(alpha)) + cos(alpha),nx * ny * (1 - cos(alpha)) + nz * sin(alpha),nx * nz * (1 - cos(alpha)) - ny * sin(alpha)]
第二行为:
[nx * ny * (1 - cos(alpha)) - nz * sin(alpha),ny * ny * (1 - cos(alpha)) + cos(alpha),ny * nz * (1 - cos(alpha)) + nx * sin(alpha)]
第三行为:
[nx * nz * (1 - cos(alpha)) + ny * sin(alpha),ny * nz * (1 - cos(alpha)) - nx * sin(alpha),nz * nz * (1 - cos(alpha)) + cos(alpha)]

至此,我们就得到了绕任意轴的3D旋转矩阵R(n,alpha).

第二种推导:
U,V和W为标准正交基.
向量X可以表达为:X = u0U + v0V + w0W(根据基向量来线性组合当前向量)
让向量X绕W轴旋转θ角度,可以表达为RX = u1U + v1V + w1W(1:认为X为列向量.2:旋转后得到的向量,还是在这个坐标系中,依然可以用
这个坐标系的基向量来进行线性组合,只不过线性组合的参数发生了变化.)
因为是绕W轴旋转,所以旋转前后向量X的w分量是相同的.所以w1 = w0 = W.X,为什么等于一个点积呢?首先我们知道点积的结果是一个标量,
那么这里想表达的就是在W轴上的坐标值,这个坐标值为|W| * |X| * cos(alpha),注意alpha为X和W轴之间的夹角,注意和前面的θ进行区别,
又因为W为单位向量,所以这个坐标值为|X| * cos(alpha),通过画图就可以看出,向量X的w分量的确是|X| * cos(alpha).
那么向量X剩下的两个分量就可以看作是在一个2D平面中旋转了.
所以有u1 = cos(θ)u0 - sin(θ)v0,v1 = sin(θ)u0 + cos(θ)v0
接下来,我们写两个等式:
式1:
W X X = u0W X U + v0W X V + w0W X W = -v0U + u0V(其中W X U为V,W X V为-U,自己叉乘自己为0)
式2:
W X (W X X) = -v0W X U + u0W X V = -u0U - v0V(其中W X V为-U,W X U为V)
接下来,把式1和式2结合起来:
(sinθ)W X X + (1 - cosθ)W X (W X X) = (-v0sinθ - u0(1 - cosθ))U = (u0sinθ - v0(1 - cosθ))V
														= (u1 - u0)U + (v1 -v0)V
														= RX - X
那么RX = X + (sinθ)W X X + (1 - cosθ)W X (W X X)
根据Vec3<T>中,关于叉积的资料,这里我们把轴W给表达为W = (a,b,c),然后得出一个斜对称矩阵S.(注意这里的W位于叉乘的左边)

S:
0   -c   b
c    0   -a
-b   a   0
然后,我们可以把旋转矩阵R给写为:
R = I + (sinθ)S + (1 - cosθ)S^2(表示S * S),再进一步把R写为:
R = (cosθ) * I + (sinθ) * S + (1 - cosθ) * (S^2 + I)
先计算S^2 + I为:
a * a			ab			ac
ab				b * b		bc
ac				bc			c * c

这样R就为:
(1 - cosθ) * a * a + cosθ			(1 - cosθ) * a * b - sinθ * c			(1 - cosθ) * a * c + sinθ * b
(1 - cosθ) * a * b + sinθ * c		(1 - cosθ) * b * b + cosθ				(1 - cosθ) * b * c - sinθ * a
(1 - cosθ) * a * c - sinθ * b		(1 - cosθ) * b * c + sinθ * a			(1 - cosθ) * c * c + cosθ
但是因为我们上面是R * X,也就是说,我们把X写成了列向量,而要把X写成行向量,即放到左边,我们就需要对R进行一个转置操作,令R(t)为M,
则M为:
(1 - cosθ) * a * a + cosθ			(1 - cosθ) * a * b + sinθ * c			(1 - cosθ) * a * c - sinθ * b
(1 - cosθ) * a * b - sinθ * c		(1 - cosθ) * b * b + cosθ				(1 - cosθ) * b * c + sinθ * a
(1 - cosθ) * a * c + sinθ * b		(1 - cosθ) * b * c - sinθ * a			(1 - cosθ) * c * c + cosθ

A:加减
M:乘法
D:除法
F:调用函数,比如单位化等
C:比较
13A + 15M + 2F
*/

/*
Matrix to Axis-Angle
抽取的轴为单位长度.
trace:矩阵的轨迹被定义为对角线上元素之和.即:m[0][0] + m[1][1] + m[2][2]
有两个公式:
cos(θ) = (trace(R) - 1) / 2							//式1
R - R^t = 2 * sin(θ) * S(S为斜对角矩阵)			//式2

我们要分为3中情况:
1:θ = 0,不存在旋转,所以可以返回任何一个单位向量.
2:0 < θ < PI,这种情况,允许直接抽取轴.D = (r21 - r12,r02 - r20,r10 - r01),然后W = D / |D|.
3:θ = PI,这种情况,sinθ为0,所以式2为0,已然不能够去使用式2了.但是我们知道:R = I + (sinθ)S + (1 - cosθ)S^2(表示S * S),
所以(1 - cosθ) = 2,故,R = I + 2S^2
		1 - 2(w1^2 + w^2)				2w0w1							2w0w2
R =	2w0w1							1 - 2(w0^2 + w2^2)			2w1w2
		2w0w2								2w1w2					1 - 2(w0^2 + w1^2)
W为(w0,w1,w2).
现在的办法是,从旋转矩阵的对角项中提取轴的最大分量.
如果r00最大,那么w0就是其magnitude的最大分量(或者说,对其长度/模贡献最大的分量).则:
4w0^2 = r00 - r11 - r22 + 1,我们选择w0 = sqrt(r00 - r11 - r22 + 1) / 2,同样w1 = r01 / (2w0),w2 = r02 / (2w0).
如果r11最大,则:
4w1^2 = r11 - r00 - r22 + 1,我们选择w1 = sqrt(r11 - r00 - r22 + 1) / 2,同样w0 = r01 / (2w1),w2 = r12 / (2w1).
如果r22最大,则:
4w2^2 = r22 - r00 - r11 + 1,我们选择w0 = sqrt(r22 - r00 - r11 + 1) / 2,同样w0 = r02 / (2w2),w1 = r12 / (2w2).

----------------------------------------------------------------------------------------------------------------------------------------

8A + 7M + 1D + 2F
*/

/*
Quater to Matrix

从四元数来构建一个旋转矩阵.
绕任意轴旋转的矩阵,是用n和θ来表示的,但四元数分量是:
w = cos(θ / 2)
x = sin(θ / 2) * n.x
y = sin(θ / 2) * n.y
z = sin(θ / 2) * n.z
那么就需要对矩阵进行变形,以便于带入x,y,z,w
有公式:
2 * (sin2(θ / 2))^2 = 1 - cos(θ)
sin(θ) = 2 * sin(θ / 2) * cos(θ / 2)
最后得到R:
1.0 - twoY2 - twoZ2		twoXY + twoWZ			twoXZ - twoWY
twoXY - twoWZ				1.0 - twoX2 - twoZ2		twoYZ + twoWX
twoXZ + twoWY			twoYZ - twoWX				1.0 - twoX2 - twoY2

-------------------------------------------------------------------------

12A + 12M
*/

/*
正交投影
一般来说,投影意味着降维操作.
有一种投影方法是在某个方向上用0作为缩放因子.这种情况下,所有点都被拉平至垂直的轴(2D)或平面(3D)上.这种类型的投影称作正交投影.

由于不考虑平移,投影平面必须通过原点.投影由垂直于平面的单位向量n定义(通过使该方向的缩放因子为0).P(n) = S(n,0)
*/

/*
镜像(反射)
镜像是一种变换,使缩放因子为-1能够很容易地实现镜像变换.
n:单位向量.
矩阵将沿通过原点且垂直于n的平面来进行镜像变换.
*/

/*
严格的说,C++中没有多维数组,通常所指的多维数组其实就是数组的数组.
当一个数组的元素仍然是数组时,通常使用两个维度来定义它:一个维度表示数组本身大小,另外一个维度表示其元素(也是数组)大小.
如何阅读多维数组?
int ia[3][4];//大小为3的数组,每个元素是含有4个整数的数组
按照由内而外的顺序阅读此类定义有助于更好地理解其真实含义.
我们定义的名字是ia,显然ia是一个含有3个元素的数组.接着观察右边发现,ia的元素也有自己的维度,所以ia的元素本身又都是含有4个元素的数组.
再观察左边知道,真正存储的元素是整数.因此最后可以明确这条语句的含义:它定义了一个大小为3的数组,该数组的每个元素都是含有4个整数的数
组.
对于二维数组来说,常把第一个维度称作行,第二个维度称作列.
如何初始化多维数组?
int ia[3][4] =			//3个元素,每个元素都是大小为4的数组
{
	{0,1,2,3},				//第1行的初始值
	{4,5,6,7},				//第2行的初始值
	{8,9,10,11}			//第3行的初始值
};
其中内层嵌套着的花括号并非必需的,例如下面的初始化语句,形式上更为简洁,完成的功能和上面这段代码完全一样:
int ia[3][4] = {0,1,2,3,4,5,6,7,8,9,10,11};
类似于一维数组,在初始化多维数组时也并非所有元素的值都必须包含在初始化列表之内.如果仅仅想初始化每一行的第一个元素,通过如下的语句
即可:
int ia[3][4] = {{0},{4},{8}};			//显式地初始化每行的首元素
其它未列出的元素执行默认值初始化,这个过程和一维数组一样.
*/
/*
定义数组的时候必须指定数组的类型,不允许用auto关键字由初始值的列表推断类型.
*/

/*
sizeof运算符:
sizeof运算符返回一条表达式或一个类型名字所占的字节数.其所得的值是一个usize类型的常量表达式.


Sales_data data,*p;
sizeof(Sales_data);					//存储Sales_data类型的对象所占的空间大小
sizeof data;							//data的类型的大小,即sizeof(Sales_data)
sizeof p;								//指针所占的空间大小
sizeof *p;								//p所指类型的空间大小,即sizeof(Sales_data)
sizeof data.revenue;				//Sales_data的revenue成员对应类型的大小
C++11:
sizeof Sales_data::revenue;		//另一种获取revenue大小的方式
上面例子中,最有趣的一个是sizeof *p.首先,因为sizeof满足右结合律并且与*运算符的优先级一样,所以表达式按照从右向左的顺序组合.也就是说,它等价
于sizeof(*p).其次,因为sizeof不会实际求运算对象的值,所以即使p是一个无效(未初始化的)指针也不会有什么影响.在sizeof的运算对象中解引用一个无效
指针仍然是一种安全的行为,因为指针实际上并没有被真正使用.sizeof不需要真的解引用指针也能知道它所指对象的类型.

C++11新标准允许我们使用作用域运算符来获取类成员的大小.通常情况下只有通过类的对象才能访问到类的成员,但是sizeof运算符无须我们提供一个具体的对
象,因为要想知道类成员的大小无须真的获取该成员.

sizeof运算符的结果部分地依赖于其作用的类型:
对char或者类型为char的表达式执行sizeof运算,结果得1.
对引用类型执行sizeof运算得到被引用对象所占空间的大小.
对指针执行sizeof运算得到指针本身所占空间的大小.
对解引用指针执行sizeof运算得到指针指向的对象所占空间的大小,指针不需有效.
对数组执行sizeof运算得到整个数组所占空间的大小,等价于对数组中所有的元素各执行一次sizeof运算并将所得结果求和.注意:sizeof运算不会把数组转换成指针来处理.
对string对象或vector对象执行sizeof运算只返回该类型固定部分的大小,不会计算对象中的元素占用了多少空间.

因为sizeof的返回值是一个常量表达式,所以我们可以用sizeof的结果声明数组的维度.
*/