/*!
 * \brief
 * 线段
 * \file UPELineSegment.h
 *
 * \author Su Yang
 *
 * \date 2017/04/10
 */
#ifndef _UPE_LINESEGMENT_H_
#define _UPE_LINESEGMENT_H_

#ifndef _UPE_CONFIG_H_
#include "UPEConfig.h"
#endif

#ifdef UPE_HIERARCHICAL_COMPILE

#ifndef _UPE_GEOMETRY_H_
#include "UPEGeometry.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 线段
	 * \class LineSegment
	 *
	 * \author Su Yang
	 *
	 * \date 2017/04/10
	 *
	 * \todo
	 */
	class UngExport LineSegment : public Geometry
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		LineSegment() = delete;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param Vector3 const & start 起点
		 * \param Vector3 const & end 终点
		*/
		LineSegment(Vector3 const& start, Vector3 const& end);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~LineSegment() = default;

		/*!
		 * \remarks 获取起点
		 * \return 
		*/
		Vector3 const& getStart() const;

		/*!
		 * \remarks 获取终点
		 * \return 
		*/
		Vector3 const& getEnd() const;

		/*!
		 * \remarks 获取方向
		 * \return 
		*/
		Vector3 getDir() const;

		/*!
		 * \remarks 获取参数所指定的点
		 * \return 
		 * \param real_type t
		*/
		Vector3 getPoint(real_type t) const;

		/*!
		 * \remarks 获取线段的长度
		 * \return 
		*/
		real_type getLength() const;

		/*!
		 * \remarks 获取线段长度的平方
		 * \return 
		*/
		real_type getSquaredLength() const;

	private:
		/*
		S(t) = A + t(B - A)，0 ≤ t ≤ 1
		或
		S(t) = A + td，|d| = 1.0，这时t的范围为[0.0,|mEnd - mStart|]
		*/
		Vector3 mStart;
		Vector3 mEnd;
	};
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

#endif//_UPE_LINESEGMENT_H_