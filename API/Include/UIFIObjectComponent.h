/*!
 * \brief
 * 对象组件接口。
 * \file UIFIObjectComponent.h
 *
 * \author Su Yang
 *
 * \date 2016/12/13
 */
#ifndef _UIF_IOBJECTCOMPONENT_H_
#define _UIF_IOBJECTCOMPONENT_H_

#ifndef _UIF_CONFIG_H_
#include "UIFConfig.h"
#endif

#ifdef UIF_HIERARCHICAL_COMPILE

#ifndef _INCLUDE_TINYXML2_H_
#include "tinyxml2.h"
#define _INCLUDE_TINYXML2_H_
#endif

namespace ung
{
	/*!
	 * \brief
	 * 场景对象组件。(组件就是对feature的封装，组件通过XML中的数据来描述，这也表达了数据驱动。)
	 * \class IObjectComponent
	 *
	 * \author Su Yang
	 *
	 * \date 2016/12/13
	 *
	 * \todo
	 */
	class UngExport IObjectComponent : public virtual MemoryPool<IObjectComponent>
	{
		BOOST_STATIC_ASSERT(boost::has_new_operator<IObjectComponent>::value);

	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		IObjectComponent() = default;

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~IObjectComponent() = default;

		/*!
		 * \remarks 初始化组件
		 * \return 
		 * \param tinyxml2::XMLElement* pComponentElement
		*/
		virtual void init(tinyxml2::XMLElement* pComponentElement) = 0;

		/*!
		 * \remarks 对象组装完成后，对象可能会再根据其都由那些组件所组合的来决定是否再进行一些初始化工作。
		 * 而对象的后初始化工作，会调用组件的后初始化。
		 * \return 
		*/
		virtual void postInit() = 0;

		/*!
		 * \remarks 更新组件
		 * \return 
		*/
		virtual void update() = 0;

		/*!
		 * \remarks 获取组件的名字
		 * \return 
		*/
		virtual String const& getName() const = 0;

		/*!
		 * \remarks 设置该组件所属的对象
		 * \return 
		 * \param StrongIObjectPtr pOwner
		*/
		virtual void setOwner(StrongIObjectPtr pOwner) = 0;
	};
}//namespace ung

#endif//UIF_HIERARCHICAL_COMPILE

#endif//_UIF_IOBJECTCOMPONENT_H_