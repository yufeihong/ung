/*!
 * \brief
 * 通用物理接口
 * \file UPEInterface.h
 *
 * \author Su Yang
 *
 * \date 2017/03/31
 */
#ifndef _UPE_INTERFACE_H_
#define _UPE_INTERFACE_H_

#ifndef _UPE_CONFIG_H_
#include "UPEConfig.h"
#endif

#ifdef UPE_HIERARCHICAL_COMPILE

namespace ung
{
	//临时
	class WeakObjectPtr;

	/*!
	 * \brief
	 * 通用物理接口
	 * \class IPhysics
	 *
	 * \author Su Yang
	 *
	 * \date 2017/03/31
	 *
	 * \todo
	 */
	class IPhysics : public MemoryPool<IPhysics>,boost::noncopyable
	{
	public:
		IPhysics() = default;

		virtual ~IPhysics() = default;

		virtual void initialize() = 0;

		virtual void update() = 0;

		virtual void addSphere(real_type radius, WeakObjectPtr object) = 0;

		virtual void addBox(Vector3 const& min, Vector3 const& max, WeakObjectPtr object) = 0;
	};
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

#endif//_UPE_INTERFACE_H_