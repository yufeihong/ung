/*!
 * \brief
 * 子mesh。
 * \file URMSubMesh.h
 *
 * \author Su Yang
 *
 * \date 2016/11/13
 */
#ifndef _URM_SUBMESH_H_
#define _URM_SUBMESH_H_

#ifndef _URM_CONFIG_H_
#include "URMConfig.h"
#endif

#ifdef URM_HIERARCHICAL_COMPILE

//#ifndef _INCLUDE_STLIB_MUTEX_
//#include <mutex>
//#define _INCLUDE_STLIB_MUTEX_
//#endif
//
//#ifndef _INCLUDE_STLIB_FUTURE_
//#include <future>
//#define _INCLUDE_STLIB_FUTURE_
//#endif

namespace ung
{
	/*!
	 * \brief
	 * 子mesh。
	 * \class SubMesh
	 *
	 * \author Su Yang
	 *
	 * \date 2016/11/13
	 *
	 * \todo
	 */
	class SubMesh : public std::enable_shared_from_this<SubMesh>,public MemoryPool<SubMesh>
	{
		friend class Mesh;

	public:
		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~SubMesh();

	private:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		SubMesh();

		/*!
		 * \remarks 获取材质的名字。
		 * \return 
		*/
		String const& getMaterialName() const;

		/*!
		 * \remarks 获取图元的组织模式
		 * \return 
		*/
		String const& getTrianglesMode() const;

		/*!
		 * \remarks 获取子mesh的三角形数量
		 * \return 
		*/
		int32 const getFaceCount() const;

		/*!
		 * \remarks 获取子mesh的顶点数量
		 * \return 
		*/
		int32 const getVertexCount() const;

		/*!
		 * \remarks 获取子mesh的索引数量
		 * \return 
		*/
		int32 const getIndicesCount() const;

		/*!
		 * \remarks 获取子mesh的所有索引
		 * \return 
		 * \param uint32
		*/
		STL_VECTOR(uint32) const& getIndices() const;

		/*!
		 * \remarks 判断子mesh是否有材质
		 * \return 
		*/
		bool hasMaterial() const;

	private:
		/*!
		 * \remarks 设置材质名字
		 * \return 
		 * \param String const & materialName
		*/
		void setMaterialName(String const& materialName);

		/*!
		 * \remarks 设置图元的组织模式
		 * \return 
		 * \param String const & trianglesMode
		*/
		void setTrianglesMode(String const& trianglesMode);

		/*!
		 * \remarks 设置三角形的数量
		 * \return 
		 * \param usize faceCount
		*/
		void setFaceCount(usize faceCount);

		/*!
		 * \remarks 设置顶点数量
		 * \return 
		 * \param usize vertexCount
		*/
		void setVertexCount(usize vertexCount);

		/*!
		 * \remarks 设置索引数量
		 * \return 
		 * \param usize indexCount
		*/
		void setIndexCount(usize indexCount);

		/*!
		 * \remarks 添加索引
		 * \return 
		 * \param uint32 index
		*/
		void addIndices(uint32 index);

		/*!
		 * \remarks 设置后台线程的返回信息
		 * \return 
		 * \param std::shared_future<bool> sft
		*/
		void setFuture(std::shared_future<bool> sft);

		/*!
		 * \remarks 判断是否已经载入
		 * \return 
		*/
		bool isLoaded() const;

		/*!
		 * \remarks 部署子mesh的材质后台载入工作
		 * \return 
		*/
		void buildMaterial() const;

	private:
		String mMaterialName;
		String mTrianglesMode;
		usize mFaceCount = 0;
		usize mVertexCount = 0;
		usize mIndexCount = 0;
		STL_VECTOR(uint32) mIndices;
		mutable bool mIsLoaded = false;
		//mutable std::recursive_mutex mRecursiveMutex;
		//mutable std::shared_future<bool> mNakedFuture;
	};
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

#endif//_URM_SUBMESH_H_