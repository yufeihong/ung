#ifndef _UFC_GENSCATTERHIERARCHY_H_
#define _UFC_GENSCATTERHIERARCHY_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#ifndef _UFC_TYPELIST_H_
#include "UFCTypeList.h"
#endif

#ifndef _UFC_METAFUNCTION_H_
#include "UFCMetaFunction.h"
#endif

namespace ung
{
	namespace utp
	{
		template<typename TList, template<typename> class Unit>
		class GenScatterHierarchy;

		template<typename T1, typename T2, template<typename> class Unit>
		class GenScatterHierarchy<TypeList<T1, T2>, Unit> : public GenScatterHierarchy<T1, Unit>, public GenScatterHierarchy<T2, Unit>
		{
		};

		//传递一个atomic type(nontypelist)给Unit
		template <typename AtomicType, template <typename> class Unit>
		class GenScatterHierarchy : public Unit<AtomicType>
		{
		};

		//NullType
		template <template <typename> class Unit>
		class GenScatterHierarchy<NullType, Unit>
		{
		};

		//通过type name来访问一个base class
		template <typename T, typename TList, template <typename> class Unit>
		Unit<T>& Field(GenScatterHierarchy<TList, Unit>& obj)
		{
			return obj;
		}

		template <typename T, typename TList, template <typename> class Unit>
		Unit<T> const& Field(GenScatterHierarchy<TList, Unit> const& obj)
		{
			return obj;
		}

		template <typename TList, template <typename> class Unit>
		Unit<typename TList::Head>& FieldHelper(GenScatterHierarchy<TList, Unit>& obj, Int2Type<0>)
		{
			GenScatterHierarchy<typename TList::Head, Unit>& leftBase = obj;
			return leftBase;
		}

		template <int i, typename TList, template <typename> class Unit>
		Unit<typename TypeAt<TList, i>::result>& FieldHelper(GenScatterHierarchy<TList, Unit>& obj, Int2Type<i>)
		{
			GenScatterHierarchy<typename TList::Tail, Unit>& rightBase = obj;
			return FieldHelper(rightBase, Int2Type<i - 1>());
		}

		template <int i, typename TList, template <typename> class Unit>
		Unit<typename TypeAt<TList, i>::result>& Field(GenScatterHierarchy<TList, Unit>& obj)
		{
			return FieldHelper(obj, Int2Type<i>());
		}

		template <typename TList, template <typename> class Unit>
		Unit<typename TList::Head> const& FieldHelper(GenScatterHierarchy<TList, Unit> const& obj, Int2Type<0>)
		{
			GenScatterHierarchy<typename TList::Head, Unit> const& leftBase = obj;
			return leftBase;
		}

		template <int i, typename TList, template <typename> class Unit>
		Unit<typename TypeAt<TList, i>::result> const& FieldHelper(GenScatterHierarchy<TList, Unit> const& obj, Int2Type<i>)
		{
			GenScatterHierarchy<typename TList::Tail, Unit> const& rightBase = obj;
			return FieldHelper(rightBase, Int2Type<i - 1>());
		}

		template <int i, typename TList, template <typename> class Unit>
		Unit<typename TypeAt<TList, i>::result> const& Field(GenScatterHierarchy<TList, Unit> const& obj)
		{
			return FieldHelper(obj, Int2Type<i>());
		}
	}//namespace utp
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_GENSCATTERHIERARCHY_H_