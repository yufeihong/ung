/*!
 * \brief
 * 三角形
 * \file UPETriangle.h
 *
 * \author Su Yang
 *
 * \date 2016/06/17
 */
#ifndef _UPE_TRIANGLE_H_
#define _UPE_TRIANGLE_H_

#ifndef _UPE_CONFIG_H_
#include "UPEConfig.h"
#endif

#ifdef UPE_HIERARCHICAL_COMPILE

#ifndef _UPE_GEOMETRY_H_
#include "UPEGeometry.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 三角形
	 * \class Triangle
	 *
	 * \author Su Yang
	 *
	 * \date 2016/11/24
	 *
	 * \todo
	 */
	class UngExport Triangle : public Geometry
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		Triangle() = delete;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param Vector3 const & v1 三角形的三个顶点位置
		 * \param Vector3 const & v2
		 * \param Vector3 const & v3
		*/
		Triangle(Vector3 const& v1,Vector3 const& v2,Vector3 const& v3);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~Triangle();

		/*!
		 * \remarks 获取三角形的三个顶点位置
		 * \return 
		*/
		std::array<Vector3, 3> const& getVertices() const;

	private:
		std::array<Vector3, 3> mVertices;
	};
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

#endif//_UPE_TRIANGLE_H_