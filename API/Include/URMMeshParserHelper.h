/*!
 * \brief
 * 解析mesh的帮助类。
 * \file URMMeshParserHelper.h
 *
 * \author Su Yang
 *
 * \date 2016/11/13
 */
#ifndef _URM_MESHPARSERHELPER_H_
#define _URM_MESHPARSERHELPER_H_

#ifndef _URM_CONFIG_H_
#include "URMConfig.h"
#endif

#ifdef URM_HIERARCHICAL_COMPILE

namespace ung
{
	/*!
	 * \brief
	 * 解析mesh的帮助类。
	 * \class MeshParserHelper
	 *
	 * \author Su Yang
	 *
	 * \date 2016/11/13
	 *
	 * \todo
	 */
	class MeshParserHelper
	{
	public:
		/*!
		 * \remarks 把一个Vector2形式的字符串给转换成一个真正的Vector2
		 * \return 
		 * \param String & stringValue "1.0,2.0"
		 * \param const char * pred
		*/
		static Vector2 stringToDVector2(String& stringValue,const char* pred = ",");

		/*!
		 * \remarks 把一个Vector3形式的字符串给转换成一个真正的Vector3
		 * \return 
		 * \param String & stringValue
		 * \param const char * pred
		*/
		static Vector3 stringToDVector3(String& stringValue,const char* pred = ",");

		/*!
		 * \remarks 把一个Vector4形式的字符串给转换成一个真正的Vector4
		 * \return 
		 * \param String & stringValue
		 * \param const char * pred
		*/
		static Vector4 stringToDVector4(String& stringValue,const char* pred = ",");
	};
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

#endif//_URM_MESHPARSERHELPER_H_