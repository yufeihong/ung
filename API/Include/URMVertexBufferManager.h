/*!
 * \brief
 * 顶点缓冲区管理器
 * \file URMVertexBufferManager.h
 *
 * \author Su Yang
 *
 * \date 2017/06/09
 */
#ifndef _URM_VERTEXBUFFERMANAGER_H_
#define _URM_VERTEXBUFFERMANAGER_H_

#ifndef _URM_CONFIG_H_
#include "URMConfig.h"
#endif

#ifdef URM_HIERARCHICAL_COMPILE

#ifndef _URM_ENUMCOLLECTION_H_
#include "URMEnumCollection.h"
#endif

#ifndef _UFC_SEPARATEDPOINTER_H_
#include "UFCSeparatedPointer.h"
#endif

#ifndef _UIF_IVIDEOBUFFER_H_
#include "UIFIVideoBuffer.h"
#endif

namespace ung
{
	class VertexElement;
	class VertexDeclaration;
	class VertexBuffer;

	/*!
	 * \brief
	 * 顶点缓冲区管理器
	 * \class VertexBufferManager
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/09
	 *
	 * \todo
	 */
	class UngExport VertexBufferManager : public MemoryPool<VertexBufferManager>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		VertexBufferManager();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~VertexBufferManager();

		/*!
		 * \remarks 创建element
		 * \return 
		 * \param uint8 stream
		 * \param uint8 offset
		 * \param VertexElementSemantic semantic
		 * \param VertexElementType theType
		 * \param uint8 index
		*/
		VertexElement* createElement(uint8 stream, uint8 offset, VertexElementSemantic semantic, VertexElementType theType, uint8 index = 0);

		/*!
		 * \remarks 创建声明
		 * \return 
		*/
		virtual SeparatedPointer<VertexDeclaration> createDeclaration() PURE;

		/*!
		 * \remarks 设置顶点声明
		 * \return 
		 * \param VertexDeclaration* pDecl
		*/
		void setDeclaration(VertexDeclaration* pDecl);

		/*!
		 * \remarks 创建顶点缓冲区
		 * \return 
		 * \param uint32 vertexSize
		 * \param uint32 numVertices
		 * \param uint32 usage
		 * \param bool useShadow
		*/
		virtual SeparatedPointer<VertexBuffer> createVertexBuffer(uint32 vertexSize,uint32 numVertices,uint32 usage,bool useShadow) PURE;
	};
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

#endif//_URM_VERTEXBUFFERMANAGER_H_