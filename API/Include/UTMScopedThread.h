#ifndef _UTM_SCOPEDTHREAD_H_
#define _UTM_SCOPEDTHREAD_H_

#ifndef _UTM_CONFIG_H_
#include "UTMConfig.h"
#endif

#ifdef UTM_HIERARCHICAL_COMPILE

#ifndef _INCLUDE_STLIB_THREAD_
#include <thread>
#define _INCLUDE_STLIB_THREAD_
#endif

namespace ung
{
	/*!
	 * \brief
	 * 资源获取即初始化(RAII)，确保在调用该线程的作用域退出之前，该线程执行完成，即便调用该线程的环境发生了异常。
	 * \class ScopedThread
	 *
	 * \author Su Yang
	 *
	 * \date 2016/08/24
	 *
	 * \todo
	 */
	class UngExport ScopedThread : boost::noncopyable				//阻止编译器产生copy构造函数和copy赋值运算符
	{
	public:
		ScopedThread();

		explicit ScopedThread(std::thread t);

		ScopedThread(ScopedThread&& st) noexcept;

		ScopedThread& operator=(ScopedThread&& st) noexcept;

		~ScopedThread();

		std::thread::id getID() const;

		bool operator==(ScopedThread const& st);

		bool operator!=(ScopedThread const& st);

		bool operator<(ScopedThread const& st);

	private:
		std::thread mThread;
		std::thread::id mID;
	};//class ScopedThread

	class UngExport ScopedThreadHasher
	{
	public:
		std::size_t operator()(const ScopedThread& st) const;
	};
}//namespace ung

#endif//UTM_HIERARCHICAL_COMPILE

#endif//_UTM_SCOPEDTHREAD_H_