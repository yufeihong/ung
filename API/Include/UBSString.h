/*!
 * \brief
 * �ַ������͡�
 * \file UBSString.h
 *
 * \author Su Yang
 *
 * \date 2016/01/31
 */
#ifndef _UBS_STRING_H_
#define _UBS_STRING_H_

#ifndef _UBS_CONFIG_H_
#include "UBSConfig.h"
#endif

#ifdef UBS_HIERARCHICAL_COMPILE

#ifndef _INCLUDE_STLIB_STRING_
#include <string>
#define _INCLUDE_STLIB_STRING_
#endif

namespace ung
{
	using String = std::string;
	using StringW = std::wstring;
	using String16 = std::u16string;
	using String32 = std::u32string;

#define EMPTY_STRING String{}
}//namespace ung

#endif//UBS_HIERARCHICAL_COMPILE

#endif//_UBS_STRING_H_