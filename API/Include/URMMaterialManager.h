/*!
 * \brief
 * 材质管理器，支持多线程安全。
 * \file URMMaterialManager.h
 *
 * \author Su Yang
 *
 * \date 2016/11/13
 */
#ifndef _URM_MATERIALMANAGER_H_
#define _URM_MATERIALMANAGER_H_

#ifndef _URM_CONFIG_H_
#include "URMConfig.h"
#endif

#ifdef URM_HIERARCHICAL_COMPILE

#ifndef _URM_MATERIAL_H_
#include "URMMaterial.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 材质管理器，支持多线程安全。
	 * \class MaterialManager
	 *
	 * \author Su Yang
	 *
	 * \date 2016/11/13
	 *
	 * \todo
	 */
	class MaterialManager : public Singleton<MaterialManager>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		MaterialManager();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~MaterialManager();

		/*!
		 * \remarks 创建一个材质，并且存储到容器中
		 * \return 
		 * \param String const & materialName 仅文件名，不包含路径
		*/
		std::shared_ptr<Material> createMaterial(String const& materialName);

		/*!
		 * \remarks 销毁给定名字的材质
		 * \return 
		 * \param String const & materialName
		*/
		void destroyMaterial(String const& materialName);

		/*!
		 * \remarks 获取给定名字的材质
		 * \return 
		 * \param String const & materialName
		*/
		std::shared_ptr<Material> getMaterial(String const& materialName);

		/*!
		 * \remarks 获取给定名字的材质，常量对象调用
		 * \return 
		 * \param String const & materialName
		*/
		std::shared_ptr<const Material> getMaterial(String const& materialName) const;

		/*!
		 * \remarks 获取材质的数量
		 * \return 
		*/
		uint32 getMaterialCount() const;

	private:
		/*!
		 * \remarks 把材质给存储到容器中
		 * \return 
		 * \param String const & materialFullName
		 * \param std::shared_ptr<Material> materialPtr
		*/
		void addMaterial(String const& materialFullName,std::shared_ptr<Material> materialPtr);

		/*!
		 * \remarks 从容器中删除掉给定名字的材质
		 * \return 
		 * \param String const & materialName
		*/
		void removeMaterial(String const& materialName);

	private:
		std::unordered_map<String, std::shared_ptr<Material>> mMaterials;
	};
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

#endif//_URM_MATERIALMANAGER_H_