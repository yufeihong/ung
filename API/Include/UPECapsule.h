/*!
 * \brief
 * 胶囊体(代替圆柱体，圆柱体的相交测试代价昂贵)(球体扫掠体SSV)
 * \file UPECapsule.h
 *
 * \author Su Yang
 *
 * \date 2017/04/06
 */
#ifndef _UPE_CAPSULE_H_
#define _UPE_CAPSULE_H_

#ifndef _UPE_CONFIG_H_
#include "UPEConfig.h"
#endif

#ifdef UPE_HIERARCHICAL_COMPILE

#ifndef _UIF_IBOUNDINGVOLUME_H_
#include "UIFIBoundingVolume.h"
#endif

namespace ung
{
	class UngExport Capsule : public IBoundingVolume
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		Capsule() = delete;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param Vector3 const & start
		 * \param Vector3 const & end
		 * \param real_type radius
		*/
		Capsule(Vector3 const& start,Vector3 const& end,real_type radius);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~Capsule() = default;

		/*!
		 * \remarks 获取线段的起点
		 * \return 
		*/
		Vector3 const& getStart() const;

		/*!
		 * \remarks 获取线段的终点
		 * \return 
		*/
		Vector3 const& getEnd() const;

		/*!
		 * \remarks 获取半径
		 * \return 
		*/
		real_type getRadius() const;

	private:
		Vector3 mLineSegmentStart;
		Vector3 mLineSegmentEnd;
		real_type mRadius;
	};
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

#endif//_UPE_CAPSULE_H_