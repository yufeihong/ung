/*!
 * \brief
 * 视口
 * \file URMViewport.h
 *
 * \author Su Yang
 *
 * \date 2017/06/18
 */
#ifndef _URM_VIEWPORT_H_
#define _URM_VIEWPORT_H_

#ifndef _URM_CONFIG_H_
#include "URMConfig.h"
#endif

#ifdef URM_HIERARCHICAL_COMPILE

#ifndef _UFC_COLORVALUE_H_
#include "UFCColorValue.h"
#endif

namespace ung
{
	class Camera;
	class IRenderTarget;

	/*!
	 * \brief
	 * 视口
	 * \class Viewport
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/18
	 *
	 * \todo
	 */
	class UngExport Viewport : public MemoryPool<Viewport>
	{
	public:
		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param int32 zOrder
		*/
		Viewport(int32 zOrder = 0);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~Viewport();

		/*!
		 * \remarks clear
		 * \return 
		 * \param uint32 buffers
		 * \param const real_color & color
		 * \param real_type depth
		 * \param uint32 stencil
		*/
		virtual void clear(uint32 buffers, const real_color& color = UNG_COLOR_BLACK,
			real_type depth = 1.0, uint32 stencil = 0) PURE;

		/*!
		 * \remarks 获取左
		 * \return 
		*/
		virtual uint32 getLeft() const PURE;

		/*!
		 * \remarks 获取上
		 * \return 
		*/
		virtual uint32 getTop() const PURE;

		/*!
		 * \remarks 获取宽
		 * \return 
		*/
		virtual uint32 getWidth() const PURE;

		/*!
		 * \remarks 获取高
		 * \return 
		*/
		virtual uint32 getHeight() const PURE;

		/*!
		 * \remarks 获取min z
		 * \return 
		*/
		virtual real_type getMinZ() const PURE;

		/*!
		 * \remarks 获取max z
		 * \return 
		*/
		virtual real_type getMaxZ() const PURE;

		/*!
		 * \remarks 设置视口
		 * \return 
		*/
		virtual void set() PURE;

		/*!
		 * \remarks 设置z order
		 * \return 
		 * \param int32 zOrder
		*/
		void setZOrder(int32 zOrder);

		/*!
		 * \remarks 获取z order
		 * \return 
		*/
		int32 getZOrder() const;

		/*!
		 * \remarks 关联渲染目标
		 * \return 
		 * \param IRenderTarget * renderTarget
		*/
		void attachRenderTarget(IRenderTarget* renderTarget);

		/*!
		 * \remarks 分离渲染目标
		 * \return 
		*/
		void detachRenderTarget();

		/*!
		 * \remarks 获取关联的渲染目标
		 * \return 
		*/
		IRenderTarget* getAttachedRenderTarget() const;

		/*!
		 * \remarks 关联摄像机
		 * \return 
		 * \param Camera * cam
		*/
		void attachCamera(Camera* cam);

		/*!
		 * \remarks 分离摄像机
		 * \return 
		*/
		void detachCamera();

		/*!
		 * \remarks 获取关联的摄像机
		 * \return 
		*/
		Camera* getAttachedCamera() const;

	protected:
		Camera* mCamera;
		IRenderTarget* mRenderTarget;
		int32 mZOrder;
	};
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

#endif//_URM_VIEWPORT_H_