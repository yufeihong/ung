/*!
 * \brief
 * 向下，交叉转型
 * \file UFCPolymorphicCast.h
 *
 * \author Su Yang
 *
 * \date 2017/06/17
 */
#ifndef _UFC_POLYMORPHICCAST_H_
#define _UFC_POLYMORPHICCAST_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

#ifndef _INCLUDE_BOOST_CAST_HPP_
#include "boost/cast.hpp"
#define _INCLUDE_BOOST_CAST_HPP_
#endif

#ifndef _INCLUDE_BOOST_TYPETRAITS_HPP_
#include "boost/type_traits.hpp"
#define _INCLUDE_BOOST_TYPETRAITS_HPP_
#endif

namespace ung
{
	/*!
	 * \brief
	 * 辅助函数对象
	 * \class PolyCastImpl
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/17
	 *
	 * \todo
	 */
	template<typename Target,typename Source,bool isPointer>
	struct PolyCastImpl;

	/*!
	 * \brief
	 * 模板偏特化(转型指针)
	 * \class PolyCastImpl
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/17
	 *
	 * \todo
	 */
	template<typename Target, typename Source>
	struct PolyCastImpl<Target, Source, true>
	{
		Target operator()(Source p)
		{
			Target temp = dynamic_cast<Target>(p);

			if (!temp)
			{
				throw std::bad_cast();
			}

			return temp;
		}
	};

	/*!
	 * \brief
	 * 模板偏特化(转型引用)
	 * \class PolyCastImpl
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/17
	 *
	 * \todo
	 */
	template<typename Target, typename Source>
	struct PolyCastImpl<Target, Source, false>
	{
		Target operator()(Source p)
		{
			return dynamic_cast<Target>(p);
		}
	};

	/*!
	 * \remarks 转换指针或引用
	 * \return 
	 * \param Source p
	*/
	template<typename Target, typename Source>
	inline Target PolyCast(Source p)
	{
		//处理Target元数据
		using T = typename boost::conditional<boost::is_pointer<Target>::value,
										typename boost::remove_pointer<Target>::type,
										typename boost::remove_reference<Target>::type>::type;

		//处理Source元数据
		using S = typename boost::conditional<boost::is_pointer<Source>::value,
			typename boost::remove_pointer<Source>::type,
			typename boost::remove_reference<Source>::type>::type;

		BOOST_STATIC_ASSERT(boost::is_polymorphic<T>::value);
		BOOST_STATIC_ASSERT(boost::is_polymorphic<S>::value);

		return PolyCastImpl<Target, Source, boost::is_pointer<Target>::value>()(p);
	}
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_POLYMORPHICCAST_H_

/*
多态对象间的类型转换可以分为两类：
从基类到派生类的向下转型和从一个基类到另一个基类的交叉转型。

"boost/cast.hpp"
using namespace boost;

polymorphic_downcast:
向下转型，它使用static_cast提供高效的转型操作，但不具备dynamic_cast的错误检测能力。

polymorphic_cast:
对dynamic_cast的一个简单包装，只能对指针执行向下，交叉转型，它内部做了空指针检查，抛出异常，不
能对引用进行转型。
*/