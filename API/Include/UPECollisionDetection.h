/*!
 * \brief
 * 碰撞检测
 * \file UPECollisionDetection.h
 *
 * \author Su Yang
 *
 * \date 2016/05/22
 */
#ifndef _UPE_COLLISIONDETECTION_H_
#define _UPE_COLLISIONDETECTION_H_

#ifndef _UPE_CONFIG_H_
#include "UPEConfig.h"
#endif

#ifdef UPE_HIERARCHICAL_COMPILE

namespace ung
{
	class LineSegment;
	class Ray;
	class Plane;
	class Sphere;
	class AABB;
	class Triangle;
	class Quadrilateral;
	class Capsule;
	class Lozenge;
	class Cone;
	class Cylinder;
	class PlaneBoundedVolume;

	/*!
	 * \brief
	 * 物理相交
	 * \class CollisionDetection
	 *
	 * \author Su Yang
	 *
	 * \date 2016/05/22
	 *
	 * \todo
	 */
	class UngExport CollisionDetection
	{
	public:
		/*!
		 * \remarks 直线-直线
		 * \return 
		 * \param Vector3 const & p1 直线1上的一点
		 * \param Vector3 const & q1 直线1上的另一点
		 * \param Vector3 const & p2 直线2上的一点
		 * \param Vector3 const & q2 直线2上的另一点
		 * \param Vector3 & out 交点
		*/
		static bool intersects(Vector3 const& p1,Vector3 const& q1,Vector3 const& p2,Vector3 const& q2,Vector3& out);

		/*!
		 * \remarks 直线-三角形
		 * \return 
		 * \param Vector3 const & p 直线上一点
		 * \param Vector3 const & q 直线上另一点
		 * \param Triangle const & triangle 逆时针环绕
		 * \param real_type & u 交点的质心坐标(交点坐标为:u * a + v * b + w * c)
		 * \param real_type & v
		 * \param real_type & w
		*/
		static bool intersects(Vector3 const& p, Vector3 const& q, Triangle const& triangle, real_type& u, real_type& v, real_type& w);

		/*!
		 * \remarks 直线-四边形
		 * \return 
		 * \param Vector3 const & p 直线上一点
		 * \param Vector3 const & q 直线上另一点
		 * \param Quadrilateral const & quad 逆时针环绕
		 * \param Vector3 & out 交点
		*/
		static bool intersects(Vector3 const& p, Vector3 const& q, Quadrilateral const& quad, Vector3& out);

		//---------------------------------------------------------------------------------------------------------------------------

		/*!
		 * \remarks 线段-线段(2D)
		 * \return 两条线段必须构成“X”状，才认为是相交了，构成“L”或“--”状，认为没有相交
		 * \param Vector2 const & start1
		 * \param Vector2 const & end1
		 * \param Vector2 const & start2
		 * \param Vector2 const & end2
		 * \param Vector2 & out 交点
		*/
		static bool intersects(Vector2 const& start1,Vector2 const& end1,Vector2 const& start2,Vector2 const& end2,Vector2& out);

		/*!
		 * \remarks 线段-平面
		 * \return 
		 * \param LineSegment const & lineSegment
		 * \param Plane const & plane
		 * \param Vector3 & out
		*/
		static bool intersects(LineSegment const& lineSegment, Plane const& plane, Vector3& out);

		/*!
		 * \remarks 线段-三角形
		 * \return 
		 * \param LineSegment const & lineSegment (从线段的方向去看，三角形必须为逆时针环绕)
		 * \param Triangle const & triangle
		 * \param real_type & u 交点的质心坐标(交点坐标为:u * a + v * b + w * c)
		 * \param real_type & v
		 * \param real_type & w
		*/
		static bool intersects(LineSegment const& lineSegment, Triangle const& triangle, real_type& u, real_type& v, real_type& w);

		/*!
		 * \remarks 线段-球体
		 * \return 
		 * \param LineSegment const & lineSegment
		 * \param Sphere const & sphere
		 * \param Vector3 & out
		*/
		static bool intersects(LineSegment const& lineSegment, Sphere const& sphere, Vector3& out);

		/*!
		 * \remarks 线段-凸多面体
		 * \return 
		 * \param LineSegment const & lineSegment
		 * \param PlaneBoundedVolume const & planeBV
		 * \param Vector3 & entry 入口处点
		 * \param Vector3 & exit 出口处点
		*/
		static bool intersects(LineSegment const& lineSegment, PlaneBoundedVolume const& planeBV, Vector3& entry, Vector3& exit);

		/*!
		 * \remarks 线段-圆柱体
		 * \return 
		 * \param LineSegment const & lineSegment
		 * \param Cylinder const & cylinder
		 * \param Vector3 & out
		*/
		static bool intersects(LineSegment const& lineSegment,Cylinder const& cylinder,Vector3& out);

		//---------------------------------------------------------------------------------------------------------------------------

		/*!
		 * \remarks 射线-平面
		 * \return 
		 * \param const Ray & ray
		 * \param const Plane & plane
		 * \param Vector3 & out
		*/
		static bool intersects(const Ray& ray, const Plane& plane,Vector3& out);

		/*!
		 * \remarks 射线-三角形
		 * \return 
		 * \param Ray const & ray (从射线的方向去看，三角形必须为逆时针环绕)
		 * \param Triangle const & triangle
		 * \param real_type & u 交点的质心坐标(交点坐标为:u * a + v * b + w * c)
		 * \param real_type & v
		 * \param real_type & w
		*/
		static bool intersects(Ray const& ray, Triangle const& triangle, real_type& u, real_type& v, real_type& w);

		/*!
		 * \remarks 射线-球体
		 * \return 
		 * \param Ray const & ray
		 * \param Sphere const & sphere
		 * \param Vector3 & out
		*/
		static bool intersects(Ray const& ray,Sphere const& sphere,Vector3& out);

		/*!
		 * \remarks 射线-AABB
		 * \return 
		 * \param const Ray & ray
		 * \param const AABB & box
		 * \param Vector3 & out
		*/
		static bool intersects(const Ray& ray,const AABB& box,Vector3& out);

		/*!
		 * \remarks 射线-AABB
		 * \return 
		 * \param const Ray & ray
		 * \param const AABB & box
		 * \param Vector3 & out
		 * \param int32 & entryIndex 入口索引(0:右面,1:上面,2:前面,3:左面,4:下面,5:后面)
		 * \param int32 & exitIndex 出口索引
		*/
		static bool intersects(const Ray& ray,const AABB& box,Vector3& out,int32& entryIndex,int32& exitIndex);

		/*!
		 * \remarks 射线-凸多面体
		 * \return 
		 * \param Ray const & ray
		 * \param PlaneBoundedVolume const & planeBV
		 * \param Vector3 & entry 入口处点
		 * \param Vector3 & exit 出口处点
		*/
		static bool intersects(Ray const& ray, PlaneBoundedVolume const& planeBV, Vector3& entry, Vector3& exit);

		/*!
		 * \remarks 射线-圆柱体
		 * \return 
		 * \param Ray const & ray
		 * \param Cylinder const & cylinder
		 * \param Vector3 & out
		*/
		static bool intersects(Ray const& ray, Cylinder const& cylinder, Vector3& out);

		//---------------------------------------------------------------------------------------------------------------------------

		/*!
		 * \remarks 平面-平面
		 * \return 
		 * \param Plane const & plane1
		 * \param Plane const & plane2
		 * \param Vector3 & p0 交线上的一点
		 * \param Vector3 & lineDir 交线的方向
		*/
		static bool intersects(Plane const& plane1,Plane const& plane2,Vector3& p0, Vector3& lineDir);

		/*!
		 * \remarks 平面-线段
		 * \return 
		 * \param Plane const & plane
		 * \param LineSegment const & lineSegment
		 * \param Vector3 & out
		*/
		static bool intersects(Plane const& plane,LineSegment const& lineSegment,Vector3& out);

		/*!
		 * \remarks 平面-射线
		 * \return 
		 * \param const Plane & plane
		 * \param const Ray & ray
		 * \param Vector3 & out
		*/
		static bool intersects(const Plane& plane, const Ray& ray, Vector3& out);

		/*!
		 * \remarks 平面-球体
		 * \return 
		 * \param const Plane & p
		 * \param const Sphere & s
		*/
		static bool intersects(const Plane& p,const Sphere& s);

		/*!
		 * \remarks 平面-AABB
		 * \return 
		 * \param const Plane & plane
		 * \param const AABB & box
		*/
		static bool intersects(const Plane& plane, const AABB& box);

		/*!
		 * \remarks 平面-圆锥体
		 * \return 
		 * \param const Plane & plane
		 * \param const Cone & cone
		*/
		static bool intersects(const Plane& plane,const Cone& cone);

		/*!
		 * \remarks 平面负半空间-球体(将平面的负半空间看做一个几何实体)
		 * \return 
		 * \param const Plane & p
		 * \param const Sphere & s
		 * \param bool negativeHalfSpace
		*/
		static bool intersects(const Plane& p,const Sphere& s,bool negativeHalfSpace);

		/*!
		 * \remarks 平面负半空间-圆锥体(将平面的负半空间看做一个几何实体)
		 * \return 
		 * \param const Cone & cone
		 * \param const Plane & plane
		 * \param bool negativeHalfSpace
		*/
		static bool intersects(const Cone& cone, const Plane& plane,bool negativeHalfSpace);

		//---------------------------------------------------------------------------------------------------------------------------

		/*!
		 * \remarks 三角形-直线
		 * \return 
		 * \param Triangle const & triangle 逆时针环绕
		 * \param Vector3 const & p 直线上一点
		 * \param Vector3 const & q 直线上另一点
		 * \param real_type & u 交点的质心坐标(交点坐标为:u * a + v * b + w * c)
		 * \param real_type & v
		 * \param real_type & w
		*/
		static bool intersects(Triangle const& triangle,Vector3 const& p, Vector3 const& q, real_type& u, real_type& v, real_type& w);

		/*!
		 * \remarks 三角形-线段
		 * \return 
		 * \param Triangle const & triangle
		 * \param LineSegment const & lineSegment (从线段的方向去看，三角形必须为逆时针环绕)
		 * \param real_type & u 交点的质心坐标(交点坐标为:u * a + v * b + w * c)
		 * \param real_type & v
		 * \param real_type & w
		*/
		static bool intersects(Triangle const& triangle, LineSegment const& lineSegment, real_type& u, real_type& v, real_type& w);

		/*!
		 * \remarks 三角形-射线
		 * \return 
		 * \param Triangle const & triangle
		 * \param Ray const & ray (从射线的方向去看，三角形必须为逆时针环绕)
		 * \param real_type & u 交点的质心坐标(交点坐标为:u * a + v * b + w * c)
		 * \param real_type & v
		 * \param real_type & w
		*/
		static bool intersects(Triangle const& triangle, Ray const& ray, real_type& u, real_type& v, real_type& w);

		/*!
		 * \remarks 三角形-三角形(参数所指定的两个三角形必须共面)
		 * \return 
		 * \param const Triangle & triangle1
		 * \param const Triangle & triangle2
		 * \param bool coplanar
		*/
		static bool intersects(const Triangle& triangle1, const Triangle& triangle2, bool coplanar);

		/*!
		 * \remarks 三角形-三角形
		 * \return 
		 * \param const Triangle & A
		 * \param const Triangle & B
		*/
		static bool intersects(Triangle const& A, Triangle const& B);

		/*!
		 * \remarks 三角形-球体
		 * \return 
		 * \param Triangle const & triangle
		 * \param const Sphere & sphere
		*/
		static bool intersects(Triangle const& triangle,const Sphere& sphere);

		/*!
		 * \remarks 三角形-AABB
		 * \return 
		 * \param const Triangle & triangle
		 * \param const AABB & box
		*/
		static bool intersects(const Triangle& triangle,const AABB& box);

		//---------------------------------------------------------------------------------------------------------------------------

		/*!
		 * \remarks 四边形-直线
		 * \return 
		 * \param Quadrilateral const & quad
		 * \param Vector3 const & p
		 * \param Vector3 const & q
		 * \param Vector3 & out
		*/
		static bool intersects(Quadrilateral const& quad,Vector3 const& p, Vector3 const& q, Vector3& out);

		//---------------------------------------------------------------------------------------------------------------------------

		/*!
		 * \remarks 球体-线段
		 * \return 
		 * \param Sphere const & sphere
		 * \param LineSegment const & lineSegment
		 * \param Vector3 & out
		*/
		static bool intersects(Sphere const& sphere,LineSegment const& lineSegment,Vector3& out);

		/*!
		 * \remarks 球体-射线
		 * \return 
		 * \param Sphere const & sphere
		 * \param Ray const & ray
		 * \param Vector3 & out
		*/
		static bool intersects(Sphere const& sphere,Ray const& ray,Vector3& out);

		/*!
		 * \remarks 球体-平面
		 * \return 
		 * \param const Sphere & s
		 * \param const Plane & p
		*/
		static bool intersects(const Sphere& s, const Plane& p);

		/*!
		 * \remarks 球体-平面负半空间(将平面的负半空间看做一个几何实体)
		 * \return 
		 * \param const Sphere & s
		 * \param const Plane & p
		 * \param bool negativeHalfSpace
		*/
		static bool intersects(const Sphere& s, const Plane& p,bool negativeHalfSpace);

		/*!
		 * \remarks 球体-三角形
		 * \return 
		 * \param const Sphere & sphere
		 * \param Triangle const & triangle
		*/
		static bool intersects(const Sphere& sphere, Triangle const& triangle);

		/*!
		 * \remarks 球体-AABB
		 * \return 
		 * \param const Sphere & sphere
		 * \param const AABB & box
		*/
		static bool intersects(const Sphere& sphere, const AABB& box);

		/*!
		 * \remarks 球体-胶囊
		 * \return 
		 * \param const Sphere & sphere
		 * \param const Capsule & capsule
		*/
		static bool intersects(const Sphere& sphere, const Capsule& capsule);

		//---------------------------------------------------------------------------------------------------------------------------

		/*!
		 * \remarks AABB-射线
		 * \return 
		 * \param const AABB & box
		 * \param const Ray & ray
		 * \param Vector3 & out
		*/
		static bool intersects(const AABB& box,const Ray& ray,Vector3& out);

		/*!
		 * \remarks AABB-射线
		 * \return 
		 * \param const AABB & box
		 * \param const Ray & ray
		 * \param Vector3 & out
		 * \param int32 & entryIndex 入口索引(0:右面,1:上面,2:前面,3:左面,4:下面,5:后面)
		 * \param int32 & exitIndex 出口索引
		*/
		static bool intersects(const AABB& box,const Ray& ray,Vector3& out,int32& entryIndex,int32& exitIndex);

		/*!
		 * \remarks AABB-平面
		 * \return 
		 * \param const AABB & box
		 * \param const Plane & plane
		*/
		static bool intersects(const AABB& box,const Plane& plane);

		/*!
		 * \remarks AABB-三角形
		 * \return 
		 * \param const AABB & box
		 * \param const Triangle & triangle
		*/
		static bool intersects(const AABB& box, const Triangle& triangle);

		/*!
		 * \remarks AABB-球体
		 * \return 
		 * \param const AABB & box
		 * \param const Sphere & sphere
		*/
		static bool intersects(const AABB& box,const Sphere& sphere);

		/*!
		 * \remarks AABB-AABB
		 * \return 
		 * \param const AABB & box1
		 * \param const AABB & box2
		*/
		static bool intersects(const AABB& box1,const AABB& box2);

		//---------------------------------------------------------------------------------------------------------------------------

		/*!
		 * \remarks 凸多面体-线段
		 * \return 
		 * \param PlaneBoundedVolume const & planeBV
		 * \param LineSegment const & lineSegment
		 * \param Vector3 & entry 入口处点
		 * \param Vector3 & exit 出口处点
		*/
		static bool intersects(PlaneBoundedVolume const& planeBV, LineSegment const& lineSegment,Vector3& entry, Vector3& exit);

		/*!
		 * \remarks 凸多面体-射线
		 * \return 
		 * \param PlaneBoundedVolume const & planeBV
		 * \param Ray const & ray
		 * \param Vector3 & entry 入口处点
		 * \param Vector3 & exit 出口处点
		*/
		static bool intersects(PlaneBoundedVolume const& planeBV, Ray const& ray, Vector3& entry, Vector3& exit);

		//---------------------------------------------------------------------------------------------------------------------------

		/*!
		 * \remarks 圆锥体-线段
		 * \return 
		 * \param Cylinder const & cylinder
		 * \param LineSegment const & lineSegment
		 * \param Vector3 & out
		*/
		static bool intersects(Cylinder const& cylinder,LineSegment const& lineSegment,Vector3& out);

		/*!
		 * \remarks 圆柱体-射线
		 * \return 
		 * \param Cylinder const & cylinder
		 * \param Ray const & ray
		 * \param Vector3 & out
		*/
		static bool intersects(Cylinder const& cylinder, Ray const& ray, Vector3& out);

		/*!
		 * \remarks 圆锥体-平面
		 * \return 
		 * \param const Cone & cone
		 * \param const Plane & plane
		*/
		static bool intersects(const Cone& cone,const Plane& plane);

		/*!
		 * \remarks 圆锥体-平面负半空间(将平面的负半空间看做一个几何实体)
		 * \return 
		 * \param const Plane & plane
		 * \param const Cone & cone
		 * \param bool negativeHalfSpace
		*/
		static bool intersects(const Plane& plane, const Cone& cone,bool negativeHalfSpace);

		//---------------------------------------------------------------------------------------------------------------------------

		/*!
		 * \remarks 胶囊-球体
		 * \return 
		 * \param const Capsule & capsule
		 * \param const Sphere & sphere
		*/
		static bool intersects(const Capsule& capsule,const Sphere& sphere);

		/*!
		 * \remarks 胶囊-胶囊
		 * \return 
		 * \param const Capsule & capsule1
		 * \param const Capsule & capsule2
		*/
		static bool intersects(const Capsule& capsule1,const Capsule& capsule2);
	};
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

#endif//_UPE_COLLISIONDETECTION_H_