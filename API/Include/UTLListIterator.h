/*!
 * \brief
 * UTL链表容器的迭代器。
 * \file UTLListIterator.h
 *
 * \author Su Yang
 *
 * \date 2016/01/28
 */
#ifndef _UNG_UTL_LISTITERATOR_H_
#define _UNG_UTL_LISTITERATOR_H_

#ifndef _UNG_UTL_CONFIG_H_
#include "UTLConfig.h"
#endif

namespace ung
{
	namespace utl
	{
		/*
		如果使用boost::bidirectional_traversal_tag的话，那么在std::distance()中的计算就会出错。
		*/
		using ListIteratorTag = std::bidirectional_iterator_tag;

		/*!
		 * \brief
		 * List迭代器。
		 * \class ListIterator
		 *
		 * \author Su Yang
		 *
		 * \date 2016/01/28
		 *
		 * \todo
		 */
		template<typename NodeType,typename ElementType>
		class ListIterator : public boost::iterator_facade<ListIterator<NodeType, ElementType>, NodeType, ListIteratorTag, ElementType&>
		{
			friend class boost::iterator_core_access;

		public:
			using value_type = NodeType;
			using reference = typename iterator_facade::reference;
			using pointer = typename iterator_facade::pointer;
			using iterator_category = ListIteratorTag;
			using difference_type = typename iterator_facade::difference_type;
			using self_type = ListIterator<value_type, ElementType>;
			using self_const_reference = self_type const&;

			/*!
			 * \remarks 默认构造函数
			 * \return  
			*/
			ListIterator() :
				mPointer(nullptr)
			{
			}

			/*!
			 * \remarks 构造函数
			 * \return 指向结点类型的指针
			 * \param pointer const & p 
			*/
			ListIterator(pointer const& p) :
				mPointer(p)
			{
			}

			/*!
			 * \remarks 拷贝构造函数
			 * \return 
			 * \param self_const_reference right 
			*/
			ListIterator(self_const_reference right) :
				mPointer(right.getPointer())
			{
			}

			/*!
			 * \remarks 拷贝赋值运算符
			 * \return void 
			 * \param self_const_reference right 
			*/
			void operator=(self_const_reference right)
			{
				if (this == &right)
				{
					return;
				}

				mPointer = right.getPointer();
			}

			/*!
			 * \remarks 获取迭代器包装的常量指针
			 * \return pointer const& 返回指向结点类型的指针
			*/
			pointer const& getPointer() const
			{
				return mPointer;
			}

			/*!
			 * \remarks 同上，非常量指针
			 * \return pointer&
			*/
			pointer& getPointer()
			{
				return mPointer;
			}

		private:
			reference dereference() const
			{
				return mPointer->mData;
				//return *mPointer;
			}

			/*
			当调用std::distance(first,last)以及调用++first的时候，会调用increment()函数。
			*/
			void increment()
			{
				/*
				下面两种写法错误。
				boost::next(mPointer, 1);
				std::advance(mPointer, 1);
				*/
				mPointer = mPointer->mNext;
			}

			bool equal(self_const_reference right) const
			{
				return mPointer == right.getPointer();
			}

			/*
			当调用--last的时候，会调用decrement()函数。
			*/
			void decrement()
			{
				mPointer = mPointer->mPrev;
			}

			//difference_type distance_to(self_const_reference right) const
			//{
			//	difference_type counter = 0;
			//	//while (boost::next(mPointer) != right.mPointer)
			//	pointer curr = mPointer;
			//	while (curr != right.mPointer)
			//	{
			//		curr = curr->mNext;
			//		++counter;
			//	}

			//	return counter;
			//}

		private:
			pointer mPointer;
		};
	}//namespace utl
}//namespace ung

#endif//_UNG_UTL_LISTITERATOR_H_
