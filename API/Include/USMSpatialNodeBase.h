/*!
 * \brief
 * 空间管理器节点基类。
 * \file USMSpatialNodeBase.h
 *
 * \author Su Yang
 *
 * \date 2016/11/25
 */
#ifndef _USM_SPATIALNODEBASE_H_
#define _USM_SPATIALNODEBASE_H_

#ifndef _USM_CONFIG_H_
#include "USMConfig.h"
#endif

#ifdef USM_HIERARCHICAL_COMPILE

#ifndef _UIF_ISPATIALNODE_H_
#include "UIFISpatialNode.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 空间管理器节点基类。
	 * \class SpatialNode
	 *
	 * \author Su Yang
	 *
	 * \date 2016/11/25
	 *
	 * \todo
	 */
	class UngExport SpatialNodeBase : public ISpatialNode
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		SpatialNodeBase() = default;

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~SpatialNodeBase() = default;

		/*!
		 * \remarks 获取父节点
		 * \return 
		*/
		virtual ISpatialNode* getParent() const override;

		/*!
		 * \remarks 获取当前节点的深度
		 * \return 
		*/
		virtual int32 getDepth() const override;

		/*!
		 * \remarks 关联一个对象到当前节点
		 * \return 
		 * \param StrongIObjectPtr objectPtr
		*/
		virtual void attachObject(StrongIObjectPtr objectPtr) override;

		/*!
		 * \remarks 将一个对象脱离当前节点
		 * \return 
		 * \param StrongIObjectPtr objectPtr
		*/
		virtual void detachObject(StrongIObjectPtr objectPtr) override;

		/*!
		 * \remarks 创建一个孩子节点
		 * \return 
		*/
		virtual ISpatialNode* createChild() override;

		/*!
		 * \remarks 创建多个孩子节点
		 * \return 
		*/
		virtual void createChildren() override;
	};
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE

#endif//_USM_SPATIALNODEBASE_H_