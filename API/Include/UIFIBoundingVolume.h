/*!
 * \brief
 * 包围体。基类。
 * \file UIFIBoundingVolume.h
 *
 * \author Su Yang
 *
 * \date 2016/11/24
 */
#ifndef _UIF_IBOUNDINGVOLUME_H_
#define _UIF_IBOUNDINGVOLUME_H_

#ifndef _UIF_CONFIG_H_
#include "UIFConfig.h"
#endif

#ifdef UIF_HIERARCHICAL_COMPILE

namespace ung
{
	class UngExport IBoundingVolume : public MemoryPool<IBoundingVolume>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		IBoundingVolume() = default;

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~IBoundingVolume() = default;
	};
}//namespace ung

#endif//UIF_HIERARCHICAL_COMPILE

#endif//_UIF_IBOUNDINGVOLUME_H_