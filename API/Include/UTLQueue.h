/*!
 * \brief
 * 队列。
 * \file UTLQueue.h
 *
 * \author Su Yang
 *
 * \date 2016/02/18
 */
#ifndef _UNG_UTL_QUEUE_H_
#define _UNG_UTL_QUEUE_H_

#ifndef _UNG_UTL_CONFIG_H_
#include "UTLConfig.h"
#endif

namespace ung
{
	namespace utl
	{
		/*!
		 * \brief
		 * 队列。
		 * \class Queue
		 *
		 * \author Su Yang
		 *
		 * \date 2016/02/18
		 *
		 * \todo
		 */
		template<typename T>
		class Queue
		{
		public:
			/*!
			 * \remarks 默认构造函数
			 * \return 
			*/
			Queue() = default;

			/*!
			 * \remarks 进入队列
			 * \return 
			 * \param T const & el
			*/
			void push(T const& el)
			{
				mCont.push_back(el);
			}

			/*!
			 * \remarks 弹出队列
			 * \return 
			*/
			void pop()
			{
				mCont.pop_front();
			}

			/*!
			 * \remarks 获取队列元素
			 * \return 
			*/
			T& front()
			{
				return mCont.front();
			}

			/*!
			 * \remarks 获取队列元素，常量对象调用
			 * \return 
			*/
			T const& front() const
			{
				return mCont.front();
			}

			///*!
			//* \remarks 获取队列中的最后一个元素
			//* \return 
			//*/
			//T& back()
			//{
			//	return mCont.back();
			//}

			///*!
			//* \remarks 获取队列中的最后一个元素，常量对象调用
			//* \return 
			//*/
			//T const& back() const
			//{
			//	return mCont.back();
			//}

			/*!
			 * \remarks 获取元素数量
			 * \return 
			*/
			usize size() const
			{
				return mCont.size();
			}

			/*!
			 * \remarks 是否为空
			 * \return 
			*/
			bool empty() const
			{
				return mCont.empty();
			}

			/*!
			 * \remarks 清空
			 * \return 
			*/
			void clear()
			{
				mCont.clear();
			}

		private:
			std::list<T> mCont;
		};
	}//namespace utl
}//namespace ung

#endif//_UNG_UTL_QUEUE_H_

/*
队列是一个简单的等待序列，在尾部加入元素时队列加长，在前端删除数据时队列缩短。
与栈不同，队列是一种使用两端的结构：一端用来加入新元素，另一端用来删除元素。因此，最后一个元素必须等到排在它之前的所有元素都删除之后才能操作。
队列是先进先出(first in first out,FIFO)的结构。
STL中，队列容器默认由deque实现，用户也可以选择list容器来实现。如果用vector容器实现会导致编译错误，因为pop()是通过调用pop_front()来实现的。
queue允许新增元素，移除元素，从最底端加入元素，取得最顶端元素。但除了最底端可以加入、最顶端可以取出外，没有任何其它方法可以存取queue的其它
元素。换言之，queue不允许有遍历行为，queue没有迭代器。
*/