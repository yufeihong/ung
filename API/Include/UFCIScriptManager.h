/*!
 * \brief
 * 脚本系统接口
 * \file UFCIScriptManager.h
 *
 * \author Su Yang
 *
 * \date 2016/12/27
 */
#ifndef _UFC_ISCRIPTMANAGER_H_
#define _UFC_ISCRIPTMANAGER_H_

#ifndef _UFC_CONFIG_H_
#include "UFCConfig.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

namespace ung
{
	/*!
	 * \brief
	 * 脚本系统接口类
	 * \class IScriptManager
	 *
	 * \author Su Yang
	 *
	 * \date 2016/12/27
	 *
	 * \todo
	 */
	class IScriptManager
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		IScriptManager() = default;

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~IScriptManager();

		/*!
		 * \remarks 初始化
		 * \return 
		*/
		virtual bool init() = 0;

		/*!
		 * \remarks 打开，运行脚本文件
		 * \return 
		 * \param const char * scriptFileFullName
		*/
		virtual void executeFile(const char* scriptFileFullName) = 0;

		/*!
		 * \remarks 解析，运行脚本字符串
		 * \return 
		 * \param const char * scriptString
		*/
		virtual void executeString(const char* scriptString) = 0;
	};
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_ISCRIPTMANAGER_H_