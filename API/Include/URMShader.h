/*!
 * \brief
 * Shader
 * \file URMShader.h
 *
 * \author Su Yang
 *
 * \date 2017/06/10
 */
#ifndef _URM_SHADER_H_
#define _URM_SHADER_H_

#ifndef _URM_CONFIG_H_
#include "URMConfig.h"
#endif

#ifdef URM_HIERARCHICAL_COMPILE

#ifndef _URM_ENUMCOLLECTION_H_
#include "URMEnumCollection.h"
#endif

namespace ung
{
	class Parser;

	/*!
	 * \brief
	 * Shader
	 * \class Shader
	 *
	 * \author Su Yang
	 *
	 * \date 2017/06/10
	 *
	 * \todo
	 */
	class UngExport Shader : public MemoryPool<Shader>
	{
	public:
		/*!
		 * \remarks 构造函数(构造时读取)
		 * \return 
		 * \param String const& fullName 全名
		 * \param ShaderType type
		*/
		Shader(String const& fullName,ShaderType type);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~Shader();

		/*!
		 * \remarks 指针容器接口
		 * \return 
		*/
		virtual Shader* clone() const PURE;

		/*!
		 * \remarks 编译shader
		 * \return 
		*/
		virtual void compile() PURE;

		/*!
		 * \remarks 获取类型
		 * \return 
		*/
		ShaderType getType() const;

		/*!
		 * \remarks 获取全名
		 * \return 
		*/
		String const& getFullName() const;

		/*!
		 * \remarks 获取shader文件名
		 * \return 
		*/
		String const& getShaderName() const;

		/*!
		 * \remarks 设置常量
		 * \return 
		 * \param const char* name
		 * \param void* pBuffer
		 * \param uint32 bytes
		*/
		virtual void setConstant(const char* name, void* pBuffer,uint32 bytes) PURE;

	protected:
		String mFullName;
		String mShaderName;
		ShaderType mType;
		std::unique_ptr<Parser> mParser;
	};
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

#endif//_URM_SHADER_H_