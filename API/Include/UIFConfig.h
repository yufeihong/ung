/*!
 * \brief
 * 内部代码包含这个头文件
 * \file UIFConfig.h
 *
 * \author Su Yang
 *
 * \date 2017/04/01
 */
#ifndef _UIF_CONFIG_H_
#define _UIF_CONFIG_H_

#ifndef _UIF_ENABLE_H_
#include "UIFEnable.h"
#endif

#ifdef UIF_HIERARCHICAL_COMPILE

 //其它库的头文件
#ifndef _UBS_H_
#include "UBS.h"
#endif

#ifndef _UTM_H_
#include "UTM.h"
#endif

#ifndef _UMM_H_
#include "UMM.h"
#endif

#ifndef _UFC_H_
#include "UFC.h"
#endif

#ifndef _UML_H_
#include "UML.h"
#endif

#ifndef _UIF_FORWARDTYPEDEFINE_H_
#include "UIFForwardTypeDefine.h"
#endif

#endif//UIF_HIERARCHICAL_COMPILE

#endif//_UIF_CONFIG_H_