/*!
 * \brief
 * 处理资源创建的工厂方法。
 * \file URMResourceFactory.h
 *
 * \author Su Yang
 *
 * \date 2016/11/18
 */
#ifndef _URM_RESOURCEFACTORY_H_
#define _URM_RESOURCEFACTORY_H_

#ifndef _URM_CONFIG_H_
#include "URMConfig.h"
#endif

#ifdef URM_HIERARCHICAL_COMPILE

namespace ung
{
	class Resource;

	/*!
	 * \brief
	 * 处理资源创建的工厂方法。
	 * \class ResourceFactory
	 *
	 * \author Su Yang
	 *
	 * \date 2016/11/18
	 *
	 * \todo
	 */
	class ResourceFactory : public Singleton<ResourceFactory>
	{
	public:
		//回调函数指针
		typedef std::shared_ptr<Resource>(*creatorType)(String const&);

		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		ResourceFactory();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~ResourceFactory();

		/*!
		 * \remarks 创建资源
		 * \return 
		 * \param String const & resourceType 具体资源的class name的字符串形式
		 * \param String const & resourceFileName 资源文件名，不包含路径
		*/
		std::shared_ptr<Resource> createResource(String const& resourceType,String const& resourceFileName);

		/*!
		 * \remarks 注册回调函数
		 * \return 
		 * \param String const & resourceType 具体资源的class name的字符串形式
		 * \param creatorType creator
		*/
		void registerCreator(String const& resourceType, creatorType creator);

		/*!
		 * \remarks 注销回调函数
		 * \return 
		 * \param String const & resourceType 具体资源的class name的字符串形式
		*/
		void unregisterCreator(String const& resourceType);

	private:
		/*!
		 * \remarks 配置包含路径的文件名
		 * \return 
		 * \param String const & resourceType
		 * \param String const & resourceFileName
		*/
		const String fixFullName(String const& resourceType,String const& resourceFileName);

	private:
		STL_UNORDERED_MAP(String, creatorType) mCreators;
	};
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

#endif//_URM_RESOURCEFACTORY_H_