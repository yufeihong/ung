/*!
 * \brief
 * 手工简单几何体，认为从外表面观察，逆时针为正面
 * \file USMManualGeometry.h
 *
 * \author Su Yang
 *
 * \date 2016/12/02
 */
#ifndef _USM_MANUALGEOMETRY_H_
#define _USM_MANUALGEOMETRY_H_

#ifndef _USM_CONFIG_H_
#include "USMConfig.h"
#endif

#ifdef USM_HIERARCHICAL_COMPILE

namespace ung
{
	/*!
	 * \brief
	 * 手工几何体的抽象接口
	 * \class ManualGeometry
	 *
	 * \author Su Yang
	 *
	 * \date 2016/12/02
	 *
	 * \todo
	 */
	class UngExport ManualGeometry : public MemoryPool<ManualGeometry>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		ManualGeometry();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~ManualGeometry();

		/*!
		 * \remarks 获取positions
		 * \return 
		*/
		virtual Vector3 const* getPositions() const = 0;

		/*!
		 * \remarks 获取索引
		 * \return 
		*/
		virtual uint32 const* getIndices() const = 0;

		/*!
		 * \remarks 获取顶点的数量
		 * \return 
		*/
		virtual uint32 getVerticesCount() const = 0;

		/*!
		 * \remarks 获取索引的数量
		 * \return 
		*/
		virtual uint32 getIndicesCount() const = 0;

		/*!
		 * \remarks 获取三角形的数量
		 * \return 
		*/
		virtual uint32 getTrianglesCount() const = 0;
	};

	/*!
	 * \brief
	 * 三角形
	 * \class ManualTriangle
	 *
	 * \author Su Yang
	 *
	 * \date 2016/12/02
	 *
	 * \todo
	 */
	class UngExport ManualTriangle : public ManualGeometry
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		ManualTriangle() = delete;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param Vector3 const & v0 按照逆时针排序的三个顶点
		 * \param Vector3 const & v1
		 * \param Vector3 const & v2
		*/
		ManualTriangle(Vector3 const& v0,Vector3 const& v1,Vector3 const& v2);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~ManualTriangle();

		/*!
		 * \remarks 获取positions
		 * \return 
		*/
		virtual Vector3 const* getPositions() const override;

		/*!
		 * \remarks 获取索引
		 * \return 
		*/
		virtual uint32 const* getIndices() const override;

		/*!
		 * \remarks 获取顶点的数量
		 * \return 
		*/
		virtual uint32 getVerticesCount() const override;

		/*!
		 * \remarks 获取索引的数量
		 * \return 
		*/
		virtual uint32 getIndicesCount() const override;

		/*!
		 * \remarks 获取三角形的数量
		 * \return 
		*/
		virtual uint32 getTrianglesCount() const override;

		//-------------------------------------------------------

		/*!
		 * \remarks 获取三角形的面法线
		 * \return 长度为1.0
		*/
		Vector3 const& getFaceNormal() const;

	private:
		std::array<Vector3, 3> mVertices;
		std::array<uint32, 3> mIndices;
		Vector3 mFaceNormal;
	};

	/*!
	 * \brief
	 * box
	 * \class ManualBox
	 *
	 * \author Su Yang
	 *
	 * \date 2016/12/02
	 *
	 * \todo
	 */
	class UngExport ManualBox : public ManualGeometry
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		ManualBox() = delete;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param Vector3 const & halfSize 度量3个方向上box的半径
		*/
		ManualBox(Vector3 const& halfSize);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~ManualBox();

		/*!
		 * \remarks 获取positions
		 * \return 
		*/
		virtual Vector3 const* getPositions() const override;

		/*!
		 * \remarks 获取索引
		 * \return 
		*/
		virtual uint32 const* getIndices() const override;

		/*!
		 * \remarks 获取顶点的数量
		 * \return 
		*/
		virtual uint32 getVerticesCount() const override;

		/*!
		 * \remarks 获取索引的数量
		 * \return 
		*/
		virtual uint32 getIndicesCount() const override;

		/*!
		 * \remarks 获取三角形的数量
		 * \return 
		*/
		virtual uint32 getTrianglesCount() const override;

	private:
		std::array<Vector3, 8> mVertices;
		std::array<uint32, 36> mIndices;
	};
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE

#endif//_USM_MANUALGEOMETRY_H_