/*!
 * \brief
 * 变换组件
 * \file USMTransformComponent.h
 *
 * \author Su Yang
 *
 * \date 2017/02/11
 */
#ifndef _USM_TRANSFORMCOMPONENT_H_
#define _USM_TRANSFORMCOMPONENT_H_

#ifndef _USM_CONFIG_H_
#include "USMConfig.h"
#endif

#ifdef USM_HIERARCHICAL_COMPILE

#ifndef _USM_OBJECTCOMPONENT_H_
#include "USMObjectComponent.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 变换组件
	 * \class TransformComponent
	 *
	 * \author Su Yang
	 *
	 * \date 2017/02/11
	 *
	 * \todo
	 */
	class UngExport TransformComponent : public ObjectComponent,public Movable
	{
	public:
		UMM_OVERRIDE_NEW_DELETE;

		BOOST_STATIC_ASSERT(boost::has_new_operator<TransformComponent>::value);

		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		TransformComponent();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~TransformComponent();

		/*!
		 * \remarks 根据xml来初始化
		 * \return 
		 * \param tinyxml2::XMLElement * pComponentElement
		*/
		virtual void init(tinyxml2::XMLElement* pComponentElement) override;

		virtual void update() override;
	};
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE

#endif//_USM_TRANSFORMCOMPONENT_H_