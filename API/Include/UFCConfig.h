/*!
 * \brief
 * 内部配置文件。
 * \file UFCConfig.h
 *
 * \author Su Yang
 *
 * \date 2016/11/22
 */
#ifndef _UFC_CONFIG_H_
#define _UFC_CONFIG_H_

#ifndef _UFC_ENABLE_H_
#include "UFCEnable.h"
#endif

#ifdef UFC_HIERARCHICAL_COMPILE

//其它库的头文件
#ifndef _UBS_H_
#include "UBS.h"
#endif

#ifndef _UTM_H_
#include "UTM.h"
#endif

#ifndef _UMM_H_
#include "UMM.h"
#endif

#define UNG_LOG_INFO								\
String(BOOST_CURRENT_FUNCTION) + "@" + String(__FILE__) + ":" + LexicalCast::intToString(__LINE__) + "_Line:" + "\n"

//使用以下三个宏
#define UNG_LOG(mes)								\
LogManager::getInstance().logRecord(UNG_LOG_INFO + mes);

#define UNG_LOG_START(mes)								\
LogManager::getInstance().logRecord(UNG_LOG_INFO + "+++\t" + mes);

#define UNG_LOG_END(mes)								\
LogManager::getInstance().logRecord(UNG_LOG_INFO + "---\t" + mes);

//常量
namespace ung
{
	//千字节
	const int KILOBYTES = 1024;
	//兆字节
	const int MEGABYTES = 1048576;
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

#endif//_UFC_CONFIG_H_