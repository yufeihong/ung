/*!
 * \brief
 * 使用工具。
 * \file UPEUtilities.h
 *
 * \author Su Yang
 *
 * \date 2016/12/04
 */
#ifndef _UPE_UTILITIES_H_
#define _UPE_UTILITIES_H_

#ifndef _UPE_CONFIG_H_
#include "UPEConfig.h"
#endif

#ifdef UPE_HIERARCHICAL_COMPILE

//转换bullet向量
#define TO_UNG_VECTOR3(btV3)							\
Vector3(btV3.getX(),btV3.getY(),btV3.getZ())

//转换UNG向量
#define TO_BT_VECTOR3(ungV3)							\
btVector3(ungV3.x,ungV3.y,ungV3.z)

namespace ung
{
	class Triangle;
	class Plane;
	class Sphere;
	class AABB;
	class Cylinder;
	class Ray;
	class PlaneBoundedVolume;

	UNG_C_LINK_BEGIN;

	//---------------------------------------------------2D---------------------------------------------------

	/*!
	 * \remarks 判断在2D空间中，给定点在有向线段的左侧，右侧，之上(在延长线上也算之上)
	 * \return 正为左侧，负为右侧，0为之上
	 * \param Vector2 const & point
	 * \param Vector2 const & line
	*/
	UngExport int32 UNG_STDCALL ungPointSideOfLine(Vector2 const& point, Vector2 const& line);

	/*!
	 * \remarks 判断在2D空间中，给定点和圆的关系
	 * \return 正在内部，负在外部，0在圆上
	 * \param Vector2 const & point
	 * \param Vector2 const & c1 圆上一点(逆时针顺序)
	 * \param Vector2 const & c2 圆上一点
	 * \param Vector2 const & c3 圆上一点
	*/
	UngExport int32 UNG_STDCALL ungPointSideOfCircle(Vector2 const& point, Vector2 const& c1, Vector2 const& c2, Vector2 const& c3);

	//---------------------------------------------------距离---------------------------------------------------

	/*!
	 * \remarks 计算点到直线的距离
	 * \return 
	 * \param Vector3 const & start 直线的起点
	 * \param Vector3 const & end 直线的终点
	 * \param Vector3 const & point
	*/
	UngExport real_type UNG_STDCALL ungPointToStraightLineDistance(Vector3 const& start,Vector3 const& end,Vector3 const& point);

	/*!
	 * \remarks 点到线段的距离的平方(不考虑线段的延长线)
	 * \return 
	 * \param Vector3 const & start
	 * \param Vector3 const & end
	 * \param Vector3 const & point
	*/
	UngExport real_type UNG_STDCALL ungPointToLineSegmentSquaredDistance(Vector3 const& start,Vector3 const& end,Vector3 const& point);

	/*!
	 * \remarks 任意点到平面的有符号距离，如果任意点位于平面的背面则距离为负，如果任意点位于平面的正面则距离为正，共面则距离为0
	 * \return 
	 * \param Plane const & plane
	 * \param Vector3 const & point
	*/
	UngExport real_type UNG_STDCALL ungSignedDistanceFromPointToPlane(Plane const& plane,Vector3 const& point);

	/*!
	 * \remarks 点到AABB的距离的平方
	 * \return 
	 * \param AABB const & box
	 * \param Vector3 const & point
	*/
	UngExport real_type UNG_STDCALL ungPointToAABBSquaredDistance(AABB const& box,Vector3 const& point);

	//---------------------------------------------------位置关系-----------------------------------------------

	/*!
	 * \remarks 检查给定的3个点是否共线
	 * \return 
	 * \param Vector3 const & p1
	 * \param Vector3 const & p2
	 * \param Vector3 const & p3
	*/
	UngExport bool UNG_STDCALL ungIsThreePointsCollinear(Vector3 const& p1,Vector3 const& p2,Vector3 const& p3);

	/*!
	 * \remarks 判断给定点是否处于给定平面的外面(即，负法线半空间)
	 * \return 
	 * \param Vector3 const & a 逆时针顺序的不共线3点，构建一个平面
	 * \param Vector3 const & b
	 * \param Vector3 const & c
	 * \param Vector3 const & point
	*/
	UngExport bool UNG_STDCALL ungIsPointOutsideOfPlane(Vector3 const& a,Vector3 const& b,Vector3 const& c,Vector3 const& point);

	/*!
	 * \remarks 判断给定点是否处于给定三角形的外面(即，负法线半空间)
	 * \return 
	 * \param Triangle const & triangle
	 * \param Vector3 const & point
	*/
	UngExport bool UNG_STDCALL ungIsPointOutsideOfTriangle(Triangle const& triangle,Vector3 const& point);

	/*!
	 * \remarks 判断给定点位于三角形之上，之下，共面(共面不一定点就在三角形内部)
	 * \return 正为下，负为上，0为共面
	 * \param Triangle const & triangle 逆时针环绕的三角形
	 * \param Vector3 const & point
	*/
	UngExport int32 UNG_STDCALL ungPointSideOfTriangle(Triangle const& triangle,Vector3 const& point);

	/*!
	 * \remarks 判断点是否在三角形内(比计算质心坐标的方法更高效)
	 * \return true:内，false:外
	 * \param Triangle const & triangle 逆时针环绕
	 * \param Vector3 const & point
	*/
	UngExport bool UNG_STDCALL ungIsPointInTriangle(Triangle const& triangle,Vector3 const& point);

	/*!
	 * \remarks 判断给定点是否位于多边形的内部。(位于内部:一定共面)
	 * \return 
	 * \param STL_VECTOR(Vector3) const & polygon 顶点逆时针环绕
	 * \param Vector3 const & point
	*/
	UngExport bool UNG_STDCALL ungIsPointInPolygon(STL_VECTOR(Vector3) const& polygon,Vector3 const& point);

	/*!
	 * \remarks 判断点是否在AABB内(或其表面上)
	 * \return 
	 * \param AABB const & box
	 * \param Vector3 const & point
	*/
	UngExport bool UNG_STDCALL ungIsPointInAABB(AABB const& box,Vector3 const& point);

	/*!
	 * \remarks 判断点是否在圆柱体内(或其表面上)
	 * \return 
	 * \param Cylinder const & cylinder
	 * \param Vector3 const & point
	*/
	UngExport bool UNG_STDCALL ungIsPointInCylinder(Cylinder const& cylinder,Vector3 const& point);

	/*!
	 * \remarks 点和圆柱体的关系
	 * \return 0:内(或表面上),1:弧面之外,2:top之外,3:bottom之外
	 * \param Cylinder const & cylinder
	 * \param Vector3 const & point
	*/
	UngExport uint32 UNG_STDCALL ungPointSideOfCylinder(Cylinder const& cylinder,Vector3 const& point);

	/*!
	 * \remarks 检查给定的两个三角形是否共面
	 * \return 
	 * \param Triangle const & triangle1
	 * \param Triangle const & triangle2
	*/
	UngExport bool UNG_STDCALL ungIsTwoTrianglescoplanar(Triangle const& triangle1, Triangle const& triangle2);

	/*!
	 * \remarks 检查smallTriangle是否完全位于bigTriangle之内
	 * \return 
	 * \param Triangle const & bigTriangle
	 * \param Triangle const & smallTriangle
	*/
	UngExport bool UNG_STDCALL ungIsTriangleContainedInTriangle(Triangle const& bigTriangle, Triangle const& smallTriangle);

	/*!
	 * \remarks 判断三角形是否完全位于平面的正半空间内或负半空间内
	 * \return > 0:正半空间,< 0:负半空间,0:其它情况
	 * \param Plane const & plane
	 * \param Triangle const & triangle
	*/
	UngExport real_type UNG_STDCALL ungIsTriangleFullyInsideHalfSpaceOfPlane(Plane const& plane,Triangle const& triangle);

	/*!
	 * \remarks 判断球体是否完全位于平面的负半空间内
	 * \return 
	 * \param Plane const & plane
	 * \param Sphere const & sphere
	*/
	UngExport bool UNG_STDCALL ungIsSphereFullyInsideNegativeHalfSpaceOfPlane(Plane const& plane,Sphere const& sphere);

	/*!
	 * \remarks 判断球体是否完全位于AABB内
	 * \return 
	 * \param Sphere const & sphere
	 * \param AABB const & box
	*/
	UngExport bool UNG_STDCALL ungIsSphereFullyInsideAABB(Sphere const& sphere,AABB const& box);

	/*!
	 * \remarks 判断AABB是否完全位于平面的负半空间内
	 * \return 
	 * \param Plane const & plane
	 * \param AABB const & box
	*/
	UngExport bool UNG_STDCALL ungIsAABBFullyInsideNegativeHalfSpaceOfPlane(Plane const& plane,AABB const& box);

	/*!
	 * \remarks 判断AABB是否完全位于平面的正半空间内
	 * \return 
	 * \param Plane const & plane
	 * \param AABB const & box
	*/
	UngExport bool UNG_STDCALL ungIsAABBFullyInsidePositiveHalfSpaceOfPlane(Plane const& plane,AABB const& box);

	/*!
	 * \remarks 判断AABB是否完全位于球体内
	 * \return 
	 * \param AABB const & box
	 * \param Sphere const & sphere
	*/
	UngExport bool UNG_STDCALL ungIsAABBFullyInsideSphere(AABB const& box, Sphere const& sphere);

	/*!
	 * \remarks 判断AABB是否完全位于凸状平面包围体内
	 * \return 
	 * \param AABB const & box
	 * \param PlaneBoundedVolume const & pbv
	*/
	UngExport bool UNG_STDCALL ungIsAABBFullyInsidePlaneBoundedVolume(AABB const& box,PlaneBoundedVolume const& pbv);

	//---------------------------------------------------最近点-------------------------------------------------

	/*!
	 * \remarks 计算参数所给定的点point在参数所给定的直线上的最近点
	 * \return 
	 * \param Vector3 const & start 虽然给定了起始和结束点，但是允许投影到其两端的延长线上
	 * \param Vector3 const & end
	 * \param Vector3 const & point
	 * \param Vector3 & out
	*/
	UngExport void UNG_STDCALL ungClosestPointOnStraightLineToPoint(Vector3 const& start,Vector3 const& end,Vector3 const& point,Vector3& out);

	/*!
	 * \remarks 计算参数所给定的点point在参数所给定的线段上的最近点(不考虑线段的延长线上的点)
	 * \return 如果point在线段上的投影点位于线段的延长线上，那么返回线段的一个接近的端点
	 * \param Vector3 const & start
	 * \param Vector3 const & end
	 * \param Vector3 const & point
	 * \param Vector3 & out
	*/
	UngExport void UNG_STDCALL ungClosestPointOnLineSegmentToPoint(Vector3 const& start,Vector3 const& end,Vector3 const& point,Vector3& out);

	/*!
	 * \remarks 计算参数所给定的点point在参数所给定的射线上的最近点
	 * \return 
	 * \param Ray const & ray
	 * \param Vector3 const & point
	 * \param Vector3 & out
	*/
	UngExport void UNG_STDCALL ungClosestPointOnRayToPoint(Ray const& ray,Vector3 const& point,Vector3& out);

	/*!
	 * \remarks 计算参数所给定的点point在参数所给定的平面上的最近点(平面上距离参数所给定的点的最近点)
	 * \return 返回参数所给定的点在平面上的正交投影
	 * \param Plane const & plane
	 * \param Vector3 const & point
	 * \param Vector3 & out
	*/
	UngExport void UNG_STDCALL ungClosestPointOnPlaneToPoint(Plane const& plane,Vector3 const& point,Vector3& out);

	/*!
	 * \remarks 计算参数所给定的点point在参数所给定的三角形上的最近点
	 * \return 
	 * \param Triangle const & triangle
	 * \param Vector3 const & point
	 * \param Vector3& out
	*/
	UngExport void UNG_STDCALL ungClosestPointOnTriangleToPoint(Triangle const& triangle,Vector3 const& point,Vector3& out);

	/*!
	 * \remarks 在AABB空间中(内，上)距离参数所给定的点最近的一点
	 * \return 
	 * \param AABB const & box
	 * \param Vector3 const & point
	 * \param Vector3 & out
	*/
	UngExport void UNG_STDCALL ungClosestPointOnAABBToPoint(AABB const& box,Vector3 const& point,Vector3& out);

	/*!
	 * \remarks 计算参数所给定的点point在参数所给定的四面体上的最近点
	 * \return 
	 * \param Vector3 const & a 四个顶点构建的四面体
	 * \param Vector3 const & b
	 * \param Vector3 const & c
	 * \param Vector3 const & d
	 * \param Vector3 const & point
	 * \param Vector3 & out
	*/
	UngExport void UNG_STDCALL ungClosestPointOnTetrahedronToPoint(Vector3 const& a,Vector3 const& b,Vector3 const& c,Vector3 const& d,Vector3 const& point,Vector3& out);

	/*!
	 * \remarks 两条直线间的最近点
	 * \return 
	 * \param Vector3 const & p1 直线1上的一点
	 * \param Vector3 const & q1 直线1上的另一点
	 * \param Vector3 const & p2 直线2上的一点
	 * \param Vector3 const & q2 直线2上的另一点
	 * \param Vector3& out1 直线1上的最近点
	 * \param Vector3& out2 直线2上的最近点
	*/
	UngExport void UNG_STDCALL ungClosestPointBetweenTwoStraightLines(Vector3 const& p1,Vector3 const& q1,Vector3 const& p2,Vector3 const& q2,Vector3& out1,Vector3& out2);

	/*!
	 * \remarks 两条线段间的最近点
	 * \return 
	 * \param Vector3 const & start1 线段1的起点
	 * \param Vector3 const & end1 线段1的终点
	 * \param Vector3 const & start2 线段2的起点
	 * \param Vector3 const & end2 线段2的终点
	 * \param Vector3 & out1 线段1上的最近点
	 * \param Vector3 & out2 线段2上的最近点
	*/
	UngExport void UNG_STDCALL ungClosestPointBetweenTwoLineSegments(Vector3 const& start1,Vector3 const& end1,Vector3 const& start2,Vector3 const& end2,Vector3& out1,Vector3& out2);

	//---------------------------------------------------质心坐标-----------------------------------------------

	/*!
	 * \remarks 计算给定点point在三角形triangle的质心坐标
	 * \return true:给定点在三角形内，false:给定点在三角形外
	 * \param Triangle const & triangle
	 * \param Vector3 const & point
	 * \param real_type & u
	 * \param real_type & v
	 * \param real_type & w
	*/
	UngExport bool UNG_STDCALL ungTriangleBarycentric(Triangle const& triangle, Vector3  const& point, real_type& u, real_type& v, real_type& w);

	/*!
	 * \remarks 计算给定点point在由a,b,c,d构成的四面体的质心坐标
	 * \return true:给定点在四面体内，false:给定点在四面体外
	 * \param Vector3 const & a
	 * \param Vector3 const & b
	 * \param Vector3 const & c
	 * \param Vector3 const & d
	 * \param Vector3 const & point
	 * \param real_type & u
	 * \param real_type & v
	 * \param real_type & w
	 * \param real_type & x
	*/
	UngExport bool UNG_STDCALL ungTetrahedronBarycentric(Vector3  const& a,Vector3  const& b,Vector3  const& c,Vector3  const& d,Vector3  const& point,real_type& u, real_type& v, real_type& w,real_type& x);

	//---------------------------------------------------体积---------------------------------------------------

	/*!
	 * \remarks 计算由给定4个顶点所构成的四面体的有符号体积
	 * \return 
	 * \param Vector3 const & v1
	 * \param Vector3 const & v2
	 * \param Vector3 const & v3
	 * \param Vector3 const & v4
	*/
	UngExport int32 UNG_STDCALL ungSignedVolumeOfTetrahedron(Vector3 const& v1,Vector3 const& v2,Vector3 const& v3,Vector3 const& v4);
	
	//---------------------------------------------------其它---------------------------------------------------

	/*!
	 * \remarks 检查参数给定的4个顶点所构成的四边形是否是一个凸面
	 * \return 
	 * \param Vector3 const & a
	 * \param Vector3 const & b
	 * \param Vector3 const & c
	 * \param Vector3 const & d
	*/
	UngExport bool UNG_STDCALL ungIsQuadConvex(Vector3 const& a,Vector3 const& b,Vector3 const& c,Vector3 const& d);

	/*!
	 * \remarks 计算两个平面的交线
	 * \return true 两个平面相交，false 两个平面平行
	 * \param Plane const & plane1
	 * \param Plane const & plane2
	 * \param Vector3 & p0 交线上的一点
	 * \param Vector3 & lineDir 交线的方向
	*/
	UngExport bool UNG_STDCALL ungIntersectionLineOfTwoPlanes(Plane const& plane1, Plane const& plane2, Vector3& p0, Vector3& lineDir);

	UNG_C_LINK_END;
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

#endif//_UPE_UTILITIES_H_