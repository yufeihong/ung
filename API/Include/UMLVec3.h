/*!
 * \brief
 * 3维向量
 * \file UMLVec3.h
 *
 * \author Su Yang
 *
 * \date 2016/06/15
 */
#ifndef _UML_VEC3_H_
#define _UML_VEC3_H_

#ifndef _UML_CONFIG_H_
#include "UMLConfig.h"
#endif

#ifdef UML_HIERARCHICAL_COMPILE

#ifndef _UML_MATH_H_
#include "UMLMath.h"
#endif

#ifndef _UML_CONSTS_H_
#include "UMLConsts.h"
#endif

#ifndef _UML_MAT3_H_
#include "UMLMat3.h"
#endif

#ifndef _UML_MAT4_H_
#include "UMLMat4.h"
#endif

#ifndef _UML_QUATER_H_
#include "UMLQuater.h"
#endif

#ifndef _UML_SQT_H_
#include "UMLSQT.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 3维向量
	 * \class Vec3
	 *
	 * \author Su Yang
	 *
	 * \date 2016/06/15
	 *
	 * \todo
	 */
	template<typename T>
	class Vec3 :public MemoryPool<Vec3<T>>
	{
		/*!
		 * \remarks 打印Vec3
		 * \return 
		 * \param std::ostream & o
		 * \param const Vec3<T> & v
		*/
		friend std::ostream& operator<<(std::ostream& o, const Vec3<T>& v)
		{
			o << "Vector3(" << v.x << "," << v.y << "," << v.z << ")";
			return o;
		}

		/*!
		 * \remarks scaler + Vec3
		 * \return 
		 * \param T const & scaler
		 * \param Vec3 const & v
		*/
		friend Vec3 operator+(T const& scaler, Vec3 const& v)
		{
			return v + scaler;
		}

		/*!
		 * \remarks scaler * Vec3
		 * \return 
		 * \param T const & scaler
		 * \param Vec3 const & v
		*/
		friend Vec3 operator*(T const& scaler, Vec3 const& v)
		{
			return v * scaler;
		}

	public:
		using element_type = T;
		using math_type = MathImpl<element_type>;

		/*
		默认构造函数
		*/
		Vec3();

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~Vec3() = default;

		/*!
		 * \remarks 委托构造函数
		 * \return 
		 * \param const T scaler
		*/
		explicit Vec3(const T& scaler);

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param const T& xx
		 * \param const T& yy
		 * \param const T& zz
		*/
		Vec3(const T& xx, const T& yy, const T& zz);

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param const T* const ar 数组地址
		 * \param const uint32 sz 数组大小
		*/
		Vec3(const T* const ar, const uint32 sz);

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param const T
		 * \param & ar
		 * \param [3]
		*/
		explicit Vec3(const T(&ar)[3]);

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param Vec2<T> const & v2
		 * \param T zz
		*/
		Vec3(Vec2<T> const& v2, T zz);

		/*!
		 * \remarks 拷贝构造函数
		 * \return 
		 * \param const Vec3 & v
		*/
		Vec3(const Vec3 &v);

		/*!
		 * \remarks 拷贝赋值运算符
		 * \return 
		 * \param const Vec3 & v
		*/
		Vec3& operator=(const Vec3& v);

		/*!
		 * \remarks 移动构造函数
		 * \return 
		 * \param Vec3 & & v
		*/
		Vec3(Vec3 &&v) noexcept;

		/*!
		 * \remarks 移动赋值运算符
		 * \return 
		 * \param Vec3 & & v
		*/
		Vec3& operator=(Vec3 &&v) noexcept;

		/*!
		 * \remarks 重载[]运算符
		 * \return 元素的引用
		 * \param const usize i
		*/
		T& operator [](const usize i);

		/*!
		 * \remarks 重载[]运算符，常量对象调用
		 * \return 元素的常量引用
		 * \param const usize i
		*/
		const T& operator [](const usize i) const;

		/*!
		 * \remarks 获取x的地址
		 * \return 
		*/
		T* getAddress();

		/*!
		 * \remarks 获取x的地址
		 * \return 
		*/
		T const* getAddress() const;

		/*!
		 * \remarks 比较当前向量是否小于给定向量
		 * \return 如果当前向量的3个分量都小于给定向量的对应分量，则返回true
		 * \param const Vec3& v
		*/
		bool operator<(const Vec3& v) const;

		/*!
		 * \remarks 比较当前向量是否大于给定向量
		 * \return 
		 * \param const Vec3 & v
		*/
		bool operator>(const Vec3& v) const;

		/*!
		 * \remarks 比较当前向量是否小于等于给定向量
		 * \return 
		 * \param const Vec3 & v
		*/
		bool operator<=(const Vec3& v) const;

		/*!
		 * \remarks 比较当前向量是否大于等于给定向量
		 * \return 
		 * \param const Vec3 & v
		*/
		bool operator>=(const Vec3& v) const;

		/*!
		 * \remarks 比较当前向量是否等于给定向量
		 * \return 
		 * \param const Vec3& v
		*/
		bool operator==(const Vec3& v) const;

		/*!
		 * \remarks 比较当前向量是否不等于给定向量
		 * \return 
		 * \param const Vec3& v
		*/
		bool operator!=(const Vec3& v) const;

		/*!
		 * \remarks Vec3 + Vec3
		 * \return 
		 * \param const Vec3 & v
		*/
		Vec3 operator+(const Vec3& v) const;

		/*!
		 * \remarks 重载+=运算符
		 * \return 
		 * \param const Vec3& v
		*/
		Vec3& operator+=(const Vec3& v);

		/*!
		 * \remarks Vec3 + scaler
		 * \return 
		 * \param const T & scaler
		*/
		Vec3 operator+(const T& scaler) const;

		/*!
		 * \remarks 重载+=运算符
		 * \return 
		 * \param const T& scaler
		*/
		Vec3& operator+=(const T& scaler);

		/*!
		 * \remarks Vec3 - Vec3
		 * \return 
		 * \param const Vec3 & v
		*/
		Vec3 operator-(const Vec3& v) const;

		/*!
		 * \remarks 重载-=运算符
		 * \return 
		 * \param const Vec3 & v
		*/
		Vec3& operator-=(const Vec3& v);

		/*!
		 * \remarks Vec3 - scaler
		 * \return 
		 * \param const T & scaler
		*/
		Vec3 operator-(const T& scaler) const;

		/*!
		 * \remarks 重载-=运算符
		 * \return 
		 * \param const T & scaler
		*/
		Vec3& operator-=(const T& scaler);

		/*!
		 * \remarks Vec3 * Vec3
		 * \return 
		 * \param const Vec3 & v
		*/
		Vec3 operator*(const Vec3& v) const;

		/*!
		 * \remarks 重载*=运算符
		 * \return 
		 * \param const Vec3& v
		*/
		Vec3& operator*=(const Vec3& v);

		/*!
		 * \remarks Vec3 * scaler
		 * \return 
		 * \param const T & scaler
		*/
		Vec3 operator*(const T& scaler) const;

		/*!
		 * \remarks 重载*=运算符
		 * \return 
		 * \param const T& scaler
		*/
		Vec3& operator*=(const T& scaler);

		/*!
		 * \remarks Vec3 * Mat3
		 * \return 
		 * \param Mat3<T> const & mat3
		*/
		Vec3 operator*(Mat3<T> const& mat3) const;

		/*!
		 * \remarks 重载*=运算符
		 * \return 
		 * \param Mat3<T> const & mat3
		*/
		Vec3& operator*=(Mat3<T> const& mat3);

		/*!
		 * \remarks Vec3 * Mat4(忽略Mat4中的平移)
		 * \return 
		 * \param Mat4<T> const & mat4
		*/
		Vec3 operator*(Mat4<T> const& mat4) const;

		/*!
		 * \remarks 重载*=运算符(忽略Mat4中的平移)
		 * \return 
		 * \param Mat4<T> const & mat4
		*/
		Vec3& operator*=(Mat4<T> const& mat4);

		/*!
		 * \remarks Vec3 * Quater
		 * \return 
		 * \param Quater<T> const & q
		*/
		Vec3 operator*(Quater<T> const& q) const;

		/*!
		 * \remarks Vec3 *= Quater
		 * \return 
		 * \param Quater<T> const & q
		*/
		Vec3& operator*=(Quater<T> const& q);

		/*!
		 * \remarks Vec3 * SQT(注意：该函数的实现中，忽略了平移)
		 * \return 
		 * \param SQTImpl<T> const & sqt
		*/
		Vec3 operator*(SQTImpl<T> const& sqt) const;

		/*!
		 * \remarks Vec3 *= SQT(注意：该函数的实现中，忽略了平移)
		 * \return 
		 * \param SQTImpl<T> const & sqt
		*/
		Vec3& operator*=(SQTImpl<T> const& sqt);

		/*!
		 * \remarks Vec3 / Vec3
		 * \return 
		 * \param const Vec3 & v
		*/
		Vec3 operator/(const Vec3& v) const;

		/*!
		 * \remarks 重载/=运算符
		 * \return 
		 * \param const Vec3 & v
		*/
		Vec3& operator/=(const Vec3& v);

		/*!
		 * \remarks Vec3 / scaler
		 * \return 
		 * \param const T & scaler
		*/
		Vec3 operator/(const T& scaler) const;

		/*!
		 * \remarks 重载/=运算符
		 * \return 
		 * \param const T & scaler
		*/
		Vec3& operator/=(const T& scaler);

		/*!
		 * \remarks 重载-运算符
		 * \return 
		*/
		Vec3 operator-() const;

		/*!
		 * \remarks 设定当前向量为0向量
		 * \return 
		*/
		void setZero();

		/*!
		 * \remarks 交换
		 * \return 
		 * \param Vec3 & other
		*/
		void swap(Vec3& other);

		/*!
		 * \remarks 计算当前向量的长度(模)
		 * \return 
		*/
		T length() const;

		/*!
		 * \remarks 计算当前向量长度的平方
		 * \return 
		*/
		T squaredLength() const;

		/*!
		 * \remarks 计算当前点与给定点之间的距离
		 * \return 
		 * \param const Vec3& p
		*/
		T distance(const Vec3& p) const;

		/*!
		 * \remarks 计算当前点与给定点之间距离的平方
		 * \return 
		 * \param const Vec3& p
		*/
		T squaredDistance(const Vec3& p) const;

		/*!
		 * \remarks 计算当前向量与给定向量的点积
		 * \return 
		 * \param const Vec3& v
		*/
		T dotProduct(const Vec3& v) const;

		/*!
		 * \remarks 计算当前向量,在向量v上面的投影向量.
		 * \return 
		 * \param const Vec3& v
		*/
		const Vec3 projectionComponent(const Vec3& v) const;

		/*!
		 * \remarks 计算当前向量垂直于给定向量的分量
		 * \return 
		 * \param const Vec3& v
		*/
		const Vec3 perpendicularComponent(const Vec3& v) const;

		/*!
		 * \remarks 归一化当前向量，修改当前向量
		 * \return 
		*/
		void normalize();

		/*!
		 * \remarks 归一化当前向量，不修改当前向量
		 * \return 
		*/
		Vec3 normalizeCopy() const;

		/*!
		 * \remarks 计算当前点与给定点的中点
		 * \return 
		 * \param const Vec3& p
		*/
		Vec3 midPoint(const Vec3& p) const;

		/*!
		 * \remarks 设置当前向量(或点)的每个分量都不超过给定向量(或点)的每个对应的分量
		 * \return 
		 * \param const Vec3& v
		*/
		void setFloor(const Vec3& v);

		/*!
		 * \remarks 设置当前向量(或点)的每个分量都超过或等于给定向量(或点)的每个对应的分量
		 * \return
		 * \param const Vec3& v
		*/
		void setCeil(const Vec3& v);

		/*!
		 * \remarks 获取一个与当前向量垂直的向量
		 * \return 单位长度向量
		*/
		Vec3 perpendicular() const;

		/*!
		 * \remarks 计算当前向量与给定向量的叉积
		 * \return 没有对返回结果进行规范化操作
		 * \param const Vec3& v
		*/
		Vec3 crossProduct(const Vec3& v) const;

		/*!
		 * \remarks 计算当前向量与给定向量的叉积，叉积结果为单位向量
		 * \return 单位长度向量
		 * \param const Vec3& v
		*/
		Vec3 crossProductUnit(const Vec3& v) const;

		/*!
		 * \remarks 通过给定法线，计算当前向量的反射向量。如果是用来计算入射光线的反射向量的话，为了确保反射光线
		 * 的方向是从“地面”出发去指向“天空”，那么入射光线的向量，其方向必须为从“地面”出发而指向“天空”。
		 * \return 返回的反射向量是单位向量
		 * \param const Vec3& normal
		*/
		Vec3 reflect(const Vec3& normal) const;

		/*!
		 * \remarks 判断当前向量是否和给定向量平行
		 * \return 
		 * \param const Vec3& v
		*/
		bool isParallel(const Vec3& v) const;

		/*!
		 * \remarks 计算当前向量与给定向量之间的夹角(弧度)
		 * \return 
		 * \param const Vec3& v
		*/
		Radian angleBetween(const Vec3& v) const;

		/*!
		 * \remarks 判断当前向量的长度是否为0
		 * \return 
		*/
		bool isZeroLength() const;

		/*!
		 * \remarks 判断当前向量的长度是否为1
		 * \return 
		*/
		bool isUnitLength() const;

		/*!
		 * \remarks 获取当前向量主要受x,y,z哪个轴影响最多。比如向量(3.1,-4.8,2.9)其主要受-y轴(0.0,-1.0,0.0)影响最多
		 * \return 
		*/
		Vec3 primaryAxis() const;

		/*!
		 * \remarks 线性插值
		 * \return 
		 * \param Vec3 const & start
		 * \param Vec3 const & end
		 * \param T const & t
		*/
		static Vec3 lerp(Vec3 const& start, Vec3 const& end, T const& t);

	public:
		T x;
		T y;
		T z;

		static const Vec3 ZERO;
		static const Vec3 UNIT_X;
		static const Vec3 UNIT_Y;
		static const Vec3 UNIT_Z;
		static const Vec3 NEGATIVE_UNIT_X;
		static const Vec3 NEGATIVE_UNIT_Y;
		static const Vec3 NEGATIVE_UNIT_Z;
		static const Vec3 UNIT_SCALE;
	};
}//namespace ung

#ifndef _UML_VEC3DEF_H_
#include "UMLVec3Def.h"
#endif

#endif//UML_HIERARCHICAL_COMPILE

#endif//_UML_VEC3_H_