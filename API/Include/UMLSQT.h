/*!
 * \brief
 * SQT
 * \file UMLSQT.h
 *
 * \author Su Yang
 *
 * \date 2017/03/20
 */
#ifndef _UML_SQT_H_
#define _UML_SQT_H_

#ifndef _UML_CONFIG_H_
#include "UMLConfig.h"
#endif

#ifdef UML_HIERARCHICAL_COMPILE

#ifndef _UML_MATH_H_
#include "UMLMath.h"
#endif

#ifndef _UML_VEC3_H_
#include "UMLVec3.h"
#endif

#ifndef _UML_QUATER_H_
#include "UMLQuater.h"
#endif

#ifndef _UML_MAT3_H_
#include "UMLMat3.h"
#endif

#ifndef _UML_MAT4_H_
#include "UMLMat4.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * SQT
	 * \class SQTImpl
	 *
	 * \author Su Yang
	 *
	 * \date 2017/03/20
	 *
	 * \todo
	 */
	template<typename T>
	class SQTImpl : public MemoryPool<SQTImpl<T>>
	{
	public:
		using element_type = T;
		using math_type = MathImpl<element_type>;

		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		SQTImpl() :
			mScale(1.0)
		{
		}

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~SQTImpl() = default;

		/*!
		 * \remarks 拷贝构造函数
		 * \return 
		 * \param SQTImpl const & other
		*/
		SQTImpl(SQTImpl const& other) :
			mRotate(other.mRotate),
			mTranslate(other.mTranslate),
			mScale(other.mScale)
		{
		}

		/*!
		 * \remarks 拷贝赋值运算符
		 * \return 
		 * \param SQTImpl const & other
		*/
		SQTImpl& operator=(SQTImpl const& other)
		{
			if (this != &other)
			{
				mRotate = other.mRotate;
				mTranslate = other.mTranslate;
				mScale = other.mScale;
			}

			return *this;
		}

		/*!
		 * \remarks 移动构造函数
		 * \return 
		 * \param SQTImpl & & other
		*/
		SQTImpl(SQTImpl&& other) noexcept :
			mRotate(other.mRotate),
			mTranslate(other.mTranslate),
			mScale(other.mScale)
		{
		}

		/*!
		 * \remarks 移动赋值运算符
		 * \return 
		 * \param SQTImpl & & other
		*/
		SQTImpl& operator=(SQTImpl&& other) noexcept
		{
			if (this != &other)
			{
				mRotate = other.mRotate;
				mTranslate = other.mTranslate;
				mScale = other.mScale;
			}

			return *this;
		}

		/*!
		 * \remarks 设置为单位SQT
		 * \return 
		*/
		void setIdentity()
		{
			setScale(1.0);
			setTranslate(Vec3<T>::ZERO);
			setRotate(Quater<T>::IDENTITY);
		}

		/*!
		 * \remarks 判断当前SQT是否为单位SQT
		 * \return 
		*/
		bool isIdentity() const
		{
			return (math_type::isUnit(mScale) && 
				mTranslate == Vec3<T>::ZERO && 
				mRotate.isIdentity());
		}

		/*!
		 * \remarks 获取旋转
		 * \return 引用
		*/
		Quater<T>& getRotate()
		{
			return mRotate;
		}

		/*!
		 * \remarks 获取旋转
		 * \return 常量引用
		*/
		Quater<T> const& getRotate() const
		{
			return mRotate;
		}

		/*!
		 * \remarks 获取平移
		 * \return 引用
		*/
		Vec3<T>& getTranslate()
		{
			return mTranslate;
		}

		/*!
		 * \remarks 获取平移
		 * \return 常量引用
		*/
		Vec3<T> const& getTranslate() const
		{
			return mTranslate;
		}

		/*!
		 * \remarks 获取缩放
		 * \return 引用
		*/
		T& getScale()
		{
			return mScale;
		}

		/*!
		 * \remarks 获取缩放
		 * \return 常量引用
		*/
		T const& getScale() const
		{
			return mScale;
		}

		/*!
		 * \remarks 设置旋转
		 * \return 
		 * \param Quater<T> const & q 单位四元数
		*/
		void setRotate(Quater<T> const& q)
		{
			BOOST_ASSERT(q.isUnit());

			mRotate = q;
		}

		/*!
		 * \remarks 设置旋转
		 * \return 
		 * \param const Mat3<T> & rot 旋转矩阵
		*/
		void setRotate(const Mat3<T>& rot)
		{
			mRotate.fromRotationMatrix(rot);
		}

		/*!
		 * \remarks 设置旋转
		 * \return 
		 * \param const Vec3<T> & axis 单位长度轴
		 * \param const Radian& r 弧度
		*/
		void setRotate(const Vec3<T>& axis, const Radian& r)
		{
			mRotate.fromAxisAngle(axis,r);
		}

		/*!
		 * \remarks 设置旋转
		 * \return 
		 * \param const Vec3<T> & axis 单位长度轴
		 * \param const Degree& d 度
		*/
		void setRotate(const Vec3<T>& axis, const Degree& d)
		{
			mRotate.fromAxisAngle(axis, d);
		}

		/*!
		 * \remarks 累计旋转
		 * \return 
		 * \param Quater<T> const & q
		*/
		void addUpRotate(Quater<T> const& q)
		{
			mRotate *= q;
		}

		/*!
		 * \remarks 累计旋转
		 * \return 
		 * \param Mat3<T> const & rot
		*/
		void addUpRotate(Mat3<T> const& rot)
		{
			Quater<T> temp{};
			temp.fromRotationMatrix(rot);

			mRotate *= temp;
		}

		/*!
		 * \remarks 累计旋转
		 * \return 
		 * \param const Vec3<T> & axis
		 * \param const Radian& r 弧度
		*/
		void addUpRotate(const Vec3<T>& axis, const Radian& r)
		{
			Quater<T> temp{};
			temp.fromAxisAngle(axis,r);

			mRotate *= temp;
		}

		/*!
		 * \remarks 累计旋转
		 * \return 
		 * \param const Vec3<T> & axis
		 * \param const Degree& d 度
		*/
		void addUpRotate(const Vec3<T>& axis, const Degree& d)
		{
			Quater<T> temp{};
			temp.fromAxisAngle(axis,d);

			mRotate *= temp;
		}

		/*!
		 * \remarks 设置平移
		 * \return 
		 * \param T x
		 * \param T y
		 * \param T z
		*/
		void setTranslate(T x, T y, T z)
		{
			mTranslate.x = x;
			mTranslate.y = y;
			mTranslate.z = z;
		}

		/*!
		 * \remarks 设置平移
		 * \return 
		 * \param Vec3<T> const & v3
		*/
		void setTranslate(Vec3<T> const& v3)
		{
			mTranslate = v3;
		}

		/*!
		 * \remarks 累计平移
		 * \return 
		 * \param T x
		 * \param T y
		 * \param T z
		*/
		void addUpTranslate(T x, T y, T z)
		{
			mTranslate.x += x;
			mTranslate.y += y;
			mTranslate.z += z;
		}

		/*!
		 * \remarks 累计平移
		 * \return 
		 * \param Vec3<T> const & v3
		*/
		void addUpTranslate(Vec3<T> const& v3)
		{
			mTranslate += v3;
		}

		/*!
		 * \remarks 设置统一缩放
		 * \return 
		 * \param T const & s
		*/
		void setScale(T const& s)
		{
			mScale = s;
		}

		/*!
		 * \remarks 累计统一缩放
		 * \return 
		 * \param T const & s
		*/
		void addUpScale(T const& s)
		{
			mScale *= s;
		}

		/*!
		 * \remarks 返回当前SQT所表达的变换的逆变换
		 * \return 
		*/
		SQT inverse() const
		{
			SQT inv;
			inv.mTranslate = mTranslate * -1.0;
			inv.mScale = 1.0 / mScale;
			inv.mRotate = mRotate.inverse();

			return inv;
		}

		/*!
		 * \remarks 重载==
		 * \return 
		 * \param SQTImpl const & other
		*/
		bool operator==(SQTImpl const& other) const
		{
			return (math_type::isEqual(mScale, other.mScale) &&
				mTranslate == other.mTranslate && 
				mRotate == other.mRotate);
		}

		/*!
		 * \remarks 重载!=
		 * \return 
		 * \param SQTImpl const & other
		*/
		bool operator!=(SQTImpl const& other) const
		{
			return !(operator==(other));
		}

		/*!
		 * \remarks 重载*运算符(连接两个SQTImpl)
		 * \return 
		 * \param SQTImpl const & other
		*/
		SQTImpl operator*(SQTImpl const& other) const
		{
			SQTImpl outSQT{};

			//缩放
			outSQT.mScale = mScale * other.mScale;

			//平移
			outSQT.mTranslate = mTranslate + other.mTranslate;

			//旋转
			outSQT.mRotate = mRotate * other.mRotate;

			return std::move(outSQT);
		}

		/*!
		 * \remarks 重载*=运算符
		 * \return 
		 * \param SQTImpl const & other
		*/
		SQTImpl& operator*=(SQTImpl const& other)
		{
			//缩放
			mScale *= other.mScale;

			//平移
			mTranslate += other.mTranslate;

			//旋转
			mRotate *= other.mRotate;

			return *this;
		}

		/*!
		 * \remarks SQT到仿射矩阵
		 * \return 
		 * \param Mat4<T> & outMat
		*/
		void toAffineMatrix(Mat4<T>& outMat) const
		{
			//旋转矩阵和缩放矩阵(仅统一缩放矩阵)进行相乘时，二者前后顺序无所谓。
			Mat3<T> rotMat{}, scaleMat{};
			rotMat.setRotateFromQuaternion(mRotate);
			scaleMat.setUniformScale(mScale);

			/*
			这里如果不构建一个缩放矩阵，而是给旋转矩阵的对角线分别乘以缩放，那么这种做法
			是错误的。
			*/

			outMat = rotMat * scaleMat;

			//Mat3<T>::operator[]，返回的是副本，不是引用
			outMat.m[3][0] = mTranslate.x;
			outMat.m[3][1] = mTranslate.y;
			outMat.m[3][2] = mTranslate.z;
		}

		/*!
		 * \remarks 对缩放和平移进行线性插值，对旋转进行球面线性插值
		 * \return 
		 * \param SQTImpl const & start
		 * \param SQTImpl const & end
		 * \param T const & t
		 * \param bool shortestPath
		*/
		static SQTImpl slerp(SQTImpl const& start, SQTImpl const& end, T const& t, bool shortestPath = false)
		{
			//缩放
			T const& sStart = start.getScale();
			T const& sEnd = end.getScale();
			T scale = (1.0 - t) * sStart + t * sEnd;

			//平移
			Vec3<T> const& tStart = start.getTranslate();
			Vec3<T> const& tEnd = end.getTranslate();
			Vec3<T> translate = Vec3<T>::lerp(tStart, tEnd, t);

			//旋转
			Quater<T> const& qStart = start.getRotate();
			Quater<T> const& qEnd = end.getRotate();
			Quater<T> quater = Quater<T>::slerp(qStart,qEnd,t,shortestPath);

			SQTImpl<T> sqt;
			sqt.setScale(scale);
			sqt.setRotate(quater);
			sqt.setTranslate(translate);

			return std::move(sqt);
		}

	private:
		Quater<T> mRotate;
		Vec3<T> mTranslate;
		T mScale;
	};
}//namespace ung

#endif//UML_HIERARCHICAL_COMPILE

#endif//_UML_SQT_H_