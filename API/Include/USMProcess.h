/*!
 * \brief
 * 过程基类
 * \file USMProcess.h
 *
 * \author Su Yang
 *
 * \date 2016/12/14
 */
#ifndef _USM_PROCESS_H_
#define _USM_PROCESS_H_

#ifndef _USM_CONFIG_H_
#include "USMConfig.h"
#endif

#ifdef USM_HIERARCHICAL_COMPILE

namespace ung
{
	/*!
	 * \brief
	 * 过程基类
	 * \class Process
	 *
	 * \author Su Yang
	 *
	 * \date 2016/12/14
	 *
	 * \todo
	 */
	class UngExport Process : public MemoryPool<Process>
	{
		friend class ProcessManager;

	public:
		using StrongProcessPtr = std::shared_ptr<Process>;
		using WeakProcessPtr = std::weak_ptr<Process>;

	public:
		enum State
		{
			INITIALIZED,			//初始状态

			RUNNING,				//正在运行状态
			PAUSED,				//暂停状态

			SUCCEEDED,			//正常结束状态
			ABORTED				//放弃状态
		};

	public:
		/*!
		 * \remarks 构造函数
		 * \return 
		*/
		Process();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~Process();

		/*!
		 * \remarks 暂停过程，当前的状态必须为RUNNING，设置状态为PAUSED
		 * \return 
		*/
		void pause();

		/*!
		 * \remarks 恢复过程，当前的状态必须为PAUSED，设置状态为RUNNING
		 * \return 
		*/
		void unpause();

		/*!
		 * \remarks 正常结束过程，当前的状态必须为RUNNING或PAUSED，设置状态为END
		 * \return 
		*/
		void succeed();

		/*!
		 * \remarks 当前是否处于暂停状态
		 * \return 
		*/
		bool isPaused() const;

		/*!
		 * \remarks 当前是否处于“运行”或“暂停”状态
		 * \return 
		*/
		bool isAlive() const;

		/*!
		 * \remarks 当前是否处于“结束”或“放弃”状态
		 * \return 
		*/
		bool isDead() const;

		/*!
		 * \remarks 添加孩子过程
		 * \return 
		 * \param StrongProcessPtr pProcess
		*/
		void attachChild(StrongProcessPtr pProcess);

		/*!
		 * \remarks 移除孩子过程
		 * \return 返回被移除的孩子过程
		*/
		StrongProcessPtr removeChild();

		/*!
		 * \remarks 获取孩子过程
		 * \return 
		*/
		StrongProcessPtr getChild();

		/*!
		 * \remarks 获取当前的状态
		 * \return 
		*/
		State getState() const;

	protected:
		/*!
		 * \remarks 初始化时调用。子类如果重写了这个方法，那么也必须调用这个方法，以便将状态设置为RUNNING
		 * \return 
		*/
		virtual void onInit();

		/*!
		 * \remarks 退出函数，正常结束时调用
		 * \return 
		*/
		virtual void onSuccess();

		/*!
		 * \remarks 退出函数，放弃时调用
		 * \return 
		*/
		virtual void onAbort();

		/*!
		 * \remarks 更新时调用，这个函数定义了“过程”真正的工作
		 * \return 
		 * \param ufast deltaMS
		*/
		virtual void onUpdate(ufast deltaMS) = 0;

	private:
		/*!
		 * \remarks 设置状态
		 * \return 
		 * \param State newState
		*/
		void setState(State newState);

	private:
		State mState;
		/*
		The child is a suspended(延缓的) process that’s attached to this process.If this process 
		completes successfully, the ProcessManager will attach the child and process it in the
		next frame.
		This is a very simple yet powerful technique, allowing you to create chains of processes(过程链).
		
		If a process is successful and it has a child process attached, that child will be promoted 
		into the ProcessManager’s list. It will get initialized and run the next frame.
		If a process is aborted, the child will not get promoted.
		*/
		StrongProcessPtr mChild;
	};
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE

#endif//_USM_PROCESS_H_