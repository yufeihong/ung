/*!
 * \brief
 * shadow buffer
 * \file URMShadowBuffer.h
 *
 * \author Su Yang
 *
 * \date 2017/05/30
 */
#ifndef _URM_SHADOWBUFFER_H_
#define _URM_SHADOWBUFFER_H_

#ifndef _URM_CONFIG_H_
#include "URMConfig.h"
#endif

#ifdef URM_HIERARCHICAL_COMPILE

#ifndef _URM_VIDEOBUFFER_H_
#include "URMVideoBuffer.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * shadow vertex buffer
	 * \class ShadowVertexBuffer
	 *
	 * \author Su Yang
	 *
	 * \date 2017/05/30
	 *
	 * \todo
	 */
	class UngExport ShadowVertexBuffer : public VideoBuffer
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		ShadowVertexBuffer() = delete;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param uint32 vertexSize
		 * \param uint32 numVertices
		*/
		ShadowVertexBuffer(uint32 vertexSize, uint32 numVertices);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~ShadowVertexBuffer();

		/*!
		 * \remarks Lock the entire buffer for reading / writing
		 * \return 
		 * \param LockOptions options
		*/
		virtual void* lock(LockOptions options) override;

		/*!
		 * \remarks Lock the buffer for reading / writing
		 * \return 
		 * \param uint32 offset
		 * \param uint32 length 要lock的区域的size，单位：字节
		 * \param LockOptions options
		*/
		virtual void* lock(uint32 offset, uint32 length, LockOptions options) override;

		/*!
		 * \remarks Releases the lock on this buffer
		 * 当缓存处于locked的状态，这个时候去切换video mode，会抛出异常，必须重新upload数据，如果
		 * 要100%确保数据不会lost，那么就使用readData和writeData。
		 * \return 
		*/
		virtual void unlock() override;

		/*!
		 * \remarks Reads data from the buffer and places it in the memory pointed to by pDest
		 * \return 
		 * \param uint32 offset
		 * \param uint32 length
		 * \param void* pDest
		*/
		virtual void readData(uint32 offset,uint32 length,void* pDest) override;

		/*!
		 * \remarks Writes data to the buffer from an area of system memory
		 * \return 
		 * \param uint32 offset The byte offset from the start of the buffer to start writing
		 * \param uint32 length
		 * \param const void* pSource
		 * \param bool discardWholeBuffer 建议为true，这样就允许驱动在写入数据的时候丢弃整个缓存，就可以避免DMA stalls
		*/
		virtual void writeData(uint32 offset,uint32 length,const void* pSource,bool discardWholeBuffer = true) override;

	protected:
		virtual void* lockImpl(uint32 offset, uint32 length, LockOptions options) override;

		virtual void unlockImpl() override;

	protected:
		uint8* mData;
	};//end class ShadowVertexBuffer

	//-------------------------------------------------------------------------------------------------

	/*!
	 * \brief
	 * shadow index buffer
	 * \class ShadowIndexBuffer
	 *
	 * \author Su Yang
	 *
	 * \date 2017/05/31
	 *
	 * \todo
	 */
	class UngExport ShadowIndexBuffer : public VideoBuffer
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		ShadowIndexBuffer() = delete;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param uint32 indexSize
		 * \param uint32 numIndices
		*/
		ShadowIndexBuffer(uint32 indexSize, uint32 numIndices);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~ShadowIndexBuffer();

		/*!
		 * \remarks Lock the entire buffer for reading / writing
		 * \return 
		 * \param LockOptions options
		*/
		virtual void* lock(LockOptions options) override;

		/*!
		 * \remarks Lock the buffer for reading / writing
		 * \return 
		 * \param uint32 offset
		 * \param uint32 length 要lock的区域的size，单位：字节
		 * \param LockOptions options
		*/
		virtual void* lock(uint32 offset, uint32 length, LockOptions options) override;

		/*!
		 * \remarks Releases the lock on this buffer
		 * 当缓存处于locked的状态，这个时候去切换video mode，会抛出异常，必须重新upload数据，如果
		 * 要100%确保数据不会lost，那么就使用readData和writeData。
		 * \return 
		*/
		virtual void unlock() override;

		/*!
		 * \remarks Reads data from the buffer and places it in the memory pointed to by pDest
		 * \return 
		 * \param uint32 offset
		 * \param uint32 length
		 * \param void* pDest
		*/
		virtual void readData(uint32 offset,uint32 length,void* pDest) override;

		/*!
		 * \remarks Writes data to the buffer from an area of system memory
		 * \return 
		 * \param uint32 offset The byte offset from the start of the buffer to start writing
		 * \param uint32 length
		 * \param const void* pSource
		 * \param bool discardWholeBuffer 建议为true，这样就允许驱动在写入数据的时候丢弃整个缓存，就可以避免DMA stalls
		*/
		virtual void writeData(uint32 offset,uint32 length,const void* pSource,bool discardWholeBuffer = true) override;

	protected:
		virtual void* lockImpl(uint32 offset, uint32 length, LockOptions options) override;

		virtual void unlockImpl() override;

	protected:
		uint8* mData;
	};//end class ShadowIndexBuffer
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

#endif//_URM_SHADOWBUFFER_H_