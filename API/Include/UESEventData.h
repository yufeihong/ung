/*!
 * \brief
 * 消息数据
 * \file UESEventData.h
 *
 * \author Su Yang
 *
 * \date 2016/12/25
 */
#ifndef _UES_EVENTDATA_H_
#define _UES_EVENTDATA_H_

#ifndef _UES_CONFIG_H_
#include "UESConfig.h"
#endif

#ifdef UES_HIERARCHICAL_COMPILE

#ifndef _UES_IEVENTDATA_H_
#include "UESIEventData.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 消息数据
	 * \class EventData
	 *
	 * \author Su Yang
	 *
	 * \date 2016/12/25
	 *
	 * \todo
	 */
	class EventData : public IEventData
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		EventData();

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param real_type timeStamp
		*/
		explicit EventData(real_type timeStamp);

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~EventData();

		/*!
		 * \remarks 获取时间戳
		 * \return 
		*/
		virtual real_type getTimeStamp() const override;

	private:
		real_type mTimeStamp;
	};
}//namespace ung

#endif//UES_HIERARCHICAL_COMPILE

#endif//_UES_EVENTDATA_H_