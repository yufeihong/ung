/*!
 * \brief
 * 3x3矩阵
 * \file UMLMat3.h
 *
 * \author Su Yang
 *
 * \date 2016/06/15
 */
#ifndef _UML_MAT3_H_
#define _UML_MAT3_H_

#ifndef _UML_CONFIG_H_
#include "UMLConfig.h"
#endif

#ifdef UML_HIERARCHICAL_COMPILE

namespace ung
{
	template<typename T>
	class Vec3;

	template<typename T>
	class Quater;

	/*!
	 * \brief
	 * 3x3矩阵
	 * \class Mat3
	 *
	 * \author Su Yang
	 *
	 * \date 2016/06/15
	 *
	 * \todo
	 */
	template<typename T>
	class Mat3 : public MemoryPool<Mat3<T>>
	{
	public:
		using element_type = T;
		using math_type = MathImpl<element_type>;

		/*!
		 * \remarks 打印Mat3
		 * \return 
		 * \param std::ostream & o
		 * \param const Mat3 & mat
		*/
		friend std::ostream& operator<<(std::ostream& o, const Mat3& mat)
		{
			o << "Mat3(" << mat.m[0][0] << "," << mat.m[0][1] << "," << mat.m[0][2] << ","
				<< mat.m[1][0] << "," << mat.m[1][1] << "," << mat.m[1][2] << ","
				<< mat.m[2][0] << "," << mat.m[2][1] << "," << mat.m[2][2] << ")";

			return o;
		}

		/*!
		 * \remarks scalar * Mat3
		 * \return 
		 * \param T scalar
		 * \param Mat3 const &
		*/
		friend Mat3 operator*(T scalar, Mat3 const& mat3)
		{
			return mat3 * scalar;
		}

	public:
		/*
		默认构造函数(单位矩阵)
		*/
		Mat3();

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~Mat3() = default;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param T m00
		 * \param T m01
		 * \param T m02
		 * \param T m10
		 * \param T m11
		 * \param T m12
		 * \param T m20
		 * \param T m21
		 * \param T m22
		*/
		Mat3(T m00, T m01, T m02,
					T m10, T m11, T m12,
					T m20, T m21, T m22);
		
		/*
		等价定义:
		Mat3(const T ar[3][3], const uint32 rowSize)
		Mat3(const T ar[][3], const uint32 rowSize)
		*/
		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param const T
		 * \param * const ar
		 * \param [3]
		 * \param const uint32 rowSize
		*/
		Mat3(const T(*const ar)[3], const uint32 rowSize);

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param const T * const ar
		 * \param const uint32 sz
		*/
		Mat3(const T *const ar, const uint32 sz);

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param const T
		 * \param & ar
		 * \param [9]
		*/
		explicit Mat3(const T(&ar)[9]);

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param const T
		 * \param & ar
		 * \param [3][3]
		*/
		explicit Mat3(const T(&ar)[3][3]);

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param Vec3<T> const & row1
		 * \param Vec3<T> const & row2
		 * \param Vec3<T> const & row3
		*/
		Mat3(Vec3<T> const& row1,Vec3<T> const& row2,Vec3<T> const& row3);

		/*!
		 * \remarks 拷贝构造函数
		 * \return 
		 * \param const Mat3 & other
		*/
		Mat3(const Mat3& other);

		/*!
		 * \remarks 拷贝赋值运算符
		 * \return 
		 * \param const Mat3 & other
		*/
		Mat3& operator=(const Mat3& other);

		/*!
		 * \remarks 移动构造函数
		 * \return 
		 * \param Mat3 & & other
		*/
		Mat3(Mat3&& other) noexcept;

		/*!
		 * \remarks 移动赋值运算符(不会修改被赋值对象的内存地址)
		 * \return 
		 * \param Mat3 & & other
		*/
		Mat3& operator=(Mat3&& other) noexcept;

		/*!
		 * \remarks []运算符
		 * \return 返回的是副本，不是引用
		 * \param usize iRow
		*/
		const Vec3<T> operator[](usize iRow) const;

		/*!
		 * \remarks []运算符
		 * \return 返回的是副本，不是引用
		 * \param usize iRow
		*/
		Vec3<T> operator[](usize iRow);

		/*!
		 * \remarks 重载()
		 * \return 拷贝
		 * \param usize row
		 * \param usize col
		*/
		T operator()(usize row, usize col) const;

		/*!
		 * \remarks 重载()
		 * \return 引用
		 * \param usize row
		 * \param usize col
		*/
		T& operator()(usize row, usize col);

		/*!
		 * \remarks 获取参数所指定的元素
		 * \return 引用(注意：auto m00 = m3.getElement(0, 0);auto推断出来的m00的类型是T，而不是T&，必须写成auto&)
		 * \param usize r
		 * \param usize c
		*/
		T& getElement(usize r, usize c);

		/*!
		 * \remarks 获取参数所指定的元素
		 * \return 常量引用
		 * \param usize r
		 * \param usize c
		*/
		T const& getElement(usize r, usize c) const;

		/*!
		 * \remarks 获取00的地址
		 * \return 
		*/
		T* getAddress();

		/*!
		 * \remarks 获取00的地址
		 * \return 
		*/
		T const* getAddress() const;

		/*!
		 * \remarks 获取缩放部分
		 * \return 
		*/
		Vec3<T> getScale() const;

		/*!
		 * \remarks 判断当前矩阵是否等于给定矩阵
		 * \return 
		 * \param const Mat3 & other
		*/
		bool operator==(const Mat3& other) const;

		/*!
		 * \remarks 判断当前矩阵是否不等于给定矩阵
		 * \return 
		 * \param const Mat3 & other
		*/
		bool operator!=(const Mat3& other) const;

		/*!
		 * \remarks Mat3 * scalar
		 * \return 
		 * \param T scalar
		*/
		Mat3 operator*(T scalar) const;

		/*!
		 * \remarks 当前矩阵*=给定标量
		 * \return 
		 * \param T scalar
		*/
		Mat3& operator*=(T scalar);

		/*!
		 * \remarks Mat3 * Mat3
		 * \return 
		 * \param Mat3 const & other
		*/
		Mat3 operator*(Mat3 const& other) const;

		/*!
		 * \remarks 当前矩阵*=给定矩阵
		 * \return 
		 * \param Mat3 const & other
		*/
		Mat3& operator*=(Mat3 const& other);

		/*!
		 * \remarks Mat3 / scalar
		 * \return 
		 * \param T scalar
		*/
		Mat3 operator/(T scalar) const;

		/*!
		 * \remarks 当前矩阵/=给定标量
		 * \return 
		 * \param T scalar
		*/
		Mat3& operator/=(T scalar);

		/*!
		 * \remarks 如果所有非对角线元素都为0,那么称这种矩阵为对角矩阵.单位矩阵是一种特殊的对角矩阵.
		 * \return 
		*/
		void identity();

		/*!
		 * \remarks 计算行列式.在任意方阵中都存在一个标量,称作该方阵的行列式.如果矩阵行列式为0,那么该矩阵包含投影.如果为负,那么该矩阵包含镜像.
		 * \return 
		*/
		T determinant() const;

		/*!
		 * \remarks 计算转置矩阵(不改变当前矩阵)
		 * \return 
		 * \param Mat3& outMat
		*/
		void transpose(Mat3& outMat) const;

		/*!
		 * \remarks 转置，改变了当前矩阵
		 * \return 
		*/
		void transpose();

		/*!
		 * \remarks 计算逆矩阵.矩阵转置的逆等于它的逆的转置.矩阵乘积的逆等于矩阵的逆的相反顺序的乘积.
		 * \return 
		 * \param Mat3 & invMat
		*/
		bool inverse(Mat3& invMat) const;

		/*!
		 * \remarks 矩阵正交化
		 * 有时可能会遇到略微违反了正交性的矩阵.例如,浮点数运算的累计错误.这种情况下需要做矩阵正交化,来得到一个正交矩阵.
		 * 正交化后的矩阵乘以其转置矩阵，结果为单位矩阵(它的转置矩阵等于它的逆矩阵)。
		 * 正交矩阵的行列式为1或-1.
		 * \return 
		*/
		void orthogonalization();

		/*!
		 * \remarks 获取给定行
		 * \return 返回的是拷贝
		 * \param const usize iRow
		*/
		Vec3<T> getRow(const usize iRow) const;

		/*!
		 * \remarks 获取给定列
		 * \return 返回的是拷贝
		 * \param const usize iCol
		*/
		Vec3<T> getColumn(const usize iCol) const;

		/*!
		 * \remarks 设置给定行
		 * \return 
		 * \param const usize iRow
		 * \param const Vec3<T> & vec
		*/
		void setRow(const usize iRow, const Vec3<T>& vec);

		/*!
		 * \remarks 设置给定列
		 * \return 
		 * \param const usize iCol
		 * \param const Vec3<T> & vec
		*/
		void setColumn(const usize iCol, const Vec3<T>& vec);

		/*!
		 * \remarks 沿坐标轴,均匀缩放.
		 * \return 
		 * \param T r
		*/
		Mat3& setUniformScale(T r);

		/*!
		 * \remarks 沿任意方向缩放.
		 * \return 
		 * \param const Vec3<T> & n 平行于缩放方向的单位向量.
		 * \param T k
		*/
		static Mat3 makeScaleAnyDirection(const Vec3<T>& n, T k);

		/*!
		 * \remarks 绕x轴旋转r弧度.
		 * 旋转方向使用右手法则，也就是说，若使右手拇指朝向旋转轴的方向，正旋转角则是其余4只手指弯曲的方向。
		 * set的意思是对当前矩阵的数据进行覆写
		 * \return 
		 * \param Radian r
		*/
		void setRotateX(Radian r);

		/*!
		 * \remarks 绕x轴旋转d度
		 * \return 
		 * \param Degree d
		*/
		void setRotateX(Degree d);

		/*!
		 * \remarks 绕x轴旋转r弧度
		 * \return 
		 * \param Radian r
		*/
		static Mat3 makeRotateX(Radian r);

		/*!
		 * \remarks 绕x轴旋转d度.
		 * \return 
		 * \param Degree d
		*/
		static Mat3 makeRotateX(Degree d);

		/*!
		 * \remarks 绕y轴旋转r弧度.
		 * \return 
		 * \param Radian r
		*/
		void setRotateY(Radian r);

		/*!
		 * \remarks 绕y轴旋转d度.
		 * \return 
		 * \param Degree d
		*/
		void setRotateY(Degree d);

		/*!
		 * \remarks 绕y轴旋转r弧度.
		 * \return 
		 * \param Radian r
		*/
		static Mat3 makeRotateY(Radian r);

		/*!
		 * \remarks 绕y轴旋转d度.
		 * \return 
		 * \param Degree d
		*/
		static Mat3 makeRotateY(Degree d);

		/*!
		 * \remarks 绕z轴旋转r弧度.
		 * \return 
		 * \param Radian r
		*/
		void setRotateZ(Radian r);

		/*!
		 * \remarks 绕z轴旋转d度.
		 * \return 
		 * \param Degree d
		*/
		void setRotateZ(Degree d);

		/*!
		 * \remarks 绕z轴旋转r弧度.
		 * \return 
		 * \param Radian r
		*/
		static Mat3 makeRotateZ(Radian r);

		/*!
		 * \remarks 绕z轴旋转d度.
		 * \return 
		 * \param Degree d
		*/
		static Mat3 makeRotateZ(Degree d);

		/*!
		 * \remarks Axis-Angle to Matrix(绕任意轴旋转r弧度).
		 * \return 
		 * \param const Vec3<T> & n
		 * \param Radian alpha
		*/
		void setRotateAnyAxis(const Vec3<T>& n, Radian alpha);

		/*!
		 * \remarks Axis-Angle to Matrix(绕任意轴旋转d度).
		 * \return 
		 * \param const Vec3<T> & n
		 * \param Degree d
		*/
		void setRotateAnyAxis(const Vec3<T>& n, Degree d);

		/*!
		 * \remarks Axis-Angle to Matrix(绕任意轴旋转r弧度).
		 * \return 
		 * \param const Vec3<T> & n
		 * \param Radian alpha
		*/
		static Mat3 makeRotateAnyAxis(const Vec3<T>& n, Radian alpha);

		/*!
		 * \remarks Axis-Angle to Matrix(绕任意轴旋转d度).
		 * \return 
		 * \param const Vec3<T> & n
		 * \param Degree d
		*/
		static Mat3 makeRotateAnyAxis(const Vec3<T>& n, Degree d);

		/*!
		 * \remarks Matrix to Axis-Angle.抽取的轴为单位长度.抽取旋转矩阵的轴和角度(弧度)
		 * \return 
		 * \param Vec3<T> & axis
		 * \param Radian& alpha
		*/
		void extractAxisAngle(Vec3<T>& axis, Radian& alpha) const;

		/*!
		 * \remarks Matrix to Axis-Angle.抽取的轴为单位长度.抽取旋转矩阵的轴和角度(度)
		 * \return 
		 * \param Vec3<T> & axis
		 * \param Degree& d
		*/
		void extractAxisAngle(Vec3<T>& axis, Degree& d) const;

		/*!
		 * \remarks 从四元数到旋转矩阵
		 * \return 
		 * \param const Quater<T> & q
		*/
		void setRotateFromQuaternion(const Quater<T>& q);

		/*!
		 * \remarks 从旋转矩阵到四元数
		 * \return 
		*/
		Quater<T> getQuaternionOfRotate() const;

		/*!
		 * \remarks 正交投影(在投影方向上进行0缩放(所以，投影方向的正负对结果没有影响))
		 * \return 
		 * \param const Vec3<T> & n 投影方向
		*/
		static Mat3 makeOrthogonal(const Vec3<T>& n);

		/*!
		 * \remarks 镜像(反射)(沿通过原点且垂直于n的平面来进行镜像变换.)
		 * \return 
		 * \param const Vec3<T> & n 方向的正负对结果不影响
		*/
		static Mat3 makeReflectAnyPlane(const Vec3<T>& n);

	public:
		T m[3][3];

		static const Mat3 ZERO;
		static const Mat3 IDENTITY;
	};
}//namespace ung

#ifndef _UML_MAT3DEF_H_
#include "UMLMat3Def.h"
#endif

#endif//UML_HIERARCHICAL_COMPILE

#endif//_UML_MAT3_H_