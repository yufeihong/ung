/*!
 * \brief
 * 对象接口
 * \file UIFIObject.h
 *
 * \author Su Yang
 *
 * \date 2016/11/17
 */
#ifndef _UIF_IOBJECT_H_
#define _UIF_IOBJECT_H_

#ifndef _UIF_CONFIG_H_
#include "UIFConfig.h"
#endif

#ifdef UIF_HIERARCHICAL_COMPILE

#ifndef _INCLUDE_TINYXML2_H_
#include "tinyxml2.h"
#define _INCLUDE_TINYXML2_H_
#endif

namespace ung
{
	class ISpatialNode;
	class AABB;

	/*!
	 * \brief
	 * 对象接口
	 * \class IObject
	 *
	 * \author Su Yang
	 *
	 * \date 2016/12/14
	 *
	 * \todo
	 */
	class UngExport IObject : public MemoryPool<IObject>,public std::enable_shared_from_this<IObject>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		IObject() = default;

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~IObject() = default;

		/*!
		 * \remarks 获取对象的名字
		 * \return 
		*/
		virtual String const& getName() const = 0;

		/*!
		 * \remarks 关联对象到场景图节点
		 * \return 
		 * \param StrongISceneNodePtr sceneNodePtr
		*/
		virtual void attachToSceneNode(StrongISceneNodePtr sceneNodePtr) = 0;

		/*!
		 * \remarks 对象脱离其所关联的场景图节点
		 * \return 
		 * \param bool remove true:将对象从场景节点的容器中移除
		*/
		virtual void detachFromSceneNode(bool remove = true) = 0;

		/*!
		 * \remarks 对象是否已经关联到场景图中的一个节点
		 * \return 
		*/
		virtual bool isAttachSceneNode() const = 0;

		/*!
		 * \remarks 获取对象所关联的场景图节点
		 * \return 
		*/
		virtual WeakISceneNodePtr getAttachedNode() const = 0;

		/*!
		 * \remarks 销毁对象，当不再需要该对象时，必须手工调用该函数，以便于触发组件的析构函数，
		 * 从而释放组件对该对象的“强引用”，否则将无法触发该对象自己的析构函数。
		 * 已经在SceneManager::destroyObject()中调用了。客户代码要销毁对象时只需通过该接口即可。
		 * \return 
		*/
		virtual void destroy() = 0;

		/*!
		 * \remarks 设置对象所关联的场景节点(同时更新对象与空间管理器的关系)。该函数仅可被SceneNode调用
		 * \return 
		 * \param WeakISceneNodePtr sceneNodePtr
		*/
		virtual void setAttachedSceneNode(WeakISceneNodePtr sceneNodePtr) = 0;

		/*!
		 * \remarks 在添加组件之前，进行一些初始化工作
		 * \return 
		 * \param tinyxml2::XMLElement* pRoot
		*/
		virtual void preInit(tinyxml2::XMLElement* pRoot) = 0;

		/*!
		 * \remarks 在添加组件之后，进行一些初始化工作
		 * \return 
		*/
		virtual void postInit() = 0;

		/*!
		 * \remarks 添加组件
		 * \return 
		 * \param StrongIObjectComponentPtr pComponent
		*/
		virtual void addComponent(StrongIObjectComponentPtr pComponent) = 0;

		/*!
		 * \remarks 便捷接口，获取TransformComponent的一个强引用
		 * \return 
		*/
		virtual std::shared_ptr<IMovable> getTransformComponent() = 0;

		/*!
		 * \remarks 更新
		 * \return 
		*/
		virtual void update() = 0;

		/*!
		 * \remarks 调整对象在空间管理器树中的位置
		 * \return 
		*/
		virtual void adjustmentHangInSpatial() = 0;

		/*!
		 * \remarks 获取对象所关联的空间管理器树节点
		 * \return 
		*/
		virtual ISpatialNode* getAttachedSpatialNode() const = 0;

		/*!
		 * \remarks 获取对象的包围体的半径
		 * \return 
		*/
		virtual real_type getRadius() const = 0;

		//这两个接口要使用UPE，需要调整

		/*!
		 * \remarks 获取对象的AABB包围体(对象模型空间)
		 * \return 
		*/
		virtual AABB const& getAABB() const = 0;

		/*!
		 * \remarks 获取对象的AABB包围体(世界空间)
		 * \return 
		*/
		virtual AABB getWorldAABB() = 0;
	};
}//namespace ung

#endif//UIF_HIERARCHICAL_COMPILE

#endif//_UIF_IOBJECT_H_