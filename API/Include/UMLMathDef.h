#ifndef _UML_MATH_DEF_H_
#define _UML_MATH_DEF_H_

#ifndef _UML_CONFIG_H_
#include "UMLConfig.h"
#endif

#ifdef UML_HIERARCHICAL_COMPILE

#ifndef _UML_MATH_H_
#include "UMLMath.h"
#endif

#ifndef _INCLUDE_STLIB_LIMITS_
#include <limits>
#define _INCLUDE_STLIB_LIMITS_
#endif

namespace ung
{
	template<typename T> const T MathImpl<T>::POS_INFINITY = std::numeric_limits<T>::infinity();
	template<typename T> const T MathImpl<T>::NEG_INFINITY = -std::numeric_limits<T>::infinity();
}//namespace ung

#endif//UML_HIERARCHICAL_COMPILE

#endif//_UML_MATH_DEF_H_

/*
显式删除构造函数.
这个类只有静态的方法,没有任何构造函数,也不想让编译器生成默认构造函数.
*/