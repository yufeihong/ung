/*!
 * \brief
 * 像素格式接口
 * \file UIFIPixelFormat.h
 *
 * \author Su Yang
 *
 * \date 2017/04/29
 */
#ifndef _UIF_IPIXELFORMAT_H_
#define _UIF_IPIXELFORMAT_H_

#ifndef _UIF_CONFIG_H_
#include "UIFConfig.h"
#endif

#ifdef UIF_HIERARCHICAL_COMPILE

//namespace ung
//{
//	enum class PixelFormat : uint32
//	{
//		PF_RGBA8,PF_RGBA16,PF_RGBA32
//	};
//
//	/*!
//	 * \brief
//	 * 像素格式接口
//	 * \class IPixelFormat
//	 *
//	 * \author Su Yang
//	 *
//	 * \date 2017/04/29
//	 *
//	 * \todo
//	 */
//	class UngExport IPixelFormat : public MemoryPool<IPixelFormat>
//	{
//	public:
//		/*!
//		 * \remarks 默认构造函数
//		 * \return 
//		*/
//		IPixelFormat() = default;
//
//		/*!
//		 * \remarks 虚析构函数
//		 * \return 
//		*/
//		virtual ~IPixelFormat() = default;
//
//		/*!
//		 * \remarks 设置像素格式
//		 * \return 
//		 * \param PixelFormat pf
//		*/
//		virtual void set(PixelFormat pf) = 0;
//
//		/*!
//		 * \remarks 获取像素格式
//		 * \return 
//		*/
//		virtual PixelFormat get() const = 0;
//	};
//}//namespace ung

#endif//UIF_HIERARCHICAL_COMPILE

#endif//_UIF_IPIXELFORMAT_H_