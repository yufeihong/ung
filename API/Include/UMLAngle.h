/*!
 * \brief
 * 角度
 * \file UMLAngle.h
 *
 * \author Su Yang
 *
 * \date 2017/04/08
 */
#ifndef _UML_ANGLE_H_
#define _UML_ANGLE_H_

#ifndef _UML_CONFIG_H_
#include "UMLConfig.h"
#endif

#ifdef UML_HIERARCHICAL_COMPILE

namespace ung
{
	class Radian;

	/*!
	 * \brief
	 * 度
	 * \class Degree
	 *
	 * \author Su Yang
	 *
	 * \date 2017/04/08
	 *
	 * \todo
	 */
	class UngExport Degree : public MemoryPool<Degree>
	{
	public:
		/*!
		 * \remarks real_type + Degree
		 * \return 
		 * \param real_type d1
		 * \param Degree d2
		*/
		friend UngExport Degree operator+(real_type d1, Degree d2);

		/*!
		 * \remarks real_type - Degree
		 * \return 
		 * \param real_type d1
		 * \param Degree d2
		*/
		friend UngExport Degree operator-(real_type d1, Degree d2);

		/*!
		 * \remarks real_type * Degree
		 * \return 
		 * \param real_type d1
		 * \param Degree d2
		*/
		friend UngExport Degree operator*(real_type d1, Degree d2);

		/*!
		 * \remarks real_type == Degree
		 * \return 
		 * \param real_type d1
		 * \param Degree d2
		*/
		friend UngExport bool operator==(real_type d1, Degree d2);

		/*!
		 * \remarks real_type != Degree
		 * \return 
		 * \param real_type d1
		 * \param Degree d2
		*/
		friend UngExport bool operator!=(real_type d1, Degree d2);

		/*!
		 * \remarks real_type > Degree
		 * \return 
		 * \param real_type d1
		 * \param Degree d2
		*/
		friend UngExport bool operator>(real_type d1, Degree d2);

		/*!
		 * \remarks real_type < Degree
		 * \return 
		 * \param real_type d1
		 * \param Degree d2
		*/
		friend UngExport bool operator<(real_type d1, Degree d2);

		/*!
		 * \remarks real_type >= Degree
		 * \return 
		 * \param real_type d1
		 * \param Degree d2
		*/
		friend UngExport bool operator>=(real_type d1, Degree d2);

		/*!
		 * \remarks real_type <= Degree
		 * \return 
		 * \param real_type d1
		 * \param Degree d2
		*/
		friend UngExport bool operator<=(real_type d1, Degree d2);

		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		Degree() = default;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param real_type d
		*/
		explicit Degree(real_type d);

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~Degree() = default;

		/*!
		 * \remarks 获取度值
		 * \return 
		*/
		real_type getDegree() const;

		/*!
		 * \remarks 转换为弧度
		 * \return 
		*/
		Radian toRadian() const;

		/*!
		 * \remarks 负
		 * \return 
		*/
		Degree operator-() const;

		/*!
		 * \remarks Degree + real_type
		 * \return 
		 * \param real_type d
		*/
		Degree operator+(real_type d) const;

		/*!
		 * \remarks Degree += real_type
		 * \return 
		 * \param real_type d
		*/
		Degree& operator+=(real_type d);

		/*!
		 * \remarks Degree - real_type
		 * \return 
		 * \param real_type d
		*/
		Degree operator-(real_type d) const;

		/*!
		 * \remarks Degree -= real_type
		 * \return 
		 * \param real_type d
		*/
		Degree& operator-=(real_type d);

		/*!
		 * \remarks Degree * real_type
		 * \return 
		 * \param real_type d
		*/
		Degree operator*(real_type d) const;

		/*!
		 * \remarks Degree *= real_type
		 * \return 
		 * \param real_type d
		*/
		Degree& operator*=(real_type d);

		/*!
		 * \remarks Degree + Degree
		 * \return 
		 * \param Degree d
		*/
		Degree operator+(Degree d);

		/*!
		 * \remarks Degree += Degree
		 * \return 
		 * \param Degree d
		*/
		Degree& operator+=(Degree d);

		/*!
		 * \remarks Degree - Degree
		 * \return 
		 * \param Degree d
		*/
		Degree operator-(Degree d);

		/*!
		 * \remarks Degree -= Degree
		 * \return 
		 * \param Degree d
		*/
		Degree& operator-=(Degree d);

		/*!
		 * \remarks Degree == real_type
		 * \return 
		 * \param real_type d
		*/
		bool operator==(real_type d);

		/*!
		 * \remarks Degree != real_type
		 * \return 
		 * \param real_type d
		*/
		bool operator!=(real_type d);

		/*!
		 * \remarks Degree == Degree
		 * \return 
		 * \param Degree d
		*/
		bool operator==(Degree d);

		/*!
		 * \remarks Degree != Degree
		 * \return 
		 * \param Degree d
		*/
		bool operator!=(Degree d);

		/*!
		 * \remarks Degree > real_type
		 * \return 
		 * \param real_type d
		*/
		bool operator>(real_type d);

		/*!
		 * \remarks Degree < real_type
		 * \return 
		 * \param real_type d
		*/
		bool operator<(real_type d);

		/*!
		 * \remarks Degree >= real_type
		 * \return 
		 * \param real_type d
		*/
		bool operator>=(real_type d);

		/*!
		 * \remarks Degree <= real_type
		 * \return 
		 * \param real_type d
		*/
		bool operator<=(real_type d);

		/*!
		 * \remarks Degree > Degree
		 * \return 
		 * \param Degree d
		*/
		bool operator>(Degree d);

		/*!
		 * \remarks Degree < Degree
		 * \return 
		 * \param Degree d
		*/
		bool operator<(Degree d);

		/*!
		 * \remarks Degree >= Degree
		 * \return 
		 * \param Degree d
		*/
		bool operator>=(Degree d);

		/*!
		 * \remarks Degree <= Degree
		 * \return 
		 * \param Degree d
		*/
		bool operator<=(Degree d);

	private:
		real_type mDegree;
	};

	UngExport Degree operator+(real_type d1, Degree d2);

	UngExport Degree operator-(real_type d1, Degree d2);

	UngExport Degree operator*(real_type d1, Degree d2);

	UngExport bool operator==(real_type d1, Degree d2);

	UngExport bool operator!=(real_type d1, Degree d2);

	UngExport bool operator>(real_type d1, Degree d2);

	UngExport bool operator<(real_type d1, Degree d2);

	UngExport bool operator>=(real_type d1, Degree d2);

	UngExport bool operator<=(real_type d1, Degree d2);

	/*!
	 * \brief
	 * 弧度
	 * \class Radian
	 *
	 * \author Su Yang
	 *
	 * \date 2017/04/08
	 *
	 * \todo
	 */
	class UngExport Radian : public MemoryPool<Radian>
	{
	public:
		/*!
		 * \remarks real_type + Radian
		 * \return 
		 * \param real_type r1
		 * \param Radian r2
		*/
		friend UngExport Radian operator+(real_type r1, Radian r2);

		/*!
		 * \remarks real_type - Radian
		 * \return 
		 * \param real_type r1
		 * \param Radian r2
		*/
		friend UngExport Radian operator-(real_type r1, Radian r2);

		/*!
		 * \remarks real_type * Radian
		 * \return 
		 * \param real_type r1
		 * \param Radian r2
		*/
		friend UngExport Radian operator*(real_type r1, Radian r2);

		/*!
		 * \remarks real_type == Radian
		 * \return 
		 * \param real_type r1
		 * \param Radian r2
		*/
		friend UngExport bool operator==(real_type r1, Radian r2);

		/*!
		 * \remarks real_type != Radian
		 * \return 
		 * \param real_type r1
		 * \param Radian r2
		*/
		friend UngExport bool operator!=(real_type r1, Radian r2);

		/*!
		 * \remarks real_type > Radian
		 * \return 
		 * \param real_type d1
		 * \param Radian d2
		*/
		friend UngExport bool operator>(real_type d1, Radian d2);

		/*!
		 * \remarks real_type < Radian
		 * \return 
		 * \param real_type d1
		 * \param Radian d2
		*/
		friend UngExport bool operator<(real_type d1, Radian d2);

		/*!
		 * \remarks real_type >= Radian
		 * \return 
		 * \param real_type d1
		 * \param Radian d2
		*/
		friend UngExport bool operator>=(real_type d1, Radian d2);

		/*!
		 * \remarks real_type <= Radian
		 * \return 
		 * \param real_type d1
		 * \param Radian d2
		*/
		friend UngExport bool operator<=(real_type d1, Radian d2);

		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		Radian() = default;

		/*!
		 * \remarks 构造函数
		 * \return 
		 * \param real_type r
		*/
		explicit Radian(real_type r);

		/*!
		 * \remarks 析构函数
		 * \return 
		*/
		~Radian() = default;

		/*!
		 * \remarks 获取弧度值
		 * \return 
		*/
		real_type getRadian() const;

		/*!
		 * \remarks 转换为度
		 * \return 
		*/
		Degree toDegree() const;

		/*!
		 * \remarks 负
		 * \return 
		*/
		Radian operator-() const;

		/*!
		 * \remarks Radian + real_type
		 * \return 
		 * \param real_type r
		*/
		Radian operator+(real_type r) const;

		/*!
		 * \remarks Radian += real_type
		 * \return 
		 * \param real_type r
		*/
		Radian& operator+=(real_type r);

		/*!
		 * \remarks Radian - real_type
		 * \return 
		 * \param real_type r
		*/
		Radian operator-(real_type r) const;

		/*!
		 * \remarks Radian -= real_type
		 * \return 
		 * \param real_type r
		*/
		Radian& operator-=(real_type r);

		/*!
		 * \remarks Radian * real_type
		 * \return 
		 * \param real_type r
		*/
		Radian operator*(real_type r) const;

		/*!
		 * \remarks Radian *= real_type
		 * \return 
		 * \param real_type r
		*/
		Radian& operator*=(real_type r);

		/*!
		 * \remarks Radian + Radian
		 * \return 
		 * \param Radian r
		*/
		Radian operator+(Radian r);

		/*!
		 * \remarks Radian += Radian
		 * \return 
		 * \param Radian r
		*/
		Radian& operator+=(Radian r);

		/*!
		 * \remarks Radian - Radian
		 * \return 
		 * \param Radian r
		*/
		Radian operator-(Radian r);

		/*!
		 * \remarks Radian -= Radian
		 * \return 
		 * \param Radian r
		*/
		Radian& operator-=(Radian r);

		/*!
		 * \remarks Radian == real_type
		 * \return 
		 * \param real_type d
		*/
		bool operator==(real_type d);

		/*!
		 * \remarks Radian != real_type
		 * \return 
		 * \param real_type d
		*/
		bool operator!=(real_type d);

		/*!
		 * \remarks Radian == Radian
		 * \return 
		 * \param Radian r
		*/
		bool operator==(Radian r);

		/*!
		 * \remarks Radian != Radian
		 * \return 
		 * \param Radian r
		*/
		bool operator!=(Radian r);

		/*!
		 * \remarks Radian > real_type
		 * \return 
		 * \param real_type d
		*/
		bool operator>(real_type d);

		/*!
		 * \remarks Radian < real_type
		 * \return 
		 * \param real_type d
		*/
		bool operator<(real_type d);

		/*!
		 * \remarks Radian >= real_type
		 * \return 
		 * \param real_type d
		*/
		bool operator>=(real_type d);

		/*!
		 * \remarks Radian <= real_type
		 * \return 
		 * \param real_type d
		*/
		bool operator<=(real_type d);

		/*!
		 * \remarks Radian > Radian
		 * \return 
		 * \param Radian d
		*/
		bool operator>(Radian d);

		/*!
		 * \remarks Radian < Radian
		 * \return 
		 * \param Radian d
		*/
		bool operator<(Radian d);

		/*!
		 * \remarks Radian >= Radian
		 * \return 
		 * \param Radian d
		*/
		bool operator>=(Radian d);

		/*!
		 * \remarks Radian <= Radian
		 * \return 
		 * \param Radian d
		*/
		bool operator<=(Radian d);

	private:
		real_type mRadian;
	};

	UngExport Radian operator+(real_type r1, Radian r2);

	UngExport Radian operator-(real_type r1, Radian r2);

	UngExport Radian operator*(real_type r1, Radian r2);

	UngExport bool operator==(real_type r1, Radian r2);

	UngExport bool operator!=(real_type r1, Radian r2);

	UngExport bool operator>(real_type d1, Radian d2);

	UngExport bool operator<(real_type d1, Radian d2);

	UngExport bool operator>=(real_type d1, Radian d2);

	UngExport bool operator<=(real_type d1, Radian d2);
}//namespace ung

#endif//UML_HIERARCHICAL_COMPILE

#endif//_UML_ANGLE_H_