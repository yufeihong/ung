/*!
 * \brief
 * 可运动属性。
 * \file UPEMovable.h
 *
 * \author Su Yang
 *
 * \date 2017/03/24
 */
#ifndef _UPE_MOVABLE_H_
#define _UPE_MOVABLE_H_

#ifndef _UPE_CONFIG_H_
#include "UPEConfig.h"
#endif

#ifdef UPE_HIERARCHICAL_COMPILE

#ifndef _UIF_IMOVABLE_H_
#include "UIFIMovable.h"
#endif

namespace ung
{
	/*!
	 * \brief
	 * 可运动属性。
	 * \class Movable
	 *
	 * \author Su Yang
	 *
	 * \date 2017/03/24
	 *
	 * \todo
	 */
	class UngExport Movable : public IMovable
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		Movable();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~Movable();

		/*!
		 * \remarks 拷贝构造函数
		 * \return 
		 * \param Movable const & other
		*/
		Movable(Movable const& other);

		/*!
		 * \remarks 拷贝赋值运算符
		 * \return 
		 * \param Movable const & other
		*/
		Movable& operator=(Movable const& other);

		/*!
		 * \remarks 移动构造函数
		 * \return 
		 * \param Movable & & other
		*/
		Movable(Movable&& other);

		/*!
		 * \remarks 移动赋值运算符
		 * \return 
		 * \param Movable & & other
		*/
		Movable& operator=(Movable&& other);

		/*!
		 * \remarks 设置更新标记
		 * \return 
		 * \param bool flag
		*/
		virtual void setNeedUpdateFlag(bool flag) override;

		/*!
		 * \remarks 获取更新标记
		 * \return 
		*/
		virtual bool getNeedUpdateFlag() const override;

		/*!
		 * \remarks 覆盖设置在模型空间的旋转
		 * \return 
		 * \param Vector3 const & axis
		 * \param Radian r 弧度
		*/
		virtual void setRotate(Vector3 const& axis,Radian r) override;

		/*!
		 * \remarks 覆盖设置在模型空间的旋转
		 * \return 
		 * \param Vector3 const & axis
		 * \param Degree d 度
		*/
		virtual void setRotate(Vector3 const& axis,Degree d) override;

		/*!
		 * \remarks 覆盖设置在模型空间的缩放
		 * \return 
		 * \param real_type s
		*/
		virtual void setScale(real_type s) override;

		/*!
		 * \remarks 覆盖设置在父空间的平移
		 * \return 
		 * \param real_type x
		 * \param real_type y
		 * \param real_type z
		*/
		virtual void setTranslate(real_type x, real_type y, real_type z) override;

		/*!
		 * \remarks 覆盖设置在父空间的平移
		 * \return 
		 * \param Vector3 t
		*/
		virtual void setTranslate(Vector3 t) override;

		/*!
		 * \remarks 累计在模型空间的旋转
		 * \return 
		 * \param Vector3 const & axis
		 * \param Radian r 弧度
		*/
		virtual void addUpRotate(Vector3 const& axis,Radian r) override;

		/*!
		 * \remarks 累计在模型空间的旋转
		 * \return 
		 * \param Vector3 const & axis
		 * \param Degree d 度
		*/
		virtual void addUpRotate(Vector3 const& axis,Degree d) override;

		/*!
		 * \remarks 累计在模型空间的缩放
		 * \return 
		 * \param real_type s
		*/
		virtual void addUpScale(real_type s) override;

		/*!
		 * \remarks 累计在父空间的平移
		 * \return 
		 * \param real_type x
		 * \param real_type y
		 * \param real_type z
		*/
		virtual void addUpTranslate(real_type x, real_type y, real_type z) override;

		/*!
		 * \remarks 累计在父空间的平移
		 * \return 
		 * \param Vector3 t
		*/
		virtual void addUpTranslate(Vector3 t) override;

		/*!
		 * \remarks 获取从模型空间到父空间的变换
		 * \return 
		*/
		virtual SQT const& getToParentSQT() const override;

		/*!
		 * \remarks 获取从模型空间到父空间的变换
		 * \return 
		*/
		virtual Matrix4 getToParentMatirx() const override;

		/*!
		 * \remarks 获取从模型空间到世界空间的变换
		 * \return 
		*/
		virtual SQT const& getToWorldSQT() const override;

		/*!
		 * \remarks 获取从模型空间到世界空间的变换
		 * \return 
		*/
		virtual Matrix4 getToWorldMatrix() const override;

		/*!
		 * \remarks 获取从模型空间到世界空间的变换的逆变换
		 * \return 
		*/
		virtual SQT getToWorldSQTInverse() const override;

		/*!
		 * \remarks 获取从模型空间到世界空间的变换的逆变换
		 * \return 
		*/
		virtual Matrix4 getToWorldMatrixInverse() const override;

		/*!
		 * \remarks 获取在父空间的位置
		 * \return 
		*/
		virtual Vector4 getInParentPosition() override;

		/*!
		 * \remarks 获取在父空间的朝向
		 * \return 
		*/
		virtual Vector3 getInParentOrientation() override;

		/*!
		 * \remarks 获取在父空间的缩放
		 * \return 
		*/
		virtual real_type getInParentScale() override;

		/*!
		 * \remarks 获取在世界空间的位置
		 * \return 
		*/
		virtual Vector4 getInWorldPosition() override;

		/*!
		 * \remarks 获取在世界空间的朝向
		 * \return 
		*/
		virtual Vector3 getInWorldOrientation() override;

		/*!
		 * \remarks 获取在世界空间的缩放
		 * \return 
		*/
		virtual real_type getInWorldScale() override;

		/*!
		 * \remarks 更新到世界空间的变换
		 * \return 
		*/
		virtual void update() override;

		/*!
		 * \remarks 设置world矩阵
		 * \return 
		 * \param SQT const & world
		*/
		virtual void setWorld(SQT const& world) override;

	protected:
		SQT mLocal;
		SQT mWorld;
		bool mNeedUpdateFlag;

		static Vector4 sInModelPosition;
		static Vector3 sInModelOrientation;
		static real_type sScale;
	};
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

#endif//_UPE_MOVABLE_H_