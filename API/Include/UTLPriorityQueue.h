/*!
 * \brief
 * 优先队列
 * \file UTLPriorityQueue.h
 *
 * \author Su Yang
 *
 * \date 2016/02/25
 */
#ifndef _UNG_UTL_PRIORITYQUEUE_H_
#define _UNG_UTL_PRIORITYQUEUE_H_

#ifndef _UNG_UTL_CONFIG_H_
#include "UTLConfig.h"
#endif

namespace ung
{
	namespace utl
	{
		/*!
		 * \brief
		 * 优先队列
		 * \class PriorityQueue
		 *
		 * \author Su Yang
		 *
		 * \date 2016/02/26
		 *
		 * \todo
		 */
		template<typename T,typename Sequence = std::vector<T>>
		class PriorityQueue
		{
		public:
			using value_type = typename Sequence::value_type;
			using size_type = typename Sequence::size_type;
			using reference = typename Sequence::reference;
			using const_reference = typename Sequence::const_reference;

			/*!
			 * \remarks 构造函数，构造一个空优先队列
			 * \return 
			*/
			PriorityQueue() :
				mCont()
			{
			}

			/*!
			 * \remarks 构造函数
			 * \return 
			 * \param InputIterator first 区间开始
			 * \param PriorityQueue last 区间尾后
			*/
			template<typename InputIterator>
			PriorityQueue(InputIterator first, PriorityQueue last) :
				mCont(first, last)
			{
				ung::utl::heap::make_heap(mCont.begin(), mCont.end());
			}

			/*!
			 * \remarks 判断优先队列是否有元素
			 * \return 
			*/
			bool empty() const
			{
				return mCont.empty();
			}

			/*!
			 * \remarks 获取优先队列的元素数量
			 * \return 
			*/
			size_type size() const
			{
				return mCont.size();
			}

			/*!
			 * \remarks 获取权值最高的元素
			 * \return 
			*/
			const_reference top() const
			{
				return mCont.front();
			}

			/*!
			 * \remarks 压入元素
			 * \return 
			 * \param value_type const & val
			*/
			void push(value_type const& val)
			{
				mCont.push_back(val);
				ung::utl::heap::push_heap(mCont.begin(), mCont.end());
			}

			/*!
			 * \remarks 删除权值最高的元素
			 * \return 
			*/
			void pop()
			{
				ung::utl::heap::pop_heap(mCont.begin(), mCont.end());
				mCont.pop_back();
			}

		protected:
			Sequence mCont;
		};
	}//namespace utl
}//namespace ung

#endif//_UNG_UTL_PRIORITYQUEUE_H_

/*
priority_queue是一个拥有权值观念的queue，它允许加入新元素，移除旧元素，审视元素值等功能。由于这是一个queue，所以只允许在底端加入元素，并
从顶端取出元素，除此之外别无其它存取元素的途径。
priority_queue带有权值观念，其内的元素并非依照被推入的次序排列，而是自动依照元素的权值排列。
缺省情况下priority_queue系利用一个max-heap完成，后者是一个以vector表现的complete binary tree。max-heap可以满足priority_queue所需要的
“依权值高低自动递减排序”的特性。
queue以底部容器完成其所有工作。具有这种“修改某物接口，形成另一种风貌”之性质者，称为adapter，因此priority_queue往往不被归类为container，而
被归类为container adapter。
priority_queue没有迭代器
priority_queue的所有元素，进出都有一定的规则，只有queue顶端的元素(权值最高者)，才有机会被外界取用。priority_queue不提供遍历功能，也不提供
迭代器。
*/

/*
优先级队列的元素可以有相同的优先级，对这样的元素，查找与删除可以按任意顺序处理。
一棵大根树(小根树)是这样一棵树，其中每个节点的值都大于(小于)或等于其子节点(如果有子节点的话)的值。
在大根树或小根树中，节点的子节点个数可以任意。
一个大根堆(小根堆)既是大根树(小根树)也是完全二叉树。
堆是完全二叉树，具有n个元素的堆的高度为log2(n+1)。

插入过程是这样的，把新元素插入新节点，然后沿着从新节点到根节点的路径，执行一趟起泡操作，将新元素与其父节点的元素比较交换，直到后者大于或等于
前者为止。
在大根堆中删除一个元素，就是删除根节点的元素。

左高树：
上面的堆结构是一种隐式数据结构(implicit data structure)。用完全二叉树表示的堆在数组中是隐式存储的(即没有明确的指针或其他数据能够用来重塑这种
结构)。由于没有存储结构信息，这种表示方法的空间利用率很高，它实际上没有浪费空间。而且它的时间效率也很高。尽管如此，它并不适合于所有优先级
队列的应用，尤其是当两个优先级队列或多个长度不同的队列需要合并的时候，这时我们就需要其他数据结构了。左高树就能满足这种需要。
*/

/*
在优先队列(Priority Queue)中，根据元素的优先级以及在队列中的当前位置决定出队列的顺序。
STL中，priority_queue容器默认用vector容器实现，用户也可以使用deque容器。priority_queue容器总是把优先级最高的元素放在队列的最前方以维持队列的顺序。
为此，插入操作push()使用一个双参数的布尔函数，将队列中的元素重新排序以满足这个要求。该函数可以由用户提供，此外也可以使用<运算符，元素的值越大，
优先级越高。如果元素值越小优先级越高，则需要使用函数对象greater，表明在决定向优先队列中插入新元素时push()应该应用运算符>而不是<。
*/

/*
一棵高为h的完全二叉树有2^h到2^(h+1) - 1个节点。这意味着，完全二叉树的高是logN，显然它是O(logN)。
*/