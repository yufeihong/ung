/*!
 * \brief
 * 对freeglut的第二种包装。
 * \file UUTFreeGlutApplication.h
 *
 * \author Su Yang
 *
 * \date 2016/12/01
 */
#ifndef _UUT_FREEGLUTAPPLICATION_H_
#define _UUT_FREEGLUTAPPLICATION_H_

#ifndef _UUT_CONFIG_H_
#include "UUTConfig.h"
#endif

#ifdef UUT_HIERARCHICAL_COMPILE

namespace ung
{
	class UngExport FreeglutApplication : public MemoryPool<FreeglutApplication>
	{
	public:
		FreeglutApplication();

		virtual ~FreeglutApplication();

		virtual void initialize();
		
		//------------------hooks-----------------
		virtual void keyboard(unsigned char key, int x, int y);
		virtual void keyboardUp(unsigned char key, int x, int y);
		virtual void special(int key, int x, int y);
		virtual void specialUp(int key, int x, int y);
		virtual void reshape(int w, int h);
		virtual void idle();
		virtual void mouse(int button, int state, int x, int y);
		virtual void passiveMotion(int x, int y);
		virtual void motion(int x, int y);
		virtual void display();
	};

	static FreeglutApplication* globalApplicationPtr;

	static void keyboardCallback(unsigned char key, int x, int y)
	{
		globalApplicationPtr->keyboard(key, x, y);
	}

	static void keyboardUpCallback(unsigned char key, int x, int y)
	{
		globalApplicationPtr->keyboardUp(key, x, y);
	}

	static void specialCallback(int key, int x, int y)
	{
		globalApplicationPtr->special(key, x, y);
	}

	static void specialUpCallback(int key, int x, int y)
	{
		globalApplicationPtr->specialUp(key, x, y);
	}

	static void reshapeCallback(int w, int h)
	{
		globalApplicationPtr->reshape(w, h);
	}

	static void idleCallback()
	{
		globalApplicationPtr->idle();
	}

	static void mouseCallback(int button, int state, int x, int y)
	{
		globalApplicationPtr->mouse(button, state, x, y);
	}

	static void motionCallback(int x, int y)
	{
		globalApplicationPtr->motion(x, y);
	}

	static void displayCallback(void)
	{
		globalApplicationPtr->display();
	}

	UNG_C_LINK_BEGIN;

	UngExport int UNG_STDCALL freeglutGo(int argc, char **argv, int width, int height, const char* title, FreeglutApplication* appPtr);

	UNG_C_LINK_END;

	//-----------------------------------------------------------------------------------------

	class UngExport BaseFreeglutApplication : public FreeglutApplication
	{
	public:
		BaseFreeglutApplication();

		virtual ~BaseFreeglutApplication();

		//------------override---------------------------
		virtual void keyboard(unsigned char key, int x, int y) override;

		virtual void special(int key, int x, int y) override;

		virtual void reshape(int nW, int nH) override;

		virtual void initialize() override;

		//-------------------------------------------------

	protected:
		void updateCamera();
		void rotateCamera(real_type& angle, real_type value);
		void zoomCamera(real_type distance);

	protected:
		uint32 mScreenWidth;
		uint32 mScreenHeight;

		Vector3 mCameraPosition;
		Vector3 mCameraTarget;
		real_type mNearPlane;
		real_type mFarPlane;
		Vector3 mUpVector;
		//distance from the camera to its target.
		real_type mCameraDistance;
		real_type mCameraPitch;
		real_type mCameraYaw;
	};
}//namespace ung

#endif//UUT_HIERARCHICAL_COMPILE

#endif//_UUT_FREEGLUTAPPLICATION_H_