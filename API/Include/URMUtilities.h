/*!
 * \brief
 * URM工具
 * \file URMUtilities.h
 *
 * \author Su Yang
 *
 * \date 2017/06/07
 */
#ifndef _URM_UTILITIES_H_
#define _URM_UTILITIES_H_

#ifndef _URM_CONFIG_H_
#include "URMConfig.h"
#endif

#ifdef URM_HIERARCHICAL_COMPILE

#ifndef _URM_ENUMCOLLECTION_H_
#include "URMEnumCollection.h"
#endif

namespace ung
{
	UNG_C_LINK_BEGIN;

	/*!
	 * \remarks 处理color类型
	 * \return 
	 * \param VertexElementType vet
	*/
	UngExport VertexElementType UNG_STDCALL ungProcessVertexElementType(VertexElementType vet);

	UNG_C_LINK_END;
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

#endif//_URM_UTILITIES_H_