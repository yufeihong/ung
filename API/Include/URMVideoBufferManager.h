///*!
// * \brief
// * video buffer manager
// * \file URMVideoBufferManager.h
// *
// * \author Su Yang
// *
// * \date 2017/06/02
// */
//#ifndef _URM_VIDEOBUFFERMANAGER_H_
//#define _URM_VIDEOBUFFERMANAGER_H_
//
//#ifndef _URM_CONFIG_H_
//#include "URMConfig.h"
//#endif
//
//#ifdef URM_HIERARCHICAL_COMPILE
//
//#ifndef _UFC_STLWRAPPER_H_
//#include "UFCSTLWrapper.h"
//#endif
//
//#ifndef _URM_ENUMCOLLECTION_H_
//#include "URMEnumCollection.h"
//#endif
//
//namespace ung
//{
//	class VertexDeclaration;
//	class VertexBuffer;
//	class IndexBuffer;
//
//	/*!
//	 * \brief
//	 * video buffer manager
//	 * \class VideoBufferManager
//	 *
//	 * \author Su Yang
//	 *
//	 * \date 2017/06/02
//	 *
//	 * \todo
//	 */
//	class UngExport VideoBufferManager : public MemoryPool<VideoBufferManager>
//	{
//	protected:
//		using VertexDeclarations = SetWrapper<VertexDeclaration*>;
//		using VertexBuffers = SetWrapper<VertexBuffer*>;
//		using IndexBuffers = SetWrapper<IndexBuffer*>;
//
//	public:
//		/*!
//		 * \remarks 默认构造函数
//		 * \return 
//		*/
//		VideoBufferManager();
//
//		/*!
//		 * \remarks 虚析构函数
//		 * \return 
//		*/
//		virtual ~VideoBufferManager();
//
//		/*!
//		 * \remarks 创建顶点声明
//		 * \return 
//		*/
//		virtual VertexDeclaration* createVertexDeclaration();
//
//		/*!
//		 * \remarks 销毁顶点声明
//		 * \return 
//		 * \param VertexDeclaration * decl
//		*/
//		virtual void destroyVertexDeclaration(VertexDeclaration* decl);
//
//		/*!
//		 * \remarks 创建顶点缓冲区
//		 * \return 
//		 * \param uint32 vertexSize 一个顶点的字节数
//		 * \param uint32 numVertices 顶点数量
//		 * \param uint8 usage
//		 * \param bool useShadowBuffer
//		*/
//		virtual VertexBuffer* createVertexBuffer(uint32 vertexSize, uint32 numVertices, uint8 usage, bool useShadowBuffer = false) PURE;
//
//		/*!
//		 * \remarks 创建索引缓冲区
//		 * \return 
//		 * \param IndexType type
//		 * \param uint32 numIndexes
//		 * \param uint8 usage
//		 * \param bool useShadowBuffer
//		*/
//		virtual IndexBuffer* createIndexBuffer(IndexType type,uint32 numIndexes,uint8 usage, bool useShadowBuffer = false) PURE;
//
//	protected:
//		virtual VertexDeclaration* createVertexDeclarationImpl();
//
//		virtual void destroyVertexDeclarationImpl(VertexDeclaration* decl);
//
//	protected:
//		VertexDeclarations mVertexDeclarations;
//		VertexBuffers mVertexBuffers;
//		IndexBuffers mIndexBuffers;
//	};
//}//namespace ung
//
//#endif//URM_HIERARCHICAL_COMPILE
//
//#endif//_URM_VIDEOBUFFERMANAGER_H_