/*!
 * \brief
 * OpenGL program
 * \file UUTGLProgram.h
 *
 * \author Su Yang
 *
 * \date 2016/11/13
 */
#ifndef _UUT_GLPROGRAM_H_
#define _UUT_GLPROGRAM_H_

#ifndef _UUT_CONFIG_H_
#include "UUTConfig.h"
#endif

#ifdef UUT_HIERARCHICAL_COMPILE

namespace ung
{
	class GLShader;

	/*!
	 * \brief
	 * 封装OpenGL program行为。
	 * \class GLProgram
	 *
	 * \author Su Yang
	 *
	 * \date 2016/11/13
	 *
	 * \todo
	 */
	class UngExport GLProgram : public MemoryPool<GLProgram>
	{
	public:
		/*!
		 * \remarks 默认构造函数
		 * \return 
		*/
		GLProgram();

		/*!
		 * \remarks 虚析构函数
		 * \return 
		*/
		virtual ~GLProgram();

		/*!
		 * \remarks 连接shader到program
		 * \return 
		 * \param std::shared_ptr<GLShader> shader
		*/
		void attach(std::shared_ptr<GLShader> shader);

		/*!
		 * \remarks 从program中分离shader
		 * \return 
		 * \param std::shared_ptr<GLShader> shader
		*/
		void detach(std::shared_ptr<GLShader> shader);

		/*!
		 * \remarks 从program中分离所有的shader
		 * \return 
		*/
		void detachAll();

		/*!
		 * \remarks 链接GPU程序
		 * \return 
		*/
		void link();

		/*!
		 * \remarks 使用program
		 * \return 
		*/
		void use();

		/*!
		 * \remarks 获取program的标识符
		 * \return 
		*/
		GLuint const getProgram() const;

	private:
		/*!
		 * \remarks 把给定shader给保存到容器中。
		 * \return 
		 * \param std::shared_ptr<GLShader> shader
		*/
		void insert(std::shared_ptr<GLShader> shader);

		/*!
		 * \remarks 从容器中删除给定的shader
		 * \return 
		 * \param std::shared_ptr<GLShader> shader
		*/
		void erase(std::shared_ptr<GLShader> shader);

		/*!
		 * \remarks 从容器中删除所有的shader
		 * \return 
		*/
		void eraseAll();

	private:
		GLuint mProgram;
		STL_LIST(std::shared_ptr<GLShader>) mShaders;
	};
}//namespace ung

#endif//UUT_HIERARCHICAL_COMPILE

#endif//_UUT_GLPROGRAM_H_