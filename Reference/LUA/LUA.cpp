/*
Lua:
In the past decade or so, games have started to become much more data driven.For example,
the Actor system lets you create whole classes of different actors by just mixing and matching 
different components.You can even add and remove components at runtime, making it 
possible to completely change an actor’s definition,behavior, and properties without having to 
recompile and relaunch the game.

Scripting Languages:
a scripting language is a high-level programming language that is interpreted by another 
program at runtime.
Is C# a scripting language? It certainly meets the criteria.C# is compiled into byte-code that’s 
interpreted at runtime by NET,yet most people don’t consider it a scripting language.
I’m going to consider anything higher level than C++ that is embedded into the core game 
engine to be a scripting language.
You should absolutely use it(脚本语言) for all of your high-level game logic.

Rapid Prototyping:
One of the coolest things about using a scripting language is the rapid prototyping.
Functions are usually first-class objects,which means they are treated like any other variable.
You can pass them around,assign them to other variables, and so on.
Another big advantage to scripts is that they can usually be reloaded at runtime.
There was a programmer who was in charge of the camera system for Drawn to Life.He 
implemented the entire thing in Lua very quickly, moving the math functions to C++ as 
needed. This allowed him to iterate with the designers very quickly, often making changes 
and reloading the scripts while they were sitting right there. I think we went through a dozen 
different camera schemes, all very different from each other. When the design finally 
solidified, he moved most of it down to C++ for performance reasons.This is a great example 
of what rapid prototyping buys you. If he had started in C++, he wouldn’t have been able to 
iterate nearly as quickly.

Design Focused:
An interesting side effect to scripting languages is that they tend to be a bit more designer 
friendly.All of the AI on Drawn to Life, for instance, was configured through these 
designer-facing scripts. This is a very powerful concept.
For example, you could load up a creature and pass in an AI script. You could even expose 
the scripts to the end-users and allow them to mod your game.

Speed and Memory Costs:
All of this power comes at a cost. Scripts are generally very slow when compared to C++,
and they take up more memory, which is one of the main disadvantages to using a scripting 
language.
Crossing the boundary between C++ and script is also very slow,especially if you’re 
marshalling a large amount of data. For these reasons, you would never want to attempt to 
write a high performance graphics engine or physics engine using a scripting language;these 
systems are best saved for C++.

Choosing a Scripting Language:
The two most common general-purpose scripting languages used in game development are 
Python and Lua.
Lua runs in a much smaller memory space than other languages like Python.

Object-Oriented Programming in Lua:
Lua doesn’t have any direct support for object-oriented programming, although it’s possible 
to plug it in using tables. Tables give you a way to group data together and map chunks of 
data to names (string keys). Since functions are really just another form of data, you can 
easily group them all together to create encapsulation.

Binding Lua to C++:
The Lua C API:
Binding C functions to the Lua API is not particularly easy, and calling Lua functions from C is 
equally difficult.
tolua++:
This library attempts to solve the problem of registering functions and classes to Lua so they 
can be used in script. First, you go into your C++ code and tag the things you want to 
expose with special comments. When you build your game, you run a simple pre-process that 
scans your source tree and generates the binding C file that exports these functions and 
classes for Lua. When your program compiles, these are all visible to Lua. We used this at 
Super-Ego Games very successfully.
The advantage of this system is that it’s trivial to export any function or class to the script.
You literally just tag the line with a //tolua_export comment, and the pre-process does the 
rest. There are a few disadvantages, though. One of the biggest disadvantages is that tolua++ 
is a one-way street. It’s not easy to call a Lua function from within C++ code and read a 
Lua-specific data structure like a table. You can return simple types like numbers and strings,
but tables are much more difficult to work with.
luabind:
luabind solves a lot of the problems of two-way communication that tolua++ has by wrapping 
a lot of the Lua C API functionality into classes. You can grab any Lua variable, read it, iterate 
across its elements if it’s a table, call it if it’s a function, and so on. You can expose C++ class,
functions, and other objects to Lua, going back and forth relatively easily.Overall, it’s a great 
system.
One big disadvantage of luabind is its reliance(依赖) on boost, which includes a lot of overhead.
Some people don’t mind this much, but for others it’s a deal breaker.

LuaPlus
LuaPlus has a lot of the same core functionality as luabind, but it has absolutely no reliance 
on other libraries. It tends to run faster and adds wide-string support to the core Lua library.
Many of the same class and function binding capabilities exist in LuaPlus as well.
LuaPlus does have a few disadvantages. First, it modifies the core Lua implementation.This is 
done for performance reasons and to add wide-string support. For some people, modifying 
the core library is a deal breaker. Another slight flaw when compared to luabind is that 
LuaPlus doesn’t include all of the same functionality,although LuaPlus has more than enough 
for most purposes.
*/

/*
A Crash Course in LuaPlus:
LuaState:
Everything in Lua begins with the Lua state.The Lua state represents an execution 
environment for Lua. You can have as many states as you want; each will be totally separate
with its own set of global variables, functions, etc. There are many reasons you might
want multiple states. One example might be allowing each C++ thread to have its own
state.
In the Lua C API, the lua_State struct contains all the data necessary to access the state.
Nearly all Lua functions require the lua_State object as their first parameter.
LuaPlus removes this entirely and wraps the whole thing in a single C++ class called LuaState.
To create a LuaState object, call the static Create() function.Call Destroy() to destroy it.Do 
not use new or delete on these objects.
LuaState has a number of very useful functions for accessing the Lua execution unit as a 
whole. Two key functions are DoString() and DoFile(), both of which take a string as an 
argument. DoString() will parse and execute an arbitrary string as Lua code.DoFile() will 
open,parse,and execute a file.
pLuaState->DoFile(“test.lua”);					//execute the test.lua file
pLuaState->DoString(“x = {}”);				//after this line, there will be a new global variable called x,which is an empty table.

LuaObject:
The LuaObject class represents a single Lua variable.This can be a number, string,table,
function, nil, or any other object Lua supports.This is the main interface for dealing with Lua 
variables.Note that a LuaObject is considered a strong reference to the underlying data.In 
other words, you don’t have to worry about the Lua garbage collector coming to clean up the 
object out from under you, even if all the references in Lua go away. On the flip side,make 
sure you get rid of any references to LuaObject variables that you want Lua to garbage collect.
You can check the type of an object by using the Type() function, which returns a value from 
the Types enum in LuaState. There are also a number of Is*() functions:
bool IsNil()
bool IsTable()
bool IsUserData()
bool IsCFunction()
bool IsNumber()
bool IsString()
bool IsWString()
bool IsConvertibleToNumber()
bool IsConvertibleToString()
bool IsConvertibleToWString()
bool IsFunction()
bool IsNone()
bool IsLightUserData()
bool IsBoolean()
These functions return true if the variable type matches. To retrieve a value, call one of the 
Get*() functions:
int GetInteger()
float GetFloat()
double GetDouble()
const char* GetString()
const wchar_t* GetWString()
void* GetUserData()
void* GetLightUserData()
bool GetBoolean()
To assign a value to a value to a LuaObject, use the Assign*() functions:
void AssignNil(LuaState* state)
void AssignBoolean(LuaState* state, bool value)
void AssignInteger(LuaState* state, int value)
void AssignNumber(LuaState* state, double value)
void AssignString(LuaState* state, const char* value)
void AssignWString(LuaState* state, const wchar_t* value)
void AssignUserData(LuaState* state, void* value)
void AssignLightUserData(LuaState* state, void* value)
void AssignObject(LuaState* state, LuaObject& value)
void AssignNewTable(LuaState* state, int narray = 0, int nhash = 0)
Notice that the various assignment functions require a LuaState pointer.This is because every 
valid value must be attached to a state in Lua, so when you create a new value, you tell 
LuaPlus where to attach it.

Tables:
LuaObject has a number of functions and operators specifically written to help deal with tables.
The easiest way to look up a value on a table is to use the overloaded array access operator.
You can also use the GetByName(),GetByObject(),or GetByIndex() functions to retrieve a 
value from the table.
For example,let’s saywe have the following table in Lua:
positionVec = { x = 10, y = 15 }
Let’s also say that this value is stored in a LuaObject called positionTable.We can access fields 
on this table like so:
GCC_ASSERT(positionTable.IsTable());					//safety first
LuaObject x = positionTable[“x”];							//this is one way
LuaObject y = positionTable.GetByName(“y”);			//here’s another
//let’s fill up a Vec2:
GCC_ASSERT(x.IsNumber() && y.IsNumber());		//more type checking
Vec2 vec(x.GetFloat(), y.GetFloat());
That’s all there is to it. Of course, without any type safety, you need to handle all the
error checking yourself.
You can also set a field on a table with the various Set*() functions:
void SetNil(const char* key)
void SetBoolean(const char* key, bool value)
void SetInteger(const char* key, int value)
void SetNumber(const char* key, double value)
void SetString(const char* key, const char* value)
void SetWString(const char* key, const wchar_t* value)
void SetUserData(const char* key, void* value)
void SetLightUserData(const char* key, void* value)
void SetObject(const char* key, LuaObject& value)
To add a z field to the table above, you would do the following:
positionTable.SetNumber(“z”, 0);
Iterating through tables is possible with the use of LuaPlus’s LuaTableIterator.
It’s somewhat similar in form to STL iterators, but it is not STL compliant.Here’s an example 
that loops through an entire table:
//set up a test table in Lua and read it into a LuaObject
pLuaState->DoString(“birthdayList = { John = ‘Superman’, Mary = ‘Batman’ }”);
LuaObject table = pLuaState->GetGlobals().GetByName(“globalPosition”);
//loop through the table, printing out the pair
for (LuaTableIterator it(table); it; it.Next())
{
	LuaObject key = it.GetKey();
	LuaObject value = it.GetValue();
	//do whatever you want with the objects…
}

Globals:
In Lua,tables are used for just about everything.A variable is really just a field in a table 
indexed by a string.That string is the variable name.When you define a global variable in Lua,
what really happens is that Lua inserts it into a special table where all global variables live.
There’s nothing special about this table;you can even access it directly.
-- These two lines are equivalent
x = 10
_G[“x”] = 10
-- This will print out all global variables
for key, val in pairs(_G) do
print(key, val)
end
To access a global variable in LuaPlus, you grab the globals table from the LuaState object 
and access it like any other member.
LuaObject globals = pLuaState->GetGlobals();
LuaObject positionTable = globals.GetByName(“positionVec”);
Once you have this globals table, you can assign values as well:
globals.SetString(“programName”, “Teapot Wars”);
This creates a global variable called programName and sets it to the value of
“Teapot Wars.” You can access it in Lua as normal:
print(programName) -- > prints “Teapot Wars”

Functions:
LuaPlus provides a few ways to call Lua functions from C++.The easiest way is to use the 
overloaded template LuaFunction functor class, which makes calling Lua functions look a lot 
like calling any C++ function. It takes care of all the parameter and return value conversions 
as well. For example, let’s say we have the following Lua function:
function Square(val)
	return val * val
end
To call this function from C++, you would do the following:
LuaPlus::LuaState* pLuaState;				//assume this is valid
LuaPlus::LuaFunction<float> LuaSquare = pLuaState->GetGlobal(“Square”);
float result = LuaSquare(5);
cout << result;									//this will print out 25
The LuaFunction template parameter defines the return value.The parameters are determined 
by what you pass when you call the functor.There are a number of overloaded templated call 
operators so that nearly every combination is supported, up to and including eight different 
parameters.

Calling C++ Functions from Lua:
Calling a C++ function from Lua requires you to bind that function to the Lua state.
All you need to do is bind your function to a variable in Lua. That variable becomes a Lua 
function that can be accessed directly in your Lua code. Simple types are automatically 
converted,though there’s still a little translation that needs to happen in the case of tables.
There are several ways to perform this binding with LuaPlus. The simplest way is to call the 
RegisterDirect() function on the table you want the function bound to:
float Square(float val)
{
	return val * val;
}
LuaState* pState;									//assume this is a valid LuaState pointer
LuaObject globals = pState->GetGlobals();
globals.RegisterDirect(“Square”, &Square);
That’s all there is to it.This binds the global C++ function Square() to the name “Square” in 
the globals table in Lua. That means anywhere in your Lua code, you can do this:
x = Sqaure(5) -- x will be 25
This works for static functions as well. This is how you would bind a static function to a 
global Lua function:
globals.RegisterDirect(“SomeFunction”, &SomeClass::SomeStaticFunction);
You can use an overloaded version of RegisterDirect() to bind member functions of C++ 
classes. If you have an object you know isn’t going to be destroyed while the script has 
access to it, you can bind the pointer and function pair directly by providing a reference to 
the object as the second parameter.
class SingletonClass
{
public:
	void MemberFunction(int param);
	virtual VirtualMemeberFunction(char* str);
};
SingletonClass singletonInst;
LuaState* pLuaState;								//once again, assume this is valid
//Register the member function
pLuaState->GetGlobals().RegisterDirect(“MemberFunction”, singletonInst,&SingletonClass::MemberFunction);
//You can register virtual functions too,it doesn’t matter.The correct version will get called.
pLuaState->GetGlobals().RegisterDirect(“VirtualMemberFunction”, singletonInst,&SingletonClass::VirtualMemberFunction);
In the example above,two member functions along with their instances are bound to global 
Lua functions. This still only gets us part of the way there since the reference you bind with 
RegisterDirect() never changes. What we really need is a way to bind C++ member functions 
to a special table in Lua without having to specify the object instance at registration time.This 
table can serve as the metatable for other tables that represent instances of the object. It only 
needs to be created once. When a new object is sent to the script, a new table is created with 
that metatable applied.This new table has a special __object field that contains a light user data 
pointer back to the C++ instance. This is how LuaPlus know which C++ instance to invoke 
the function on. In Lua, light user data is a type that is ignored by the Lua interpreter. It’s a 
raw pointer that is effectively equivalent to a void* in C++. In fact,when you retrieve a 
light user data object in C++, a void* is returned.
Creating this table and binding methods to it are relatively straightforward. You create the 
table as you would any other variable and call RegisterObjectDirect() for each method you 
want to bind.As an example, let’s say you have a very simple class you want to expose to the 
script.
class Ninja
{
	Vec3 m_position;
public:
	void SetPosition(float x, float y, float z);
};
The SetPosition() method is the one you want to expose to the script. Somewhere in the 
initialization code, the metatable needs to be created, and the function needs to be registered.
LuaState* pLuaState;										//assume this is valid
//create the metatable under the global variable name NinjaMetaTable
LuaObject metaTable = pLuaState->GetGlobalVars().CreateTable(“NinjaMetaTable”);
metaTable.SetObject(“__index”, metaTable);		//it’s also its own metatable
//register the SetPosition() function
metaTable.RegisterObjectDirect(“SetPosition”, (Ninja*)0, &Ninja::SetPosition);
The metatable now exists in Lua and has the SetPosition() method bound to it.It can’t be 
called, of course, since it’s missing the instance pointer. When the object itself is created,that 
pointer needs to be bound to a new table, which will serve as the instance of that object in 
Lua. One way to do this is to create a new static method that will instantiate the object, take 
care of the binding, and return the table with the C++ instance pointer bound to it.
class Ninja
{
	Vec3 m_position;
public:
	void SetPosition(float x, float y, float z);
	static LuaObject CreateFromScript(void);						//new function on the Ninja class
};
LuaObject Ninja::CreateFromScript(void)
{
	//create the C++ instance
	Ninja* pCppInstance = new Ninja();
	//create the Lua instance
	LuaObject luaInstance;
	luaInstance.AssignNewTable(pLuaState);
	//assign the C++ instance pointer to the lua instance
	luaInstance.SetLightUserData(“__object”, pCppInstance);
	//assign the metatable to the new Lua instance table
	LuaObject metaTable = pLuaState->GetGlobalVars().GetByName(“NinjaMetaTable”);
	luaInstance.SetMetaTable(metaTable)

	return luaObject;
}
The CreateFromScript() function also needs to be registered to Lua.
LuaObject globals = pLuaState->GetGlobals();
globals.RegisterDirect(“CreateNinja”, &Ninja::CreateFromScript);
Now you can create instances of the Ninja class in Lua and call the SetPosition() function just 
like you would any other Lua object.
ninja = CreateNinja()
ninja:SetPosition(10, 20, 30)
These two methods of function and object registration form the basis of the glue between 
C++ and Lua.
*/

/*
Typing lua starts the Lua interpreter.

Lua sees that the first line you typed was incomplete, so it printed >>. This is a continuation 
prompt, Lua’s way of letting you know that it’s waiting for you to finish what you started.

You can also write numbers using scientific notation, where the part before the upper- or 
lowercase e is multiplied by 10 to the power after the e. For example, 5e2 means 5 times 10 
to the 2nd power, or 500.
Scientific notation is normally used to write extremely large or extremely small numbers.

There are three ways to get out of the interpreter:
Press Ctrl+C. This sends the interpreter what is called an interrupt signal. If the interpreter is
waiting for you to type something, this will exit the interpreter. If the interpreter is stuck, doing
something (or nothing) over and over without stopping, then this same key combination will
get you back to the state where the interpreter is waiting for you to type something.
Press Ctrl+Z (or on Unix-like platforms, including Mac OS X, press Ctrl+D) at the beginning of
an empty line and press Enter. This sends an end-of-file (EOF). It’s a signal to Lua that it should
give up trying to get input from you.
The most typing-intensive way to stop the interpreter is to type the following line:
os.exit()

On most systems, the highest and lowest possible numbers have more than 300 digits (when written in
regular base-10 notation).

Lua’s reserved
words or keywords are and, break, do, else, elseif, end, false, for, function, if, in, local,
nil, not, or, repeat, return, then, true, until, and while.

You can use multiple assignment to swap,without multiple assignment, swapping the contents of two 
variables would require a third variable for temporary storage.

There are three ways to quote strings: with double quotes, with single quotes, and with square brackets.
Single quotes work exactly like double quotes except that a single-quoted string can contain a double
quote.Similarly, a double-quoted string can
contain a single quote.
If you mark the beginning of a string with two open square brackets ([[), then you mark the end with
two close square brackets (]]). Inside such a string, no characters have any special meaning — a double
quote represents a double quote, a newline (the invisible character that marks the end of a line, as when
you press Enter) represents a newline, a backslash represents a backslash, a backslash followed by a letter 
n represents a backslash followed by a letter n, and so on. Strings quoted this way are sometimes
called long strings, because they can be spread out over several lines, but they work fine on one line too.
If the first character of a square-bracket string is a newline, it is ignored. This allows you to write multiline 
square-bracket strings in an easier-to-read way. The following two prints print exactly the same
thing, but the second looks more like its result:
> print([[+-----+
>> | Lua |
>> +-----+
>> ]])
+-----+
| Lua |
+-----+
> print([[
>> +-----+
>> | Lua |
>> +-----+
>> ]])
+-----+
| Lua |
+-----+
What if you wanted to print two close square brackets inside a long string? For example:
> print([[Here ]] are some square brackets.]])
stdin:1: ‘)’ expected near ‘are’
The square brackets are interpreted by Lua as the end of the string. A backslash(反斜杠) won’t 
prevent this from happening, because backslashes have no special meaning inside square-bracket 
strings. Instead, put an equal sign between the string’s opening square brackets, and another 
between the closing square brackets:
> print([=[Here ]] are some square brackets.]=])
Here ]] are some square brackets.
If you need to, you can use multiple equal signs — the same number at the beginning and the end.

Backslash Escaping(反斜杠转义):
You can use the backslash character (\)inside double- and single-quoted strings to do things.
When a double quote is preceded by a backslash, the double quote’s end-of-string meaning is taken
away (escaped), and it becomes a part of the string. The backslash itself is not part of the string.
In other words, a backslash followed by a double quote represents a double quote. In the same way, a
backslash followed by a single quote represents a single quote.
A backslash followed by a backslash represents a backslash (just one):
> print(“1 backslash: \\ 2: \\\\ 3: \\\\\\”)
1 backslash: \ 2: \\ 3: \\\
And a backslash followed by a newline represents a newline.

Escape		Sequence Meaning
\a		Bell (in certain circumstances, printing this character causes the computer to beep — a stands for “alert”)
\b		Backspace
\f		Formfeed
\n		Newline
\r		Carriage return (some operating systems use this by itself or in combination with newline to represent the end of a line)
\t		Tab (used to format text in columns)
\v		Vertical tab
\\		Backslash
\”		Double quote
\’		Single quote (apostrophe)
\99		The character whose numeric representation is 99
\		Newline(a backslash followed by a literal newline represents a newline)
A backslash followed by one, two, or three decimal digits represents the character whose numeric representation 
(inside the computer’s memory or on a disk) is that number. This varies from system to system, but on most 
systems “\99” is the same as “c”.
Lua’s escape sequences are similar to those of the C language. The most important difference is that
numeric escapes like \123 are interpreted as decimal (base 10), not octal (base 8).
Lua strings can include any character, including the null byte: the (invisible) character whose numeric
representation is 0. However, parts of Lua that depend on the C language to handle strings will consider
that character to mark the end of the string. For example:
> EmbeddedNull = “BEFORE\0after”
> print(EmbeddedNull)
BEFORE

The absence(缺席) of a character is considered less than any character, as these examples illustrate:
> print(“” < “A”)
true
> print(“” < “\0”)
true
> print(“abc” < “abcd”)
true

== and ~= can be used with values of any type, even values of two different types.

The value nil,its type is also named nil; in fact it is the only value whose type is nil. It is used
mainly to represent the absence of any other value. Any uninitialized variable has nil as its value.

> A, B, C, D = 1, 2
acts just as though it had been written like this:
> A, B, C, D = 1, 2, nil, nil
This means that extra variables (C and D in this example) are set to nil, even if they already have 
been set to another value.

The .. operator creates a new string by putting its two operands together with no space between them.
Concatenating a string to the empty string gives the same string:
> print(“abcd” .. “” == “abcd”)
true

“\n” is a single character,even though it is typed as two.
UTF-8,in which some characters take up more than one byte.
the following word looks like it’s four characters long, but Lua sees it as five:
> print(#”fi_o”)
5
Lua counts the null byte just like any other character:
> NullByte = “\0”
> NullBytes = NullByte .. NullByte .. NullByte
> print(#NullByte)
1
> print(#NullBytes)
3

As with division by zero, using zero as the second operand of % is undefined.

The following chart lists all of Lua’s operators, from highest precedence to lowest. (All operators are
binary unless marked as unary, and all binary operators are left associative unless marked as right
associative.)
^ (exponentiation; right-associative)
not (Boolean negation; unary), - (unary minus), # (length; unary)
* (multiplication), / (division), % (modulo)
+ (addition), - (subtraction)
.. (string concatenation; right-associative)
< (less than), > (greater than), ~= (not equal to), <= (less than or equal to), == (equal to), >= (greater
than or equal to)
and (Boolean “multiplication”)
or (Boolean “addition”)

An expression is something that has a value. As you’ve used print, you’ve been putting expressions
(separated by commas when there is more than one) between its parentheses. An expression can be a 
literal value, such as a quoted string, true, false, or nil. Literal means that you can tell just by 
looking at it what it is. For example, a variable named Count may contain a number (and probably 
does, given its name), but it’s not a literal number, like 42 is.
An expression can also be a variable name, like Count. It can also be an expression preceded by a unary
operator, an expression wrapped in parentheses, or two expressions separated by a binary operator. For
example, -Count is an expression because Count is an expression, and an expression preceded by a
unary operator is also an expression. 2 + 3 is an expression because 2 and 3 are expressions, and separating 
two expressions with a binary operator makes another expression.

A statement is the smallest complete unit of Lua code. Lua doesn’t know what to do with an expression 
unless it’s wrapped in a statement.

The repeat loop is different in three ways from the while loop:
Its expression is tested after its body (the statements between do and end), which means that the
body is always executed at least once.
The sense of the test is reversed — the while loop keeps going while the expression is true; the
repeat loop, whose expression comes after the keyword until, keeps going until the expression is true.
A repeat loop, unlike a while (or any other compound statement for that matter), does not end
with the keyword end.

A break must be the last statement in a block. A block is a group of statements between the following:
do and end
repeat and until
then and elseif, else, or end
elseif and end
else and end
The following example demonstrates the error caused by trying to use break as something other than the 
last statement in a block:
> while true do
>> break
>> print(“Never reached”)
stdin:3: ‘end’ expected (to close ‘while’ at line 1) near ‘print’
This limitation is not a hardship, because even if you could put statements after a break, they would
never get executed. During debugging, however, it can be convenient to break out of the middle of a
block, and you can do this by wrapping the break in its own block. Lua’s final compound statement, the
do block, is useful for this, as you can see in the following example:
> for N = 1, 10 do
>> print(“Before”)
>> do break end
>> print(“After”)
>> end
Before
>
The do block has the following form:
do
zero or more statements
end
It can also be used to force code typed directly into the interpreter to be executed as one unit.

If a function is called with fewer arguments than it was written for, the remaining arguments are (inside
the function) set to nil as follows:
> function Print2Args(Arg1, Arg2)
>> print(Arg1, Arg2)
>> end
> Print2Args(“1st argument”, “2nd argument”)
1st argument 2nd argument
> Print2Args(“1st argument”)
1st argument nil
> Print2Args()
nil nil

break exits the innermost loop; return exits the innermost function.
Like break, return must be the last statement in a block.
A chunk is a piece of code executed as a unit, such as the following:
A complete file.
A single line typed into the Lua interpreter without causing the continuation prompt to appear.
Multiple lines typed into the Lua interpreter, of which all but the last cause the continuation
prompt to appear.
Again as with break, if you want (for debugging purposes) to return from the middle of a block, just use
a dummy do block like this:
> function ReturnFromMiddle()
>> print(“Does get printed”)
>> do return true end
>> print(“Doesn’t get printed”)
>> end
> ReturnFromMiddle()
Does get printed
>

Not having a return statement at the end of a function has exactly the same effect as having a return
that returns no values.

Adjusting Value Lists:
You can use a function that returns multiple values as the first and only argument to
another function (in which case it’s as though the function was called with multiple arguments) or as the
entire right-hand side of an assignment (in which case it’s as though multiple values were assigned).
What if you use a function that returns multiple values as one of several arguments to another function,
or as only part of the right side of an assignment? As you know, if you call a function with more than
one argument, then the arguments are separated with commas. The same applies to assignment statements 
that assign more than one value and return statements that return more than one value. These
lists of zero or more (in the case of functions and return) or one or more (in the case of assignment) values 
are called value lists.

> function ReturnArgs(Arg1, Arg2, Arg3)
>> return Arg1, Arg2, Arg3
>> end
> print(ReturnArgs(1, 2, 3))
1 2 3
Using Multiple-Valued Functions in Value Lists:
Here’s how multiple-valued functions work in various positions in value lists of various lengths, using
the ReturnArgs function defined in the previous section:
> print(1, ReturnArgs(“a”, “b”, “c”))
1 a b c
> print(ReturnArgs(1, 2, 3), “a”)
1 a
> print(ReturnArgs(1, 2, 3), ReturnArgs(“a”, “b”, “c”))
1 a b c
The rule that Lua follows it this:
If a function call returning multiple values is the last (or only) expression in a value
list, then all the function’s return values are used. If a function call returning multiple values is in a 
value list but is not the last expression, then only its first return value is used; its remaining return 
values are discarded.
Although these examples were given using the print function, the value lists in return and assignment 
statements work the same way:
> function Test()
>> return ReturnArgs(1, 2, 3), ReturnArgs(4, 5, 6)
>> end
> print(Test())
1 4 5 6
> A, B, C, D = ReturnArgs(1, 2, 3), ReturnArgs(4, 5, 6)
> print(A, B, C, D)
1 4 5 6
Using Valueless Functions in Value Lists:
What about functions that return no values, such as the following:
> function DoNothing()
>> end
> print(1, DoNothing())
1>
print(DoNothing(), 2)
nil 2
> print(DoNothing(), DoNothing())
nil
This is just an application of the same rule: When the call to DoNothing is the last expression in the
value list, no adjustment is made and no corresponding value is passed to print, and when the call to
DoNothing is not the last expression in the value list, it is adjusted from no values to one value, namely
nil. Here’s the rule rephrased to cover functions that return no values:
If a function call is the last (or only) expression in a value list, then all (if any) values
returned by the function are used. If a function call is in a value list but is not the
last expression, then its first return value (or nil, if it returns nothing) is used and
any remaining return values are discarded.

To force a function call at the end of a value list to be adjusted to one value, surround the whole function
call (including its parentheses) with parentheses:
> print(“a”, (ReturnArgs(“b”, “c”, “d”)))
a b
> print(“a”, (DoNothing()))
a nil

A chunk was defined as a piece of code executed as a unit. The simplest example is
code typed into the interpreter. When you enter a line into the interpreter, it checks whether you typed
any complete statements. If so, it executes those statements as a chunk. If not, it prints a continuation
prompt so you can finish. After it has amassed a whole number of statements, it compiles (converts) them
into bytecode — an internal representation much more efficient than the text that you type in (which is
also known as source code).

> return nil, “Hello”, nil
nil Hello nil
When the interpreter sees a
chunk that starts with an equal sign, it replaces the equal sign with “return “ before compiling the
chunk like this:
> =nil, “Hello”, nil
nil Hello nil

When lua is started with a filename, it loads that file as a chunk and executes it. As with any chunk, a
file can be returned from it, and values can be returned from it.

On Unix-like platforms this is done by making its first line something like #!/usr/local/bin/lua or
#!/usr/bin/env lua, and marking the file as executable with chmod a+x followed by the file’s
name. (If Lua saw #! in the middle of a script, it would complain, but if it sees # as the very first 
character, it just skips to the second line.)
There are several ways to do it on Windows. Ussing hello.lua as an example, one method is to make
a file called hello.cmd that consists of a line like this:
@lua “C:\Your\Dirs\Here\hello.lua” %*
Or if that doesn’t work because you are running an earlier version of Windows, you can use the 
following method, which limits the number of command-line arguments to nine (replace
“C:\Your\Dirs\Here\” with the full path to the Lua file):
@lua “C:\Your\Dirs\Here\hello.lua” %1 %2 %3 %4 %5 %6 %7 %8 %9
The .cmd file must be in a directory in your system’s search path(必须把cmd文件的所在路径添加到环境变量Path里面).
After you do this, typing hello will run the file(在运行中输入cmd文件的文件名，无需输入cmd后缀，然后回车就可以了).

That’s demonstrated by the following example, which also shows that loadstring takes an optional
second argument — a string used as a name for the chunk in any error messages:
> Fnc, ErrStr = loadstring(“print(2 + + 2)”, “A STRING CHUNK”)
> if Fnc then
>> Fnc()
>> else
>> print(ErrStr)
>> end
[string “A STRING CHUNK”]:1: unexpected symbol near ‘+’

Like all chunks, a chunk compiled by loadstring has no access to local variables from other chunks.
For example:
> Test = “global”
> do
>> local Test = “local”
>> Fnc = loadstring(“print(Test)”)
>> Fnc() -- This prints Test’s global value.
>> end
global

Lua keeps a stack of information about all currently running functions. It’s called a stack because things
are only put onto or taken off of the top. When a function is called, information about it is put on the top
of the stack (making the stack taller), and when a function returns, that function’s information is
removed from the top of the stack (making the stack shorter). Because this stack grows every time a
function is called, it’s called the call stack. The information about one function call is said to occupy one
stack frame.

If the call stack gets too tall, it can run out of space. This is called stack overflow, and it is an error.

For most purposes, you are not likely to run out of stack space if your program isn’t buggy. Stack overflow 
is usually a sign of an infinite recursion, as the previous example shows. If you do want to keep the
stack from growing unnecessarily, there are two ways to do it. One is to use iteration (a loop) instead of
recursion, like this:
-- Returns the factorial of N (iteratively):
function Fact(N)
local Ret = 1
for I = 1, N do
Ret = Ret * I
end
return Ret
end
The other way is to make sure that a recursive call is a tail call.

Tail Calls:
In the recursive version of Fact, there’s still more work to be done after the recursive call returns. You
need to multiply the recursive call’s result by N, and assign that value to Ret, which you must then
return. Here’s how:
-- Returns the factorial of N:
function Fact(N)
local Ret
if N == 0 then
-- Base case:
Ret = 1
else
-- Recursive case:
Ret = N * Fact(N - 1)
end
return Ret
end
If there were nothing left to do but return the recursive call’s result after it returned, you wouldn’t need
to make a new stack frame for it. Instead, you could overwrite the current function’s stack frame (the
information in it no longer being necessary).
This type of function call whose result is immediately returned by the calling function is called a tail call.
When Lua sees a tail call, it does the preceding optimization, reusing the calling function’s stack frame
rather than making a new one. Therefore, the following function will run forever (or until interrupted),
continually calling itself but never consuming more than one stack frame:
function ForeverTail()
return ForeverTail()
end
When you do interrupt it, it looks like the stack is big, but it isn’t really. Lua keeps track of how many
tail calls have happened and shows them in the traceback, but they don’t take up any space in the actual
call stack:
> ForeverTail()
stdin:2: interrupted!
stack traceback:
stdin:2: in function ‘ForeverTail’
stdin:2: in function <stdin:1>
(tail call): ?
(tail call): ?
(tail call): ?
(tail call): ?
(tail call): ?
(tail call): ?
(tail call): ?
(tail call): ?
...
(tail call): ?
(tail call): ?
(tail call): ?
(tail call): ?
(tail call): ?
(tail call): ?
(tail call): ?
(tail call): ?
stdin:1: in main chunk
[C]: ?
In the following function, it may seem that there is nothing left to do after the call, and that the call is
therefore a tail call:
function ForeverNotTail()
ForeverNotTail() -- Is this a tail call?
end
This is not a tail call, though, because there is something left to do before returning: the list of
ForeverNotTail return values must be adjusted to zero. Therefore, ForeverNotTail, unlike
ForeverTail, will overflow the stack.

A tail call is always a return statement whose expression is a single function call.

None of the following are tail calls — the first because there’s no return, the rest because the return’s
expression is something other than a single function call:
Fun()
return Fun() + 1
return X and Fun()
return (Fun()) -- This expression is a single function call
-- surrounded in parentheses, which is different than the
-- required single function call (the parentheses adjust
-- Fun to one return value).
return Fun(), Fun()

A recursive function that uses a tail call to call itself is said to be tail recursive. The recursive version of
Fact is not tail recursive, but you can rewrite it to be tail recursive by introducing an accumulator — a
variable that keeps track of all the multiplications done so far:
-- Returns the factorial of N (tail-recursively). Calls
-- itself with two arguments, but when you call it, you need
-- supply only one argument (like the other Fact functions).
function Fact(N, Acc)
-- Initialize the accumulator to 1:
Acc = Acc or 1
if N == 0 then
-- Base case:
return Acc
else
-- Recursive case:
return Fact(N - 1, N * Acc)
end
end

Functions as Values:
Functions are values (as are numbers, strings, Booleans, and nil). For example, take a look at the following:
print(type(“Hello”))
This operates by looking in the global variable type, finding a function there, calling that function with
the value “Hello”, looking in the global variable print, finding a function there, and calling that function with 
the value returned by the function in type.
If you call type with a function as an argument, it returns the string “function”:
> print(type(print))
function

Comparing and Printing Functions:
You can compare functions for equality (or inequality), like this:
> print = RealPrint
> print(print == RealPrint)
true
> print(print == type)
false
If you print a function (or convert it to a string with tostring), it will appear as the word function, followed 
by a colon, a space, and a number (a hexadecimal number on most systems):
> print(print)
function: 0x481720
The only thing you need to know about this number is that two different functions that exist at the same
time will have different numbers.
Under the hood(在底层), this number represents the function’s location in memory.

You can use a function expression wherever you can use any other expression. You can print it, pass it to
another function, assign it, compare it, and so on. In the following example, function() end is a function 
expression representing a function that takes no arguments, does nothing, and returns nothing:
> print(function() end)
function: 0x493888
> print(type(function() end))
function
As this example shows, you do not need to give a function a name. A function without a name is called
an anonymous function. You can call an anonymous function by wrapping the function expression that
created it in parentheses (and following that with the usual parentheses used in function calls), like this:
> (function(A, B)
>> print(A + B)
>> end)(2, 3)
5

Every time a function expression is evaluated, a new function is created. That’s why DoNothing1 and
DoNothing2 are not equal in the following example, even though the expressions that created them look
alike:
> DoNothing1, DoNothing2 = function() end, function() end
> print(DoNothing1, DoNothing2)
function: 0x493f20 function: 0x493f38
> print(DoNothing1 == DoNothing2)
false
For the same reason, both times MakeDoNothing is called in the following example, it returns a different
function, even though each of those functions is created by literally the same function expression:
> -- Returns a do-nothing function:
> function MakeDoNothing()
>> return function() end
>> end
>>
print(MakeDoNothing() == MakeDoNothing())
false
Two different function expressions or function statements will always create two
different functions, even if the text of the expressions or statements is the same. A
single function expression or statement will create two different functions if it is
executed twice.

Local Functions:
You can also assign functions to local variables. This can be done either with a function expression, like
this:
> do
>> local LclAverage = function(Num1, Num2)
>> return (Num1 + Num2) / 2
>> end
>> print(LclAverage(10, 20))
>> end
15
> -- This will print the global variable LclAverage, which
> -- will be nil:
> print(LclAverage)
nil
Or with a local form of the function statement, like this:
> do
>> local function LclAverage(Num1, Num2)
>> return (Num1 + Num2) / 2
>> end
>> print(LclAverage(10, 20))
>> end
15
> -- This will print the global variable LclAverage, which
> -- will be nil:
> print(LclAverage)
nil
Because they can be hidden from the rest of a program, local functions have the same benefits as local
variables of other types. More specifically, a function is a good candidate for localization if it only makes
sense in one small part of a program — especially if it’s a closure that will be recreated several times as a
program runs.
Another use of local functions is to speed up time-critical loops. This works because access to local variables 
is faster than access to global variables. For example, if you use the (global) type function inside a
loop that needed to run as quickly as possible, you could precede(先于) the loop with the following:
local type = type

Because the scope of a variable created by a local statement starts on the next statement, creating a
recursive local function like the following will not work because the F referred to inside the function is a
global variable (or possibly a local in a containing scope):
local F = function()
code that does something or other
F() -- A failed attempt at a recursive call -- the local F
-- is not visible here.
more code that does something or other
end
Instead, you need to create the local first, and then assign the function:
local F
F = function()
something or other
F() -- This really is a recursive call.
more something or other
end

Upvalues and Closures:
If the same function statement or expression is executed twice, it creates two different functions.The reason for this is
that two functions created by the same source code can act differently from each other if they have upvalues and are
therefore closures.

Defining Functions that Create Functions:
> -- Returns a function that tests whether a number is
> -- less than N:
> function MakeLessThan(N)
>> return function(X)
>> return X < N
>> end
>> end
>>
LessThanFive = MakeLessThan(5)
> LessThanTen = MakeLessThan(10)
> print(LessThanFive(4))
true
> print(LessThanTen(4))
true
> print(LessThanFive(5))
false
> print(LessThanTen(5))
true
> print(LessThanFive(9))
false
> print(LessThanTen(9))
true
> print(LessThanFive(10))
false
> print(LessThanTen(10))
false
Remember that when you call a function, a new local variable is created for each of its arguments. So
when MakeLessThan is called with 5 as an argument, a local variable N is created and initialized to 5.
Normally, this N would no longer be visible after MakeLessThan returns, but because there’s a function
(the anonymous one after return) in N’s scope that uses N, N will last as long as the function does.
MakeLessThan returns the function and assigns it to LessThanFive.
Next, MakeLessThan is called with 10 as an argument. At this point, a local variable N is created and initialized to 10.
This is a newly created variable — it’s different from the other N created for the previous
call. MakeLessThan returns a function that uses this new N, and this function is assigned to LessThan Ten.
When a function uses a variable that is local to a containing scope (such as N in this example), that variable
is called an external local variable or an upvalue. The term “upvalue” is somewhat misleading, because an
upvalue is not a value, but a variable used in a certain context.A function that has one or
more upvalues is called a closure. (All functions, even those with no upvalues, are represented in the same
way internally. For this reason, “closure” is sometimes used as a synonym(同义词) for “function.”)
You can call a function returned by another function directly, without giving it a name first. In the following example,
MakeLessThan is called with 10 as an argument. The function it returns is then called
with 5 as an argument. Because 5 is less than 10, true is printed:
> print(MakeLessThan(10)(5))
true
Two closures can share an upvalue, and upvalues can be assigned to. Both of these facts are demonstrated by the following example.

Defining Functions with Private State:
The following MakeGetAndInc example makes and returns two functions: one that gets a value, and
another that increments that value. These functions have private state — ”state” because there’s changeable data that’s 
remembered in between calls, and “private” because this data is stored in a local variable
visible to only these functions.
> -- Returns two functions: a function that gets N’s value,
> -- and a function that increments N by its argument.
> function MakeGetAndInc(N)
>> -- Returns N:
>> local function Get()
>> return N
>> end
>>
>> -- Increments N by M:
>> local function Inc(M)
>> N = N + M
>> end
>>
>> return Get, Inc
>> end
>>
-- Make two pairs of get and increment functions, one
> -- pair initialized to 0 and the other initialized to 100:
> GetA, IncA = MakeGetAndInc(0)
> GetB, IncB = MakeGetAndInc(100)
> -- Try them out:
> print(GetA())
0>
print(GetB())
100
> IncA(5)
> print(GetA())
5>
IncA(5)
> print(GetA())
10
> IncB(1)
> print(GetB())
101
> IncA(1)
> print(GetA())
11
As you can see, GetA and IncA both refer to the same N, but GetB and IncB both refer to another N.
GetA and IncA refer to the N created by the first call to MakeGetAndInc. The initial value of this N is 0,
but it gets a new value every time IncA is called, and that new value is visible to GetA. GetB and IncB
act the same way, except their value is stored in the N created by the second call to MakeGetAndInc.

Figuring Out Tricky Scope Situations:
Every iteration of a for loop creates a new local loop variable. This is demonstrated by the following
example:
> for I = 1, 2 do
>> if I == 1 then
>> function One()
>> return I
>> end
>> else
>> function Two()
>> return I
>> end
>> end
>> end
> print(One())
1>
print(Two())
2
Both One and Two return variables named I, but they are different variables — if they were the same I,
then One and Two would return the same value.
If you are ever in doubt about the scope of a variable, start from the statement where the variable’s name
is used and search upwards for the following:
A local statement that creates a variable of this name, or a local function statement that creates a function of this name
A function definition (a function statement, local function statement, or function expression) that uses this name as a formal argument
A for loop that uses this name as a loop variable
If your search hits the top of the file without finding anything, the variable is global.

In the following example, PrintStr prints “Inside first do block” even though the call to it is in the
scope of a local Str whose value is “Inside second do block”:
> do
>> local Str = “Inside first do block”
>>
>> function PrintStr()
>> print(Str)
>> end
>> end
> do
>> local Str = “Inside second do block”
>> PrintStr()
>> end
Inside first do block
It doesn’t matter where the call is. What matters is where Str is actually named, and that’s inside the
definition of PrintStr (which is inside the first do block). A function cannot see local variables from the
scope that called it (unless that happens to be the same scope in which it’s defined). This means that you
can tell everything you need to know about a variable’s scope by looking at the program’s source code,
without having to figure out which functions call which. In the same way, Lua itself can tell everything it
needs to know about a variable’s scope when it compiles source code into bytecode, rather than having
to wait till runtime (when the program is running).

Because variable scope is determined by the structure of a program’s source code, Lua is said to have
lexical scope (The term “lexical” here means “based on source code.”)

Each time a local or local function statement is executed, a new local variable (or possibly
several, in the case of local) is created. Its scope extends downward to the end of the innermost enclosing block.
Each time a for loop iterates, a new local variable (or possibly several, in the case of the generic
for loop) is created. Its scope extends to the end of the loop.
Each time a function is called, a new local variable is created for each formal argument. The
scope of these variables extends to the end of the function.
If a variable was not created in any of the three aforementioned ways, it’s global.
Each time a function or local function statement is executed, or a function expression is
evaluated, a new function is created.
There is no limitation on reading from or assigning to a visible local variable from within a function.
(In other words, closures are possible.)

So if you find yourself not understanding why a program is doing what it’s doing, and you think it may
be a scoping issue, ask yourself these questions:
Is this a global or local variable?
Where and when was this local variable created?
Where and when was this function created?

If an expression inside square brackets is a function call, it is adjusted to one value.

The value of a particular index of a particular table is often called a field, and the index itself is called a
field name. This terminology is used especially when the field name is a valid identifier. If a field name is
a valid identifier, you can use it in a table constructor without the square brackets or quotes. The following 
is another way to write the constructor for NameToInstr:
NameToInstr = {John = “rhythm guitar”,
Paul = “bass guitar”,
George = “lead guitar”,
Ringo = “drumkit”}

You can index a table within a table in one step, as follows:
> Tbl1 = {Tbl2 = {Bool = true}}
> print(Tbl1.Tbl2.Bool)
true
> print(Tbl1[“Tbl2”].Bool)
true
> print(Tbl1.Tbl2[“Bool”])
true
> print(Tbl1[“Tbl2”][“Bool”])
true

Remember that NameToInstr[“John”] and NameToInstr.John both mean “get the value for the
“John” key,” and NameToInstr[John] means “get the value for whatever key is in the variable John.”

> T = {A = “x”, “one”, B = “y”, “two”, C = “z”, “three”}
> print(T[1], T[2], T[3])
one two three

If a function call is used as the value of an explicit key ({K = F()}, for example), it’s adjusted to one
return value. If it’s used as the value of an implicit integer key, it’s only adjusted to one return value if
it’s not the last thing in the table constructor; if it is the last thing, no adjustment is made:
> function ReturnNothing()
>> end
>>
function ReturnThreeVals()
>> return “x”, “y”, “z”
>> end
>>
TblA = {ReturnThreeVals(), ReturnThreeVals()}
> print(TblA[1], TblA[2], TblA[3], TblA[4])
x x y z
> TblB = {ReturnNothing(), ReturnThreeVals()}
> -- The following nil is the result of adjustment:
> print(TblB[1], TblB[2], TblB[3], TblB[4])
nil x y z
> TblC = {ReturnThreeVals(), ReturnNothing()}
> -- The following three nils are not the result of adjustment;
> -- they’re there because TblC[2] through TblC[4] were not
> -- given values in the constructor:
> print(TblC[1], TblC[2], TblC[3], TblC[4])
x nil nil nil
> TblD = {ReturnNothing(), ReturnNothing()}
> -- The first nil that follows is the result of adjustment; the
> -- second is there because TblD[2] was not given a value
> -- in the constructor:
> print(TblD[1], TblD[2])
nil nil

Array Length:
The # (length) operator can be used to measure the length of an array. Normally, this number is also the
index of the last element in the array, as in the following example:
> Empty = {}
> One = {“a”}
> Three = {“a”, “b”, “c”}
> print(#Empty, #One, #Three)
0 1 3
Noninteger indexes (or nonpositive integer indexes, for that matter) do not count — the length operator
measures the length of a table as an array as follows:
> Two = {“a”, “b”, Ignored = true, [0.5] = true}
> print(#Two)
2
Arrays with gaps cause the length operator to behave unpredictably(不可预见的).

Using ipairs to Loop through an Array:
> Squares = {}
> for I = 1, 5 do							--numeric for loop
>> Squares[I] = I ^ 2
>> end
> for I, Square in ipairs(Squares) do						--generic for loop
>> print(I .. “ squared is “ .. Square)
>> end
1 squared is 1
2 squared is 4
3 squared is 9
4 squared is 16
5 squared is 25
for I, Square in ipairs(Squares) do
This line means “loop (in order) through each key-value pair in the array Squares, assigning the key
and value to (respectively) the loop variables I and Square.”

If you use a generic for loop with ipairs to loop through an array that has gaps, it will stop when it
reaches the first gap, as follows:
> for Number, Word in ipairs({“one”, “two”, nil, “four”}) do
>> print(Number, Word)
>> end
1 one
2 two

pairs:
The key-value pairs are looped through in an arbitrary order.

Like the loop variable in a numeric for, the loop variables in a generic for are local to each iteration.

Adding a previously nonexistent key to a table while looping over it with pairs has undefined results.

You can use loop variables as upvalues to closures, each iteration means a new upvalue:
> -- A table that maps numbers to their English names:
> Numbers = {“one”, “two”, “three”}
> -- A table that will contain functions:
> PrependNumber = {}
> for Num, NumName in ipairs(Numbers) do
>> -- Add a function to PrependNumber that prepends NumName
>> -- to its argument:
>> PrependNumber[Num] = function(Str)
>> return NumName .. “: “ .. Str
>> end
>> end
> -- Call the second and third functions in PrependNumber:
> print(PrependNumber[2](“is company”))
two: is company
> print(PrependNumber[3](“is a crowd”))
three: is a crowd

> NamesAndInstrs = {
>> {Name = “John”, Instr = “rhythm guitar”},
>> {Name = “Paul”, Instr = “bass guitar”},
>> {Name = “George”, Instr = “lead guitar”},
>> {Name = “Ringo”, Instr = “drumkit”}}
> for _, NameInstr in ipairs(NamesAndInstrs) do
>> print(NameInstr.Name .. “ played “ .. NameInstr.Instr)
>> end
John played rhythm guitar
Paul played bass guitar
George played lead guitar
Ringo played drumkit

Tables of Functions:
Using tables that contain functions is a handy way to organize functions, and Lua keeps many of its
built-in functions in tables, indexed by strings.

table.sort:
One function in the table library is table.sort. Here is an example of how you use this function:
> Names = {“Scarlatti”, “Telemann”, “Corelli”, “Purcell”,
>> “Vivaldi”, “Handel”, “Bach”}
> table.sort(Names)
> for I, Name in ipairs(Names) do
>> print(I, Name)
>> end
1 Bach
2 Corelli
3 Handel
4 Purcell
5 Scarlatti
6 Telemann
7 Vivaldi
The table.sort function takes an array and sorts it in place. This means that, rather than returning a
new array that’s a sorted version of the one given to it, table.sort uses indexing assignment (a side
effect) on the given array itself to move its values to different keys.
table.sort uses the < operator to decide whether an element of the array should come before another
element. To override this behavior, give a comparison function as a second argument to table.sort. A
comparison function takes two arguments and returns a true result if and only if its first argument
should come before its second argument.
table.sort only looks at a table as an array. It ignores any noninteger keys and any integer keys less
than 1 or greater than the table’s array length. To sort a table that isn’t an array, you need to put its 
contents into an array and sort that array.

Sorting the Contents of an Associative Table:
-- A demonstration of sorting an associative table.
NameToInstr = {John = “rhythm guitar”,
Paul = “bass guitar”,
George = “lead guitar”,
Ringo = “drumkit”}
-- Transfer the associative table NameToInstr to the
-- array Sorted:
Sorted = {}
for Name, Instr in pairs(NameToInstr) do
table.insert(Sorted, {Name = Name, Instr = Instr})
end
-- The comparison function sorts by Name:
table.sort(Sorted, function(A, B) return A.Name < B.Name end)
-- Output:
for _, NameInstr in ipairs(Sorted) do
print(NameInstr.Name .. “ played “ .. NameInstr.Instr)
end

table.concat:
The function table.concat takes an array of strings (or numbers) and concatenates them all into one
string, as follows:
> print(table.concat({“a”, “bc”, “d”}))
abcd
Normally, all elements from the first to the last will be concatenated. To start concatenating at a different
element, give its index as the third argument of table.concat; to stop concatenating at a different element,
give its index as the fourth argument of table.concat:
> Tbl = {“a”, “b”, “c”, “d”}
> -- Concatenate the second through last elements:
> print(table.concat(Tbl, “”, 2))
bcd
> -- Concatenate the second through third elements:
> print(table.concat(Tbl, “”, 2, 3))
bc

table.remove:
The table.insert function has a counterpart that removes elements from an array, table.remove. By default,
both work on the last element of the array (or the top
element when viewing the array as a stack). table.remove works by side effect like table.insert
does, but it also returns a useful value — the element it just removed.

table.maxn:
The function table.maxn looks at every single key-value pair in a table and returns the highest positive
number used as a key, or 0 if there are no positive numbers used as keys.

Object-Oriented Programming with Tables:
Another use for tables is in what is known as object-oriented programming.
-- Returns a table of two functions: a function that gets
-- N’s value, and a function that increments N by its
-- argument.
function MakeGetAndInc(N)
-- Returns N:
local function Get()
return N
end
-- Increments N by M:
local function Inc(M)
N = N + M
end
return {Get = Get, Inc = Inc}
end
An object is created and used like so:
> -- Create an object:
> A = MakeGetAndInc(50)
> -- Try out its methods:
> print(A.Get())
50
> A.Inc(2)
> print(A.Get())
52

> do -- Local scope for Get and Inc.
>> -- Returns Obj.N:
>> local function Get(Obj)
>> return Obj.N
>> end
>>
>> -- Increments Obj.N by M:
>> local function Inc(Obj, M)
>> Obj.N = Obj.N + M
>> end
>>
>> -- Creates an object:
>> function MakeGetAndInc(N)
>> return {N = N, Get = Get, Inc = Inc}
>> end
>> end
>
> -- Create an object:
> A = MakeGetAndInc(50)
> -- Try out its methods:
> print(A:Get())
50
> A:Inc(2)
> print(A:Get())
52
*/