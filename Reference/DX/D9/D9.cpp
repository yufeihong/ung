/*
为了编写和执行DirectX 9.0程序，计算机中需要安装DirectX 9.0运行时(runtime)以及DirectX 9.0 SDK
(Software Development Kit)。
安装DirectX SDK时，运行时会自动安装。
需要链接的库文件：
d3d9.lib,d3dx9.lib
winmm.lib(并非DirectX库文件，它是Windows多媒体库文件)
*/

/*
在Direct3D和图形设备之间有一个中间环节---HAL（Hardware Abstraction Layer，硬件抽象层）。由于市面上的图形卡品种繁多，每种卡的性能和实现同样功能的机理
都有差异，所以Direct3D无法与图形设备直接交互。所以Direct3D就需要设备制造商实现一个HAL。HAL是一个指示设备完成某些操作的设备相关的代码集。按照这种方式，
Direct3D就可不必了解设备的具体细节。设备制造商将其产品所支持的全部功能都实现到HAL中。调用一个没有在HAL中实现的Direct3D函数会导致调用失败，除非它是一
种顶点处理运算，并且用户已指定了使用软件顶点运算方式。在这种情况下用户所期望实现的功能便可由Direct3D运行时以软件运算方式来模拟。
Direct3D提供了参考光栅设备（reference rasterizer device），即REF设备，它能以软件运算方式完全支持Direct3D API。注意，REF设备仅用于开发阶段作为测试，它
与DirectX SDK捆绑在一起，无法发布给最终用户。HAL设备用值D3DDEVTYPE_HAL来指定，REF设备用值D3DDEVTYPE_REF来指定。

COM（Component Object Model，组件对象模型）是一项能够使DirectX独立于编程语言并具备向下兼容的技术。常称COM对象为接口，可将其视为一个C++类来使用。
我们所必须知道的仅仅是如何通过某个特定函数或另一个COM接口的方法来获取指向某一COM接口的指针。创建COM接口时不可使用C++的关键字new。使用完一个接口，
应调用该接口相应的Release方法而不是用C++的delete。COM对象能够对其所使用的内存实施自治。COM接口都有一个前缀I。

表面（IDirect3DSurface9）是Direct3D主要用于存储2D图像数据的一个像素矩阵。（像素数据实际存储在一个线性数组中。）表面的宽度（width）和高度（height）都
用像素来度量。跨度（pitch，stride的同义语）则用字节来度量。pitch可能会比width更“宽”，这要依赖于底层的硬件实现，所以不能简单地假设pitch = width * sizeof(pixelFormat)。
如图：Knowledge/DX/001.png所示。
在代码中，我们用接口IDirect3DSurface9来描述表面。该接口提供了几种直接从表面读取和写入数据的方法，以及一种获取表面相关信息的方法。
接口IDirect3DSurface9中最重要的方法如下：
1，LockRect，该方法用于获取指向表面存储区（surface memory）的指针。通过指针运算，可对表面中的每一个像素进行读写操作。
2，UnlockRect，如果调用了LockRect方法，而且已执行完访问表面存储区的操作，必须调用该方法以解除对表面存储区的锁定。
3，GetDesc，该方法可通过填充结构D3DSURFACE_DESC来获取该表面的描述信息。

多重采样:
用像素矩阵表示图像时往往会出现块状效应，多重采样(multisampling)便是一项用于平滑块状图像的技术。对表面进行多重采样常用于全屏反走样（full-screen antialiasing）。
可以使用IDirect3D9::CheckDeviceMultiSampleType来检查当前图形设备是否支持所希望采用的多重采样类型。

像素格式：
创建表面或纹理时，常常需要指定像素格式。像素格式可以用D3DFORMAT枚举类型的枚举常量来定义。
1，D3DFMT_R8G8B8					每个像素由24位组成。
2，D3DFMT_X8R8G8B8				每个像素由32位组成。最左端8位未加使用。
3，D3DFMT_A8R8G8B8				每个像素由32位组成。
4，D3DFMT_A16B16G16R16F		每个像素由64位组成。是一种浮点数类型的像素格式。
5，D3DFMT_A32B32G32R32F		每个像素由128位组成。是一种浮点数类型的像素格式。

内存池：
1，D3DPOOL_DEFAULT		默认值。该类型的内存池指示Direct3D将资源放入最适合该资源类型及其使用方式的存储区中。该存储区可能是显存（video memory），
										AGP存储区或系统存储区。注意，调用函数IDirect3DDevice9::Reset之前，必须对默认内存池中的资源destroy或release。上述函数调用
										之后，还必须对内存池中的资源重新初始化。
2，D3DPOOL_MANAGE		放入该托管内存池中的资源将交由Direct3D管理（即，这些资源将根据需要被设备自动转移到显存或AGP存储区中）。这些资源将在系统存
										储区中保留一个备份。这样，必要时Direct3D会将这些资源自动更新到显存中。
3，D3DPOOL_SYSTEMMEM	指定将资源放入系统存储区中。
4，D3DPOOL_SCRATCH		指定将资源放入系统存储区中。不同于D3DPOOL_SYSTEMMEM，这些资源不受图形设备的制约。所以，设备无法访问该类型内存池中的
										资源。但这些资源之间可互相复制。

交换链:
Direct3D维护着一个表面集合（collection），该集合通常由两到三个表面组成，称为交换链（Swap Chain）。该集合用接口IDirect3DSwapChain9来表示。
交换链是一个或多个后台缓冲区的集合,这些后台缓冲区可以连续地显示到前台缓冲区.每个设备始终至少有一个交换链,该交换链称为隐式交换链.可以创建附加
SwapChain对象,用于呈现同一设备的多个视图。

顶点运算:
使用hardware vertex processing，可以不占用CPU资源，也就意味着CPU可被解放出来进行其他运算。
图形卡支持硬件顶点运算的另一种等价说法是该图形卡支持变换和光照的硬件计算。

设备性能(Device Capabilities):
Direct3D所提供的每一项性能都对应于结构D3DCAPS9中的一个数据成员或某一位。

初始化:
Direct3D的初始化过程可分解为如下步骤：
1，获取IDirect3D9的指针。该接口用于获取系统中物理硬件设备的信息并创建接口IDirect3DDevice9，该接口代表了物理硬件设备。
2，检查设备性能（D3DCAPS9），判断主显卡（primary display adapter或primary graphics card）是否支持硬件顶点运算。为了创建接口IDirect3DDevice9，我们
必须明确显卡是否支持该功能。
3，初始化D3DPRESENT_PARAMETERS结构的一个实例。该结构由许多数据成员组成，我们可以通过这些变量来指定即将创建的接口IDirect3DDevice9的特性。
4，利用已初始化的D3DPRESENT_PARAMETERS结构创建IDirect3DDevice9。
*/

/*
投影平面/投影窗口：
Direct3D定义它与平面z = 1重合。
Direct3D定义矩形投影窗口在X和Y方向上坐标的最小值是-1，最大值是1。
为了简化绘制工作，常将近裁剪面和投影平面合二为一。

一个简化的绘制流水线：
局部坐标系->世界坐标系->观察坐标系->背面消除->光照->裁剪->投影->视口坐标系->光栅化

Direct3D
当摄像机的位置和朝向任意时，投影变换及其他类型的变换就略显困难。为了简化运算，我们将摄像机变换至
世界坐标系的原点，并将其旋转，使摄像机的光轴与世界坐标系的z轴正方向一致。

背面消除：
不是说一个三角形的内表面，而是说某个三角形的外表面法线与摄像机的朝向关系不合适，那么站在摄像机的
角度看，该三角形就符合背面消除。
默认情况下，Direct3D认为顶点排列顺序为顺时针（观察坐标系中）的三角形单元是正面朝向的。

光照：
光源是在世界坐标系中定义的，必须将其变换至观察坐标系中方可使用。

投影：
从n维变换为n - 1维的过程。
投影变换定义了视域体，并负责将视域体中的几何体投影到投影窗口中。

视口变换：
将顶点坐标从投影窗口转换到屏幕的一个矩形区域中，该矩形区域称为视口。
矩形的视口是相对于窗口来描述的，其位置要用窗口坐标来指定。
The viewport transform is responsible for transforming normalized device coordinates(NDC) to
a rectangle on the back buffer call the viewport.
Direct3D将深度缓存的范围设在[0,1]区间内。
可以用下面的矩阵来描述视口变换：
width / 2			0						0							0
0						-height / 2			0							0
0						0						maxZ - minZ			0
x + width / 2		y + height / 2	minZ						1
其中x，y为视口左上角坐标。

光栅化：
After the vertices are transformed to the back buffer,we have a list of 2D triangles in image space
to be processed one by one.The rasterization stage is responsible for computing the colors of the
individual pixels that make up the interiors and boundaries of these triangles.Pixel operation like
texturing,pixel shaders,depth buffering and alpha blending occur in the rasterization stage.
在光栅化过程中，需要对多边形进行着色（shading）。着色规定了如何利用顶点的颜色来计算构成图元的像
素的颜色。
两种着色模式：
平面着色（flat shading）：
每个图元的每个像素都被一致地赋予该图元的第一个顶点所指定的颜色，忽略第二和第三个顶点的颜色。这种
着色模式容易使物体呈现出“块状”，这是因为各颜色之间没有平滑地过度。
Gouraud着色（平滑着色（smooth shading））：
图元中各像素的颜色值由各顶点的颜色经线性插值得到。

*/

/*
顶点缓存与索引缓存：
我们之所以用顶点缓存和索引缓存而非数组来存储数据，是因为顶点缓存和索引缓存可以被放置在显存(video 
memory)中。
*/

/*
Backface Culling:
A polygon has two sides,and we label one side as the front side and the other as the back side.
A polygon whose front side faces the camera is called a front facing polygon,and a polygon 
whose front side faces away from the camera is called a back facing polygon.
Direct3D needs to know which polygons are front facing and which are back facing.By default,
Direct3D treats triangles with vertices specified in a clockwise winding order (in view space.
2D triangles in screen space will have the same wind order as 3D triangles in view space) as 
front facing.Triangles with vertices specified in a counterclockwise winding order(in view space) 
are considered back facing.
Notice that we said "in view space."This is because when a triangle is rotated 180°,its winding 
order is flipped.Thus,a triangle that had a clockwise winding order when it was defined in its 
local space might not have a clockwise winding order when it's transformed to view space due 
to possible rotations.
If for some reason we are not happy with the default culling behavior (e.g.,we need to disable it 
because we want to render transparent objects),we can change it by changing the 
D3DRS_CULLMODE render state.
gd3dDevice->SetRenderState(D3DRS_CULLMODE, Value);
Where Value can be one of the following:
D3DCULL_NONE: Disables backface culling entirely.
D3DCULL_CW: Triangles with a clockwise wind are culled.
D3DCULL_CCW: Triangles with a counterclockwise wind are culled. This is the default state.

Clipping:
we need to cull the geometry that is outside the viewing volume(frustum).This process is called 
clipping.
Note that clipping is done in homogeneous clip space.Let (x/w, y/w, z/w, 1) be a vertex inside 
the frustum after the homogeneous divide.Then the coordinates of this point are bounded as 
follows:
-1 <= x/w <= 1
-1 <= y/w <= 1
0 <= z/w <= 1

Viewport Transform:
The viewport transform is responsible for transforming normalized device coordinates to a 
rectangle on the back buffer called the viewport.
The viewport rectangle is described relative to the back buffer(image coordinates).
Viewports allow us to render to subsections of the back buffer.
In Direct3D a viewport is represented by the D3DVIEWPORT9 structure.It is defined as:
typedef struct _D3DVIEWPORT9
{
	DWORD X;
	DWORD Y;
	DWORD Width;
	DWORD Height;
	DWORD MinZ;
	DWORD MaxZ;
}D3DVIEWPORT9;
The first four data members define the viewport rectangle relative to the window in which it 
resides.The MinZ member specifies the minimum depth buffer value and MaxZ specifies the 
maximum depth buffer value.Direct3D uses a depth buffer range of 0 to 1,so MinZ and MaxZ 
should be set to those values respectively unless a special effect is desired.
Once we have filled out the D3DVIEWPORT9 structure,we set the viewport with Direct3D like so:
D3DVIEWPORT9 vp = { 0, 0, 800, 600, 0, 1 };
Device->SetViewport(&vp);
By default, the viewport is the entire back buffer - so you don't need to set it unless you want 
to change it.
Direct3D handles the viewport transformation for us automatically, but for reference, the viewport transformation is described with the following matrix. The variables
are the same as those described for the D3DVIEWPORT9 structure.
width / 2			0						0							0
0						-height / 2			0							0
0						0						maxZ - minZ			0
x + width / 2		y + height / 2	minZ						1
其中x，y为视口左上角坐标。

Rasterization:
After the vertices are transformed to the back buffer, we have a list of 2D triangles in image 
space to be processed one by one.The rasterization stage is responsible for computing the 
colors of the individual pixels that make up the interiors and boundaries of these triangles.
Pixel operations like texturing,pixel shaders,depth buffering,and alpha blending occur in the 
rasterization stage.
The color values coupled to each vertex are interpolated across the face of the triangle in order 
to color the other pixels that are part of the triangle face.However,other vertex components can 
be interpolated just as well.Recall that we have a depth component associated with each vertex.
these depth values are interpolated across the face of the triangle as well - thus each pixel now 
has its own depth value (and the depth buffering algorithm can be used).Texture coordinates 
are also interpolated across the 2D triangle.We can even interpolate vertex normals,and do 
lighting calculations at the pixel level instead of the vertex level for better accuracy (vertex 
normals are used in lighting calculations).To summarize,interpolation is a way of transferring 
information at a vertex level to the pixel level.
*/

/*
Managing Resources:
Resource management is the process where resources are promoted from system-memory 
storage to device-accessible storage and discarded from device-accessible storage.The Direct3D 
run time has its own management algorithm based on a least-recently-used priority technique.
Direct3D switches to a most-recently-used priority technique when it detects that more 
resources than can coexist in device-accessible memory are used in a single frame - between 
IDirect3DDevice9::BeginScene and IDirect3DDevice9::EndScene calls.

Use the D3DPOOL_DEFAULT flag at creation time to specify that a resource are placed in the 
memory pool most appropriate for the set of usages requested for the given resource.This is 
usually video memory,including both local video memory and accelerated graphics port (AGP) 
memory.Resources in the default pool do not persist through transitions between the lost and 
operational states of the device.These resources must be released before calling reset and must 
then be re-created.

Use the D3DPOOL_MANAGED flag at creation time to specify a managed resource.Managed 
resources persist through transitions between the lost and operational states of the device.
These resources exist both in video and system memory.A managed resource will be copied 
into video memory when it is needed during rendering.The device can be restored with a call to 
IDirect3DDevice9::Reset,and such resources continue to function normally without being 
reloaded with data.However,if the device must be destroyed and re-created,all resources must 
be re-created.

Resource management is not recommended for resources which change with high frequency.For 
example,vertex and index buffers which are used to traverse a scene graph every frame 
rendering only geometry visible to the user changes every frame.Since managed resources are 
backed by system memory,the constant changes must be updated in system memory,which 
can cause a severe degradation in performance.For this particular scenario,D3DPOOL_DEFAULT 
along with D3DUSAGE_DYNAMIC should be used.

Another example for which resource management is not recommended (and explicitly not 
allowed by the runtime) is management of a texture created with D3DUSAGE_RENDERTARGET.
If the memory used by render targets were managed by the runtime,it's contents would have 
to constantly be copied to system memory during rendering,causing large performance 
penalties(造成严重的性能损失).For this reason,render targets must be created in the default pool.
If CPU access of the data contained in a render target is needed,the data must be copied to a 
resource created in D3DPOOL_SYSTEMMEM using IDirect3DDevice9::UpdateTexture,or 
IDirect3DDevice9::UpdateSurface.

Usage and Resource Combinations:
Usages are either specified when a resource is created,or specified with CheckDeviceType to 
test the capability of an existing resource.The following table identifies which usages can be 
applied to which resource types.
Usage													Vertex buffer create		Index buffer create			Texture create		Cube texture create		Volume texture create			Surface create		Check device format
D3DUSAGE_AUTOGENMIPMAP					0									0									1							1									0										0							1
D3DUSAGE_DEPTHSTENCIL						0									0									1							1									0										1							1
D3DUSAGE_DMAP									0									0									1							0									0										0							1
D3DUSAGE_DONOTCLIP							1									1									0							0									0										0							0
D3DUSAGE_DYNAMIC								1									1									1							1									1										0							1
D3DUSAGE_NONSECURE						1									1									1							1									1										1							1
D3DUSAGE_NPATCHES							1									1									0							0									0										0							0
D3DUSAGE_POINTS								1									1									0							0									0										0							0
D3DUSAGE_RTPATCHES							1									1									0							0									0										0							0
D3DUSAGE_RENDERTARGET					0									0									1							1									0										1							1
D3DUSAGE_SOFTWAREPROCESSING		1									1									1							1									1										0							1
D3DUSAGE_TEXTAPI								1									0									1							0									0										0							0
D3DUSAGE_WRITEONLY							1									1									0							0									0										0							0
Use CheckDeviceFormat to check hardware support for these usages.

Using Dynamic Vertex and Index Buffers:
Locking a static vertex buffer while the graphics processor is using the buffer can have a 
significant performance penalty(惩罚).The lock call must wait until the graphics processor is 
finished reading vertex or index data from the buffer before it can return to the calling 
application,a significant delay.Locking and then rendering from a static buffer several times per 
frame also prevents the graphics processor from buffering rendering commands,since it must 
finish commands before returning the lock pointer.Without buffered commands,the graphics 
processor remains idle until after the application is finished filling the vertex buffer or index 
buffer and issues a rendering command.
Ideally the vertex or index data would never change,however this is not always possible.There 
are many situations where the application needs to change vertex or index data every frame,
perhaps even multiple times per frame.For these situations,the vertex or index buffer should be 
created with D3DUSAGE_DYNAMIC.This usage flag causes Direct3D to optimize for frequent 
lock operations.D3DUSAGE_DYNAMIC is only useful when the buffer is locked frequently;data 
that remains constant should be placed in a static vertex or index buffer.
To receive a performance improvement when using dynamic vertex buffers,the application must 
call IDirect3DVertexBuffer9::Lock or IDirect3DIndexBuffer9::Lock with the appropriate flags.
D3DLOCK_DISCARD indicates that the application does not need to keep the old vertex or 
index data in the buffer. If the graphics processor is still using the buffer when lock is called 
with D3DLOCK_DISCARD,a pointer to a new region of memory is returned instead of the old 
buffer data.This allows the graphics processor to continue using the old data while the 
application places data in the new buffer.No additional memory management is required in the 
application;the old buffer is reused or destroyed automatically when the graphics processor is 
finished with it.Note that locking a buffer with D3DLOCK_DISCARD always discards the entire 
buffer,specifying a nonzero offset or limited size field does not preserve(保存，保持) information 
in unlocked areas of the buffer.
There are cases where the amount of data the application needs to store per lock is small,such 
as adding four vertices to render a sprite.D3DLOCK_NOOVERWRITE indicates that the 
application will not overwrite data already in use in the dynamic buffer.The lock call will return 
a pointer to the old data,allowing the application to add new data in unused regions of the 
vertex or index buffer.The application should not modify vertices or indices used in a draw 
operation as they might still be in use by the graphics processor.The application should then 
use D3DLOCK_DISCARD after the dynamic buffer is full to receive a new region of memory,
discarding the old vertex or index data after the graphics processor is finished.
The asynchronous query mechanism(异步查询机制) is useful to determine if vertices are still in 
use by the graphics processor.Issue a query of type D3DQUERYTYPE_EVENT after the last 
DrawPrimitive call that uses the vertices.The vertices are no longer in use when 
IDirect3DQuery9::GetData returns S_OK.Locking a buffer with D3DLOCK_DISCARD or no flags 
will always guarantee the vertices are synchronized properly with the graphics processor,
however using lock without flags will incur the performance penalty described earlier.Other API 
calls such as IDirect3DDevice9::BeginScene,IDirect3DDevice9::EndScene,and 
IDirect3DDevice9::Present do not guarantee the graphics processor is finished using vertices.
//USAGE STYLE 1
//Discard the entire vertex buffer and refill with thousands of vertices.
//Might contain multiple objects and/or require multiple DrawPrimitive calls separated by state 
changes,etc.
//Determine the size of data to be moved into the vertex buffer.
UINT nSizeOfData = nNumberOfVertices * m_nVertexStride;
//Discard and refill the used portion of the vertex buffer.
CONST DWORD dwLockFlags = D3DLOCK_DISCARD;
//Lock the vertex buffer.
BYTE* pBytes;
if(FAILED(m_pVertexBuffer->Lock(0,0,&pBytes,dwLockFlags)))
	return false;
//Copy the vertices into the vertex buffer.
memcpy(pBytes,pVertices,nSizeOfData );
m_pVertexBuffer->Unlock();
//Render the primitives.
m_pDevice->DrawPrimitive(D3DPT_TRIANGLELIST,0,nNumberOfVertices/3)

//USAGE STYLE 2
//Reusing one vertex buffer for multiple objects
//Determine the size of data to be moved into the vertex buffer.
UINT nSizeOfData = nNumberOfVertices * m_nVertexStride;
//No overwrite will be used if the vertices can fit into the space remaining in the vertex buffer.
DWORD dwLockFlags = D3DLOCK_NOOVERWRITE;
//Check to see if the entire vertex buffer has been used up yet.
if(m_nNextVertexData > m_nSizeOfVB - nSizeOfData )
{
	//No space remains.Start over from the beginning of the vertex buffer.
	dwLockFlags = D3DLOCK_DISCARD;
	m_nNextVertexData = 0;
}
//Lock the vertex buffer.
BYTE* pBytes;
if(FAILED(m_pVertexBuffer->Lock((UINT)m_nNextVertexData,nSizeOfData,&pBytes,dwLockFlags)))
	return false;
//Copy the vertices into the vertex buffer.
memcpy(pBytes,pVertices,nSizeOfData);
m_pVertexBuffer->Unlock();
//Render the primitives.
m_pDevice->DrawPrimitive(D3DPT_TRIANGLELIST,m_nNextVertexData/m_nVertexStride,nNumberOfVertices/3)
//Advance to the next position in the vertex buffer.
m_nNextVertexData += nSizeOfData;
*/