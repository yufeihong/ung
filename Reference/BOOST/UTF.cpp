/*
API测试应该组合使用单元测试和集成测试.也可以适当运用非功能性技术测试.

1:白盒测试,测试是在理解源代码的基础上进行的,且通常使用一种编程语言编写.
2:黑盒测试,测试是基于产品说明进行的,而不关心任何实现细节.黑盒测试一般是在最终用户应用上手工执行测试,但是黑盒测试也可以应用于API测试,
即根据API说明编写测试API的代码.
3:灰盒测试,白盒测试和黑盒测试的组合,其中黑盒测试是在获知软件实现细节的基础上进行的.

单元测试:验证软件是否符合程序员预期设计.用于验证一个最小单元(例如独立的方法或者类)的源代码.单元测试的目的是隔离API的最小可测试部分,从而
独立验证它们的功能正确性.单元测试是一种白盒测试技术.
集成测试:验证软件能否满足客户的需求.关注几个组件一起协作时的交互过程.理想情况下,每个独立的组件都已经通过单元测试.集成测试通常根据API文档
进行开发,因此不要求理解内部的实现细节.也就是说,它们是站在客户的角度编写的.集成测试是一种黑盒测试技术.

非功能性测试:
性能测试,验证API的功能满足最低运行速度或最小内存使用的要求.
负载测试,给系统增加需求或者压力,并测试系统处理这些负载的能力,通常指同时有很多并发用户,或者每秒执行很多API请求的测试.有时也称作压力测试.
可扩展性测试,确保系统能够处理庞大的复杂生产数据的输入,而不是仅仅测试数据集.它有时也称作容量测试或者体积测试.
浸泡测试,尝试长期持续地运行软件,以满足客户对软件健壮性的需求,保证软件能够持续使用.
安全性测试,确保代码满足所有的安全性要求,比如保密性、身份认证、授权、完整性和敏感信息的可获得性.
并发测试,验证代码的多线程行为,确保它能够正确执行而不会死锁.
*/

/*
#include "boost/test/unit_test.hpp"
test库将测试程序定义为一个测试模块,由测试安装、测试主体、测试清理和测试运行器四个部分组成.

测试安装:执行测试前的准备工作,初始化测试用例或测试套件所需的数据.

测试主体:是测试模块的实际运行部分,由测试用例和测试套件组织成测试树的形式.
测试用例,是一个包含多个测试断言的函数,它是可以被独立执行测试的最小单元,各个测试用例之间是无关的,发生的错误不会影响到其他测试用例.要添加测试
用例,需要向UTF注册:BOOST_AUTO_TEST_CASE(),宏的参数是测试用例的名字.
测试套件,是测试用例的容器,它包含一个或多个测试用例,可以将繁多的测试用例分组管理,共享安装/清理代码,更好地组织测试用例.测试套件可以嵌套,并且
没有嵌套层数的限制.测试套件的自动使用方式是两个宏BOOST_AUTO_TEST_SUITE()和BOOST_AUTO_TEST_SUITE_END().这两个宏必须成对使用,宏
之间的所有测试用例都属于这个测试套件.一个C++源文件中可以有任意多个测试套件.
任何一个UTF单元测试程序都必须存在一个主测试套件,它是整个测试树的根节点,其他的测试套件都是它的子节点.主测试套件的定义可以使用宏
BOOST_TEST_MAIN或者BOOST_TEST_MODULE.

测试清理:是测试安装的反向操作,执行必要的清理工作.测试安装和测试清理的动作很像C++中的构造函数和析构函数,因此,可以定义一个辅助类,它的构造
函数和析构函数分别执行测试安装和测试清理,之后我们就可以在每个测试用例最开始声明一个对象,它将自动完成测试安装和测试清理.基于这个原理,UTF中
定义了"测试夹具"的概念,它实现了自动的测试安装和测试清理,就像是一个夹在测试用例和测试套件两端的夹子.测试夹具不仅可以用于测试用例,也可以用于
测试套件和单元测试全局.测试夹具通常是个struct,因为它被UTF用于继承.指定测试用例和测试套件的夹具类需要使用两个宏:
BOOST_FIXTURE_TEST_SUITE(suite_name,F),BOOST_FIXTURE_TEST_CASE(test_name,F)它们替代了之前的BOOST_AUTO_TEST_CASE和
BOOST_AUTO_TEST_SUITE,第二个参数指定了要使用的夹具类.可以在测试套件级别指定夹具,这样套件内的所有子套件和测试用例都自动使用夹具类提供
的安装和清理功能,但子套件和测试用例也可以另外指定其他夹具,不会受上层测试套件的影响.全局测试夹具需要使用另一个宏BOOST_GLOBAL_FIXTURE,
它定义的夹具类被应用于整个测试模块的所有测试套件,包括主测试套件.
*/

/*
test库提供了一个用于单元测试的基于命令行界面的测试套件Unit Test Framework，简称UTF。
#include <boost/test/unit_test.hpp>

定义主测试套件，是测试main函数入口
#define BOOST_TEST_MAIN

四个基本的测试断言：
BOOST_CHECK(predicate):
如果不通过，不影响程序执行。
BOOST_REQUIRE(predicate):
如果不通过，则程序停止执行。
BOOST_ERROR(message):
给出一个错误消息，程序继续执行。
BOOST_FAIL(message)
给出一个错误消息，程序停止执行。
这四个基本的测试断言，能够在任何地方使用，但同时为了通用性也不具有其他断言的好处。

test库中的测试断言具有这样的形式：BOOST_XXX_YYY:
XXX:
断言的级别。
WARN是警告级别，不影响程序运行，也不增加错误数量。
CHECK是检查级别，如果断言失败增加错误数量，但不影响程序运行。是最常用的断言级别。
REQUIRE是最高级别，如果断言失败增加错误数量，终止程序运行。
YYY:
各种具体的测试断言，如相等/不等，抛出/不抛出异常，大于/小于。

最常用的几个测试断言有：
BOOST_XXX_EQUAL(l,r)：检查l==r。但是不能用于浮点数的比较，浮点数的相等比较应该使用BOOST_XXX_CLOSE。
BOOST_XXX_GE(l,r)：检查l>=r，同样的还有GT大于，LT小于，LE小于等于和NE不等。
BOOST_XXX_THROW(expr,exception)：检测表达式expr抛出指定的exception异常。
BOOST_XXX_NO_THROW(expr)：检测表达式expr不抛出任何异常。
BOOST_XXX_MESSAGE(expr,message)：测试失败时给出指定的消息。
BOOST_TEST_MESSAGE(message)：仅输出通知用的消息，不含有任何警告或错误，默认情况下不会显示。

test库将测试程序定义为一个测试模块,由测试安装、测试主体、测试清理和测试运行器四个部分组成.

测试主体:是测试模块的实际运行部分,由测试用例和测试套件组织成测试树的形式.

测试用例,是一个包含多个测试断言的函数,它是可以被独立执行测试的最小单元,各个测试用例之间是无关的,发生的错误不会影响到其他测试用例.要添加测试
用例,需要向UTF注册:BOOST_AUTO_TEST_CASE(),宏的参数是测试用例的名字.
BOOST_AUTO_TEST_CASE(t_case1)							//测试用例声明
{
	BOOST_CHECK_EQUAL(1,1);								//测试1==1
	...
}

测试套件,是测试用例的容器,它包含一个或多个测试用例,可以将繁多的测试用例分组管理,共享安装/清理代码,更好地组织测试用例.测试套件可以嵌套,并且
没有嵌套层数的限制.测试套件的自动使用方式是两个宏BOOST_AUTO_TEST_SUITE()和BOOST_AUTO_TEST_SUITE_END().这两个宏必须成对使用,宏
之间的所有测试用例都属于这个测试套件.一个C++源文件中可以有任意多个测试套件.
BOOST_AUTO_TEST_SUITE(s_suite1)						//测试套件开始

BOOST_AUTO_TEST_CASE(t_case1)							//测试用例声明
{
	BOOST_CHECK_EQUAL(1,1);
	...
}

BOOST_AUTO_TEST_CASE(t_case2)							//测试用例声明
{
	BOOST_CHECK_EQUAL(2,2);
	...
}

BOOST_AUTO_TEST_SUITE_END()							//测试套件结束
任何一个UTF单元测试程序都必须存在一个主测试套件,它是整个测试树的根节点,其他的测试套件都是它的子节点.主测试套件的定义可以使用宏
BOOST_TEST_MAIN或者BOOST_TEST_MODULE.定义了这个宏的源文件中不需要再有宏BOOST_AUTO_TEST_SUITE()和BOOST_AUTO_TEST_SUITE_END()，
所有测试用例都自动属于主测试套件。

测试夹具：
测试安装:执行测试前的准备工作,初始化测试用例或测试套件所需的数据.

测试清理:是测试安装的反向操作,执行必要的清理工作.

测试安装和测试清理的动作很像C++中的构造函数和析构函数,因此,可以定义一个辅助类,它的构造函数和析构函数分别执行测试安装和测试清理,之后我们就可以在每个测试
用例最开始声明一个对象,它将自动完成测试安装和测试清理.基于这个原理,UTF中定义了"测试夹具"的概念,它实现了自动的测试安装和测试清理,就像是一个夹在测试用例和
测试套件两端的夹子.
测试夹具不仅可以用于测试用例,也可以用于测试套件和单元测试全局.
使用测试夹具，必须要定义一个夹具类，它只有构造函数和析构函数，用于执行测试安装和测试清理，基本形式是：
struct test_fixture_name											//测试夹具类
{
	test_fixture_name();											//测试安装工作
	~test_fixture_name();											//测试清理工作
};
测试夹具通常是个struct,因为它被UTF用于继承.
指定测试用例和测试套件的夹具类需要使用两个宏:
BOOST_FIXTURE_TEST_SUITE(suite_name,F)
BOOST_FIXTURE_TEST_CASE(test_name,F)
它们替代了之前的BOOST_AUTO_TEST_CASE和BOOST_AUTO_TEST_SUITE,第二个参数指定了要使用的夹具类.
可以在测试套件级别指定夹具,这样套件内的所有子套件和测试用例都自动使用夹具类提供的安装和清理功能,但子套件和测试用例也可以另外指定其他夹具,不会受上层测试
套件的影响.
全局测试夹具需要使用另一个宏BOOST_GLOBAL_FIXTURE,它定义的夹具类被应用于整个测试模块的所有测试套件,包括主测试套件.
//全局测试夹具类
struct global_fixture
{
	global_fixture()
	{
		cout << "global setup" << endless;
	}

	~global_fixture()
	{
		cout << "global teardown" << endless;
	}
};

//定义全局夹具
BOOST_GLOBAL_FIXTURE(global_fixture);

//测试套件夹具类
struct assign_fixture
{
	assign_fixture()
	{
		cout << "suit setup" << endless;
	}

	~assign_fixture()
	{
		cout << "suit teardown" << endless;
	}

	//所有测试用例都可用的成员变量
	vector<int> v;
};

//定义测试套件级别的夹具
BOOST_FIXTURE_TEST_SUITE(s_assign,assign_fixture);

BOOST_AUTO_TEST_CASE(t_assign1)										//测试+=操作符
{
	using namespace boost::assign;

	v += 1,2,3,4;

	BOOST_CHECK_EQUAL(v.size(),4);
	BOOST_CHECK_EQUAL(v[2],3);
}

BOOST_AUTO_TEST_CASE(t_assign2)										//测试push_back函数
{
	using namespace boost::assign;

	push_back(v) (10) (20) (30);

	BOOST_CHECK_EQUAL(v.empty(),false);
	BOOST_CHECK_LT(v[0],v[1]);
}

BOOST_AUTO_TEST_SUITE_END();											//测试套件结束

测试日志：
测试日志是单元测试在运行过程中产生的各种文本信息，包括警告，错误和基本信息，默认情况下这些测试日志都被定向到标准输出(stdout)。
每条测试日志都有一个级别，只有超过允许级别的日志才能被输出。这些日志级别是：
all:
输出所有的测试日志。
success:
相当于all。
test_suite:
仅允许运行测试套件的信息。
message:
仅允许输出用户测试信息(BOOST_TEST_MESSAGE)。
warning:
仅允许输出警告断言信息(BOOST_WARN_XXX)。
error:
仅允许输出CHECK,REQUIRE断言信息(BOOST_CHECK_XXX)。
cpp_exception:
仅允许输出未被捕获的C++异常信息。
system_error:
仅允许非致命的系统错误。
fatal_error:
仅允许输出致命的系统错误。
nothing:
禁止任何信息输出。
默认情况下，UTF的日志级别是warning，会输出大部分单元测试相关的诊断信息，但BOOST_TEST_MESSAGE宏由于是message级别，它的信息不会输出。
日志级别可以通过单元测试程序的命令行参数改变。

运行参数：
基于UTF的单元测试程序在编译完成后可以独立运行。
UTF的命令行参数基本格式是：
--arg_name=arg_value
参数名称和参数值都是大小写敏感的，并且=两边和--右边不能有空格。
常用的命令行参数如下：
run_test:
可以指定要运行的测试用例或测试套件，用斜杠(/)来访问测试树的任意节点，支持使用通配符*。
build_info:
单元测试时输出编译器，STL，Boost等系统信息，取值为yes/no。
output_format:
指定输出信息的格式，取值为hrf(可读格式)/xml。
log_format:
指定日志信息的格式，取值为hrf(可读格式)/xml。
log_level:
允许输出的日志级别。
举例：
--build_info=yes --run_test=s_assign/* --output_format=xml

测试泛型代码
UTF也能够测试模板函数和模板类。
如果采用自动测试方式，可使用BOOST_AUTO_TEST_CASE_TEMPLATE，它需要使用模板元编程库mpl，声明如下：
#define BOOST_AUTO_TEST_CASE_TEMPLATE(test_name,type_name,TL)
举例，测试lexical_cast:
#include "boost/lexical_cast.hpp"
#include "boost/mpl/list.hpp"
using namespace boost;

BOOST_AUTO_TEST_SUITE(s_lexical_cast)

typedef mpl::list<short,int,long> types;									//模板元编程，类型容器
BOOST_AUTO_TEST_CASE_TEMPLATE(t_lexical_cast,T,types)
{
	T n(20);
	BOOST_CHECK_EQUAL(lexical_cast<string>(n),"20");
}

BOOST_AUTO_TEST_SUITE_END()
*/