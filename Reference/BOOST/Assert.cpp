/*
如果在头文件boost/assert.hpp之前定义了宏BOOST_DISABLE_ASSERTS，那么BOOST_ASSERT将会定义为((void)0)，自动失效，但是标准的
assert宏并不会受到影响，这样可以有选择地关闭BOOST_ASSERT。
如果在头文件boost/assert.hpp之前定义了宏BOOST_ENABLE_ASSERT_HANDLER，这将导致BOOST_ASSERT的行为发生改变，它将不再等同于
assert宏，断言的表达式无论是在Debug还是Release模式下都将被求值。如果断言失败，会发生一个断言失败的函数调用boost::assertion_failed()，
这相当于提供了一个错误处理handler。当断言失败时，BOOST_ASSERT宏会把断言表达式字符串、调用函数名(使用BOOST_CURRENT_FUNCTION)、
所在源文件名和行号都传递给assertion_failed()函数处理。用户需要自己实现assertion_failed()函数，以恰当的方式处理错误，通常是记录日志或者抛出
异常。
namespace boost
{
	void assertion_failed(char const* expr,char const* function,char const* file,long line)
	{
	}
}
*/

/*
BOOST_ASSERT_MSG宏与BOOST_ASSERT行为类似，但多了一个错误信息参数，声明如下：
#define BOOST_ASSERT_MSG(expr,msg)
如果断言失败，那么它会自动在标准错误流(std::cerr)上输出表达式字符串、调用函数名、源文件名、行号和自定义的错误信息，然后调用std::abort()终止
程序的运行，相当于是为用户提供了一个缺省的assertion_failed()函数实现。
BOOST_ASSERT_MSG的输出流可以用宏BOOST_ASSERT_MSG_OSTREAM来重定向，也可以使用宏BOOST_ENABLE_ASSERT_HANDLER来自定义
处理handler，需要提供boost::assertion_failed_msg()函数的实现：
namespace boost
{
	void assertion_failed_msg(char const* expr,char const* msg,char const* function,char const* file,long line)
	{
	}
}
*/

/*
BOOST_VERIFY宏具有与BOOST_ASSERT一样的行为，之前对BOOST_ASSERT的讨论对BOOST_VERIFY也同样适用。仅有一点区别：断言的表达式
一定会被求值。它在Release模式下同样会失效。
*/

/*
assert宏和BOOST_ASSERT宏是用于运行时的断言，static_assert库把断言的诊断时刻由运行期提前到编译期，让编译器检查可能发生的错误，它模拟实现
了C++11标准里的static_assert关键字。
static_assert库定义了两个宏用来进行编译期断言，它们是：
BOOST_STATIC_ASSERT(expr)
BOOST_STATIC_ASSERT_MSG(expr,msg)
如果编译器支持C++11标准的新关键字static_assert，那么它们会直接使用static_assert关键字，如果编译器不支持新关键字static_assert，那么
BOOST_STATIC_ASSERT_MSG将简单地等同于BOOST_STATIC_ASSERT，字符串消息功能会失效。
BOOST_ASSERT(expr)必须是一个能够执行的语句，它只能在函数域里出现，而BOOST_STATIC_ASSERT则可以出现在程序的任何位置：名字空间域、
类域或者函数域。其在类域和名字空间域的使用方式与在函数域的方式相同。
断言的表达式必须能够在编译期求值。
namespace my_space
{
	class empty_class
	{
	//在类域中的静态断言，要求int至少4字节
	BOOST_STATIC_ASSERT_MSG(sizeof(int) >= 4,"for 32 bit");
	};

//在名字空间域的静态断言，是一个“空类”
BOOST_STATIC_ASSERT(sizeof(empty_class) == 1);
}
*/