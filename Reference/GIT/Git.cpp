/*
初始化：
在网站上创建仓库
cd /path/to/your/project
git init
git remote add origin git@bitbucket.org:suyang/gl.git

第一次push：
git add .gitignore
git commit -m 'Initial commit with gitignore'
git push -u origin master

第二次push：
git config --global push.default simple
设置push的时候，只push当前的分支到远端对应的分支

第三次push:
git push
*/

/*
设置Bash的起始位置
在Bash的快捷方式上，右键，更改起始位置，取消目标里面的--cd-to-home

设置姓名和邮箱地址
这里设置的姓名和邮箱地址会用在提交日志中。
git config --global user.name "ung.yang.su"
git config --global user.email "ung.yang.su@gmail.com"
提高命令输出的可读性
git config --global color.ui auto

生成SSH Key(不设置公开密钥的密码)
ssh-keygen -t rsa -C "ung.yang.su@gmail.com"
查看公开密钥
在.ssh文件夹中启动Bash
cat id_rsa.pub
需要copy前面的ssh-rsa，而无需copy后面的邮箱地址
添加后，邮箱会收到一封邮件

测试连接
ssh -T git@bitbucket.org
yes
看到用户名后就成功了

克隆仓库
git clone 仓库地址 ./
执行clone命令后我们会默认处于master分支下，同时系统会自动将origin设置成该远程仓库的标识符。也就
是说，当前本地仓库的master分支与远程仓库(origin)的master分支在内容上是完全相同的。

添加远程仓库
如果在本地有了一个名字为name的仓库，那么可以在远程新建一个名字为name的仓库，这个新建的仓库的
地址可能为git@bitbucket.org:xxx/name.git，这时，可以用命令git remote add将远程仓库设置成本地
仓库所对应的远程仓库。
git remote add origin git@bitbucket.org:xxx/name.git
Git会自动将远程仓库的名称设置为origin(标识符)。

查看仓库状态
git status

查看分支
git branch
*表示当前所在的分支
git branch -a
-a命令查看当前分支的相关信息，可以同时显示本地仓库和远程仓库的分支信息。

切换分支
git checkout branch_name

切换回上一个分支
git checkout -

以远程仓库的某个分支为来源，在本地仓库中创建一个对应的分支
git checkout -b local_branch_name origin/remote_branch_name
-b参数的后面是本地仓库中新建分支的名称。新建分支名称后面是获取来源的分支名称

在本地，以当前分支为基础，创建一个新的分支，并切换到这个新的分支
git checkout -b new_branch_name
或者连续执行下面的两条命令也能收到同样的效果
git branch new_branch_name
git checkout new_branch_name

删除本地的一个分支
git branch -d local_branch_name

特性分支
假如我们有一个feature_some分支，那么在该分支中，我们仅进行feature的工作，如果在这个过程中发现了
Bug，也需要再创建一个新的fix_some分支，在fix_some分支中进行修正。请养成创建特性分支后再修改代
码的好习惯。

合并分支
首先切换到“主”分支，然后再进行合并操作。
为了在历史记录中明确记录下本次分支合并，我们需要创建“合并提交”。因此，在合并时加上--no-ff参数。
git merge --no-ff 次分支名字
如果产生了冲突，那么在冲突解决后，进行add与commit命令。

通过将本地仓库的某个分支给推送至远程仓库来实现在远程仓库创建分支的效果
比如，我们在本地创建了一个分支为branch_name，然后使用命令
git push -u origin branch_name
这样远程仓库也有了branch_name这个分支，并且设置远程的分支为本地分支的upstream。

查看文件的暂存状态
git status -s

暂存所有的文件
git add .

提交
将当前暂存区中的文件实际保存到仓库的历史记录中
记述一行提交信息
git commit -m ""
记述详细提交信息
git commit
#后面的内容会被忽略掉
空，放弃提交
第一行：用一行文字简述提交的更改内容
第二行：留空
第三行：记述更改的原因和详细内容
i插入内容
esc退出插入模式
:wq保存并退出
修改上一条提交信息
git commit --amend
把add和commit命令给放到一条命令中
git commit -am "contain error"
如果发现某个分支的已提交中存在一个错误，那么就提交一个修改，然后将这个修改包含到之前的提交中，压
缩成一个历史记录。
然后进行提交
git commit -am "fix error"
然后更改历史，将"fix error"与"contain error"两个提交进行合并，在历史记录中合并为一次提交
git rebase -i HEAD-2(目前还不知道命令格式是什么)
一般从一个分支提交代码时，为了避免冲突则先Pull一下代码，Git会进行自动Merge，然后在进行Push代码。

回溯某个分支至其某个历史版本
git reset --hard 哈希值

查看提交日志
git log
只显示提交信息的第一行
git log --pretty=short
只显示指定目录，文件的日志
只要在git log命令后加上目录名，便会只显示该目录下的日志。如果加的是文件名，就会只显示与该文件相关的日志。
显示文件的改动
git log -p file_name
以图表形式查看分支
git log --graph
查看仓库的操作日志
git reflog

查看更改前后的差别
git diff命令可以查看工作树，暂存区，最新提交之间的差别
查看工作树和暂存区的差别
git diff
查看工作树和最新提交的差别
git diff HEAD
这里的HEAD是指向当前分支中最新一次提交的指针
不妨养成这样一个好习惯，在执行git commit命令之前先执行git diff HEAD命令，查看本次提交与上次提交之前有什么差别。

推送至远程仓库
git push -u origin remote_branch_name
-u参数可以在推送的同时，讲origin仓库的remote_branch_name分支设置为本地仓库当前分支的
upstream(上游)。添加了这个参数，将来运行git pull命令从远程仓库获取内容时，本地仓库的这个分支就可
以直接从origin的remote_branch_name分支获取内容，省去了另外添加参数的麻烦。
已经设置了upstream的本地分支，再将其推送至远程仓库的话，就只需git push就好了。

假如本地有个分支branch_name，远程也有个对应的分支branch_name，当另外一个开发者对远程的这个
分支进行了内容更新，那么我们自己本地的这个分支也需要保持更新，这个时候我们就可以使用git pull命令
来将本地的分支更新到最新的状态。
git pull origin branch_name
如果两人同时修改了同一部分的源代码，push时就很容易发生冲突。所以多名开发者在同一个分支中进行作业时，为减少冲突情况的发生，建议频繁地进行push和pull操作。

名词：
提交：
记录工作树中所有文件的当前状态

Fork：
点击Fork按钮创建自己的仓库。

Issue：
用于Bug报告，功能添加，方向性讨论等。
Pull Request时也会创建Issue。
Issue与Pull Request的编号是通用的。
遇到下面几种情况时，就可以使用这个功能。
1，发现软件的Bug并报告
2，有事想向作者询问，探讨
3，事先列出今后准备实施的任务

Pull Request：
Pull Request是用户修改代码后向对方仓库发送采纳请求的功能。
在我们发送Pull Request后，接收方的仓库会创建一个附带源代码的Issue，我们在这个Issue中记录详细内
容，这就是Pull Request。
发送Pull Request时，一般都是发送特性分支。这样一来，Pull Request就拥有了更明确的特性(主题)。让对
方了解自己修改代码的意图，有助于提高代码审查的效率。
那么就首先创建一个PR分支，在这个分支中修改代码。要发送Pull Request，在远端的仓库中必须有一个包含
了修改后代码的分支，这时可能就需要基于本地的这个PR分支来在远端创建一个相应的PR分支了。
git push origin branch_PR
在网站上查看分支，找到我们刚才创建的远端branch_PR分支，然后点击创建Pull Request，填写信息，选择
在Pull Request被合并后关闭该分支。
在软件的设计与实现过程中如果想发起讨论，Pull Request是个非常好的契机，不必等代码最终完成。
向发送过Pull Request的分支添加提交时，该提交会自动添加至已发送的Pull Request中。
如果用户对仓库有编辑权限的话，那么可以略去Fork操作，直接创建分支，从分支发送Pull Request。

仓库的维护：
通常来说clone来的仓库实际上与原仓库并没有任何关系，所以我们需要将原仓库设置为远程仓库，从该仓库
获取(fetch)数据与本地仓库进行合并(merge)，让本地仓库的源代码保持最新状态。
给原仓库设置名称(upstream)，将其作为远程仓库：
git remote add upstream git://xxx.git
从远程仓库获取(fetch)最新源代码，与自己仓库的分支进行合并。要让仓库维持最新状态，只需要重复这一
工作即可。
git fetch upstream
这样一来，本地的当前分支就获得了最新的源代码。我们在创建特性分支，编辑源代码之前，应该先将本地仓
库更新一下。

接收Pull Request：
令发送方的用户名为“发送者”，接收方的用户名为“接收者”
首先，将接收方的仓库更新为最新的状态，通过clone或pull。
将发送方的仓库设置为本地仓库的远程仓库，获取发送方仓库的数据。
git remote add 发送者 git@xxx发送者xxx.git
git fetch 发送者
这样，我们就获取了Pull Request发送方仓库以及分支的数据。
创建用于检查的分支，创建这个分支，可以用来模拟采纳Pull Request后的状态。
将fetch完毕的内容与检查分支进行合并，删除上面创建的用于检查的分支。
在浏览器中点击合并Pull Request的按钮。或者在本地合并后push到远端。
*/

/*
Git是把数据看作是对小型文件系统的一组快照。每次你提交更新，或在 Git 中保存项目状态时，它主要对当
时的全部文件制作一个快照并保存这个快照的索引。为了高效，如果文件没有修改，Git 不再重新存储该文
件，而是只保留一个链接指向之前存储的文件。Git 对待数据更像是一个 快照流。
近乎所有操作都是本地执行:
在Git中的绝大多数操作都只需要访问本地文件和资源，一般不需要来自网络上其它计算机的信息。
Git保证完整性
Git中所有数据在存储前都计算校验和，然后以校验和来引用。这意味着不可能在 Git 不知情时更改任何文件
内容或目录内容。这个功能建构在 Git 底层，是构成 Git 哲学不可或缺的部分。若你在传送过程中丢失信息
或损坏文件，Git 就能发现。
Git用以计算校验和的机制叫做 SHA-1 散列（hash，哈希）。 这是一个由 40 个十六进制字符
（0-9 和 a-f） 组成字符串，基于 Git 中文件的内容或目录结构计算出来。
Git数据库中保存的信息都是以文件内容的哈希值来索引，而不是文件名。
Git一般只添加数据:
你执行的Git操作，几乎只往 Git 数据库中增加数据。很难让 Git 执行任何不可逆操作，或者让它以任何方式
清除数据。未提交更新时有可能丢失或弄乱修改的内容。但是一旦你提交快照到 Git 中，就难以再丢失数据。
三种状态：
Git有三种状态，你的文件可能处于其中之一：已提交（committed）、已修改（modified）和已暂存
（staged）。已提交表示数据已经安全的保存在本地数据库中。已修改表示修改了文件，但还没保存到数据库
中。已暂存表示对一个已修改文件的当前版本做了标记，使之包含在下次提交的快照中。
由此引入 Git 项目的三个工作区域的概念：Git 仓库、工作目录以及暂存区域。
Git仓库目录是 Git 用来保存项目的元数据和对象数据库的地方。这是 Git 中最重要的部分，从其它计算机克
隆仓库时，拷贝的就是这里的数据。
工作目录是对项目的某个版本独立提取出来的内容。这些从 Git 仓库的压缩数据库中提取出来的文件，放在
磁盘上供你使用或修改。
暂存区域是一个文件，保存了下次将提交的文件列表信息，一般在 Git 仓库目录中。有时候也被称作"索引"，
不过一般说法还是叫暂存区域。
基本的Git工作流程如下：
1. 在工作目录中修改文件。
2. 暂存文件，将文件的快照放入暂存区域。
3. 提交更新，找到暂存区域的文件，将快照永久性存储到 Git 仓库目录。
可以使用Git来获取Git的升级：
$git clone git://git.kernel.org/pub/scm/git/git.git
检查配置信息:
如果想要检查你的配置，可以使用 git config --list 命令来列出所有 Git 当时能找到的配置。
你可以通过输入 git config <key>：来检查 Git 的某一项配置
$git config user.name
获取帮助:
若你使用Git时需要获取帮助，有三种方法可以找到 Git 命令的使用手册：
$git help <verb>
$git <verb> --help
$man git-<verb>
例如，要想获得 config 命令的手册，执行
$git help config
获取Git仓库:
有两种取得Git项目仓库的方法。第一种是在现有项目或目录下导入所有文件到 Git 中。第二种是从一个服务器
克隆一个现有的 Git 仓库。
在现有目录中初始化仓库
如果你打算使用 Git 来对现有的项目进行管理，你只需要进入该项目目录并输入：
$git init
该命令将创建一个名为 .git 的子目录，这个子目录含有你初始化的 Git 仓库中所有的必须文件，这些文件是
Git 仓库的骨干。但是，在这个时候，我们仅仅是做了一个初始化的操作，你的项目里的文件还没有被跟踪。
如果你是在一个已经存在文件的文件夹（而不是空文件夹）中初始化 Git 仓库来进行版本控制的话，你应该开
始跟踪这些文件并提交。你可通过 git add 命令来实现对指定文件的跟踪，
然后执行git commit提交：
$git add *.c
$git add LICENSE
$git commit -m 'initial project version'
现在，你已经得到了一个实际维护（或者说是跟踪）着若干个文件的Git仓库。
克隆现有的仓库
如果你想获得一份已经存在了的Git仓库的拷贝，比如说，你想为某个开源项目贡献自己的一份力，这时就要
用到git clone命令。
Git克隆的是该 Git 仓库服务器上的几乎所有数据，而不是仅仅复制完成你的工作所需要文件。当你执行
git clone 命令的时候，默认配置下远程Git仓库中的每一个文件的每一个版本都将被拉取下来。事实上，如果
你的服务器的磁盘坏掉了，你通常可以使用任何一个克隆下来的用户端来重建服务器上的仓库。
如果你想在克隆远程仓库的时候，自定义本地仓库的名字，你可以使用如下命令：
$git clone https://github.com/libgit2/libgit2 mylibgit
这将执行与上一个命令相同的操作，不过在本地创建的仓库名字变为mylibgit 。
请记住，你工作目录下的每一个文件都不外乎这两种状态：已跟踪或未跟踪。 已跟踪的文件是指那些被纳入了
版本控制的文件，在上一次快照中有它们的记录，在工作一段时间后，它们的状态可能处于未修改，已修改或
已放入暂存区。 工作目录中除已跟踪文件以外的所有其它文件都属于未跟踪文件，它们既不存在于上次快照的
记录中，也没有放入暂存区。 初次克隆某个仓库的时候，工作目录中的所有文件都属于已跟踪文件，并处于未
修改状态。
编辑过某些文件之后，由于自上次提交后你对它们做了修改，Git 将它们标记为已修改文件。我们逐步将这些
修改过的文件放入暂存区，然后提交所有暂存了的修改，如此反复。
文件的状态变化周期：
如图Knowledge/Git/001.png所示：
未跟踪的文件意味着 Git 在之前的快照（提交） 中没有这些文件；Git 不会自动将之纳入跟踪范围，除非你明
明白白地告诉它“我需要跟踪该文件”。
git add命令使用文件或目录的路径作为参数；如果参数是目录的路径，该命令将递归地跟踪该目录下的所有
文件。这是个多功能命令：可以用它开始跟踪新文件，或者把已跟踪的文件放到暂存区，还能用于合并时把
有冲突的文件标记为已解决状态等。 将这个命令理解为“添加内容到下一次提交中”。
状态简览：
git status命令的输出十分详细，但其用语有些繁琐。 如果你使用 git status -s 命令或git status --short
命令，你将得到一种更为紧凑的格式输出。 运行 git status -s ，状态报告输出如下：
$git status -s
 M README
MM Rakefile
A lib/git.rb
M  lib/simplegit.rb
?? LICENSE.txt
新添加的未跟踪文件前面有 ?? 标记，新添加到暂存区中的文件前面有 A 标记，修改过的文件前面有 M 标
记。 你可能注意到了 M 有两个可以出现的位置，出现在右边的 M 表示该文件被修改了但是还没放入暂存区，
出现在靠左边的 M 表示该文件被修改了并放入了暂存区。例如，上面的状态报告显示： README 文件在工
作区被修改了但是还没有将修改后的文件放入暂存区, lib/simplegit.rb 文件被修改了并将修改后的文件放入
了暂存区。而Rakefile 在工作区被修改并提交到暂存区后又在工作区中被修改了，所以在暂存区和工作区都
有该文件被修改了的记录。
文件 .gitignore 的格式规范如下：
所有空行或者以 ＃ 开头的行都会被 Git 忽略。
可以使用标准的 glob 模式匹配。
匹配模式可以以（/ ） 开头防止递归。
匹配模式可以以（/ ） 结尾指定目录。
要忽略指定模式以外的文件或目录，可以在模式前加上惊叹号（! ） 取反。
所谓的 glob 模式是指 shell 所使用的简化了的正则表达式。 星号匹配零个或多个任意字符； [abc] 匹配任
何一个列在方括号中的字符（这个例子要么匹配一个 a，要么匹配一个b，要么匹配一个 c） ；问号（? ） 只
匹配一个任意字符；如果在方括号中使用短划线分隔两个字符，表示所有在这两个字符范围内的都可以匹配
（比如 [0-9] 表示匹配所有 0 到 9 的数字） 。 使用两个星号表示匹配任意中间目录。
GitHub 有一个十分详细的针对数十种项目及语言的 .gitignore 文件列表，你可以在
https://github.com/github/gitignore 找到它.
要查看尚未暂存的文件更新了哪些部分，不加参数直接输入 git diff。此命令比较的是工作目录中当前文件和
暂存区域快照之间的差异， 也就是修改之后还没有暂存起来的变化内容。
若要查看已暂存的将要添加到下次提交里的内容，可以用 git diff --cached 命令。（Git1.6.1 及更高版本还
允许使用 git diff --staged ，效果是相同的，但更好记些。）
Git Diff 的插件版本：
我们使用 git diff 来分析文件差异。 但是，如果你喜欢通过图形化的方式或其它格式输出方式的话，可以使用
git difftool命令来用Araxis，emerge 或 vimdiff等软件输出 diff 分析结果。 使用
git difftool --tool-help命令来看你的系统支持哪些Git Diff插件。
$git commit：
这种方式会启动文本编辑器以便输入本次提交的说明。(默认会启用 shell 的环境变量$EDITOR 所指定的软
件，一般都是vim或emacs。使用 git config --global core.editor 命令设定你喜欢的编辑软件。）
Git提供了一个跳过使用暂存区域的方式， 只要在提交的时候，给git commit加上 -a 选项，Git 就会自动把
所有已经跟踪过的文件暂存起来一并提交，从而跳过git add步骤。
移除文件：
要从 Git 中移除某个文件，就必须要从已跟踪文件清单中移除（确切地说，是从暂存区域移除），然后提交。
可以用git rm命令完成此项工作，并连带从工作目录中删除指定的文件，这样以后就不会出现在未跟踪文件清
单中了。
如果删除之前修改过并且已经放到暂存区域的话，则必须要用强制删除选项 -f （译注：即 force 的首字母）。
这是一种安全特性，用于防止误删还没有添加到快照的数据，这样的数据不能被 Git 恢复。
另外一种情况是，我们想把文件从 Git 仓库中删除（亦即从暂存区域移除） ，但仍然希望保留在当前工作目录
中。 换句话说，你想让文件保留在磁盘，但是并不想让 Git 继续跟踪。当你忘记添加 .gitignore 文件，不小
心把一个很大的日志文件或一堆 .a 这样的编译生成文件添加到暂存区时，这一做法尤其有用。 为达到这一目
的，使用 --cached 选项：
$git rm --cached README
git rm 命令后面可以列出文件或者目录的名字，也可以使用 glob 模式。 比方说：
$git rm log/\*.log
注意到星号 * 之前的反斜杠 \ ， 因为 Git 有它自己的文件模式扩展匹配方式，所以我们不用shell来帮忙展
开。此命令删除 log/ 目录下扩展名为 .log 的所有文件。
移动文件：
不像其它的 VCS 系统，Git 并不显式跟踪文件移动操作。 如果在 Git 中重命名了某个文件，仓库中存储的元
数据并不会体现出这是一次改名操作。 不过 Git 非常聪明，它会推断出究竟发生了什么，至于具体是如何做
到的，我们稍后再谈。既然如此，当你看到 Git 的 mv 命令时一定会困惑不已。 要在 Git 中对文件改名，可
以这么做：
$git mv file_from file_to
其实，运行 git mv 就相当于运行了下面三条命令：
$ mv README.md README
$ git rm README.md
$ git add README
如此分开操作，Git 也会意识到这是一次改名，所以不管何种方式结果都一样。 两者唯一的区别是， mv 是一
条命令而另一种方式需要三条命令，直接用 git mv 轻便得多。 不过有时候用其他工具批处理改名的话，要记
得在提交前删除老的文件名，再添加新的文件名。
git log有许多选项可以帮助你搜寻你所要找的提交， 接下来我们介绍些最常用的。一个常用的选项是 -p，用
来显示每次提交的内容差异。 你也可以加上 -2 来仅显示最近两次提交。
如果你想看到每次提交的简略的统计信息，你可以使用--stat选项。
另外一个常用的选项是 --pretty 。 这个选项可以指定使用不同于默认格式的方式展示提交历史。这个选项有
一些内建的子选项供你使用。 比如用 oneline 将每个提交放在一行显示，查看的提交数很大时非常有用。 另
外还有 short ， full 和 fuller 可以用。
但最有意思的是 format，可以定制要显示的记录格式。 这样的输出对后期提取分析格外有用，因为你知道输
出的格式不会随着Git的更新而发生改变：
$git log --pretty=format:"%h - %an, %ar : %s"
ca82a6d - Scott Chacon, 6 years ago : changed the version number
085bb3b - Scott Chacon, 6 years ago : removed unnecessary test
选项		说明
%H		提交对象（commit） 的完整哈希字串
%h		提交对象的简短哈希字串
%T		树对象（tree） 的完整哈希字串
%t		树对象的简短哈希字串
%P		父对象（parent） 的完整哈希字串
%p		父对象的简短哈希字串
%an		作者（author） 的名字
%ae		作者的电子邮件地址
%ad		作者修订日期（可以用 --date= 选项定制格式）
%ar		作者修订日期，按多久以前的方式显示
%cn		提交者(committer)的名字
%ce		提交者的电子邮件地址
%cd		提交日期
%cr		提交日期，按多久以前的方式显示
%s		提交说明
你一定奇怪 作者 和 提交者 之间究竟有何差别， 其实作者指的是实际作出修改的人，提交者指的是最后将此工
作成果提交到仓库的人。 所以，当你为某个项目发布补丁，然后某个核心成员将你的补丁并入项目时，你就是
作者，而那个核心成员就是提交者。
当 oneline 或 format 与另一个 log 选项 --graph 结合使用时尤其有用。 这个选项添加了一些ASCII字符串
来形象地展示你的分支、合并历史：
$git log --pretty=format:"%h %s" --graph
git log的常用选项：
选项								说明
-p									按补丁格式显示每个更新之间的差异。
--stat							显示每次更新的文件修改统计信息。
--shortstat						只显示 --stat 中最后的行数修改添加移除统计。
--name-only					仅在提交信息后显示已修改的文件清单。
--namestatus					显示新增、修改、删除的文件清单。
--abbrevcommit				仅显示 SHA-1 的前几个字符，而非所有的 40 个字符。
--relativedate					使用较短的相对时间显示（比如，“2 weeks ago”） 。
--graph							显示 ASCII 图形表示的分支合并历史。
--pretty							使用其他格式显示历史提交信息。可用的选项包括 oneline，short，full，fuller
									和 format（后跟指定格式） 。
限制输出长度：
除了定制输出格式的选项之外， git log 还有许多非常实用的限制输出长度的选项，也就是只输出部分提交信
息。 之前你已经看到过 -2 了，它只显示最近的两条提交， 实际上，这是-<n> 选项的写法，其中的 n 可以
是任何整数，表示仅显示最近的若干条提交。 不过实践中我们是不太用这个选项的，Git 在输出所有提交时会
自动调用分页程序，所以你一次只会看到一页的内容。
另外还有按照时间作限制的选项，比如 --since 和 --until 也很有用。 例如，下面的命令列出所有最近两周
内的提交：
$ git log --since=2.weeks
这个命令可以在多种格式下工作，比如说具体的某一天 "2008-01-15" ，或者是相对地多久以前
"2 years 1 day 3 minutes ago" 。
还可以给出若干搜索条件，列出符合的提交。 用 --author 选项显示指定作者的提交，用 --grep 选项搜索提
交说明中的关键字。 （请注意，如果要得到同时满足这两个选项搜索条件的提交，就必须用 --all-match 选
项。否则，满足任意一个条件的提交都会被匹配出来）
另一个非常有用的筛选选项是 -S ，可以列出那些添加或移除了某些字符串的提交。 比如说，你想找出添加或
移除了某一个特定函数的引用的提交，你可以这样使用：
$ git log -S function_name
最后一个很实用的 git log 选项是路径(path)， 如果只关心某些文件或者目录的历史提交，可以在git log选项
的最后指定它们的路径。 因为是放在最后位置上的选项，所以用两个短划线（--） 隔开之前的选项和后面限定
的路径名。
限制git log输出的选项：
选项								说明
-(n)								仅显示最近的 n 条提交
--since , --after				仅显示指定时间之后的提交。
--until , --before				仅显示指定时间之前的提交。
--author							仅显示指定作者相关的提交。
--committer					仅显示指定提交者相关的提交。
--grep							仅显示含指定关键字的提交
-S									仅显示添加或移除了某个关键字的提交
来看一个实际的例子，如果要查看 Git 仓库中，2008年10月期间，Junio Hamano提交的但未合并的测试
文件，可以用下面的查询命令：
$git log --pretty="%h - %s" --author=gitster --since="2008-10-01" \
--before="2008-11-01" --no-merges -- t/
撤消操作：
在任何一个阶段，你都有可能想要撤消某些操作。
注意，有些撤消操作是不可逆的。 
有时候我们提交完了才发现漏掉了几个文件没有添加，或者提交信息写错了。 此时，可以运行带有--amend
选项的提交命令尝试重新提交：
$git commit --amend
这个命令会将暂存区中的文件提交。 如果自上次提交以来你还未做任何修改（例如，在上次提交后马上执行
了此命令） ，那么快照会保持不变，而你所修改的只是提交信息。
文本编辑器启动后，可以看到之前的提交信息。 编辑后保存会覆盖原来的提交信息。
例如，你提交后发现忘记了暂存某些需要的修改，可以像下面这样操作：
$git commit -m 'initial commit'
$git add forgotten_file
$git commit --amend
最终你只会有一个提交 - 第二次提交将代替第一次提交的结果。
取消暂存的文件：
例如，你已经修改了两个文件并且想要将它们作为两次独立的修改提交，但是却意外地输入了git add *暂存了
它们两个。 如何只取消暂存两个
中的一个呢？ git status 命令提示了你：
$ git add *
$ git status
On branch master
Changes to be committed:
(use "git reset HEAD <file>..." to unstage)
renamed: README.md -> README
modified: CONTRIBUTING.md
在"Changes to be committed'' 文字正下方，提示使用git reset HEAD <file>...来取消暂存。 所以，
我们可以这样来取消暂存CONTRIBUTING.md文件：
$ git reset HEAD CONTRIBUTING.md
CONTRIBUTING.md 文件已经是修改未暂存的状态了。
撤消对文件的修改：
如果你并不想保留对 CONTRIBUTING.md 文件的修改怎么办？ 你该如何方便地撤消修改，将它还原成上次
提交时的样子（或者刚克隆完的样子，或者刚把它放入工作目录时的样子）
$git checkout -- CONTRIBUTING.md
你需要知道git checkout -- [file]是一个危险的命令，这很重要。 你对那个文件做的任何修改都会消失，你
只是拷贝了另一个文件来覆盖它。 除非你确实清楚不想要那个文件了，否则不要使用这个命令。
记住，在 Git 中任何 已提交的 东西几乎总是可以恢复的。 甚至那些被删除的分支中的提交或使用--amend选
项覆盖的提交也可以恢复。然而，任何你未提交的东西丢失后很可能再也找不到了。
查看远程仓库：
如果想查看你已经配置的远程仓库服务器，可以运行 git remote 命令。 它会列出你指定的每一个远程服务
器的简写。 如果你已经克隆了自己的仓库，那么至少应该能看到 origin，这是Git 给你克隆的仓库服务器的默
认名字。
添加远程仓库：
运行git remote add <shortname> <url> 添加一个新的远程 Git 仓库，同时指定一个你可以轻松引用的
简写：
$ git remote
origin
$ git remote add pb https://github.com/paulboone/ticgit
$ git remote -v
origin https://github.com/schacon/ticgit (fetch)
origin https://github.com/schacon/ticgit (push)
pb https://github.com/paulboone/ticgit (fetch)
pb https://github.com/paulboone/ticgit (push)
现在你可以在命令行中使用字符串 pb 来代替整个 URL。 例如，如果你想拉取 Paul 的仓库中有但你没有的
信息，可以运行 git fetch pb。
从远程仓库中抓取与拉取：
从远程仓库中获得数据，可以执行：
$ git fetch [remote-name]
这个命令会访问远程仓库，从中拉取所有你还没有的数据。 执行完成后，你将会拥有那个远程仓库中所有分支
的引用，可以随时合并或查看。
如果你使用 clone 命令克隆了一个仓库，命令会自动将其添加为远程仓库并默认以origin'为简写。所以，
git fetch origin会抓取克隆（或上一次抓取） 后新推送的所有工作。必须注意git fetch命令会将数据拉取到
你的本地仓库，它并不会自动合并或修改你当前的工作。 当准备好时你必须手动将其合并入你的工作。
如果你有一个分支设置为跟踪一个远程分支，可以使用 git pull 命令来自动的抓取然后合并远程分支到当前分
支。默认情况下， git clone 命令会自动设置本地 master分支跟踪克隆的远程仓库的 master 分支（或不管
是什么名字的默认分支） 。 运行 git pull通常会从最初克隆的服务器上抓取数据并自动尝试合并到当前所在
的分支。
推送到远程仓库：
当你想分享你的项目时，必须将其推送到上游。这个命令很简单：git push [remote-name][branch-name]。
当你想要将 master 分支推送到 origin 服务器时（再次说明，克隆时通常会自动帮你设置好那两个名字），
那么运行这个命令就可以将你所做的备份到服务器：
$ git push origin master
只有当你有所克隆服务器的写入权限，并且之前没有人推送过时，这条命令才能生效。 当你和其他人在同一时
间克隆，他们先推送到上游然后你再推送到上游，你的推送就会毫无疑问地被拒绝。 你必须先将他们的工作拉
取下来并将其合并进你的工作后才能推送。
查看远程仓库：
如果想要查看某一个远程仓库的更多信息，可以使用 git remote show [remote-name] 命令。
远程仓库的移除与重命名：
如果想要重命名引用的名字可以运行 git remote rename 去修改一个远程仓库的简写名。 例如，想要将pb
重命名为paul，可以用 git remote rename 这样做：
$ git remote rename pb paul
$ git remote
origin
paul
如果因为一些原因想要移除一个远程仓库 - 你已经从服务器上搬走了或不再想使用某一个特定的镜像了，又或
者某一个贡献者不再贡献了，可以使用 git remote rm ：
$ git remote rm paul
$ git remote
origin
列出标签：
在 Git 中列出已有的标签是非常简单直观的。 只需要输入 git tag ：
$ git tag
v0.1
v1.3
你也可以使用特定的模式查找标签。 例如，Git 自身的源代码仓库包含标签的数量超过500个。 如果只对
1.8.5系列感兴趣，可以运行：
$ git tag -l 'v1.8.5*'
v1.8.5
v1.8.5-rc0
v1.8.5-rc1
v1.8.5-rc2
v1.8.5-rc3
v1.8.5.1
v1.8.5.2
v1.8.5.3
v1.8.5.4
v1.8.5.5
创建标签：
Git 使用两种主要类型的标签：轻量标签（lightweight） 与附注标签（annotated） 。一个轻量标签很像一
个不会改变的分支 - 它只是一个特定提交的引用。然而，附注标签是存储在 Git 数据库中的一个完整对象。
它们是可以被校验的；其中包含打标签者的名字、电子邮件地址、日期时间；还有一个标签信息；并且可以使
用 GNU Privacy Guard （GPG） 签名与验证。 通常建议创建附注标签，这样你可以拥有以上所有信息；但
是如果你只是想用一个临时的标签，或者因为某些原因不想要保存那些信息，轻量标签也是可用的。
附注标签：
在 Git 中创建一个附注标签是很简单的。 最简单的方式是当你在运行 tag 命令时指定 -a选项：
$ git tag -a v1.4 -m 'my version 1.4'
$ git tag
v0.1
v1.3
v1.4
-m选项指定了一条将会存储在标签中的信息。 如果没有为附注标签指定一条信息，Git 会运行编辑器要求你
输入信息。
通过使用 git show 命令可以看到标签信息与对应的提交信息：
$ git show v1.4
轻量标签：
轻量标签本质上是将提交校验和存储到一个文件中，没有保存任何其他信息。 创建轻量标签，不需要使用
-a 、-s 或-m选项，只需要提供标签名字：
$ git tag v1.4-lw
$ git tag
v0.1
v1.3
v1.4
v1.4-lw
v1.5
后期打标签：
你也可以对过去的提交打标签。 假设提交历史是这样的：
$ git log --pretty=oneline
15027957951b64cf874c3557a0f3547bd83b3ff6 Merge branch 'experiment'
a6b4c97498bd301d84096da251c98a07c7723e65 beginning write support
0d52aaab4479697da7686c15f77a3d64d9165190 one more thing
6d52a271eda8725415634dd79daabbc4d9b6008e Merge branch 'experiment'
0b7434d86859cc7b8c3d5e1dddfed66ff742fcbc added a commit function
4682c3261057305bdd616e23b64b0857d832627b added a todo file
166ae0c4d3f420721acbb115cc33848dfcc2121a started write support
9fceb02d0ae598e95dc970b74767f19372d61af8 updated rakefile
964f16d36dfccde844893cac5b347e7b3d44abbc commit the todo
8a5cbc430f1a9c3d00faaeffd07798508422908a updated readme
现在，假设在 v1.2 时你忘记给项目打标签，也就是在updated rakefile提交。你可以在之后补上标签。要在
那个提交上打标签，你需要在命令的末尾指定提交的校验和（或部分校验和）:
$ git tag -a v1.2 9fceb02
共享标签：
默认情况下， git push 命令并不会传送标签到远程仓库服务器上。 在创建完标签后你必须显式地推送标签到
共享服务器上。 这个过程就像共享远程分支一样 - 你可以运行 git push origin [tagname] 。
$ git push origin v1.5
如果想要一次性推送很多标签，也可以使用带有--tags选项的 git push 命令。 这将会把所有不在远程仓库服
务器上的标签全部传送到那里。
$ git push origin --tags
现在，当其他人从仓库中克隆或拉取，他们也能得到你的那些标签。
检出标签：
在 Git 中你并不能真的检出一个标签，因为它们并不能像分支一样来回移动。 如果你想要工作目录与仓库中特
定的标签版本完全一样，可以使用 git checkout -b [branchname] [tagname] 在特定的标签上创建一个新
分支：
$ git checkout -b version2 v2.0.0
Switched to a new branch 'version2'
当然，如果在这之后又进行了一次提交， version2 分支会因为改动向前移动了，那么version2 分支就会和
v2.0.0标签稍微有些不同，这时就应该当心了。
Git 别名：
Git 并不会在你输入部分命令时自动推断出你想要的命令。 如果不想每次都输入完整的 Git 命令，可以通过
git config文件来轻松地为每一个命令设置一个别名。 这里有一些例子你可以试试：
$ git config --global alias.co checkout
$ git config --global alias.br branch
$ git config --global alias.ci commit
$ git config --global alias.st status
例如，为了解决取消暂存文件的易用性问题，可以向 Git 中添加你自己的取消暂存别名：
$ git config --global alias.unstage 'reset HEAD --'
这会使下面的两个命令等价：
$ git unstage fileA
$ git reset HEAD -- fileA
这样看起来更清楚一些。 通常也会添加一个 last 命令，像这样：
$ git config --global alias.last 'log -1 HEAD'
这样，可以轻松地看到最后一次提交：
$ git last
可以看出Git只是简单地将别名替换为对应的命令。然而，你可能想要执行外部命令，而不是一个Git子命
令。 如果是那样的话，可以在命令前面加入 ! 符号。 如果你自己要写一些与 Git 仓库协作的工具的话，那会
很有用。我们现在演示将 git visual 定义为 gitk 的别名：
$ git config --global alias.visual '!gitk'
Git 分支：
使用分支意味着你可以把你的工作从开发主线上分离开来，以免影响开发主线。
Git 鼓励在工作流程中频繁地使用分支与合并，哪怕一天之内进行许多次。
Git 保存的不是文件的变化或者差异，而是一系列不同时刻的文件快照。
在进行提交操作时，Git 会保存一个提交对象（commit object） 。知道了 Git 保存数据的方式，我们可以
很自然的想到——该提交对象会包含一个指向暂存内容快照的指针。 但不仅仅是这样，该提交对象还包含了
作者的姓名和邮箱、提交时输入的信息以及指向它的父对象的指针。首次提交产生的提交对象没有父对象，
普通提交操作产生的提交对象有一个父对象，而由多个分支合并产生的提交对象有多个父对象。
为了说得更加形象，我们假设现在有一个工作目录，里面包含了三个将要被暂存和提交的文件。 暂存操作会
为每一个文件计算校验和，然后会把当前版本的文件快照保存到 Git 仓库中（Git 使用 blob 对象来保存它
们） ，最终将校验和加入到暂存区域等待提交。
当使用 git commit 进行提交操作时，Git 会先计算每一个子目录的校验和，然后在 Git 仓库中这些校验和保
存为树对象。 随后，Git 便会创建一个提交对象，它除了包含上面提到的那些信息外，还包含指向这个树对
象（项目根目录） 的指针。如此一来，Git 就可以在需要的时候重现此次保存的快照。
现在，Git 仓库中有五个对象：三个 blob 对象（保存着文件快照） 、一个树对象（记录着目录结构和blob对
象索引） 以及一个提交对象（包含着指向前述树对象的指针和所有提交信息） 。
Git 的分支，其实本质上仅仅是指向提交对象的可变指针。 Git 的默认分支名字是 master 。在多次提交操作
之后，你其实已经有一个指向最后那个提交对象的 master 分支。 它会在每次的提交操作中自动向前移动。
Git 的 `master'' 分支并不是一个特殊分支。 它就跟其它分支完全没有区别。 之所以几乎每一个仓库都有
master分支，是因为 `git init 命令默认创建它，并且大多数人都懒得去改动它。
分支创建：
它只是为你创建了一个可以移动的新的指针。 比如，创建一个 testing 分支， 你需要使用 git branch 命令：
$ git branch testing
这会在当前所在的提交对象上创建一个指针。
那么，Git 又是怎么知道当前在哪一个分支上呢？ 也很简单，它有一个名为 HEAD 的特殊指针。 请注意它和
许多其它版本控制系统（如 Subversion 或 CVS） 里的 HEAD 概念完全不同。 在 Git 中，它是一个指针，
指向当前所在的本地分支（译注：将 HEAD 想象为当前分支的别名） 。 在本例中，你仍然在 master 分支
上。 因为 git branch 命令仅仅 创建 一个新分支，并不会自动切换到新分支中去。
你可以简单地使用 git log 命令查看各个分支当前所指的对象。 提供这一功能的参数是 --decorate 。
$ git log --oneline --decorate
f30ab (HEAD, master, testing) add feature #32 - ability to add new
34ac2 fixed bug #1328 - stack overflow under certain conditions
98ca9 initial commit of my project
正如你所见，当前 master'' 和 testing'' 分支均指向校验和以 f30ab 开头的提交对象。
分支切换：
要切换到一个已存在的分支，你需要使用 git checkout 命令。 我们现在切换到新创建的testing 分支去：
$ git checkout testing
这样 HEAD 就指向 testing 分支了。
HEAD 分支随着提交操作自动向前移动。
检出时 HEAD 随之移动。
你创建了一个新分支，并切换过去进行了一些工作，随后又切换回 master 分支进行了另外一些工作。 上述
两次改动针对的是不同分支：你可以在不同分支间不断地来回切换和工作，并在时机成熟时将它们合并起来。
而所有这些工作，你需要的命令只有 branch 、 checkout 和 commit 。
你可以简单地使用 git log 命令查看分叉历史。
运行 git log --oneline --decorate --graph --all ，它会输出你的提交历史、各个分支的指向以及项目的分
支分叉情况。
$ git log --oneline --decorate --graph --all
由于 Git 的分支实质上仅是包含所指对象校验和（长度为 40 的 SHA-1 值字符串） 的文件，所以它的创建和
销毁都异常高效。 创建一个新分支就像是往一个文件中写入 41 个字节（40个字符和 1 个换行符） ，如此的
简单能不快吗？
合并分支：
当你试图合并两个分支时，如果顺着一个分支走下去能够到达另一个分支，那么 Git 在合并两者的时候，只会
简单的将指针向前推进（指针右移） ，因为这种情况下的合并操作没有需要解决的分歧——这就叫做 “快
进（fast-forward）”。
另外一种合并情况，如图Knowledge/Git/001.png所示：
出现这种情况的时候，Git 会使用两个分支的末端所指的快照（C4 和 C5 ） 以及这两个分支的工作祖先
（C2 ），做一个简单的三方合并。
*/