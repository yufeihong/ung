/*
Function语意学：
C++支持三种类型的member functions：static，nonstatic和virtual，每一种类型被调用的方式都不相同。

Nonstatic Member Functions：
C++的设计准则之一就是：弄static member function至少必须和一般的nonmember function有相同的效率。
编译器在内部将“member函数实例”转换为对等的“nonmember函数实例”。
1，改写函数的signature，以安插一个额外的参数到member function中，用以提供一个存取管道，是class object得以将此函数调用。
2，将每一个“对nonstatic data member的存取操作”改为经由this指针来存取。
3，将member function重新写成一个外部函数。将函数名称经过“mangling”处理。

如果你声明extern "C"，就会压抑nonmember functions的“mangling”效果。

Virtual Member Functions：
在一个复杂的class派生体系中，可能存在多个vptrs，所以其名称也会被“mangled”。

在C++中，多态表示“以一个public base class的指针（或引用），寻址出一个derived class object”。

1，为了找到表格，每一个class object被安插了一个由编译器产生的指针，指向该表格。
2，为了找到函数地址，每一个virtual function被指派一个表格索引值。

当Y派生自X时，Y可以继承base class所声明的virtual functions的函数实例。正确地说是，该函数实例的地址会被拷贝到derived class的virtual table的相对应slot
之中。

在多重继承之下，一个derived class内含n - 1个额外的virtual tables，n表示其上一层base classes的个数（因此，单一继承将不会有额外的virtual tables）。
针对每一个virtual tables，derived object中有对应的vptr。

建议，不要在一个virtual base class中声明nonstatic data members。

Static Member Functions：
1，它不能够直接存取其class中的nonstatic members。
2，它不能够被声明为const，volatile或virtual。
3，它不需要经由class object才被调用---虽然大部分时候它是这样被调用的。

一个static member function，会被提出于class声明之外，并给予一个经过“mangled”的适当名称。

如果取一个static member function的地址，获得的将是其在内存中的位置，也就是其地址。由于static member function没有this指针，所以其地址的类型并不是一
个“指向class member function的指针”，而是一个“nonmember函数指针”。
&Point3d::object_count();
会得到：
unsigned int (*)();
而不是：
unsigned int (Point3d::*)();

static member function由于缺乏this指针，因此差不多等同于nonmember function，它提供了一个意想不到的好处：成为一个callback函数。

指向Member Function的指针：
取一个nonstatic member function的地址，如果该函数是nonvirtual，得到的结果是它在内存中真正的地址。然而这个值也是不完全的。它也需要被绑定于某个class 
object的地址上，才能够通过它调用该函数。所有的nonstatic member functions都需要对象的地址（以参数this指出）。

class X
{
public:
	virtual ~X()
	{
	}

	virtual void func()
	{
		cout << "func" << endl;
	}
};
class Y : public X
{
public:
	virtual ~Y()
	{
	}

	virtual void func() override
	{
		cout << "override func" << endl;
	}
};
void (X::*pmf)() = &X::func;
X* ptr = new Y;
(ptr->*pmf)();																											//输出：override func
虚拟机制能够在使用“指向member function之指针”的情况下运行。
对一个virtual function，其地址在编译时期是未知的，所能知道的仅是virtual function在其相关之virtual table中的索引值。也就是说，对一个virtual member function
取其地址，所能获得的只是一个索引值。
*/