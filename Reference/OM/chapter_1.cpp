/*
抽象数据类型（abstract data type，ADT）

加上封装后的布局成本：
数据成员直接内含在每一个对象之中.
成员函数虽然含在类的声明之内,却不出现在对象之中.
每一个非内联的成员函数只会诞生一个函数实例.
内联函数则会在其每一个使用者(模块)身上产生一个函数实例.
C++在布局以及存取时间上主要的额外负担是由virtual引起的:
virtual function机制,用以支持一个有效率的"执行期绑定(runtime binding)".
virtual base class,用以实现"多次出现在继承体系中的base class",有一个单一而被共享的实例.
此外,还有一些多重继承下的额外负担,发生在"一个derived class和其第二或后继之base class的转换"之间.

C++对象模式：
在C++中,有两种class data members:static和nonstatic,以及三种class member functions:static,nonstatic和virtual.

简单对象模型：
一个object是一系列的slots,每一个slot指向一个members.members按其声明顺序,各被指定一个slot.每一个data member或function
member都有自己的一个slot.在这个简单模型下,members本身并不放在object之中.只有"指向member的指针"才放在object内.这么做可以避免"members
有不同的类型,因而需要不同的存储空间"所招致的问题.object中的members是以slot的索引值来寻址的.一个class object的大小很容易计算出来:"指针大小,
乘以class中所声明的members个数"便是.虽然这个模型并没有被应用于实际产品上,不过关于索引或slot个数的观念,倒是被应用到C++的"指向成员的指针"(
pointer-to-member)观念之中.

表格对象模型：
为了对所有classes的所有objects都有一致的表达方式,另一种对象模型是把所有与members相关的信息抽出来,放在一个data member table和
一个member function table之中,class object本身则内含指向这两个表格的指针.member function table是一系列的slots,每一个slot指向一个member
function(内含函数地址),data member table则直接持有data本身.虽然这个模型也没有实际应用于真正的C++编译器身上,但member function table这个
观念却成为支持virtual functions的一个有效方案.

C++对象模型：
Stroustrup当初设计(目前仍占有优势)的C++对象模型是从简单对象模型派生而来的,并对内存空间和存取时间做了优化.在此模型中,nonstatic data members
被配置与每一个class object之内,static data members则被存放在个别的class object之外.static和nonstatic function members也被放在个别class object
之外.virtual functions则以两个步骤支持之:
1,每一个class产生出一堆指向virtual functions的指针,放在表格之中.这个表格被称为virtual table(vtbl).
2,每一个class object被安插一个指针,指向相关的virtual table.通常这个指针被称为vptr.vptr的设定(setting)和重置(resetting)都由每一个class的constructors,
destructor和copy assignment运算符自动完成.每一个class所关联的type_info object(用以支持runtime type identification,RTTI)也经由virtual table被
指出来,通常放在表格的第一个slot.这个模型的主要优点在于它的空间和存取时间的效率.主要缺点则是,如果应用程序代码本身未曾改变,但所用到的class
objects的nonstatic data members有所修改(可能是增加,移除或更改),那么那些应用程序代码同样得重新编译.关于此点,前述的双表格模型就提供了较大的
弹性,因为它多提供了一层间接性,不过它也因此付出了空间和执行效率两方面的代价.

加上继承：
C++支持单一继承,也支持多重继承,甚至,继承关系也可以指定为虚拟(virtual,也就是共享的意思).在虚拟继承的情况下,base class不管在继承串链中被派生(derived)多少
次,永远只会存在一个实例(称为subobject).
一个derived class如何在本质上模塑其base class的实例呢?在"简单对象模型"中,每一个base class可以被derived class object内的一个slot指出,该slot内
含base class subobject的地址.这个体制的主要缺点是,因为间接性而导致的空间和存取时间上的额外负担,优点则是class object的大小不会因其base classes
的改变而受到影响.
当然,你也可以想象另一种所谓的base table模型.这里所说的base class table被产生出来时,表格中的每一个slot内含一个相关的base class地址,这很像
virtual table内含每一个virtual function的地址一样.每一个class object内含一个bptr,它会被初始化,指向其base class table.这种策略的主要缺点是由于
间接性而导致的空间和存取时间上的额外负担,优点则是在每一个class object中对于继承都有一致的表现方式:每一个class object都应该在某个固定位置上安放
一个base table指针,与base classes的大小或个数无关.第二个优点是,无须改变class objects本身,就可以放大,缩小或更改base class table.
不管上述哪一种体制，“间接性”的级数将因为继承的深度而增加。如果在derived class内复制一个指针，指向继承串链中的每一个base class，倒是可以获得一个永远不变
的存取时间，当然这必须付出代价，因为需要额外的空间来放置额外的指针。
C++最初采用的继承模型并不运用任何间接性：base class subobject的data members被直接放置于derived class object中。这提供了对base class members最紧凑
而且最有效率的存取。缺点呢？当然就是：base class members的任何改变，包括增加，移除或改变类型等等，都使得所有用到“此base class或其derived class之objects”
者必须重新编译。
自C++2.0起才新导入的virtual base class，需要一些间接的base class表现方法。Virtual base class的原始模型是在class object中为每一个有关联的virtual base class
加上一个指针。其他演化出来的模型则要不是导入一个virtual base class table，就是扩充原已存在的virtual table，以便维护每一个virtual base class的位置。

对象模型如何影响程序：
不同的对象模型，会导致“现有的程序代码必须修改”以及“必须加入新的程序代码”两个结果。下面这个函数，其中class X定义了一个copy constructor，一个virtual destructor
和一个virtual function foo：
X foobar()
{
	X xx;
	X* px = new X;

	//foo()是一个virtual function
	xx.foo();
	px->foo();

	delete px;

	return xx;
}
//可能的内部转换结果
void foobar(X& _result)
{
	//构造_result，_result用来取代local xx
	_result.X::X();

	//扩展X* px = new X;
	px = _new(sizeof(X));
	if (!px)
	{
		px->X::X();
	}

	//扩展xx.foo()，不使用virtual机制，以_result取代xx
	foo(&_result);

	//使用virtual机制扩展px->foo()
	(*px->vtbl[2])(px);

	//扩展delete px
	if (!px)
	{
		(*px->vtbl[1])(px);
		_delete(px);
	}

	//无须使用named return statement
	//无须摧毁local object xx

	return;
}

策略性正确的struct：
C++中凡处于同一个access section的数据,必定保证以其声明顺序出现在内存布局当中.
然而被放置在多个access sections中的各笔数据,排列顺序就不一定了.
base classes和derived classes的data members的布局也未有谁先谁后的强制规定.
组合（composition），而非继承，才是把C和C++结合在一起的唯一可行方法。

对象的差异：
C++程序设计模型支持三种programming paradigms(程序设计范式):
1,程序模型(procedural model)
2,抽象数据类型模型(abstract data type model,ADT).此模型所谓的"抽象"是和一组表达式(public接口)一起提供的.在ADT paradigm中，程序员处理的是一个拥有固定
而单一类型的实例，它在编译时期就已经完全定义好了。
3,面向对象模型(object-oriented model)。被指定的object的真实类型在每一个特定执行点之前，是无法解析的，只有通过pointers和references的操作才能够完成。
在C++,多态只存在于一个个的public class体系中.nonpublic的派生行为以及类型为void*的指针,可以说是多态的,但它们并没有被语言明确地支持,也就是
说它们必须由程序员通过显式的转换操作来管理(你或许可以说它们并不是多态对象的一线选手).
多态的主要用途是经由一个共同的接口来影响类型的封装,这个接口通常被定义在一个抽象的base class中.这个共享接口是以virtual function机制引发的,它可
以在执行期根据object的真正类型解析出到底是哪一个函数实例被调用.
需要多少内存才能够表现一个class object?
其nonstatic data members的总和大小.
加上任何由于alignment的需求而填补(padding)上去的空间(可能存在于members之间,也可能存在于集合体边界).
加上为了支持virtual而由内部产生的任何额外负担(overhead).
alignment:就是将数值调整到某数的倍数.在32位计算机上,通常alignment为4bytes(32位),以使bus的"运输量"达到最高效率.
string在内存中的可能布局：分为两部分，1，int length,2，char*。

指针的类型：
“指向不同类型之各指针”间的差异，既不在其指针表示法不同，也不在其内容（代表一个地址）不同，而是在其所寻址出来的object类型不同。也就是说，“指针类型”会教导
编译器如何解释某个特定地址中的内存内容及其大小。这就是为什么一个类型为void*的指针只能够持有一个地址，而不能够通过它操作所指之object的缘故。

加上多态之后：
class ZooAnimal
{
public:
	ZooAnimal();
	virtual ~ZooAnimal();

	virtual void rotate();

protected:
	int loc;
	string name;
};

class Bear : public ZooAnimal
{
public:
	Bear();
	virtual ~Bear();

	virtual void rotate();

	void doSomething();

protected:
	int age;
};
假设我们的Bear object放在地址1000处，一个ZooAnimal指针和一个Bear指针有什么不同？
Bear b;
ZooAnimal* pz = &b;
Bear* pb = &b;
它们每个都指向Bear object的第一个byte。其间的差别是，pb所涵盖的地址包含整个Bear object，而pz所涵盖的地址只包含Bear object中的ZooAnimal subobject。
除了ZooAnimal subobject中出现的members，你不能够使用pz来直接处理Bear的任何members。唯一例外是通过virtual机制：
//不合法，doSomething()不是ZooAnimal的一个member，虽然我们知道pz目前指向一个Bear object
pz->doSomething();
//OK,经过一个显式的downcast操作就没有问题
(static_cast<Bear*>(pz))->doSomething();
//这样更好，但它是一个run-time operation
if(Bear* pb2 = dynamic_cast<Bear*>(pz))
{
	pb2->doSomething();
}
当我们写：
pz->rotate();
时，pz的类型将在编译时期决定以下两点：
1，固定的可用接口，也就是说，pz只能够调用ZooAnimal的public接口。
2，该接口的access level。
在执行点，pz所指的object类型可以决定rotate()所调用的实例。
Bear b;
ZooAnimal za = b;															//这会引起切割（sliced）
za.rotate();																		//调用ZooAnimal::rotate()
如果初始化函数（上述assignment操作发生时）将一个object内容完整拷贝到另一个object去，为什么za的vptr不指向Bear的virtual table？
编译器在初始化及assignment操作时，确保如果某个object含有一个或一个以上的vptrs，那些vptrs的内容不会被base class object初始化或改变。
一个pointer或一个reference之所以支持多态，是因为它们并不引发内存中任何“与类型有关的内存委托操作（type-dependent commitment）”，会受到改变的，只有它们
所指向的内存的“大小和内容解释方式”而已。
当一个base class object被直接初始化为（或assignment）一个derived class object时，derived object就会被切割（sliced）以塞入较小的base type内存中，derived 
type将没有留下任何蛛丝马迹，多态于是不再呈现。
C++通过class的pointers和references来支持多态,这种程序设计风格就称为"面向对象".
*/