/*
构造，析构，拷贝语意学：

class AbstractClass
{
protected:
	AbstractClass(char* value = 0) :
		_mumble(value)
	{
	}

public:
	virtual ~AbstractClass();
	virtual void interface() = 0;

	const char* mumble() const
	{
		return _mumble;
	}

protected:
	char* _mumble;
};
如果没有这个构造函数的初始化操作的话，那么其derived class的局部性对象_mumble将无法决定初值。
将“被共享的数据”抽取出来放在base class中，也是一种正当的设计。

纯虚函数的存在：
每一个derived class destructor会被编译器加以扩张，以静态调用的方式调用其“每一个virtual base class”以及“上一层base class”的destructor。因此，只要缺乏
任何一个base class destructor的定义，就会导致链接失败。
不要把virtual destructor声明为pure。

虚拟规格中const的存在：
在抽象接口类中的虚函数，是否要设计为const的呢？建议是：尽量少用，除非特别明确。

继承体系下的对象构造：
1，记录在member initialization list中的data members初始化操作会被放进constructor的函数本体，并以members的声明顺序为顺序。
2，如果有一个member并没有出现在member initialization list之中，但它有一个default constructor，那么该default constructor必须被调用。
3，如果class object有virtual table pointer(s)，它（们）必须被设定初值，指向适当的virtual table(s)。
4，所有上一层的base class constructors必须被调用，以base class的声明顺序为顺序（与member initialization list中的顺序没关联）：
	a，如果base class被列于member initialization list中，那么任何显式指定的参数都应该传递过去。
	b，如果base class没有被列于member initialization list中，而它有default constructor（或default memberwise copy constructor），那么就调用之。
	c，如果base class是多重继承下的第二或后继的base class，那么this指针必须有所调整。
5，所有virtual base class constructors必须被调用，从左到右，从最深到最浅：
	a，如果class被列于member initialization list中，那么如果有任何显式指定的参数，都应该传递过去。如果没有被列于list之中，而其有default constructor，那
	么调用之。
	b，class中的每一个virtual base class subobject的偏移位置（offset）必须在执行期可被存取。
	c，如果class object是最底层（most-derived）的class，其constructors可能被调用，某些用以支持这一行为的机制必须被放进来。

vptr初始化语意学：
在base class constructors调用操作之后，但是在程序员供应的代码或是“member initialization list中所列的members初始化操作”之前，做vptr的初始化操作。

析构语意学：
如果class没有定义destructor，那么只有在class内含的member object（或class自己的base class）拥有destructor的情况下，编译器才会自动合成出一个来。否则，
destructor被视为不需要，也就不需被合成（当然更不需要被调用）。
*/