/*
构造函数语意学：

tirvial，没有用的
nontrivial，有用的
memberwise，对每一个member施以...
bitwise，对每一个bit施以...

事实上，关键字explicit之所以被导入这个语言，就是为了给程序员提供一种方法，使他们能够制止“单一参数的constructor”被当作一个conversion运算符。

Default Constructor的构造操作：
Global objects的内存保证会在程序启动的时候被清为0.
Local objects配置于程序的堆栈中,Heap objects配置于自由空间中,都不一定会被清为0,它们的内容将是内存上次被使用后的遗迹.

对于class X,如果没有任何user-declared constructor,那么会有一个default constructor被隐式(implicitly)声明出来,一个被隐式声明出来的default
constructor将是一个trivial(浅薄而无能,没啥用的) constructor.
下面4个小节分别讨论nontrivial default constructor的4种情况。

1:带有Default Constructor的Member Class Object
如果一个class没有任何constructor,但它内含一个member object,而后者有default constructor,那么这个class的implicit default constructor就是
"nontrivial",编译器需要为该class合成出一个default constructor.不过这个合成操作只有在constructor真正需要被调用时才会发生.
于是出现了一个有趣的问题:在C++各个不同的编译模块中,编译器如何避免合成出多个default constructor呢?解决方法是把合成的default constructor、
copy constructor、destructor、assignment copy operator都以inline方式完成.一个inline函数有静态链接(static linkage),不会被文件以外者看到.如果
函数太复杂,不适合做成inline,就会合成出一个explicit non-inline static实例.
举例,在下面的程序片段中,编译器为class Bar合成一个default constructor:
class Foo
{
public:
	Foo();
	Foo(int);
};

class Bar
{
public:
	Foo foo;
	char* str;
};

void foo_bar()
{
	Bar bar;															//Bar::foo必须在此处初始化.因为Bar::foo是一个member object,而其class Foo拥有default constructor
}
被合成的Bar default constructor内含必要的代码,能够调用class Foo的default constructor来处理member object Bar::foo,但它并不产生任何代码来
初始化Bar::str.是的,将Bar::foo初始化是编译器的责任,将Bar::str初始化则是程序员的责任.被合成的default constructor看起来可能像这样:
inline Bar::Bar()
{
	foo.Foo:Foo();												//伪代码
}
再一次请注意,合成的default constructor只满足编译器的需要,而不是程序的需要.
让我们假设程序员经由下面的default constructor提供了str的初始化操作:
//程序员定义的default constructor
Bar::Bar()
{
	str = 0;
}
现在程序的需求获得满足了，但是编译器还需要初始化member object foo。
由于default constructor已经被显式地定义出来了,编译器没办法合成第二个.编译器的行动是:如果class A内含一个或一个以上的member class objects,
那么class A的每一个constructor必须调用每一个member classes的default constructor.编译器会扩张已存在的constructors,在其中安插一些代码,使
得user code被执行之前,先调用必要的default constructors.
//扩张后的default constructor可能像这样:
Bar::Bar()
{
	foo.Foo::Foo();												//附加上的compiler code
	str = 0;															//explicit user code
}
如果有多个class member objects都要求constructor初始化操作,将如何?
C++语言要求以"member objects在class中的声明顺序"来调用各个constructors.这一点由编译器完成,它为每一个constructor安插程序代码,以"member声
明顺序"调用每一个member所关联的default constructors.这些代码将被安插在explicit user code之前.

2:带有Default Constructor的Base Class
如果一个没有任何constructors的class派生自一个"带有default constructor"的base class,那么这个derived class的default constructor会被视为
nontrivial,并因此需要被合成出来.它将调用上一层base classes的default constructor(根据它们的声明顺序)。对于一个后继派生的class而言，这个合成的constructor
和一个“被显式提供的default constructor”没有什么差异。
如果设计者提供多个constructors,但其中都没有default constructor呢?编译器会扩张现有的每一个constructors,将"用以调用所有必要之default
constructors"的程序代码加进去.它不会合成一个新的default constructor,因为其他"由user所提供的constructors"存在的缘故.如果同时亦存在着"带有
default constructors"的member class objects,那些default constructor也会被调用---在所有base class constructor都被调用之后.

3:带有一个virtual Function的class
class声明(或继承)一个virtual function.
下面两个扩张行动会在编译期间发生:
a,一个virtual function table会被编译器产生出来,内放class的virtual functions地址.
b,在每一个class object中,一个额外的pointer member会被编译器合成出来,内含相关之class vbtl的地址.
此外,虚拟调用操作(virtual invocation)会被重新改写,以使用vptr和vtbl中的条目.
为了让这个机制发挥功效,编译器必须为该class的每一个object的vptr设定初值,放置适当的virtual table地址.对于class所定义的每一个constructor,编译
器会安插一些代码来做这样的事情.对于那些未声明任何constructors的classes,编译器会为它们合成一个default constructor,以便正确地初始化每一个class
object的vptr.

4:带有一个Virtual Base Class的Class
class派生自一个继承串链，其中有一个或更多的virtual base classes。
virtual base class的实现法在不同的编译器之间有极大的差异.然而,每一种实现法的共同点在于必须使virtual base class在其每一个derived class object中
的位置,能够于执行期准备妥当.
对于class所定义的每一个constructor,编译器会安插那些"允许每一个virtual base class的执行期存取操作"的代码.如果class没有声明任何constructors,编
译器必须为它合成一个default constructor.

总结：
C++标准把那些合成物称为implicit nontrivial default constructors.被合成出来的constructor只能满足编译器（而非程序）的需要。
至于没有存在上面那4种情况而又没有声明任何constructor的classes,我们说它们拥有的是implicit trivial default constructors,它们实际上并不会被合成出来.
在合成的default constructor中,只有base class subobjects和member class objects会被初始化.所有其他的nonstatic data member(如整数、整数指针、
整数数组等等)都不会被初始化.这些初始化操作对程序而言或许有需要,但对编译器则非必要.如果程序需要一个"把某指针设为0"的default constructor,那么
提供它的人应该是程序员.

Copy Constructor的构造操作：
有三种情况，会以一个object的内容作为另一个class object的初值。
1，定义一个class object的时候通过“=”来初始化。
2，当object被当作参数交给某个函数时。
3，当函数传回一个class object时。

Default Memberwise Initialization：
如果class没有提供一个explicit copy constructor，那么当class object以“相同class的另一个object”作为初值，其内部是以所谓的default memberwise initialization
手法完成的，也就是把每一个内建的或派生的data member（例如一个指针或一个数组）的值，从某个object拷贝一份到另一个object身上。不过它并不会拷贝其中的member 
class object，而是以递归的方式施行memberwise initialization。例如：
class String
{
private:
	char* str;
	int len;
};
一个String object的default memberwise initialization发生在这种情况之下：
String noun("book");
String verb = noun;
其完成方式就好像个别设定每一个members一样：
//语意相等
verb.str = noun.str;
verb.len = noun.len;
如果一个String object被声明为另一个class的member，像这样：
class Word
{
private:
	int _occurs;
	String _word;																			//String object成为class Word的一个member
};
那么一个Word object的default memberwise initialization会拷贝其内建的member _occurs，然后再于String member object _word身上递归实施memberwise 
initialization。

一个class object可用两种方式复制得到，一种是被初始化，另一种是被指定（assignment）。从概念上而言，这两个操作分别是以copy constructor和copy assignment 
operator完成的。

C++标准把copy constructor区分为trivial和nontrivial两种。只有nontrivial的实例才会被合成于程序之中，决定一个copy constructor是否为trivial的标准在于class
是否展现出所谓的“bitwise copy semantics”。

Bitwise Copy Semantics（位逐次拷贝）：
在下面的程序片段中：
Word noun("book");

void foo()
{
	Word verb = noun;
}
如果class Word没有定义explicit copy constructor，那么是否会有一个编译器合成的实例被调用呢？这就得视该class是否展现“bitwise copy semantics”而定了。举个
例子，已知下面的class Word声明：
//以下声明展现了bitwise copy semantics
class Word
{
public:
	Word(const char*);
	~Word()
	{
		delete [] str;
	}

private:
	int cnt;
	char* str;
};
这种情况下并不需要合成出一个default copy constructor，因为上述声明展现了“default copy semantics”。
当然，因为这导致了verb和noun指向了相同的字符串，在退出foo()之前，local object verb会执行destructor，
于是字符串被删除，global object noun从此指向一堆无意义之物。member str的问题只能够靠“由class设计者实现出一个explicit copy constructor以改写default 
memberwise initialization”来解决。
然而，如果class Word是这样声明的：
//以下声明并未展现出bitwise copy semantics
class Word
{
public:
	Word(const String&);
	~Word();

private:
	int cnt;
	String str;
};
其中String声明了一个explicit copy constructor：
class String
{
public:
	String(const char*);
	~String();

	String(const String&);
};
这种情况下，编译器必须合成出一个copy constructor，以便调用member class String object的copy constructor：
//一个被合成出来的copy constructor，伪码
inline Word::Word(const Word& wd)
{
	str.String::String(wd.str);
	cnt = wd.cnt;
}
在这被合成出来的copy constructor中，如整数，指针，数组等等的nonclass members也都会被复制。

不要Bitwise Copy Semantics：
什么时候一个class不展现出“bitwise copy semantics”呢？有4种情况：
1，当class内含一个member object，而后者的class声明了一个copy constructor时（不论是被class设计者显式地声明，像String那样，还是被编译器合成，像Word
那样）。
2，当class继承自一个base class而后者存在一个copy constructor时（不论是被显式声明或是被合成而得）。
3，当class声明了一个或多个virtual functions时。
4，当class派生自一个继承串链，其中有一个或多个virtual base classes时。
在前两种情况中，编译器必须将member或base class的“copy constructors调用操作”安插到被合成的copy constructor中。情况3，4有点复杂，我们在下面讨论。

重新设定Virtual Table的指针：
编译期间的两个扩张操作（只要有一个class声明了一个或多个virtual functions）：
1，增加一个virtual function table（vtbl），内含每一个有作用的virtual function的地址。
2，一个指向virtual function table的指针（vptr），安插在每一个class object内。
如果编译器对于每一个新产生的class object的vptr不能成功而正确地设好其初值，将导致可怕的后果。因此，当编译器导入一个vptr到class之中时，该class就不再展现
bitwise semantics了。下面的例子，编译器需要合成出一个copy constructor以求将vptr适当地初始化：
class ZooAnimal
{
public:
	ZooAnimal();
	virtual ~ZooAnimal();

	virtual void animate();
	virtual void draw();
};

class Bear : public ZooAnimal
{
public:
	Bear();
	virtual ~Bear();

	virtual void animate();
	virtual void draw();

	virtual void dance();
};
ZooAnimal class object以另一个ZooAnimal class object作为初值，或Bear class object以另一个Bear class object作为初值，都可以直接靠“bitwise copy semantics”
完成（除了可能会有pointer member之外）。举个例子：
Bear yogi;
Bear winnie = yogi;
yogi会被default Bear constructor初始化，而在constructor中，yogi的vptr被设定指向Bear class的virtual table（靠编译器安插的代码完成）。因此，把yogi的vptr值
拷贝给winnie的vptr是安全的。
当一个base class object以其derived class的object内容做初始化操作时，其vptr复制操作也必须保证安全，例如：
ZooAnimal franny = yogi;																				//这会发生切割（sliced）行为
franny的vptr不可以被设定指向Bear class的virtual table（如果yogi的vptr被直接“bitwise copy”的话，就会导致此结果），否则当下面的程序片段中的draw()被调用而
franny被传进去时，就会导致灾难。（通过franny调用virtual function draw()，调用的是ZooAnimal实例而非Bear实例（虽然franny是以Bear object yogi作为初值），
因为franny是一个ZooAnimal object。事实上，yogi中的Bear部分已经在franny初始化时被切割（sliced）掉了。如果franny被声明为一个reference（或如果它是一个指针，
而其值为yogi的地址），那么经由franny所调用的draw()才会是Bear的函数实例）：
void draw(const ZooAnimal& zoey)
{
	zoey.draw();
}

void foo()
{
	//franny的vptr指向ZooAnimal的virtual table，而非Bear的virtual table
	ZooAnimal franny = yogi;

	draw(yogi);																					//调用Bear::draw()
	draw(franny);																					//调用ZooAnimal::draw()
}
也就是说，合成出来的ZooAnimal copy constructor会显式设定object的vptr指向ZooAnimal class的virtual table，而不是直接从右手边的class object中将其vptr现
值拷贝过来。

处理Virtual Base Class Subobject：
Virtual base class的存在需要特别处理。一个class object如果以另一个object作为初值，而后者有一个virtual base class subobject，那么“bitwise copy semantics”
就会失效。
每一个编译器对于虚继承的支持承诺，都必须让“derived class object中的virtual base class subobject位置”在执行期就准备妥当。维护“位置的完整性”是编译器的责任。
“Bitwise copy semantics”可能会破坏这个位置，所以编译器必须在它自己合成出来的copy constructor中做出仲裁。
class Raccoon : public virtual ZooAnimal
{
public:
	Raccoon();
	Raccoon(int val);
};
编译器所产生的代码（用以调用ZooAnimal的default constructor，将Raccoon的vptr初始化，并定位出Raccoon中的ZooAnimal subobject）被安插在两个Raccoon 
constructors之内，成为其“先头部队”。
问题并不发生于“一个class object以另一个同类的object作为初值”之时，而是发生于“一个class object以其derived classes的某个object作为初值”之时。例如下面的例子：
class RedPanda : public Raccoon
{
public:
	RedPanda();
	RedPanda(int val);
};
再强调一次，如果以一个Raccoon object作为另一个Raccoon object的初值，那么“bitwise copy”就绰绰有余了：
//简单的bitwise copy就足够了
Raccoon rocky;
Raccoon little_critter = rocky;
然而如果企图以一个RedPanda object作为little_critter的初值，编译器必须判断“后续当程序员企图存取其ZooAnimal subobject时是否能够正确地执行”：
//简单的bitwise copy不够，编译器必须显式地将little_critter的virtual base class pointer/offset初始化
RedPanda little_red;
Raccoon little_critter = little_red;
在这种情况下，为了完成正确的little_critter初值设定，编译器必须合成一个copy constructor，安插一些代码以设定virtual base class pointer/offset的初值，对每一个
members执行必要的memberwise初始化操作，以及执行其他的内存相关工作。

总结：上述4中情况下，class不再保持“bitwise copy semantics”，而且default copy constructor如果未被声明的话，会被视为nontrivial。在这4种情况下，如果缺乏
一个已声明的copy constructor，编译器为了正确处理“以一个class object作为另一个class object的初值”，必须合成出一个copy constructor。

显式的初始化操作：
已知有这样的定义：
X x0;
下面的三个定义，每一个都明显地以x0来初始化其class object：
void foo_bar()
{
	X x1(x0)																											//定义了x1
	X x2 = x0;																											//定义了x2
	X x3 = X(x0);																										//定义了x3
}
必要的程序转化有两个阶段：
1，重写每一个定义，其中的初始化操作会被剥除（“定义”是指“占用内存”的行为）。
2，class的copy constructor调用操作会被安插进去。
在明确的两阶段转化之后，foo_bar()可能看起来像这样：
void foo_bar()
{
	X x1;																													//定义被重写，初始化操作被剥除
	X x2;
	X x3;

	//编译器安插X copy construction的调用操作
	x1.X::X(x0);
	x2.X::X(x0);
	x3.X::X(x0);
}

在编译器层面做优化：
X bar()
{
	X xx;

	return xx;
}
编译器以__result参数取代named return value “xx”：
void bar(X& __result)
{
	//default constructor被调用
	__result.X::X();

	return;
}
这样的编译器优化，被称为Named Return Value(NRV)优化。NRV优化通过copy constructor来激活。

Copy Constructor：要还是不要？
已知下面的类：
class Point3d
{
public:
	Point3d(float x,float y,float z);

private:
	float _x,_y,_z;
};
这个class的设计者应该提供一个explicit copy constructor吗？
上述class的default copy constructor被视为trivial。它既没有任何member（或base）class objects带有copy constructor，也没有任何的virtual base class或virtual 
function。所以，默认情况下，一个Point3d class object的“memberwise”初始化操作会导致“bitwise copy”。三个坐标成员是以数值来存储的，bitwise copy既不会导致
memory leak，也不会产生address aliasing，因此它既快速，又安全。所以没有必要提供一个explicit copy constructor。但是，如果你被问及是否预见class需要大量的
memberwise初始化操作，例如，以传值（by value）的方式传回objects？如果答案是Yes，那么提供一个copy constructor的explicit inline函数实例就非常合理---在“你
的编译器提供NRV优化”的前提下。
实现copy constructor的最简单方法像这样：
Point3d::Point3d(const Point3d& rhs)
{
	_x = rhs._x;
	_y = rhs._y;
	_z = rhs._z;
}
这没问题，但使用C++库的memcpy()会更有效率：
Point3d::Point3d(const Point3d& rhs)
{
	memcpy(this,&rhs,sizeof(Point3d));
}
然而，不管使用memcpy()还是memset()，都只有在“classes不含任何由编译器产生的内部members”时才能有效运行。如果Point3d class声明一个或多个virtual functions，
或内含一个virtual base class，那么使用上述函数将会导致那些“被编译器产生的内部members”的初值被改写。例如：
class Shape
{
public:
	//这会改变内部的vptr
	Shape()
	{
		memset(this,0,sizeof(Shape));
	}

	virtual ~Shape();
};
编译器为此constructor扩张的内容看起来像这样：
Shape::Shape()
{
	//vptr必须在user的代码执行之前先设定妥当
	__vptr__Shape = __vtbl__Shape;

	//memset会将vptr清为0
	memset(this,0,sizeof(Shape));
}

成员初始化列表（Member Initialization List）：
下面4中情况下，必须使用member initialization list：
1，初始化一个reference member。
2，初始化一个const member。
3，调用一个base class的constructor，而它拥有一组参数。
4，调用一个member class的constructor，而它拥有一组参数。

class Word
{
public:
	Word()
	{
		name = 0;
		cnt = 0;
	}

private:
	String name;
	int cnt;
}
上述代码，Word constructor会先产生一个临时的String object，然后将它初始化，之后以一个assignment运算符将临时的object指定给name，随后再摧毁那个临时
object。以下是constructor可能的内部扩张结果：
Word::Word()
{
	//调用String的default constructor
	name.String::String();

	//产生临时对象
	String temp = String(0);

	//"memberwise"地拷贝name
	name.String::operator=(temp);

	//摧毁临时对象
	temp.String::~String();

	cnt = 0;
}

Word() :
	name(0)
{
	cnt = 0;
}
会被扩张成下面这样：
Word()
{
	//调用String(int) constructor
	name.String::String(0);

	cnt = 0;
}

编译器会一一操作initialization list，以声明的顺序在constructor之内安插初始化操作，并放置于任何explicit user code之前。
*/