/*
环境：
32位计算机，VS14.0

-------------------------------------------------------------------------------------------------------------------------------------------------------

Data语意学：
class X
{
};

class Y : public virtual X
{
};

class Z : public virtual X
{
};

class A : public Y,public Z
{
};

cout << "X:" << sizeof(X) << endl;												//输出：1
cout << "Y:" << sizeof(Y) << endl;												//输出：4
cout << "Z:" << sizeof(Z) << endl;												//输出：4
cout << "A:" << sizeof(A) << endl;												//输出：8

X并不是空的，它有一个隐藏的1byte大小，那是被编译器安插进去的一个char。这使得这一class的两个objects得以在内存中配置独一无二的地址。
对Y和Z的大小有影响的三个因素：
1，语言本身所造成的额外负担（overhead）。
当语言支持virtual base classes时，就会导致一些额外负担。在derived class中，这个额外负担反映在某种形式的指针身上，它或者指向virtual base class subobject，
或者指向一个相关表格，表格中存放的若不是virtual base class subobject的地址，就是其偏移位置。
2，编译器对于特殊情况所提供的优化处理。
3，Alignment的限制。

Empty virtual base class已经成为C++ OO设计的一个特有术语了。它提供一个virtual interface，没有定义任何数据。有些编译器对此提供了特殊的处理：一个empty 
virtual base class被视为derived class object最开头的一部分，也就是说它并没有花费任何的额外空间。

记住，一个virtual base class subobject只会在derived class中存在一份实例，不管它在class继承体系中出现了多少次。

static data members，被放置在程序的一个global data segment中，不会影响个别的class object的大小。在程序中，不管一个class被产生出多少个objects（经由
直接产生或间接派生），static data members永远只存在一份实例（甚至即使该class没有任何object实例，其static data members也已存在）。但是一个template 
class的static data members的行为稍有不同。

编译器会在下面两个方面来影响一个object的大小：
1，由编译器自动加上的额外data members，用以支持某些语言特性（各种virtual特性）。
2，alignment（边界调整）的需要。边界调整是由处理器（processor）来决定的，使object能够符合一台机器的word边界。

Data Member的布局：
Access sections的多寡并不会招来额外负担。例如在一个section中声明8个members，或是在8个sections中总共声明8个members，得到的object大小是一样的。
下面这个template function，接受两个data members，然后判断谁先出现在class object之中。如果两个members都是不同的access sections中的第一个被声明
者，此函数就可以用来判断哪一个section先出现：
template<typename class_type,typename data1_type,typename data2_type>
char* accessOrder(data1_type class_type::*data1,data2_type class_type::*data2)
{
	assert(data1 != data2);

	return data1 < data2 ? "data1 occurs first" : "data2 occurs first";
}
用法：
accessOrder(&Point3d::z,&Point3d::y);

Static Data Members：
Static data members，被编译器提出于class之外，并被视为一个global变量（但只在class生命范围之内可见）。
“经由member selection operator（“.”运算符）对一个static data member进行存取操作”只是文法上的一种便宜行事而已。static data member并不在object之中，
因此存取static data member并不需要通过class object。
不论static data members是否是从一个复杂的继承关系中继承而来的，比如它是某个virtual base class的，就其存取而言，都是无关紧要的，它只有唯一一个实例，其
存取路径都是直接的。

如果取一个static data member的地址，会得到一个指向其数据类型的指针，而不是一个指向其class member的指针，因为static data member并不内含在一个class 
object之中。例如：
&Point3d::chunkSize;
会获得类型如下的内存地址：
int const*

如果两个classes中的static data member名字相同，就存在冲突问题。编译器的解决方法是暗中对每一个static data member编码（name-mangling），以获得一个
独一无二的程序识别代码。

Nonstatic Data Members:
Nonstatic data members直接存放在每一个class object之中。除非经由显式的或隐式的class object，否则没有办法直接存取它们。只要程序员在一个member function
中直接处理一个nonstatic data member，所谓“implicit class object”就会发生。例如：
Point3d Point3d::translate(Point3d const& pt)
{
	x += pt.x;
	y += pt.y;
	z += pt.z;
}
表面上所看到的对于x，y，z的直接存取，事实上是经由一个“implicit class object”（由this指针表达）完成的。事实上这个函数是这样的：
Point3d Point3d::translate(Point3d* const this,Point3d const& pt)
{
	this->x += pt.x;
	this->y += pt.y;
	this->z += pt.z;
}

要对一个nonstatic data member进行存取操作，编译器需要把class object的起始地址加上data member的偏移位置（offset）。例如：
origin._y = 0.0;
那么地址&origin._y等于：
&origin + (&Point3d::_y - 1);
指向data member的指针，其offset值总是被加上1，这样可以使编译系统区分出“一个指向data member的指针，用以指出class的第一个member”和“一个指向
data member的指针，没有指出任何member”两种情况。

每一个nonstatic data member的偏移位置（offset）在编译时期即可获知，甚至如果member属于一个base class subobject（派生自单一或多重继承串链）也是
一样的。因此，存取一个nonstatic data member，其效率和存取一个C struct member或一个nonderived class的member是一样的。

虚继承将为“经由base class subobject存取class members”导入一层新的间接性，比如：
Point3d* pt3d;
pt3d->_x = 0.0;
其执行效率在_x是一个struct member，一个class member，单一继承，多重继承的情况下都完全相同。
但如果_x是一个virtual base class的member，存取速度会稍慢一点。
origin.x = 0.0;
pt->x = 0.0;
当Point3d是一个derived class，而其继承结构中有一个virtual base class，并且被存取的member（x）是一个从该virtual base class继承而来的member时，上
面两个语句就有差异。这时候我们不能够说pt必然指向哪一种class type（因此，我们也就不知道编译时期这个member真正的offset），所以这个存取操作必须延迟至
执行期，经由一个额外的间接导引，才能够解决。但如果使用origin，就不会有这个问题，其类型无疑是Point3d class，而即使它继承自virtual base class，members
的offset位置也在编译时期就固定了。

“继承”与Data Member：
一个derived class object所表现出来的东西，是其自己的members加上其base classes members的总和。
在大部分编译器上头，base class members总是先出现，但属于virtual base class的除外。

只要继承不要多态：
一般而言，具体继承（concrete inheritance，相对于虚继承virtual inheritance）并不会增加空间或存取时间上的额外负担。

C++语言保证：出现在derived class中的base class subobject有其完整原样性。例如：
class X
{
	int a;
	bool b;
};

class Y : public X
{
	bool c;
};

class Z : public Y
{
	bool d;
};
cout << sizeof(xx) << endl;																//输出：8
cout << sizeof(yy) << endl;																//输出：12
cout << sizeof(zz) << endl;																//输出：16
也就是说，编译器不会把base class object原本的填补空间让出来给derived class members使用。如果这样的话，那么当发生复制base subobject的时候，就会
破坏derived class object的members。

加上多态：
class X
{
public:
	virturl ~X();
};

class Y : public X
{
};
每个Y class object内含一个额外的vptr member（继承自X）。

如果基类没有虚函数，而子类有虚函数，那么这种情况下，把一个子类对象转换为其基类型，就需要编译器的介入，用以调整地址（因vptr插入之故）。

多重继承：
class Z : public X,public Y
{
};
Z的对象中，会包含vptr_x和vptr_y。

对于一个多重派生对象，将其地址指定给“最左端base class的指针”，情况将和单一继承时相同，因为二者都指向相同的起始地址。需付出的成本只有地址的指定操作
而已。至于第二个或后继的base class的地址指定操作，则需要将地址修改：加上（或减去，如果downcast的话）介于中间的base class subobject(s)大小。例如：
Y* py;
Z zz;
py = (Y*)( ((char*)&zz) + sizeof(X) );

如果要存取第二个（后继）base class中的一个data member需要付出额外的成本吗？不，members的位置在编译时就固定了，因此存取members只是一个简单的
offset运算，就像单一继承一样简单---不管是经由一个指针，一个引用或一个对象来存取。

虚继承：
多重继承的一个语意上的副作用就是，它必须支持某种形式的“shared subobject继承”。
VS引入所谓的virtual base class table，每一个class object如果有一个或多个virtual base classes，就会由编译器安插一个指针，指向virtual base class table。至于
真正的virtual base class指针，是被放在该表格中。
virtual base class最有效的一种运用形式就是：一个抽象的virtual base class，没有任何data members。

-------------------------------------------------------------------------------------------------------------------------------------------------------

测试：
class X
{
};
cout << sizeof(X) << endl;															//输出：1

class X
{
public:
	bool mBool;
};
cout << sizeof(X) << endl;															//输出：1

class X
{
public:
	short mShort;
};
cout << sizeof(X) << endl;															//输出：2

class X
{
public:
	bool mBool;
	short mShort;
};
cout << sizeof(X) << endl;															//输出：4

class X
{
public:
	bool mBool1;
	bool mBool2;
	short mShort;
};
cout << sizeof(X) << endl;															//输出：4

class X
{
public:
	bool mBool1;
	short mShort;
	bool mBool2;
};
cout << sizeof(X) << endl;															//输出：6

class X
{
public:
	bool mBool1;
	int mInt;
	bool mBool2;
};
cout << sizeof(X) << endl;															//输出：12

--------------------------------------------------------------------------------------

//带有自己的虚函数
class X
{
public:
	virtual ~X()
	{
	}
};
cout << sizeof(X) << endl;															//输出：4

//普通继承，没有虚函数
class X
{
};
class Y : public X
{
};
cout << sizeof(Y) << endl;															//输出：1

//普通继承，有虚函数
class X
{
public:
	virtual ~X()
	{
	}
};
class Y : public X
{
public:
	virtual ~Y()
	{
	}

	virtual void func()
	{
	}
};
cout << sizeof(Y) << endl;															//输出：4

//虚继承，没有虚函数
class X
{
};
class Y : public virtual X
{
};
cout << sizeof(Y) << endl;															//输出：4

//虚继承，有虚函数，子类没有新增虚函数
class X
{
public:
	virtual ~X()
	{
	}
};
class Y : public virtual X
{
public:
	virtual ~Y()
	{
	}
};
cout << sizeof(Y) << endl;															//输出：8

//虚继承，有虚函数，子类有新增虚函数
class X
{
public:
	virtual ~X()
	{
	}
};
class Y : public virtual X
{
public:
	virtual ~Y()
	{
	}

	virtual void func()
	{
	}
};
cout << sizeof(Y) << endl;															//输出：12

--------------------------------------------------------------------------------------

//普通继承，查看基类数据成员和子类数据成员的布局
class X
{
public:
	virtual ~X()
	{
	}

	int mBaseData = 5;
};
class Y : public X
{
public:
	virtual ~Y()
	{
	}

	int mDerivedData = 6;
};
char* pObj = reinterpret_cast<char*>(&yy);
int* pData = reinterpret_cast<int*>(pObj + 4);								//输出5
int* pData = reinterpret_cast<int*>(pObj + 8);								//输出6
cout << *pData << endl;

//虚继承，查看基类数据成员和子类数据成员的布局
class X
{
public:
	virtual ~X()
	{
	}

	int mBaseData = 5;
};
class Y : public virtual X
{
public:
	virtual ~Y()
	{
	}

	int mDerivedData = 6;
};
char* pObj = reinterpret_cast<char*>(&yy);
int* pData = reinterpret_cast<int*>(pObj + 4);									//输出6
int* pData = reinterpret_cast<int*>(pObj + 12);								//输出5
cout << *pData << endl;
*/