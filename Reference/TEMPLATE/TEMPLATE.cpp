/*
关键字:
函数模板，实参的演绎
函数模板，重载
类模板，类模板特化
类模板局部特化
缺省模板实参
非类型的类模板参数，非类型的函数模板参数，非类型模板参数的限制
关键字typename，使用this->
成员模板
模板的模板参数
零初始化
包含模型，显式实例化
显式特化，局部特化
template-id
虚成员函数，模板的链接，基本模板
模板实参，SFINAE
类型实参
非类型实参
模板的模板实参，实参的等价性
友元
派生和类模板

----------------------------------------------------------------------------------------------------

函数模板，实参的演绎:
函数模板是那些被参数化的函数，它们代表的是一个函数家族。
函数模板和普通的函数，唯一的区别就是有些函数元素是未确定的：这些元素将在使用时被参数化。

返回两个值中最大者的函数模板。
类型参数T表示的是，调用者调用这个函数时所指定的任意类型。你可以使用任何类型来实例化该类型参数，只要所用类型提供模板使用的操作就可以。例如，在这里
的例子中，类型T需要支持operator<。

函数模板有两种类型的参数：
1：模板参数：位于函数模板名称的前面，在一对尖括号内部进行声明：T。
2：调用参数：位于函数模板名称的后面，在一对圆括号内部进行声明：a,b。

template <typename T>
inline T const& max(T const& a, T const& b)
{
	return a < b ? b : a;
}

int main()
{
	可以看到：max()模板调用的前面都有域限定符::，这是为了确认我们调用的是全局名字空间中的max()。因为标准库也有一个std::max()模板，避免产生二义性。
	对于实例化模板参数的每种类型，都从模板产生出一个不同的实体。因此，针对4中类型中的每一种，max()都被编译了一次。
	这种用具体类型代替模板参数的过程叫做实例化(instantiation)。它产生了一个模板的实例。

	cout << "调用max():" << endl;
	cout << ::max(3, 9) << endl;
	cout << ::max(4.2, 8.1) << endl;
	cout << ::max('x', 'y') << endl;
	cout << ::max(string("abc"), string("bcde")) << endl;

	如果试图基于一个不支持模板内部所使用操作的类型实例化一个模板，那么将会导致一个编译期错误。

	complex<float> c1, c2;
	//::max(c1, c2);

	于是，我们可以得出一个结论：模板被编译了两次，分别发生在
	1：实例化之前，先检查模板代码本身，查看语法是否正确。在这里会发现错误的语法，如遗漏分号等。
	2：在实例化期间，检查模板代码，查看是否所有的调用都有效。在这里会发现无效的调用，如该实例化类型不支持某些函数调用等。

	这给实际中的模板处理带来了一个很重要的问题：但使用函数模板，并且引发模板实例化的时候，编译器需要查看模板的定义。这就不同于普通函数中编译和链接
	之间的区别，因为对于普通函数而言，只要有该函数的声明（即不需要定义），就可以顺利通过编译。

	实参的演绎(deduction):
	如果我们传递了两个int给参数类型T const&，那么编译器能够得出结论：T必须是int。
	注意：不允许进行自动类型转换。每个T都必须正确地匹配。

	//::max(4, 4.2);

	有3种方法可以用来处理上面这个错误：
	1：对实参进行强制类型转换。
	2：显式指定T的类型。
	3：指定两个参数可以具有不同的类型。

	cout << ::max(static_cast<double>(4),4.2) << endl;
	cout << ::max<double>(4, 4.2) << endl;

	return 0;
}

----------------------------------------------------------------------------------------------------

把第2个参数转型为返回类型的过程将会创建一个新的局部临时对象，这导致了不能通过引用来返回结果。

template <typename T1, typename T2>
inline T1 max(T1 const& a, T2 const& b)
{
	return a < b ? b : a;
}

模板实参演绎并不适合返回类型，因为RT不会出现在函数调用参数的类型里面。因此函数调用并不能演绎出RT。

template <typename T1, typename T2, typename RT>
inline RT max(T1 const& a, T2 const& b)
{
	return a < b ? b : a;
}

只显式指定第一个实参，而让演绎过程推导出其余的实参。

template <typename RT, typename T1, typename T2>
inline RT max2(T1 const& a, T2 const& b)
{
	return a < b ? b : a;
}

int main()
{
	cout << "调用max():" << endl;
	//cout << ::max(1, 2.5) << endl;
	cout << ::max(2.5, 1) << endl;

	必须显式地指定模板实参列表。
	cout << ::max<int,double,double>(1, 2.5) << endl;

	调用max2<double>时显式地把RT指定为double，但其它两个参数T1和T2可以根据调用实参分别演绎为int和double。
	cout << ::max2<double>(1, 2.5) << endl;

	return 0;
}

----------------------------------------------------------------------------------------------------

函数模板，重载:
和普通函数一样，函数模板也可以被重载。
inline int const& max(int const& a, int const& b)
{
	return a < b ? b : a;
}

template <typename T>
inline T const& max(T const& a, T const& b)
{
	return a < b ? b : a;
}

template <typename T>
inline T const& max(T const& a, T const& b, T const& c)
{
	return ::max(::max(a, b), c);
}

int main()
{
	调用具有3个参数的模板。
	cout << "::max(7,42,68):" << ::max(7,42,68) << endl;

	调用max<double>(通过实参演绎)。
	cout << "::max(7.5,42.5):" << ::max(7.5,42.5) << endl;

	调用max<char>(通过实参演绎)。
	cout << "::max('a','b'):" << ::max('a','b') << endl;

	调用int重载的非模板函数。
	cout << "::max(7,42):" << ::max(7,42) << endl;

	调用max<int>(通过实参演绎)。
	cout << "::max<>(7,42):" << ::max<>(7,42) << endl;

	调用max<double>(没有实参演绎)。
	cout << "::max<double>(7,42):" << ::max<double>(7,42) << endl;

	调用int重载的非模板函数。
	cout << "::max('a',42.5):" << ::max('a',42.5) << endl;

	如例子所示：
	一个非模板函数可以和一个同名的函数模板同时存在。而且该函数模板还可以被实例化为这个非模板函数。
	对于非模板函数和同名的函数模板，如果其他条件都是相同的话，那么在调用的时候，重载解析过程通常会调用非模板函数，而不会从该模板产生出一个实例。

	可以显式地指定一个空的模板实参列表，这个语法告诉编译器，只有模板才能来匹配这个调用，而且所有的模板参数都应该根据调用实参演绎出来。

	因为模板是不允许自动类型转换的，但普通函数可以进行自动类型转换，所以最后一个调用将使用非模板函数('a'和42.5都被转换为int)。

	return 0;
}

----------------------------------------------------------------------------------------------------

一般而言，在重载函数模板的时候，应该限制在下面两种情况：
1：改变参数的数目。
2：显式地指定模板参数。

函数的所有重载版本的声明都应该位于该函数被调用的位置之前。
template <typename T>
inline T const& max(T const& a, T const& b)
{
	return a < b ? b : a;
}

求两个指针所指向的最大者。
template <typename T>
inline T* const& max(T* const& a, T* const& b)
{
	return *a < *b ? b : a;
}

#include <cstring>
求两个C字符串的最大者。
inline char const* const& max(char const* const& a, char const* const& b)
{
	return strcmp(a, b) < 0 ? b : a;
}

int main()
{
	int a = 7;
	int b = 42;
	cout << ::max(a, b) << endl;

	string s = "hey";
	string t = "you";
	cout << ::max(s, t) << endl;

	int* p1 = &b;
	int* p2 = &a;
	cout << ::max(p1, p2);

	char const* s1 = "David";
	char const* s2 = "Nico";
	cout << ::max(s1, s2);

	return 0;
}

----------------------------------------------------------------------------------------------------

类模板，类模板特化:
这个类的类型是Stack<T>。因此，当在声明中需要使用该类的类型时，必须使用Stack<T>。例如：
拷贝构造函数：
Stack(Stack<T> const&);
赋值运算符：
Stack<T>& operator=(Stack<T> const&);

然而，当使用类名而不是类的类型时，就应该只用Stack。譬如，在构造函数和析构函数中。
template <typename T>
class Stack
{
public:
	void push(T const&);
	void pop();
	T top() const;
	bool empty() const
	{
		return mContainer.empty();
	}

private:
	vector<T> mContainer;
};

为了定义类模板的成员函数，必须指定该成员函数是一个函数模板，而且还需要使用这个类模板的完整类型限
定符：Stack<T>。
template <typename T>
void Stack<T>::push(T const& elem)
{
	mContainer.push_back(elem);
}

template <typename T>
void Stack<T>::pop()
{
	if (empty())
	{
		throw out_of_range("Stack<>::pop():empty stack.");
	}

	mContainer.pop_back();
}

template <typename T>
T Stack<T>::top() const
{
	if (empty())
	{
		throw out_of_range("Stack<>::top():empty stack.");
	}

	return mContainer.back();
}

用模板实参来特化类模板。
和函数模板的重载类似，通过特化类模板，可以优化基于某种特定类型的实现，或者克服某种特定类型在实例化类模板时所出现的不足(该类型没有提供某种操作等)。

如果要特化一个类模板，就要特化该类模板的所有成员函数。每个成员函数都必须重新定义为普通函数，原来模板函数中的每个T相应地被进行特化的类型取代。

为了特化一个类模板，必须在起始处声明一个template<>，接下来声明用来特化类模板的类型。这个类型被用作模板实参，且必须在类名的后面直接指定。

template<>
class Stack<string>
{
public:
	void push(string const&);
	void pop();
	string top() const;
	bool empty() const;

private:
	特化的实现可以和基本类模板的实现完全不同。
	deque<string> mContainer;
};

void Stack<string>::push(string const& elem)
{
	mContainer.push_back(elem);
}

void Stack<string>::pop()
{
	if (empty())
	{
		throw out_of_range("Stack<string>::pop():empty stack.");
	}

	mContainer.pop_back();
}

string Stack<string>::top() const
{
	if (empty())
	{
		throw out_of_range("Stack<string>::top():empty stack.");
	}

	return mContainer.back();
}

bool Stack<string>::empty() const
{
	return mContainer.empty();
}

int main()
{
	Stack<int> intStack;

	没有特化版本的时候，这个对象调用基本类模板的成员函数。有了特化的版本后，这个对象就调用特化版本的成员函数。
	Stack<string> stringStack;

	只有那些被调用的成员函数，才会产生这些函数的实例化代码。
	对于类模板，成员函数只有在被使用的时候才会被实例化。
	对于那些"未能提供所有成员函数中所有操作的"类型，也可以使用该类型来实例化类模板，只要对那些"未能提供某些操作的"成员函数，模板内部不使用就可以。
	如果类模板中含有静态成员，那么用来实例化的每种类型，都会实例化这些静态成员。
	intStack.push(7);
	cout << intStack.top() << endl;

	stringStack.push("hello");
	cout << stringStack.top() << endl;
	stringStack.pop();

	return 0;
}

----------------------------------------------------------------------------------------------------

类模板局部特化:
类模板可以被局部特化。
我们可以在特定的环境下指定类模板的特定实现，并且要求某些模板参数仍然必须由用户来定义。

template <typename T1, typename T2>
class MyClass
{

};

局部特化，两个模板参数具有相同的类型。
template<typename T>
class MyClass<T, T>
{

};

局部特化，第二个模板参数的类型是int。
template<typename T>
class MyClass<T, int>
{

};

局部特化，两个模板参数都是指针类型。
template<typename T1, typename T2>
class MyClass<T1*, T2*>
{

};

int main()
{
	使用MyClass<T1,T2>
	MyClass<int, float> mif;

	使用MyClass<T,T>
	MyClass<float, float> mff;

	使用MyClass<T,int>
	MyClass<float, int> mfi;

	使用MyClass<T1*,T2*>
	MyClass<int*, float*> mp;

	如果有多个局部特化同等程度地匹配某个声明，那么就称该声明具有二义性：
	MyClass<int,int> m;		同等程度地匹配MyClass<T,T>和MyClass<T,int>
	MyClass<int*,int*> m;	同等程度地匹配MyClass<T,T>和MyClass<T1*,T2*>
	为了解决第二种二义性，可以另外提供一个指向相同类型指针的特化：
	template<typename T>
	class MyClass<T*,T*>
	{
	};

	return 0;
}

----------------------------------------------------------------------------------------------------

缺省模板实参:
对于类模板，可以为模板参数定义缺省值。这些值就被称为缺省模板实参。而且它们还可以引用之前的模板参数。

template <typename T, typename CONT = vector<T>>		//缺省实参不能依赖于自身的参数，但可以依赖于前面的参数
class Stack
{
public:
	void push(T const&);
	void pop();
	T top() const;
	bool empty() const
	{
		return mContainer.empty();
	}

private:
	CONT mContainer;
};

template <typename T, typename CONT>
void Stack<T, CONT>::push(T const& elem)
{
	mContainer.push_back(elem);
}

template <typename T, typename CONT>
void Stack<T, CONT>::pop()
{
	if (empty())
	{
		throw out_of_range("Stack<>::pop():empty stack.");
	}

	mContainer.pop_back();
}

template <typename T, typename CONT>
T Stack<T, CONT>::top() const
{
	if (empty())
	{
		throw out_of_range("Stack<>::top():empty stack.");
	}

	return mContainer.back();
}

int main()
{
	Stack<int> intStack;
	Stack<double, deque<double>> doubleStack;

	intStack.push(7);
	cout << intStack.top() << endl;
	intStack.pop();

	doubleStack.push(42.5);
	cout << doubleStack.top() << endl;
	doubleStack.pop();

	return 0;
}

----------------------------------------------------------------------------------------------------

非类型的类模板参数，非类型的函数模板参数，非类型模板参数的限制:
非类型的类模板参数：
对于函数模板和类模板，模板参数并不局限于类型，普通值也可以作为模板参数。
1：整型或者枚举类型。
2：指针类型（普通对象的指针类型，函数指针类型，指向成员的指针类型）。
3：引用类型（指向对象或者指向函数的引用）。

template<typename T, int MAXSIZE>
class Stack
{
public:
	Stack();

	void push(T const&);
	void pop();
	T top() const;
	bool empty() const;
	bool full() const;

private:
	T mContainer[MAXSIZE];
	int mNum;
};

template<typename T, int MAXSIZE>
Stack<T, MAXSIZE>::Stack() :
	mNum(0)
{
}

template<typename T, int MAXSIZE>
void Stack<T, MAXSIZE>::push(T const& elem)
{
	if (full())
	{
		throw std::out_of_range("Stack<T, MAXSIZE>::push():stack is full.");
	}

	mContainer[mNum] = elem;
	++mNum;
}

template<typename T, int MAXSIZE>
void Stack<T, MAXSIZE>::pop()
{
	if (empty())
	{
		throw std::out_of_range("Stack<T, MAXSIZE>::pop():stack is empty.");
	}

	--mNum;
}

template<typename T, int MAXSIZE>
T Stack<T, MAXSIZE>::top() const
{
	if (empty())
	{
		throw std::out_of_range("Stack<T, MAXSIZE>::top():stack is empty.");
	}

	return mContainer[mNum - 1];
}

template<typename T, int MAXSIZE>
bool Stack<T, MAXSIZE>::empty() const
{
	return mNum == 0;
}

template<typename T, int MAXSIZE>
bool Stack<T, MAXSIZE>::full() const
{
	return mNum == MAXSIZE;
}

typename用法：
template<typename T,							//类型参数
	typename T::Allocator* Allocator>		//非类型参数
class List;

函数和数组类型：
template<int buf[5]>			//buf实际上是一个int*类型
class Box;
template<int* buf>				//正确：这是上面的重新声明
class Sphere;

非类型模板参数，不能具有static、mutable等修饰符。只能具有const和volatile限定符，但如果这两个限定符限定的是最外层的参数类型，编译器将会忽略它们。

非类型模板参数只能是右值：它们不能被取地址，也不能被赋值。
template<int const MAX>		//这里的const是没用的，被忽略了。
class Dog;
template<int MAX>				//和上面是等同的。
class Cat;

非类型的函数模板参数。
template<typename T, int VAL>
T addValue(T const& x)
{
	return x + VAL;
}

非类型模板参数的限制：
通常而言，可以是常整数（包括枚举值）或者指向外部链接对象的指针。
浮点数和类对象是不允许作为非类型模板参数的。

int main()
{
	每个模板实例都具有自己的类型，因此int20Stack和int30Stack属于不同的类型，而且这两种类型之间也不存在显式或隐式的类型转换。所以它们不能互相替换，
	不能互相赋值。
	Stack<int, 20> int20Stack;
	Stack<int, 30> int30Stack;

	return 0;
}

----------------------------------------------------------------------------------------------------

关键字typename，使用this->:
template<typename T>
class MyClass
{
	typename被用来说明：SubType是定义于类T内部的一种类型。
	因此，ptr是一个指向T::SubType类型的指针。

	如果不使用typename，SubType就会被认为是一个静态成员，那么它应该是一个具体的变量或对象，于是表达式T::SubType * ptr会被看作是类T的静态成员
	SubType和ptr的乘积。
	typename T::SubType * ptr;
};

一个typename的典型应用，即在模板代码中访问STL容器的迭代器：
#include <iostream>
using namespace std;

template<typename T>
void print(T const& cont)
{
	在每个STL容器类中，都声明有迭代器类型const_iterator。
	typename T::const_iterator cit;

	for (cit = cont.begin(); cit != cont.end(); ++cit)
	{
		cout << *cit << ' ';
	}
	cout << endl;
}

使用this->
template<typename T>
class Base
{
public:
	void exit()
	{
		cout << "调用Base<T>::exit()." << endl;
	}
};

template<typename T>
class Derived : Base<T>
{
public:
	void foo()
	{
		并不会考虑基类Base中定义的exit()。

		对于那些在基类中声明，并且依赖于模板参数的符号（函数或者变量等），应该在它们前面使用this->或者Base<T>::。
		//exit();
		//this->exit();
		Base<T>::exit();
		//测试发现，上面3个调用都会输出同样的信息。
	}
};

int main()
{
	Derived<int> derivedIntObj;
	derivedIntObj.foo();

	return 0;
}

----------------------------------------------------------------------------------------------------

成员模板:
类成员也可以是模板。嵌套类和成员函数都可以作为模板。

一般，栈之间只有在类型完全相同时才能互相赋值，其中类型指的是元素的类型。也就是说，对于元素类型不同的栈，不能对它们进行相互赋值，即使这两种（元素
的）类型之间存在隐式类型转换。比如：
Stack<int> intStack1,intStack2;
Stack<float> floatStack;
intStack1 = intStack2;			//OK,具有相同类型的栈
floatStack = intStack1;			//ERROR,两边栈的类型不同
缺省赋值运算符要求两边具有相同的类型，当元素类型不同时，两个栈的类型显然不同，不能符合缺省赋值运算符的要求。然而，通过定义一个身为模板的赋值运算符，
针对元素类型可以转换的两个栈就可以进行相互赋值。如下所示：
template<typename T>
class Stack
{
public:
	void push(T const&);
	void pop();
	T top() const;
	bool empty() const;

	使用元素类型为T2的栈进行赋值。
	在定义有模板参数T的模板内部，还定义了一个含有模板参数T2的内部模板。
	模板赋值运算符并没有取代缺省赋值运算符。对于相同类型栈之间的赋值，仍然调用缺省赋值运算符。
	再次强调：对于类模板而言，只有那些被调用的成员函数才会被实例化。
	template<typename T2>
	Stack<T>& operator=(Stack<T2> const&);

private:
	deque<T> mContainer;
};

template<typename T>
void Stack<T>::push(T const& elem)
{
	mContainer.push_back(elem);
}

template<typename T>
void Stack<T>::pop()
{
	if (empty())
	{
		throw out_of_range("Stack<T>::pop():empty stack.");
	}

	mContainer.pop_back();
}

template<typename T>
T Stack<T>::top() const
{
	if (empty())
	{
		throw out_of_range("Stack<T>::top():empty stack.");
	}

	return mContainer.back();
}

template<typename T>
bool Stack<T>::empty() const
{
	return mContainer.empty();
}

#pragma warning(disable:4244)

template<typename T>
template<typename T2>
Stack<T>& Stack<T>::operator=(Stack<T2> const& op2)
{
	if (static_cast<const void*>(this) == static_cast<const void*>(&op2))
	{
		return *this;
	}

	Stack<T2> tmp(op2);//为什么这里要用一个临时对象？因为没有迭代器。
	mContainer.clear();
	while (!tmp.empty())
	{
		mContainer.push_front(tmp.top());
		tmp.pop();
	}

	return *this;
}

int main()
{
	Stack<int> intStack;
	Stack<float> floatStack;

	intStack.push(4);
	intStack.push(5);

	floatStack = intStack;

	cout << "floatStack:" << endl;
	while (!floatStack.empty())
	{
		cout << floatStack.top() << ' ';
		floatStack.pop();
	}
	cout << endl;

	return 0;
}

----------------------------------------------------------------------------------------------------

模板的模板参数:
模板的模板参数(现上轿现扎耳朵眼):
第二个模板参数现在被声明为一个类模板：
template<typename ELEM,typename ALLOC = allocator<ELEM>>
class CONT = deque>
这里的CONT是定义了一个类。

模板的模板参数是代表类模板的占位符。
template<typename T,
	template<typename ELEM,typename ALLOC = allocator<ELEM>>
	class CONT = deque>
class Stack
{
public:
	void push(T const&);
	void pop();
	T top() const;
	bool empty() const;

	使用元素类型为T2的栈对原栈赋值
	template<typename T2,
		template<typename ELEM2,typename = allocator<ELEM2>>
	class CONT2>
		Stack<T, CONT>& operator=(Stack<T2, CONT2> const&);

private:
	CONT<T> mContainer;

	//对于模板的模板参数而言，它的参数名称只能被自身其它参数的声明使用。
	//ELEM mError;
};

template<typename T,
	template<typename ELEM,typename ALLOC = allocator<ELEM>>
	class CONT>
void Stack<T, CONT>::push(T const& elem)
{
	mContainer.push_back(elem);
}

template<typename T,
	template<typename ELEM,typename ALLOC = allocator<ELEM>>
	class CONT>
void Stack<T, CONT>::pop()
{
	if (empty())
	{
		throw out_of_range("Stack<T, CONT>::pop():empty stack.");
	}

	mContainer.pop_back();
}

template<typename T,
	template<typename ELEM,typename ALLOC = allocator<ELEM>>
	class CONT>
T Stack<T, CONT>::top() const
{
	if (empty())
	{
		throw out_of_range("Stack<T, CONT>::top():empty stack.");
	}

	return mContainer.back();
}

template<typename T,
	template<typename ELEM,typename ALLOC = allocator<ELEM>>
	class CONT>
bool Stack<T, CONT>::empty() const
{
	return mContainer.empty();
}

template<typename T,
	template<typename,typename>
	class CONT>
template<typename T2,
	template<typename,typename>
	class CONT2>
Stack<T, CONT>& Stack<T, CONT>::operator=(Stack<T2, CONT2> const& op2)
{
	if ((void*)this == (void*)&op2)
	{
		return *this;
	}

	Stack<T2, CONT2> tmp(op2);
	mContainer.clear();
	while (!tmp.empty())
	{
		mContainer.push_front(tmp.top());
		tmp.pop();
	}
	return *this;
}

int main()
{
	Stack<int> intDequeStack;
	intDequeStack.push(1);
	cout << "intDequeStack:" << intDequeStack.top() << endl;

	Stack<double,vector> doubleVectorStack;
	doubleVectorStack.push(2.5);
	cout << "doubleVectorStack:" << doubleVectorStack.top() << endl;

	//-------------------------------------------

	Stack<double> doubleDequeStack;
	doubleDequeStack.push(3.5);
	doubleDequeStack = intDequeStack;
	cout << "doubleDequeStack:" << doubleDequeStack.top() << endl;

	return 0;
}

----------------------------------------------------------------------------------------------------

零初始化:
零初始化：
对于基本类型，任何未被初始化的局部变量都具有一个不确定值。例如：
void foo()
{
	int x;					//x具有一个不确定值
	int* ptr;				//ptr指向某块内存
}

对于函数模板：
template<typename T>
void foo()
{
	T x = T();			//如果T是内建类型，x是0或者false。
}

对于类模板：
template<typename T>
class MyClass
{
public:
	MyClass() :
	x()					//确认x已被初始化，内建类型对象也是如此。
	{
	}

private:
	T x;
};

----------------------------------------------------------------------------------------------------

包含模型，显式实例化:
.h
template<typename T>
class MyClass
{
public:
	MyClass();
	~MyClass();

	void func();

private:
	static T mStaticValue;
	T mNormalValue;
};

template<typename T>
void freeFunc(T const&);

typedef MyClass<int> IntMyClass;
typedef MyClass<double> DoubleMyClass;
typedef MyClass<char> CharMyClass;

.cpp
template<typename T>
MyClass<T>::MyClass() :
	mNormalValue(T())
{
	cout << "call MyClass()." << endl;
}

template<typename T>
MyClass<T>::~MyClass()
{
	cout << "call ~MyClass()." << endl;
}

template<typename T>
void MyClass<T>::func()
{
	cout << "call func()." << endl;
}

template<typename T>
T MyClass<T>::mStaticValue;

template<typename T>
void freeFunc(T const& x)
{
	cout << typeid(x).name() << endl;
	cout << "call freeFunc()." << endl;
}

template class MyClass<int>;
template class MyClass<double>;
template class MyClass<char>;

template void freeFunc<char>(char const&);
template void freeFunc<double>(double const&);

main.cpp
显式实例化：
显式实例化(人工实例化)有一个显著的缺点：我们必须仔细跟踪每个需要实例化的实体。对于大项目而言，这种跟踪很快就会带来巨大负担。
显式实例化的优点：实例化可以在需要的时候才进行。显然，我们因此避免包含庞大头文件的开销，更可以把模板定义的源文件封装起来(这些是包含模型的缺点)。但
封装之后，用户就不能基于其它类型来进行额外的实例化了(整合包含模型和显式实例化可以解决这个缺憾)。

整合包含模型和显式实例化：
为了能够根据实际情况，自由地选择包含模型或者显式实例化，通常的做法是使用头文件来表示这两个文件。如果我们希望使用包含模型，那么只需要#include头文件
xxxDef.h就可以了。如果我们希望显式实例化模板，那么只需要#include头文件xxx.h，然后显式实例化就可以了。

//显式实例化
#include "MyClass.h"

int main()
{
	//显式实例化
	IntMyClass intObj;
	DoubleMyClass doubleObj;
	CharMyClass charObj;

	intObj.func();

	freeFunc('a');
	freeFunc(3.2);

	return 0;
}

----------------------------------------------------------------------------------------------------

显式特化，局部特化:
#include<typeinfo>
#include<iostream>
using namespace std;

template<typename T1,typename T2>
class MyClass
{
public:
	void foo(T1 const&, T2 const&);

private:
	T1 mValue1;
	T2 mValue2;
};

template<typename T1,typename T2>
void MyClass<T1, T2>::foo(T1 const& a, T2 const& b)
{
	cout << "MyClass<T1, T2>::foo()." << endl;
	cout << typeid(a).name() << "," << typeid(b).name() << endl;
}

显式特化
template<>
class MyClass<int, double>
{
public:
	void foo(int const&, double const&);

private:
	int mValue1;
	double mValue2;
};

void MyClass<int, double>::foo(int const& a, double const& b)
{
	cout << "MyClass<int, double>::foo()." << endl;
	cout << typeid(a).name() << "," << typeid(b).name() << endl;
}

局部特化
template<typename T>
class MyClass<char, T>
{
public:
	void foo(char const&, T const&);

private:
	char mValue1;
	T mValue2;
};

template<typename T>
void MyClass<char, T>::foo(char const& a, T const& b)
{
	cout << "MyClass<char, T>::foo()." << endl;
	cout << typeid(a).name() << "," << typeid(b).name() << endl;
}

int main()
{
	MyClass<string, char> obj1;
	obj1.foo(string("abc"), 'd');

	MyClass<int,double> obj2;
	obj2.foo(3, 4.5);

	MyClass<char, float> obj3;
	obj3.foo('e', 1.2f);

	return 0;
}

----------------------------------------------------------------------------------------------------

template-id:
template<typename T,int N>
class ArrayInClass
{
public:
	T mArray[N];
};

这个T是模板参数。
template<typename T>
class Dozen
{
public:
	这个T是模板实参。
	ArrayInClass<T, 12> mContents;
};

int main()
{
	模板实参：
	紧接在模板名称ArrayInClass后面的是用一对尖括号包围起来的模板实参列表。

	template-id：
	template-id指的是模板名称与“紧随其后的尖括号内部的所有实参”的组合。
	ArrayInClass<double, 10> obj;

	return 0;
}

----------------------------------------------------------------------------------------------------

虚成员函数，模板的链接，基本模板:
成员函数模板不能被声明为虚函数。这是一种强制限制，因为虚函数调用机制是使用了一个大小固定的表，每个虚函数都对应表的一个入口。然而，成员函数模板
的实例化个数，要等到整个程序都翻译完毕才能够确定，这就和表的大小（是固定的）发生了冲突。
相反，类模板中的普通成员函数可以是虚函数，因为当类被实例化之后，它们的个数是固定的。
template<typename T>
class MyClass
{
public:
	virtual ~MyClass()						//OK,每个MyClass只对应一个析构函数
	{
	}

	//template<typename T2>
	//virtual void func(T2 const&) {}		//ERROR,在确定MyClass<T>实例的时候，并不知道func()的个数
};

模板的链接：
模板名字是具有链接的，但它们不能具有C链接。
extern "C++"
template<typename T>
void func1() {}						//这是缺省情况，上面的链接规范可以不写

//extern "C"
//template<typename T>
//void func2() {}						//ERROR

模板通常具有外部链接。
唯一的例外就是前面有static修饰符的名字空间作用域下的函数模板。
template<typename T>
void func3();					//作为一个声明，引用位于其它文件的、具有相同名称的实体。即引用位于其它文件的func3()函数模板，也称前置声明。

template<typename T>
static void func4();			//与其它文件中具有相同名称的模板没有关系。即不是外部链接。

基本模板：
没有在模板名称后面添加一对尖括号(和里面的实参)的声明。
template<typename T>
class Box;						//正确，基本模板

//template<typename T>
//class Sphere<T>;			//错误

template<typename T>
void func5(T*);				//正确，基本模板

//template<typename T>
//void func6<T>(T*);		//错误

----------------------------------------------------------------------------------------------------

模板实参，SFINAE:
模板实参：
显式模板实参：紧跟在模板名称后面，在一对尖括号内部的显式模板实参值。所组成的整个实体称为template-id。
注入式类名称：对于具有模板参数P1,P2...的类模板X，在它的作用域中，模板名称(即X)等同于template-id：X<P1,P2...>。
缺省模板实参：如果提供缺省模板实参的话，在类模板的实例中就可以省略显式模板实参。然而，即使所有的模板参数都具有缺省值，一对尖括号还是不能省略的。
实参演绎：对于不是显式指定的函数模板实参，可以在函数的调用语句中，根据函数调用实参的类型来演绎出函数模板实参。如果所有的模板实参都可以通过演绎
获得，那么在函数模板名称后面就不需要指定尖括号。

函数模板实参：
template<typename T>
T const& max(T const& a, T const& b)
{
	return a < b ? b : a;
}

template<typename T1, typename T2>
T1 func1(T2 const& x)
{
	return x;
}

由于函数模板可以被重载，所以对于函数模板而言，显式提供所有的实参并不足以标识每一个函数：在一些例子中，它标识的是由许多函数组成的函数集合。
template<typename FUNC, typename T>
void apply(FUNC funcPtr, T x)
{
	funcPtr(x);
}

template<typename T>
void single(T a)
{
}

template<typename T>
void multi(T a)
{
}

template<typename T>
void multi(T* a)
{
}

SFINAE:
替换失败并非错误(substitution-failure-is-not-an-error)。
template<typename T>
void test(typename T::X const*)
{
}

template<typename T>
void test(...)
{
}

#pragma warning(disable : 4244)

int main()
{
	::max<double>(1.0, -3.0);					//显式指定函数模板实参
	::max(1.0, -3.0);								//函数模板实参被演绎成double
	::max<int>(1.0, -3.0);						//显式的<int>禁止了演绎，因此返回结果是int类型

	double value = func1<double>(-1);		//T1显式指定，T2演绎获得

	apply()的第一次调用是正确的，因为表达式&single<int>的类型是确定的。因此，可以很容易地演绎出funcPtr参数的模板实参值。
	在第二次的调用中，&multi<double>可以是两种函数类型中的任意一种，因此，会产生二义性，不能演绎出funcPtr实参值。
	apply(&single<int>, 3);						//正确
	//apply(&multi<double>, 3.0);				//错误，&multi<double>不唯一

	表达式test<int>会使第一个函数模板毫无意义，因为int类型根本就没有成员类型X。第二个函数模板不存在这个问题。因此，表达式&test<int>能够标识一个
	唯一的函数地址。
	不能用int来替换第一个函数模板的参数，并不意味着&test<int>是非法的(SFINAE原则)。
	test<int>(5);

	return 0;
}

----------------------------------------------------------------------------------------------------

类型实参:
模板的类型实参是一些用来指定模板类型参数的值。
大多数类型都可以被用作模板的类型实参，但有两种情况例外：
1：局部类和局部枚举(在函数定义内部声明的类型)不能作为模板的类型实参。
2：未命名的class类型或者未命名的枚举类型不能作为模板的类型实参。然而，通过typedef声明给出的未命名类和枚举是可以作为模板类型实参的。
template<typename T>
class List
{
private:
	T mValue;
};

int main()
{
	struct Dog
	{
		Dog() :
			a(5)
		{
		}

		int a;
	};

	List<Dog> list1;			//事实证明局部类型可以。

	return 0;
}

----------------------------------------------------------------------------------------------------

非类型实参:
1：某一个具有正确类型的非类型模板参数。
2：一个编译期整型常值（或枚举值）。这只有在参数类型和值的类型能够进行匹配，或者值的类型可以隐式地转换为参数类型（例如，一个char值可以作为int参数
的实参）的前提下，才是合法的。
3：前面有单目运算符&（取地址）的外部变量或者函数的名称。对于函数或数组变量，&运算符可以省略。这类模板实参可以匹配指针类型的非类型参数。
4：对于引用类型的非类型模板参数，前面没有&运算符的外部变量和外部函数也是可取的。
5：一个指向成员的指针常量。类似&C::m的表达式，其中C是一个class类型，m是一个非静态成员（成员变量或者函数）。这类实参只能匹配类型为“成员指针”的
非类型参数。

有些常值不能作为有效的非类型实参：空指针常量、浮点型值和字符串。

当实参匹配“指针类型或者引用类型的参数”时，用户定义的类型转换（例如，单参数的构造函数和重载类型转换运算符）和由派生类到基类的类型转换，都是不会被
考虑的。即使在其它的情况下，这些隐式类型转换是有效的，但在这里都是无效的。隐式类型转换的唯一应用只能是：给实参加上关键字const或者volatile。

模板实参的一个普遍约束是：在程序创建的时候，编译器或者链接器要能够确定实参的值。如果实参的值要等到程序运行时才能够确定（局部变量的地址），就不符合
“模板是在程序创建的时候进行实例化”的概念了。

template<typename T, T nontype_param>
class C
{
};

#pragma warning(disable:4101)

int a;

void f();
void f(int);

class X
{
public:
	int n;
	static bool b;
};

template<typename T>
void templ_func();

int main()
{
	C<int, 33>* c1;							//整型

	C<int*, &a>* c2;						//外部变量的地址

	C<void(*)(int), f>* c3;				//函数名称：重载解析会选择f(int)，f前面的&隐式省略了

	C<bool&, X::b>* c4;					//静态类成员是可取的变量（和函数）名称

	C<int X::*, &X::n>* c5;				//指向成员的指针常量

	C<void(), &templ_func<double>>* c6;			//函数模板实例同时也是函数

	return 0;
}

----------------------------------------------------------------------------------------------------

模板的模板实参，实参的等价性:
模板的模板实参：
“模板的模板实参”必须是一个类模板，它本身具有参数，该参数必须精确匹配它“所替换的模板的模板参数”本身的参数。在匹配过程中，“模板的模板实参”的缺省模板
实参将不会被考虑。但是，如果“模板的模板参数”具有缺省实参，那么模板的实例化过程是会考虑模板的模板参数的缺省实参的。

List的声明：
//namespace std
//{
//template<typename T,
//				typename Allocator = allocator<T>>
//class list;
//}

template<typename T1,typename T2,
				//template<typename> class Container>			//标记1：Container期望的是只具有一个参数的模板
				template<typename T,typename = allocator<T>> class Container>			//标记2：现在Container就能够接受一个标准容器模板了
class Relation
{
private:
	Container<T1> mC1;
	Container<T2> mC2;
};

实参的等价性:
template<typename T,int I>
class Mix
{
};

//typedef int Int;
//Mix<int,3 * 3>* p1;
//Mix<Int,4 + 5>* p2;		//p2和p1的类型是相同的

int main()
{
	标记1:
	错误，list是一个具有2个参数的模板。
	这里的问题是：std::list模板具有2个参数，它的第二个参数（内存配置器allocator）具有一个缺省值。但是当我们匹配std::list和Container参数时，并不会
	考虑这个缺省值，即认为缺省值并不存在。
	//Relation<int, double, list> rel;

	标记2:
	Relation<int, double, list> rel;

	return 0;
}

----------------------------------------------------------------------------------------------------

友元:
template<typename T>
class Node
{
};

template<typename T>
class Tree
{
	如果要把类模板的实例声明为其它类（或者类模板）的友元，该类模板在声明的地方必须是可见的。
	然而，对于一个普通类，就没有这个要求。
	friend class Node<T>;
	friend class Dog;
};

template<typename T1,typename T2>
void combine(T1, T2);

class Mixer
{
	friend void combine<>(int&, int&);			//正确，T1 = int&,T2 = int&

	friend void combine<int, int>(int, int);		//正确，T1 = int,T2 = int

	friend void combine<char>(char, int);		//正确，T1 = char,T2 = int

	//friend void combine<char>(char&, int);	//错误

	//friend void combine<>(long, long) {}		//错误，这里的友元声明不允许出现定义
};

如果名称后面没有紧跟一对尖括号，那么只有在下面两种情况下是合法的：
1：如果名称不是受限的（没有包含一个形如双冒号的域运算符），那么该名称一定不是（也不能）引用一个模板实例。如果在友元声明的地方，还看不到所匹配的
非模板函数（即普通函数），那么这个友元声明就是函数的首次声明。于是，该声明可以是定义。
2：如果名称是受限的（前面有双冒号），那么该名称必须引用一个在此之前声明的函数或者函数模板。在匹配的过程中，匹配的函数要优先于匹配的函数模板。然而，
这样的友元声明不能是定义。
总结：
没有双冒号：
肯定是普通函数。如果能看到，那么是声明，否则要为定义。
有双冒号：
可以是普通函数也可以是函数模板，必须能够看到，只能是声明。
void func(void*);			//普通函数

template<typename T>
void func(T);					//函数模板

class Dog
{
	定义了一个新函数。
	friend void func(int) {}

	引用上面的普通函数。
	friend void ::func(void*);

	引用上面函数模板的一个实例。
	friend void ::func(int);

	受限名称可以具有一对尖括号。
	friend void ::func<double*>(double*);

	错误，受限的友元不能是一个定义。
	//friend void ::error() {}

	上面的例子，都是在一个普通类里面声明友元函数。如果要在类模板里面声明友元函数，前面的这些规则仍然适用，唯一的区别就是：可以使用模板参数来标识
	友元函数。
};

如果我们在类模板中定义一个友元函数，那么将会出现一个有趣的现象。因为对于任何只在模板内部声明的实体，都要等到模板被实例化之后，才会是一个具体的实体，
在这之前，该实体是不存在的。
template<typename T>
class Cat
{
	//friend void func() {}			//定义了一个函数，但是要等到Cat被实例化之后才存在

	friend void miao(Cat<T>*) {}		//每个T都生成一个不同的::miao()函数

	由于函数的实体处于类定义的内部，所以这些函数是内联函数。因此，在两个不同的翻译单元中可以生成相同的函数。
};

友元模板：
我们通常声明的友元只是：函数模板的实例或者类模板的实例，我们指定的友元也只是特定的实体。然而，我们有时候需要让模板的所有实例都成为友元，这就需要
声明友元模板。
class Manager
{
	template<typename T>
	friend class MyClass;

	//template<typename T>
	//friend void Class1<T>::func1(Class2<T>*);

	template<typename T>
	friend int ticket()
	{
		return ++Manager::counter;
	}

	static int counter;

	和普通友元的声明一样，只有在友元模板声明的是一个非受限的函数名称，并且后面没有紧跟尖括号的情况下，该友元模板声明才能成为定义。
	友元模板声明的只是基本模板和基本模板的成员。当进行这些声明之后，与该基本模板相对应的模板局部特化和显式特化都会被自动地看成友元。
};

#pragma warning(disable:4101)

int main()
{
	两个不同的实例化过程生成了两个完全相同的定义（即func()函数），这违反了ODR原则。
	Cat<void> c1;			//这时才生成func()
	//Cat<int> c2;			//错误，func()第二次被生成

	因此，我们必须确定：在类模板内部定义的友元函数必须包含类模板的模板参数。
	Cat<int> c2;

	return 0;
}

----------------------------------------------------------------------------------------------------

派生和类模板:
非依赖型基类：
template<typename X>
class Base
{
public:
	int basefield;
	typedef int T;
};

template<typename T>
class D : public Base<double>			//非依赖型基类
{
public:
	D() :
		value(T())
	{
	}

	T value;										//T是Base<double>::T，而不是模板参数
};

依赖型基类：
template<typename T>
class C : public Base<T>					//依赖型基类
{

};

int main()
{
	D<string> d;

	cout << typeid(d.value).name() << endl;			//输出int

	return 0;
}
*/