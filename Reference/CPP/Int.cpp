/*
虽然C++提供了内置的整数类型定义，但不同的计算机硬件体系下整数的宽度(sizeof)是不同的：32位硬件int的宽度为32位，64位硬件int的宽度为64位。
*/

/*
C++算数类型：
bool:
布尔类型，最小尺寸-未定义
char:
字符，最小尺寸-8位
wchar_t:
宽字符，最小尺寸-16位
char16_t:
Unicode字符，最小尺寸-16位
char32_t:
Unicode字符，最小尺寸-32位
short:
短整型，最小尺寸-16位
int:
整型，最小尺寸-16位
long:
长整型，最小尺寸-32位
long long:
长整型，最小尺寸-64位
float:
单精度浮点数，最小尺寸-6位有效数字
double:
双精度浮点数，最小尺寸-10位有效数字
long double:
扩展精度浮点数，最小尺寸-10位有效数字
*/

/*
计算机以比特序列存储数据，每个比特非0即1，大多数计算机以2的整数次幂个比特作为块来处理内存，可寻址的最小内存块称为“字节(byte)”，存储的基
本单元称为“字(word)”，它通常由两个字节组成。在C++语言中，一个字节要至少能容纳机器基本字符集中的字符。
大多数计算机将内存中的每个字节与一个数字(被称为“地址(address)”)关联起来。为了赋予内存中某个地址明确的含义，必须首先知道存储在该地址的数
据的类型。类型决定了数据所占的比特数以及该如何解释这些比特的内容。
*/

/*
除去布尔型和扩展的字符型(wchar_t:宽字符,char16_t和char32_t:Unicode字符)之外，其他整形可以划分为带符号的(signed)和无符号的(unsigned)
两种。
与其他整形不同，字符型被分为了三种：char,signed char和unsigned char。特别需要注意的是：类型char和类型signed char并不一样。尽管字符型
有三种，但是字符的表现形式却只有两种：带符号的和无符号的。类型char实际上会表现为上述两种形式中的一种，具体是哪种由编译器决定。

无符号类型中所有比特都用来存储值，例如，8比特的unsigned char可以表示0至255区间内的值。
C++标准并没有规定带符号类型应如何表示，但是约定了在表示范围内正值和负值的量应该平衡。因此，8比特的signed char理论上应该可以表示-127至
127区间内的值，大多数现代计算机将实际的表示范围定为-128至127。
*/

/*
一个char的空间应确保可以存放机器基本字符集中任意字符对应的数字值。也就是说，一个char的大小和一个机器字节一样。
如果需要使用一个不大的整数，那么应该明确指定它的类型是signed char或者unsigned char。
*/

/*
信息就是位+上下文:
//hello.c
#include "stdio.h"
int main()
{
	printf("hello,world\n");

	return 0;
}
源程序实际上就是一个由值0和1组成的位（bit）序列，8个位被组织成一组，称为字节。每个字节表示程序中某个文本字符。
大部分的现代系统都使用ASCII标准来表示文本字符，这种方式实际上就是用一个唯一的单字节大小的整数值来表示每个字符。
hello.c程序以字节序列的方式存储在文件中。每个字节都有一个整数值，而该整数值对应于某个字符。例如，第一个字节的整数值
是35，它对应的就是字符'#'。第二个字节整数值为105，它对应的字符是'i'，依此类推。注意，每个文本行都是以一个不可见的
换行符'\n'来结束的，它所对应的整数值为10.像hello.c这样只由ASCII字符构成的文件称为文本文件，所有其他文件都称为二进制文件。
hello.c的表示方法说明了一个基本的思想：系统中所有的信息---包括磁盘文件、存储器中的程序、存储器中存放的用户数据以及网络
上传送的数据，都是由一串位表示的。区分不同数据对象的唯一方法是我们读到这些数据对象时的上下文。比如，在不同的上下文中，
一个同样的字节序列可能表示一个整数、浮点数、字符串或者机器指令。

程序被其他程序翻译成不同的格式：
为了在系统上运行hello.c程序，每条C语句都必须被其他程序转化为一系列的低级机器语言指令。然而这些指令按照一种称为可执行
目标程序的格式打好包，并以二进制磁盘文件的形式存放起来。目标程序也称为可执行目标文件。
这个翻译的过程可分为四个阶段完成。执行这四个阶段的程序（预处理器，编译器，汇编器和链接器）一起构成了编译系统(compilation 
system)。
预处理阶段：
预处理器（cpp）根据以字符#开头的命令，修改原始的C程序。比如hello.c中第一行的#include <stdio.h>命令告诉预处理器读取系统
头文件stdio.h的内容，并把它直接插入到程序文本中。结果就得到了另一个C程序，通常是以.i作为文件扩展名。
编译阶段：
编译器（ccl）将文本文件hello.i翻译成文本文件hello.s，它包含一个汇编语言程序。汇编语言程序中的每条语句都以一种标准的文本格式
确切地描述了一条低级机器语言指令。汇编语言是非常有用的，因为它为不同高级语言的不同编译器提供了通用的输出语言。例如，C编译器
和Fortran编译器产生的输出文件用的都是一样的汇编语言。
汇编阶段：
汇编器（as）将hello.s翻译成机器语言指令，把这些指令打包成一种叫做可重定位目标程序（relocatable object program）的格式，
并将结果保存在目标文件hello.o中。hello.o文件是一个二进制文件，它的字节编码是机器语言指令而不是字符。如果我们在文本编辑器中
打开hello.o文件，看到的将是一堆乱码。
链接阶段：
hello程序调用了printf函数，它是每个C编译器都会提供的标准C库中的一个函数。printf函数存在于一个名为printf.o的单独的预编译好了
的目标文件中，而这个文件必须以某种方式合并到我们的hello.c程序中。链接器（ld）就负责处理这种合并。结果就得到了hello文件，它是
一个可执行目标文件（或者简称为可执行文件），可以被加载到内存中，由系统执行。

系统的硬件组成：
1，总线：
贯穿整个系统的是一组电子管道，称作总线，它携带信息字节并负责在各个部件间传递。通常总线被设计成传送定长的字节块，
也就是字（word）。字中的字节数（即字长）是一个基本的系统参数，在各个系统中的情况都不尽相同。现在的大多数机器字长
有的是4个字节（32位），有的是8个字节（64位）。
2，I/O设备：
输入/输出（I/O）设备是系统与外部世界的联系通道。比如，键盘，鼠标，显示器以及磁盘。最初可执行程序hello就存放在磁盘上。
每个I/O设备都通过一个控制器或适配器与I/O总线相连。控制器和适配器之间的区别主要在于它们的封装方式。控制器是置于I/O
设备本身的或者系统主板上的芯片组，而适配器则是一块插在主板插槽上的卡。无论如何，它们的功能都是在I/O总线和I/O设备
之间传递信息。
3，主存：
主存是一个临时存储设备，在处理器执行程序时，用来存放程序和程序处理的数据。从物理上来说，主存是由一组动态随机存取存储
器（DRAM）芯片组成的。从逻辑上来说，存储器是一个线性的字节数组，每个字节都有其唯一的地址（即数组索引），这些地址
是从零开始的。一般来说，组成程序的每条机器指令都由不同数量的字节构成。
4，处理器：
中央处理单元（CPU），是解释（或执行）存储在主存中指令的引擎。处理器的核心是一个字长的存储设备（或寄存器），称为程序
计数器（PC）。在任何时刻，PC都指向主存中的某条机器语言指令（即含有该条指令的地址）。
从系统通电开始，知道系统断电，处理器一直在不断地执行程序计数器指向的指令，再更新程序计数器，使其指向下一条指令。
处理器从程序计数器指向的存储器处读取指令，解释指令中的位，执行该指令指示的简单操作，然后更新PC，使其指向下一条指令，
而这条指令并不一定与存储器中刚刚执行的指令相邻。操作是围绕着主存，寄存器文件（register file）和算术/逻辑单元（ALU）
进行的。寄存器文件是一个小的存储设备，由一些1字长的寄存器组成，每个寄存器都有唯一的名字。ALU计算新的数据和地址值。

高速缓存至关重要：
hello程序揭示了一个重要的问题，即系统花费了大量的时间把信息从一个地方挪到另一个地方。hello程序的机器指令最初是存放在磁盘上的，当程
序加载时，它们被复制到主存，当处理器运行程序时，指令又从主存复制到处理器。相似地，数据串"hello,world\n"初识时在磁盘上，然后复制到
主存，最后从主存上复制到显示设备。
CPU芯片包括系统总线，总线接口，高速缓存存储器，寄存器文件和ALU。
位于处理器芯片上的L1高速缓存的容量可以达到数万字节，访问速度几乎和访问寄存器文件一样快。一个容量为数十万到数百万字节的更大的L2高
速缓存通过一条特殊的总线连接到处理器。进程访问L2高速缓存的时间要比访问L1高速缓存的时间长5倍，但是这仍然比访问主存的时间快5-10倍。
L1和L2高速缓存是用一种叫做静态随机访问存储器（SRAM）的硬件技术实现的。比较新的处理器甚至有三级高速缓存。

存储设备形成层次结构：
每个计算机系统中的存储设备都被组织成了一个存储器层次结构：
磁盘-主存（DRAM）-L3高速缓存（SRAM）-L2高速缓存（SRAM）-L1高速缓存（SRAM）-寄存器

存储器层次结构的主要思想是一层上的存储器作为低一层存储器的高速缓存。因此，寄存器文件就是L1的高速缓存，L1是L2的高速缓存，L2是L3的
高速缓存，L3是主存的高速缓存，而主存又是磁盘的高速缓存。

操作系统管理硬件：
当shell加载和运行hello程序，以及hello程序输出自己的消息时，shell和hello程序都没有直接访问键盘、显示器、磁盘或者主存。取而代之的是，
它们依靠操作系统提供的服务。我们可以把操作系统看成是应用程序和硬件之间插入的一层软件。所有应用程序对硬件的操作都必须通过操作系统。
操作系统有两个基本功能：
1：防止硬件被失控的应用程序滥用。
2：向应用程序提供简单一致的机制来控制复杂而又通常大相径庭的低级硬件设备。
操作系统通过几个基本的抽象概念（进程、虚拟存储器和文件）来实现这两个功能。
文件是对I/O设备的抽象表示。
虚拟存储器是对主存和磁盘I/O设备的抽象表示。
进程是对处理器，主存和I/O设备的抽象表示。

进程：
像hello这样的程序在运行时，操作系统会提供一种假象，就好像系统上只有这个程序在运行，看上去只有这个程序在使用处理器、主存和I/O设备。
处理器看上去就像在不间断地一条接一条地执行程序中的指令，即该程序的代码和数据是系统存储器中唯一的对象。这些假象是通过进程的概念来
实现的，进程是计算机科学中最重要和最成功的概念之一。
进程是操作系统对一个正在运行的程序的一种抽象。在一个操作系统上可以同时运行多个进程，而每个进程都好像在独占地使用硬件。而并发运行，
则是说一个进程的指令和另一个进程的指令是交错执行的。在大多数系统中，需要运行的进程数是多于可以运行它们的CPU个数的。传统系统在一个
时刻只能执行一个程序，而先进的多核处理器同时能够执行多个程序。无论是在单核还是多核系统中，一个CPU看上去都像是在并发地执行多个进
程，这是通过处理器在进程间切换来实现的。操作系统实现这种交错执行的机制称为上下文切换。
操作系统保持跟踪进程运行所需的所有状态信息。这种状态，也就是上下文，它包括许多信息，例如PC和寄存器文件的当前值，以及主存的内容。
在任何一个时刻，单处理器系统都只能执行一个进程的代码。当操作系统决定要把控制权从当前进程转移到某个新进程时，就会进行上下文切换，
即保存当前进程的上下文、恢复新进程的上下文，然后将控制权传递到新进程。新进程就会从上次停止的地方开始。
在现代系统中，一个进程实际上可以由多个称为线程的执行单元组成，每个线程都运行在进程的上下文中，并共享同样的代码和全局数据。多线程
之间比多进程之间更容易共享数据，线程一般来说都比进程更高效。

虚拟存储器：
虚拟存储器是一个抽象概念，它为每个进程提供了一个假象，即每个进程都在独占地使用主存。每个进程看到的是一致的存储器，称为虚拟地址空
间。在Linux中，地址空间最上面的区域是为操作系统中的代码和数据保留的，这对所有进程来说都是一样的。地址空间的底部区域存放用户进程定
义的代码和数据。
每个进程看到的虚拟地址空间由大量准确定义的区构成，每个区都有专门的功能：
程序代码和数据：
对于所有的进程来说，代码是从同一固定地址开始的，紧接着的是和C全局变量相对应的数据位置。代码和数据区是直接按照可执行目标文件的内
容初始化的。
堆：
代码和数据区后紧随着的是运行时堆。代码和数据区是在进程一开始运行时就被规定了大小。与此不同，当调用如malloc和free这样的C标准库函
数时，堆可以在运行时动态地扩展和收缩。
共享库：
大约在地址空间的中间部分是一块用来存放像C标准库和数学库这样共享库的代码和数据的区域。
栈：
位于用户虚拟地址空间顶部的是用户栈，编译器用它来实现函数调用。和堆一样，用户栈在程序执行期间可以动态地扩展和收缩。特别是每次我们
调用一个函数时，栈就会增长。从一个函数返回时，栈就会收缩。
内核虚拟存储器：
内核总是驻留在内存中，是操作系统的一部分。地址空间顶部的区域是为内核保留的，不允许应用程序读写这个区域的内容或者直接调用内核代码
定义的函数。
虚拟存储器的运作需要硬件和操作系统软件之间精密复杂的交互，包括对处理器生成的每个地址的硬件翻译。其基本思想是把一个进程虚拟存储器
的内容存储在磁盘上，然后用主存作为磁盘的高速缓存。

文件：
文件就是字节序列，仅此而已。
文件向应用程序提供了一个统一的视角，来看待系统中可能含有的所有各式各样的I/O设备，包括磁盘，键盘，显示器，甚至网络，都可以视为文件。
*/

/*
超线程：
有时称为同时多线程（sinultaneous multi-threading），是一项允许一个CPU执行多个控制流的技术。它涉及CPU某些硬件有多个备份，比如程
序计数器和寄存器文件，而其他的硬件部分只有一份，比如执行浮点算术运算的单元。Intel Core i7处理器可以让一个核执行两个线程，所以一个
4核的系统实际上可以并行地执行8个线程。

指令级并行：
在较低的抽象层次上，现代处理器可以同时执行多条指令的属性称为指令级并行。

单指令、多数据并行：
在最低层次上，许多现代处理器拥有特殊的硬件，允许一条指令产生多个可以并行执行的操作，这种方式称为单指令、多数据，即SIMD并行。例
如，并行地对4对单精度浮点数做加法的指令。
*/

/*
计算机系统中抽象的重要性：
在处理器里，指令集结构提供了对实际处理器硬件的抽象。使用这个抽象，机器代码程序表现得就好像它是运行在一个一次只执行一条指令的处理
器上。底层的硬件比抽象描述的要复杂精细得多，它并行地执行多条指令，但又总是与那个简单有序的模型保持一致。只要执行模型一样，不同的
处理器实现也能执行同样的机器代码，而又提供不同的开销和性能。
*/

/*
信息的表示和处理：
浮点数运算有完全不同于整数的数学属性。虽然溢出会产生特殊的值：正无穷，但是一组正数的乘积总是正的。由于表示的精度有限，浮点运算是
不可结合的。例如，在大多数机器上，C表达式(3.14 + 1e20) - 1e20求得的值会是0.0，而3.14 + (1e20 - 1e20)求得的值会是3.14。整数运
算和浮点数运算会有不同的数学属性是因为它们处理数字表示有限性的方式不同---整数的表示虽然只能编码一个相对较小的数值范围，但是这种表
示是精确的。而浮点数虽然可以编码一个较大的数值范围，但是这种表示只是近似的。

信息存储：
大多数计算机使用8位的块，或者字节（byte），作为最小的可寻址的存储器单位。机器级程序将存储器视为一个非常大的字节数组，称为虚拟存
储器（virtual memory）。存储器的每个字节都由一个唯一的数字来标识，称为它的地址，所有可能地址的集合称为虚拟地址空间（virtual 
address space）。

十六进制表示法：
一个字节由8位组成。在二进制表示法中，它的值域是00000000-11111111。如果用十进制整数表示，它的值域就是0-255。两种表示法对于描
述位模式来说都不是非常方便。二进制表示法太冗长，而十进制表示法与位模式的互相转化又很麻烦。替代的方法是，以16为基数，或者叫十六进
制（hexadecimal）数，来表示位模式。十六进制（简写为hex）使用数字0-9，以及字符A-F来表示16个可能的值。用十六进制来书写，一个字
节的值域为00-FF。
如果给定一个二进制数字，你可以首先把它分为每4位一组，再把它转换为十六进制。不过要注意，如果位的总数不是4的倍数，最左边的一组少于
4位，前面用0补足，然后将每个4位组转换为相应的十六进制数字。
十进制和十六进制表示法之前的转换需要使用乘法或者除法来处理。将一个十进制数字x转换为十六进制，可以反复地用16除x，得到一个商q和一
个余数r，也就是x = q X 16 + r。然后，我们用十六进制数字表示的r作为最低位数字，并且通过对q反复进行这个过程得到剩下的数字。例如：
考虑十进制314156的转换：
314156	= 19634 X 16 + 12		(C)
19634	= 1227 X 16 + 2			(2)
1227		= 76 X 16 + 11				(B)
76			= 4 X 16 + 12				(C)
4			= 0 X 16 + 4					(4)
这样我们就得到了十六进制的表示法：0x4CB2C。

字：
每台计算机都有一个字长（word size），指明整数和指针的大小。因为虚拟地址是以这样的一个字来编码的，所以字长决定的最重要的系统参数就
是虚拟地址空间的最大大小。也就是说，对于一个字长为w位的机器而言，虚拟地址的范围为0~2^w-1，程序最多访问2^w个字节。
今天大多数计算机的字长都是32位。这就限定了虚拟地址空间为4千兆字节（写作4GB）。
“长”整数使用机器的全字长。

寻址和字节顺序：
对于跨越多字节的程序对象，我们必须建立两个规则：
这个对象的地址是什么？
在存储器中如何排列这些字节？
在几乎所有的机器上，多字节对象都被存储为连续的字节序列，对象的地址为所使用字节中最小的地址。
字0x01234567的小端法表示为：0x100(67)-0x101(45)-0x102(23)-0x103(01)。注意，高位字节的十六进制值为0x01，而低位字节值为
0x67。
*/

/*
Boost.Integer:
Boost.Integer provides integer type support, particularly(格外，尤其) helpful in generic programming(泛型编程).It provides the means 
to select an integer type based upon its properties, like the number of bits or the maximum supported value, as well as compile-time 
bit mask selection.There is a derivative(衍生物) of std::numeric_limits that provides integral constant expressions for min and max.
Finally,it provides two compile-time algorithms: determining the highest power of two in a compile-time value; and computing min 
and max of constant expressions.

1,Forward Declarations:
<boost/integer_fwd.hpp>
Forward declarations of classes and class templates - for use when just the name of a class is needed.

2,Integer Traits:
<boost/integer_traits.hpp>
Class template boost::integer_traits, derives from std::numeric_limits and adds const_min and const_max members.

3,Integer Type Selection:
<boost/integer.hpp>
Templates for integer type selection based on properties such as maximum value or number of bits: Use to select the type of an 
integer when some property such as maximum value or number of bits is known. Useful for generic programming.

4,Integer Masks:
<boost/integer/integer_mask.hpp>
Templates for the selection of integer masks, single or lowest group, based on the number of bits: Use to select a particular mask 
when the bit position(s) are based on a compile-time variable. Useful for generic programming.

5,Compile time log2 Calculation:
<boost/integer/static_log2.hpp>
Template for finding the highest power of two in a number: Use to find the bit-size/range based on a maximum value. Useful for 
generic programming.

6,Compile time min/max calculation:
<boost/integer/static_min_max.hpp>
Templates for finding the extrema(极值) of two numbers: Use to find a bound based on a minimum or maximum value. Useful for 
generic programming.

Integer Traits:
Motivation:
The C++ Standard Library <limits> header supplies a class template numeric_limits<> with specializations for each fundamental 
type.
For integer types, the interesting members of std::numeric_limits<> are:
static const bool is_specialized;			//Will be true for integer types.
static T min() throw();						//Smallest representable value.
static T max() throw();					//Largest representable value.
static const int digits;						//For integers, the number of value bits.
static const int digits10;					//The number of base 10 digits that can be represented.
static const bool is_signed;				//True if the type is signed.
static const bool is_integer;				//Will be true for all integer types.
For many uses, these are sufficient(足够). But min() and max() are problematical(有问题) because they are not constant expressions 
(std::5.19), yet some usages require constant expressions.
The template class integer_traits addresses(解决了) this problem.
Synopsis(概要，简介):
namespace boost
{
	template<class T>
	class integer_traits : public std::numeric_limits<T>
	{
	public:
		static const bool is_integral = false;
		//
		//These members are defined only if T is a built-in integal type:
		//
		static const T const_min = implementation-defined;
		static const T const_max = implementation-defined;
	};
}
Description:
Template class integer_traits is derived from std::numeric_limits. The primary specialization adds the single bool member is_integral 
with the compile-time constant value false. However, for all integral types T (std::3.9.1/7 [basic.fundamental]), there are 
specializations provided with the following compile-time constants defined:
is_integral:
bool
true
const_min:
T
equivalent to std::numeric_limits<T>::min()
const_max:
T
equivalent to std::numeric_limits<T>::max()
Note: The is_integral flag is provided, because a user-defined integer class should specialize 
std::numeric_limits<>::is_integer = true, while compile-time constants const_min and const_max are not provided for that 
user-defined class, unless boost::integer_traits is also specialized.

Integer Type Selection:
The <boost/integer.hpp> type selection templates allow integer types to be selected based on desired characteristics such as 
number of bits or maximum value. This facility is particularly useful for solving generic programming problems.
Synopsis:
namespace boost
{
	//fast integers from least integers
	template<typename LeastInt>
	struct int_fast_t
	{
		typedef implementation-defined-type type;
	};

	//signed
	template<int Bits>
	struct int_t
	{
		//Member exact may or may not be defined depending upon Bits
		typedef implementation - defined - type exact;
		typedef implementation - defined - type least;
		typedef int_fast_t<least>::fast fast;
	};

	//unsigned
	template<int Bits>
	struct uint_t
	{
		//Member exact may or may not be defined depending upon Bits
		typedef implementation - defined - type exact;
		typedef implementation - defined - type least;
		typedef int_fast_t<least>::fast fast;
	};

	//signed
	template<long long MaxValue>
	struct int_max_value_t
	{
		typedef implementation - defined - type least;
		typedef int_fast_t<least>::fast fast;
	};

	template<long long MinValue>
	struct int_min_value_t
	{
		typedef implementation - defined - type least;
		typedef int_fast_t<least>::fast fast;
	};

	//unsigned
	template<unsigned long long Value>
	struct uint_value_t
	{
		typedef implementation - defined - type least;
		typedef int_fast_t<least>::fast fast;
	};
}//namespace boost
Easiest-to-Manipulate Types:
The int_fast_t class template maps its input type to the next-largest type that the processor can manipulate the easiest, or to itself 
if the input type is already an easy-to-manipulate type. For instance, processing a bunch of char objects may go faster if they were 
converted to int objects before processing. The input type, passed as the only template parameter, must be a built-in integral type,
except bool. Unsigned integral types can be used, as well as signed integral types. The output type is given as the nested type fast.
Implementation Notes: By default, the output type is identical to the input type. Eventually, this code's implementation should be 
customized for each platform to give accurate mappings between the built-in types and the easiest-to-manipulate built-in types. 
Also, there is no guarantee that the output type actually is easier to manipulate than the input type.
Sized Types:
The int_t, uint_t, int_max_value_t, int_min_value_t, and uint_value_t class templates find the most appropiate built-in integral type 
for the given template parameter. This type is given by the nested type least. The easiest-to-manipulate version of that type is given 
by the nested type fast. The following table describes each template's criteria.
Table 1.Criteria(标准) for the Sized Type Class Templates:
boost::int_t<N>::least
The smallest, built-in, signed integral type with at least N bits, including the sign bit. The parameter should be a positive number. A 
compile-time error results if the parameter is larger than the number of bits in the largest integer type.

boost::int_t<N>::fast
The easiest-to-manipulate, built-in, signed integral type with at least N bits, including the sign bit. The parameter should be a 
positive number. A compile-time error results if the parameter is larger than the number of bits in the largest integer type.

boost::int_t<N>::exact
A built-in, signed integral type with exactly N bits, including the sign bit. The parameter should be a positive number. Note that the 
member exact is defined only if there exists a type with exactly N bits.

boost::uint_t<N>::least
The smallest, built-in, unsigned integral type with at least N bits. The parameter should be a positive number. A compile-time error 
results if the parameter is larger than the number of bits in the largest integer type.

boost::uint_t<N>::fast
The easiest-to-manipulate, built-in, unsigned integral type with at least N bits. The parameter should be a positive number. A 
compile-time error results if the parameter is larger than the number of bits in the largest integer type.

boost::uint_t<N>::exact
A built-in, unsigned integral type with exactly N bits. The parameter should be a positive number. A compile-time error results if the 
parameter is larger than the number of bits in the largest integer type. Note that the member exact is defined only if there exists a 
type with exactly N bits.

boost::int_max_value_t<V>::last
The smallest, built-in, signed integral type that can hold all the values in the inclusive range 0 - V. The parameter should be a 
positive number.

boost::int_max_value_t<V>::fast
The easiest-to-manipulate, built-in, signed integral type that can hold all the values in the inclusive range 0 - V. The parameter 
should be a positive number.

boost::int_min_value_t<V>::least
The smallest, built-in, signed integral type that can hold all the values in the inclusive range V - 0. The parameter should be a 
negative number.

boost::int_min_value_t<V>::fast
The easiest-to-manipulate, built-in, signed integral type that can hold all the values in the inclusive range V - 0. The parameter 
should be a negative number.

boost::uint_value_t<V>::least
The smallest, built-in, unsigned integral type that can hold all positive values up to and including V. The parameter should be a 
positive number.

boost::uint_value_t<V>::fast
The easiest-to-manipulate, built-in, unsigned integral type that can hold all positive values up to and including V. The parameter 
should be a positive number.
Example:
#include <boost/integer.hpp>
int main()
{
	boost::int_t<24>::least my_var;  //my_var has at least 24-bits
	//...
	//This one is guarenteed not to be truncated:
	boost::int_max_value_t<1000>::least my1000 = 1000;
	//...
	//This one is guarenteed not to be truncated, and as fast to manipulate as possible, its size may be greater than that of my1000:
	boost::int_max_value_t<1000>::fast my_fast1000 = 1000;
}

Integer Masks:
Overview:
The class templates in <boost/integer/integer_mask.hpp> provide bit masks for a certain bit position or a contiguous-bit(连续的位) 
pack of a certain size. The types of the masking constants come from the integer type selection templates header.
Synopsis:
#include <cstddef>  //for std::size_t
namespace boost
{

	template <std::size_t Bit>
	struct high_bit_mask_t
	{
		typedef implementation-defined-type least;
		typedef implementation-defined-type fast;

		static const least		high_bit				= implementation-defined;
		static const fast			high_bit_fast			= implementation-defined;

		static const std::size_t bit_position		= Bit;
	};

	template <std::size_t Bits>
	struct low_bits_mask_t
	{
		typedef implementation-defined-type least;
		typedef implementation-defined-type fast;

		static const least		sig_bits					= implementation-defined;
		static const fast			sig_bits_fast			= implementation-defined;

		static const std::size_t bit_count			= Bits;
	};

	//Specializations for low_bits_mask_t exist for certain bit counts.
}//namespace boost
Single Bit-Mask Class Template:
The boost::high_bit_mask_t class template provides constants for bit masks representing the bit at a certain position. The masks are 
equivalent to the value 2^Bit, where Bit is the template parameter. The bit position must be a nonnegative number from zero to Max,
where Max is one less than the number of bits supported by the largest unsigned built-in integral type.
The following table describes the members of an instantiation of high_bit_mask_t.
Table 2. Members of the boost::high_bit_mask_t Class Template:
least
The smallest, unsigned, built-in type that supports the given bit position.

fast
The easiest-to-manipulate analog of least.

high_bit
A least constant of the value 2Bit.

high_bit_fast
A fast analog of high_bit.

bit_position
The value of the template parameter, in case its needed from a renamed instantiation of the class template.

Group Bit-Mask Class Template:
The boost::low_bits_mask_t class template provides constants for bit masks equivalent to the value (2^Bits - 1), where Bits is the 
template parameter. The parameter Bits must be a non-negative integer from zero to Max, where Max is the number of bits 
supported by the largest, unsigned, built-in integral type.
The following table describes the members of low_bits_mask_t.
Table 3. Members of the boost::low_bits_mask_t Class Template:
least
The smallest, unsigned built-in type that supports the given bit count.

fast
The easiest-to-manipulate analog of least.

sig_bits
A least constant of the desired bit-masking value.

sig_bits_fast
A fast analog of sig_bits.

bit_count
The value of the template parameter, in case its needed from a renamed instantiation of the class template.

Implementation Notes:
When Bits is the exact size of a built-in unsigned type, the implementation has to change to prevent undefined behavior. Therefore,
there are specializations of low_bits_mask_t at those bit counts.

Example:
#include <boost/integer/integer_mask.hpp>
int main()
{
	typedef boost::high_bit_mask_t<29> mask1_type;
	typedef boost::low_bits_mask_t<15> mask2_type;

	mask1_type::least my_var1;
	mask2_type::fast my_var2;
	//...

	my_var1 |= mask1_type::high_bit;
	my_var2 &= mask2_type::sig_bits_fast;

	//...
}

Rationale:
The class templates in this header are an extension of the integer type selection class templates. The new class templates provide 
the same sized types, but also convenient masks to use when extracting the highest or all the significant bits when the containing 
built-in type contains more bits. This prevents contamination of values by the higher, unused bits.

Compile Time log2 Calculation:
The class template in <boost/integer/static_log2.hpp> determines the position of the highest bit in a given value. This facility is 
useful for solving generic programming problems.
Synopsis:
namespace boost
{
	typedef implementation-defined static_log2_argument_type;
	typedef implementation-defined static_log2_result_type;

	template <static_log2_argument_type arg>
	struct static_log2
	{
		static const static_log2_result_type value = implementation-defined;
	};

	template <>
	struct static_log2< 0 >
	{
		//The logarithm of zero is undefined.
	};
}//namespace boost
Usage:
The boost::static_log2 class template takes one template parameter, a value of type static_log2_argument_type. The template only 
defines one member, value, which gives the truncated, base-two logarithm of the template argument.
Since the logarithm of zero, for any base, is undefined, there is a specialization of static_log2 for a template argument of zero. This 
specialization has no members, so an attempt to use the base-two logarithm of zero results in a compile-time error.
Note:
static_log2_argument_type is an unsigned integer type (C++ standard, 3.9.1p3).
static_log2_result_type is an integer type (C++ standard, 3.9.1p7).
Rationale
The base-two (binary) logarithm, abbreviated lb, function is occasionally used to give order-estimates of computer algorithms. The 
truncated logarithm can be considered the highest power-of-two in a value, which corresponds to the value's highest set bit (for 
binary integers). Sometimes the highest-bit position could be used in generic programming, which requires the position to be 
available statically (i.e. at compile-time).

Compile time min/max calculation:
The class templates in <boost/integer/static_min_max.hpp> provide a compile-time evaluation of the minimum or maximum of two 
integers. These facilities are useful for generic programming problems.
Synopsis:
namespace boost
{

	typedef implementation-defined static_min_max_signed_type;
	typedef implementation-defined static_min_max_unsigned_type;

	template <static_min_max_signed_type Value1, static_min_max_signed_type Value2 >
	struct static_signed_min;

	template <static_min_max_signed_type Value1, static_min_max_signed_type Value2>
	struct static_signed_max;

	template <static_min_max_unsigned_type Value1, static_min_max_unsigned_type Value2>
	struct static_unsigned_min;

	template <static_min_max_unsigned_type Value1, static_min_max_unsigned_type Value2>
	struct static_unsigned_max;
}//namespace boost
Usage:
The four class templates provide the combinations for finding the minimum or maximum of two signed or unsigned (long) parameters,
Value1 and Value2, at compile-time. Each template has a single static data member, value, which is set to the respective minimum 
or maximum of the template's parameters.

Example:
#include <boost/integer/static_min_max.hpp>

template < unsigned long AddendSize1, unsigned long AddendSize2 >
class adder
{
public:
	static  unsigned long  const  addend1_size = AddendSize1;
	static  unsigned long  const  addend2_size = AddendSize2;
	static  unsigned long  const  sum_size = boost::static_unsigned_max<AddendSize1, AddendSize2>::value + 1;

	typedef int  addend1_type[ addend1_size ];
	typedef int  addend2_type[ addend2_size ];
	typedef int  sum_type[ sum_size ];

	void  operator ()( addend1_type const &a1, addend2_type const &a2, sum_type &s ) const;
};

//...

int main()
{
	int const a1[] = { 0, 4, 3 };		//340
	int const a2[] = { 9, 8 };			//89
	int s[ 4 ];
	adder<3,2> obj;

	obj( a1, a2, s );						//'s' should be 429 or { 9, 2, 4, 0 }
	//...
}
Rationale:
Sometimes the minimum or maximum of several values needs to be found for later compile-time processing, e.g. for a bound for 
another class template.
*/

/*
variant与any有些类似,是一种可变类型,是对C++中union概念的增强和扩展.普通的union只能持有POD(普通数据类型)(C++11新标准中,含有构造函数
或析构函数的类类型也可以作为union的成员类型).
variant的接口与any也很类似,但它是一个模板类.
variant是有界类型(any是无界类型),允许保存的数据类型必须在模板参数列表中声明.对类型参数的最低要求是可拷贝构造且析构不抛出异常.

无参的构造函数将把variant的值用第一个模板参数(T1)的缺省构造函数初始化,如果T1没有缺省构造函数,那么variant也不能缺省构造.
带参数的构造函数参数可以是模板类型参数中任意类型的值,variant将初始化为这种类型.

variant支持拷贝构造和赋值,赋值操作要求模板类型参数也都支持赋值操作.

成员函数empty()永远返回false,因为variant不能是空的,它总有值.
成员函数which()返回variant当前值的类型在模板参数列表的索引号(从0开始计数).

variant提供了比较操作符,这使得它可以被用作关联容器的元素或者用于排序,但要求variant的所有模板参数也是可以比较的.
variant也支持流输出,但同样要求每一个模板参数是可流输出的.
*/