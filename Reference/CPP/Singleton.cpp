/*
class Singleton : boost::noncopyable
使用默认的私有继承是允许的.
也可以显式写出private或者public修饰词,效果是相同的.
不写修饰词,更表明了has-a关系,而不是is-a.

如果企图copy构造或者copy赋值,则编译无法通过.
*/

/*
singleton类声明摘要如下:
template <typename T>
class singleton : boost::noncopyable
{
public:
	static const	T& get_const_instance();
	static			T& get_mutable_instance();
};

对模板参数T的要求是:具有默认构造函数,构造函数不抛异常.
假设有个类名称为LogManager,其有个函数为print().
用法1:
typedef singleton<LogManager> SLogManager;
SLogManager::get_const_instance().print();
SLogManager::get_mutable_instance().print();
用法2:
class LogManager : public singleton<LogManager>
{
};
LogManager::get_const_instance().print();
LogManager::get_mutable_instance().print();
与方法1的模板参数方式相比,继承方式实现单件模式更为彻底一些,使被单件话的类成为了一个真正的单件类,而模板参数方式则更"温和"一些,它包装
(wrap)了被单件化的类,对原始类没有任何影响.
*/