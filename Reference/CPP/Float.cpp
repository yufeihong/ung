/*
Boost.MathImpl

Specified-width floating-point typedefs

Overview
The header <boost/cstdfloat.hpp> provides optional standardized floating-point typedefs having specified widths. These are useful for writing portable 
code because they should behave identically on all platforms. These typedefs are the floating-point analog(类似物) of specified-width integers in 
<cstdint> and stdint.h.
The typedefs are based on N3626 proposed(建议，提议) for a new C++14 standard header <cstdfloat> and N1703 proposed for a new C language 
standard header <stdfloat.h>.
All typedefs are in namespace boost (would be in namespace std if eventually(最终) standardized).
The typedefs include float16_t, float32_t, float64_t, float80_t, float128_t, their corresponding least and fast types, and the corresponding 
maximum-width type. The typedefs are based on underlying(底层) built-in types such as float, double, or long double, or based on other 
compiler-specific non-standardized types such as __float128. The underlying types of these typedefs must conform(符合) with the corresponding 
specifications of binary16, binary32, binary64, and binary128 in IEEE_floating_point floating-point format.
The 128-bit floating-point type (of great interest in scientific and numeric programming) is not required in the Boost header, and may not be supplied 
for all platforms/compilers, because compiler support for a 128-bit floating-point type is not mandated(授权) by either the C standard or the C++ 
standard.

Rationale(解释)
The implementation of <boost/cstdfloat.hpp> is designed to utilize(利用，采用) <float.h>, defined in the 1989 C standard. The preprocessor is used to 
query certain preprocessor definitions in <float.h> such as FLT_MAX, DBL_MAX, etc. Based on the results of these queries, an attempt is made to 
automatically detect the presence(存在) of built-in floating-point types having specified widths. An unequivocal(明确的) test regarding(关于) 
conformance(一致性) with IEEE_floating_point (IEC599) based on std::numeric_limits<>::is_iec559 is performed with BOOST_STATIC_ASSERT.
In addition(此外，另外), this Boost implementation <boost/cstdfloat.hpp> supports an 80-bit floating-point typedef if it can be detected, and a 128-bit 
floating-point typedef if it can be detected, provided that the underlying types conform with IEEE-754 precision extension 
(ifstd::numeric_limits<>::is_iec559 is true for this type).
The header <boost/cstdfloat.hpp> makes the standardized floating-point typedefs safely available in namespace boost without placing any names in 
namespace std. The intention(意图) is to complement(补充) rather than compete with a potential(潜在) future C/C++ Standard Library that may contain 
these typedefs. Should some future C/C++ standard include <stdfloat.h> and <cstdfloat>, then <boost/cstdfloat.hpp> will continue to function, but 
will become redundant(冗) and may be safely deprecated.
Because <boost/cstdfloat.hpp> is a Boost header, its name conforms to the boost header naming conventions(惯例), not the C++ Standard Library 
header naming conventions.
Note
<boost/cstdfloat.hpp> cannot synthesize(合成，综合) or create a typedef if the underlying type is not provided by the compiler. For example, if a 
compiler does not have an underlying floating-point type with 128 bits (highly sought-after in scientific and numeric programming), then float128_t 
and its corresponding least and fast types are not provided by <boost/cstdfloat.hpp>.
Warning
If <boost/cstdfloat.hpp> uses a compiler-specific non-standardized type (not derived from float, double, or long double) for one or more of its 
floating-point typedefs, then there is no guarantee that specializations(专业化，特化) of numeric_limits<> will be available for these types. Typically, 
specializations of numeric_limits<> will only be available for these types if the compiler itself supports corresponding specializations for the underlying 
type(s), exceptions are GCC's __float128 type and Intel's _Quad type which are explicitly supported via our own code.
Warning
As an implementation(履行) artifact(人工品), certain C macro names from <float.h> may possibly be visible to users of <boost/cstdfloat.hpp>. Don't 
rely on using these macros; they are not part of any Boost-specified interface. Use std::numeric_limits<> for floating-point ranges, etc. instead.
Tip
For best results, <boost/cstdfloat.hpp> should be #included before other headers that define generic code(通用代码) making use(利用) of standard 
library functions defined in <cmath>.
This is because <boost/cstdfloat.hpp> may define overloads of standard library functions where a non-standard type (i.e. other than float, double, or 
long double) is used for one of the specified width types. If generic code (for example in another Boost.MathImpl header) calls a standard library function,
then the correct overload will only be found if these overloads are defined prior to the point of use. See implementation for more details.
For this reason, making #include <boost/cstdfloat.hpp> the first include is usually best.

Exact-Width Floating-Point typedefs
The typedef float#_t, with # replaced by the width, designates a floating-point type of exactly # bits. For example float32_t denotes(表示，意味着) a 
single-precision floating-point type with approximately(大约) 7 decimal digits(小数位数) of precision (equivalent(相当的) to binary32 in 
IEEE_floating_point).
Floating-point types in C and C++ are specified to be allowed to have (optionally) implementation-specific(实现特定的) widths and formats. However, if 
a platform supports underlying floating-point types (conformant with IEEE_floating_point) with widths of 16, 32, 64, 80, 128 bits, or any combination 
thereof(它们的组合), then <boost/cstdfloat.hpp> does provide the corresponding typedefs float16_t, float32_t, float64_t, float80_t, float128_t, their 
corresponding least and fast types, and the corresponding maximum-width type.
How to tell which widths are supported
The definition (or not) of a floating-point constant macro is the way to test if a specific width is available on a platform.

#if defined(BOOST_FLOAT16_C)
//Can use boost::float16_t.
#endif

#if defined(BOOST_FLOAT32_C)
//Can use boost::float32_t.
#endif

#if defined(BOOST_FLOAT64_C)
//Can use boost::float64_t.
#endif

#if defined(BOOST_FLOAT80_C)
//Can use boost::float80_t.
#endif

#if defined(BOOST_FLOAT128_C)
//Can use boost::float128_t.
#endif
This can be used to write code which will compile and run (albeit(尽管) differently) on several platforms. Without these tests, if a width, say float128_t 
is not supported, then compilation would fail. (It is of course, rare for float64_t or float32_t not to be supported).
The number of bits in just the significand(尾数) can be determined using:
std::numeric_limits<boost::floatmax_t>::digits
and from this one can safely infer(推断) the total number of bits because the type must be IEEE754 format, so, for example, if 
std::numeric_limits<boost::floatmax_t>::digits == 113, then floatmax_t must be float128_t.
The total number of bits using floatmax_t can be found thus(从而，因而，由此，于是，便):
const int fpbits =
  (std::numeric_limits<boost::floatmax_t>::digits == 113) ? 128 :
  (std::numeric_limits<boost::floatmax_t>::digits == 64) ? 80 :
  (std::numeric_limits<boost::floatmax_t>::digits == 53) ? 64 :
  (std::numeric_limits<boost::floatmax_t>::digits == 24) ? 32 :
  (std::numeric_limits<boost::floatmax_t>::digits == 11) ? 16 :
  0;																									//Unknown - not IEEE754 format.
 std::cout << fpbits << " bits." << std::endl;
and the number of 'guaranteed' decimal digits(可以保证的小数位数) using
std::numeric_limits<boost::floatmax_t>::digits10
and the maximum number of possibly(可能的最大数量) significant decimal digits using
std::numeric_limits<boost::floatmax_t>::max_digits10
Tip
max_digits10 is not always supported, but can be calculated at compile-time using the Kahan formula.
Note
One could test
std::is_same<boost::floatmax_t, boost::float128_t>::value == true
but this would fail to compile on a platform where boost::float128_t is not defined. So use the MACROs BOOST_FLOATnnn_C.

Minimum-width(最小宽度) floating-point typedefs
The typedef float_least#_t, with # replaced by the width, designates a floating-point type with a width of at least # bits, such that(这样) no 
floating-point type with lesser size has at least the specified width. Thus, float_least32_t denotes the smallest floating-point type with a width of at 
least 32 bits.
Minimum-width floating-point types are provided for all existing exact-width floating-point types on a given platform.
For example, if a platform supports float32_t and float64_t(这些是exact-width), then float_least32_t and float_least64_t will also be supported, etc.

Fastest floating-point typedefs
The typedef float_fast#_t, with # replaced by the width, designates the fastest floating-point type with a width of at least # bits.
There is no absolute guarantee that these types are the fastest for all purposes. In any case, however, they satisfy(满足) the precision and width 
requirements.
Fastest minimum-width floating-point types are provided for all existing exact-width floating-point types on a given platform.
For example, if a platform supports float32_t and float64_t, then float_fast32_t and float_fast64_t will also be supported, etc.

Greatest-width(最大宽度) floating-point typedef
The typedef floatmax_t designates a floating-point type capable of(能够) representing any value of any floating-point type in a given platform most 
precisely.
The greatest-width typedef is provided for all platforms, but, of course, the size may vary.
To provide floating-point constants most precisely(最精确) for a floatmax_t type, use the macro BOOST_FLOATMAX_C.
For example, replace a constant 123.4567890123456789012345678901234567890 with
BOOST_FLOATMAX_C(123.4567890123456789012345678901234567890)
If, for example, floatmax_t is float64_t then the result will be equivalent to a long double suffixed(后缀) with L, but if floatmax_t is float128_t then the 
result will be equivalent to a quad type suffixed with Q (assuming, of course, that float128 is supported).
If we display with max_digits10, the maximum possibly significant decimal digits:

#ifdef BOOST_FLOAT32_C
  std::cout.precision(boost::max_digits10<boost::float32_t>());								//Show all significant decimal digits,
  std::cout.setf(std::ios::showpoint);																		//including all significant trailing zeros.
  std::cout << "BOOST_FLOAT32_C(123.4567890123456789012345678901234567890) = "
	<< BOOST_FLOAT32_C(123.4567890123456789012345678901234567890) << std::endl;
	//BOOST_FLOAT32_C(123.4567890123456789012345678901234567890) = 123.456787
#endif
then on a 128-bit platform (GCC 4.8.1. with quadmath):

BOOST_FLOAT32_C(123.4567890123456789012345678901234567890) = 123.456787							//小数位数：6
BOOST_FLOAT64_C(123.4567890123456789012345678901234567890) = 123.45678901234568				//小数位数：14
BOOST_FLOAT80_C(123.4567890123456789012345678901234567890) = 123.456789012345678903
BOOST_FLOAT128_C(123.4567890123456789012345678901234567890) = 123.456789012345678901234567890123453

Floating-Point Constant Macros
All macros of the type BOOST_FLOAT16_C, BOOST_FLOAT32_C, BOOST_FLOAT64_C, BOOST_FLOAT80_C, BOOST_FLOAT128_C, and 
BOOST_FLOATMAX_C are always defined after inclusion of <boost/cstdfloat.hpp>.
These allow floating-point constants of at least the specified width to be declared:
//Declare Archimedes'(阿基米德) constant using float32_t with approximately 7 decimal digits of precision.
static const boost::float32_t pi = BOOST_FLOAT32_C(3.1415926536);
//Declare the Euler-gamma constant with approximately 15 decimal digits of precision.
static const boost::float64_t euler = BOOST_FLOAT64_C(0.57721566490153286060651209008240243104216);
//Declare the Golden Ratio constant with the maximum decimal digits of precision that the platform supports.
static const boost::floatmax_t golden_ratio = BOOST_FLOATMAX_C(1.61803398874989484820458683436563811772);
Tip
Boost.MathImpl provides many constants 'built-in', so always use Boost.MathImpl constants if available, for example:
//Display the constant pi to the maximum available precision.
boost::floatmax_t pi_max = boost::math::constants::pi<boost::floatmax_t>();
std::cout.precision(std::numeric_limits<boost::floatmax_t>::digits10);
std::cout << "Most precise pi = "  << pi_max << std::endl;
//If floatmax_t is float_128_t, then
//Most precise pi = 3.141592653589793238462643383279503
from cstdfloat_example.cpp.
*/

/*
测试代码：
#include "boost/cstdfloat.hpp"

#include <iostream>

int main()
{
	const int floatTotalBits =
		(std::numeric_limits<boost::floatmax_t>::digits == 113) ? 128 :
		(std::numeric_limits<boost::floatmax_t>::digits == 64) ? 80 :
		(std::numeric_limits<boost::floatmax_t>::digits == 53) ? 64 :
		(std::numeric_limits<boost::floatmax_t>::digits == 24) ? 32 :
		(std::numeric_limits<boost::floatmax_t>::digits == 11) ? 16 :
		0;
	std::cout << "floatTotalBits:" << floatTotalBits << std::endl;

	std::cout << std::endl;

	const int floatDigits10 = std::numeric_limits<boost::floatmax_t>::digits10;
	std::cout << "floatDigits10:" << floatDigits10 << std::endl;

	const int floatMaxDigits10 = std::numeric_limits<boost::floatmax_t>::max_digits10;
	std::cout << "floatMaxDigits10:" << floatMaxDigits10 << std::endl;

	std::cout << std::endl;

#ifdef BOOST_FLOAT16_C
	std::cout << "BOOST_FLOAT16_C defined" << std::endl;
#else
	std::cout << "BOOST_FLOAT16_C not defined" << std::endl;
#endif

#ifdef BOOST_FLOAT32_C
	std::cout << "BOOST_FLOAT32_C defined" << std::endl;
#else
	std::cout << "BOOST_FLOAT32_C not defined" << std::endl;
#endif

#ifdef BOOST_FLOAT64_C
	std::cout << "BOOST_FLOAT64_C defined" << std::endl;
#else
	std::cout << "BOOST_FLOAT64_C not defined" << std::endl;
#endif

#ifdef BOOST_FLOAT128_C
	std::cout << "BOOST_FLOAT128_C defined" << std::endl;
#else
	std::cout << "BOOST_FLOAT128_C not defined" << std::endl;
#endif

#ifdef BOOST_FLOATMAX_C
	std::cout << "BOOST_FLOATMAX_C defined" << std::endl;
#else
	std::cout << "BOOST_FLOATMAX_C not defined" << std::endl;
#endif

	std::cout << std::endl;

	std::cout << "123.4567890123456789012345678901234567890:" << std::endl;
	std::cout.setf(std::ios::showpoint);
#ifdef BOOST_FLOAT32_C
	std::cout.precision(std::numeric_limits<boost::float32_t>::max_digits10);
	std::cout << "BOOST_FLOAT32_C:" << BOOST_FLOAT32_C(123.4567890123456789012345678901234567890) << std::endl;
#endif
#ifdef BOOST_FLOAT64_C
	std::cout.precision(std::numeric_limits<boost::float64_t>::max_digits10);
	std::cout << "BOOST_FLOAT64_C:" << BOOST_FLOAT64_C(123.4567890123456789012345678901234567890) << std::endl;
#endif
#ifdef BOOST_FLOATMAX_C
	std::cout.precision(std::numeric_limits<boost::floatmax_t>::max_digits10);
	std::cout << "BOOST_FLOATMAX_C:" << BOOST_FLOATMAX_C(123.4567890123456789012345678901234567890) << std::endl;
#endif

	return 0;
}

测试结果：
如图boost_cstdfloat所示
*/