/*
第17章：
算法之美在于算法不仅独立于底层元素的类型，而且还独立于操作的容器的类型。算法仅使用迭代器接口执行
操作。

大部分算法都接受回调(callback)，回调可以是一个函数指针，也可以是行为上类似于函数指针的对象。例如，
重载了运算符operator()的对象或内嵌lambda表达式。为了方便起见，STL还提供了一组类，用于创建算法
使用的回调对象。这些回调对象称为函数对象，或仿函数(functor)。

算法的魔力在于，算法把迭代器作为中介操作容器，而不直接操作容器本身。这样，算法没有绑定至特定的
容器实现。所有STL算法都实现为函数模板的形式，其中模板类型参数一般都是迭代器类型。迭代器本身指定
为函数的参数。模板化的函数通常可以通过函数参数推导出模版类型。因此，通常情况下可以像调用普通函数
(而非模板)那样调用算法。

算法对传递给它的迭代器有一些要求。例如，copy_backward()需要双向迭代器，stable_sort()需要随机访
问迭代器.这意味着算法不能操作没有提供所需迭代器的容器。forward_list容器只支持前向迭代器，不支持双向访问
迭代器或随机访问迭代器，因此copy_backward()和stable_sort()不能用于forward_list。

大部分算法都定义在<algorithm>头文件中，一些数值算法定义在<numeric>头文件中。它们都在std名称空间中。

警告，如果find()没有找到元素，那么返回的迭代器等于函数调用中指定的尾迭代器，而不是底层容器的尾迭代器。

一些容器(例如map和set)以类方法的方式提供了自己的find()版本。

泛型find()算法的是复杂度为线性时间，用于map迭代器时也是如此。而map上的find()方法的复杂度是对数时间。

find_if()和find()类似，区别在于find_if()接受谓词函数回调作为参数。find_if()算法对范围内的每个元素调用
谓词，直到谓词返回true，如果返回了true，find_if()返回引用这个元素的迭代器。

但STL没有提供find_all()或等效算法来返回匹配谓词的所有实例。

accumulate()，计算指定范围中元素的总和。
accumulate()在<numeric>中声明，而不是在<algorithm>中声明。accumulate()算法接受的第三个参数
是总和的初始值。
accumulate()的第二种形式允许调用者指定要执行的操作，而不是执行默认的加法操作。这个操作的形式是
二元回调。

在算法中使用移动语义：
与STL容器一样，STL算法也做了优化，以便在合适时使用移动语义。这可以极大地加速特定算法，例如sort()。
因此，强烈建议在需要保存到容器中的自定义元素类中实现移动语义。通过实现移动构造函数和移动赋值运算符，
任何类都可以添加移动语义。它们都标记为noexcept，因为它们不应抛出异常。

lambda表达式：
lambda表达式可以编写内嵌的匿名函数。
auto basiclambda = [] {cout << "Hello from lambda." << endl;};
basiclambda();
输出：
Hello from lambda.
如果lambda表达式不接受参数，就可以指定空圆括号或忽略它们。
lambda表达式可以返回值。返回类型在箭头后面指定，称为拖尾返回类型。
auto returninglambda = [](int a,int b) -> int {return a + b};
即使lambda表达式返回了值，也可以忽略返回类型。如果忽略了返回类型，编译器就根据函数返回类型推断规则
来推断lambda表达式的返回类型。
auto returninglambda = [](int a,int b){return a + b};
lambda表达式可以在其封装的作用域内捕捉变量。
lambda表达式的方括号部分称为lambda捕捉块(capture block)。
在捕捉块中只写出变量名，就是按值捕捉该变量。
编译器把lambda表达式转换为某种仿函数。捕捉的变量变成这个仿函数的数据成员。按值捕捉的变量复制到
仿函数的数据成员中。这些数据成员与捕捉的变量具有相同的const性质。
const double data = 1.23;
auto capturinglambda = [data]{...};
上面代码，仿函数得到const数据成员data，因为捕捉的变量是const。
仿函数总是实现了函数调用运算符。对于lambda表达式，这个函数调用运算符默认标记为const，这表示，即使
在lambda表达式中按值捕捉了非const变量，lambda表达式也不能修改其副本。
把lambda表达式指定为mutable，就可以把函数调用运算符标记为非const:
double data = 1.23;
auto capturinglambda = [data]() mutable{data *= 2.0};
在上面代码中，非const变量data是按值捕捉的，因此仿函数得到了一个非const数据成员，它是data的副本。
因为使用了mutable关键字，函数调用运算符标记为非const，所以lambda表达式体可以修改data的副本。
注意：如果指定了mutable，就必须给参数指定圆括号，即使圆括号为空。
在变量名前面加上&，就可以按引用捕捉它。
下面的示例按引用捕捉变量data，因此lambda表达式可以直接在其内部的作用域中修改data：
double data = 1.23;
auto capturinglambda = [&data]{data *= 2.0};
按引用捕捉变量时，必须确保执行lambda表达式时，该引用是有效的。
可以采用两种方式来捕捉所在作用域中的所有变量：
1，[=]:通过值捕捉所有变量
2，[&]:通过引用捕捉所有变量
还可以酌情决定捕捉哪些变量以及这些变量的捕捉方法，方法是指定一个捕捉列表，其中带有可选的默认捕捉
选项。前缀为&的变量通过引用捕捉。不带前缀的变量通过值捕捉。默认捕捉应该是捕捉列表中的第一个元素，
可以是=或&。例如：
[&x]，只通过引用捕捉x，不捕捉其他变量。
[x]，只通过值捕捉x，不捕捉其他变量。
[=,&x,&y]，默认通过值捕捉，变量x和y是例外，这两个变量通过引用捕捉。
[&,x]，默认通过引用捕捉，变量x是例外，这个变量通过值捕捉。
[&x,&x]，非法，因为标识符不允许重复。
[this]，捕捉周围的对象。即使没有使用this->，也可以在lambda表达式体中访问这个对象。
警告：
即使可行，也不建议在内部作用域中使用[=]，[&]或捕捉默认值捕捉所有变量，而应选择捕捉需要的变量。
lambda表达式的完整语法如下所示：
[capture_block](parameters) mutable exception_specification attribute_specifier -> return_type {body}
只有在不需要任何参数并且没有指定mutable,exception_specification,attribute_specifier和return_type
的情况下才能忽略参数列表。
exception_specification，用于指定lambda表达式体可以抛出的异常。
attribute_specifier，用于指定lambda表达式的属性。
return_type，返回值的类型。如果忽略，那么编译器会根据函数返回类型推断原则判断返回类型。
泛型lambda表达式：
从C++14开始，就可以给lambda表达式的参数使用自动推断类型的功能，而无需显式指定它们的具体类型。
要为参数使用自动推断类型功能，只需把类型指定为auto，类型推断规则与模板参数推断规则相同。
vector<int> ints{11,55,101,200};
vector<double> doubles{11.1,55.5,200.2};
//define a generic lambda to find values > 100
auto isGreaterThan100 = [](auto i){return i > 100;}
auto it1 = find_if(cbegin(ints),cend(ints),isGreaterThan100);
auto it2 = find_if(cbegin(doubles),cend(doubles),isGreaterThan100);
lambda捕捉表达式：
C++14支持lambda捕捉表达式，允许用任何类型的表达式初始化捕捉变量。
这可以用于在lambda表达式中引入根本不在其内部的作用域中捕捉的变量。
double pi = 3.14;
auto mylambda = [myCapture = "Pi:",pi]{cout << myCapture << pi;};
上面的代码，其中有两个变量：myCapture使用lambda捕捉表达式初始化为字符串"Pi:",pi在内部的作用域
中按值捕捉。注意：用捕捉初始化器来初始化的非引用捕捉变量，如myCapture，是通过复制来构建的，这
表示省略了const限定符。
lambda捕捉变量可以用任何类型的表达式初始化，也可以用std::move()初始化。这对于不能复制只能移动
的对象而言很重要。例如unique_ptr。默认情况下，按值捕捉要使用复制语义，所以不可能在lambda表达式
中捕捉unique_ptr。
auto myPtr = std::make_unique<double>(3.14);
auto mylambda = [p = std::move(myPtr)]{cout << *p;};
捕捉变量允许使用与内部作用域中相同的名称，例如：
auto myPtr = std::make_unique<double>(3.14);
auto mylambda = [myPtr = std::move(myPtr)]{cout << *myPtr;};
将lambda表达式用作返回值：
定义在<functional>头文件中的std::function是一个多态的函数对象包装，类似于函数指针。它可以绑定至
任何能调用的对象(仿函数，成员函数指针，函数指针和lambda表达式)，只要参数和返回类型符合包装的类型
即可。返回一个double，接受两个整数参数的函数包装定义如下：
function<double(int,int)> myWrapper;
使用std::function，可以给lambda表达式指定名称，并从函数中返回。
function<int(void)> multiplyBy2lambda(int x)
{
	return [x] {return 2 * x;};
}
C++14支持函数返回类型推断：
auto multiplyBy2lambda(int x)
{
	return [x] {return 2 * x;};
}
auto fn = multiplyBy2lambda(5);
cout << fn() << endl;
输出为10。
multiplyBy2lambda示例通过值捕捉了变量x。假设这个函数重写为通过引用捕捉变量，那么会出现bug，因为
lambda表达式会在程序后面执行，而不会在multiplyBy2lambda()函数的作用域中执行，在那里x的引用不再
有效：
auto multiplyBy2lambda(int x)
{
	return [&x] {return 2 * x;};						//bug
}
将lambda表达式用作参数：
可以编写接受lambda表达式作为参数的函数。例如，可通过这种方式实现回调函数。
void testCallback(const vector<int>& vec,const function<bool(int)>& callback)
{
	for(const auto& i : vec)
	{
		if(!callback())
		{
			break;
		}

		cout << i << " ";
	}

	cout << endl;
}
vector<int> vec{1,2,3,4,5,6,7,8,9,10};
testCallback(vec,[](int i){return i < 6;});
//输出如下：
1 2 3 4 5

函数对象：
在类中，可以重载函数调用运算符，使类的对象可以取代函数指针。这些对象称为函数对象，或仿函数。
C++提供了一些预定义的仿函数类，这些类定义在<functional>头文件中，执行最常用的回调操作。
算术函数对象：
C++提供了5类二元算术运算符的仿函数类模板：plus,minus,multiplies,divides和modulus。此外，
还提供了一元的取反操作。
算术函数对象的好处在于可将它们以回调形式传递给算法，而使用算术运算符时却不能直接这样做。
double mult = accumulate(cbegin(nums),cend(nums),1,multiplies<int>());
表达式multiplies<int>()创建了一个新的multiplies仿函数类对象，并且通过int类型实例化。
透明运算符仿函数：
C++14支持透明运算符仿函数，允许忽略模板类型参数。例如，可以只指定multiplies<>()，而不是
multiplies<int>()：
double mult = accumulate(cbegin(nums),cend(nums),1,multiplies<>());
vec<int> nums{1,2,3};
double result = accumulate(cbegin(nums),cend(nums),1.1,multiplies<int>());
结果为整数，result为6。
vec<int> nums{1,2,3};
double result = accumulate(cbegin(nums),cend(nums),1.1,multiplies<>());
结果为double，result为6.6。
这就是透明运算符(异构的)的好处。
建议总是使用透明运算符仿函数。
比较函数对象：
equal_to,no_equal_to,less,greater,less_equal和greater_equal。
逻辑函数对象：
logical_not(operator!),logical_and(operator&&)和logical_or(operator||)。逻辑操作只操作true和false值。
按位函数对象：
bit_and(operator&),bit_or(operator|)和bit_xor(operator^)。C++14增加了bit_not(operator~)。
函数对象适配器：
在使用标准提供的基本函数对象时，往往会有不搭配的感觉。
例如，使用find_if()时，不能通过less函数对象找到比某一个值小的元素，因为find_if()每次只向回调传递一个
参数而不是两个参数。函数适配器(function adapter)试图解决这个问题和其他问题。函数适配器对函数组合
(functional composition)提供了一些支持，即能够将函数组合在一起，以精确提供所需的行为。
绑定器：
绑定器(binder)可用于将函数的参数绑定至特定的值。为此要使用<functional>头文件定义的std::bind()，
它允许采用灵活的方式绑定函数的参数。即可以将函数的参数绑定至固定值，甚至还能够重新安排函数参数的
顺序。
void func(int num,const string& str)
{
	cout << "func(" << num << "," << str << ")" << endl;
}
下面的代码演示了如何通过bind()将func()函数的第二个参数绑定至一个固定值myString。结果保存在f1()
中。使用auto关键字，就无须指定精确的返回类型。没有绑定至指定值的参数应该标记为_1,_2和_3等。这
些都定义在std::placeholders名称空间中。在f1()的定义中，_1指定了调用func()时，f1()的第一个参数
应该出现的位置。
string myString = "abc";
auto f1 = bind(func,placeholders::_1,myString);
f1(16);
输出如下：
func(16,abc)
bind()还可以用于重新排列参数的顺序，如下列代码所示。_2指定了func()调用时，f2()的第二个参数应该
出现的位置。换句话说，f2()绑定的意义是：f2()的第一个参数将成为函数func()的第二个参数，f2()的第二
个参数成为函数func()的第一个参数。
auto f2 = bind(func,placeholders::_2,placeholders::_1);
f2("Test",32);
输出如下：
func(32,Test);
<functional>头文件定义了std::ref()和std::cref()辅助函数，它们分别用于绑定引用或const引用。
例如，假定有如下函数：
void increment(int& value)
{
	++value;
}
如果调用了这个函数，如下所示，index的值就是1:
int index = 0;
increment(index);
如果使用bind()调用它，如下所示，index的值就不递增，因为建立了index的一个副本，并把这个副本的
引用绑定到increment()函数的第一个参数上：
int index = 0;
auto incr = bind(increment,index);
incr();
使用std::ref()就会递增index:
int index = 0;
auto incr = bind(increment,ref(index));
incr();
结合重载函数使用时，绑定参数会出现一个小问题。
假设有下面两个名为overloaded()的重载函数：
void overloaded(int num)
{
}
void overloaded(float f)
{
}
如果要对这些重载的函数使用bind()，那么必须显式地指定绑定这两个重载中的哪一个。
下面的代码无法成功编译：
auto f3 = bind(overloaded,placeholders::_1);									//error
如果需要绑定接受浮点数参数的重载函数的参数，需要使用以下语法：
auto f4 = bind((void(*)(float))overloaded,placeholders::_1);			//ok
警告：在C++11之前有bind2nd()和bind1st()。两者都被C++11废弃了，请改用lambda表达式和bind()。
取反器：
取反器(negator)是类似于绑定器的函数，但是取反器计算谓词结果的反结果。
not1(),not2()
调用成员函数：
假设有一个对象容器，有时需要传递一个指向类方法的指针作为算法的回调。例如，假设要对序列中的每个
string调用empty()方法，找到string vector中的第一个空string。然而，如果将指向string::empty()
的指针传递给find_if()，这个算法无法知道接受的是指向方法的指针，而不是不同函数指针或仿函数。调用
方法指针的代码和调用普通函数指针的代码是不一样的，因为前者必须在对象的上下文内调用。
C++提供了mem_fn()转换函数，在传递给算法之前可以对函数指针调用这个函数。
注意：必须将方法指针指定为&string::empty。
void findEmptyString(const vector<string>& strings)
{
	auto endIter = end(strings);
	auto it = find_if(begin(strings),endIter,mem_fn(&string::empty));
	if(it == endIter)
	{
		cout << "No empty strings!" << endl;
	}
	else
	{
		cout << "Empty string at position: " << static_cast<int>(it - begin(strings)) << endl;
	}
}
如果容器内保存的不是对象本身，而是对象指针，mem_fn()的使用方法也完全一样。
编写自己的函数对象：
如果需要完成不适合用lambda表达式执行的更复杂的任务，那么可以编写自己的函数对象，来执行预定义
仿函数不能执行的更特定的任务。如果需要将函数适配器用于这些仿函数，还必须提供特定的typedef。实现
这一点最简单的方法是：根据是接受一个参数还是两个参数，从unary_function或binary_function派生自己
的函数对象类。这两个类定义在<functional>中，根据它们提供的函数的参数类型和返回类型进行模板化。
class myIsDigit : public unary_function<char,bool>
{
public:
	bool operator()(char c) const
	{
		return ::isdigit(c) != 0;;
	}
};
bool isNumber(const string& str)
{
	auto endIter = end(str);
	auto it = find_if(begin(str),endIter,not1(myIsDigit()));
	return (it == endIter);
}
注意：myIsDigit类中重载的函数调用运算符必须是const，才能将这个类的对象传递给find_if()。
警告：算法可以生成函数对象谓词的多份副本，并对不同的元素调用不同的副本。函数调用运算符必须为const，
因此，编写仿函数时，不能让仿函数依赖在调用之间保持一致的对象的任何内部状态。
在C++11之前，在函数作用域内局部定义的类不能用作模板参数。这个限制已经取消了。
bool isNumber(const string& str)
{
	class myIsDigit : public unary_function<char,bool>
	{
	public:
		bool operator()(char c) const
		{
			return ::isdigit(c) != 0;;
		}
	};
	auto endIter = end(str);
	auto it = find_if(begin(str),endIter,not1(myIsDigit()));
	return (it == endIter);
}
*/