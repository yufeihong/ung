/*
The world object:
The primary control object for a Bullet physics simulation is an instance of
btDynamicsWorld.All of our physical objects will be controlled by the rules
defined by this class. There are several types of btDynamicsWorld that can be used,
depending on how you want to customize your physics simulation, but the one we
will be using is btDiscreteDynamicsWorld.This world moves objects in discrete
steps (hence the name(因此得名)) in space as time advances.
This class doesn't define how to detect collisions, or how objects respond to collisions. It 
only defines how they move in response to(对…做出反应) stepping the simulation through time.

The broad phase:
A physics simulation runs in real time, but it does so in discrete(离散的，不连续的) steps of time.
Each step, there would be some number of objects which may have moved a small
distance based on their motion and how much time has passed. After this movement
has completed, a verification(验证) process checks whether a collision has occurred, and if
so, then it must generate the appropriate response(恰如其分的反应).
Generating an accurate collision response alone can be highly computationally
expensive, but we also have to worry about how much time we spend checking for
collisions in the first place. The brute(粗鲁的) force method is to make sure that no collisions
have been missed by comparing every object against every other object, and finding
any overlaps in space, and doing this every step.
This would be all well and good for simple simulations with few objects, but not
when we potentially have hundreds of objects moving simultaneously(同时), such as in a
videogame. If we brute force our way through the collision checks, then we need to
check all the N objects against the other N-1 objects. In Big O notation this is an  O(N^2)
situation. This design scales badly for increasing values of N,generating an enormous(巨大的)
performance bottleneck as the CPU buckles under the strain of having so much work
to do every step. For example, if we have 100 objects in our world, then we have
100*99 = 9,900 pairs of objects to check!
But, imagine if only two of those 100 objects are even remotely close together and the
rest are spread too far apart to matter; why would we waste the time doing precise
collision checks on the other 9,899 pairs? This is the purpose of "broad phase collision
detection". It is the process of quickly culling away object pairs, which have little or
no chance of collision in the current step, and then creating a shortlist of those that
could collide(碰撞).This is an important point because the process merely(只是) provides a rough(粗略的)
estimate(估计), in order to keep the mathematics computationally cheap. It does not miss any
legitimate（合法的） collision pairs, but it will return some that aren't actually colliding.
Once we have shortlisted the potential collisions, we pass them on to another
component of the physics simulation called "narrow phase collision detection",
which checks the shortlist for legitimate collisions using more intense（强烈的，认真的）, but
accurate mathematical techniques.
A btBroadPhaseInterface object is needed to tell our world object what technique
to use for its broad phase collision detection and the built-in type we will be using is
btDbvtBroadphase.

The collision configuration:
This is a relatively simple component on the surface, but under the hood(在底层) it
provides the physics simulation with components that handle essential tasks such
as determining how Bullet manages memory allocation, provides the algorithms for
solving various collisions (box-box, sphere-box, and so on), and how to manage the
data that comes out of the broad phase collision detection called Manifolds(歧管,具有多种形式的东西).
Bullet's default collision configuration object,is btDefaultCollisionConfiguration .

The collision dispatcher:
The collision dispatcher, as the name implies, dispatches collisions into our
application. For a video game, it is practically(实际上) guaranteed（保证） that we will want to
be informed（被告知） of inter-object collision at some point, and this is the purpose of the
collision dispatcher.
One of the built-in collision dispatcher class definitions that come with Bullet is
the basic btCollisionDispatcher.The only requirement is that it must be fed
with the collision configuration object in its constructor (which forces us to create
this object second).

The constraint solver(约束求解器):
The constraint solver's job is to make our objects respond to(对...做出反应) specific constraints.

Creating the Bullet components:
btBroadphaseInterface* m_pBroadphase;
btCollisionConfiguration* m_pCollisionConfiguration;
btCollisionDispatcher* m_pDispatcher;
btConstraintSolver* m_pSolver;
btDynamicsWorld* m_pWorld;

//Create the collision configuration
m_pCollisionConfiguration = new btDefaultCollisionConfiguration();
//Create the dispatcher
m_pDispatcher = new btCollisionDispatcher(m_pCollisionConfiguration);
//Create the broadphase
m_pBroadphase = new btDbvtBroadphase();
//Create the constraint solver
m_pSolver = new btSequentialImpulseConstraintSolver();
//Create the world
m_pWorld = new btDiscreteDynamicsWorld(m_pDispatcher, m_pBroadphase, m_pSolver, m_pCollisionConfiguration);

Bullet maintains the same modular design of its core components even down to
individual physics objects. This allows us to customize physics objects through
their components by interchanging or replacing them at will.

Three components are necessary to build a physics object in Bullet:
A collision shape, defining the object's volume and how it should respond to the collisions with other collision shapes.
A motion state, which keeps track of the motion of the object.
A collision object, which acts as a master controller of the object,managing the previously mentioned components and the physical properties of the object.
*/