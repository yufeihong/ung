/*
什么是C风格字符串?以空字符结束的字符数组.字符串字面值是C风格字符串.

混用string对象和C风格字符串 :
允许使用字符串字面值来初始化string对象:
string s("Hello World");			//s的内容是Hello World
更一般的情况是,任何出现字符串字面值的地方都可以用以空字符结束的字符数组来替代:
1:允许使用以空字符结束的字符数组来初始化string对象或为string对象赋值.
2:在string对象的加法运算中允许使用以空字符结束的字符数组作为其中一个运算对象(不能两个运算对象都是);在string对象的复合赋值运算中允许
使用以空字符结束的字符数组作为右侧的运算对象.
上述性质反过来就不成立了:如果程序的某处需要一个C风格字符串,无法直接用string对象来代替它.例如,不能用string对象直接初始化指向字符的
指针.为了完成该功能,string专门提供了一个名为c_str的成员函数:
char *str = s;						//错误,不能用string对象初始化char*
const char *str = s.c_str();	//正确
顾名思义,c_str()函数的返回值是一个C风格的字符串.也就是说,函数的返回结果是一个指针,该指针指向一个以空字符结束的字符数组,而这个数
组所存的数据恰好与那个string对象的一样.结果指针的类型是const char*,从而确保我们不会改变字符数组的内容.
如果执行完c_str()函数后程序想一直都能使用其返回的数组,最好将该数组重新拷贝一份.
注意函数c_str()的返回类型是const char*.
*/

/*
现在一个指向string的指针ps,不论string是否为空,sizeof(ps)都是4个字节.

size()函数返回string对象的长度(即string对象中字符的个数,注意:如果是通过一个const char*字符串初始化的string,那么size函数的返回值不会计算字符
数组最后的空字符).

string类及其他大多数标准库类型都定义了几种配套的类型.
这些配套类型体现了标准库类型与机器无关的特性,类型size_type就是其中的一种.
在具体使用的时候,通过作用域操作符来表明名字size_type是在类string中定义的.

string::size_type是一个无符号类型的值,而且能足够存放下任何string对象的大小.所有用于存放string类的size函数返回值的变量,都应该是
string::size_type类型的.
由于size函数返回的是一个无符号整型数,因此切记,如果在表达式中混用了带符号数和无符号数将可能产生意想不到的结果.例如,
假设n是一个具有负值的int,则表达式s.size() < n的判断结果几乎肯定是true.这是因为负值n会自动地转换成一个比较大的无符号值.

如果一条表达式中已经有了size()函数,就不要再使用int了,这样可以避免混用int和unsigned可能带来的问题.

size()和length()都返回string的字符个数,其行为完全一样.

注意:size()的结果和string实际占用的内存量是两回事.
*/

/*
使用老式C语言时,只能使用普通的以null结尾的字符数组来表示字符串.遗憾的是,这种表示方式会导致很多问题,例如会导致安全攻击的缓冲区溢出.C++STL
包含了一个安全易用的string类,这个类没有这些缺点.

C风格的字符串:
在C语言中,字符串表示为字符的数组.字符串中的最后一个字符是空字符('\0'),这样,操作字符串的代码就知道字符串在哪里结束.官方将这个空字符定义为
NUL,这个拼写中只有一个L,而不是两个L.NUL和nullptr指针是两回事.
目前,程序员使用C字符串时最常犯的错误是忘记为'\0'字符分配空间.例如,字符串"hello"看上去是5个字符长,但在内存中需要6个字符的空间才能保存这个字
符串的值.
C++包含一些来自C语言的字符串操作函数,它们在<cstring>头文件中定义.通常,这些函数不直接操作内存分配.例如,strcpy()函数有两个字符串参数,这个
函数将第二个字符串复制到第一个字符串,而不考虑第二个字符串是否能恰当地填入第一个字符串.
C和C++中的sizeof()运算符可用于获得给定数据类型或变量的大小.例如,sizeof(char)返回1,因为char的大小是1个字节.但是,在C风格的字符串中,
sizeof()和strlen()是不同的.绝对不要通过sizeof()获得字符串的大小.如果C风格的字符串存储为char[],则sizeof()返回字符串使用的实际内存,包括'\0'字
符.例如:
char text1[] = "abcdef";
size_t s1 = sizeof(text1);		//is 7
size_t s2 = strlen(text1);		//is 6,空字符不计算在内
但是,如果C风格的字符串存储为char*,sizeof()就返回指针的大小.例如:
const char* text2 = "abcdef";
size_t s3 = sizeof(text2)		//is 4(platform-dependent)
size_t s4 = strlen(text2)		//is 6,空字符不计算在内
在32位模式编译时,s3的值为4,而在64位编译时,s3的值为8,因为这返回的是指针const char*的大小.
警告:在VS中使用C风格的字符串函数时,编译器可能会给出安全相关的警告甚或错误,说明这些函数已经被废弃了.使用其他C标准库函数可以避免这些警告,例
如strcpy_s()和strcat_s(),这些函数是"安全C库".

字符串字面量:
下面的代码输出字符串hello,这段代码包含这个字符串本身,而不是一个包含这个字符串的变量:
cout << "hello" << endl;
在上面的代码中,"hello"是一个字符串字面量(string literal),因为这个字符串以值的形式写出,而不是一个变量.与字符串字面量关联的真正内存在内存的只读
部分中.通过这种方式,编译器可以重用等价字符串字面量的引用,来优化内存的使用.也就是说,即使一个程序使用了500次"hello"字符串字面量,编译器也
只在内存中创建一个hello实例.这种技术称为字面量池(literal pooling).
字符串字面量可以赋值给变量,但因为字符串字面量位于内存的只读部分,且使用了字面量池,所以这样做会产生风险.C++标准正式指出:字符串字面量的类型
为"n个const char的数组",然而为了向后兼容较老的不支持const的代码,大部分编译器不会强制程序将字符串字面量赋值给const char*类型的变量.这些编译
器允许将字符串赋值给不带有const的char*,而且整个程序可以正常运行,除非试图修改字符串.
char* a = "hello";

*a = "hi";			//编译错误
a[0] = 'm';		//抛出异常

cout << typeid("hello").name() << endl;			//char const [6]
cout << typeid(*a).name() << endl;				//char
cout << typeid(a).name() << endl;					//char *

cout << a << endl;				//hello
cout << *a << endl;				//h

int* pi = new int;
cout << pi << endl;				//00241E88(内存地址)
一种更安全的编码方法是在引用字符串常量时,使用指向const字符的指针,这样编译器就会捕捉到任何写入只读内存的企图.
还可以将字符串字面量用作字符数组(char[])的初始值.这种情况下,编译器会创建一个足以放下这个字符串的数组,然后将字符串复制到这个数组.因此,编译器
不会将字面量放在只读的内存中,也不会进行字面量的池操作.
char arr[] = "hello";				//Compiler takes care of creating appropriate sized character array arr.
arr[0] = 'm'm;						//The contents can be modified.

C++string类:
在C++中,string是一个类(实际上是basic_string类模板的一个实例),这个类支持<cstring>中提供的许多功能,还能自动管理内存分配.
当string操作需要扩展string时,string类能够自动处理内存需求,因此不再会出现内存溢出的情况了.
为了达到兼容的目的,还可以应用string的c_str()方法获得一个表示C风格字符串的const字符指针.不过,一旦string执行了任何内存重分配或string对象被销毁
了,这个返回的const指针就失效了.应该在使用结果之前调用这个方法,以便它准确反映string当前的内容.永远不要从函数中返回在基于堆栈的string上调用
c_str()的结果.
string s("hello");
const char* ps = s.c_str();

cout << ps << endl;					//hello
cout << *ps << endl;				//h
cout << sizeof *ps << endl;		//1
源代码中的字符串字面量通常解释为const char*.使用用户定义的标准字面量"s"可以把字符串字面量解释为string.例如:
auto string1 = "hello world";	//string1 will be a const char*
auto string2 = "hello world"s;	//string2 will be an string

cout << string2 << endl;//hello world

cout << typeid("hello world").name() << endl;		//char const [12]
cout << typeid(string1).name() << endl;					//char const *
cout << typeid(string2).name() << endl;					//class std::basic_string<char,struct std::char_traits<char>,class std::allocator<char>>
std名称空间包含很多辅助函数,以便完成数值和字符串之间的转换.
下面的函数可以用于将数值转换为字符串:
string to_string(int val);
string to_string(unsigned val);

string to_string(long val);
string to_string(unsigned long val);
string to_string(long long val);
string to_string(unsigned long long val);

string to_string(float val);
string to_string(real_type val);
string to_string(long real_type val);
下面的函数可以用于将string转换为数值.
在这些函数原型中,str表示要转换的string,idx是一个指针,这个指针将接收第一个未转换的字符的索引,base表示转换过程中使用的进制.idx指针可以是空
指针,如果是空指针则被忽略.如果不能执行任何转换,这些函数会抛出invalid_argument异常,如果转换的值超出了返回类型的返回,则抛出out_of_range
异常.
int stoi(const string& str,size_t* idx = 0,int base = 10);

long stol(const string& str,size_t* idx = 0,int base = 10);
unsigned long stoul(const string& str,size_t* idx = 0,int base = 10);
long long stoll(const string& str,size_t* idx = 0,int base = 10);
unsigned long long stoull(const string& str,size_t* idx = 0,int base = 10);

float stof(const string& str,size_t* idx = 0);
real_type stod(const string& str,size_t* idx = 0);
long real_type stold(const string& str,size_t* idx = 0);

原始字符串字面量:
原始字符串字面量(raw string literal)是可以横跨多行代码的字符串字面量,不需要转义嵌入的双引号,像\t和\n这种转义序列不按照转义序列的处理方式,而是
按照普通文本的方式处理.
string str1 = "hello "world"!";					//错误
string str2 = "hello \"world\"!";
cout << str2 << endl;								//hello "world"!
对于原始字符串字面量,就不需要转义引号了.原始字符串字面量以R"(开头,以)"结尾.
string str3 = R"(hello "world"!)";
cout << str3 << endl;								//hello "world"!
原始字符串字面量可以跨越多行代码.普通的字符串字面量不能跨越多行.
string str4 = "hello
\tworld";//错误
string str5 = R"(hello
\tworld)";
cout << str5 << endl;								//输出如下:
hello
\tworld
这也说明,使用原始字符串字面量时,\t转义字符没有替换为实际的制表符字符,而是按照字面形式保存.
因为原始字符串字面量以"(开头,)"结尾,所以使用这个语法时,不能在字符串中嵌入"(或)".
string str6 = R"(hello)" world)";				//错误
如果需要嵌入,则需要使用扩展的原始字符串字面量语法:
R"d-char-sequence(实际的原始字符串)d-char-sequence"
d-char-sequence是可选的分隔符序列,原始字符串首尾的分隔符序列应该一致.分隔符序列最多能有16个字符.
string str7 = R"-(hello)" world)-";
cout << str7 << endl;								//hello)" world
string str8 = R"-("(hello)" world)-";
cout << str8 << endl;								//"(hello)" world
string str9 = R"-((hello) world)-";
cout << str9 << endl;								//(hello) world
*/