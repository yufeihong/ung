/*
关联容器支持高效的关键字查找和访问.两个主要的关联容器(associative-container)类型是std::map和std::set.std::map中的元素是一些关键字-值
(key-value)对:关键字起到索引的作用,值则表示与索引相关联的数据.

关联容器的迭代器都是双向的.

标准库提供8个关联容器.这8个容器间的不同体现在三个维度上:
1:每个容器或者是一个std::set,或者是一个std::map.
2:或者要求不重复的关键字,或者允许重复关键字.
3:按顺序保存元素,或无序保存.

std::map类型通常被称为关联数组.关联数组与"正常"数组类似,不同之处在于其下标不必是整数.我们通过一个关键字而不是位置来查找值.

一个经典的使用关联数组的例子是单词计数程序:
//统计每个单词在输入中出现的次数
std::map<string,size_t> word_count;		//string到size_t的空std::map
string word;
while(cin >> word)
	++word_count[word];						//提取word的计数器并将其加1
for(const auto &w : word_count)				//对std::map中的每个元素
	//打印结果
	std::cout << w.first << " occurs " << w.second
			<< ((w.second > 1) ? " times" " time") << std::endl;
此程序读取输入,报告每个单词出现多少次.

类似顺序容器,关联容器也是模板.为了定义一个std::map,我们必须指定关键字和值的类型.
当对word_count进行下标操作时,我们使用一个string作为下标,获得与此string相关联的size_t类型的计数器.
while循环每次从标准输入读取一个单词.它使用每个单词对word_count进行下标操作.如果word还未在std::map中,下标运算符会创建一个新元素,
其关键字为word,值为0.不管元素是否是新创建的,我们将其值加1.

当从std::map中提取一个元素时,会得到一个std::pair类型的对象,简单来说,std::pair是一个模板类型,保存两个名为first和second的(公有)数据成员.

每个关联容器都定义了一个默认构造函数,它创建一个指定类型的空容器.我们也可以将关联容器初始化为另一个同类型容器的拷贝,或是从一个值
范围来初始化关联容器,只要这些值可以转化为容器所需类型就可以.
在新标准下,我们也可以对关联容器进行值初始化.

关联容器对其关键字类型有一些限制.
对于有序容器---std::map、multimap、std::set以及multiset,关键字类型必须定义元素比较的方法.默认情况下,标准库使用关键字类型的<运算符来比
较两个关键字.
在集合类型中,关键字类型就是元素类型.在映射类型中,关键字类型是元素的第一部分的类型.

标准库类型std::pair,它定义在头文件utility中.
一个std::pair保存两个数据成员.
std::pair是一个用来生成特定类型的模板.当创建一个std::pair时,我们必须提供两个类型名,std::pair的数据成员将具有对应的类型.
std::pair<string,string> anon;						//保存两个string
std::pair<string,size_t> word_count;				//保存一个string和一个size_t
std::pair<string,std::vector<int>> line;		//保存string和std::vector<int>
std::pair的默认构造函数对数据成员进行值初始化.因此,anon是一个包含两个空string的std::pair,line保存一个空string和一个空std::vector.
word_count中的size_t成员值为0,而string成员被初始化为空.

我们也可以为每个成员提供初始化器:
std::pair<string,string> author{"James","Joyce"};
这条语句创建一个名为author的std::pair,两个成员被初始化为"James"和"Joyce".

与其他标准库类型不同,std::pair的数据成员是public的.两个成员分别命名为first和second.

std::pair上的操作:
std::pair<T1,T2> p:
p是一个std::pair,两个类型分别为T1和T2的成员都进行了值初始化.

std::pair<T1,T2> p(v1,v2):
p是一个成员类型为T1和T2的std::pair,first和second成员分别用v1和v2进行初始化.

std::pair<T1,T2> p = {v1,v2}:
等价于p(v1,v2).

std::make_pair(v1,v2):
返回一个用v1和v2初始化的std::pair.std::pair的类型从v1和v2的类型推断出来.

p.first:
返回p的名为first的(公有)数据成员.

p.second:
返回p的名为second的(公有)数据成员.

p1 relop p2:
关系运算符(<,>,<=,>=)按字典序定义:例如,当p1.first < p2.first或!(p2.first < p1.first) && p1.second < p2.second成立时,
p1 < p2为true.关系运算符利用元素的<运算符来实现.

p1 == p2,p1 != p2:
当first和second成员分别相等时,两个std::pair相等.相等性判断利用元素的==运算符实现.

创建std::pair对象的函数:
C++11:
std::pair<string,int> process(std::vector<string> &v)
{
	//处理v
	if(!v.empty())
		return {v.back(),v.back().size()}	//列表初始化
	else
		return std::pair<string,int>();		//隐式构造返回值
}
若v不为空,我们返回一个由v中最后一个string及其大小组成的std::pair.否则,隐式构造一个空std::pair,并返回它.
在较早的C++版本中,不允许用花括号包围的初始化器来返回std::pair这种类型的对象,必须显式构造返回值:
if(!v.empty())
	return std::pair<string,int>(v.back(),v.back().size());
我们还可以用std::make_pair来生成std::pair对象,std::pair的两个类型来自于std::make_pair的参数:
if(!v.empty())
	return std::make_pair(v.back(),v.back().size());

关联容器额外的类型别名:
key_type:此容器类型的关键字类型.
mapped_type:每个关键字关联的类型.只适用于std::map.
value_type:对于std::set,与key_type相同.对于std::map,为std::pair<const key_type,mapped_type>,这里的const是因为我们不能改变一个元素的关
键字,因此这些std::pair的关键字部分是const的.

std::set<string>::value_type v1;
v1是一个string.

std::set<string>::key_type v2;
v2是一个string.

std::map<string,int>::value_type v3;
v3是一个std::pair<const string,int>.

std::map<string,int>::key_type v4;
v4是一个string.

std::map<string,int>::mapped_type v5;
v5是一个int.

只有std::map类型(unordered_map,unoredered_multimap,multimap和std::map)才定义了mapped_type.

关联容器迭代器:
当解引用一个关联容器迭代器时,我们会得到一个类型为容器的value_type的值的引用.对std::map而言value_type是一个std::pair类型,其first成员保
存const的关键字,second成员保存值.
std::set的迭代器是const的.虽然std::set类型同时定义了iterator和const_iterator类型,但两种类型都只允许只读访问std::set中的元素.

遍历关联容器:
std::map和std::set类型都支持begin和end操作.我们可以用这些操作来获取迭代器,然后用迭代器来遍历容器.
当使用一个迭代器遍历一个std::map、multimap、std::set或multiset时,迭代器按关键字升序遍历元素.

关联容器和算法:
我们通常不对关联容器使用泛型算法.
关键字是const这一特性意味着不能将关联容器传递给修改或重排容器元素的算法,因为这类算法需要向元素写入值,而std::set类型中的元素是const的,
std::map中的元素是std::pair,其第一个成员是const的.
关联容器可用于只读取元素的算法.但是,很多这类算法都要搜索序列.由于关联容器中的元素能通过它们的关键字进行(快速)查找,因此对其使用泛
型搜索算法几乎总是个坏主意.(这里中文版翻译的时候第383页写错了,原文为Because elements in an associative container can be found
(quickly) by their key, it is almost always a bad idea to use a generic search algorithm.)例如,关联容器定义了一个名为find的成员,它通
过一个给定的关键字直接获取元素.我们可以用泛型find算法来查找一个元素,但此算法会进行顺序搜索.使用关联容器定义的专用的find成员会比调
用泛型find快得多.

在实际编程中,如果我们真要对一个关联容器使用算法,要么是将它当作一个源序列,要么当作一个目的位置.例如,可以用泛型copy算法将元素从
一个关联容器拷贝到另一个序列.类似的,可以调用inserter将一个插入器绑定到一个关联容器.通过使用inserter,我们可以将关联容器当作一个
目的位置来调用另一个算法.

向关联容器中添加元素:
关联容器的insert成员向容器中添加一个元素或一个元素范围.由于std::map和std::set(以及对应的无序类型)包含不重复的关键字,因此插入一个已存在的元
素对容器没有任何影响:
std::vector<int> ivec = {2,4,6,8,2,4,6,8};		//ivec has eight elements
std::set<int> set2;											//empty std::set
set2.insert(ivec.cbegin(), ivec.cend());				//set2 has four elements
set2.insert({1,3,5,7,1,3,5,7});							//set2 now has eight elements
insert有两个版本,分别接受一对迭代器,或是一个初始化器列表.

向std::map添加元素:

对一个std::map进行insert操作时,必须记住元素类型是std::pair.通常,对于想要插入的数据,并没有一个现成的std::pair对象.可以在insert的参数列表中创建
一个std::pair:
//向word_count插入word的4中方法
word_count.insert({word, 1});
word_count.insert(std::make_pair(word, 1));
word_count.insert(std::pair<string, size_t>(word, 1));
word_count.insert(std::map<string, size_t>::value_type(word, 1));
C++11:
如我们所见,在新标准下,创建一个std::pair最简单的方法是在参数列表中使用花括号初始化.也可以调用std::make_pair或显式构造std::pair.最后一个insert调
用中的参数:std::map<string, size_t>::value_type(word, 1)构造一个恰当的std::pair类型,并构造该类型的一个新对象,插入到std::map中.

关联容器insert操作:
c.insert(v),c.emplace(args):
v是value_type类型的对象,args用来构造一个元素.对于std::map和std::set,只有当元素的关键字不在c中时才插入(或构造)元素.函数返回一个std::pair,包
含一个迭代器,指向具有指定关键字的元素,以及一个指示插入是否成功的bool值.对于multimap和multiset,总会插入(或构造)给定元素,并返回
一个指向新元素的迭代器.
c.insert(b,e),c.insert(il):
b和e是迭代器,表示一个c::value_type类型值的范围.il是这种值的花括号列表.函数返回void.对于std::map和std::set,只插入关键字不在c中的元素.对
于multimap和multiset,则会插入范围中的每个元素.
c.insert(p,v),c.emplace(p,args):
类似insert(v)(或emplace(args)),但将迭代器p作为一个提示,指出从哪里开始搜索新元素应该存储的位置.返回一个迭代器,指向具有给定关键字
的元素.

检测insert的返回值:
insert(或emplace)返回的值依赖于容器类型和参数.对于不包含重复关键字的容器,添加单一元素的insert和emplace版本返回一个std::pair,告诉我们
插入操作是否成功.std::pair的first成员是一个迭代器,指向具有给定关键字的元素.second成员是一个bool值,指出元素是插入成功还是已经存在于容
器中.如果关键字已在容器中,则insert什么事情也不做,且返回值中的bool部分为false.如果关键字不存在,元素被插入容器中,且bool值为true.

std::map<string, size_t> word_count;		//empty std::map from string to size_t
string word;
while (cin >> word)
{
	//inserts an element with key equal to word and value 1;
	//if word is already in word_count, insert does nothing
	auto ret = word_count.insert({word, 1});
	if (!ret.second)						//word was already in word_count
		++ret.first->second;		//increment the counter
}

//++((ret.first) ->second);		//equivalent expression
ret保存insert返回的值,是一个std::pair.
ret.first是std::pair的第一个成员,是一个std::map迭代器,指向具有给定关键字的元素.
ret.first->解引用此迭代器,提取std::map中的元素,元素也是一个std::pair.
ret.first->second是std::map中元素的值部分.
++ret.first->second递增此值.
如果使用的是旧版本的编译器,ret的声明和初始化可能复杂些:
std::pair<std::map<string,size_t>::iterator,bool> ret = word_count.insert(std::make_pair(word,1));

删除关联容器中的元素:
关联容器定义了三个版本的erase.
与顺序容器一样,我们可以通过传递给erase一个迭代器或一个迭代器对来删除一个元素或者一个元素范围.函数返回void.
关联容器提供一个额外的erase操作,它接受一个key_type参数.此版本删除所有匹配给定关键字的元素(如果存在的话),返回实际删除的元素的数
量.对于保存不重复关键字的容器,erase的返回值总是0或1.
c.erase(k):
从c中删除每个关键字为k的元素.返回一个size_type值,指出删除的元素的数量.
c.erase(p):
从c中删除迭代器p指定的元素.p必须指向c中一个真实元素,不能等于c.end().返回一个指向p之后元素的迭代器,若p指向c中的尾元素,则返
回c.end().
c.erase(b,e):
删除迭代器对b和e所表示的范围中的元素.返回e.

std::map的下标操作:
std::map和unordered_map容器提供了下标运算符和一个对应的at函数.std::set类型不支持下标,因为std::set中没有与关键字相关联的"值".元素本身就是关键字.
我们不能对一个multimap或一个unordered_multimap进行下标操作,因为这些容器中可能有多个值与一个关键字相关联.
与其他下标运算符不同的是,如果关键字并不在std::map中,会为它创建一个元素并插入到std::map中,关联值将进行值初始化.
例如,如果我们编写如下代码:
std::map<string,size_t> word_count;	//empty std::map
//插入一个关键字为Anna的元素,关联值进行值初始化,然后将1赋予它
word_count["Anna"] = 1;
将会执行如下操作:
1:在word_count中搜索关键字为Anna的元素,未找到.
2:将一个新的关键字-值对插入到word_count中.关键字是一个const string,保存Anna.值进行值初始化,在本例中意味着值为0.
3:提取出新插入的元素,并将值1赋予它.

由于下标运算符可能插入一个新元素,我们只可以对非const的std::map使用下标操作.

std::map的下标运算符与我们用过的其他下标运算符的另一个不同之处是其返回类型.通常情况下,解引用一个迭代器所返回的类型与下标运算符返回的类型是
一样的.但对std::map则不然,当对一个std::map进行下标操作时,会获得一个mapped_type对象,但当解引用一个std::map迭代器时,会得到一个value_type对象.

与其他下标运算符相同的是,std::map的下标运算符返回一个左值.由于返回的是一个左值,所以我们既可以读也可以写元素.
std::cout << word_count["Anna"];		//fetch the element indexed by Anna; prints 1
++word_count["Anna"];						//fetch the element and add 1 to it
std::cout << word_count["Anna"];		//fetch the element and print it; prints 2

访问元素:
关联容器提供多种查找一个指定元素的方法.应该使用哪个操作依赖于我们要解决什么问题.如果我们所关心的只不过是一个特定元素是否已在容器中,可能
find是最佳选择.对于不允许重复关键字的容器,可能使用find还是count没什么区别.但对于允许重复关键字的容器,count还会做更多的工作:如果元素
在容器中,它还会统计有多少个元素具有相同的关键字.如果不需要计数,最好使用find.

lower_bound和upper_bound不适用于无序容器
下标和at操作只是用于非const的std::map和unordered_map.
c.find(k):
返回一个迭代器,指向第一个关键字为k的元素,若k不在容器中,则返回尾后迭代器.
c.count(k):
返回关键字等于k的元素的数量.对于不允许重复关键字的容器,返回值永远是0或1.
c.lower_bound(k):
返回一个迭代器,指向第一个关键字不小于k的元素.
c.upper_bound(k):
返回一个迭代器,指向第一个关键字大于k的元素.
c.equal_range(k):
返回一个迭代器std::pair,表示关键字等于k的元素的范围.若k不存在,std::pair的两个成员均等于c.end().

对std::map使用find代替下标操作:
对std::map和unordered_map类型,下标运算符提供了最简单的提取元素的方法.但是,使用下标操作有一个严重的副作用:如果关键在还未在std::map中,下标
操作会插入一个具有给定关键字的元素.这种行为是否正确完全依赖于我们的预期是什么.
*/