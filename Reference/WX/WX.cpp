/*
wxApp Overview
A wxWidgets application does not have a main procedure; the equivalent is the wxApp::OnInit member defined for a class derived from wxApp.
OnInit will usually create a top window as a bare minimum（最低限度）. Unlike in earlier versions of wxWidgets, OnInit does not return a frame.
Instead it returns a boolean value which indicates whether processing should continue(true) or not(false).
Note that the program's command line arguments, represented by argc and argv, are available from within wxApp member functions.
An application closes by destroying all windows. Because all frames must be destroyed for the application to exit, it is advisable to use parent frames 
wherever possible when creating new frames, so that deleting the top level frame will automatically delete child frames. The alternative is to explicitly 
delete child frames in the top-level frame's wxCloseEvent handler.
In emergencies（在紧急情况下） the wxExit function can be called to kill the application however normally the application shuts down automatically, see 
Application Shutdown.

Application Shutdown
The application normally shuts down when the last of its top level windows is closed. This is normally the expected behaviour and means that it is 
enough to call wxWindow::Close() in response to the "Exit" menu command if your program has a single top level window. If this behaviour is not 
desirable（可取的，想要的） wxApp::SetExitOnFrameDelete can be called to change it.
Note that such logic doesn't apply for（不适用于） the windows shown before the program enters the main loop（程序进入主循环前）: in other words, you 
can safely show a dialog from wxApp::OnInit and not be afraid that（不用害怕） your application terminates when this dialog – which is the last top 
level window for the moment – is closed.
Another aspect of（另一方面） the application shutdown is wxApp::OnExit which is called when the application exits but before wxWidgets cleans up its 
internal structures. You should delete all wxWidgets object that you created by the time OnExit finishes.

In particular（特别是）, do not destroy them from application class' destructor! For example, this code may crash:
class MyApp : public wxApp
{
public:
	wxCHMHelpController m_helpCtrl;
};
The reason for that is that m_helpCtrl is a member object and is thus（因此是） destroyed from MyApp destructor. But MyApp object is deleted after 
wxWidgets structures that wxCHMHelpController depends on were uninitialized! The solution is to destroy HelpCtrl in OnExit:
class MyApp : public wxApp
{
public:
	wxCHMHelpController *m_helpCtrl;
};
bool MyApp::OnInit()
{
	m_helpCtrl = new wxCHMHelpController;
}
int MyApp::OnExit()
{
	delete m_helpCtrl;
	return 0;
}
*/

/*
wxDocManager:
The wxDocManager class is part of the document/view framework supported by wxWidgets, and cooperates with the wxView, wxDocument and 
wxDocTemplate classes. 

virtual void wxDocManager::ActivateView( wxView *  doc,bool  activate = true)

Sets the current view.

virtual void wxDocManager::AddFileToHistory( const wxString &  filename)

Adds a file to the file history list, if we have a pointer to an appropriate file menu.

bool wxDocManager::CloseDocument( wxDocument *  doc,bool  force = false)

Closes the specified document.

If force is true, the document is closed even if it has unsaved changes.
Parameters
doc The document to close, must be non-NULL.
force If true, close the document even if wxDocument::Close() returns false.
Returnstrue if the document was closed or false if closing it was cancelled by user(only in force = false case).

bool wxDocManager::CloseDocuments( bool  force = true)

Closes all currently opened documents.

wxDocument* wxDocManager::CreateNewDocument()

Creates an empty new document.

This is equivalent to calling CreateDocument() with wxDOC_NEW flags and without the file name.

virtual wxPreviewFrame* wxDocManager::CreatePreviewFrame( wxPrintPreviewBase *  preview,
wxWindow *  parent,const wxString &  title)

Create the frame used for print preview(预览).

This method can be overridden if you need to change the behaviour or appearance of the preview window. By default, a standard wxPreviewFrame is created.
Since2.9.1Parameters
preview The associated preview object.
parent The parent window for the frame.
title The suggested title for the print preview frame.
ReturnsA new print preview frame, must not return NULL.

void wxDocManager::DisassociateTemplate( wxDocTemplate *  temp)

Removes the template from the list of templates.

virtual void wxDocManager::FileHistoryAddFilesToMenu()

Appends the files in the history list to all menus managed by the file history object.

virtual void wxDocManager::FileHistoryAddFilesToMenu( wxMenu *  menu)

Appends the files in the history list to the given menu only.

virtual void wxDocManager::FileHistoryLoad( const wxConfigBase &  config)

Loads the file history from a config object.

virtual void wxDocManager::FileHistoryRemoveMenu( wxMenu *  menu)

Removes the given menu from the list of menus managed by the file history object.

virtual void wxDocManager::FileHistorySave( wxConfigBase &  resourceFile)

Saves the file history into a config object.

This must be called explicitly by the application.

wxDocument* wxDocManager::FindDocumentByPath( const wxString &  path) const

Search for the document corresponding to the given file.
Parameters
path Document file path.
ReturnsPointer to a wxDocument, or NULL if none found.Since2.9.5

wxDocTemplate* wxDocManager::FindTemplate( const wxClassInfo *  classinfo)

Search for a particular document template.

Example:
//creating a document instance of the specified document type:m_doc =(MyDoc*)docManager->FindTemplate(CLASSINFO(MyDoc))->             CreateDocument(wxEmptyString, wxDOC_SILENT);Parameters
classinfo Class info of a document class for which a wxDocTemplate had been previously created.
ReturnsPointer to a wxDocTemplate, or NULL if none found.Since2.9.2

virtual wxDocTemplate* wxDocManager::FindTemplateForPath( const wxString &  path)

Given a path, try to find template that matches the extension.

This is only an approximate method of finding a template for creating a document.

wxView* wxDocManager::GetAnyUsableView() const

Returns the view to apply a user command to.

This method tries to find the view that the user wants to interact with. It returns the same view as GetCurrentDocument() if there is any currently active view but falls back to the first view of the first document if there is no active view.
Since2.9.5

wxDocument* wxDocManager::GetCurrentDocument() const

Returns the document associated with the currently active view(if any).

virtual wxView* wxDocManager::GetCurrentView() const

Returns the currently active view.

This method can return NULL if no view is currently active.
See AlsoGetAnyUsableView()

virtual wxFileHistory* wxDocManager::GetFileHistory() const

Returns a pointer to file history.

virtual size_t wxDocManager::GetHistoryFilesCount() const

Returns the number of files currently stored in the file history.

wxString wxDocManager::GetLastDirectory() const

Returns the directory last selected by the user when opening a file.

Initially empty(最初是空的).

int wxDocManager::GetMaxDocsOpen() const

Returns the number of documents that can be open simultaneously.

wxList& wxDocManager::GetTemplates()

Returns a reference to the list of associated templates.

wxDocTemplateVector wxDocManager::GetTemplatesVector() const

Returns a vector of wxDocTemplate pointers.

virtual bool wxDocManager::Initialize()

Initializes data; currently just calls OnCreateFileHistory().

Some data cannot always be initialized in the constructor because the programmer must be given the opportunity to override functionality. If OnCreateFileHistory() was called from the constructor, an overridden virtual OnCreateFileHistory() would not be called due to C++'s 'interesting' constructor semantics. In fact Initialize() is called from the wxDocManager constructor, but this can be vetoed by passing false to the second argument, allowing the derived class's constructor to call Initialize(), possibly calling a different OnCreateFileHistory() from the default.

The bottom line: if you're not deriving from Initialize(), forget it and construct wxDocManager with no arguments.

virtual wxString wxDocManager::MakeNewDocumentName()

Return a string containing a suitable default name for a new document.

By default this is implemented by appending an integer counter to the string unnamed but can be overridden in the derived classes to do something more appropriate.

virtual wxFileHistory* wxDocManager::OnCreateFileHistory()

A hook to allow a derived class to create a different type of file history.

Called from Initialize().

void wxDocManager::OnFileClose( wxCommandEvent &  event)

Closes and deletes the currently active document.

void wxDocManager::OnFileCloseAll( wxCommandEvent &  event)

Closes and deletes all the currently opened documents.

void wxDocManager::OnFileNew( wxCommandEvent &  event)

Creates a document from a list of templates(if more than one template).

void wxDocManager::OnFileOpen( wxCommandEvent &  event)

Creates a new document and reads in the selected file.

void wxDocManager::OnFileRevert(wxCommandEvent &  event)

Reverts(恢复原状) the current document by calling wxDocument::Revert() for the current document.

void wxDocManager::OnFileSave(wxCommandEvent &  event)

Saves the current document by calling wxDocument::Save() for the current document.

void wxDocManager::OnFileSaveAs(wxCommandEvent &  event)

Calls wxDocument::SaveAs() for the current document.

virtual void wxDocManager::OnMRUFileNotExist(unsigned  n,const wxString &  filename)

Called when a file selected from the MRU list doesn't exist any more.

The default behaviour is to remove the file from the MRU(most recently used) files list and the corresponding menu and notify the user about it but this method can be overridden to customize it.

For example, an application may want to just give an error about the missing file filename but not remove it from the file history. Or it could ask the user whether the file should be kept or removed.

Notice that this method is called only if the file selected by user from the MRU files in the menu doesn't exist, but not if opening it failed for any other reason because in the latter case the default behaviour of removing the file from the MRU list is inappropriate. If you still want to do it, you would need to do it by calling RemoveFileFromHistory() explicitly in the part of the file opening code that may fail.
Since2.9.3Parameters
n The index of the file in the MRU list, it can be passed to RemoveFileFromHistory() to remove this file from the list.
filename The full name of the file.

void wxDocManager::RemoveDocument(wxDocument* doc)

Removes the document from the list of documents.

virtual wxDocTemplate* wxDocManager::SelectDocumentPath(wxDocTemplate **  templates,
int  noTemplates,wxString &  path,long  flags,bool  save = false)

Under Windows, pops up a file selector with a list of filters corresponding to document templates.

The wxDocTemplate corresponding to the selected file's extension is returned.

On other platforms, if there is more than one document template a choice list is popped up, followed by a file selector.

This function is used in CreateDocument().

virtual wxDocTemplate* wxDocManager::SelectDocumentType( wxDocTemplate **  templates,
int  noTemplates,bool  sort = false)

Returns a document template by asking the user(if there is more than one template).

This function is used in CreateDocument().
Parameters
templates Pointer to an array of templates from which to choose a desired template.
noTemplates Number of templates being pointed to by the templates pointer.
sort If more than one template is passed into templates, then this parameter indicates whether the list of templates that the user will have to choose from is sorted or not when shown the choice box dialog. Default is false.

virtual wxDocTemplate* wxDocManager::SelectViewType(wxDocTemplate **  templates,
int noTemplates,bool sort = false)

Returns a document template by asking the user(if there is more than one template), displaying a list of valid views.

This function is used in CreateView(). The dialog normally will not appear because the array of templates only contains those relevant to the document in question, and often there will only be one such.
Parameters
templates Pointer to an array of templates from which to choose a desired template.
noTemplates Number of templates being pointed to by the templates pointer.
sort If more than one template is passed into templates, then this parameter indicates whether the list of templates that the user will have to choose from is sorted or not when shown the choice box dialog. Default is false.

void wxDocManager::SetLastDirectory( const wxString &  dir)

Sets the directory to be displayed to the user when opening a file.

Initially this is empty.
*/

/*
wxFrame:
A frame is a window whose size and position can (usually) be changed by the user.
It usually has thick borders and a title bar, and can optionally contain a menu bar, toolbar and status bar. A frame can contain any window that is not
a frame or dialog.
A frame that has a status bar and toolbar, created via the CreateStatusBar() and CreateToolBar() functions, manages these windows and adjusts the
value returned by GetClientSize() to reflect the remaining size available to application windows.
RemarksAn application should normally define an wxCloseEvent handler for the frame to respond to system close events, for example so that related
data and subwindows can be cleaned up.

Default event processing
wxFrame processes the following events:
wxEVT_SIZE: if the frame has exactly one child window, not counting the status and toolbar, this child is resized to take the entire frame client area.
If two or more windows are present, they should be laid out explicitly either by manually handling wxEVT_SIZE or using sizers;
wxEVT_MENU_HIGHLIGHT: the default implementation displays the help string associated with the selected item in the first pane of the status bar, if
there is one.

Events emitted by this class
Event macros for events emitted by this class:
EVT_CLOSE(func):
Process a wxEVT_CLOSE_WINDOW event when the frame is being closed by the user or programmatically (see wxWindow::Close). The user may
generate this event clicking the close button (typically the 'X' on the top-right of the title bar) if it's present (see the wxCLOSE_BOX style). See
wxCloseEvent.

EVT_ICONIZE(func):
Process a wxEVT_ICONIZE event. See wxIconizeEvent.

EVT_MENU_OPEN(func):
A menu is about to be opened. See wxMenuEvent.

EVT_MENU_CLOSE(func):
A menu has been just closed. See wxMenuEvent.

EVT_MENU_HIGHLIGHT(id, func):
The menu item with the specified id has been highlighted: used to show help prompts in the status bar by wxFrame. See wxMenuEvent.

EVT_MENU_HIGHLIGHT_ALL(func):
A menu item has been highlighted, i.e. the currently selected menu item has changed. See wxMenuEvent.

*/

/*
wxDocParentFrame:
The wxDocParentFrame class provides a default top-level frame for applications using the document/view framework.
This class can only be used for SDI (not MDI) parent frames.
It cooperates with the wxView, wxDocument, wxDocManager and wxDocTemplate classes.
Notice that this class processes wxEVT_CLOSE_WINDOW event and tries to close all open views from its handler. If all the views can be closed, i.e.
if none of them contains unsaved changes or the user decides to not save them, the window is destroyed. Don't intercept(拦截) this event in your
code unless you want to replace this logic.
*/

/*
wxGLCanvas:
Detailed Description:
wxGLCanvas is a class for displaying OpenGL graphics.
It is always used in conjunction with wxGLContext as the context can only be made current (i.e. active for the OpenGL commands) when it is
associated to a wxGLCanvas.
More precisely, you first need to create a wxGLCanvas window(注意这里的window，因为wxGLCanvas是从wxWindow继承下来的，而wxView的继承链中没有
wxWindow) and then create an instance of a wxGLContext that is initialized with this wxGLCanvas and then later use either SetCurrent() with the
instance of the wxGLContext or wxGLContext::SetCurrent() with the instance of the wxGLCanvas (which might be not the same as was used for the
creation of the context) to bind the OpenGL state that is represented by the rendering context to the canvas, and then finally call SwapBuffers() to
swap the buffers of the OpenGL canvas and thus show your current output.
To set up the attributes for the canvas (number of bits for the depth buffer, number of bits for the stencil buffer and so on) you pass them in the
constructor using a wxGLAttributes instance. You can still use the way before 3.1.0 (setting up the correct values of the attribList parameter) but it's
discouraged.

Note On those platforms which use a configure script (e.g. Linux and OS X) OpenGL support is automatically enabled if the relative headers and
libraries are found. To switch it on under the other platforms (e.g. Windows), you need to edit the setup.h file and set wxUSE_GLCANVAS to 1 and
then also pass USE_OPENGL=1 to the make utility. You may also need to add opengl32.lib (and glu32.lib for old OpenGL versions) to the list of the
libraries your program is linked with.
*/

/*
wxDocument:
The document class can be used to model an application's file-based data.

It is part of the document/view framework supported by wxWidgets, and cooperates with the wxView, wxDocTemplate and wxDocManager classes.

A normal document is the one created without parent document and is associated with a disk file. Since version 2.9.2 wxWidgets also supports a 
special kind of documents called child documents which are virtual in the sense that they do not correspond to a file but rather to a part of their 
parent document. Because of this, the child documents can't be created directly by user but can only be created by the parent document (usually 
when it's being created itself). They also can't be independently saved. A child document has its own view with the corresponding window. This 
view can be closed by user but, importantly, is also automatically closed when its parent document is closed. Thus, child documents may be 
convenient for creating additional windows which need to be closed when the main document is. The docview sample demonstrates this use of child 
documents by creating a child document containing the information about the parameters of the image opened in the main document.
*/

/*
Document/View Framework:
The document/view framework is found in most application frameworks, because it can dramatically simplify(大大简化) the code required to build 
many kinds of application.
The idea is that you can model your application primarily(主要的) in terms of(以...为单位) documents to store data and provide interface-independent 
operations upon it, and views to visualise(使可见) and manipulate(操纵) the data. Documents know how to do input and output given stream objects,
and views are responsible for taking input from physical windows and performing the manipulation on the document data.
If a document's data changes, all views should be updated to reflect the change. The framework can provide many user-interface elements based on 
this model.
Once you have defined your own classes and the relationships between them, the framework takes care of popping up file selectors, opening and 
closing files, asking the user to save modifications, routing menu commands to appropriate (possibly default) code, even some default print/preview 
functionality and support for command undo/redo.
The framework is highly modular(模块化), allowing overriding and replacement of functionality and objects to achieve more than the default 
behaviour.

These are the overall(总体的) steps involved in creating an application based on the document/view framework:
Define your own document and view classes, overriding a minimal set of member functions e.g. for input/output, drawing and initialization.
Define any subwindows (such as a scrolled window) that are needed for the view(s). You may need to route some events to views or documents, for 
example, "OnPaint" needs to be routed to wxView::OnDraw.
Decide what style of interface you will use: Microsoft's MDI (multiple document child frames surrounded(包围) by an overall frame), SDI (a separate, 
unconstrained(无约束) frame for each document), or single-window (one document open at a time, as in Windows Write).
Use the appropriate wxDocParentFrame and wxDocChildFrame classes. Construct an instance of wxDocParentFrame in your wxApp::OnInit, and a 
wxDocChildFrame (if not single-window) when you initialize a view. Create menus using standard menu ids (such as wxID_OPEN, wxID_PRINT).
Construct a single wxDocManager instance at the beginning of your wxApp::OnInit, and then as many wxDocTemplate instances as necessary to 
define relationships between documents and views. For a simple application, there will be just one wxDocTemplate.

If you wish to implement Undo/Redo, you need to derive your own class(es) from wxCommand and use wxCommandProcessor::Submit instead of 
directly executing code. The framework will take care of calling Undo and Do functions as appropriate, so long as(只要) the wxID_UNDO and 
wxID_REDO menu items are defined in the view menu.

Here are a few examples of the tailoring(剪裁,调整使适应) you can do to go beyond the default framework behaviour:
Override wxDocument::OnCreateCommandProcessor to define a different Do/Undo strategy, or a command history editor.
Override wxView::OnCreatePrintout to create an instance of a derived wxPrintout class, to provide multi-page document facilities.
Override wxDocManager::SelectDocumentPath to provide a different file selector.
Limit the maximum number of open documents and the maximum number of undo commands.

Note that to activate framework functionality, you need to use some or all of the wxWidgets Predefined Command Identifiers in your menus.

wxDocument Overview
The wxDocument class can be used to model an application's file-based data. It is part of the document/view framework supported by wxWidgets, 
and cooperates with the wxView, wxDocTemplate and wxDocManager classes. Using this framework can save a lot of routine user-interface 
programming, since a range of menu commands – such as open, save, save as – are supported automatically.

The programmer just needs to define a minimal set of classes and member functions for the framework to call when necessary. Data, and the means 
to view and edit the data, are explicitly separated out in this model, and the concept of multiple views onto the same data is supported.

Note that the document/view model will suit many but not all styles of application. For example, it would be overkill(矫枉过正) for a simple file 
conversion utility, where there may be no call for views on documents or the ability to open, edit and save files. But probably(可能) the majority(大多数) 
of applications are document-based.

See the example application in samples/docview. To use the abstract wxDocument class, you need to derive a new class and override at least the 
member functions SaveObject and LoadObject. SaveObject and LoadObject will be called by the framework when the document needs to be saved or 
loaded.

Use the macros wxDECLARE_DYNAMIC_CLASS and wxIMPLEMENT_DYNAMIC_CLASS in order to allow the framework to create document objects on 
demand. When you create a wxDocTemplate object on application initialization, you should pass CLASSINFO(YourDocumentClass) to the 
wxDocTemplate constructor so that it knows how to create an instance of this class.

If you do not wish to use the wxWidgets method of creating document objects dynamically, you must override wxDocTemplate::CreateDocument to 
return an instance of the appropriate class.

wxView Overview
The wxView class can be used to model the viewing and editing component of an application's file-based data. It is part of the document/view 
framework supported by wxWidgets, and cooperates with the wxDocument, wxDocTemplate and wxDocManager classes.

See the example application in samples/docview.

To use the abstract wxView class, you need to derive a new class and override at least the member functions OnCreate, OnDraw, OnUpdate and 
OnClose. You will probably want to respond to menu commands from the frame containing the view.

Use the macros wxDECLARE_DYNAMIC_CLASS and wxIMPLEMENT_DYNAMIC_CLASS in order to allow the framework to create view objects on 
demand. When you create a wxDocTemplate object on application initialization, you should pass CLASSINFO(YourViewClass) to the wxDocTemplate 
constructor so that it knows how to create an instance of this class.

If you do not wish to use the wxWidgets method of creating view objects dynamically, you must override wxDocTemplate::CreateView to return an 
instance of the appropriate class.

wxDocTemplate Overview
The wxDocTemplate class is used to model the relationship between a document class and a view class. The application creates a document template 
object for each document/view pair. The list of document templates managed by the wxDocManager instance is used to create documents and views.
Each document template knows what file filters and default extension are appropriate for a document/view combination, and how to create a 
document or view.

For example, you might write a small doodling application that can load and save lists of line segments. If you had two views of the data – graphical,
and a list of the segments – then you would create one document class DoodleDocument, and two view classes (DoodleGraphicView and 
DoodleListView). You would also need two document templates, one for the graphical view and another for the list view. You would pass the same 
document class and default file extension to both document templates, but each would be passed a different view class. When the user clicks on the 
Open menu item, the file selector is displayed with a list of possible file filters – one for each wxDocTemplate. Selecting the filter selects the 
wxDocTemplate, and when a file is selected, that template will be used for creating a document and view.

For the case where an application has one document type and one view type, a single document template is constructed, and dialogs will be 
appropriately simplified.

wxDocTemplate is part of the document/view framework supported by wxWidgets, and cooperates with the wxView, wxDocument and wxDocManager 
classes.

See the example application in samples/docview.

To use the wxDocTemplate class, you do not need to derive a new class. Just pass relevant information to the constructor including 
CLASSINFO(YourDocumentClass) and CLASSINFO(YourViewClass) to allow dynamic instance creation.

If you do not wish to use the wxWidgets method of creating document objects dynamically, you must override wxDocTemplate::CreateDocument and 
wxDocTemplate::CreateView to return instances of the appropriate class.

Note The document template has nothing to do with the C++ template construct.

wxDocManager Overview
The wxDocManager class is part of the document/view framework supported by wxWidgets, and cooperates with the wxView, wxDocument and 
wxDocTemplate classes.

A wxDocManager instance coordinates(协调,调和) documents, views and document templates. It keeps a list of document and template instances, and 
much functionality is routed through this object, such as providing selection and file dialogs. The application can use this class 'as is' or derive a 
class and override some members to extend or change the functionality.

Create an instance of this class near the beginning of your application initialization, before any documents, views or templates are manipulated.

There may be multiple wxDocManager instances in an application. See the example application in samples/docview.

Event Propagation(传播) in Document/View framework
While wxDocument, wxDocManager and wxView are abstract objects, with which the user can't interact(互动) directly, all of them derive from 
wxEvtHandler class and can handle events arising(产生的) in the windows showing the document with which the user does interact. This is 
implemented by adding additional steps to the event handling process described in How Events are Processed, so the full list of the handlers 
searched for an event occurring directly in wxDocChildFrame is:
1. wxDocument opened in this frame.
2. wxView shown in this frame.
3. wxDocManager associated with the parent wxDocParentFrame.
4. wxDocChildFrame itself.
5. wxDocParentFrame, as per the usual event bubbling up to parent rules.
6. wxApp, again as the usual fallback for all events.

This is mostly useful to define handlers for some menu commands directly in wxDocument or wxView and is also used by the framework itself to 
define the handlers for several standard commands, such as wxID_NEW or wxID_SAVE, in wxDocManager itself. Notice that due to the order of the 
event handler search detailed above, the handling of these commands can not be overridden at wxDocParentFrame level but must be done at the 
level of wxDocManager itself.

wxCommand Overview
wxCommand is a base class for modelling an application command, which is an action usually performed by selecting a menu item, pressing a 
toolbar button or any other means provided by the application to change the data or view.

Instead of the application functionality being scattered around switch statements and functions in a way that may be hard to read and maintain, the 
functionality for a command is explicitly represented as an object which can be manipulated by a framework or application.

When a user interface event occurs, the application submits a command to a wxCommandProcessor object to execute and store.

The wxWidgets document/view framework handles Undo and Redo by use of wxCommand and wxCommandProcessor objects. You might find further 
uses for wxCommand, such as implementing a macro facility that stores, loads and replays commands.

An application can derive a new class for every command, or, more likely, use one class parameterized with an integer or string command identifier.

wxCommandProcessor Overview
wxCommandProcessor is a class that maintains a history of wxCommand instances, with undo/redo functionality built-in. Derive a new class from 
this if you want different behaviour.

wxFileHistory Overview
wxFileHistory encapsulates functionality to record the last few files visited, and to allow the user to quickly load these files using the list appended 
to the File menu. Although wxFileHistory is used by wxDocManager, it can be used independently. You may wish to derive from it to allow different 
behaviour, such as popping up a scrolling list of files.

By calling wxFileHistory::UseMenu() you can associate a file menu with the file history. The menu will then be used for appending filenames that are 
added to the history.

Please notice that currently if the history already contained filenames when UseMenu() is called (e.g. when initializing a second MDI child frame), 
the menu is not automatically initialized with the existing filenames in the history and so you need to call wxFileHistory::AddFilesToMenu() after 
UseMenu() explicitly in order to initialize the menu with the existing list of MRU files (otherwise an assertion failure is raised in debug builds).

The filenames are appended using menu identifiers in the range wxID_FILE1 to wxID_FILE9.

In order to respond to a file load command from one of these identifiers, you need to handle them using an event handler, for example:
wxBEGIN_EVENT_TABLE(wxDocParentFrame, wxFrame)
	EVT_MENU(wxID_EXIT, wxDocParentFrame::OnExit)
	EVT_MENU_RANGE(wxID_FILE1, wxID_FILE9, wxDocParentFrame::OnMRUFile)
wxEND_EVENT_TABLE()

void wxDocParentFrame::OnExit(wxCommandEvent& WXUNUSED(event))
{
	Close();
}

void wxDocParentFrame::OnMRUFile(wxCommandEvent& event)
{
	wxString f(m_docManager->GetHistoryFile(event.GetId() - wxID_FILE1));
	if (!f.empty())
		(void)m_docManager-CreateDocument(f, wxDOC_SILENT);
}
 Predefined Command Identifiers

To allow communication between the application's menus and the document/view framework, several command identifiers are predefined for you to 
use in menus.
wxID_OPEN (5000)
wxID_CLOSE (5001)
wxID_NEW (5002)
wxID_SAVE (5003)
wxID_SAVEAS (5004)
wxID_REVERT (5005)
wxID_EXIT (5006)
wxID_UNDO (5007)
wxID_REDO (5008)
wxID_HELP (5009)
wxID_PRINT (5010)
wxID_PRINT_SETUP (5011)
wxID_PREVIEW (5012)
*/

/*
A Quick Guide to Writing Applications:
To set a wxWidgets application going, you will need to derive a wxApp class and override wxApp::OnInit.

An application must have a top-level wxFrame or wxDialog window. Each frame may contain one or more instances of classes such as wxPanel,
wxSplitterWindow or other windows and controls.

A frame can have a wxMenuBar, a wxToolBar, a wxStatusBar, and a wxIcon for when the frame is iconized.

A wxPanel is used to place controls (classes derived from wxControl) which are used for user interaction. Examples of controls are wxButton,
wxCheckBox, wxChoice, wxListBox, wxRadioBox, and wxSlider.

Instances of wxDialog can also be used for controls and they have the advantage of not requiring a separate frame.

Instead of creating a dialog box and populating it with items, it is possible to choose one of the convenient common dialog classes, such as
wxMessageDialog and wxFileDialog.

You never draw directly onto a window - you use a device context (DC). wxDC is the base for wxClientDC, wxPaintDC, wxMemoryDC,
wxPostScriptDC, wxMemoryDC, wxMetafileDC and wxPrinterDC. If your drawing functions have wxDC as a parameter, you can pass any of these DCs
to the function, and thus use the same code to draw to several different devices. You can draw using the member functions of wxDC, such as
wxDC::DrawLine and wxDC::DrawText. Control color on a window (wxColor) with brushes (wxBrush) and pens (wxPen).

To intercept(拦截) events, you add a wxDECLARE_EVENT_TABLE macro to the window class declaration, and put a wxBEGIN_EVENT_TABLE ...
wxEND_EVENT_TABLE block in the implementation file. Between these macros, you add event macros which map the event (such as a mouse click)
to a member function. These might override predefined event handlers such as for wxKeyEvent and wxMouseEvent.

Most modern applications will have an on-line, hypertext help system; for this, you need wxHelp and the wxHelpController class to control wxHelp.

GUI applications aren't all graphical wizardry. List and hash table needs are catered for by wxList and wxHashMap. You will undoubtedly need some
platform-independent Files and Directories, and you may find it handy to maintain and search a list of paths using wxPathList. There's many
Miscellaneous of operating system methods and other functions.
*/