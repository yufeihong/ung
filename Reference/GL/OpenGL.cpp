/*
glewExperimental = GL_TRUE;
//当环境创建失败的时候(比如：版本号错误)，这里就会失败。
if(glewInit())
{
//glew初始化失败的原因一般是在建立OpenGL context之前去初始化了。
wxMessageBox(wxT("Fail to initialise glew."));
exit(-1);
}

//实现这个OpenGL的公司
const uint8* openGLVendor = glGetString(GL_VENDOR);
BOOST_ASSERT(openGLVendor);
//OpenGL的版本号
const uint8* openGLVersion = glGetString(GL_VERSION);
BOOST_ASSERT(openGLVersion);
int32 openGLVersionMajor, openGLVersionMinor;
glGetIntegerv(GL_MAJOR_VERSION, &openGLVersionMajor);
glGetIntegerv(GL_MINOR_VERSION, &openGLVersionMinor);
BOOST_ASSERT(openGLVersionMajor >= 3);
BOOST_ASSERT(openGLVersionMinor >= 0);
//GLSL的版本号
std::string glslVersion = std::string(reinterpret_cast<const char*>(glGetString(GL_SHADING_LANGUAGE_VERSION))).substr(0, 4);
*/

/*
OpenGL概述：
什么是OpenGL：
OpenGL 是一种应用程序编程接口（ Application Programming Interface, API），它是一种
可以对图形硬件设备特性进行访问的软件库。

将输入图元的数学描述转换为与屏幕位置对应的像素片元（fragment）。这一步也称
作光栅化（ rasterization）。
最后，针对光栅化过程产生的每个片元，执行片元着色器（fragment shader），从而
决定这个片元的最终颜色和位置。

OpenGL 是使用客户端 - 服务端的形式实现的，我们编写的应用程序可以看做客户端，
而计算机图形硬件厂商所提供的 OpenGL 实现可以看做服务端。

初识OpenGL程序：
OpenGL 在其内部包含了所有的编译器工具，可以直接从着色器源代码创建 GPU 所
需的编译代码并执行。在 OpenGL 中，会用到四种
不同的着色阶段（ shader stage）。其中最常用的包括
的顶点着色器（vertex shader）以及片元着色器，前
者用于处理顶点数据，后者用于处理光栅化后的片
元数据。所有的 OpenGL 程序都需要用到这两类着
色器。

最终生成的图像包含了屏幕上绘制的所有像素
点。像素（pixel）是显示器上最小的可见单元。计
算机系统将所有的像素保存到帧缓存（framebuffer）
当中，后者是由图形硬件设备管理的一块独立内存
区域，可以直接映射到最终的显示设备上。

OpenGL是一个C语言形式的库。

着色管线装配（shader plumbing），也就是将应用程序的
数据与着色器程序的变量关联起来。

OpenGL语法：
OpenGL库中所有的函数都会以字符“gl”作为前
缀，然后是一个或者多个大写字母开头的词组，以此来命名一个完整的函数（例如
glBindVertexArray()）。 OpenGL 的所有函数都是这种格式。
为了能够方便地在不同的操作系统之间移植 OpenGL 程序， OpenGL 还为函数定义了
不同的数据类型，例如 GLfloat 是浮点数类型。
由于 OpenGL 是一个“C”语言形式的库，因此它不能使用函数的重载来处理不同类型的
数据，此时它使用函数名称的细微变化来管理实现同一类功能的函数集。
在函数名称的“核心”部分之后，我们通过后缀的变化来提示函数应当
传入的参数。例如， glUniform2f() 中的“ 2”表示这个函数需要传入 2 个参数值。
我们还要注意“ 2”之后的
“f”。这个字符表示这两个参数都是 GLfloat 类型的。最后，有些类型的函数名称末尾会有
一个“v”，它是 vector 的缩写，即表示我们需要用一个 1 维的 GLfloat 数组来传入 2 个浮
点数值（对于 glUniform2fv() 而言），而不是两个独立的参数值。
命令后缀与参数数据类型：
后缀		数据类型						通常对应的C语言数据类型								OpenGL 类型定义
b			8 位整型						signed char												GLbyte
s			16 位整型					signed short											GLshort
i			32 位整型					int															GLint,GLsizei
f			32 位浮点型					float														GLfloat,GLclampf
d			64 位浮点型					double													GLdouble,GLclampd
ub			8 位无符号整型				unsigned char											GLubyte
us			16 位无符号整型			unsigned short										GLushort
ui			32 位无符号整型			unsigned int											GLuint,GLenum,GLbitfield
使用C语言的数据类型来直接表示OpenGL数据类型时，因为OpenGL自身的实
现不同，可能会造成类型不匹配。如果直接在应用程序中使用OpenGL定义的数据
类型，那么当需要在不同的OpenGL实现之间移植自己的代码时，就不会产生数据
类型不匹配的问题了。

OpenGL 渲染管线：
OpenGL 实现了我们通常所说的渲染管线（rendering pipeline），它是一系列数据处理
过程，并且将应用程序的数据转换到最终渲染的图像。
OpenGL 首先接收用户提供的几何数据（顶点和几何图元），并且将它输入到一系列
着色器阶段中进行处理，包括：顶点着色、细分着色（它本身包含两个着色器），以及最
后的几何着色，然后它将被送入光栅化单元（rasterizer）。光栅化单元负责对所有剪切
区域（clipping region）内的图元生成片元数据，然后对每个生成的片元都执行一个片元
着色器。
OpenGL 管线：
顶点数据->顶点着色器->细分控制着色器->细分计算着色器->几何着色器->图元设置->剪切->光栅化->片元着色器->最终图像
准备向 OpenGL 传输数据
OpenGL 需要将所有的数据都保存到缓存对象（buffer object）中，它相当于由 OpenGL
服务端维护的一块内存区域。我们可以使用多种方式来创建这样的数据缓存，不过最常用
的方法就是使用glBufferData() 命令。我们可能还需要对缓存做一些额外的设
置。
将数据传输到 OpenGL
当将缓存初始化完毕之后，我们可以通过调用 OpenGL 的一个绘制命令来请求渲染几
何图元，glDrawArrays()就是一个常用的绘制命令。
OpenGL 的绘制通常就是将顶点数据传输到 OpenGL 服务端。我们可以将一个顶点视为
一个需要统一处理的数据包。这个包中的数据可以是我们需要的任何数据（也就是说，我们
自己负责定义构成顶点的所有数据），通常其中几乎始终会包含位置数据。其他的数据可能
用来决定一个像素的最终颜色。
顶点着色
对于绘制命令传输的每个顶点， OpenGL 都会调用一个顶点着色器来处理顶点相关的数
据。根据其他光栅化之前的着色器的活跃与否，顶点着色器可能会非常简单，例如，只是
将数据复制并传递到下一个着色阶段，这叫做传递着色器（pass-through shader）；它也可能
非常复杂，例如，执行大量的计算来得到顶点在屏幕上的位置（一般情况下，我们会用到变
换矩阵（transformation matrix）的概念，或者通过光照的计算
来判断顶点的颜色，或者其他一些技法的实现。
通常来说，一个复杂的应用程序可能包含许多个顶点着色器，但是在同一时刻只能有
一个顶点着色器起作用。
细分着色
顶点着色器处理每个顶点的关联数据之后，如果同时激活了细分着色器（tessellation
shader），那么它将进一步处理这些数据。细分着色器会使用
Patch 来描述一个物体的形状，并且使用相对简单的 Patch 几何体连接来完成细分的工作，
其结果是几何图元的数量增加，并且模型的外观会变得更为平顺。细分着色阶段会用到两
个着色器来分别管理 Patch 数据并生成最终的形状。
几何着色
下一个着色阶段—几何着色—允许在光栅化之前对每个几何图元做更进一步的处理，
例如创建新的图元。
图元装配
前面介绍的着色阶段所处理的都是顶点数据，此外这些顶点之间如何构成几何图元的
所有信息也会被传递到 OpenGL 当中。图元装配阶段将这些顶点与相关的几何图元之间组
织起来，准备下一步的剪切和光栅化工作。
剪切
顶点可能会落在视口（viewport）之外—也就是我们可以进行绘制的窗口区域—此
时与顶点相关的图元会做出改动，以保证相关的像素不会在视口外绘制。这一过程叫做剪
切（ clipping），它是由 OpenGL 自动完成的。
光栅化
剪切之后马上要执行的工作，就是将更新后的图元传递到光栅化单元，生成对应的片
元。我们可以将一个片元视为一个“候选的像素”，也就是可以放置在帧缓存中的像素，但
是它也可能被最终剔除，不再更新对应的像素位置。之后的两个阶段将会执行片元的处理，
即片元着色和逐片元的操作。
片元着色
最后一个可以通过编程控制屏幕上显示颜色的阶段，叫做片元着色阶段。在这个阶段
中，我们使用着色器来计算片元的最终颜色（尽管在下一个阶段（逐片元的操作）时可能
还会改变颜色一次）和它的深度值。片元着色器非常强大，在这里我们会使用纹理映射的
方式，对顶点处理阶段所计算的颜色值进行补充。如果我们觉得不应该继续绘制某个片元，
在片元着色器中还可以终止这个片元的处理，这一步叫做片元的丢弃（ discard）。
如果我们需要更好地理解处理顶点的着色器和片元着色器之间的区别，可以用这种方
法来记忆：顶点着色（包括细分和几何着色）决定了一个图元应该位于屏幕的什么位置，而
片元着色使用这些信息来决定某个片元的颜色应该是什么。
逐片元的操作
除了我们在片元着色器里做的工作之外，片元操作的下一步就是最后的独立片元处理
过程。在这个阶段里会使用深度测试（depth test，或者通常也称作 z-buffering）和模板测试
（ stencil test）的方式来决定一个片元是否是可见的。
如果一个片元成功地通过了所有激活的测试，那么它就可以被直接绘制到帧缓存中了，
它对应的像素的颜色值（也可能包括深度值）会被更新，如果开启了融合（blending）模式，
那么片元的颜色会与该像素当前的颜色相叠加，形成一个新的颜色值并写入帧缓存中。

第一个程序：深入分析：
glutInit() 负责初始化 GLUT 库。它会处理向程序输入的命令行参数，并且
移除其中与控制 GLUT 如何操作相关的部分（例如设置窗口的大小）。 glutInit() 必须是应用
程序调用的第一个 GLUT 函数，它会负责设置其他 GLUT 例程所必需的数据结构。
glutInitDisplayMode() 设置了程序所使用的窗口的类型。在这个例子中只需要设置窗
口使用 RGBA 颜色空间。除此之外，还可以给窗口设置更多
的 OpenGL 特性，例如使用深度缓存或者动画效果。
glutInitWindowSize() 设置所需的窗口大小。如果不想在这里设置一个固定值，也可以
先查询显示设备的尺寸，然后根据计算机的屏幕动态设置窗口的大小。
glutInitContextVersion() 和 glutInitContextProfile() 设置了我们所需
的 OpenGL 环境（context）的类型—这是 OpenGL 内部用于记录状态设置和操作的数据
结构。
核心模式（core profile），这个模
式可以确保使用的只是 OpenGL 的最新特性，否则也可以选择另外一种兼容模式，这样自
OpenGL 1.0 版本以来的所有特性都可以在程序中使用。
glutCreateWindow()，它的功能和它的名字一致。如果当前的系统
环境可以满足 glutInitDisplayMode() 的显示模式要求，这里就会创建一个窗口（此时会调
用计算机窗口系统的接口）。
glewExperimental = GL_TRUE，在glewInit()前面设置，否则gl函数会抛出异常。
glewInit() 函数，它属于我们用到的另一个辅助库
GLEW（OpenGL Extension Wrangler）。 GLEW 可以简化获取函数地址的过程，并且包含了
可以跨平台使用的其他一些 OpenGL 编程方法。
glutDisplayFunc()，它设置了显示回调（display callback），即 GLUT 在
每次更新窗口内容的时候会自动调用的例程。GLUT 可以使用一系列回调函数来处理诸如用户输入、重设
窗口尺寸等不同的操作。
glutMainLoop()，这是一个无限执行的循环，它
会负责一直处理窗口和操作系统的用户输入等操作。举例来说， glutMainLoop() 会判断窗
口是否需要进行重绘，然后它就会自动调用 glutDisplayFunc() 中注册的函数。特别要注意
的是， glutMainLoop() 是一个无限循环，因此不会执行在它之后的所有命令。
glGenVertexArrays() 分配了顶点数组对象（vertex-array object）。 OpenGL 会因此分配一部分顶点数组对象的名称供我们使用。
void glGenVertexArrays(GLsizei n, GLuint *arrays);
返回n个未使用的对象名到数组 arrays 中，用作顶点数组对象。返回的名字可以用来分配更多的缓存对象，并且它们已经使用未初始化的顶点数组集合的默认状态进行
了数值的初始化。Returns n currently unused names for use as vertex-array objects in the array arrays. The names returned are marked as used for 
the purposes of allocating additional buffer objects, and initialized with values representing the default state of the collection of uninitialized 
vertex arrays.
我们会发现很多 OpenGL 命令都是 glGen* 的形式，它们负责分配不同类型的 OpenGL
对象的名称。这里的名称类似 C 语言中的一个指针变量，我们必须分配内存并且用名称引
用它之后，名称才有意义。在 OpenGL 中，这个分配的机制叫做绑定对象（bind an object），
它是通过一系列 glBind* 形式的 OpenGL 函数集合去实现的。
void glBindVertexArray(GLuint array);
glBindVertexArray()完成了三项工作。如果输入的变量array非0，并且是
glGenVertexArrays() 所返回的，那么它将创建一个新的顶点数组对象并且与其名称关联
起来。如果绑定到一个已经创建的顶点数组对象中，那么会激活这个顶点数组对象，并
且直接影响对象中所保存的顶点数组状态。如果输入的变量array为0，那么 OpenGL 将
不再使用程序所分配的任何顶点数组对象，并且将渲染状态重设为顶点数组的默认状态。
如果array不是glGenVertexArrays()所返回的数值，或者它已经被glDeleteVertexArrays()函数释放了，那么这里将产生一个 GL_INVALID_OPERATION 错误。
glBindVertexArray() does three things. When using the value array that
is other than zero and was returned from glGenVertexArrays(), a new
vertex-array object is created and assigned that name. When binding to a
previously created vertex-array object, that vertex array object becomes
active, which additionally affects the vertex array state stored in the
object. When binding to an array value of zero, OpenGL stops using
application-allocated vertex-array objects and returns to the default state
for vertex arrays.
A GL_INVALID_OPERATION error is generated if array is not a value
previously returned from glGenVertexArrays(), or if it is a value that has
been released by glDeleteVertexArrays().
在生成一个顶点数组对象的名字之后，就会使用 glBindVertexArray()
将它绑定起来。在 OpenGL 中这样的对象绑定操作非常常见，但是我们可能无法立即了
解它做了什么。当我们第一次绑定对象时（例如，第一次用指定的对象名作为参数调用
glBind*()）， OpenGL 内部会分配这个对象所需的内存并且将它作为当前对象，即所有后继
的操作都会作用于这个被绑定的对象，例如，这里的顶点数组对象的状态就会被后面执行
的代码所改变。在第一次调用 glBind*() 函数之后，新创建的对象都会初始化为其默认状
态，而我们通常需要一些额外的初始化工作来确保这个对象可用。
after we generate a vertex-array object name, we bind it
with our call to glBindVertexArray(). Object binding like this is a very
common operation in OpenGL, but it may be immediately intuitive how
or why it works. When you bind an object for the first time (e.g., the first
time glBind*() is called for a particular object name), OpenGL will internally allocate the memory it needs and make that object current, which
means that any operations relevant to the bound object, like the vertexarray object we’re working with, will affect its state from that point on in
the program’s execution. After the first call to any glBind*() function, the
newly created object will be initialized to its default state and will usually
require some additional initialization to make it useful.
绑定对象的过程有点类似设置铁路的道岔开关。一旦设置了开关，从这条线路通过的
所有列车都会驶向对应的轨道。如果我们将开关设置到另一个状态，那么所有之后经过的
列车都会驶向另一条轨道。 OpenGL 的对象也是如此。总体上来说，在两种情况下我们需
要绑定一个对象：创建对象并初始化它所对应的数据时；以及每次我们准备使用这个对象，
而它并不是当前绑定的对象时。
当我们完成对顶点数组对象的操作之后，是可以调用 glDeleteVertexArrays() 将它释放的。
void glDeleteVertexArrays(GLsizei n, GLuint *arrays);
删除 n 个在 arrays 中定义的顶点数组对象，这样所有的名称可以再次用作顶点数组。
如果绑定的顶点数组已经被删除，那么当前绑定的顶点数组对象被重设为 0（类似执行了
glBindBuffer() 函数，并且输入参数为 0），而默认的顶点数组会变成当前对象。在 arrays
当中未使用的名称都会被释放，但是当前顶点数组的状态不会发生任何变化。
Deletes the n vertex-arrays objects specified in arrays, enabling the names
for reuse as vertex arrays later. If a bound vertex array is deleted, the
bindings for that vertex array become zero (as if you had called
glBindBuffer() with a value of zero) and the default vertex array becomes
the current one. Unused names in arrays are released, but no changes to
the current vertex array state are made.
最后，为了确保程序的完整性，我们可以调用 glIsVertexArray() 检查某个名称是否已
经被保留为一个顶点数组对象了。
GLboolean glIsVertexArray(GLuint array);
如果 array 是一个已经用 glGenVertexArrays() 创建且没有被删除的顶点数组对象的
名称，那么返回 GL_TRUE。如果 array 为 0 或者不是任何顶点数组对象的名称，那么返
回 GL_FALSE。
对于 OpenGL 中其他类型的对象，我们都可以看到类似的名为 glDelete* 和 glIs* 的例程。
Returns GL_TRUE if array is the name of a vertex-array object that was
previously generated with glGenVertexArrays(), but has not been
subsequently deleted. Returns GL_FALSE if array is zero or a nonzero
value that is not the name of a vertex-array object.
分配顶点缓存对象
顶点数组对象负责保存一系列顶点的数据。这些数据保存到缓存对象当中，并且由当
前绑定的顶点数组对象管理。我们只有一种顶点数组对象类型，但是却有很多种类型的
对象，并且其中一部分对象并不负责处理顶点数据。正如前文中所提到的，缓存对象就是
OpenGL 服务端分配和管理的一块内存区域，并且几乎所有传入 OpenGL 的数据都是存储在
缓存对象当中的。
A vertex-array object holds various data related to a collection of vertices.
Those data are stored in buffer objects and managed by the currently
bound vertex-array object. While there is only a single type of vertex-array
object, there are many types of objects, but not all of them specifically deal
with vertex data. As mentioned previously, a buffer object is memory that
the OpenGL server allocates and owns, and almost all data passed into
OpenGL is done by storing the data in a buffer object.
顶点缓存对象的初始化过程与顶点数组对象的创建过程类似，不过需要有向缓存中添
加数据的一个过程。
首先，我们需要创建顶点缓存对象的名称。我们调用的还是 glGen* 形式的函数，
即 glGenBuffers()。（VBO 即 Vertex Buffer
Objects）
void glGenBuffers(GLsizei n, GLuint *buffers);
返回 n 个当前未使用的缓存对象名称，并保存到 buffers 数组中。返回到 buffers 中的
名称不一定是连续的整型数据。
这里返回的名称只用于分配其他缓存对象，它们在绑定之后只会记录一个可用的状态。
0 是一个保留的缓存对象名称， glGenBuffers() 永远都不会返回这个值的缓存对象。
Returns n currently unused names for buffer objects in the array buffers.
The names returned in buffers do not have to be a contiguous set of
integers.
The names returned are marked as used for the purposes of allocating
additional buffer objects, but only acquire a valid state once they have
been bound.
Zero is a reserved buffer object name and is never returned as a buffer
object by glGenBuffers().
当分配缓存的名称之后，就可以调用 glBindBuffer() 来绑定它们了。由于 OpenGL 中有
很多种不同类型的缓存对象，因此绑定一个缓存时，需要指定它所对应的类型。在这个例
子中，由于是将顶点数据保存到缓存当中，因此使用 GL_ARRAY_BUFFER 类型。缓存对
象的类型现在共有 8 种，分别用于不同的 OpenGL 功能实现。
void glBindBuffer(GLenum target, GLuint buffer);
指定当前激活的缓存对象。 target 必须设置为以下类型中的一个： GL_ARRAY_
BUFFER、 GL_ELEMENT_ARRAY_BUFFER、 GL_PIXEL_PACK_BUFFER、 GL_PIXEL_
UNPACK_BUFFER、 GL_COPY_READ_BUFFER、 GL_COPY_WRITE_BUFFER、 GL_
TRANSFORM_FEEDBACK_BUFFER 和 GL_UNIFORM_BUFFER。 buffer 设置的是要绑定
的缓存对象名称。
glBindBuffer() 完成了三项工作： 1）如果是第一次绑定 buffer，且它是一个非零的无
符号整型，那么将创建一个与该名称相对应的新缓存对象。 2）如果绑定到一个已经创建
的缓存对象，那么它将成为当前被激活的缓存对象。 3）如果绑定的 buffer 值为 0，那么
OpenGL 将不再对当前 target 应用任何缓存对象。
所有的缓存对象都可以使用 glDeleteBuffers() 直接释放。
void glDeleteBuffers(GLsizei n, const GLuint *buffers);
删除 n 个保存在 buffers 数组中的缓存对象。被释放的缓存对象可以重用（例如，使
用 glGenBuffers()）。
如果删除的缓存对象已经被绑定，那么该对象的所有绑定将会重置为默认的缓存对
象，即相当于用 0 作为参数执行 glBindBuffer() 的结果。如果试图删除不存在的缓存对
象，或者缓存对象为 0，那么将忽略该操作（不会产生错误）。
我们也可以用 glIsBuffer() 来判断一个整数值是否是一个缓存对象的名称。
GLboolean glIsBuffer(GLuint buffer);
如果 buffer 是一个已经分配并且没有释放的缓存对象的名称，则返回 GL_TRUE。如
果 buffer 为 0 或者不是缓存对象的名称，则返回 GL_FALSE。
将数据载入缓存对象
初始化顶点缓存对象之后，我们需要把顶点数据从对象传输到缓存对象当中。这一步
是通过 glBufferData() 例程完成的，它主要有两个任务：分配顶点数据所需的存储空间，然
后将数据从应用程序的数组中拷贝到 OpenGL 服务端的内存中。
void glBufferData(GLenum target, GLsizeiptr size, const GLvoid *data, GLenum usage);
在 OpenGL 服务端内存中分配 size 个存储单元（通常为 byte），用于存储数据或者索引。
如果当前绑定的对象已经存在了关联的数据，那么会首先删除这些数据。
对于顶点属性数据， target 设置为 GL_ARRAY_BUFFER；索引数据为 GL_ELEMENT_
ARRAY_BUFFER ； OpenGL 的 像 素 数 据 为 GL_PIXEL_UNPACK_BUFFER ； 对 于 从
OpenGL 中获取的像素数据为 GL_PIXEL_PACK_BUFFER ；对于缓存之间的复制数据为
GL_COPY_READ_BUFFER 和 GL_COPY_WRITE_BUFFER ；对于纹理缓存中存储的纹理
数据为 GL_TEXTURE_BUFFER ；对于通过 transform feedback 着色器获得的结果设置为
GL_TRANSFORM_FEEDBACK_BUFFER；而一致变量要设置为 GL_UNIFORM_BUFFER。
size 表示存储数据的总数量。这个数值等于 data 中存储的元素的总数乘以单位元素
存储空间的结果。
data 要么是一个客户端内存的指针，以便初始化缓存对象，要么是 nullptr。如果传
入的指针合法，那么将会有 size 大小的数据从客户端拷贝到服务端。如果传入 nullptr，
那么将保留 size 大小的未初始化的数据，以备后用。
usage 用于设置分配数据之后的读取和写入方式。可用的方式包括 GL_STREAM_
DRAW、 GL_STREAM_READ、 GL_STREAM_COPY、 GL_STATIC_DRAW、 GL_STATIC_
READ、 GL_STATIC_COPY、 GL_DYNAMIC_DRAW、 GL_DYNAMIC_READ 和 GL_
DYNAMIC_COPY。
如果所需的 size 大小超过了服务端能够分配的额度，那么 glBufferData() 将产生一
个 GL_OUT_OF_MEMORY 错误。如果 usage 设置的不是可用的模式值，那么将产生
GL_INVALID_VALUE 错误。

GL_STATIC_DRAW：我们需要指定数据在
OpenGL 中使用的方式。因为我们只是用它来绘制几何体，不会在运行时对它做出修改，所以设置 glBufferData() 的 usage 参数为 GL_STATIC_DRAW。

OpenGL 只能够绘制坐标空间内的几何体图元。而具有该范围[–1, 1]限
制的坐标系统也称为规格化设备坐标系统（Normalized Device Coordinate， NDC）。

初始化顶点与片元着色器
我们可以以字符串的形式传输 GLSL 着色器到 OpenGL。
“#version 430 core”指定了我们所用的 OpenGL 着色语言的版本。这里的
“ 430”表示我们准备使用 OpenGL 4.3 对应的 GLSL 语言。
这里的“core”表示我们将使用 OpenGL 核心模式（core profile），这与之前
GLUT 的函数 glutInitContextProfile() 设置的内容应当一致。每个着色器的第一行都应该设
置“#version”，否则系统会假设使用“ 110”版本，但是这与 OpenGL 核心模式并不兼容。
着色器变量是着色器与外部世界的联系所在。
换句话说，着色器并不知道自己的数据从哪里来，它只是在每次运行时直接获取数据对应
的输入变量。而我们必须自己完成着色管线的装配，然后才可以将应用程序中的数据与不同的 OpenGL 着色阶段互相关联。
layout(location = 0) in vec4 vPosition;
vec4，GLSL 的四维浮点数向量。OpenGL会用默认数值自动填充缺失的坐标值。而vec4的默认值为(0,0,0,1)
layout(location = 0),布局限定符，目的是为变量提供元数据。我们可以使用布局限定符来设置很多不同的属性，其中有些是与不同的着色阶段相关的。
在这里，设置vPosition的位置属性location为0。这个设置与下面两行共同起作用：
glVertexAttribPointer(vPosition, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
glEnableVertexAttribArray(vPosition);
这两个函数指定了顶点着色器的变量与我们存储在缓存对象中数据的关系，这也就是我们所说的着色管线装配的过程，即将应用程序与着色器之间，以及不同着色阶段
之间的数据通道连接起来。
为了输入顶点着色器的数据，也就是OpenGL将要处理的所有顶点数据，需要在着色器中声明一个in变量，然后使用glVertexAttribPointer()将它关联到一个顶点属性
数组。
void glVertexAttribPointer(GLuint index,GLint size,GLenum type,GLboolean normalized,GLsizei,const GLvoid* pointer):
设置index（着色器中的属性位置）位置对应的数据值。pointer表示缓存对象中，从起始位置开始计算的数组数据的偏移值（假设起始地址为0），使用基本的系统单位
（byte）。size表示每个顶点需要更新的分量数目，可以是1，2，3，4或者GL_BGRA。type指定了数组中每个元素的数据类型（GL_BYTE,GL_UNSIGNED_TYPE,
GL_SHORT、 GL_UNSIGNED_SHORT、 GL_INT、 GL_UNSIGNED_INT、 GL_FIXED、 GL_
HALF_FLOAT、 GL_FLOAT 或 GL_DOUBLE）。 normalized 设置顶点数据在存储前是否
需要进行归一化（或者使用 glVertexAttribFourN*() 函数）。 stride 是数组中每两个元素之
间的大小偏移值（ byte）。如果 stride 为 0，那么数据应该紧密地封装在一起。）
参数名称,数值,解释
index
0
这 就 是 顶 点 着 色 器 中 输 入 变 量 的 location 值， 也 就 是 之 前 的
vPosition。在着色器中这个值用来直接指定布局限位符，不过也
可以用于着色器编译后的判断
size
2
这 就 是 数 组 中 每 个 顶 点 的 元 素 数 目。 vertices 中 共 有
NumVertices 个顶点，每个顶点有两个元素值
type
GL_FLOAT
这个枚举量表示 GLfloat 类型
normalized
GL_FALSE
这里设置为 GL_FALSE 的原因有两个：最重要的第一点是因为它
表示位置坐标值，因此可以是任何数值，不应当限制在 [-1, 1] 的归
一化范围内，第二点是因为它不是整型（ GLint 或者 GLshort）
stride
0
数据在这里是“紧密封装”的，即每组数据值在内存中都是立即
与下一组数据值相衔接的，因此可以直接设置为 0
pointer
BUFFER_OFFSET(0)
这里设置为 0，因为数据是从缓存对象的第一个字节（地址为 0）
开始的

#define BUFFER_OFFSET(offset) ((void *)(offset))
在以往版本的 OpenGL 当中并不需要用到这个宏 ，不过现在我们希望使用它来设置数据在缓
存对象中的偏移量，而不是像 glVertexAttribPointer() 的原型那样直接设置一个指向内存块的指针。
在OpenGL的早期版本当中（3.1版本之前），顶点属性数据可以直接保存在应用程序内存中，而不一定是GPU的缓存对象。

glEnableVertexAttribArray(vPosition);
启用顶点属性数组，同时将glVertexAttribPointer初始化的属性数组指针索引传入这个函数。

void glEnableVertexAttribArray(GLuint index);
void glDisableVertexAttribArray(GLuint index);
设置是否启用与 index 索引相关联的顶点数组。 index 必须是一个介于 0 到 GL_
MAX_VERTEX_ATTRIBS-1 之间的值。

OpenGL使用RGBA颜色空间。

void glClear(GLbitfield mask);
清除指定的缓存数据并重设为当前的清除值。mask是一个可以通过逻辑“或”操作来指定多个数值的参数。
缓存				名称
颜色缓存			GL_COLOR_BUFFER_BIT
深度缓存			GL_DEPTH_BUFFER_BIT
模板缓存			GL_STENCIL_BUFFER_BIT

void glClearColor(GLclampf red, GLclampf green, GLclampf blue, GLclampf alpha);
设置当前使用的清除颜色值，用于 RGBA 模式下对颜色缓存的清除工作。这里的 red、 green、 blue、 alpha 都会被截断到 [0, 1] 的范围内。
默认的清除颜色是 (0, 0, 0, 0)，在 RGBA 模式下它表示黑色。

OpenGL 有一个庞大的状态量列表。

使用 OpenGL 进行绘制
用 glBindVertexArray() 来选择作为顶点数据使用的顶点数组。其次调用 glDrawArrays() 来实现顶点数据向 OpenGL 管线的传输。
void glDrawArrays(GLenum mode, GLint first, GLsizei count);
使用当前绑定的顶点数组元素来建立一系列的几何图元，起始位置为 first，而结
束位置为 first + count-1。 mode 设置了构建图元的类型，它可以是 GL_POINTS、 GL_
LINES、 GL_LINE_STRIP、 GL_LINE_LOOP、 GL_TRIANGLES、 GL_TRIANGLE_
STRIP、 GL_TRIANGLE_FAN 和 GL_PATCHES 中的任意一种。GL_PATCHES 类型是不会输出任何结果的，因为它是用于细分着色器的。
glFlush()，即强制所有进行中的 OpenGL 命令立即完成并
传输到 OpenGL 服务端处理。
void glFlush(void);
强制之前的 OpenGL 命令立即执行，这样就可以保证它们在一定时间内全部完成。
事实上 glFlush()
只是强制所有运行中的命令送入 OpenGL 服务端而已，并且它会立即返回—它并不会等
待所有的命令完成，而等待却是我们所需要的。为此，我们需要使用 glFinish() 命令，它会
一直等待所有当前的 OpenGL 操作完成后，再返回。
void glFinish(void);
强制所有当前的 OpenGL 命令立即执行，并且等待它们全部完成。
注意：你最好只是在开发阶段使用 glFinish()—如果你已经完成了开发的工作，那么最
好去掉对这个命令的调用。虽然它对于判断 OpenGL 命令运行效率很有帮助，但是
对于程序的整体性能却有着相当的拖累。
绝大多数的操作模式都可以通过 glEnable()
和 glDisable() 命令开启或者关闭。
void glEnable(GLenum capability);
void glDisable(GLenum capability);
glEnable() 会开启一个模式， glDisable() 会关闭它。有很多枚举量可以作为模式
参数传入 glEnable() 和 glDisable()。例如 GL_DEPTH_TEST 可以用来开启或者关闭深
度测试； GL_BLEND 可以用来控制融合的操作，而 GL_RASTERIZER_DISCARD 用于
transform feedback 过程中的高级渲染控制。
GLboolean glIsEnabled(GLenum capability);
根据是否启用当前指定的模式，返回 GL_TRUE 或者 GL_FALSE。

=====================================================================================

着色器基础：
从 3.1 版本开始，固定功能管线从核心模式中去除。

OpenGL 的可编程管线：
4.3 版本的图形管线有 4 个处理阶段，还有 1 个通用
计算阶段，每个阶段都需要由一个专门的着色器进行控制。
1）顶点着色阶段（vertex shading stage）将接收你在顶点缓存对象中给出的顶点数据，
独立处理每个顶点。这个阶段对于所有的 OpenGL 程序都是必需的，并且必须绑定一个着
色器。
2）细分着色阶段（tessellation shading stage）是一个可选的阶段，与应用程序中显式地
指定几何图元的方法不同，它会在 OpenGL 管线内部生成新的几何体。这个阶段启用之后，
会收到来自顶点着色阶段的输出数据，并且对收到的顶点进行进一步的处理。
3）几何着色阶段（geometry shading stage）也是一个可选的阶段，它会在 OpenGL 管
线内部对所有几何图元进行修改。这个阶段会作用于每个独立的几何图元。此时你可以选
择从输入图元生成更多的几何体，改变几何图元的类型（例如将三角形转化为线段），或者
放弃所有的几何体。如果这个阶段被启用，那么几何着色阶段的输入可能会来自顶点着色
阶段完成几何图元的顶点处理之后，也可能来自细分着色阶段生成的图元数据（如果它也被
启用）。
4） OpenGL 着色管线的最后一个部分是片元着色阶段（Fragment shading stage）。这个阶
段会处理 OpenGL 光栅化之后生成的独立片元（如果启用了采样着色的模式，就是采样数据），
并且这个阶段也必须绑定一个着色器。在这个阶段中，计算一个片元的颜色和深度值，然后
传递到管线的片元测试和混合的模块。
5）计算着色阶段（Compute shading stage）和上述阶段不同，它并不是图形管线的一部
分，而是在程序中相对独立的一个阶段。计算着色阶段处理的并不是顶点和片元这类图形
数据，而是应用程序给定范围的内容。计算着色器在应用程序中可以处理其他着色器程序
所创建和使用的缓存数据。这其中也包括帧缓存的后处理效果，或者我们所期望的任何事
物。

GLSL 的 main() 函数没有任何参数，在某个着色阶段中
输入和输出的所有数据都是通过着色器中的特殊全局变量来传递的（请不要将它们与应用
程序中的全局变量相混淆—着色器变量与你在应用程序代码中声明的变量是完全不相干
的）。

#version 330 core
in vec4 vPosition;
in vec4 vColor;
out vec4 color;
uniform mat4 ModelViewProjectionMatrix;
void
main()
{
color = vColor;
gl_Position = ModelViewProjectionMatrix * vPosition;
}
在程序起始的位置总是要使用 #version 来声明所使用的版本。
main()函数不需要返回一个整数值，它被声明为void。
OpenGL会使用输入和输出变量来传输着色器所需的数
据。除了每个变量都有一个类型之外（例如 vec4，后文将深入地进行介绍）， OpenGL 还定
义了 in 变量将数据拷贝到着色器中，以及 out 变量将着色器的内容拷贝出去。这些变量的
值会在 OpenGL 每次执行着色器的时候更新（如果 OpenGL 处理的是顶点，那么这里会为每
个顶点传递新的值；如果是处理片元，那么将为每个片元传递新值）。另一类变量是直接从
OpenGL 应用程序中接收数据的，称作 uniform 变量。 uniform 变量不会随着顶点或者片元
的变化而变化，它对于所有的几何体图元的值都是一样的，除非应用程序对它进行了更新。
数字或者下划线不能作为变量名称的
第一个字符。此外，变量名称也不能包含连续
的下划线（这些名称是 GLSL 保留使用的）。
GLSL中的基本数据类型
类型		描述
float		IEEE 32 位浮点值
double	IEEE 64 位浮点值
int			有符号二进制补码的 32 位整数
uint		无符号的 32 位整数
bool		布尔值

在任何函数定义之外声明的变量拥有全局作用域，因此对着色器程序中的所有函数
都是可见的。
在一组大括号之内（例如函数定义、循环或者“if”引领的代码块等）声明的变量，
只能在大括号的范围内存在。
循环的迭代自变量，例如，下面的循环中的i只能在循环体内起作用。
for (int i = 0; i < 10; ++i)
{
//loop body
}
所有变量都必须在声明的同时进行初始化。
浮点数可以选择在末尾添加一个“f”或者“F”后缀。
如果要表达一个double精度的浮点数，必须在末尾添加后缀“lF”或者“LF”。

GLSL 中的隐式类型转换
所需的类型		可以从这些类型隐式转换
uint				int
float				int、 uint
double			int、 uint、 float
int ten = int(f);
这里用到了一个int转换构造函数来完成转换。

聚合类型
GLSL 支持 2 个、 3 个以及 4 个分量的向量，每个分量都可以使用 bool、 int、
uint、 fl oat 和 double 这 些 基 本 类 型。 此 外， GLSL 也 支 持 fl oat 和 double 类 型 的
矩阵。下表给出了所有可用的向量和矩阵类型。
GLSL 的向量与矩阵类型
基本类型			2D向量				3D向量					4D向量						矩阵类型
float				vec2					vec3						vec4							mat2			mat3			mat4
																										mat2×2		mat2×3		mat2×4
																										mat3×2		mat3×3		mat3×4
																										mat4×2		mat4×3		mat4×4

double			dvec2				dvec3					dvec4						dmat2		dmat3		dmat4
																										dmat2×2	dmat2×3	dmat2×4
																										dmat3×2	dmat3×3	dmat3×4
																										dmat4×2	dmat4×3	dmat4×4
int					ivec2					ivec3						ivec4							—
uint				uvec2				uvec3					uvec4						—
bool				bvec2				bvec3					bvec4						—

mat4x3,其中第一个值表示列数，第二个值表示行数。
vec3 white = vec3(1.0)										//white = (1.0,1.0,1.0)
							4.0	0.0	0.0
m = mat3(4.0) =	0.0	4.0	0.0
							0.0	0.0	4.0
矩阵的指定需要遵循列主序的原则，也就是说，传入的数据将首先填充列。
vec4 zVec = m[2];												//获取矩阵的第3列

向量分量的访问符
分量访问符				符号描述
(x, y, z, w)				与位置相关的分量
(r, g, b, a)				与颜色相关的分量
(s, t, p, q)				与纹理坐标相关的分量

数组属于 GLSL 中的第一等（first-class）类型，也就是说它有构造函数，并且可以用作
函数的参数和返回类型。如果我们要静态初始化一个数组的值，那么可以按照下面的形式
来使用构造函数：
float coeff[3] = float[3](1.0,1.0,2.0);
这里构造函数的维数可以不填。
返回元素的个数：coeff.length()
向量和矩阵类型也可以使用 length() 方法。向量的长度也就是它包含的分量的个数，
矩阵的长度是它包含的列的个数。事实上，当我们使用数组的形式来索引向量和矩阵的值
的时候（例如， m[2] 是矩阵 m 的第三列），这个方法返回的就是我们需要的数据。
mat3x4 m;
int c = m.length(); //m 包含的列数为 3
int r = m[0].length(); //第 0 个列向量中分量的个数为 4
因为长度值在编译时就是已知的，所以 length() 方法会返回一个编译时常量，我们可
以在需要使用常量的场合直接使用它，例如：
mat4 m;
fl oat diagonal[m.length()]; //设置数组的大小与矩阵大小相等
fl oat x[gl_in.length()]; //设置数组的大小与几何着色器的输入顶点数相等
对于所有向量和矩阵，以及大部分的数组来说， length() 都是一个编译时就已知的常量。
但是对于某些数组来说， length() 的值在链接之前可能都是未知的。如果使用链接器来减
少同一阶段中多个着色器的大小，那么可能发生这种情况。对于着色器中保存的缓存对象， length() 的值直到渲染时才可能得到。如果我
们需要 length() 返回一个编译时常量，那么我们需要保证着色器中的数组大小在使用它的
length() 方法之前就已经确定了。
多维数组相当于从数组中再创建数组，它的语法与 C 语言当中类似：
fl oat coeff[3][5]; //一个大小为 3 的数组，其中包含了大小为 5 的多个数组
coeff[2][1] *= 2.0; //内层索引为 1，外层为 2
coeff.length(); //这个方法会返回常量 3
coeff[2]; //这是一个大小为 5 的 1 维数组
coeff[2].length(); //这个方法会返回常量 5
多维数组可以使用任何类型或者形式来构成。

GLSL 的类型修饰符
类型修饰符	描述
const			将一个变量定义为只读形式。如果它初始化时用的是一个编译时常量，那么它本身也会成为编译时常量
in				设置这个变量为着色器阶段的输入变量
out			设置这个变量为着色器阶段的输出变量
uniform		设置这个变量为用户应用程序传递给着色器的数据，它对于给定的图元而言是一个常量
buffer		设置应用程序共享的一块可读写的内存。这块内存也作为着色器中的存储缓存（storage buffer）使用
				Specifies read-write memory shared with the application. This memory is also referred to as a shader storage buffer.
shared		设置变量是本地工作组（ local work group）中共享的。它只能用于计算着色器中

in 存储限制符
in 修饰符用于定义着色器阶段的输入变量。这类输入变量可以是顶点属性（对于顶点
着色器），或者前一个着色器阶段的输出变量。
片元着色器也可以使用一些其他的关键词来限定自己的输入变量
out 存储限制符
out 修饰符用于定义一个着色器阶段的输出变量—例如，顶点着色器中输出变换后
的齐次坐标，或者片元着色器中输出的最终片元颜色。
uniform 存储限制符
在着色器运行之前， uniform 修饰符可以指定一个在应用程序中设置好的变量，它不
会在图元处理的过程中发生变化。 uniform 变量在所有可用的着色阶段之间都是共享的，
它必须定义为全局变量。任何类型的变量（包括结构体和数组）都可以设置为 uniform 变
量。着色器无法写入到 uniform 变量，也无法改变它的值。
举例来说，我们可能需要设置一个给图元着色的颜色值。此时可以声明一个 uniform 变量，将颜色值信息传递到着色器当中。而着色器中会进行如下声明：
uniform vec4 BaseColor;
在着色器中，可以根据名字 BaseColor 来引用这个变量，但是如果需要在用户应
用程序中设置它的值，还需要多做一些工作。 GLSL 编译器会在链接着色器程序时创建
一个 uniform 变量列表。如果需要设置应用程序中 BaseColor 的值，我们需要首先获得
BaseColor 在列表中的索引，这一步可以通过 glGetUniformLocation() 函数来完成。
GLint glGetUniformLocation(GLuint program, const char* name);
返回着色器程序中 uniform 变量 name 对应的索引值。 name 是一个以 nullptr 结尾的字
符串，不存在空格。如果 name 与启用的着色器程序中的所有 uniform 变量都不相符，或者
name 是一个内部保留的着色器变量名称（例如，以 gl_ 开头的变量），那么返回值为 -1。
name 可以是单一的变量名称、数组中的一个元素（此时 name 主要包含方括号以及
对应的索引数字），或者结构体的域变量（设置 name 时，需要在结构体变量名称之后添加
“.”符号，再添加域变量名称，并与着色器程序中的写法一致）。对于 uniform 变量数组，
也可以只通过指定数组的名称来获取数组中的第一个元素（例如，直接用“arrayName”），
或者也可以通过指定索引值来获取数组的第一个元素（例如，写作“ arrayName[0]”）。
除非我们重新链接着色器程序（参见 glLinkProgram()），否则这里的返回值不会发生
变化。
当 得 到 uniform 变 量 的 对 应 索 引 值 之 后， 我 们 就 可 以 通 过 glUniform*() 或 者
glUniformMatrix*() 系列函数来设置 uniform 变量的值了。
下面是一个获取 uniform 变量的索引并且设置具体值的示例。
GLint timeLoc; //着色器中的uniform变量time的索引
GLfloat timeValue; //程序运行时间
timeLoc = glGetUniformLocation(program, "time");
glUniform1f(timeLoc, timeValue)

void glUniform{1234}{fdi ui}(GLint location, TYPE value);
void glUniform{1234}{fdi ui}v(GLint location, GLsizei count, const TYPE* values);
void glUniformMatrix{234}{fd}v(GLint location, GLsizei count, GLboolean transpose,const GLfloat* values);
void glUniformMatrix{2x3,2x4,3x2,3x4,4x2,4x3}{fd}v(GLint location, GLsizei count,GLboolean transpose, const GLfloat* values);
设置与 location 索引位置对应的 uniform 变量的值。其中向量形式的函数会载入
count 个数据的集合（根据 glUniform*() 的调用方式，读入 1 ～ 4 个值），并写入 location
位置的 uniform 变量。如果 location 是数组的起始索引值，那么数组之后的连续 count 个
元素都会被载入。
GLfloat 形式的函数（后缀中有 f）可以用来载入单精度类型的浮点数、 float 类型的
向量、 float 类型的数组、或者 float 类型的向量数组。与之类似， GLdouble 形式的函数
（后缀中有 d）可以用来载入双精度类型的标量、向量和数组。 GLfloat 形式的函数也可以
载入布尔数据。
GLint 形式的函数（后缀中有 i）可以用来更新单个有符号整型、有符号整型向量、
有符号整型数组，或者有符号整型向量数组。此外，可以用这种形式载入独立纹理采样
器或者纹理数组、布尔类型的标量、向量和数组。与之类似， GLuint 形式的函数（后缀
中有 ui）也可以用来载入无符号整型标量、向量和数组。
对于 glUniformMatrix{234}*() 系列函数来说，可以从 values 中读入 2×2、 3×3
或者 4×4 个值来构成矩阵。
对于 glUniformMatrix{2x3,2x4,3x2,3x4,4x2,4x3}*() 系列函数来说，可以从 values
中读入对应矩阵维度的数值并构成矩阵。如果 transpose 设置为 GL_TRUE，那么 values
中的数据是以行主序的顺序读入的（与 C 语言中的数组类似），如果是 GL_FALSE，那么
values 中的数据是以列主序的顺序读入的。

buffer 存储限制符
如果需要在应用程序中共享一大块缓存给着色器，那么最好的方法是使用 buffer 变
量。它与 uniform 变量非常类似，不过也可以用着色器对它的内容进行修改。通常来说，
需要在一个 buffer 块中使用 buffer 变量，本章后面将对“块”的概念进行介绍。
buffer 修饰符指定随后的块作为着色器与应用程序共享的一块内存缓存。这块缓存对
于着色器来说是可读的也是可写的。缓存的大小可以在着色器编译和程序链接完成后设置。
shared 存储限制符
shared 修饰符只能用于计算着色器当中，它可以建立本地工作组内共享的内存。

GLSL 操作符与优先级（降序排列）
优先级		操作符										可用类型									描述
1				()												—											成组操作
2				[]												数组、矩阵、向量						数组的下标
				f()												函数										函数调用与构造函数
				.（句点）									结构体									访问结构体的域变量或者方法
				++ --										算术类型									后置递增 / 递减
3				++ --										算术类型									前置递增 / 递减
				+ -											算术类型									一元正 / 负数
				~												整型										一元按位“非”（ not）
				!												布尔型									一元逻辑“非”（ not）
4				* / %										算术类型									乘法运算
5				+ -											算术类型									相加运算
6				<< >>										整型										按位操作
7				< > <= >=								算术类型									关系比较操作
8				== !=										任意										相等操作
9				&												整型										按位“与”（ and）
10				^												整型										按位“异或”（ xor）
11				|												整型										按位“或”（ or）
12				&&											布尔型									逻辑“与”（ and）
13				^^											布尔型									逻辑“异或”（ xor）
14				||												布尔型									逻辑“或”（ or）
15				a ? b : c										布尔型 ? 任意 : 任意					三元选择操作符（相当于内部进行了条件判断，如果a 成立则执行 b，否则执行 c）
16				=												任意										赋值
				+= -=										算术类型									算术赋值
				*= /=										算术类型
				%= <<= >>=							整型
				&= ^= |=									整型
17				,（逗号）									任意										操作符序列

GLSL支持switch语句，支持for，while，do...while循环。

discard 丢弃当前的片元，终止着色器的执行。 discard 语句只能用于片元着色器中。

用户自定义函数可以在单个着色器对象中定义，然后在多个着色器程序中复用。
在函数的声明语法中，需要给参数添加访问修饰符，返回值为数组时，必须显式地指定其大小，数组作为参数时也一样。
GLSL函数参数的访问修饰符
访问修饰符				描述
in							将数据拷贝到函数中（如果没有指定修饰符，默认这种形式）
const in					将只读数据拷贝到函数中
out						从函数中获取数值（因此输入函数的值是未定义的）
inout						将数据拷贝到函数中，并且返回函数中修改的数据
如果需要在编译时验证函数是否修改了某个输入变量，可以添加一个 const in 修
饰符来阻止函数对变量进行写操作。如果不这么做，那么在函数中写入一个 in 类型的变量，
相当于对变量的局部拷贝进行了修改，因此只在函数自身范围内产生作用。

GLSL中没有文件包含的命令#include。
GLSL 的预处理器命令
预处理器命令				描述
#define
#undef						控制常量与宏的定义，与 C 语言的预处理器命令类似

#if
#ifdef
#ifndef
#else
#elif
#endif						代码的条件编译，与 C 语言的预处理器命令和 defi ned 操作符均类似。条件表达式中只可以使用整数表达式或者 #define 定义的值

#error text					强制编译器将 text 文字内容（直到第一个换行符为止）插入到着色器的信息日志当中
#pragma options			控制编译器的特定选项
#extension options	设置编译器支持特定 GLSL 扩展功能
#version number		设置当前使用的 GLSL 版本名称
#line options				设置诊断行号

宏定义，不支持字符串替换以及预编译连接符。宏可以定义为单一的值：
#define NUM_ELEMENTS 10
或者带有参数，例如：
#define LPos(n) gl_LightSource[(n)].position
此外，还提供了一些预先定义好的宏，用于记录一些诊断信息（可以通过#error命令输出）
GLSL 预处理器中的预定义宏
__LINE__				行号，默认为已经处理的所有换行符的个数加一，也可以通过 #line 命令修改
__FILE__				当前处理的源字符串编号
__VERSION_			OpenGL 着色语言版本的整数表示形式
此外，也可以通过 #undef 命令来取消之前定义过的宏（ GLSL 内置的宏除外）。
#ifdef NUM_ELEMENTS
...
#endif

#if defined(NUM_ELEMENTS) && NUM_ELEMENTS > 3
...
#elif NUM_ELEMENTS < 7
...
#endif

编译器的控制
#pragma 命令可以向编译器传递附加信息，并在着色器代码编译时设置一些额外属性。
编译器优化选项
优化选项用于启用或者禁用着色器的优化，它会直接影响该命令所在的着色器源代码。
可以通过下面的命令分别启用或者禁用优化选项：
#pragma optimize(on)
or
#pragma optimize(off)
这类选项必须在函数定义的代码块之外设置。一般默认所有着色器都开启了优化选项。

编译器调试选项
调试选项可以启用或者禁用着色器的额外诊断信息输出。可以通过下面的命令分别启
用或者禁用调试选项：
#pragma debug(on)
or
#pragma debug(off)
默认情况下，所有着色器都会禁用调试选项。

数据块接口：
着色器与应用程序之间，或者着色器各阶段之间共享的变量可以组织为变量块的形式，
并且有的时候必须采用这种形式。 uniform 变量可以使用 uniform 块，输入和输出变量可
以使用 in 和 out 块，着色器的存储缓存可以使用 buffer 块。
uniform b { //"uniform" or "in" or "out" or "buffer"
vec4 v1; //list of variables
bool v2; //...
}; //access members as "v1" and "v2"
Or:
uniform b { //"uniform" or "in" or "out" or "buffer"
vec4 v1; //list of variables
bool v2; //...
} name; //access members as "name.v1" and "name.v2"
uniform 块
如果着色器程序变得比较复杂，那么其中用到的 uniform 变量的数量也会上升。通常
会在多个着色器程序中用到同一个 uniform 变量。由于 uniform 变量的位置是着色器链接
的时候产生的（也就是调用 glLinkProgram() 的时候），因此它在应用程序中获得的索引可
能会有变化，即使我们给 uniform 变量设置的值可能是完全相同的。而 uniform 缓存对象
（Uniform buffer object）就是一种优化 uniform 变量访问，以及在不同的着色器程序之间共
享 uniform 数据的方法。
正如你所知道的， uniform 变量是同时存在于用户应用程序和着色器当中的，因此需要
同时修改着色器的内容并调用 OpenGL 函数来设置 uniform 缓存对象。

指定着色器中的 uniform 块
访问一组 uniform 变量的方法是使用诸如 glMapBuffer() 的 OpenGL 函数。
注意，着色器中的数据类型有两种：不透明的和透明的；其中不透明类型包括采样器、
图像和原子计数器。一个 uniform 块中只可以包含透明类型的变量。此外， uniform 块必须在全局作用域内声明。
uniform 块的布局控制
在 uniform 块中可以使用不同的限制符来设置变量的布局方式。这些限制符可以用来
设置单个的 uniform 块，也可以用来设置所有后继 uniform 块的排列方式（需要使用布局声
明）。可用的限制符及其介绍如下表所示：
布局限制符					描述
shared						设置 uniform 块是多个程序间共享的（这是默认的布局方式，与 shared 存储限制符不存在混淆）
packed						设置 uniform 块占用最小的内存空间，但是这样会禁止程序间共享这个块
std140						使用标准布局方式来设置 uniform 块或者着色器存储的 buffer 块
std430						使用标准布局方式来设置 buffer 块
row_major					使用行主序的方式来存储 uniform 块中的矩阵
column_major				使用列主序的方式来存储 uniform 块中的矩阵（这也是默认的顺序）
例如，如果需要共享一个 uniform 块，并且使用行主序的方式来存储数据，那么可以使
用下面的代码来声明它：
layout (shared, row_major) uniform { ... };

访问 uniform 块中声明的 uniform 变量
虽然 uniform 块已经命名了，但是块中声明的 uniform 变量并不会受到这个命名的限
制。也就是说， uniform 块的名称并不能作为 uniform 变量的父名称，因此在两个不同名的
uniform 块中声明同名变量会在编译时造成错误。然而，在访问一个 uniform 变量的时候，也不一定非要使用块的名称。

从应用程序中访问 uniform 块：
uniform 变量是着色器与应用程序之间共享数据的桥梁，因此如果着色器中的 uniform
变量是定义在命名的 uniform 块中，那么就有必要找到不同变量的偏移值。如果获取了这些
变量的具体位置，那么就可以使用数据对它们进行初始化，这一过程与处理缓存对象（使用
glBufferData() 等函数）是一致的。
首 先 假 设 已 知 应 用 程 序 的 着 色 器 中 uniform 块 的 名 字。 如 果 要 对 uniform 块 中 的
uniform 变量进行初始化，那么第一步就是找到块在着色器程序中的索引位置。可以调
用 glGetUniformBlockIndex() 函数返回对应的信息，然后在应用程序的地址空间里完成
uniform 变量的映射。
GLuint glGetUniformBlockIndex(GLuint program, const char * uniformBlockName);
返 回 program 中 名 称 为 uniformBlockName 的 uniform 块 的 索 引 值。 如 果
uniformBlockName 不是一个合法的 uniform 程序块，那么返回 GL_INVALID_INDEX。
如果要初始化 uniform 块对应的缓存对象，那么我们需要使用 glBindBuffer() 将缓存对
象绑定到目标 GL_UNIFORM_BUFFER 之上。
当对缓存对象进行初始化之后，我们需要判断命名的 uniform 块中的变量总共占据了多
大的空间。我们可以使用函数 glGetActiveUniformBlockiv() 并且设置参数为 GL_UNIFORM_
BLOCK_DATA_SIZE，这样就可以返回编译器分配的块的大小（根据 uniform 块的布局设置，
编译器可能会自动排除着色器中没有用到的 uniform 变量）。 glGetActiveUniformBlockiv() 函
数还可以用来获取一个命名的 uniform 块的其他一些相关参数。
在获取 uniform 块的索引之后，我们需要将一个缓存对象与这个块相关联。最常见的方
法是调用 glBindBufferRange()，或者如果 uniform 块是全部使用缓存来存储的，那么可以
使用 glBindBufferBase()。
void glBindBufferRange(GLenum target, GLuint index, GLuint buffer, GLintptr offset,
GLsizeiptr size);
void glBindBufferBase(GLenum target, GLuint index, GLuint buffer);
将缓存对象 buffer 与索引为 index 的命名 uniform 块关联起来。 target 可以是
GL_UNIFORM_BUFFER（对于 uniform 块）或者 GL_TRANSFORM_FEEDBACK_BUFFER。
index 是 uniform 块的索引。 offset 和 size
分别指定了 uniform 缓存映射的起始索引和大小。
调 用 glBindBufferBase() 等 价 于 调 用 glBindBufferRange() 并 设 置 offset 为 0，
size 为缓存对象的大小。
在下列情况下调用这两个函数可能会产生 OpenGL 错误 GL_INVALID_VALUE ： size
小于 0 ； offset + size 大于缓存大小； offset 或 size 不是 4 的倍数； index 小于 0
或者大于等于 GL_MAX_UNIFORM_BUFFER_BINDINGS 的返回值。
当建立了命名 uniform 块和缓存对象之间的关联之后，只要使用缓存相关的命令即可对
块内的数据进行初始化或者修改。
我们也可以直接设置某个命名 uniform 块和缓存对象之间的绑定关系，也就是说，不
使用链接器内部自动绑定块对象并且查询关联结果的方式。如果多个着色器程序需要共享
同一个 uniform 块，那么你可能需要用到这种方法。这样可以避免对于不同的着色器程序
同一个块有不同的索引号。如果需要显式地控制一个 uniform 块的绑定方式，可以在调用
glLinkProgram() 之前调用 glUniformBlockBinding() 函数。
GLint glUniformBlockBinding(GLuint program, GLuint uniformBlockIndex, GLuint
uniformBlockBinding);
显式地将块 uniformBlockIndex 绑定到 uniformBlockBinding。
在一个命名的 uniform 块中， uniform 变量的布局是通过各种布局限制符在编译和链
接时控制的。如果使用了默认的布局方式，那么需要判断每个变量在 uniform 块中的偏移
量和数据存储大小。为此，需要调用两个命令： glGetUniformIndices() 负责获取指定名称
uniform 变量的索引位置，而 glGetActiveUniformsiv() 可以获得指定索引位置的偏移量和大
小。
void glGetUniformIndices(GLuint program, GLsizei uniformCount, const char**
uniformNames, GLuint* uniformIndices);
返 回 所 有 uniformCount 个 uniform 变 量 的 索 引 位 置， 变 量 的 名 称 通 过 字 符 串
数 组 uniformNames 来 指 定， 程 序 返 回 值 保 存 在 数 组 uniformIndices 当 中。 在
uniformNames 中 的 每 个 名 称 都 是 以 nullptr 来 结 尾 的， 并 且 uniformNames 和
uniformIndices 的数组元素数都应该是 uniformCount 个。如果在 uniformNames
中给出的某个名称不是当前启用的 uniform 变量名称，那么 uniformIndices 中对应的
位置将会记录为 GL_INVALID_INDEX。

buffer块：
GLSL中的 buffer 块，或者对于应用程序而言，就是着色器的存储缓存对象（shader
storage buffer object），它的行为类似 uniform 块。不过两者之间有两个决定性的差别，使得
buffer 块的功能更为强大。首先，着色器可以写入 buffer 块，修改其中的内容并呈现给其他
的着色器调用或者应用程序本身。其次，可以在渲染之前再决定它的大小，而不是编译和链接的时候。

GLuint glCreateShader(GLenum type);
分配一个着色器对象。 type必须是GL_VERTEX_SHADER、GL_FRAGMENT_SHADER、
GL_TESS_CONTROL_SHADER、GL_TESS_EVALUATION_SHADER 或 者
GL_GEOMETRY_SHADER 中的一个。返回值可能是一个非零的整数值，如果为 0 则说
明发生了错误。

void glShaderSource(GLuint shader, GLsizei count, const GLchar** string, const
GLint* length);
将着色器源代码关联到一个着色器对象 shader 上。 string 是一个由 count 行 GLchar
类型的字符串组成的数组，用来表示着色器的源代码数据。 string 中的字符串可以是
nullptr 结尾的，也可以不是。而 length 可以是以下三种值的一种。如果 length 是 nullptr，
那么我们假设 string 给出的每行字符串都是 nullptr 结尾的。否则， length 中必须有 count
个元素，它们分别表示 string 中对应行的长度。如果 length 数组中的某个值是一个整数，
那么它表示对应的字符串中的字符数。如果某个值是负数，那么 string 中的对应行假设
为 nullptr 结尾。

void glCompileShader(GLuint shader);
编 译 着 色 器 的 源 代 码。 结 果 查 询 可 以 调 用 glGetShaderiv()， 并 且 参 数 为 GL_
COMPILE_STATUS。

void glGetShaderInfoLog(GLuint shader, GLsizei bufSize, GLsizei* length, char*
infoLog);
返回 shader 的最后编译结果。返回的日志信息是一个以 nullptr 结尾的字符串，它保
存在 infoLog 缓存中，长度为 length 个字符串。日志可以返回的最大值是通过 bufSize 来
定义的。
如果 length 设置为 nullptr，那么将不会返回 infoLog 的大小。

GLuint glCreateProgram(void);
创建一个空的着色器程序。返回值是一个非零的整数，如果为 0 则说明发生了错误。

void glAttachShader(GLuint program, GLuint shader);
将着色器对象 shader 关联到着色器程序 program 上。着色器对象可以在任何时候关
联到着色器程序，但是它的功能只有经过程序的成功链接之后才是可用的。着色器对象
可以同时关联到多个不同的着色器程序上。

void glDetachShader(GLuint program, GLuint shader);
移除着色器对象 shader 与着色器程序 program 的关联。如果着色器已经被标记为要
删除的对象（调用 glDeleteShader()），然后又被解除了关联，那么它将会被即时删除。

void glLinkProgram(GLuint program);
处理所有与 program 关联的着色器对象来生成一个完整的着色器程序。链接操作
的结果查询可以调用 glGetProgramiv()，且参数为 GL_LINK_STATUS。如果返回 GL_
TRUE，那么链接成功；否则，返回 GL_FALSE。

void glGetProgramInfoLog(GLuint program, GLsizei bufSize, GLsizei* length, char*
infoLog);
返回最后一次 program 链接的日志信息。日志返回的字符串以 nullptr 结尾，长度
为 length 个字符，保存在 infoLog 缓存中。 log 可返回的最大值通过 bufSize 指定。如果
length 为 nullptr，那么不会再返回 infoLog 的长度。

void glUseProgram(GLuint program);
使用链接过的着色器程序 program。如果 program 为零，那么所有当前使用的着色器
都会被清除。如果没有绑定任何着色器，那么 OpenGL 的操作结果是未定义的，但是不
会产生错误。
如果已经启用了一个程序，而它需要关联新的着色器对象，或者解除之前关联的对
象，那么我们需要重新对它进行链接。如果链接过程成功，那么新的程序会直接替代之
前启用的程序。如果链接失败，那么当前绑定的着色器程序依然是可用的，不会被替代，
直到我们成功地重新链接或者使用 glUseProgram() 指定了新的程序为止。

void glDeleteShader(GLuint shader);
删除着色器对象 shader。如果 shader 当前已经链接到一个或者多个激活的着色器程
序上，那么它将被标识为“可删除”，当对应的着色器程序不再使用的时候，就会自动删
除这个对象。

void glDeleteProgram(GLuint program);
立即删除一个当前没有在任何环境中使用的着色器程序 program，如果程序正在被某
个环境使用，那么等到它空闲时再删除。

GLboolean glIsShader(GLuint shader);
如果 shader 是一个通过 glCreateShader() 生成的着色器对象的名称，并且没有被删
除，那么返回 GL_TRUE。如果 shader 是零或者不是着色器对象名称的非零值，则返回
GL_FALSE。

GLboolean glIsProgram(GLuint program);
如果 program 是一个通过 glCreateProgram() 生成的程序对象的名称，并且没有被
删除，那么返回 GL_TRUE。如果 program 是 0 或者不是着色器程序名称的非零值，则返
回 GL_FALSE。

GLint glGetSubroutineUniformLocation(GLuint program, GLenum shadertype, const
char* name);
返回名为 name 的子程序 uniform 的位置，相应的着色阶段通过 shadertype 来指
定。 name 是 一 个 以 nullptr 结 尾 的 字 符 串， 而 shadertype 的 值 必 须 是 GL_VERTEX_
SHADER、 GL_TESS_CONTROL_SHADER、 GL_TESS_EVALUATION_SHADER、 GL_
GEOMETRY_SHADER 或者 GL_FRAGMENT_SHADER 中的一个。
如果 name 不是一个激活的子程序 uniform，则返回 –1。如果 program 不是一个可用
的着色器程序，那么会生成一个 GL_INVALID_OPERATION 错误。

GLuint glGetSubroutineIndex(GLuint program, GLenum shadertype, const char* name);
从程序 program 中返回 name 所对应的着色器函数的索引，相应的着色阶段通过
shadertype 来 指 定。 name 是 一 个 以 nullptr 结 尾 的 字 符 串， 而 shadertype 的 值 必 须 是
GL_VERTEX_SHADER、 GL_TESS_CONTROL_SHADER、 GL_TESS_EVALUATION_
SHADER、 GL_GEOMETRY_SHADER 或者 GL_FRAGMENT_SHADER 中的一个。
如果 name 不是 shadertype 着色器的一个活动子程序，那么会返回 GGL_INVALID_INDEX。

GLuint glUniformSubroutinesuiv(GLenum shadertype, GLsizei count, const GLuint*
indices);
设 置 所 有 count 个 着 色 器 子 程 序 uniform 使 用 indices 数 组 中 的 值， 相 应 的 着 色
阶 段 通 过 shadertype 来 指 定。 shadertype 的 值 必 须 是 GL_VERTEX_SHADER、 GL_
TESS_CONTROL_SHADER、 GL_TESS_EVALUATION_SHADER、 GL_GEOMETRY_
SHADER 或 者 GL_FRAGMENT_SHADER 中 的 一 个。 第 i 个 子 程 序 uniform 对 应 于
indices[i] 的值。
如 果 count 不 等 于 当 前 绑 定 程 序 的 着 色 阶 段 shadertype 的 GL_ACTIVE_
SUBROUTINE_UNIFORM_LOCATIONS 值，那么会产生一个 GL_INVALID_VALUE 错
误。 indices 中的所有值都必须小于 GL_ACTIVE_SUBROUTINES，否则会产生一个 GL_
INVALID_VALUE 错误。

void glProgramUniform{1234}{fdi ui}(GLuint program, GLint location, TYPE value);
void glProgramUniform{1234}{fdi ui}v(GLuint program, GLint location, GLsizei
count, const TYPE* values);
void glProgramUniformMatrix{234}{fd}v(GLuint program, GLint location, GLsizei
count, GLboolean transpose, const GLfloat* values);
void glProgramUniformMatrix{2x3,2x4,3x2,3x4,4x2,4x3}{fd}v( GLuint program,
GLint location, GLsizei count, GLboolean transpose, const GLfloat* values);
glProgramUniform*() 和 glProgramUniformMatrix*() 函数的使用与 glUniform*() 和
glUniformMatrix*() 的使用是一样的，唯一的区别是使用一个 program 参数来设置准备更
新 uniform 变量的着色器程序。这些函数的主要优点是， program 可以不是当前绑定的程
序（即最后一个使用 glUseProgram() 指定的着色器程序）。

void glPointSize(GLfloat size);
设置固定的像素大小，如果没有开启 GL_PROGRAM_POINT_SIZE，那么它将被用
于设置点的大小。

void glLineWidth(GLfloat width);
设置线段的固定宽度。默认值为 1.0。这里的 width 表示线段的宽度，它必须是一个
大于 0.0 的值，否则会产生一个错误信息。

void glPolygonMode(GLenum face, GLenum mode);
控制多边形的正面与背面绘制模式。参数 face 必须是 GL_FRONT_AND_BACK，
而 mode 可以是 GL_POINT、 GL_LINE 或者 GL_FILL，它们分别设置多边形的绘制
模式是点集、轮廓线还是填充模式。默认情况下，正面和背面的绘制都使用填充模式
来完成。

void glFrontFace(GLenum mode);
控制多边形正面的判断方式。默认模式为 GL_CCW，即多边形投影到窗口坐标系之
后，顶点按照逆时针排序的面作为正面。如果模式为 GL_CW，那么采用顺时针方向的面
将被认为是物体的正面。

void glCullFace(GLenum mode);
在转换到屏幕空间渲染之前，设置需要抛弃（裁减）哪一类多边形。 mode 可以是
GL_FRONT、 GL_BACK 或者 GL_FRONT_AND_BACK，分别表示正面、背面或者所有
多边形。要使命令生效，我们还需要使用 glEnable() 和 GL_CULL_FACE 参数来开启裁
减；之后也可以使用 glDisable() 和同样的参数来关闭它。

void glGenBuffers(GLsizei n, GLuint* buffers);
返回 n 个当前未使用的缓存对象名称，并保存到 buffers 数组中。

缓存绑定的目标：
目标															用途
GL_ARRAY_BUFFER										这个结合点可以用来保存 glVertexAttribPointer() 设置的顶点数组数据。在实际工程中这一目标可能是最为常用的
GL_COPY_READ_BUFFER 和 GL_COPY_
WRITE_BUFFER											这两个目标是一对互相匹配的结合点，用于拷贝缓存之间的数据，并且不会引起 OpenGL 的状态变化，也不会产生任何特殊形式的 OpenGL 调用
GL_DRAW_INDIRECT_BUFFER						如果采取间接绘制（indirect drawing）的方法，那么这个缓存目标用于存储绘制命令的参数，详细的解释请参见下一节
GL_ELEMENT_ARRAY_BUFFER						绑定到这个目标的缓存中可以包含顶点索引数据，以便用于glDrawElements() 等索引形式的绘制命令
GL_PIXEL_PACK_BUFFER								这一缓存目标用于从图像对象中读取数据，例如纹理和帧缓存数据。相关的OpenGL命令包括glGetTexImage()和glReadPixels()等
GL_PIXEL_UNPACK_BUFFER							这一缓存目标与之前的 GL_PIXEL_PACK_BUFFER 相反，它可以作为 glTexImage2D() 等命令的数据源使用
GL_TEXTURE_BUFFER									纹理缓存也就是直接绑定到纹理对象的缓存，这样就可以直接在着色器中读取它们的数据信息。 GL_TEXTURE_BUFFER 可以提供一个操控此类缓存的目标，但是我们还需要将缓存关联到纹理，才能确保它们在着色器中可用
GL_TRANSFORM_FEEDBACK_BUFFER				transform feedback 是 OpenGL 提供的一种便捷方案，它可以在管线的顶点处理部分结束时（即经过了顶点着色，可能还有几何着色阶段），将经过变换的顶点重新捕获，并且将部分属性写入到缓存对象中。这一目标就提供了这样的结合点，可以建立专门的缓存来记录这些属性数据。
GL_UNIFORM_BUFFER									这个目标可以用于创建uniform缓存对象（uniform buffer object）的缓存数据

void glBindBuffer(GLenum target, GLuint buffer);
将 名 称 为 buffer 的 缓 存 对 象 绑 定 到 target 所 指 定 的 缓 存 结 合 点。 target 必 须 是
OpenGL 支持的缓存绑定目标之一， buffer 必须是通过 glGenBuffers() 分配的名称。如果
buffer 是第一次被绑定，那么它所对应的缓存对象也将同时被创建。

void glBufferData(GLenum target, GLsizeiptr size, const GLvoid* data, GLenum usage);
为绑定到 target 的缓存对象分配 size 大小（单位为字节）的存储空间。如果参数 data
不是 nullptr，那么将使用 data 所在的内存区域的内容来初始化整个空间。 usage 允许应用
程序向 OpenGL 端发出一个提示，指示缓存中的数据可能具备一些特定的用途。

缓存用途标识符：
“分解”的标识符			意义
_STATIC_				数据存储内容只写入一次，然后多次使用
_DYNAMIC_			数据存储内容会被反复写入和反复使用
_STREAM_				数据存储内容只写入一次，然后也不会被频繁使用
_DRAW					数据存储内容由应用程序负责写入，并且作为 OpenGL 绘制和图像命令的数据源
_READ					数据存储内容通过 OpenGL 反馈的数据写入，然后在应用程序进行查询时返回这些数据
_COPY					数据存储内容通过 OpenGL 反馈的数据写入，并且作为 OpenGL 绘制和图像命令的数据源

void glBufferSubData(GLenum target, GLintptr offset, GLsizeiptr size, const GLvoid* data);
使用新的数据替换缓存对象中的部分数据。绑定到 target 的缓存对象要从 offset 字节
处开始需要使用地址为 data、大小为 size 的数据块来进行更新。如果 offset 和 size 的总
和超出了缓存对象绑定数据的范围，那么将产生一个错误。

void glClearBufferData(GLenum target, GLenum internalformat, GLenum format,
GLenum type, const void* data);
void glClearBufferSubData(GLenum target, GLenum internalformat, GLintptr offset,
GLintptr size, GLenum format, GLenum type, const void* data);
清除缓存对象中所有或者部分数据。绑定到 target 的缓存存储空间将使用 data 中存
储的数据进行填充。 format 和 type 分别指定了 data 对应数据的格式和类型。
首先将数据被转换到 internalformat 所指定的格式，然后填充缓存数据的指定区域范围。
对 于 glClearBufferData() 来 说， 整 个 区 域 都 会 被 指 定 的 数 据 所 填 充。 而 对 于
glClearBufferSubData() 来说，填充区域是通过 offset 和 size 来指定的，它们分别给出
了以字节为单位的起始偏移地址和大小。

void glCopyBufferSubData(GLenum readtarget, GLenum writetarget, GLintptr
readoffset, GLintprr writeoffset, GLsizeiptr size);
将绑定到 readtarget 的缓存对象的一部分存储数据拷贝到与 writetarget 相绑定的缓存对
象的数据区域上。 readtarget 对应的数据从 readoffset 位置开始复制 size 个字节，然后拷贝
到 writetarget 对应数据的 writeoffset 位置。如果 readoffset 或者 writeoffset 与 size 的和超出
了绑定的缓存对象的范围，那么 OpenGL 会产生一个 GL_INVALID_VALUE 错误。

void glGetBufferSubData(GLenum target, GLintptr offset, GLsizeiptr size, GLvoid* data);
返回当前绑定到 target 的缓存对象中的部分或者全部数据。起始数据的偏移字节位
置为 offset，回读的数据大小为 size 个字节，它们将从缓存的数据区域拷贝到 data 所指
向的内存区域中。如果缓存对象当前已经被映射，或者 offset 和 size 的和超出了缓存对
象数据区域的范围，那么将提示一个错误。

void* glMapBuffer(GLenum target, GLenum access);
将当前绑定到 target 的缓存对象的整个数据区域映射到客户端的地址空间中。之后
可以根据给定的 access 策略，通过返回的指针对数据进行直接读或者写的操作。如果
OpenGL 无法将缓存对象的数据映射出来，那么 glMapBuffer() 将产生一个错误并且返回
nullptr。发生这种情况的原因可能是与系统相关的，比如可用的虚拟内存过低等。

glMapBuffer() 的访问模式
标识符 意义
GL_READ_ONLY 应用程序仅对 OpenGL 映射的内存区域执行读操作
GL_WRITE_ONLY 应用程序仅对 OpenGL 映射的内存区域执行写操作
GL_READ_WRITE 应用程序对 OpenGL 映射的内存区域可能执行读或者写的操作

GLboolean glUnmapBuffer(GLenum target);
解除 glMapBuffer() 创建的映射。如果对象数据的内容在映射过程中没有发生损坏，
那么 glUnmapBuffer() 将返回 GL_TRUE。发生损坏的原因通常与系统相关，例如屏幕模
式发生了改变，这会影响图形内存的可用性。这种情况下，函数的返回值为 GL_FALSE，
并且对应的数据内容是不可预测的。应用程序必须考虑到这种几率较低的情形，并且及
时对数据进行重新初始化。

void* glMapBufferRange(GLenum target, GLintptr offset, GLsizeiptr length, GLbitfi eld access);
将缓存对象数据的全部或者一部分映射到应用程序的地址空间中。 target 设置了缓存
对象当前绑定的目标。 offset 和 length 一起设置了准备映射的数据范围（单位为字节）。
access 是一个位域标识符，用于描述映射的模式。

glMapBufferRange() 中使用的标识符
标识符 意义
GL_MAP_INVALIDATE_RANGE_BIT
如果设置的话，给定的缓存区域内任何数据都可以被抛弃以及无
效化。如果给定区域范围内任何数据没有被随后重新写入的话，那
么它将变成未定义的数据。这个标识符无法与 GL_MAP_READ_
BIT 同时使用
GL_MAP_INVALIDATE_BUFFER_BIT
如果设置的话，缓存的整个内容都可以被抛弃和无效化，不再受到
区域范围的设置影响。所有映射范围之外的数据都会变成未定义的状
态，而如果范围内的数据没有被随后重新写入的话，那么它也会变成
未定义。这个标识符无法与 GL_MAP_READ_BIT 同时使用
GL_MAP_FLUSH_EXPLICIT_BIT
应 用 程 序 将 负 责 通 知 OpenGL 映 射 范 围 内 的 哪 个 部 分 包
含 了 可 用 数 据， 方 法 是 在 调 用 glUnmapBuffer() 之 前 调 用
glFlushMappedBufferRange()。如果缓存中较大范围内的数据
都会被映射，而并不是全部被应用程序写入的话，应当使用这
个标识符。这个位标识符必须与 GL_MAP_WRITE_BIT 结合使
用。如果 GL_MAP_FLUSH_EXPLICIT_BIT 没有定义的话，那么
glUnmapBuffer() 会自动刷新整个映射区域的内容
GL_MAP_UNSYNCHRONIZED_BIT
如果这个位标识符没有设置的话，那么 OpenGL 会等待所有正在
处理的缓存访问操作结束，然后再返回映射范围的内存。如果设置
了这个标识符，那么 OpenGL 将不会尝试进行这样的缓存同步操作

void glFlushMappedBufferRange(GLenum target, GLintptr offset, GLsizeiptr length);
通知 OpenGL，绑定到 target 的映射缓存中由 offset 和 length 所划分的区域已经发生
了修改，需要立即更新到缓存对象的数据区域中。

void glInvalidateBufferData(GLuint buffer);
void glInvalidateBufferSubData(GLuint buffer, GLintptr offset, GLsizeiptr length);
通知 OpenGL，应用程序已经完成对缓存对象中给定范围内容的操作，因此可以随时
根据实际情况抛弃数据。 glInvalidateBufferSubData() 会抛弃名称为 buffer 的缓存对象
中，从 offset 字节处开始共 length 字节的数据。 glInvalidateBufferData() 会直接抛弃整
个缓存的数据内容。

void glVertexAttribPointer(GLuint index, GLint size, GLenum type, GLboolean
normalized, GLsizei stride, const GLvoid* pointer);
设置顶点属性在 index 位置可访问的数据值。 pointer 的起始位置也就是数组中的
第一组数据值，它是以基本计算机单位（例如字节）度量的，由绑定到 GL_ARRAY_
BUFFER 目标的缓存对象中的地址偏移量确定的。 size 表示每个顶点中需要更新的元素
个数。 type 表示数组中每个元素的数据类型。 normalized 表示顶点数据是否需要在传递
到顶点数组之前进行归一化处理。 stride 表示数组中两个连续元素之间的偏移字节数。如
果 stride 为 0，那么在内存当中各个数据就是紧密贴合的。

glVertexAttribPointer() 的数据类型标识符
标识符 OpenGL 类型
GL_BYTE GLbyte（有符号 8 位整型）
GL_UNSIGNED_BYTE GLubyte（无符号 8 位整型）
GL_SHORT GLshort（有符号 16 位整型）
GL_UNSIGNED_SHORT GLushort（无符号 16 位整型）
GL_INT GLint（有符号 32 位整型）
GL_UNSIGNED_INT GLuint（无符号 32 位整型）
GL_FIXED GLfixed（有符号 16 位定点型）
GL_FLOAT GLfloat（32 位 IEEE 单精度浮点型）
GL_HALF_FLOAT GLhalf（16 位 S1E5M10 半精度浮点型）
GL_DOUBLE GLdouble（64 位 IEEE 双精度浮点型）
GL_INT_2_10_10_10_REV GLuint（压缩数据类型）
GL_UNSIGNED_INT_2_10_10_10_REV GLuint（压缩数据类型）

void glVertexAttribLPointer(GLuint index, GLint size, GLenum type, GLsizei stride,
const GLvoid* pointer);
与 glVertexAttribPointer() 类似，不过对于传入顶点着色器的 64 位的双精度浮点型
顶点属性来说， type 必须设置为 GL_DOUBLE。

void glVertexAttrib{1234}{fds}(GLuint index, TYPE values);
void glVertexAttrib{1234}{fds}v(GLuint index, const TYPE* values);
void glVertexAttrib4{bsifd ub us ui}v(GLuint index, const TYPE* values);
设置索引为 index 的顶点属性的静态值。如果函数名称末尾没有 v，那么最多可以指
定 4 个参数值，即 x、 y、 z、 w 参数。如果函数末尾有 v，那么最多有 4 个参数值是保存
在一个数组中传入的，它的地址通过 values 来指定，存储顺序依次为 x、 y、 z 和 w 分量。

void glVertexAttrib4Nub(GLuint index, GLubyte x, GLubyte y, GLubyte z, GLubyte w);
void glVertexAttrib4N{bsi ub us ui}v(GLuint index, const TYPE* v);
设置属性 index 所对应的一个或者多个顶点属性值，并且在转换过程中将无符号参数
归一化到 [0, 1] 的范围，将有符号参数归一化到 [-1, 1] 的范围。

void glVertexAttribI{1234}{i ui}(GLuint index, TYPE values);
void glVertexAttribI{123}{i ui}v(GLuint index, const TYPE* values);
void glVertexAttribI4{bsi ub us ui}v(GLuint index, const TYPE* values);
设置一个或者多个静态整型顶点属性值，以用于 index 位置的整型顶点属性。

void glVertexAttribL{1234}(GLuint index, TYPE values);
void glVertexAttribL{1234}v(GLuint index, const TYPE* values);
设置一个或者多个静态顶点属性值，以用于 index 位置的双精度顶点属性。

void glDrawArrays(GLenum mode, GLint first, GLsizei count);
使用数组元素建立连续的几何图元序列，每个启用的数组中起始位置为 first，结束
位置为 first + count–1。 mode 表示构建图元的类型，它必须是 GL_TRIANGLES、 GL_
LINE_LOOP、 GL_LINES、 GL_POINTS 等类型标识符之一。

void glDrawElements(GLenum mode, GLsizei count, GLenum type, const GLvoid* indices);
使用 count 个元素来定义一系列几何图元，而元素的索引值保存在一个绑定到 GL_
ELEMENT_ARRAY_BUFFER 的缓存中（元素数组缓存， element array buffer）。 indices 定义
了元素数组缓存中的偏移地址，也就是索引数据开始的位置，单位为字节。 type 必须是 GL_
UNSIGNED_BYTE、 GL_UNSIGNED_SHORT 或者 GL_UNSIGNED_INT 中的一个，它给出
了元素数组缓存中索引数据的类型。 mode 定义了图元构建的方式，它必须是图元类型标识
符中的一个，例如 GL_TRIANGLES、 GL_LINE_LOOP、 GL_LINES 或者 GL_POINTS。

void glDrawElementsBaseVertex(GLenum mode, GLsizei count, GLenum type, const
GLvoid* indices, GLint basevertex);
本质上与 glDrawElements() 并无区别，但是它的第 i 个元素在传入绘制命令时，实
际上读取的是各个顶点属性数组中的第 indices[i] + basevertex 个元素。

void glDrawRangeElements(GLenum mode, GLuint start, GLuint end, GLsizei count,
GLenum type, const GLvoid* indices);
这是 glDrawElements() 的一种更严格的形式，它实际上相当于应用程序（也就是开
发者）与 OpenGL 之间形成的一种约定，即元素数组缓存中所包含的任何一个索引值（来
自 indices）都会落入到 start 和 end 所定义的范围当中。

void glDrawRangeElementsBaseVertex(GLenum mode, GLuint start, GLuint end,
GLsizei count, GLenum type, const GLvoid* indices, GLint basevertex);
同应用程序之间建立一种约束，其形式与 glDrawRangeElements() 类似，不过它同
时也支持使用 basevertex 来设置顶点索引的基数。在这里，这个函数将首先检查元素数
组缓存中保存的数据是否落入 start 和 end 之间，然后再对其添加 basevertex 基数。

void glDrawArraysIndirect(GLenum mode, const GLvoid* indirect);
特性与 glDrawArraysInstanced() 完全一致，不过绘制命令的参数是从绑定到 GL_
DRAW_INDIRECT_BUFFER 的缓存（间接绘制缓存， draw indirect buffer）中获取的结
构体数据。 indirect 记录间接绘制缓存中的偏移地址。 mode 必须是 glDrawArrays() 所支
持的某个图元类型

void glMultiDrawArrays(GLenum mode, const GLint* first, const GLint* count,
GLsizei primcount);
在一个 OpenGL 函数调用过程中绘制多组几何图元集。first 和 count 都是数组的形式，
数组的每个元素都相当于一次 glDrawArrays() 调用，元素的总数由 primcount 决定。

void glMultiDrawElements(GLenum mode, const GLint* count, GLenum type, const
GLvoid* const* indices, GLsizei primcount);
在一个 OpenGL 函数调用过程中绘制多组几何图元集。 fi rst 和 indices 都是数组的形式，
数组的每个元素都相当于一次 glDrawElements() 调用，元素的总数由 primcount 决定。

void glMultiDrawElementsBaseVertex(GLenum mode, const GLint* count, GLenum
type, const GLvoid* const* indices, GLsizei primcount, const GLint* baseVertex);
在一个 OpenGL 函数调用过程中绘制多组几何图元集。 first、 indices 和 baseVertex
都是数组的形式，数组的每个元素都相当于一次 glDrawElementsBaseVertex() 调用，
元素的总数由 primcount 决定。

void glMultiDrawArraysIndirect(GLenum mode, const void* indirect, GLsizei
drawcount, GLsizei stride);
绘制多组图元集，相关参数全部保存到缓存对象中。在 glMultiDrawArraysIndirect()
的 一 次 调 用 当 中， 可 以 分 发 总 共 drawcount 个 独 立 的 绘 制 命 令， 命 令 中 的 参 数 与
glDrawArraysIndirect() 所用的参数是一致的。每个 DrawArraysIndirectCommand 结构体
之间的间隔都是 stride 个字节。如果 stride 是 0 的话，那么所有的数据结构体将构成一个
紧密排列的数组。

void glMultiDrawElementsIndirect(GLenum mode, GLenum type, const void* indirect,
GLsizei drawcount, GLsizei stride);
绘制多组图元集，相关参数全部保存到缓存对象中。在 glMultiDrawElementsIndirect()
的 一 次 调 用 当 中， 可 以 分 发 总 共 drawcount 个 独 立 的 绘 制 命 令， 命 令 中 的 参 数 与
glDrawElementsIndirect() 所用的参数是一致的。每个 DrawElementsIndirectCommand
结构体之间的间隔都是 stride 个字节。如果 stride 是 0 的话，那么所有的数据结构体将构
成一个紧密排列的数组

void glPrimitiveRestartIndex(GLuint index);
设置一个顶点数组元素的索引值，用来指定渲染过程中，从什么地方启动新的图元
绘制。如果在处理定点数组元素索引的过程中遇到了一个符合该索引的数值，那么系统
不会处理它对应的顶点数据，而是终止当前的图元绘制，并且从下一个顶点重新开始渲
染同一类型的图元集合。

void glDrawArraysInstanced(GLenum mode, GLint first, GLsizei count, GLsizei
primCount);
通过 mode、 first 和 count 所构成的几何体图元集（相当于 glDrawArrays() 函数所需
的独立参数），绘制它的 primCount 个实例。对于每个实例，内置变量 gl_InstanceID 都会
依次递增，新的数值会被传递到顶点着色器，以区分不同实例的顶点属性。

void glDrawElementsInstanced(GLenum mode, GLsizei count, GLenum type, const
void* indices, GLsizei primCount);
通过 mode、 count 和 indices 所构成的几何体图元集（相当于 glDrawElements() 函
数所需的独立参数），绘制它的 primCount 个实例。与 glDrawArraysInstanced() 类似，
对于每个实例，内置变量 gl_InstanceID 都会依次递增，新的数值会被传递到顶点着色
器，以区分不同实例的顶点属性。

void glDrawElementsInstancedBaseVertex(GLenum mode, GLsizei count, GLenum
type, const void* indices, GLsizei instanceCount, GLuint baseVertex);
通 过 mode、 count、 indices 和 baseVertex 所 构 成 的 几 何 体 图 元 集（相 当 于
glDrawElementsBaseVertex() 函数所需的独立参数），绘制它的 instanceCount 个实例。
与 glDrawArraysInstanced() 类似，对于每个实例，内置变量 gl_InstanceID 都会依次递
增，新的数值会被传递到顶点着色器，以区分不同实例的顶点属性。

void glVertexAttribDivisor(GLuint index, GLuint divisor);
设置多实例渲染时，位于 index 位置的顶点着色器中顶点属性是如何分配值到每个实
例的。 divisor 的值如果是 0 的话，那么该属性的多实例特性将被禁用，而其他的值则表
示顶点着色器，每 divisor 个实例都会分配一个新的属性值。

void glDrawArraysInstancedBaseInstance(GLenum mode, GLint first, GLsizei
count, GLsizei instanceCount, GLuint baseInstance);
对于通过 mode、 first 和 count 所构成的几何体图元集（相当于 glDrawArrays() 函数
所需的独立参数），绘制它的 primCount 个实例。对于每个实例，内置变量 gl_InstanceID
都会依次递增，新的数值会被传递到顶点着色器，以区分不同实例的顶点属性。此外，
baseInstance 的值用来对实例化的顶点属性设置一个索引的偏移值，从而改变 OpenGL 取
出的索引位置。

void glDrawElementsInstancedBaseInstance(GLenum mode, GLsizei count,
GLenum type, const GLvoid* indices, GLsizei instanceCount, GLuint baseInstance);
对于通过 mode、 count 和 indices 所构成的几何体图元集（相当于 glDrawElements()
函数所需的独立参数），绘制它的 primCount 个实例。与 glDrawArraysInstanced() 类似，
对于每个实例，内置变量 gl_InstanceID 都会依次递增，新的数值会被传递到顶点着色
器，以区分不同实例的顶点属性。此外， baseInstance 的值用来对实例化的顶点属性设置
一个索引的偏移值，从而改变 OpenGL 取出的索引位置。

void glDrawElementsInstancedBaseVertexBaseInstance(GLenum mode, GLsizei count,
GLenum type, const GLvoid* indices, GLsizei instanceCount, GLuint baseVertex, GLuint baseInstance);
对 于 通 过 mode、 count、 indices 和 baseVertex 所 构 成 的 几 何 体 图 元 集（相 当 于
glDrawElementsBaseVertex() 函数所需的独立参数），绘制它的 primCount 个实例。与
glDrawArraysInstanced() 类似，对于每个实例，内置变量 gl_InstanceID 都会依次递增，
新的数值会被传递到顶点着色器，以区分不同实例的顶点属性。此外， baseInstance 的值
用来对实例化的顶点属性设置一个索引的偏移值，从而改变 OpenGL 取出的索引位置。
*/

/*
2D + 透视（Perspective） = 3D

变换（Transformation）

光栅化（Rasterization）

着色（Shading）

着色器（Shader）是在图形硬件上执行的单独程序。

纹理贴图（Texture Mapping）

实心3D几何体无非是将顶点间的点连接起来，然后对三角形进行光栅化而使对象变得有实体。
任何渲染场景都是对变换，着色，纹理和混合，这4种技术的灵活运用。

原点（Origin）

窗口是以像素为单位进行度量的。
开始在窗口中绘制点，线和形状之前，必须告诉OpenGL如何把指定的坐标对翻译为屏幕坐标。我们可以通过指定
“占据窗口的笛卡尔空间区域”完成这个任务，这个区域称为裁剪区域。

视口：把绘图坐标映射到窗口坐标。
裁剪区域的宽度和高度很少正好与窗口的宽度和高度（以像素为单位）相匹配。因此，坐标系统必须从逻辑笛卡尔
坐标映射到物理屏幕像素坐标。这个映射是通过视口（ViewPort）来指定的。
视口就是窗口内部用于绘制裁剪区域的客户区域。

正投影（Orthographic Projection），使用这种投影时，需要指定一个正方形或长方形的视景体。

--------------------------------------------------------------------------------------------------

OpenGL被定义为“图形硬件的一种软件接口”。

OpenGL是一种过程性而不是描述性的图形API。

为了使OpenGL代码更易于从一个平台移植到另一个平台，OpenGL定义了数据类型。这些数据类型可以映射
到所有平台上的特定最小格式。

一种OpenGL实现常常包含两种方法来执行一个特定的任务，一种是快速的方法，在性能上稍作妥协。另一种
是慢速的方法，着重于改进视觉质量。glHint函数允许我们指定偏重于速度还是视觉质量。在OpenGL中，这是
唯一一个行为完全依赖生产商的函数。

渲染环境（Rendering Context），是一个运行中的OpenGL状态机的句柄。在任何OpenGL函数起作用之前
必须创建一个渲染环境。

在窗口创建时，认为改变了一次窗口的大小。

void glViewport(GLint x,GLint y,GLsizei width,GLsizei height);
其中x和y代表窗口中视口的左下角坐标，而宽度和高度是用像素表示的。通常x和y都设置为0。

r,g,b颜色分量可以是0到1之间任何有效的浮点值。OpenGL接受这个颜色值，并在内部把它转换为能够与可用
的视频硬件准确匹配的最接近颜色。

--------------------------------------------------------------------------------------------------

客户端：CPU和内存
服务端：GPU和显存
二者是异步的。

有3中向OpenGL着色器传递数据的方法：
1，属性。
2，uniform值。
3，纹理。

uniform变量在每个批次改变一次，而不是每个顶点改变一次。

我们通常在2D绘图中使用正投影，并在我们的几何图形中将z坐标设为0.0。
在正投影中，所有位于视景体范围内的东西都会被显示在屏幕上，而不存在照相机或视点坐标系的概念。

OpenGL支持多达16种可以为每个顶点设置的不同类型参数。这些参数编号为从0到15，并且可以与顶点
着色器中的任何指定变量相关联。

在默认情况下，OpenGL认为具有逆时针方向环绕的多边形是正面。

GL_TRIANGLE_STRIP的优点：
1，节省大量的数据存储空间。
2，提供运算性能和节省带宽。更少的顶点意味着数据从内存传输到显卡的速度更快，并且顶点着色器需要进行
处理的次数也更少。

首先绘制那些离观察者较近的对象，然后再绘制那些较远的对象。

贴花（decaling）
想要绘制实心几何图形但又要突出它的边时。
z冲突(z-fighting)
确保只能沿着z轴向镜头方向移动，而且要移动得足够多以使深度测试产生偏移，同时又不能移动太多而导致
几何图层之间产生缝隙。但是，我们还有更好的办法：
void glPolygonOffset(GLfloat factor,GLfloat units);
glPolygonOffset函数使我们可以调节片段的深度值，这样就能使深度值产生偏移而并不实际改变3D空间中
的物理位置。
可以将两个参数都设置为-1.0。
除了使用glPolygonOffset设置偏移值之外，还必须启用多边形单独偏移来填充几何图形（GL_POLYGON_OFFSET_FILL），
线（GL_POLYGON_OFFSET_LINE）和点（GL_POLYGON_OFFSET_POINT）。

在正常情况下，任何绘制操作不是被完全丢弃，就是完全覆盖原来的颜色值，这取决于深度测试的结果。如果
打开了OpenGL的混合功能，那么下层的颜色值就不会被清除。

已经存储在颜色缓冲区中的颜色值叫做目标颜色。
作为当前渲染命令的结果进入颜色缓冲区的颜色值称为源颜色。

当混合功能被启用时，源颜色和目标颜色的组合方式是由混合方程式控制的。在默认情况下，混合方程式如下：
Color =(Color_src * factor_src) +(Color_des * factor_des)
其中两个混合因子是用下面这个函数进行设置的：
glBlendFunc(GLenum S,GLenum D);
注意：S和D都是枚举值，而不是可以直接指定的实际值。
举例：glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA)意思为：
(源颜色 * 源alpha) +(目标颜色 *(1 - 源alpha))
这个混合函数经常用于实现在其他一些不透明的物体前面绘制一个透明物体的效果。这种技巧需要首先绘制
一个或多个背景物体，然后再在上面绘制经过混合的透明物体。

OpenGL混合功能的另一个用途是抗锯齿。
开启抗锯齿：
glEnable(GL_BLEND);
glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
确保混合方程为GL_ADD，这也是默认的设置。
glEnable(GL_POINT_SMOOTH);
glEnable(GL_LINE_SMOOTH);
glEnable(GL_POLYGON_SMOOTH);

在使用GL_POLYGON_SMOOTH时应该注意，不要用它去处理实心物体的边缘抗锯齿，而应该去使用多重
采样（multisampling）的方法。

使用多重采样，那么已经包含了颜色，深度和模板的帧缓冲区就会添加一个额外的缓冲区。所有的图元在每个
像素上都进行了多次采样，其结果就存储在这个缓冲区中。

为了进行多重采样，首先必须获得一个支持多重采样帧缓冲区的渲染环境。

可以使用glEnable/glDisable组合（GL_MULTISAMPLE标记）来打开或关闭多重采样。

注意，当启用多重采样后，点，线和多边形的抗锯齿设置都将被忽略。

多重采样缓冲区在默认情况下使用片段的RGB值，并不包括颜色的alpha成分。

--------------------------------------------------------------------------------------------------

OpenGL使用以列为主（Column-Major）的矩阵约定。

前3列的前3个元素只是方向向量，它们表示空间中x轴，y轴和z轴上的方向。

--------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------
*/

/*
vertex array object-顶点数组对象
用于为环境状态描述内存中顶点数据的布局。使用glGenVertexArrays()来分配顶点数组对象的名字。

buffer object-缓存对象
用于引用一块GPU内存区域。所有的数据都存储在这GPU内存里。

缓存结合点
用于表达出缓存对象所引用的GPU内存的布局，涉及到GPU内存优化。至于这个所引用的GPU内存的大小，现在还不知道。

------------------------------------------------------------------------------------------------------------------

frame buffer-帧缓存
GPU管理的一块独立内存区域。像素就存储在这里。

NDC：
顶点着色器需求的就是NDC。
*/

/*
gen:
产生名字。
bind:
new对象，设置环境状态。
*/

/*
缓存对象名字->缓存对象->GPU内存

缓存对象名字，一个GLuint
缓存对象，通过第一次bind时分配
GPU内存，通过glBufferData()分配（如果之前的空间比较小，那么回重新分配，同样，也可能会缩小。这就更说明了缓存对象对于GPU内存的关系为引用）

最终目的：就是要把顶点数据从内存给拷贝到GPU内存中去，所以说GPU内存中最后存储的就是顶点数据。
*/

/*
创建“顶点数组对象”，用于管理将来位于“顶点缓存对象”里的“实际数据”。

创建“顶点数组对象”：
1，产生一个名字，相当于一个引用。																			//gen
2，new出这个对象，然后通过之前的名字来引用。这样便有了实实在在的“顶点数组对象”。			//bind

创建“顶点缓存对象”：
1，产生一个名字，相当于一个引用。																			//gen
2，new出这个对象，然后通过之前的名字来引用。这样便有了实实在在的“顶点缓存对象”。			//bind

实际的位于内存里的“顶点数据”-----实际的位于显存里的“顶点数据”：
通过glBufferData()在显存分配空间，然后把内存中的“顶点数据”给拷贝到显存。
这个显存的空间，通过“顶点缓存对象”VBO来表达。
实际的显存里的“顶点数据”，通过“顶点数组对象”VAO来管理。

VAO,VBO分别用于描述“顶点数据”在内存和显存的布局。只不过它们两个皆是以对象的形式出现，同时肩负维护状态机的任务。
*/

/*
Name
glGenBuffers — generate buffer object names
C Specification
void glGenBuffers(GLsizei n,GLuint * buffers);

Parameters
n
Specifies the number of buffer object names to be generated.

buffers
Specifies an array in which the generated buffer object names are stored.

Description
glGenBuffers returns n buffer object names in buffers. There is no guarantee that the names form a contiguous set of integers; however, it is 
guaranteed that none of the returned names was in use immediately before the call to glGenBuffers.

Buffer object names returned by a call to glGenBuffers are not returned by subsequent calls, unless they are first deleted with glDeleteBuffers.

No buffer objects are associated with the returned buffer object names until they are first bound by calling glBindBuffer.

Errors
GL_INVALID_VALUE is generated if n is negative.

Associated Gets
glIsBuffer

----------------------------------------------------------------------------------------------------------------------------------------------------------------

Name
glIsBuffer — determine if a name corresponds to a buffer object
C Specification
GLboolean glIsBuffer(GLuint buffer);

Parameters
buffer
Specifies a value that may be the name of a buffer object.

Description
glIsBuffer returns GL_TRUE if buffer is currently the name of a buffer object. If buffer is zero, or is a non-zero value that is not currently the name 
of a buffer object, or if an error occurs, glIsBuffer returns GL_FALSE.

A name returned by glGenBuffers, but not yet associated with a buffer object by calling glBindBuffer, is not the name of a buffer object.

----------------------------------------------------------------------------------------------------------------------------------------------------------------
Name
glDeleteBuffers — delete named buffer objects
C Specification
void glDeleteBuffers(GLsizei n,const GLuint * buffers);

Parameters
n
Specifies the number of buffer objects to be deleted.

buffers
Specifies an array of buffer objects to be deleted.

Description
glDeleteBuffers deletes n buffer objects named by the elements of the array buffers. After a buffer object is deleted, it has no contents, and its name 
is free for reuse(for example by glGenBuffers). If a buffer object that is currently bound is deleted, the binding reverts to 0(the absence of any 
buffer object).

glDeleteBuffers silently ignores 0's and names that do not correspond to existing buffer objects.

Errors
GL_INVALID_VALUE is generated if n is negative.

Associated Gets
glIsBuffer

----------------------------------------------------------------------------------------------------------------------------------------------------------------

Name
glBindBuffer — bind a named buffer object
C Specification
void glBindBuffer(GLenum target,GLuint buffer);

Parameters
target
Specifies the target to which the buffer object is bound, which must be one of the buffer binding targets in the following table:

Buffer Binding Target																								Purpose
GL_ARRAY_BUFFER																									Vertex attributes
GL_ATOMIC_COUNTER_BUFFER																					Atomic counter storage
GL_COPY_READ_BUFFER																							Buffer copy source
GL_COPY_WRITE_BUFFER	Buffer																				copy destination
GL_DISPATCH_INDIRECT_BUFFER																				Indirect compute dispatch commands
GL_DRAW_INDIRECT_BUFFER																					Indirect command arguments
GL_ELEMENT_ARRAY_BUFFER																					Vertex array indices
GL_PIXEL_PACK_BUFFER																							Pixel read target
GL_PIXEL_UNPACK_BUFFER																						Texture data source
GL_QUERY_BUFFER																									Query result buffer
GL_SHADER_STORAGE_BUFFER																				Read-write storage for shaders
GL_TEXTURE_BUFFER																								Texture data buffer
GL_TRANSFORM_FEEDBACK_BUFFER																			Transform feedback buffer
GL_UNIFORM_BUFFER																								Uniform block storage

buffer
Specifies the name of a buffer object.

Description
glBindBuffer binds a buffer object to the specified buffer binding point. Calling glBindBuffer with target set to one of the accepted symbolic 
constants and buffer set to the name of a buffer object binds that buffer object name to the target. If no buffer object with name buffer exists, one 
is created with that name. When a buffer object is bound to a target, the previous binding for that target is automatically broken.

Buffer object names are unsigned integers. The value zero is reserved, but there is no default buffer object for each buffer object target. Instead,
buffer set to zero effectively unbinds any buffer object previously bound, and restores client memory usage for that buffer object target(if 
supported for that target). Buffer object names and the corresponding buffer object contents are local to the shared object space of the current GL 
rendering context; two rendering contexts share buffer object names only if they explicitly enable sharing between contexts through the appropriate 
GL windows interfaces functions.

glGenBuffers must be used to generate a set of unused buffer object names.

The state of a buffer object immediately after it is first bound is an unmapped zero-sized memory buffer with GL_READ_WRITE access and 
GL_STATIC_DRAW usage.

While a non-zero buffer object name is bound, GL operations on the target to which it is bound affect the bound buffer object, and queries of the 
target to which it is bound return state from the bound buffer object. While buffer object name zero is bound, as in the initial state, attempts to 
modify or query state on the target to which it is bound generates an GL_INVALID_OPERATION error.

When a non-zero buffer object is bound to the GL_ARRAY_BUFFER target, the vertex array pointer parameter is interpreted as an offset within the 
buffer object measured in basic machine units.

When a non-zero buffer object is bound to the GL_DRAW_INDIRECT_BUFFER target, parameters for draws issued through glDrawArraysIndirect and 
glDrawElementsIndirect are sourced from the specified offset in that buffer object's data store.

When a non-zero buffer object is bound to the GL_DISPATCH_INDIRECT_BUFFER target, the parameters for compute dispatches issued through 
glDispatchComputeIndirect are sourced from the specified offset in that buffer object's data store.

While a non-zero buffer object is bound to the GL_ELEMENT_ARRAY_BUFFER target, the indices parameter of glDrawElements,
glDrawElementsInstanced, glDrawElementsBaseVertex, glDrawRangeElements, glDrawRangeElementsBaseVertex, glMultiDrawElements, or 
glMultiDrawElementsBaseVertex is interpreted as an offset within the buffer object measured in basic machine units.

While a non-zero buffer object is bound to the GL_PIXEL_PACK_BUFFER target, the following commands are affected: glGetCompressedTexImage,
glGetTexImage, and glReadPixels. The pointer parameter is interpreted as an offset within the buffer object measured in basic machine units.

While a non-zero buffer object is bound to the GL_PIXEL_UNPACK_BUFFER target, the following commands are affected: glCompressedTexImage1D,
glCompressedTexImage2D, glCompressedTexImage3D, glCompressedTexSubImage1D, glCompressedTexSubImage2D, glCompressedTexSubImage3D,
glTexImage1D, glTexImage2D, glTexImage3D, glTexSubImage1D, glTexSubImage2D, and glTexSubImage3D. The pointer parameter is interpreted as 
an offset within the buffer object measured in basic machine units.

The buffer targets GL_COPY_READ_BUFFER and GL_COPY_WRITE_BUFFER are provided to allow glCopyBufferSubData to be used without disturbing 
the state of other bindings. However, glCopyBufferSubData may be used with any pair of buffer binding points.

The GL_TRANSFORM_FEEDBACK_BUFFER buffer binding point may be passed to glBindBuffer, but will not directly affect transform feedback state.
Instead, the indexed GL_TRANSFORM_FEEDBACK_BUFFER bindings must be used through a call to glBindBufferBase or glBindBufferRange. This will 
affect the generic GL_TRANSFORM_FEEDBACK_BUFFER binding.

Likewise, the GL_UNIFORM_BUFFER, GL_ATOMIC_COUNTER_BUFFER and GL_SHADER_STORAGE_BUFFER buffer binding points may be used, but do 
not directly affect uniform buffer, atomic counter buffer or shader storage buffer state, respectively. glBindBufferBase or glBindBufferRange must be 
used to bind a buffer to an indexed uniform buffer, atomic counter buffer or shader storage buffer binding point.

The GL_QUERY_BUFFER binding point is used to specify a buffer object that is to receive the results of query objects through calls to the 
glGetQueryObject family of commands.

A buffer object binding created with glBindBuffer remains active until a different buffer object name is bound to the same target, or until the bound 
buffer object is deleted with glDeleteBuffers.

Once created, a named buffer object may be re-bound to any target as often as needed. However, the GL implementation may make choices about 
how to optimize the storage of a buffer object based on its initial binding target.

Notes
The GL_COPY_READ_BUFFER, GL_UNIFORM_BUFFER and GL_TEXTURE_BUFFER targets are available only if the GL version is 3.1 or greater.

The GL_ATOMIC_COUNTER_BUFFER target is available only if the GL version is 4.2 or greater.

The GL_DISPATCH_INDIRECT_BUFFER and GL_SHADER_STORAGE_BUFFER targets are available only if the GL version is 4.3 or greater.

The GL_QUERY_BUFFER target is available only if the GL version is 4.4 or greater.

Errors
GL_INVALID_ENUM is generated if target is not one of the allowable values.

GL_INVALID_VALUE is generated if buffer is not a name previously returned from a call to glGenBuffers.

----------------------------------------------------------------------------------------------------------------------------------------------------------------

Name
glBufferData, glNamedBufferData — creates and initializes a buffer object's data store
C Specification
void glBufferData(GLenum target,GLsizeiptr size,const GLvoid * data,GLenum usage);

void glNamedBufferData(GLuint buffer,GLsizei size,const void *data,GLenum usage);

Parameters
target
Specifies the target to which the buffer object is bound for glBufferData, which must be one of the buffer binding targets in the following table:

buffer
Specifies the name of the buffer object for glNamedBufferData function.

size
Specifies the size in bytes of the buffer object's new data store.

data
Specifies a pointer to data that will be copied into the data store for initialization, or NULL if no data is to be copied.

usage
Specifies the expected usage pattern of the data store. The symbolic constant must be GL_STREAM_DRAW, GL_STREAM_READ, GL_STREAM_COPY,
GL_STATIC_DRAW, GL_STATIC_READ, GL_STATIC_COPY, GL_DYNAMIC_DRAW, GL_DYNAMIC_READ, or GL_DYNAMIC_COPY.

Description
glBufferData and glNamedBufferData create a new data store for a buffer object. In case of glBufferData, the buffer object currently bound to target 
is used. For glNamedBufferData, a buffer object associated with ID specified by the caller in buffer will be used instead.

While creating the new storage, any pre-existing data store is deleted. The new data store is created with the specified size in bytes and usage. If 
data is not NULL, the data store is initialized with data from this pointer. In its initial state, the new data store is not mapped, it has a NULL mapped 
pointer, and its mapped access is GL_READ_WRITE.

usage is a hint to the GL implementation as to how a buffer object's data store will be accessed. This enables the GL implementation to make more 
intelligent decisions that may significantly impact buffer object performance. It does not, however, constrain the actual usage of the data store.
usage can be broken down into two parts: first, the frequency of access(modification and usage), and second, the nature of that access. The 
frequency of access may be one of these:

STREAM
The data store contents will be modified once and used at most a few times.

STATIC
The data store contents will be modified once and used many times.

DYNAMIC
The data store contents will be modified repeatedly and used many times.

The nature of access may be one of these:

DRAW
The data store contents are modified by the application, and used as the source for GL drawing and image specification commands.

READ
The data store contents are modified by reading data from the GL, and used to return that data when queried by the application.

COPY
The data store contents are modified by reading data from the GL, and used as the source for GL drawing and image specification commands.

Notes
If data is NULL, a data store of the specified size is still created, but its contents remain uninitialized and thus undefined.

Clients must align data elements consistently with the requirements of the client platform, with an additional base-level requirement that an offset 
within a buffer to a datum comprising NN bytes be a multiple of NN.

The GL_ATOMIC_COUNTER_BUFFER target is available only if the GL version is 4.2 or greater.

The GL_DISPATCH_INDIRECT_BUFFER and GL_SHADER_STORAGE_BUFFER targets are available only if the GL version is 4.3 or greater.

The GL_QUERY_BUFFER target is available only if the GL version is 4.4 or greater.

Errors
GL_INVALID_ENUM is generated by glBufferData if target is not one of the accepted buffer targets.

GL_INVALID_ENUM is generated if usage is not GL_STREAM_DRAW, GL_STREAM_READ, GL_STREAM_COPY, GL_STATIC_DRAW, GL_STATIC_READ,
GL_STATIC_COPY, GL_DYNAMIC_DRAW, GL_DYNAMIC_READ, or GL_DYNAMIC_COPY.

GL_INVALID_VALUE is generated if size is negative.

GL_INVALID_OPERATION is generated by glBufferData if the reserved buffer object name 0 is bound to target.

GL_INVALID_OPERATION is generated by glNamedBufferData if buffer is not the name of an existing buffer object.

GL_INVALID_OPERATION is generated if the GL_BUFFER_IMMUTABLE_STORAGE flag of the buffer object is GL_TRUE.

GL_OUT_OF_MEMORY is generated if the GL is unable to create a data store with the specified size.

Associated Gets
glGetBufferSubData

glGetBufferParameter with argument GL_BUFFER_SIZE or GL_BUFFER_USAGE

----------------------------------------------------------------------------------------------------------------------------------------------------------------

Name
glBindBufferRange — bind a range within a buffer object to an indexed buffer target
C Specification
void glBindBufferRange(GLenumtarget,GLuintindex,GLuintbuffer,GLintptroffset,GLsizeiptrsize);

Parameters
target
Specify the target of the bind operation. target must be one of GL_ATOMIC_COUNTER_BUFFER, GL_TRANSFORM_FEEDBACK_BUFFER, 
GL_UNIFORM_BUFFER, or GL_SHADER_STORAGE_BUFFER.

index
Specify the index of the binding point within the array specified by target.

buffer
The name of a buffer object to bind to the specified binding point.

offset
The starting offset in basic machine units into the buffer object buffer.

size
The amount of data in machine units that can be read from the buffer object while used as an indexed target.

Description
glBindBufferRange binds a range the buffer object buffer represented by offset and size to the binding point at index index of the array of targets 
specified by target. Each target represents an indexed array of buffer binding points, as well as a single general binding point that can be used by 
other buffer manipulation functions such as glBindBuffer or glMapBuffer. In addition to binding a range of buffer to the indexed buffer binding target,
glBindBufferRange also binds the range to the generic buffer binding point specified by target.

offset specifies the offset in basic machine units into the buffer object buffer and size specifies the amount of data that can be read from the buffer 
object while used as an indexed target.

Notes
The GL_ATOMIC_COUNTER_BUFFER target is available only if the GL version is 4.2 or greater.

The GL_SHADER_STORAGE_BUFFER target is available only if the GL version is 4.3 or greater.

Errors
GL_INVALID_ENUM is generated if target is not one of GL_ATOMIC_COUNTER_BUFFER, GL_TRANSFORM_FEEDBACK_BUFFER, GL_UNIFORM_BUFFER 
or GL_SHADER_STORAGE_BUFFER.

GL_INVALID_VALUE is generated if index is greater than or equal to the number of target-specific indexed binding points.

GL_INVALID_VALUE is generated if size is less than or equal to zero, or if offset + size is greater than the value of GL_BUFFER_SIZE.

Additional errors may be generated if offset violates any target-specific alignmemt restrictions.

----------------------------------------------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------------------------------------------------------

Name
glCreateShader — Creates a shader object
C Specification
GLuint glCreateShader(GLenum shaderType);

Parameters
shaderType
Specifies the type of shader to be created. Must be one of GL_COMPUTE_SHADER, GL_VERTEX_SHADER, GL_TESS_CONTROL_SHADER,
GL_TESS_EVALUATION_SHADER, GL_GEOMETRY_SHADER, or GL_FRAGMENT_SHADER.

Description
glCreateShader creates an empty shader object and returns a non-zero value by which it can be referenced. A shader object is used to maintain the 
source code strings that define a shader. shaderType indicates the type of shader to be created. Five types of shader are supported. A shader of type GL_COMPUTE_SHADER is a shader that is intended to run on the programmable compute processor. A shader of type GL_VERTEX_SHADER is a shader that is intended to run on the programmable vertex processor. A shader of type GL_TESS_CONTROL_SHADER is a shader that is intended to run on the programmable tessellation processor in the control stage. A shader of type GL_TESS_EVALUATION_SHADER is a shader that is intended to run on the programmable tessellation processor in the evaluation stage. A shader of type GL_GEOMETRY_SHADER is a shader that is intended to run on the programmable geometry processor. A shader of type GL_FRAGMENT_SHADER is a shader that is intended to run on the programmable fragment processor.

When created, a shader object's GL_SHADER_TYPE parameter is set to either GL_COMPUTE_SHADER, GL_VERTEX_SHADER,
GL_TESS_CONTROL_SHADER, GL_TESS_EVALUATION_SHADER, GL_GEOMETRY_SHADER or GL_FRAGMENT_SHADER, depending on the value of 
shaderType.

Notes
Like buffer and texture objects, the name space for shader objects may be shared across a set of contexts, as long as the server sides of the 
contexts share the same address space. If the name space is shared across contexts, any attached objects and the data associated with those 
attached objects are shared as well.

Applications are responsible for providing the synchronization across API calls when objects are accessed from different execution threads.

GL_COMPUTE_SHADER is available only if the GL version is 4.3 or higher.

Errors
This function returns 0 if an error occurs creating the shader object.

GL_INVALID_ENUM is generated if shaderType is not an accepted value.

----------------------------------------------------------------------------------------------------------------------------------------------------------------

Name
glIsShader — Determines if a name corresponds to a shader object
C Specification
GLboolean glIsShader(GLuint shader);

Parameters
shader
Specifies a potential shader object.

Description
glIsShader returns GL_TRUE if shader is the name of a shader object previously created with glCreateShader and not yet deleted with glDeleteShader. 
If shader is zero or a non-zero value that is not the name of a shader object, or if an error occurs, glIsShader returns GL_FALSE.

Notes
No error is generated if shader is not a valid shader object name.

A shader object marked for deletion with glDeleteShader but still attached to a program object is still considered a shader object and glIsShader will 
return GL_TRUE.

----------------------------------------------------------------------------------------------------------------------------------------------------------------

Name
glCreateProgram — Creates a program object
C Specification
GLuint glCreateProgram(void);

Description
glCreateProgram creates an empty program object and returns a non-zero value by which it can be referenced. A program object is an object to 
which shader objects can be attached. This provides a mechanism to specify the shader objects that will be linked to create a program. It also 
provides a means for checking the compatibility of the shaders that will be used to create a program(for instance, checking the compatibility 
between a vertex shader and a fragment shader). When no longer needed as part of a program object, shader objects can be detached.

One or more executables are created in a program object by successfully attaching shader objects to it with glAttachShader, successfully compiling 
the shader objects with glCompileShader, and successfully linking the program object with glLinkProgram. These executables are made part of 
current state when glUseProgram is called. Program objects can be deleted by calling glDeleteProgram. The memory associated with the program 
object will be deleted when it is no longer part of current rendering state for any context.

Notes
Like buffer and texture objects, the name space for program objects may be shared across a set of contexts, as long as the server sides of the 
contexts share the same address space. If the name space is shared across contexts, any attached objects and the data associated with those 
attached objects are shared as well.

Applications are responsible for providing the synchronization across API calls when objects are accessed from different execution threads.

Errors
This function returns 0 if an error occurs creating the program object.

----------------------------------------------------------------------------------------------------------------------------------------------------------------

Name
glDeleteProgram — Deletes a program object
C Specification
void glDeleteProgram(GLuint program);

Parameters
program
Specifies the program object to be deleted.

Description
glDeleteProgram frees the memory and invalidates the name associated with the program object specified by program. This command effectively 
undoes the effects of a call to glCreateProgram.

If a program object is in use as part of current rendering state, it will be flagged for deletion, but it will not be deleted until it is no longer part of 
current state for any rendering context. If a program object to be deleted has shader objects attached to it, those shader objects will be 
automatically detached but not deleted unless they have already been flagged for deletion by a previous call to glDeleteShader. A value of 0 for 
program will be silently ignored.

To determine whether a program object has been flagged for deletion, call glGetProgram with arguments program and GL_DELETE_STATUS.

Errors
GL_INVALID_VALUE is generated if program is not a value generated by OpenGL.

----------------------------------------------------------------------------------------------------------------------------------------------------------------

Name
glShaderSource — Replaces the source code in a shader object
C Specification
void glShaderSource(GLuint shader,GLsizei count,const GLchar **string,const GLint *length);

Parameters
shader
Specifies the handle of the shader object whose source code is to be replaced.

count
Specifies the number of elements in the string and length arrays.

string
Specifies an array of pointers to strings containing the source code to be loaded into the shader.

length
Specifies an array of string lengths.

Description
glShaderSource sets the source code in shader to the source code in the array of strings specified by string. Any source code previously stored in 
the shader object is completely replaced. The number of strings in the array is specified by count. If length is NULL, each string is assumed to be 
null terminated. If length is a value other than NULL, it points to an array containing a string length for each of the corresponding elements of string.
Each element in the length array may contain the length of the corresponding string(the null character is not counted as part of the string length) or 
a value less than 0 to indicate that the string is null terminated. The source code strings are not scanned or parsed at this time; they are simply 
copied into the specified shader object.

Notes
OpenGL copies the shader source code strings when glShaderSource is called, so an application may free its copy of the source code strings 
immediately after the function returns.

Errors
GL_INVALID_VALUE is generated if shader is not a value generated by OpenGL.

GL_INVALID_OPERATION is generated if shader is not a shader object.

GL_INVALID_VALUE is generated if count is less than 0.

----------------------------------------------------------------------------------------------------------------------------------------------------------------

Name
glCompileShader — Compiles a shader object
C Specification
void glCompileShader(	GLuint shader);

Parameters
shader
Specifies the shader object to be compiled.

Description
glCompileShader compiles the source code strings that have been stored in the shader object specified by shader.

The compilation status will be stored as part of the shader object's state. This value will be set to GL_TRUE if the shader was compiled without 
errors and is ready for use, and GL_FALSE otherwise. It can be queried by calling glGetShader with arguments shader and GL_COMPILE_STATUS.

Compilation of a shader can fail for a number of reasons as specified by the OpenGL Shading Language Specification. Whether or not the 
compilation was successful, information about the compilation can be obtained from the shader object's information log by calling 
glGetShaderInfoLog.

Errors
GL_INVALID_VALUE is generated if shader is not a value generated by OpenGL.

GL_INVALID_OPERATION is generated if shader is not a shader object.

----------------------------------------------------------------------------------------------------------------------------------------------------------------

Name
glAttachShader — Attaches a shader object to a program object
C Specification
void glAttachShader(GLuint program,GLuint shader);

Parameters
program
Specifies the program object to which a shader object will be attached.

shader
Specifies the shader object that is to be attached.

Description
In order to create a complete shader program, there must be a way to specify the list of things that will be linked together. Program objects provide 
this mechanism. Shaders that are to be linked together in a program object must first be attached to that program object. glAttachShader attaches the 
shader object specified by shader to the program object specified by program. This indicates that shader will be included in link operations that will be 
performed on program.

All operations that can be performed on a shader object are valid whether or not the shader object is attached to a program object. It is permissible to 
attach a shader object to a program object before source code has been loaded into the shader object or before the shader object has been compiled. 
It is permissible to attach multiple shader objects of the same type because each may contain a portion of the complete shader. It is also permissible to 
attach a shader object to more than one program object. If a shader object is deleted while it is attached to a program object, it will be flagged for 
deletion, and deletion will not occur until glDetachShader is called to detach it from all program objects to which it is attached.

Errors
GL_INVALID_VALUE is generated if either program or shader is not a value generated by OpenGL.

GL_INVALID_OPERATION is generated if program is not a program object.

GL_INVALID_OPERATION is generated if shader is not a shader object.

GL_INVALID_OPERATION is generated if shader is already attached to program.

----------------------------------------------------------------------------------------------------------------------------------------------------------------

Name
glDetachShader — Detaches a shader object from a program object to which it is attached
C Specification
void glDetachShader(GLuint program,GLuint shader);

Parameters
program
Specifies the program object from which to detach the shader object.

shader
Specifies the shader object to be detached.

Description
glDetachShader detaches the shader object specified by shader from the program object specified by program. This command can be used to undo the 
effect of the command glAttachShader.

If shader has already been flagged for deletion by a call to glDeleteShader and it is not attached to any other program object, it will be deleted after it 
has been detached.

Errors
GL_INVALID_VALUE is generated if either program or shader is a value that was not generated by OpenGL.

GL_INVALID_OPERATION is generated if program is not a program object.

GL_INVALID_OPERATION is generated if shader is not a shader object.

GL_INVALID_OPERATION is generated if shader is not attached to program.

----------------------------------------------------------------------------------------------------------------------------------------------------------------

Name
glGetUniformBlockIndex — retrieve the index of a named uniform block
C Specification
GLuint glGetUniformBlockIndex(GLuint program,const GLchar *uniformBlockName);

Parameters
program
Specifies the name of a program containing the uniform block.

uniformBlockName
Specifies the address an array of characters to containing the name of the uniform block whose index to retrieve.

Description
glGetUniformBlockIndex retrieves the index of a uniform block within program.

program must be the name of a program object for which the command glLinkProgram must have been called in the past, although it is not required 
that glLinkProgram must have succeeded. The link could have failed because the number of active uniforms exceeded the limit.

uniformBlockName must contain a nul-terminated string specifying the name of the uniform block.

glGetUniformBlockIndex returns the uniform block index for the uniform block named uniformBlockName of program. If uniformBlockName does not 
identify an active uniform block of program, glGetUniformBlockIndex returns the special identifier, GL_INVALID_INDEX. Indices of the active uniform 
blocks of a program are assigned in consecutive order, beginning with zero.

Errors
GL_INVALID_OPERATION is generated if program is not the name of a program object for which glLinkProgram has been called in the past.

Notes
glGetUniformBlockIndex is available only if the GL version is 3.1 or greater.

----------------------------------------------------------------------------------------------------------------------------------------------------------------

Name
glGetActiveUniformBlock — query information about an active uniform block
C Specification
void glGetActiveUniformBlockiv(GLuint program,GLuint uniformBlockIndex,GLenum pname,GLint *params);

Parameters
program
Specifies the name of a program containing the uniform block.

uniformBlockIndex
Specifies the index of the uniform block within program.

pname
Specifies the name of the parameter to query.

params
Specifies the address of a variable to receive the result of the query.

Description
glGetActiveUniformBlockiv retrieves information about an active uniform block within program.

program must be the name of a program object for which the command glLinkProgram must have been called in the past, although it is not required 
that glLinkProgram must have succeeded. The link could have failed because the number of active uniforms exceeded the limit.

uniformBlockIndex is an active uniform block index of program, and must be less than the value of GL_ACTIVE_UNIFORM_BLOCKS.

Upon success, the uniform block parameter(s) specified by pname are returned in params. If an error occurs, nothing will be written to params.

If pname is GL_UNIFORM_BLOCK_BINDING, then the index of the uniform buffer binding point last selected by the uniform block specified by 
uniformBlockIndex for program is returned. If no uniform block has been previously specified, zero is returned.

If pname is GL_UNIFORM_BLOCK_DATA_SIZE, then the implementation-dependent minimum total buffer object size, in basic machine units, 
required to hold all active uniforms in the uniform block identified by uniformBlockIndex is returned. It is neither guaranteed nor expected that a 
given implementation will arrange uniform values as tightly packed in a buffer object. The exception to this is the std140 uniform block layout, 
which guarantees specific packing behavior and does not require the application to query for offsets and strides. In this case the minimum size may 
still be queried, even though it is determined in advance based only on the uniform block declaration.

If pname is GL_UNIFORM_BLOCK_NAME_LENGTH, then the total length(including the nul terminator) of the name of the uniform block identified by 
uniformBlockIndex is returned.

If pname is GL_UNIFORM_BLOCK_ACTIVE_UNIFORMS, then the number of active uniforms in the uniform block identified by uniformBlockIndex is 
returned.

If pname is GL_UNIFORM_BLOCK_ACTIVE_UNIFORM_INDICES, then a list of the active uniform indices for the uniform block identified by 
uniformBlockIndex is returned. The number of elements that will be written to params is the value of GL_UNIFORM_BLOCK_ACTIVE_UNIFORMS for 
uniformBlockIndex.

If pname is GL_UNIFORM_BLOCK_REFERENCED_BY_VERTEX_SHADER, GL_UNIFORM_BLOCK_REFERENCED_BY_TESS_CONTROL_SHADER, 
GL_UNIFORM_BLOCK_REFERENCED_BY_TESS_EVALUATION_SHADER, GL_UNIFORM_BLOCK_REFERENCED_BY_GEOMETRY_SHADER, 
GL_UNIFORM_BLOCK_REFERENCED_BY_FRAGMENT_SHADER, or GL_UNIFORM_BLOCK_REFERENCED_BY_COMPUTE_SHADER then a boolean value 
indicating whether the uniform block identified by uniformBlockIndex is referenced by the vertex, tessellation control, tessellation evaluation, 
geometry, fragment or compute programming stages of program, respectively, is returned.

Errors
GL_INVALID_VALUE is generated if uniformBlockIndex is greater than or equal to the value of GL_ACTIVE_UNIFORM_BLOCKS or is not the index of 
an active uniform block in program.

GL_INVALID_ENUM is generated if pname is not one of the accepted tokens.

GL_INVALID_OPERATION is generated if program is not the name of a program object for which glLinkProgram has been called in the past.

Notes
glGetActiveUniformBlockiv is available only if the GL version is 3.1 or greater.

GL_UNIFORM_BLOCK_REFERENCED_BY_COMPUTE_SHADER is accepted only if the GL version is 4.3 or greater.

----------------------------------------------------------------------------------------------------------------------------------------------------------------

Name
glGetUniformIndices — retrieve the index of a named uniform block
C Specification
void glGetUniformIndices(GLuint program,GLsizei uniformCount,const GLchar **uniformNames,GLuint *uniformIndices);

Parameters
program
Specifies the name of a program containing uniforms whose indices to query.

uniformCount
Specifies the number of uniforms whose indices to query.

uniformNames
Specifies the address of an array of pointers to buffers containing the names of the queried uniforms.

uniformIndices
Specifies the address of an array that will receive the indices of the uniforms.

Description
glGetUniformIndices retrieves the indices of a number of uniforms within program.

program must be the name of a program object for which the command glLinkProgram must have been called in the past, although it is not required 
that glLinkProgram must have succeeded. The link could have failed because the number of active uniforms exceeded the limit.

uniformCount indicates both the number of elements in the array of names uniformNames and the number of indices that may be written to 
uniformIndices.

uniformNames contains a list of uniformCount name strings identifying the uniform names to be queried for indices. For each name string in 
uniformNames, the index assigned to the active uniform of that name will be written to the corresponding element of uniformIndices. If a string in 
uniformNames is not the name of an active uniform, the special value GL_INVALID_INDEX will be written to the corresponding element of 
uniformIndices.

If an error occurs, nothing is written to uniformIndices.

Errors
GL_INVALID_OPERATION is generated if program is not the name of a program object for which glLinkProgram has been called in the past.

Notes
glGetUniformIndices is available only if the GL version is 3.1 or greater.

----------------------------------------------------------------------------------------------------------------------------------------------------------------

Name
glGetActiveUniformsiv — Returns information about several active uniform variables for the specified program object
C Specification
void glGetActiveUniformsiv(GLuint program,GLsizei uniformCount,const GLuint *uniformIndices,GLenum pname,GLint *params);

Parameters
program
Specifies the program object to be queried.

uniformCount
Specifies both the number of elements in the array of indices uniformIndices and the number of parameters written to params upon successful return.

uniformIndices
Specifies the address of an array of uniformCount integers containing the indices of uniforms within program whose parameter pname should be 
queried.

pname
Specifies the property of each uniform in uniformIndices that should be written into the corresponding element of params.

params
Specifies the address of an array of uniformCount integers which are to receive the value of pname for each uniform in uniformIndices.

Description
glGetActiveUniformsiv queries the value of the parameter named pname for each of the uniforms within program whose indices are specified in the 
array of uniformCount unsigned integers uniformIndices. Upon success, the value of the parameter for each uniform is written into the corresponding 
entry in the array whose address is given in params. If an error is generated, nothing is written into params.

If pname is GL_UNIFORM_TYPE, then an array identifying the types of uniforms specified by the corresponding array of uniformIndices is returned.
The returned types can be any of the values from the following table:

Returned Symbolic Contant																			Shader Uniform Type
GL_FLOAT																									float
GL_FLOAT_VEC2																							vec2
GL_FLOAT_VEC3																							vec3
GL_FLOAT_VEC4																							vec4
GL_DOUBLE																								double
GL_DOUBLE_VEC2																						dvec2
GL_DOUBLE_VEC3																						dvec3
GL_DOUBLE_VEC4																						dvec4
GL_INT																										int
GL_INT_VEC2																								ivec2
GL_INT_VEC3																								ivec3
GL_INT_VEC4																								ivec4
GL_UNSIGNED_INT																						unsigned int
GL_UNSIGNED_INT_VEC2																				uvec2
GL_UNSIGNED_INT_VEC3																				uvec3
GL_UNSIGNED_INT_VEC4																				uvec4
GL_BOOL																									bool
GL_BOOL_VEC2																							bvec2
GL_BOOL_VEC3																							bvec3
GL_BOOL_VEC4																							bvec4
GL_FLOAT_MAT2																							mat2
GL_FLOAT_MAT3																							mat3
GL_FLOAT_MAT4																							mat4
GL_FLOAT_MAT2x3																						mat2x3
GL_FLOAT_MAT2x4																						mat2x4
GL_FLOAT_MAT3x2																						mat3x2
GL_FLOAT_MAT3x4																						mat3x4
GL_FLOAT_MAT4x2																						mat4x2
GL_FLOAT_MAT4x3																						mat4x3
GL_DOUBLE_MAT2																						dmat2
GL_DOUBLE_MAT3																						dmat3
GL_DOUBLE_MAT4																						dmat4
GL_DOUBLE_MAT2x3																					dmat2x3
GL_DOUBLE_MAT2x4																					dmat2x4
GL_DOUBLE_MAT3x2																					dmat3x2
GL_DOUBLE_MAT3x4																					dmat3x4
GL_DOUBLE_MAT4x2																					dmat4x2
GL_DOUBLE_MAT4x3																					dmat4x3
GL_SAMPLER_1D																							sampler1D
GL_SAMPLER_2D																							sampler2D
GL_SAMPLER_3D																							sampler3D
GL_SAMPLER_CUBE																						samplerCube
GL_SAMPLER_1D_SHADOW																			sampler1DShadow
GL_SAMPLER_2D_SHADOW																			sampler2DShadow
GL_SAMPLER_1D_ARRAY																				sampler1DArray
GL_SAMPLER_2D_ARRAY																				sampler2DArray
GL_SAMPLER_1D_ARRAY_SHADOW																sampler1DArrayShadow
GL_SAMPLER_2D_ARRAY_SHADOW																sampler2DArrayShadow
GL_SAMPLER_2D_MULTISAMPLE																	sampler2DMS
GL_SAMPLER_2D_MULTISAMPLE_ARRAY															sampler2DMSArray
GL_SAMPLER_CUBE_SHADOW																		samplerCubeShadow
GL_SAMPLER_BUFFER																					samplerBuffer
GL_SAMPLER_2D_RECT																				sampler2DRect
GL_SAMPLER_2D_RECT_SHADOW																	sampler2DRectShadow
GL_INT_SAMPLER_1D																					isampler1D
GL_INT_SAMPLER_2D																					isampler2D
GL_INT_SAMPLER_3D																					isampler3D
GL_INT_SAMPLER_CUBE																				isamplerCube
GL_INT_SAMPLER_1D_ARRAY																		isampler1DArray
GL_INT_SAMPLER_2D_ARRAY																		isampler2DArray
GL_INT_SAMPLER_2D_MULTISAMPLE																isampler2DMS
GL_INT_SAMPLER_2D_MULTISAMPLE_ARRAY													isampler2DMSArray
GL_INT_SAMPLER_BUFFER																			isamplerBuffer
GL_INT_SAMPLER_2D_RECT																			isampler2DRect
GL_UNSIGNED_INT_SAMPLER_1D																	usampler1D
GL_UNSIGNED_INT_SAMPLER_2D																	usampler2D
GL_UNSIGNED_INT_SAMPLER_3D																	usampler3D
GL_UNSIGNED_INT_SAMPLER_CUBE																usamplerCube
GL_UNSIGNED_INT_SAMPLER_1D_ARRAY														usampler2DArray
GL_UNSIGNED_INT_SAMPLER_2D_ARRAY														usampler2DArray
GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE											usampler2DMS
GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY									usampler2DMSArray
GL_UNSIGNED_INT_SAMPLER_BUFFER															usamplerBuffer
GL_UNSIGNED_INT_SAMPLER_2D_RECT															usampler2DRect
If pname is GL_UNIFORM_SIZE, then an array identifying the size of the uniforms specified by the corresponding array of uniformIndices is returned.
The sizes returned are in units of the type returned by a query of GL_UNIFORM_TYPE. For active uniforms that are arrays, the size is the number of 
active elements in the array; for all other uniforms, the size is one.

If pname is GL_UNIFORM_NAME_LENGTH, then an array identifying the length, including the terminating null character, of the uniform name strings 
specified by the corresponding array of uniformIndices is returned.

If pname is GL_UNIFORM_BLOCK_INDEX, then an array identifying the the uniform block index of each of the uniforms specified by the 
corresponding array of uniformIndices is returned. The uniform block index of a uniform associated with the default uniform block is -1.

If pname is GL_UNIFORM_OFFSET, then an array of uniform buffer offsets is returned. For uniforms in a named uniform block, the returned value 
will be its offset, in basic machine units, relative to the beginning of the uniform block in the buffer object data store. For atomic counter uniforms,
the returned value will be its offset relative to the beginning of its active atomic counter buffer. For all other uniforms, -1 will be returned.

If pname is GL_UNIFORM_ARRAY_STRIDE, then an array identifying the stride between elements of each of the uniforms specified by the 
corresponding array of uniformIndices is returned. For uniforms in named uniform blocks and for uniforms declared as atomic counters, the stride is 
the difference, in basic machine units, of consecutive elements in an array, or zero for uniforms not declared as an array. For all other uniforms,a 
stride of -1 will be returned.

If pname is GL_UNIFORM_MATRIX_STRIDE, then an array identifying the stride between columns of a column-major matrix or rows of a row-major 
matrix, in basic machine units, of each of the uniforms specified by the corresponding array of uniformIndices is returned. The matrix stride of a 
uniform associated with the default uniform block is -1. Note that this information only makes sense for uniforms that are matrices. For uniforms 
that are not matrices, but are declared in a named uniform block, a matrix stride of zero is returned.

If pname is GL_UNIFORM_IS_ROW_MAJOR, then an array identifying whether each of the uniforms specified by the corresponding array of 
uniformIndices is a row-major matrix or not is returned. A value of one indicates a row-major matrix, and a value of zero indicates a column-major 
matrix, a matrix in the default uniform block, or a non-matrix.

If pname is GL_UNIFORM_ATOMIC_COUNTER_BUFFER_INDEX, then an array identifying the active atomic counter buffer index of each of the 
uniforms specified by the corresponding array of uniformIndices is returned. For uniforms other than atomic counters, the returned buffer index is -1.
The returned indices may be passed to glGetActiveAtomicCounterBufferiv to query the properties of the associated buffer, and not necessarily the 
binding point specified in the uniform declaration.

Notes
The double types, GL_DOUBLE, GL_DOUBLE_VEC2, GL_DOUBLE_VEC3, GL_DOUBLE_VEC4, GL_DOUBLE_MAT2, GL_DOUBLE_MAT3,
GL_DOUBLE_MAT4, GL_DOUBLE_MAT2x3, GL_DOUBLE_MAT2x4, GL_DOUBLE_MAT3x2, GL_DOUBLE_MAT3x4, GL_DOUBLE_MAT4x2, and 
GL_DOUBLE_MAT4x3 are only available if the GL version is 4.1 or higher.

GL_UNIFORM_ATOMIC_COUNTER_BUFFER_INDEX is only accepted by pname if the GL version is 4.2 or higher.

Errors
GL_INVALID_VALUE is generated if program is not a value generated by OpenGL.

GL_INVALID_OPERATION is generated if program is not a program object.

GL_INVALID_VALUE is generated if uniformCount is greater than or equal to the value of GL_ACTIVE_UNIFORMS for program.

GL_INVALID_ENUM is generated if pname is not an accepted token.

----------------------------------------------------------------------------------------------------------------------------------------------------------------
*/