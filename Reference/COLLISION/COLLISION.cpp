/*
第2章-碰撞检测系统中的设计问题：

由于任何一个物体都有可能与其他物体发生碰撞，包含n个物体的碰撞检测过程，在最坏的情况下需要：
(n - 1) + ( n _ 2) + ... + 1 = n(n _ 1) / 2 = O(n ^ 2)次相互测试。
为了提高测试速度，上述物体间相互测试的数量必须适度地降低。这种降低的方式可以描述为：将包含大量
对象的碰撞操作划分为两个阶段---粗略测试阶段(broad phase)和精确测试阶段(narrow phase)。
粗略测试阶段，如图：Knowledge/COLLISION/001.png所示。

对于最近的n次碰撞查询，保持一个循环缓冲区。

第3章-数学和几何学入门：
矩阵的加减：
如图：Knowledge/COLLISION/002.png所示。
乘法：
如图：Knowledge/COLLISION/003.png所示。
转置：
如图：Knowledge/COLLISION/004.png所示。
行列式：
如图：Knowledge/COLLISION/005.png-008.png所示。

对于一个1x1的矩阵，A = |u1|，则det(A)代表原点至u1的线段的有符号距离长度。
对于一个2x2的矩阵，A = |u1	u2|
								 |v1	v2|，则det(A)代表原点(0,0)和点(u1,u2)，点(v1,v2)确定的平行四边形
的有符号面积。如果平行四边形的首顶点至第二个顶点是逆时针方向的，则det(A)的值为正，否则为负。
对于一个3x3的矩阵，A = |u		v		w|，其中u，v，w为列向量，则det(A)代表3个向量确定的平行六面
体的有符号体积。
通常，对于一个n维矩阵，其行列式对应着：由列向量确定的n维超平行六面体的有符号体积。

利用克莱姆法则计算线性方程组：
如图：Knowledge/COLLISION/009.png-011.png所示。

2x2矩阵和3x3矩阵的逆矩阵：
如图：Knowledge/COLLISION/012.png-013.png所示。

ORIENT2D(A,B,C)：
1，判断点和有向直线的左右关系。
2，三角形顶点的环绕方向。
3，三角形的面积。
4，2D直线的隐式方程。
如图：Knowledge/COLLISION/014.png所示。

ORIENT3D(A,B,C,D)：
1，判断点和三角形的关系。
2，四面体的体积。
3，3D面的隐式方程。
如图：Knowledge/COLLISION/015.png-016.png所示。

INCIRCLE2D(A,B,C,D)：
1，判断点与圆的关系。
如图：Knowledge/COLLISION/017.png所示。

INSPHERE(A,B,C,D,E)：
1，判断点与球的关系。
如图：Knowledge/COLLISION/018.png所示。

向量加法：
如图：Knowledge/COLLISION/019.png所示。

点积与投影：
如图：Knowledge/COLLISION/020.png所示。

叉积：
如图：Knowledge/COLLISION/021.png-024.png所示。

标量三重积(The Scalar Triple Product)：
(u × v) · w = u · (v × w)
标量三重积的值对应3个非相关向量u,v,w形成的平行六面体的有符号体积。相应的，其值是u,v,w构成的
四面体的体积的6倍。
如图：Knowledge/COLLISION/025.png-027.png所示。

质心坐标(Barycentric Coordinates)：
考察两个顶点A和B，其间有点P，且P = A + t(B - A) = (1 - t)A + tB，或简写成P = uA + vB，其中，
u + v = 1。当且仅当0 ≤ u ≤ 1，0 ≤ v ≤ 1时，顶点P位于线段AB之上。对于最后一种形式，(u,v)称作
顶点P在线段AB的质心坐标。顶点A的质心坐标为(1,0)，顶点B的质心坐标为(0,1)。
质心坐标的典型应用之一是参数化三角形。考察3个非共线顶点A，B，C构造的三角形ABC。面上一点P可以
单独表示为P = uA + vB + wC，其中u，v，w为常数，且u + v + w = 1。
三元组(u,v,w)对应着顶点的质心坐标。对于三角形ABC，顶点A，B，C的质心坐标分别为(1, 0, 0),
(0, 1, 0), 和 (0, 0, 1)。一般地，当且仅当0 ≤ u, v, w ≤ 1，或当且仅当0 ≤ v ≤ 1, 0 ≤ w ≤ 1, 和
v + w ≤ 1时，具有质心坐标(u,v,w)的顶点将位于三角形的内部（或边上）。质心坐标实际上按照
P = uA + vB + wC的方式参数化了平面，且是P = A + v(B - A) + w(C - A)的另一种表达形式：
P = A + v(B - A) + w(C - A) = (1 - v - w)A + vB + wC（该式可以推导出来）
为了求解质心坐标，将表达式写作v v0 + w v1 = v2
其中v0 = B - A, v1 = C - A, v2 = P - A
则，通过两边乘以v0,v1构造一个2x2线性方程组：
(v v0 + w v1) · v0 = v2 · v0
(v v0 + w v1) · v1 = v2 · v1
由于点积是一个线性操作符，上述表达式等价于：
v (v0 · v0) + w (v1 · v0) = v2 · v0
v (v0 · v1) + w (v1 · v1) = v2 · v1
该方程组可以应用克莱姆法则。
//Compute barycentric coordinates (u, v, w) for point p with respect to triangle (a, b, c)
void Barycentric(Point a, Point b, Point c, Point p, float &u, float &v, float &w)
{
	Vector v0 = b - a, v1 = c - a, v2 = p - a;
	float d00 = Dot(v0, v0);
	float d01 = Dot(v0, v1);
	float d11 = Dot(v1, v1);
	float d20 = Dot(v2, v0);
	float d21 = Dot(v2, v1);
	float denom = d00 * d11 - d01 * d01;
	v = (d11 * d20 - d01 * d21) / denom;
	w = (d00 * d21 - d01 * d20) / denom;
	u = 1.0f - v - w;
}
本书是以列为主来表达矩阵的。但是克莱姆法则是行列式的比值，矩阵的行列式和其转置的行列式是相等的。
对于任一维度的单形体(simplex)，都可以计算一点的质心坐标。例如，给定由顶点A，B，C，D构造的四面
体，质心坐标(u,v,w,x)，确定了3D空间中的一点P，P = uA + vB + wC + xD，u + v + w + x = 1
当且仅当，0 ≤ u, v, w, x ≤ 1，点P位于四面体内部。
给定点A = (ax, ay, az), B = (bx, by, bz), C = (cx, cy, cz),D = (dx, dy, dz),P = (px, py, pz)
质心坐标可以通过建立下列线性方程组求解：
axu + bxv + cxw + dxx = px
ayu + byv + cyw + dyx = py
azu + bzv + czw + dzx = pz
u + v + w + x = 1
将P = uA + vB + wC + xD等式两边分别减去A，得到
P - A = v(B - A) + w(C - A) + x(D - A)		（通过替换u = 1- w - v - x可以推导出）
通过求解下列等式，可以得到4个质心坐标中的3个解：
(bx - ax)v + (cx - ax) w + (dx - ax) x = px - ax,
(by - ay)v + (cy - ay) w + (dy - ay) x = py - ay
(bz - az)v + (cz - az) w + (dz - az) x = pz - az
通过克莱姆法则，可以得到：
u = dPBCD/dABCD
v = dAPCD/dABCD,
w = dABPD/dABCD, and
x = dABCP/dABCD = 1 - u - v - w
dPBCD =
px bx cx dx
py by cy dy
pz bz cz dz
1 1 1 1
dAPCD =
ax px cx dx
ay py cy dy
az pz cz dz
1 1 1 1
dABPD =
ax bx px dx
ay by py dy
az bz pz dz
1 1 1 1
dABCP =
ax bx cx px
ay by cy py
az bz cz pz
1 1 1 1
dABCD =
ax bx cx dx
ay by cy dy
az bz cz dz
1 1 1 1
行列式对应着四面体PBCD,APCD,ABPD,ABCP,ABCD的有符号体积（是每一个有符号体积的1/6）。
行列式的比值将化简为：顶点到其对应平面的相对高度。
现在再来考察三角形，正如四面体的质心坐标可以表示为体空间之比，三角形的质心坐标也可以看做是面积
之比。特别地，点P的质心坐标可以看做是三角形PBC, PCA,PAB与整个三角形ABC之比。因此，质心坐标
也常称作面积坐标。当采用有符号的三角形面积时，上述表达式对于判断位于三角形之外的点也是有效的。
鉴于此，质心坐标(u,v,w)记作：
u = SignedArea(PBC)/SignedArea(ABC),
v = SignedArea(PCA)/SignedArea(ABC), and
w = SignedArea(PAB)/SignedArea(ABC) = 1 - u – v.
因为已消除常量因子，所以任意三角形比例函数都可以用于计算上述比值。特别地，这里可以采用三角形两
边叉积的量值。正确的计算符号由叉积和三角形ABC面法线的点积值加以确定。例如，三角形ABC的有符号
面积可以计算为：
SignedArea(PBC) = Dot(Cross(B-P, C-P), Normalize(Cross(B-A, C-A))).
由于三角形的面积可以记作底边x高/2，又前述各三角形比值项中都包含相同的底边，因此表达式可以简化为
高度之比。因此，另一种看待质心坐标的方式是：分量u,v,w---点P对应于边BC，AC，AB的规范化高度---
与顶点至对应边上的高度成某种比例。由于三角形是3个2D空间的交集，每一个空间都是由基于三角形某一边
的且高度为0,1的两条平行线（或平行面）间的无限空间加以定义，这也是当一点位于三角形内部时，0 ≤ u, v, w ≤ 1
的原因。如图：Knowledge/COLLISION/028.png所示。
与三角形各边重合的直线可以看做将三角形平面分割为7个质心域，每一个域的符号将依据质心坐标分量的
正负值计算。如图：Knowledge/COLLISION/029.png所示。
关于质心坐标的重要特征之一是投影不变性，这一特征与前述的坐标计算方法相比更具高效性：
即无须使用顶点的3D坐标来计算面积，可以将顶点投影至xy，xz或yz平面从而简化计算过程。
为了防止退化现象，投影总是作用于最大投影面，三角形法线分量的最大绝对值给出了投影时的“舍弃”分量。
float TriArea2D(float x1, float y1, float x2, float y2, float x3, float y3)
{
	return (x1-x2)*(y2-y3) - (x2-x3)*(y1-y2);
}
//Compute barycentric coordinates (u, v, w) for point p with respect to triangle (a, b, c)
void Barycentric(Point a, Point b, Point c, Point p, float &u, float &v, float &w)
{
	//Unnormalized triangle normal
	Vector m = Cross(b - a, c - a);
	//Nominators and one-over-denominator for u and v ratios
	float nu, nv, ood;
	//Absolute components for determining projection plane
	float x = Abs(m.x), y = Abs(m.y), z = Abs(m.z);
	//Compute areas in plane of largest projection
	if (x >= y && x >= z)
	{
		//x is largest, project to the yz plane
		nu = TriArea2D(p.y, p.z, b.y, b.z, c.y, c.z);//Area of PBC in yz plane
		nv = TriArea2D(p.y, p.z, c.y, c.z, a.y, a.z);//Area of PCA in yz plane
		ood = 1.0f / m.x; //1/(2*area of ABC in yz plane)
	}
	else if (y >= x && y >= z)
	{
		//y is largest, project to the xz plane
		nu = TriArea2D(p.x, p.z, b.x, b.z, c.x, c.z);
		nv = TriArea2D(p.x, p.z, c.x, c.z, a.x, a.z);
		ood = 1.0f / -m.y;
	}
	else
	{
		//z is largest, project to the xy plane
		nu = TriArea2D(p.x, p.y, b.x, b.y, c.x, c.y);
		nv = TriArea2D(p.x, p.y, c.x, c.y, a.x, a.y);
		ood = 1.0f / m.z;
	}
	u = nu * ood;
	v = nv * ood;
	w = 1.0f - u - v;
}
质心坐标有多种用途，由于其投影不变性，常用于在不同的坐标系统间实行点映射。也可用于测试点是否位于
三角形内部。另外，给定一个顶点光照三角形，可以获取三角形内部一点相应的RGB值。
下面代码为利用质心坐标测试一点P是否位于三角形ABC的内部：
//Test if point p is contained in triangle (a, b, c)
int TestPointTriangle(Point p, Point a, Point b, Point c)
{
	float u, v, w;
	Barycentric(a, b, c, p, u, v, w);
	return v >= 0.0f && w >= 0.0f && (v + w) <= 1.0f;
}
对于顶点P0, P1, . . . , Pn，若点P的质心坐标满足a0, a1, . . . , an ≥ 0，则称P是P0, P1, . . . , Pn的凸
组合。

直线可以定义为一个点集，即任意不重合的两点A，B间的线性组合：
L(t) = (1 - t)A + tB
这里t的范围是全体实数，-∞ < t < ∞，连接A，B的线段只是该直线上的一部分，可以通过0 ≤ t ≤ 1加以
限定。如果两个端点A，B以某种顺序给出，则该线段是有方向的。光线可以类似的定义为“半无穷”直线，且
有t >= 0。
经常这样定义光线：
L(t) = A + t v (where v = B - A)
如图：Knowledge/COLLISION/030.png所示。

平面：
1，3点不共线（形成一个平面上的三角形）
2，平面上的一点及其法线。
3，平面的法线和距离原点的距离。
在第一种情况下，平面P可以利用顶点A，B，C给出其参数表达式：
P(u, v) = A + u(B - A) + v(C - A)			以A为原点，在两条边上进行参数化
当观察一个由三角形ABC确定的平面时，且三角形的3个顶点呈逆时针旋转，一般定义该平面法线指向观察者。
n = (B - A) x (C - A)
给定平面的法线n及平面上一点p，平面内的点集x可以归结为：向量x - p垂直于平面的法线n，即两个向量的
点积为0。这种垂直特征定义了平面的隐式方程，即“点-法”式平面方程：
n · (x - p) = 0
点积操作满足分配率
n · x - n · p = 0
令d = n · p
那么：
n · x = n · p = d
若n为单位向量，则|d|等于原点距离该平面的距离。
若n不是单位向量，|d|仍为一个距离值，但为“向量n的长度”个单位量。
如果不采用绝对值，d值将是一个有符号的距离。
“常数-法线”式平面方程记作分量形式ax + by + cz - d = 0，其中n = (a,b,c)，X（平面内的点集） = 
(x,y,z)，这里，在面相交计算中，更倾向于记作ax + by + cz + d = 0
当n为单位向量时，称作归一化的平面方程。当给定一点并采用归一化的平面方程时，结果值为该点距离平面
的有符号距离（正，点位于平面的正面，负，点位于平面的背面）。
参数化的平面方程可以定义为：
P(s, t) = A + su + tv
其中u和v为平面内两个不相关的向量，A为平面内一点。

多边形：
如果多边形的内角（该角度由连接一个顶点的两条边构成，且位于多边形内部）小于或等于180度，则称相应
顶点为凸顶点。否则为凹顶点。
如果多边形P任意两点间的所有线段皆位于多边形P内部，则称多边形P为凸多边形。否则，为凹多边形。包含
一个或多个凹顶点的多边形一定是一个凹多边形。
在多边形网格中，顶点的“度”解释为：
顶点所连接的边数量。
多边形凸性测试：
设四边形ABCD的全部顶点共面，当且仅当两对角线完全位于四边形内部，则该四边形为凸面。
如图：Knowledge/COLLISION/031.png所示。
如果AC和BD相交，则该四边形为凸面。
线段相交等价于：
顶点A，C分别位于直线BD的两侧。顶点B，D也位于直线AC的两侧。
因此，该测试也意味着三角形BDA与三角形BDC有着相反的环绕方向。三角形ACD与三角形ACB有着相反的环绕方向。
相反的环绕方向可以通过计算三角形的法线并在两个法线之间执行点积值的符号测试：如果点积值为负，则两
条法线指向相反的方向，且三角形之间的环绕方向相反。
综上所述，四边形为凸面的条件是：
(BD × BA) · (BD × BC) < 0 and
(AC × AD) · (AC × AB) < 0
对于n边多边形的凸性测试，如图：Knowledge/COLLISION/032.png-033.png所示。
对于给定点集的凸包计算，参考QuickHull算法。
Voronoi域：
Voronoi域构造了多面体的外部空间划分。
如图：Knowledge/COLLISION/042.png-043.png所示。

第4章：包围体：
包围体计算应采用预处理方式而非实时计算。
包围体类型，如图：Knowledge/COLLISION/034.png所示。
AABB：
其面法线皆平行于给定的坐标轴。
AABB的三种常用表达方式，如图：Knowledge/COLLISION/035.png所示。

球体扫掠体：
如图：Knowledge/COLLISION/036.png所示。

Kay–Kajiya平行平面空间包围体(Kay–Kajiya Slab-based Volumes)：
Kay–Kajiya包围体可以加速计算光线-物体的相交测试。
Kay–Kajiya包围体也称作基于平行平面空间的平行六面体族。
所谓平行平面空间(slab)是指两平行面之间的无限区域。
该面集使用了一个法线n加以表达，并设定两个标量用于记录两个面(沿n方向)距原点的有符号距离。
如图：Knowledge/COLLISION/037.png所示。

第5章：基本图元测试：
点到面的最近点：
如图：Knowledge/COLLISION/038.png-039.png所示。
点到线段的最近点：
如图：Knowledge/COLLISION/040.png所示。
点到三角形的最近点：
如图：Knowledge/COLLISION/041.png-042.png所示。
点到四面体的最近点：
如图：Knowledge/COLLISION/044.png-046.png所示。
两条直线间的最近点：
如图：Knowledge/COLLISION/047.png所示。
两条线段间的最近点：
如图：Knowledge/COLLISION/048.png所示。
线段和三角形的最近点：
如图：Knowledge/COLLISION/049.png-050.png所示。
两个三角形间的最近点：
如图：Knowledge/COLLISION/051.png-052.png所示。
分离轴测试：
两个多面体对象依据其特征可以产生6种不同的碰撞方式：
面-面，面-边，面-顶点，边-边，边-顶点，顶点-顶点
由于顶点可以视为边的一部分，则分类可以减少至3种组合：
面-面，面-边，边-边
对于面-面测试，可以将物体的面法线作为潜在的测试分离轴。
对于边-边测试，潜在的测试分离轴对应于两条边的叉积。
总体上讲，多面体对象间的分离测试需要考察下列轴：
平行于物体A的面法线的轴
平行于物体B的面法线的轴
平行于物体A,B各边生成的叉积向量的轴
对于包含相同数量面(F)和边(E)的两个常规多面体，潜在分离轴的数量为2F + E ^ 2。
待全部轴检测完毕后，选择具有最小重叠量的轴向作为碰撞法线，且该重叠量还可以用于估算穿越深度。
如图：Knowledge/COLLISION/053.png-055.png所示。
盒体与平面间的测试：
如图：Knowledge/COLLISION/056.png所示。
圆锥体与平面间的测试：
如图：Knowledge/COLLISION/057.png所示。
两个三角形间的测试：
如图：Knowledge/COLLISION/058.png-059.png所示。
线段与平面间的测试：
如图：Knowledge/COLLISION/060.png所示。
射线与球体间的测试：
如图：Knowledge/COLLISION/061.png所示。
射线与盒体的测试：
如图：Knowledge/COLLISION/062.png-063.png所示。
射线或线段与圆柱体间的测试：
如图：Knowledge/COLLISION/064.png所示。
射线或线段与凸多面体间的测试：
如图：Knowledge/COLLISION/065.png所示。
点与多边形间的测试：
如图：Knowledge/COLLISION/066.png所示。
三个平面间的测试：
如图：Knowledge/COLLISION/067.png-071.png所示。
*/