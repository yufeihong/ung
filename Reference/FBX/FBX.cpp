/*
class FbxParser
{
	public:
		FbxParser(const char* mFbxFileName);

		~FbxParser() = default;

		void createImporter(const char* name = "");

		void createScene(const char* name = "");

		void importScene();

		void parsing();

		//Get the file version number generate by the FBX SDK.
		void getSDKFileVersion(int32& sdkMajor,int32& sdkMinor,int32& sdkRevision);

		//Get the FBX version number of the FBX file.This function must be called after FbxImporter::Initialize().
		void getFileVersion(int32& fileMajor,int32& fileMinor,int32& fileRevision);

		usize getVertexCount();

		usize getIndexCount();

		void getPositions(real_type* positionArray,usize arraySize);

		void getPositions(std::vector<real_type>& vec);

		void getIndices(uint16* indexArray,usize arraySize);

		void getIndices(std::vector<uint16>& vec);

	private:
		class Impl;
		std::shared_ptr<Impl> mImpl;
};

class FbxParser::Impl
{
public:
	Impl() :
		mFbxImporter(nullptr),
		mFbxScene(nullptr),
		mFbxRootNode(nullptr)
		{
			mFbxManager = FbxManager::Create();
			if (!mFbxManager)
			{
				wxMessageBox(wxT("Fail to create FbxManager."));
				exit(-1);
			}

			//FbxIOSettings object holds all import/export settings.
			FbxIOSettings* ioSettings = FbxIOSettings::Create(mFbxManager, IOSROOT);
			if (!ioSettings)
			{
				wxMessageBox(wxT("Fail to create FbxIOSettings."));
				exit(-1);
			}

			mFbxManager->SetIOSettings(ioSettings);

			//exe文件所在的绝对路径
			FbxString applicationDirectory = FbxGetApplicationDirectory();
			//Load plugins from the executable directory.
			auto loadPluginsRet = mFbxManager->LoadPluginsDirectory(applicationDirectory.Buffer(), "dll");
			if (!loadPluginsRet)
			{
				wxMessageBox(wxT("Fail to load plugins."));
				exit(-1);
			}
		}

	~Impl()
	{
		if (mFbxManager)
		{
			mFbxManager->Destroy();
			mFbxManager = nullptr;
		}
	}

	void ProcessNode(FbxNode* pNode)
	{
		const char* nodeName = pNode->GetName();
		FbxDouble3 translation = pNode->LclTranslation.Get();
		FbxDouble3 rotation = pNode->LclRotation.Get();
		FbxDouble3 scaling = pNode->LclScaling.Get();

		//Get the number of node attribute(s) connected to this node.
		usize attributeCount = pNode->GetNodeAttributeCount();
		for (usize i = 0; i < attributeCount; i++)
		{
			//Get the connected node attribute by specifying its index in the connection list.
			FbxNodeAttribute* fbxAttribute = pNode->GetNodeAttributeByIndex(i);
			//Returns:Pointer to corresponding node attribute or NULL if the index is out of range.
			BOOST_ASSERT(fbxAttribute);

			FbxNodeAttribute::EType attributeType = fbxAttribute->GetAttributeType();
			const char* attributeTypeName = nullptr;
			const char* attributeName = fbxAttribute->GetName();

			switch (attributeType)
			{
			case fbxsdk::FbxNodeAttribute::eUnknown:
				attributeTypeName = "Unknown";
				break;
			case fbxsdk::FbxNodeAttribute::eNull:
				attributeTypeName = "Null";
				break;
			case fbxsdk::FbxNodeAttribute::eMarker:
				attributeTypeName = "Marker";
				break;
			case fbxsdk::FbxNodeAttribute::eSkeleton:
				attributeTypeName = "Skeleton";
				ProcessSkeleton(pNode);
				break;
			case fbxsdk::FbxNodeAttribute::eMesh:
				attributeTypeName = "Mesh";
				ProcessMesh(pNode);
				break;
			case fbxsdk::FbxNodeAttribute::eNurbs:
				attributeTypeName = "Nurbs";
				break;
			case fbxsdk::FbxNodeAttribute::ePatch:
				attributeTypeName = "Patch";
				break;
			case fbxsdk::FbxNodeAttribute::eCamera:
				attributeTypeName = "Camera";
				break;
			case fbxsdk::FbxNodeAttribute::eCameraStereo:
				attributeTypeName = "CameraStereo";
				break;
			case fbxsdk::FbxNodeAttribute::eCameraSwitcher:
				attributeTypeName = "CameraSwitcher";
				break;
			case fbxsdk::FbxNodeAttribute::eLight:
				attributeTypeName = "Light";
				break;
			case fbxsdk::FbxNodeAttribute::eOpticalReference:
				attributeTypeName = "OpticalReference";
				break;
			case fbxsdk::FbxNodeAttribute::eOpticalMarker:
				attributeTypeName = "OpticalMarker";
				break;
			case fbxsdk::FbxNodeAttribute::eNurbsCurve:
				attributeTypeName = "NurbsCurve";
				break;
			case fbxsdk::FbxNodeAttribute::eTrimNurbsSurface:
				attributeTypeName = "TrimNurbsSurface";
				break;
			case fbxsdk::FbxNodeAttribute::eBoundary:
				attributeTypeName = "Boundary";
				break;
			case fbxsdk::FbxNodeAttribute::eNurbsSurface:
				attributeTypeName = "NurbsSurface";
				break;
			case fbxsdk::FbxNodeAttribute::eShape:
				attributeTypeName = "Shape";
				break;
			case fbxsdk::FbxNodeAttribute::eLODGroup:
				attributeTypeName = "LODGroup";
				break;
			case fbxsdk::FbxNodeAttribute::eSubDiv:
				attributeTypeName = "SubDiv";
				break;
			case fbxsdk::FbxNodeAttribute::eCachedEffect:
				attributeTypeName = "CachedEffect";
				break;
			case fbxsdk::FbxNodeAttribute::eLine:
				attributeTypeName = "Line";
				break;
			default:
				wxMessageBox(wxT("Node attribute type is out of range."));
				exit(-1);
			}
		}//end for

		usize childCount = pNode->GetChildCount();
		for (usize j = 0; j < childCount; j++)
		{
			ProcessNode(pNode->GetChild(j));
		}
	}

	void ProcessMesh(FbxNode* pNode)
	{
		//FbxMesh* GetMesh()
		//Get the node attribute casted to a FbxMesh pointer.
		//Returns:Pointer to the mesh object.
		//Remarks:This method will try to process the default node attribute first. If it cannot find it, it will scan the list of connected node attributes 
		//and get the first object that is a FbxNodeAttribute::eMesh.
		//If the above search failed to get a valid pointer or it cannot be successfully casted, this method will return NULL.
		FbxMesh* pMesh = pNode->GetMesh();
		if (!pMesh)
		{
			wxMessageBox(wxT("This node's attribute type is not eMesh."));
			exit(-1);
		}

		bool isTriangleRet = pMesh->IsTriangleMesh();
		BOOST_ASSERT(isTriangleRet);

		mVertexCount = pMesh->GetControlPointsCount();
		BOOST_ASSERT(mVertexCount < std::numeric_limits<unsigned short>::max() + 1);
		mVertices = new FbxVector4[mVertexCount];
		memcpy(mVertices, pMesh->GetControlPoints(), mVertexCount * sizeof(FbxVector4));

		//Get the polygon count of this mesh.
		usize faceCount = pMesh->GetPolygonCount();
		//遍历所有的三角形
		for (usize indexOfFace = 0; indexOfFace < faceCount; ++indexOfFace)
		{
			//int GetPolygonSize(int pPolygonIndex) const
			//Get the number of polygon vertices in a polygon.
			//pPolygonIndex:Index of the polygon.
			//Returns:The number of polygon vertices in the indexed polygon.If the polygon index is out of bounds, return -1.
			uint8 polygonVerticesCount = pMesh->GetPolygonSize(indexOfFace);
			if (polygonVerticesCount != 3)
			{
				wxMessageBox(wxT("Polygon is not triangle."));
				exit(-1);
			}
			//遍历三角形的3个顶点
			for (uint8 indexOfVerticeInFace = 0; indexOfVerticeInFace < polygonVerticesCount; indexOfVerticeInFace++)
			{
				//int GetPolygonVertex(int pPolygonIndex,int pPositionInPolygon) const
				//Get a polygon vertex (i.e: an index to a control point).
				//pPolygonIndex:Index of queried polygon.The valid range for this parameter is 0 to FbxMesh::GetPolygonCount().
				//pPositionInPolygon:Position of polygon vertex in indexed polygon.The valid range for this parameter is 0 to 
				//FbxMesh::GetPolygonSize(pPolygonIndex).
				//Returns:Return the polygon vertex indexed or -1 if the requested vertex does not exists or the mIndices arguments have an invalid 
				//range.
				int32 vertexIndex = pMesh->GetPolygonVertex(indexOfFace, indexOfVerticeInFace);
				BOOST_ASSERT(vertexIndex != -1);
				mIndices.push_back(vertexIndex);

				////Read the color of each vertex  
				//ReadColor(pMesh, vertexIndex, vertexCounter, &color[j]);

				////Read the UV of each vertex  
				//for (int k = 0; k < 2; ++k)
				//{
				//	ReadUV(pMesh, vertexIndex, pMesh->GetTextureUVIndex(i, j), k, &(uv[j][k]));
				//}

				////Read the normal of each vertex  
				//ReadNormal(pMesh, vertexIndex, vertexCounter, &normal[j]);

				////Read the tangent of each vertex  
				//ReadTangent(pMesh, vertexIndex, vertexCounter, &tangent[j]);

				//vertexCounter++;
			}//end for 遍历三角形
		}//end for 遍历face

		usize indexCount = mIndices.size();
		BOOST_ASSERT(indexCount == faceCount * 3);
	}

	void ProcessSkeleton(FbxNode* pNode)
	{
	}

	const char* mFbxFileName;

	FbxManager* mFbxManager;
	FbxImporter* mFbxImporter;
	FbxScene* mFbxScene;
	FbxNode* mFbxRootNode;

	bool mImporterInitialized = false;

	usize mVertexCount;
	FbxVector4* mVertices;
	//存储所有face的索引，每个face包含3个顶点索引
	std::vector<uintfast16> mIndices;
};

//-----------------------------------------------------------------

FbxParser::FbxParser(const char* mFbxFileName) :
	mImpl(std::make_shared<Impl>())
{
	if (!wxFileExists(wxString(mFbxFileName)))
	{
		std::string errorInfo = std::string(mFbxFileName) + " is not exists.";
		wxMessageBox(wxString(errorInfo));
		exit(-1);
	}
	else
	{
		mImpl->mFbxFileName = mFbxFileName;
	}
}

void FbxParser::createImporter(const char* name)
{
	mImpl->mFbxImporter = FbxImporter::Create(mImpl->mFbxManager,name);

	auto importerInit = mImpl->mFbxImporter->Initialize(mImpl->mFbxFileName, -1, mImpl->mFbxManager->GetIOSettings());

	mImpl->mImporterInitialized = true;

	if (!importerInit)
	{
		boost::format fmt("%1%");

		const char* errorFbxString = mImpl->mFbxImporter->GetStatus().GetErrorString();
		fmt % std::string(errorFbxString);

		if (mImpl->mFbxImporter->GetStatus().GetCode() == FbxStatus::eInvalidFileVersion)
		{
			fmt.clear();
			fmt.parse("%s\n%s %d %d %d\n%s %s %s %d %d %d");
			fmt %std::string(errorFbxString);

			int32 sdkMajor, sdkMinor, sdkRevision;
			getSDKFileVersion(sdkMajor, sdkMinor, sdkRevision);
			int32 fileMajor, fileMinor, fileRevision;
			getFileVersion(fileMajor, fileMinor, fileRevision);

			fmt %"FBX version number for this FBX SDK is" %sdkMajor %sdkMinor %sdkRevision;
			fmt %"FBX version number for file" %mImpl->mFbxFileName %"is" %fileMajor %fileMinor %fileRevision;
		}

		wxString boxMes(fmt.str());
		wxMessageBox(boxMes);
		exit(-1);
	}

	//Returns true if the file format is a recognized FBX format.
	if (mImpl->mFbxImporter->IsFBX())
	{
		usize animStackCount;
		std::string activeAnimStackName;

		animStackCount = mImpl->mFbxImporter->GetAnimStackCount();
		activeAnimStackName = mImpl->mFbxImporter->GetActiveAnimStackName().Buffer();

		for (usize i = 0; i < animStackCount; ++i)
		{
			const char* takeName = mImpl->mFbxImporter->GetTakeInfo(i)->mName.Buffer();
			const char* takeInfo = mImpl->mFbxImporter->GetTakeInfo(i)->mDescription.Buffer();
			const char* importName = mImpl->mFbxImporter->GetTakeInfo(i)->mImportName.Buffer();
			bool isSelected = mImpl->mFbxImporter->GetTakeInfo(i)->mSelect;
		}

		mImpl->mFbxManager->GetIOSettings()->SetBoolProp(IMP_FBX_MATERIAL, true);
		mImpl->mFbxManager->GetIOSettings()->SetBoolProp(IMP_FBX_TEXTURE, true);
		mImpl->mFbxManager->GetIOSettings()->SetBoolProp(IMP_FBX_LINK, true);
		mImpl->mFbxManager->GetIOSettings()->SetBoolProp(IMP_FBX_SHAPE, true);
		mImpl->mFbxManager->GetIOSettings()->SetBoolProp(IMP_FBX_GOBO, true);
		mImpl->mFbxManager->GetIOSettings()->SetBoolProp(IMP_FBX_ANIMATION, true);
		mImpl->mFbxManager->GetIOSettings()->SetBoolProp(IMP_FBX_GLOBAL_SETTINGS, true);
	}
}

void FbxParser::createScene(const char* name)
{
	mImpl->mFbxScene = FbxScene::Create(mImpl->mFbxManager, name);
	if (!mImpl->mFbxScene)
	{
		wxMessageBox(wxT("Fail to create FbxScene."));
		exit(-1);
	}

	//Clear the scene content by deleting the node tree below the root node and restoring default settings.
	mImpl->mFbxScene->Clear();
}

void FbxParser::importScene()
{
	auto importRet = mImpl->mFbxImporter->Import(mImpl->mFbxScene);
	if (!importRet)
	{
		wxMessageBox("Fail to import scene.");
		exit(-1);
	}

	mImpl->mFbxImporter->Destroy();
}

void FbxParser::parsing()
{
	mImpl->mFbxRootNode = mImpl->mFbxScene->GetRootNode();
	if (!mImpl->mFbxRootNode)
	{
		wxMessageBox("Fail to get root node.");
		exit(-1);
	}

	usize childCount = mImpl->mFbxRootNode->GetChildCount();
	for (usize i = 0; i < childCount; i++)
	{
		FbxNode* fbxChildNode = mImpl->mFbxRootNode->GetChild(i);

		mImpl->ProcessNode(fbxChildNode);
	}
}

void FbxParser::getSDKFileVersion(int32 & sdkMajor, int32 & sdkMinor, int32 & sdkRevision)
{
	FbxManager::GetFileFormatVersion(sdkMajor,sdkMinor,sdkRevision);
}

void FbxParser::getFileVersion(int32 & fileMajor, int32 & fileMinor, int32 & fileRevision)
{
	if (!mImpl->mImporterInitialized)
	{
		wxMessageBox(wxT("Importer is not initialized."));
		return;
	}

	mImpl->mFbxImporter->GetFileVersion(fileMajor,fileMinor,fileRevision);
}

usize FbxParser::getVertexCount()
{
	return mImpl->mVertexCount;
}

usize FbxParser::getIndexCount()
{
	return mImpl->mIndices.size();
}

void FbxParser::getPositions(real_type* positionArray,usize arraySize)
{
	usize vertexCount = getVertexCount();
	BOOST_ASSERT(arraySize >= vertexCount * 3);

	for (usize i = 0; i < vertexCount; ++i)
	{
		FbxVector4 v = mImpl->mVertices[i];

		//后置递增运算符的优先级高于解引用运算符。先++，然后返回之前值的副本。
		*positionArray++ = v[0];
		*positionArray++ = v[1];
		*positionArray++ = v[2];
	}
}

void FbxParser::getPositions(std::vector<real_type>& vec)
{
	for (int i = 0; i < mImpl->mVertexCount; ++i)
	{
		FbxVector4 v = mImpl->mVertices[i];

		vec.push_back(v[0]);
		vec.push_back(v[1]);
		vec.push_back(v[2]);
	}
}

void FbxParser::getIndices(uint16* indexArray, usize arraySize)
{
	usize indexCount = getIndexCount();
	BOOST_ASSERT(arraySize >= indexCount);

	for (usize i = 0; i < indexCount; ++i)
	{
		//后置递增运算符的优先级高于解引用运算符。先++，然后返回之前值的副本。
		*indexArray++ = mImpl->mIndices[i];
	}
}

void FbxParser::getIndices(std::vector<uint16>& vec)
{
	for (auto const& i : mImpl->mIndices)
	{
		vec.push_back(i);
	}
}
*/