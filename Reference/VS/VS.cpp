/*
第二章：
Solution Explorer工具窗口(Ctrl+Alt+L)。

要将多个项目设为启动项目，可以从解决方案节点的右击菜单中选择Properties命令，再打开Solution Properties
对话框。

在一些环境设置下，当解决方案中只有一个项目时，解决方案节点是不可见的。这样就不能打开Solution Properties
窗口。为了显示解决方案节点，选择Tools | Options，在打开的Options对话框中选择Projects and Solutions
节点中的Always Show Solution项。

如果项目中还没有类图，单击View Class Diagram按钮可以插入一个类图，并自动添加所有的类。对于有许多
类的项目而言，这相当耗时，而且得到一个大却不实用的类图。一般最好是手动添加一个或多个类图，从而进行
全面控制。
*/