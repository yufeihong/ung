/*
Visitor(访问者)---对象行为型模式

意图：
表示一个作用于某对象结构中的各元素的操作。它使你可以在不改变各元素的类的前提下定义作用于这些元素的新操作。

动机：
将相关的操作包装在一个独立的对象（Visitor）中，将此对象传递给当前访问的元素。当一个元素“接受”该访问者时，该元素向访问者发送一个包含自身类信息的请求，该
请求同时也将该元素本身作为一个参数。Accept操作接受一个ConcreteVisitor对象为参数，每个ConcreteElement在实现Accept操作的时候，会回调访问者中针对该
ConcreteElement类的具体操作。Visitor必须为每一个ConcreteElement类定义一个操作。使用Visitor模式，必须定义两个类层次：一个对应于接受操作的元素，另一个
对应于定义对元素的操作的访问者。给访问者类层次增加一个新的子类即可创建一个新的操作。

适用性：
1，一个对象结构包含很多类对象，它们有不同的接口，而你想对这些对象实施一些依赖于其具体类的操作。
2，需要对一个对象结构中的对象进行很多不同的并且不相关的操作，而你想避免让这些操作“污染”这些对象的类。Visitor使得你可以将相关的操作集中起来定义在一个类中。
当该对象结构被很多应用共享时，用Visitor模式让每个应用仅包含需要用到的操作。
3，定义对象结构的类很少改变，但经常需要在此结构上定义新的操作。改变对象结构类需要重定义对所有访问者的接口，这可能需要很大的代价。如果对象结构类经常改变，
那么可能还是在这些类中定义这些操作较好。

参与者：
Visitor，为对象结构中ConcreteElement的每一个类声明一个Visitor操作。该操作的名字和特征标识了发送Visit请求给该访问者的那个类。这使得访问者可以确定正被访问
元素的具体的类。这样访问者就可以通过该元素的特定接口直接访问它。
ConcreteVisitor，实现每个由Visitor声明的操作。
Element，定义一个Accept操作，它以一个访问者为参数。
ConcreteElement，实现Accept操作，该操作以一个访问者为参数。
ObjectStructure，能枚举它的元素。可以提供一个高层的接口以允许访问者访问其元素。可以是一个复合（Composite）或是一个集合，如一个列表或一个无序集合。

协作：
1，一个使用Visitor模式的客户必须创建一个ConcreteVisitor对象，然后遍历对象结构，并用该访问者访问每一个元素。
2，当一个元素被访问时，它调用对应于它的类的Visitor操作。如果必要，该元素将自身作为这个操作的一个参数以便该访问者访问它的状态。

效果：
1，访问者模式使得易于增加新的操作。
访问者使得增加依赖于复杂对象结构的构件的操作变得容易了。仅需增加一个新的访问者即可在一个对象结构上定义一个新的操作。相反，如果每个功能都分散在多个类之上
的话，定义新的操作时必须修改每一类。
2，访问者集中相关的操作而分离无关的操作。
相关的行为不是分布在定义对象结构的各个类上，而是集中在一个访问者中。无关行为却被分别放在它们各自的访问者子类中。这就既简化了这些元素的类，也简化了在这些
访问者中定义的算法。所有与它的算法相关的数据结构都可以被隐藏在访问者中。
3，增加新的ConcreteElement类很困难。
Visitor模式使得难以增加新的Element的子类。每添加一个新的ConcreteElement都要在Visitor中添加一个新的抽象操作，并在每一个ConcreteVisitor类中实现相应的操作。
有时可以在Visitor中提供一个缺省的实现，这一实现可以被大多数的ConcreteVisitor继承。所以在应用访问者模式时考虑关键的问题是系统的哪个部分会经常变化，是作用
于对象结构上的算法呢还是构成该结构的各个对象的类。如果老是有新的ConcreteElement类加入进来的话，Visitor类层次将变得难以维护。在这种情况下，直接在构成该
结构的类中定义这些操作可能更容易一些。如果Element类层次是稳定的，而你不断地增加操作或修改算法，访问者模式可以帮助你管理这些改动。
4，通过类层次进行访问。
迭代器不能对具有不同元素类型的对象结构进行操作，这就意味着所有迭代器能够访问的元素都有一个共同的父类。访问者没有这种限制，它可以访问不具有相同父类的对象。
可以对一个Visitor接口增加任何类型的对象，例如：
class Visitor
{
public:
	void visitMyType(MyType*);
	void visitYourType(YourType*);
};
其中MyType和YourType可以完全无关。
5，破坏封装。
访问者方法假定ConcreteElement接口的功能足够强，足以让访问者进行它们的工作。结果是，该模式常常迫使你提供访问元素内部状态的公共操作，这可能会破坏它的封装
性。

实现：
每一个对象结构将有一个相关的Visitor类。这个抽象的访问者类为定义对象结构的每一个ConcreteElement类声明了一个VisitConcreteElement操作。每一个Visitor上的
Visit操作声明它的参数为一个特定的ConcreteElement，以允许该Visitor直接访问ConcreteElement的接口。ConcreteVisitor类重定义每一个Visit操作，从而为相应的
ConcreteElement类实现与特定访问者相关的行为。
class Visitor
{
public:
	virtual ~Visitor();

	virtual void VisitElementA(ElementA*);
	virtual void VisitElementB(ElementB*);
	//也可以通过重载来把函数都命名为Visit

protected:
	Visitor();
};
每个ConcreteElement类实现一个Accept操作，这个操作调用访问者中相应于本ConcreteElement类的Visit...的操作。这样最终得到调用的操作不仅依赖于该元素的类也
依赖于访问者的类。
class Element
{
public:
	virtual ~Element();

	virtual void Accept(Visitor&) = 0;

protected:
	Element();
};

class ElementA : public Element
{
public:
	ElementA();
	virtual ~ElementA();

	virtual void Accept(Visitor& v)
	{
		v.VisitElementA(this);
	}
};

class ElementB : public Element
{
public:
	ElementB();
	virtual ~ElementB();

	virtual void Accept(Visitor& v)
	{
		v.VisitElementB(this);
	}
};

一个CompositeElement类可能像这样实现Accept：
class CompositeElement : public Element
{
public:
	virtual void Accept(Visitor& v)
	{
		ListIterator<Element*> i(_children);

		for (i.First(); !i.IsDone(); i.Next())
		{
			i.CurrentItem()->Accept(v);
		}

		v.VisitCompositeElement(this);
	}

private:
	List<Element*>* _children;
};
双分派（Double-dispatch）。
访问者模式允许你不改变类即可有效地增加其上的操作。为达到这一效果使用了一种称为双分派的技术。双分派意味着得到执行的操作决定于请求的种类和两个接收者的类型。
Accept是一个双分派操作，它的含义决定于两个类型：Visitor的类型和Element的类型。双分派使得访问者可以对每一类的元素的请求有不同的操作。这是Visitor模式的关键
所在：得到执行的操作不仅决定于Visitor的类型还决定于它访问的Element的类型。可以不将操作静态地绑定在Element接口中，而将其安放在一个Visitor中，并使用Accept
在运行时进行绑定。扩展Element接口就等于定义一个新的Visitor子类而不是多个新的Element子类。
谁负责遍历对象结构。
一个访问者必须访问这个对象结构的每一个元素。问题是，它怎样做？我们可以将遍历的责任放到下面三个地方中的任意一个：对象结构中，访问者中，或一个独立的迭代器
对象中。通常由对象结构负责迭代。一个集合只需对它的元素进行迭代，并对每一个元素调用Accept操作。而一个复合通常让Accept操作遍历该元素的各子构件并对它们中
的每一个递归地调用Accept。

相关模式：
Composite：访问者可以用于对一个由Composite模式定义的对象结构进行操作。
*/