/*
Adapter(适配器)---类对象结构型模式

意图：
将一个类的接口转换成客户希望的另外一个接口。Adapter模式使得原本由于接口不兼容而不能一起工作的那些类可以一起工作。

别名：
包装器Wrapper。

适用性：
1，你想使用一个已经存在的类，而它的接口不符合你的需求。
2，你想创建一个可以复用的类，该类可以与其他不相关的类或不可预见的类协同工作。
3，（仅适用于对象Adapter）你想使用一些已经存在的子类，但是不可能对每一个都进行子类化以匹配它们的接口。对象适配器可以适配它的父类接口。

参与者：
Target，定义Client使用的与特定领域相关的接口。
Client，与符合Target接口的对象协同。
Adaptee，定义一个已经存在的接口，这个接口需要适配。
Adapter，对Adaptee的接口与Target接口进行适配。

效果：
类适配器：
1，用一个具体的Adapter类对Adaptee和Target进行匹配。结果是当我们想要匹配一个类以及所有它的子类时，类Adapter将不能胜任工作。
2，使得Adapter可以重定义Adaptee的部分行为，因为Adapter是Adaptee的一个子类。
3，仅仅引入了一个对象，并不需要额外的指针以间接得到Adaptee。
对象适配器：
1，允许一个Adapter与多个Adaptee---即Adaptee本身以及它的所有子类同时工作。Adapter也可以一次给所有的Adaptee添加功能。
2，使得重定义Adaptee的行为比较困难。这就需要生成Adaptee的子类并且使得Adapter引用这个子类而不是引用Adaptee本身。

实现：
使用C++实现适配器类。
在使用C++实现适配器类时，Adapter类应该采用public方式继承Target类，并且用private方式继承Adaptee类。因此，Adapter类应该是Target的子类型，但不是Adaptee
的子类型。

相关模式：
Bridge模式的结构与对象适配器类似，但是Bridge模式的出发点不同：Bridge目的是将接口部分和实现部分分离，从而对它们可以较为容易也相对独立的加以改变。而Adapter
则意味着改变一个已有对象的接口。
Decorator模式增强了其他对象的功能而同时又不改变它的接口。因此Decorator对应用程序的透明性比适配器要好。结果是Decorator支持递归组合，而纯粹是用适配器是
不可能实现这一点的。
Proxy模式在不改变它的接口的条件下，为另一个对象定义了一个代理。
*/