/*
Factory Method（工厂方法）---对象创建型模式

意图：
定义一个用于创建对象的接口，让子类决定实例化哪一个类。Factory Method使一个类的实例化延迟到其子类。

别名：
虚构造器（virtual constructor）

动机：
考虑这样一个应用框架，它可以向用户显示多个文档。在这个框架中，两个主要的抽象是类Application和Document。这两个类都是抽象的，客户必须通过它们的子类来做
与具体应用相关的实现。例如，为创建一个绘图应用，我们定义类DrawingApplication和DrawingDocument。Application类负责管理Document并根据需要创建它们---
例如，当用户从菜单中选择Open或New的时候。
因为被实例化的特定Document子类是与特定应用相关的，所以Application类不可能预测到哪个Document子类将被实例化---Application类仅知道一个新的文档何时应被
创建，而不知道哪一种Document将被创建。这就产生了一个尴尬的局面：框架必须实例化类，但是它仅知道不能被实例化的抽象类。
Factory Method模式提供了一个解决方案。它封装了哪一个Document子类将被创建的信息，并将这些信息从该框架中分离出来。
Application的子类重写了Application的抽象操作CreateDocument以返回适当的Document子类对象。一旦一个Application子类实例化以后，它就可以实例化与应用相关
的文档，而无需知道这些文档的类。我们称CreateDocument是一个工厂方法，因为它负责生产一个对象。

适用性：
1，当一个类不知道它所必须创建的对象的类的时候。
2，当一个类希望由它的子类来指定它所创建的对象的时候。
3，当类将创建对象的职责委托给多个帮助子类中的某一个，并且你希望将哪一个帮助子类是代理者这一信息局部化的时候。

参与者：
Product，定义工厂方法所创建的对象的接口。
ConcreteProduct，实现Product接口。
Creator，声明工厂方法，该方法返回一个Product类型的对象。Creator也可以定义一个工厂方法的缺省实现，它返回一个缺省的ConcreteProduct对象。可以调用工厂方法
以创建一个Product对象。
ConcreteCreator，重写工厂方法以返回一个ConcreteProduct实例。

效果：
工厂方法不再将与特定应用有关的类绑定到你的代码中。代码仅处理Product接口。
1，为子类提供挂钩（hook）。
用工厂方法在一个类的内部创建对象通常比直接创建对象更灵活。Factory Method给子类一个挂钩以提供对象的扩展版本。在Document的例子中，Document类可以定义
一个称为CreateFileDialog的工厂方法，该方法为打开一个已有的文档，来创建一个默认的文件对话框对象。Document的子类可以重写这个工厂方法以定义一个与特定应用
相关的文件对话框。在这种情况下，工厂方法就不再抽象了而是提供了一个合理的缺省实现。
2，连接平行的类层次。
工厂方法往往并不只是被Creator调用，客户可以找到一些有用的工厂方法，尤其是在平行类层次的情况下。当一个类将它的一些职责委托给一个独立的类的时候，就产生了
平行类层次。考虑用鼠标操纵一些图形，移动，伸展，旋转等。这通常需要存储和更新在给定时刻的一些操纵状态的信息，这个状态仅仅在操纵时需要，因此它不应该被保存
在图形对象中。此外，当用户操纵图形时，不同的图形有不同的行为。所以最好使用一个独立的Manipulator对象实现交互并保存所需要的任何与特定操纵相关的状态。不同
的图形将使用不同的Manipulator子类来处理特定的交互。得到的Manipulator类层次与Figure类层次是平行的（至少部分平行）。Figure类提供了一个CreateManipulator
工厂方法，它使得客户可以创建一个与Figure相对应的Manipulator。Figure子类重写该方法以返回一个合适的Manipulator子类实例。

实现：
1，主要有两种不同的情况。
	a，Creator类是一个抽象类并且不提供它所声明的工厂方法的实现。
	b，Creator是一个具体的类而且为工厂方法提供一个缺省的实现。也有可能是一个定义了缺省实现的抽象类，但这不太常见。
2，参数化工厂方法。
工厂方法采用一个标识要被创建的对象种类的参数。工厂方法创建的所有对象将共享Product接口。在Document的例子中，Application可能支持不同种类的Document，你
给CreateDocument传递一个外部参数来指定将要创建的文档的种类。
3，构造器只是将产品初始化为0，而不是创建一个具体产品。一定不要在Creator的构造器中调用工厂方法---在ConcreteCreator中该方法还不可用。
4，使用模板以避免创建子类。
工厂方法的一个潜在问题是它们可能仅为了创建适当的Product对象而迫使你创建Creator子类。在C++中的一个解决方法是提供Creator的一个模板子类，它使用Product类
作为模板参数：
class Creator
{
public:
	virtual Product* CreateProduct() = 0;
};

template<typename TheProduct>
class StandardCreator : public Creator
{
public:
	virtual Product* CreateProduct();
};

template<typename TheProduct>
Product* StandardCreator<TheProduct>::CreateProduct()
{
	return new TheProduct;
}
使用这个模板，客户仅提供产品类---而不需要创建Creator的子类。

相关模式：
Abstract Factory经常用工厂方法来实现。
工厂方法通常在Template Methods中被调用。
*/