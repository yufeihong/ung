/*
Prototype(原型)---对象创建型模式

意图：
用原型实例指定创建对象的种类，并且通过拷贝这些原型创建新的对象。

适用性：
当一个系统应该独立于它的产品创建、构成和表示时，要使用Prototype模式。
当要实例化的类是在运行时刻指定时，例如，通过动态装载。
为了避免创建一个与产品类层次平行的工厂类层次时。
当一个类的实例只能有几个不同状态组合中的一种时。建立相应数目的原型并克隆它们可能比每次用合适的状态手工实例化该类更方便一些。

参与者：
Prototype，声明一个克隆自身的接口。
ConcretePrototype，实现一个克隆自身的操作。
Client，让一个原型克隆自身从而创建一个新的对象。

协作：
客户请求一个原型克隆自身。

效果：
Prototype有许多和Abstract Factory、Builder一样的效果：它对客户隐藏了具体的产品类，因此减少了客户知道的名字的数目。此外，这些模式使客户无需改变即可使用
与特定应用相关的类。
1，运行时刻增加和删除产品。
Prototype允许只通过客户注册原型实例就可以将一个新的具体产品类并入系统。它比其他创建型模式更为灵活，因为客户可以在运行时刻建立和删除原型。
2，改变值以指定新对象。
高度动态的系统允许你通过对象复合定义新的行为---例如，通过为一个对象变量指定值---并且不定义新的类。你通过实例化已有类并且将这些实例注册为客户对象的原型，
就可以有效定义新类别的对象。客户可以将职责代理给原型，从而表现出新的行为。这种设计使得用户无需编程即可定义新“类”。实际上，克隆一个原型类似于实例化一个类。
Prototype模式可以极大的减少系统所需要的类的数目。
3，改变结构以指定新对象。
许多应用由部件和子部件来创建对象。
4，减少子类的构造。
Factory Method经常产生一个与产品类层次平行的Creator类层次。Prototype模式使得你克隆一个原型而不是请求一个工厂方法去产生一个新的对象。因此你根本不需要
Creator类层次。
5，用类动态配置应用。
一些运行时刻环境允许你动态将类装载到应用中，Prototype模式是利用这种功能的关键。
6，Prototype的主要缺陷是每一个Prototype的子类都必须实现Clone操作，这可能很困难。例如，当所考虑的类已经存在时就难以新增Clone操作。当内部包括一些不支持
拷贝的对象时，便无法实现克隆。

当实现原型时，要考虑下面一些问题：
1，使用一个原型管理器。
当一个系统中原型数目不固定时（也就是说，它们可以动态创建和销毁），要保持一个可用原型的注册表。客户不会自己来管理原型，但会在注册表中存储和检索原型。客户
在克隆一个原型前会向注册表请求该原型。我们称这个注册表为原型管理器。原型管理器是一个关联存储器，它返回一个与给定关键字相匹配的原型。它有一些操作可以用来
通过关键字注册原型和解除注册。客户可以在运行时更改或浏览这个注册表。
2，初始化克隆对象。
当一些客户对克隆对象已经相当满意时，另一些客户将会希望使用他们所选择的一些值来初始化该对象的一些或所有的内部状态。一般来说不可能在Clone操作中传递这些值，
因为这些值的数目由于原型的类的不同而不同。在Clone操作中传递参数会破坏克隆接口的统一性。

代码示例：
class MazePrototypeFactory
{
public:
	MazePrototypeFactory(Maze*, Wall*, Room*, Door*);

	Maze* MakeMaze() const;
	Room* MakeRoom(int) const;
	Wall* MakeWall() const;
	Door* MakeDoor(Room*, Room*) const;

private:
	Maze* prototypeMaze;
	Room* prototypeRoom;
	Wall* prototypeWall;
	Door* prototypeDoor;
};

MazePrototypeFactory::MazePrototypeFactory(Maze* m, Wall* w, Room* r, Door* d) :
	prototypeMaze(m),
	prototypeWall(w),
	prototypeRoom(r),
	prototypeDoor(d)
{
}

Wall* MazePrototypeFactory::MakeWall() const
{
	return prototypeWall->Clone();
}

Door* MazePrototypeFactory::MakeDoor(Room* r1, Room* r2) const
{
	Door* door = prototypeDoor->Clone();
	door->Initialize(r1, r2);

	return door;
}

MazeGame game;
MazePrototypeFactory simpleMazeFactory(new Maze, new Wall, new Room, new Door);
Maze* maze = game.CreateMaze(simpleMazeFactory);

MazePrototypeFactory bombedMazeFactory(new Maze, new BombedWall, new RoomWithABomb, new Door);

相关模式：
Prototype和Abstract Factory在某种方面是相互竞争的。但是它们也可以一起使用。Abstract Factory可以存储一个被克隆的原型的集合，并且返回产品对象。
大量使用Composite和Decorator模式的设计通常也可以从Prototype模式处获益。
*/