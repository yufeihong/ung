/*
Decorator(装饰)---对象结构型模式

意图：
动态地给一个对象添加一些额外的职责。就增加功能来说，Decorator模式相比生成子类更为灵活。

别名：
包装器Wrapper

动机：
有时我们希望给某个对象而不是整个类添加一些功能。一种较为灵活的方式是将组件嵌入到另一个对象中，我们称这个嵌入的对象为装饰。这个装饰与它所装饰的组件接口一
致，因此它对使用该组件的客户透明。它将客户请求转发给该组件，并且可能在转发前后执行一些额外的动作。透明性使得你可以递归的嵌套多个装饰，从而可以添加任意多
的功能。例如，假定有一个对象TextView，它可以在窗口中显示正文。缺省的TextView没有滚动条，因为我们可能有时并不需要滚动条。当需要滚动条时，我们可以用
ScrollDecorator添加滚动条。如果我们还想在TextView周围添加一个粗黑边框，可以使用BorderDecorator添加。因此只要简单地将这些装饰和TextView进行组合，就可以
达到预期的效果。

适用性：
1，在不影响其他对象的情况下，以动态，透明的方式给单个对象添加职责。
2，处理那些可以撤销的职责。
3，当不能采用生成子类的方法进行扩充时。一种情况是，可能有大量独立的扩展，为支持每一种组合将产生大量的子类，使得子类数目呈爆炸性增长。另一种情况可能是因为
类定义被隐藏，或类定义不能用于生成子类。

参与者：
Component，定义一个对象接口，可以给这些对象动态地添加职责。
ConcreteComponent，定义一个对象，可以给这个对象添加一些职责。
Decorator，维持一个指向Component对象的指针，并定义一个与Component接口一致的接口。
ConcreteDecorator，向组件添加职责。

协作：
Decorator将请求转发给它的Component对象，并有可能在转发请求前后执行一些附加的动作。

效果：
1，比静态继承更灵活。
与对象的静态继承（多重继承）相比，Decorator模式提供了更加灵活的向对象添加职责的方式。可以用添加和分离的方法，用装饰在运行时刻增加和删除职责。相比之下，
继承机制要求为每个添加的职责创建一个新的子类，这会产生许多新的类，并且会增加系统的复杂度。此外，为一个特定的Component类提供多个不同的Decorator类，
这就使得你可以对一些职责进行混合和匹配。使用Decorator模式可以很容易地重复添加一个特性，例如在TextView上添加双边框时，仅需添加两个BorderDecorator即可，
而两次继承Border类则极容易出错。
2，避免在层次结构高层的类有太多的特征。
Decorator模式提供了一种“即用即付”的方法来添加职责。它并不试图在一个复杂的可定制的类中支持所有可预见的特征，相反，你可以定义一个简单的类，并且用Decorator
类给它逐渐地添加功能。可以从简单的部件组合出复杂的功能。这样，应用程序不必为不需要的特征付出代价。同时也更易于不依赖于Decorator所扩展（甚至是不可预知的
扩展）的类而独立地定义新类型的Decorator。扩展一个复杂类的时候，很可能会暴露与添加的职责无关的细节。
3，Decorator与它的Component不一样。
Decorator是一个透明的包装。如果我们从对象标识的观点出发，一个被装饰了的组件与这个组件是有差别的，因此，使用装饰时不应该依赖对象标识。
4，有许多小对象。
采用Decorator模式进行系统设计往往会产生许多看上去类似的小对象，这些对象仅仅在它们相互连接的方式上有所不同，而不是它们的类或它们的属性值有所不同。

实现：
1，接口一致性。
装饰对象的接口必须与它所装饰的Component的接口是一致的，因此，所有的ConcreteDecorator类必须有一个公共的父类。
2，省略抽象的Decorator类。
当你仅需要添加一个职责时，没有必要定义抽象Decorator类。可以把Decorator向Component转发请求的职责合并到ConcreteDecorator中。
3，保持Component类的简单性。
为了保证接口的一致性，组件和装饰必须有一个公共的Component父类。因此保持这个类的简单性是很重要的，即，它应集中于定义接口而不是存储数据。对数据表示的定义
应延迟到子类中，否则Component类会变得过于复杂和庞大，因而难以大量使用。赋予Component太多的功能也使得具体的子类有一些它们并不需要的功能。
4，改变对象外壳与改变对象内核。
我们可以将Decorator看作一个对象的外壳，它可以改变这个对象的行为。另外一种方法是改变对象的内核，例如，Strategy模式。
当Component类原本就很庞大时，使用Decorator模式的代价太高，Strategy模式相对更好一些。在Strategy模式中，组件将它的一些行为转发给一个独立的策略对象，我们
可以替换strategy对象，从而改变或扩充组件的功能。Decorator模式仅从外部改变组件，因此组件无需对它的装饰有任何了解，也就是说，这些装饰对该组件是透明的。在
Strategy模式中，component组件本身知道可能进行哪些扩充，因此它必须引用并维护相应的策略。基于Strategy的方法可能需要修改component组件以适应新的扩充。另
一方面，一个策略可以有自己特定的接口，而装饰的接口则必须与组件的接口一致。Component类很庞大时，策略可以很小。

代码示例：
class Component
{
public:
	Component();

	virtual void Draw();
	virtual void Resize();
};

class Decorator : public Component
{
public:
	Decorator(Component*);

	virtual void Draw();
	virtual void Resize();

private:
	Component* mComponent;
};

Decorator装饰由mComponent实例变量引用的Component，这个实例变量在构造器中被初始化。对于Component接口中定义的每一个操作，Decorator类都定义了一个
缺省的实现，这一实现将请求转发给mComponent：

void Decorator::Draw()
{
	mComponent->Draw();
}

void Decorator::Resize()
{
	mComponent->Resize();
}

class BorderDecorator : public Decorator
{
public:
	BorderDecorator(Component*, int borderWidth);

	virtual void Draw()
	{
		Decorator::Draw();
		DrawBorder(mWidth);
	}

private:
	void DrawBorder(int);

	int mWidth;
};

相关模式：
Decorator模式不同于Adapter模式，因为装饰仅改变对象的职责而不改变它的接口，而适配器将给对象一个全新的接口。
可以将Decorator视为一个退化的，仅有一个组件的组合。然而，装饰仅给对象添加一些额外的职责---它的目的不在于对象聚集。
用一个装饰你可以改变对象的外表，而Strategy模式使得你可以改变对象的内核。这是改变对象的两种途径。
*/