/*
Strategy(策略)---对象行为型模式

意图：
定义一系列的算法，把它们一个个封装起来，并且使它们可相互替换。本模式使得算法可独立于使用它的客户而变化。

别名：
政策（Policy）

动机：
以这种方法封装的算法称为一个策略（Strategy）。

适用性：
1，许多相关的类仅仅是行为有异。策略提供了一种用多个行为中的一个行为来配置一个类的方法。
2，需要使用一个算法的不同变体。
3，算法使用客户不应该知道的数据。可使用策略模式以避免暴露复杂的，与算法相关的数据结构。
4，一个类定义了多种行为，并且这些行为在这个类的操作中以多个条件语句的形式出现。将相关的条件分支移入它们各自的Strategy类中以代替这些条件语句。

参与者：
Strategy，定义所有支持的算法的公共接口。Context使用这个接口来调用某ConcreteStrategy定义的算法。
ConcreteStrategy，以Strategy接口实现某具体算法。
Context，用一个ConcreteStrategy对象来配置。维护一个对Strategy对象的引用。可定义一个接口来让Strategy访问它的数据。

协作：
Strategy和Context相互作用以实现选定的算法。当算法被调用时，Context可以将该算法所需要的所有数据都传递给该Strategy。或者，Context可以将自身作为一个参数
传递给Strategy操作。这就让Strategy在需要时可以回调Context。
Context将它的客户的请求转发给它的Strategy。客户通常创建并传递一个ConcreteStrategy对象给该Context。这样，客户仅与Context交互。通常有一系列的ConcreteStrategy
类可供客户从中选择。

实现：
1，定义Strategy和Context接口。
Strategy和Context接口必须使得ConcreteStrategy能够有效的访问它所需要的Context中的任何数据，反之亦然。一种办法是让Context将数据放在参数中传递给Strategy
操作---也就是说，将数据发送给Strategy。这使得Strategy和Context解耦。但另一方面，Context可能发送一些Strategy不需要的数据。另一种办法是让Context将自身
作为一个参数传递给Strategy，该Strategy再显式地向该Context请求数据。或者，Strategy可以存储对它的Context的一个引用，这样根本不再需要传递任何东西。这两种
情况下，Strategy都可以请求到它所需要的数据。但现在Context必须对它的数据定义一个更为精细的接口，这将Strategy和Context更紧密地耦合在一起。
2，将Strategy作为模板参数。
可利用模板机制用一个Strategy来配置一个类。然而这种技术仅当下面条件满足时才可以使用，1，可以在编译时选择Strategy，2，它不需在运行时改变。在这种情况下，要
被配置的类（Context）被定义为以一个Strategy类作为一个参数的模板类。

相关模式：
Flyweight：Strategy对象经常是很好地轻量级对象。
*/