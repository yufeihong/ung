//#define SUITE_USM

#ifdef SUITE_USM

#include "UBTConfig.h"

#include "USM.h"

using namespace ung;

BOOST_AUTO_TEST_SUITE(suite_usm)

//深度优先遍历（前序）
BOOST_AUTO_TEST_CASE(case_usm_1)
{
	return;

	std::cout << "case_usm_1" << std::endl;

	auto fn = [](StrongSceneNodePtr p)
	{
		std::cout << p->getName() << ",";
	};

	//ubsInit();
	//usmInit();
	//场景管理器
	auto& sm = SceneManager<ST_EXTERIOR>::getInstance();

	//根节点
	auto root = sm.getSceneRootNode();

	auto n1 = sm.createSceneNode(root, "1");

	auto n7 = sm.createSceneNode(root, "7");
	auto n2 = sm.createSceneNode(n1, "2");
	auto n3 = sm.createSceneNode(n1, "3");

	auto n10 = sm.createSceneNode(root, "10");
	auto n8 = sm.createSceneNode(n7, "8");
	auto n11 = sm.createSceneNode(n10, "11");
	auto n12 = sm.createSceneNode(n10, "12");
	auto n16 = sm.createSceneNode(n10, "16");

	auto n4 = sm.createSceneNode(n3, "4");
	auto n5 = sm.createSceneNode(n3, "5");
	auto n9 = sm.createSceneNode(n8, "9");
	auto n13 = sm.createSceneNode(n12, "13");
	auto n15 = sm.createSceneNode(n12, "15");

	auto n6 = sm.createSceneNode(n5, "6");
	auto n14 = sm.createSceneNode(n13, "14");

	root->preTraverseDepthFirst(fn);
	std::cout << std::endl;
	root->postTraverseDepthFirst(fn);
	std::cout << std::endl;
}

//创建对象，节点，关联对象和节点，关联节点和节点
BOOST_AUTO_TEST_CASE(case_usm_2)
{
	return;

	std::cout << "case_usm_2" << std::endl;

	ubsInit();
	usmInit();
	//场景管理器
	auto& sm = SceneManager<ST_EXTERIOR>::getInstance();

	//根节点
	auto root = sm.getSceneRootNode();

	//创建对象1
	auto object1 = sm.createObject("../../../Resource/Script/XML/Entities/test.xml","object1");
	//销毁对象1
	sm.destroyObject(object1);

	//创建对象2
	auto object2 = sm.createObject("../../../Resource/Script/XML/Entities/test.xml", "object2");
	BOOST_CHECK(sm.getAllObjectCount() == 1);
	//连接对象2到场景图根节点
	object2->attachToSceneNode(root);
	//销毁对象2
	sm.destroyObject(object2);
}

//销毁节点
BOOST_AUTO_TEST_CASE(case_usm_3)
{
	return;

	std::cout << "case_usm_3" << std::endl;

	ubsInit();
	usmInit();
	//场景管理器
	auto& sm = SceneManager<ST_EXTERIOR>::getInstance();

	BOOST_CHECK(sm.getAllSceneNodeCount() == 1);

	//根节点
	auto root = sm.getSceneRootNode();

	//创建节点1
	auto node1 = sm.createSceneNode(root,"node1");
	BOOST_CHECK_EQUAL(root->getChildrenCount(), 1);

	//创建对象1
	auto object1 = sm.createObject("../../../Resource/Script/XML/Entities/test.xml", "object1");
	//把对象1关联到节点1上
	object1->attachToSceneNode(node1);
	//创建对象2
	auto object2 = sm.createObject("../../../Resource/Script/XML/Entities/test.xml", "object2");
	//把对象2关联到节点1上
	object2->attachToSceneNode(node1);

	//创建节点2
	auto node2 = sm.createSceneNode(root,"node2");
	//创建对象3
	auto object3 = sm.createObject("../../../Resource/Script/XML/Entities/test.xml", "object3");
	//把对象3关联到节点2上
	object3->attachToSceneNode(node2);

	//创建节点3
	auto node3 = sm.createSceneNode(node2,"node3");
	//创建节点4
	auto node4 = sm.createSceneNode(node2,"node4");
	//创建节点5
	auto node5 = sm.createSceneNode(node3,"node5");
	//创建对象4
	auto object4 = sm.createObject("../../../Resource/Script/XML/Entities/test.xml", "object4");
	//把对象4关联到节点4上
	object4->attachToSceneNode(node4);
	//创建节点6
	auto node6 = sm.createSceneNode(node1,"node6");
	//创建节点7
	auto node7 = sm.createSceneNode(node3,"node7");
	//创建节点8
	auto node8 = sm.createSceneNode(node5,"node8");
	//创建节点9
	auto node9 = sm.createSceneNode(node8,"node9");
	//创建对象5
	auto object5 = sm.createObject("../../../Resource/Script/XML/Entities/test.xml", "object5");
	//把对象5关联到节点6上
	object5->attachToSceneNode(node6);
	//创建对象6
	auto object6 = sm.createObject("../../../Resource/Script/XML/Entities/test.xml", "object6");
	//把对象6关联到节点7上
	object6->attachToSceneNode(node7);
	//创建对象7
	auto object7 = sm.createObject("../../../Resource/Script/XML/Entities/test.xml", "object7");
	//把对象7关联到节点3上
	object7->attachToSceneNode(node3);
	//创建对象8
	auto object8 = sm.createObject("../../../Resource/Script/XML/Entities/test.xml", "object8");
	//把对象8关联到节点3上
	object8->attachToSceneNode(node3);
	//创建对象9
	auto object9 = sm.createObject("../../../Resource/Script/XML/Entities/test.xml", "object9");
	//把对象9关联到节点9上
	object9->attachToSceneNode(node9);
	//创建对象10
	auto object10 = sm.createObject("../../../Resource/Script/XML/Entities/test.xml", "object10");
	//把对象10关联到节点2上
	object10->attachToSceneNode(node2);
}

//移动节点
BOOST_AUTO_TEST_CASE(case_usm_4)
{
	std::cout << "case_usm_4" << std::endl;

	//------------------------------

	auto& ung = Root::getInstance();
	//ubsInit();
	//usmInit();
	//场景管理器
	auto& sm = SceneManager<ST_EXTERIOR>::getInstance();

	//BOOST_CHECK(sm->getAllSceneNodeCount() == 1);

	////根节点
	//auto root = sm->getSceneRootNode();

	////创建节点1
	//auto node1 = sm->createSceneNode(root,"node1");

	////创建节点2
	//auto node2 = sm->createSceneNode(node1,"node2");

	////获取根节点的world
	//auto rootP = root->getInWorldPosition();
	//auto rootO = root->getInWorldOrientation();
	//std::cout << "root WP:" << rootP << std::endl;
	//std::cout << "root WO:" << rootO << std::endl;
	//BOOST_CHECK(rootP == Vector4(0,0,0,1));
	//BOOST_CHECK(rootO == Vector3(0,0,1));

	////设置节点1相对于根节点在x轴偏移了1个单位距离
	//node1->setTranslate(1, 0, 0);
	////设置节点1相对于根节点绕y轴旋转90度
	//node1->setRotate(Vector3(0, 1, 0), Constants::HALFPI);
	////更新节点1
	//node1->update();
	////获取节点1的world
	//auto node1P = node1->getInWorldPosition();
	//auto node1O = node1->getInWorldOrientation();
	//std::cout << "node1 WP:" << node1P << std::endl;
	//std::cout << "node1 WO:" << node1O << std::endl;
	//BOOST_CHECK(node1P == Vector4(1, 0, 0, 1));
	//BOOST_CHECK(node1O == Vector3(1, 0, 0));

	////设置节点2相对于节点1在y轴偏移了1个单位距离
	//node2->setTranslate(0, 1, 0);

	////创建对象1
	//auto object1 = sm->createObject("../../../Resource/Script/XML/Entities/test.xml", "object1");
	////把对象1关联到节点2上
	//object1->attachToSceneNode(node2);

	////移动和旋转对象1
	//(object1->getComponent<TransformComponent>("TransformComponent")).lock()->setTranslate(0, 5, 0);
	//(object1->getComponent<TransformComponent>("TransformComponent")).lock()->setRotate(Vector3(0, 1, 0), -Constants::HALFPI);

	////更新节点2
	//node2->update();

	////获取节点2的world
	//auto node2P = node2->getInWorldPosition();
	//auto node2O = node2->getInWorldOrientation();
	//std::cout << "node2 WP:" << node2P << std::endl;
	//std::cout << "node2 WO:" << node2O << std::endl;
	//BOOST_CHECK(node2P == Vector4(1, 1, 0, 1));
	//BOOST_CHECK(node2O == Vector3(1, 0, 0));

	////获取对象1的world
	//auto tc = (object1->getComponent<TransformComponent>("TransformComponent")).lock();
	//auto object1P = tc->getInWorldPosition();
	//auto object1O = tc->getInWorldOrientation();
	//std::cout << "object1 WP:" << object1P << std::endl;
	//std::cout << "object1 WO:" << object1O << std::endl;
	//BOOST_CHECK(object1P == Vector4(1, 6, 0, 1));
	//BOOST_CHECK(object1O == Vector3(0, 0, 1));

	////节点2脱离节点1
	//node2->detachFromParent();
	////节点2连接到根节点
	//node2->attachToParent(root);
	////更新节点2
	//node2->update();
	//std::cout << "-------------" << std::endl;
	////获取节点2的world
	//node2P = node2->getInWorldPosition();
	//node2O = node2->getInWorldOrientation();
	//std::cout << "node2 WP:" << node2P << std::endl;
	//std::cout << "node2 WO:" << node2O << std::endl;
	//BOOST_CHECK(node2P == Vector4(0, 1, 0, 1));
	//BOOST_CHECK(node2O == Vector3(0, 0, 1));
	////获取对象1的world
	//tc = (object1->getComponent<TransformComponent>("TransformComponent")).lock();
	//object1P = tc->getInWorldPosition();
	//object1O = tc->getInWorldOrientation();
	//std::cout << "object1 WP:" << object1P << std::endl;
	//std::cout << "object1 WO:" << object1O << std::endl;
	//BOOST_CHECK(object1P == Vector4(0, 6, 0, 1));
	//BOOST_CHECK(object1O == Vector3(-1, 0, 0));

	////对象1脱离节点2
	//object1->detachFromSceneNode();
	////对象1连接到节点1
	//object1->attachToSceneNode(node1);
	//std::cout << "-------------" << std::endl;
	////更新对象1
	//object1->update();
	////获取对象1的world
	//tc = (object1->getComponent<TransformComponent>("TransformComponent")).lock();
	//object1P = tc->getInWorldPosition();
	//object1O = tc->getInWorldOrientation();
	//std::cout << "object1 WP:" << object1P << std::endl;
	//std::cout << "object1 WO:" << object1O << std::endl;
	//BOOST_CHECK(object1P == Vector4(1, 5, 0, 1));
	//BOOST_CHECK(object1O == Vector3(0, 0, 1));

	//UNG_DEL sm;
	//BOOST_ASSERT(!sm);

	int aa1 = 0;
}

BOOST_AUTO_TEST_SUITE_END()

#endif//SUITE_USM