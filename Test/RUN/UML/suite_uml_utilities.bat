@echo off

::进入上层目录
cd "../"

::检查UBT_d.exe文件是否存在，进入UBT_d.exe文件所在的目录
call ubt.bat

::打印当前测试套件的名字
echo ==========%~n0==========

::执行当前测试套件
UBT_d.exe --run_test=%~n0 --detect_memory_leak=0

pause