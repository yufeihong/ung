//#define SUITE_UMM

#ifdef SUITE_UMM

#include "UBTConfig.h"

using namespace ung;

struct Data
{
	Data(uint32 v = 5)
	{
		mData = v;

		UNG_PRINT_ONE_LINE("Data()");
	}

	~Data()
	{
		UNG_PRINT_ONE_LINE("~Data()");
	}

	uint32 mData;
};

BOOST_AUTO_TEST_SUITE(suite_umm)

//AdapterAllocator
BOOST_AUTO_TEST_CASE(case_umm_1)
{
	auto pData = AdapterAllocator::allocate<Data>(ALLOCATE_INFO);

	AdapterAllocator::deallocate<Data>(pData);

	auto pData2 = AdapterAllocator::allocate<Data>(2, ALLOCATE_INFO);

	AdapterAllocator::deallocate<Data>(pData2, 2);
}

BOOST_AUTO_TEST_SUITE_END()

#endif//SUITE_UMM