//#define SUITE_DEP_TINYXML2

#ifdef SUITE_DEP_TINYXML2

#include "UBTConfig.h"

//测试夹具
struct fixture_dep_tinyxml2
{
	fixture_dep_tinyxml2()
	{
		doc.LoadFile("../../../Resource/Script/XML/Entities/box.xml");

		BOOST_ASSERT(doc.ErrorID() == 0);

		root = doc.RootElement();

		BOOST_ASSERT(root);
	}

	~fixture_dep_tinyxml2()
	{
	}

	XMLDocument doc;
	XMLElement* root;
};

BOOST_FIXTURE_TEST_SUITE(suite_dep_tinyxml2,fixture_dep_tinyxml2)

BOOST_AUTO_TEST_CASE(case_dep_tinyxml2)
{
	/*
	获取attribute中的值。
	*/
	double x = 0.0;
	double y = 0.0;
	double z = 0.0;
	XMLElement* pTransformComponentElement = root->FirstChildElement("TransformComponent");
	XMLElement* pPositionElement = pTransformComponentElement->FirstChildElement("Position");
	pPositionElement->QueryDoubleAttribute("x", &x);
	pPositionElement->QueryDoubleAttribute("y", &y);
	pPositionElement->QueryDoubleAttribute("z", &z);
	UBT_CHECK_EQUAL_FLOAT(x, 1.0);
	UBT_CHECK_EQUAL_FLOAT(y, 2.0);
	UBT_CHECK_EQUAL_FLOAT(z, 3.0);

	/*
	获取key-value，其中key被放置在一个element中，而value被放置在一个text node中，value node
	被key的element对给包裹着。
	*/
	double r = 0.0;
	double g = 0.0;
	double b = 0.0;
	double a = 0.0;
	XMLElement* pBoxRenderComponentElement = root->FirstChildElement("BoxRenderComponent");
	XMLElement* pColorElement = pBoxRenderComponentElement->FirstChildElement("Color");
	pColorElement->FirstChildElement("r")->QueryDoubleText(&r);
	pColorElement->FirstChildElement("g")->QueryDoubleText(&g);
	pColorElement->FirstChildElement("b")->QueryDoubleText(&b);
	pColorElement->FirstChildElement("a")->QueryDoubleText(&a);
	UBT_CHECK_EQUAL_FLOAT(r, 0.0);
	UBT_CHECK_EQUAL_FLOAT(g, 0.5);
	UBT_CHECK_EQUAL_FLOAT(b, 1.0);
	UBT_CHECK_EQUAL_FLOAT(a, 1.0);

	XMLElement* pPhysicsComponentElement = root->FirstChildElement("PhysicsComponent");
	/*
	获取element对中的text。
	*/
	XMLElement* pShapeElement = pPhysicsComponentElement->FirstChildElement("Shape");
	const char* shape = pShapeElement->GetText();
	BOOST_CHECK_EQUAL(shape,"box");

}

BOOST_AUTO_TEST_SUITE_END()

#endif//SUITE_DEP_TINYXML2

/*
所使用的xml文件：box.xml：
<Actor>

	<TransformComponent>
		<Position x="1.0" y="2.0" z="3.0"/>
		<Orientation degree="0.0"/>
	</TransformComponent>

	<BoxRenderComponent>
		<Color>
			<r>0.0</r>
			<g>0.5</g>
			<b>1.0</b>
			<a>1.0</a>
		</Color>
	</BoxRenderComponent>

	<PhysicsComponent>
		<Shape>box</Shape>
	</PhysicsComponent>

	<ScriptComponent>
		<ScriptObject constructor="AddSphere" destructor="RemoveSphere" />
	</ScriptComponent>

</Actor>

<!--
If you decide that this actor needs to have a brain, you can easily add an AI component 
without changing a single line of code. That’s the power of data-driven development.

Keep in mind that these actor XML files define the template for a type of actor,not a specific 
actor instance.

The XML file only defines the definition. You can think of it as defining a class for this type of 
actor.
-->
*/