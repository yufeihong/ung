/*!
 * \brief
 * 标准库容器包装的单元测试
 * \file UBT_UFC_STLWrapper.cpp
 *
 * \author Su Yang
 *
 * \date 2017/05/13
 */
//#define SUITE_UFC_STLWRAPPER
#ifdef SUITE_UFC_STLWRAPPER

#include "UBTConfig.h"
#include "UFCSTLWrapper.h"

#include <algorithm>

using namespace ung;

class Animal : public MemoryPool<Animal>
{
public:
	Animal(char cc) :
		c(cc)
	{
	}

	virtual ~Animal()
	{
		int a = 0;
	}

private:
	char c;
};

BOOST_AUTO_TEST_SUITE(suite_ufc_stlwrapper)

BOOST_AUTO_TEST_CASE(case_ufc_stlwrapper_1)
{
	STL_VECTOR(int) c1 { 1, 2, 3 };

	ungContainerClear(c1);

	STL_LIST(std::shared_ptr<int>) c2;
	c2.push_back(std::make_shared<int>(1));
	c2.push_back(std::make_shared<int>(2));
	c2.push_back(std::make_shared<int>(3));

	ungContainerClear(c2);

	STL_DEQUE(std::shared_ptr<Animal>) c3;
	{
		std::shared_ptr<Animal> p1(UNG_NEW_SMART Animal('a'));
		std::shared_ptr<Animal> p2(UNG_NEW_SMART Animal('b'));
		std::shared_ptr<Animal> p3(UNG_NEW_SMART Animal('c'));
		c3.push_back(p1);
		c3.push_back(p2);
		c3.push_back(p3);
	}

	ungContainerClear(c3);
}

BOOST_AUTO_TEST_CASE(case_ufc_stlwrapper_2)
{
	//wrapper支持范围for
	SetWrapper<int> set1{ 1,2,3 };

	for (auto const& e : set1)
	{
		std::cout << e << ",";
	}
	std::cout << std::endl;

	//----------------------------------------------

	STL_DEQUE(int) deq1 { 1, 2, 3 };

	auto lambdaExpress = [](int& i) {++i; };

	ungContainerLoop(deq1, lambdaExpress);

	for (auto const& e : deq1)
	{
		std::cout << e << ",";
	}
	std::cout << std::endl;
}

BOOST_AUTO_TEST_SUITE_END()

#endif//SUITE_UFC_STLWRAPPER