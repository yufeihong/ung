/*!
 * \brief
 * 泛化仿函数单元测试
 * \file UBT_UFC_GenericFunctor.cpp
 *
 * \author Su Yang
 *
 * \date 2017/04/23
 */
//#define SUITE_UFC_GENERICFUNCTOR

#ifdef SUITE_UFC_GENERICFUNCTOR

#include "UBTConfig.h"

using namespace ung;

void TestFunction(int i, double d)
{
	std::cout << "TestFunction(" << i << "," << d << ") called." << std::endl;
}

void TestFunction(int i)
{
	std::cout << "TestFunction(" << i << ") called." << std::endl;
}

class TestFunctor
{
public:
	void operator()(int i, double d)
	{
		std::cout << "TestFunctor::operator()(" << i << "," << d << ") called." << std::endl;
	}
};

class Dog
{
public:
	void bark()
	{
		std::cout << "bark..." << std::endl;
	}

	void sleep()
	{
		std::cout << "sleep..." << std::endl;
	}
};

BOOST_AUTO_TEST_SUITE(suite_ufc_genericfunctor)

BOOST_AUTO_TEST_CASE(case_ufc_genericfunctor_1)
{
	/*
	TestFunctor f;
	GenericFunctor<void, TYPELIST_2(int, double)> functor1(f);
	或：
	GenericFunctor<void, TYPELIST_2(int, double)> functor1(TestFunctor{});
	*/
	GenericFunctor<void, TYPELIST_2(int, double)> functor1(TestFunctor{});
	functor1(4, 5.6);

	typedef void(*FT)(int, double);
	FT f = TestFunction;
	GenericFunctor<void, TYPELIST_2(int, double)> functor2(f);
	functor2(6, 2.5);

	GenericFunctor<void, TYPELIST_1(int)> functor3(static_cast<void(*)(int)>(TestFunction));
	functor3(7);

	typedef void(Dog::*MemFT)();
	MemFT pActivity = &Dog::bark;
	Dog dog;
	Dog* pDog = &dog;
	(dog.*pActivity)();
	(pDog->*pActivity)();

	pActivity = &Dog::sleep;
	(dog.*pActivity)();

	GenericFunctor<void> cmd1(pDog,pActivity);
	cmd1();

	int a = 0;
}

BOOST_AUTO_TEST_SUITE_END()

#endif//SUITE_UFC_GENERICFUNCTOR