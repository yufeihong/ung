//#define SUITE_UFC_GENERICOBJECTFACTORY

#ifdef SUITE_UFC_GENERICOBJECTFACTORY

#include "UBTConfig.h"

using namespace ung;

//
#include "UFCSTLWrapper.h"
//

class Animal : public MemoryPool<Animal>
{
public:
	virtual ~Animal() = default;

private:
	char c;
};

class Dog : public Animal
{
public:
	double d;
	int data;
};

template<typename T>
class Cat : public Animal
{
public:
	T data;
};

BOOST_AUTO_TEST_SUITE(suite_ufc_genericobjectfactory)

BOOST_AUTO_TEST_CASE(case_ufc_genericobjectfactory_1)
{
	//定义一个对象工厂
	GenericObjectFactory<Animal, String> objectFactory;

	//对子类进行注册
	objectFactory.registerCreator<Dog>("Dog");
	objectFactory.registerCreator<Cat<int>>("Cat");

	//创建对象
	Animal* pDog = objectFactory.create("Dog");
	Animal* pCat = objectFactory.create("Cat");

	//T在内存中字节对齐的倍数。
	auto dog_am = boost::alignment_of<Dog>::value;
	auto cat_am = boost::alignment_of<Cat<int>>::value;

	//删除对象
	UNG_DEL(pCat);
	UNG_DEL(pDog);

	//
	List3<Vector3> A;

	{
		//List3<Vector3>::E eee = Vector3(1);

		A.push_back(Vector3(1));

		A.clear();
	}
	//A.push_back(UNG_NEW Animal);

	//A.clear();
}

BOOST_AUTO_TEST_SUITE_END()

#endif//SUITE_UFC_GENERICOBJECTFACTORY