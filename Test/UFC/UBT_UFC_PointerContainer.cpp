//#define SUITE_UFC_POINTERCONTAINER

#ifdef SUITE_UFC_POINTERCONTAINER

#include "UBTConfig.h"

using namespace ung;
using namespace std;

#include "UFCPointerContainerWrapper.h"

//顶点结构
struct Data : MemoryPool<Data>
{
	Data(uint32 v)
	{
		mData = v;
	}

	//不抛出异常的拷贝构造函数
	Data(Data const& r) noexcept :
		mData(r.mData)
	{
	}

	~Data()
	{
		UNG_PRINT_ONE_LINE("~Data()");
	}

	uint32 mData;
};

BOOST_AUTO_TEST_SUITE(suite_ufc_pointercontainer)

BOOST_AUTO_TEST_CASE(case_ufc_pointercontainer_1)
{
	PointerMapWrapper<int, Data> pm;

	auto a = 1;
	auto b = 2;
	pm.insert(a, UNG_NEW Data(1));
	auto ret = pm.insert(b, UNG_NEW Data(2));
	BOOST_ASSERT(ret.second);

	{
		auto first = pm.begin();
		auto br = pm.borrow(first);

		auto size = pm.size();
	}

	auto size = pm.size();

	int bk = 0;

	{
		auto k = 1;
		auto br = pm.borrow(k);

		auto size = pm.size();
	}

	size = pm.size();

	auto beg = pm.begin();
	auto ap = pm.release(beg);
	size = pm.size();

	auto pm2 = pm.clone();
	auto pm3(pm);
	auto pm4 = pm;
}

BOOST_AUTO_TEST_CASE(case_ufc_pointercontainer_2)
{
	PointerListWrapper<int> pl;
	pl.push_back(GLOBAL_NEW int(1));
	pl.push_back(GLOBAL_NEW int(3));
	pl.push_back(GLOBAL_NEW int(2));

	//参数不能为指针，不能为&，可以为int和int const&
	auto lb = [](int const& a, int const& b) ->bool {return a < b; };
	pl.sort(lb);

	{
		auto beg = pl.begin();
		auto br = pl.borrow(beg);

		auto size = pl.size();
	}

	auto size = pl.size();

	pl.replace(pl.begin(),GLOBAL_NEW int(4));

	pl.back();

	auto& ref = *(pl.begin());
}

BOOST_AUTO_TEST_SUITE_END()

#endif//SUITE_UFC_POINTERCONTAINER