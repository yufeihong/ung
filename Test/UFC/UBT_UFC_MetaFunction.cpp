/*!
 * \brief
 * 元函数单元测试
 * \file UBT_UFC_MetaFunction.cpp
 *
 * \author Su Yang
 *
 * \date 2017/04/23
 */
//#define SUITE_UFC_METAFUNCTION

#ifdef SUITE_UFC_METAFUNCTION

#include "UBTConfig.h"

using namespace ung;
using namespace ung::utp;

class Dog
{
public:
	using type = double;

	void bark()
	{
	}

	int age;
};

BOOST_AUTO_TEST_SUITE(suite_ufc_metafunction)

BOOST_AUTO_TEST_CASE(case_ufc_metafunction_1)
{
	using ret_type1 = typename ParameterType<bool>::type;
	BOOST_STATIC_ASSERT(boost::is_same<bool, ret_type1>::value);

	using ret_type2 = typename ParameterType<double>::type;
	BOOST_STATIC_ASSERT(boost::is_same<double, ret_type2>::value);

	using ret_type3 = typename ParameterType<int*>::type;
	BOOST_STATIC_ASSERT(boost::is_same<int*, ret_type3>::value);

	using ret_type4 = typename ParameterType<Dog*>::type;
	BOOST_STATIC_ASSERT(boost::is_same<Dog*, ret_type4>::value);

	using ret_type5 = typename ParameterType<std::shared_ptr<int>>::type;
	BOOST_STATIC_ASSERT(isSharedPtr<std::shared_ptr<int>>::value);
	std::cout << isSharedPtr<std::shared_ptr<int>>::value << std::endl;
	BOOST_STATIC_ASSERT(isSmartPtr<std::shared_ptr<int>>::value);
	BOOST_STATIC_ASSERT(boost::is_same<std::shared_ptr<int>, ret_type5>::value);
	std::cout << typeid(ret_type5).name() << std::endl;

	using ret_type6 = typename ParameterType<std::shared_ptr<Dog>>::type;
	BOOST_STATIC_ASSERT(boost::is_same<std::shared_ptr<Dog>, ret_type6>::value);

	using ret_type7 = typename ParameterType<int&>::type;
	BOOST_STATIC_ASSERT(boost::is_same<int&, ret_type7>::value);

	using ret_type8 = typename ParameterType<int const&>::type;
	BOOST_STATIC_ASSERT(boost::is_same<int const&, ret_type8>::value);

	using ret_type9 = typename ParameterType<Dog&>::type;
	BOOST_STATIC_ASSERT(boost::is_same<Dog&, ret_type9>::value);

	using ret_type10 = typename ParameterType<Dog const&>::type;
	BOOST_STATIC_ASSERT(boost::is_same<Dog const&, ret_type10>::value);

	using ret_type11 = typename ParameterType<int const>::type;
	BOOST_STATIC_ASSERT(boost::is_same<int const, ret_type11>::value);

	using ret_type12 = typename ParameterType<Dog const>::type;
	BOOST_STATIC_ASSERT(boost::is_const<Dog const>::value);
	BOOST_STATIC_ASSERT(boost::is_const<typename boost::remove_reference<ret_type12>::type>::value);
	BOOST_STATIC_ASSERT(boost::is_lvalue_reference<ret_type12>::value);
	BOOST_STATIC_ASSERT(boost::is_same<Dog const&, ret_type12&>::value);

	using ret_type13 = typename ParameterType<Dog>::type;
	BOOST_STATIC_ASSERT(boost::is_same<Dog const&, ret_type13>::value);

	//整形:bool,char,int,long(前面可以有signed,unsigned等关键字修饰)
	BOOST_STATIC_ASSERT(boost::is_integral<bool>::value);
	BOOST_STATIC_ASSERT(boost::is_integral<char>::value);
	BOOST_STATIC_ASSERT(boost::is_integral<int>::value);
	BOOST_STATIC_ASSERT(boost::is_integral<unsigned int>::value);
	BOOST_STATIC_ASSERT(boost::is_integral<long>::value);
	BOOST_STATIC_ASSERT(boost::is_integral<unsigned long>::value);
	BOOST_STATIC_ASSERT(boost::is_integral<int8>::value);
	BOOST_STATIC_ASSERT(boost::is_integral<uint8>::value);
	BOOST_STATIC_ASSERT(boost::is_integral<int16>::value);
	BOOST_STATIC_ASSERT(boost::is_integral<uint16>::value);
	BOOST_STATIC_ASSERT(boost::is_integral<int32>::value);
	BOOST_STATIC_ASSERT(boost::is_integral<uint32>::value);
	BOOST_STATIC_ASSERT(boost::is_integral<int64>::value);
	BOOST_STATIC_ASSERT(boost::is_integral<uint64>::value);
	BOOST_STATIC_ASSERT(boost::is_integral<intfast8>::value);
	BOOST_STATIC_ASSERT(boost::is_integral<uintfast8>::value);
	BOOST_STATIC_ASSERT(boost::is_integral<intfast16>::value);
	BOOST_STATIC_ASSERT(boost::is_integral<uintfast16>::value);
	BOOST_STATIC_ASSERT(boost::is_integral<intfast32>::value);
	BOOST_STATIC_ASSERT(boost::is_integral<uintfast32>::value);
	BOOST_STATIC_ASSERT(boost::is_integral<intfast64>::value);
	BOOST_STATIC_ASSERT(boost::is_integral<uintfast64>::value);
	BOOST_STATIC_ASSERT(boost::is_integral<intmax>::value);
	BOOST_STATIC_ASSERT(boost::is_integral<uintmax>::value);

	//浮点数:float,double,long double
	BOOST_STATIC_ASSERT(boost::is_float<float>::value);
	BOOST_STATIC_ASSERT(boost::is_float<double>::value);
	BOOST_STATIC_ASSERT(boost::is_float<long double>::value);

	//算术类型:整形和浮点型
	BOOST_STATIC_ASSERT(boost::is_arithmetic<bool>::value);
	BOOST_STATIC_ASSERT(boost::is_arithmetic<char>::value);
	BOOST_STATIC_ASSERT(boost::is_arithmetic<int>::value);
	BOOST_STATIC_ASSERT(boost::is_arithmetic<unsigned int>::value);
	BOOST_STATIC_ASSERT(boost::is_arithmetic<long>::value);
	BOOST_STATIC_ASSERT(boost::is_arithmetic<unsigned long>::value);
	BOOST_STATIC_ASSERT(boost::is_arithmetic<int8>::value);
	BOOST_STATIC_ASSERT(boost::is_arithmetic<uint8>::value);
	BOOST_STATIC_ASSERT(boost::is_arithmetic<int16>::value);
	BOOST_STATIC_ASSERT(boost::is_arithmetic<uint16>::value);
	BOOST_STATIC_ASSERT(boost::is_arithmetic<int32>::value);
	BOOST_STATIC_ASSERT(boost::is_arithmetic<uint32>::value);
	BOOST_STATIC_ASSERT(boost::is_arithmetic<int64>::value);
	BOOST_STATIC_ASSERT(boost::is_arithmetic<uint64>::value);
	BOOST_STATIC_ASSERT(boost::is_arithmetic<intfast8>::value);
	BOOST_STATIC_ASSERT(boost::is_arithmetic<uintfast8>::value);
	BOOST_STATIC_ASSERT(boost::is_arithmetic<intfast16>::value);
	BOOST_STATIC_ASSERT(boost::is_arithmetic<uintfast16>::value);
	BOOST_STATIC_ASSERT(boost::is_arithmetic<intfast32>::value);
	BOOST_STATIC_ASSERT(boost::is_arithmetic<uintfast32>::value);
	BOOST_STATIC_ASSERT(boost::is_arithmetic<intfast64>::value);
	BOOST_STATIC_ASSERT(boost::is_arithmetic<uintfast64>::value);
	BOOST_STATIC_ASSERT(boost::is_arithmetic<intmax>::value);
	BOOST_STATIC_ASSERT(boost::is_arithmetic<uintmax>::value);
	BOOST_STATIC_ASSERT(boost::is_arithmetic<float>::value);
	BOOST_STATIC_ASSERT(boost::is_arithmetic<double>::value);
	BOOST_STATIC_ASSERT(boost::is_arithmetic<long double>::value);

	//基本类型:算术类型和void
	BOOST_STATIC_ASSERT(boost::is_fundamental<bool>::value);
	BOOST_STATIC_ASSERT(boost::is_fundamental<char>::value);
	BOOST_STATIC_ASSERT(boost::is_fundamental<int>::value);
	BOOST_STATIC_ASSERT(boost::is_fundamental<unsigned int>::value);
	BOOST_STATIC_ASSERT(boost::is_fundamental<long>::value);
	BOOST_STATIC_ASSERT(boost::is_fundamental<unsigned long>::value);
	BOOST_STATIC_ASSERT(boost::is_fundamental<int8>::value);
	BOOST_STATIC_ASSERT(boost::is_fundamental<uint8>::value);
	BOOST_STATIC_ASSERT(boost::is_fundamental<int16>::value);
	BOOST_STATIC_ASSERT(boost::is_fundamental<uint16>::value);
	BOOST_STATIC_ASSERT(boost::is_fundamental<int32>::value);
	BOOST_STATIC_ASSERT(boost::is_fundamental<uint32>::value);
	BOOST_STATIC_ASSERT(boost::is_fundamental<int64>::value);
	BOOST_STATIC_ASSERT(boost::is_fundamental<uint64>::value);
	BOOST_STATIC_ASSERT(boost::is_fundamental<intfast8>::value);
	BOOST_STATIC_ASSERT(boost::is_fundamental<uintfast8>::value);
	BOOST_STATIC_ASSERT(boost::is_fundamental<intfast16>::value);
	BOOST_STATIC_ASSERT(boost::is_fundamental<uintfast16>::value);
	BOOST_STATIC_ASSERT(boost::is_fundamental<intfast32>::value);
	BOOST_STATIC_ASSERT(boost::is_fundamental<uintfast32>::value);
	BOOST_STATIC_ASSERT(boost::is_fundamental<intfast64>::value);
	BOOST_STATIC_ASSERT(boost::is_fundamental<uintfast64>::value);
	BOOST_STATIC_ASSERT(boost::is_fundamental<intmax>::value);
	BOOST_STATIC_ASSERT(boost::is_fundamental<uintmax>::value);
	BOOST_STATIC_ASSERT(boost::is_fundamental<float>::value);
	BOOST_STATIC_ASSERT(boost::is_fundamental<double>::value);
	BOOST_STATIC_ASSERT(boost::is_fundamental<long double>::value);
	BOOST_STATIC_ASSERT(boost::is_fundamental<void>::value);
	BOOST_STATIC_ASSERT(!boost::is_fundamental<int&>::value);
	BOOST_STATIC_ASSERT(boost::is_fundamental<const int>::value);
	BOOST_STATIC_ASSERT(!boost::is_fundamental<int const&>::value);
	BOOST_STATIC_ASSERT(!boost::is_fundamental<int*>::value);

	//指针:普通指针，函数指针(但不是成员函数指针，不是智能指针)
	BOOST_STATIC_ASSERT(boost::is_pointer<void*>::value);
	BOOST_STATIC_ASSERT(boost::is_pointer<Dog*>::value);
	BOOST_STATIC_ASSERT(boost::is_pointer<bool(*)(int, float)>::value);
	BOOST_STATIC_ASSERT(!boost::is_pointer<void(Dog::*)()>::value);
	BOOST_STATIC_ASSERT(!boost::is_pointer<std::shared_ptr<int>>::value);
	BOOST_STATIC_ASSERT(boost::is_pointer<int const*>::value);
	BOOST_STATIC_ASSERT(boost::is_pointer<int * const>::value);
	BOOST_STATIC_ASSERT(boost::is_pointer<int const * const>::value);

	//成员函数指针
	BOOST_STATIC_ASSERT(boost::is_member_function_pointer<void(Dog::*)()>::value);

	//成员变量指针
	BOOST_STATIC_ASSERT(boost::is_member_object_pointer<int(Dog::*)>::value);

	//成员指针:成员函数指针，成员变量指针
	BOOST_STATIC_ASSERT(boost::is_member_pointer<void(Dog::*)()>::value);
	BOOST_STATIC_ASSERT(boost::is_member_pointer<int(Dog::*)>::value);

	//左值引用
	BOOST_STATIC_ASSERT(boost::is_lvalue_reference<int&>::value);

	//右值引用
	BOOST_STATIC_ASSERT(boost::is_rvalue_reference<int&&>::value);

	//顶层const
	BOOST_STATIC_ASSERT(boost::is_const<const int>::value);
	BOOST_STATIC_ASSERT(!boost::is_const<const int*>::value);
	BOOST_STATIC_ASSERT(!boost::is_const<int const&>::value);
	BOOST_STATIC_ASSERT(boost::is_const<int* const>::value);

	//减法
	using T1 = boost::remove_const<int>::type;
	BOOST_STATIC_ASSERT(!boost::is_const<T1>::value);
	using T2 = boost::remove_reference<int>::type;
	BOOST_STATIC_ASSERT(!boost::is_reference<T2>::value);
	using T3 = boost::remove_reference<int const&>::type;
	BOOST_STATIC_ASSERT(!boost::is_reference<T3>::value);
	BOOST_STATIC_ASSERT(boost::is_const<T3>::value);
}

BOOST_AUTO_TEST_SUITE_END()

#endif//SUITE_UFC_METAFUNCTION