//#define SUITE_UD9_VIDEOBUFFER

#ifdef SUITE_UD9_VIDEOBUFFER

#include "UBTConfig.h"

#include "URMVertexElement.h"
#include "URMVertexDeclaration.h"
#include "URMVideoBuffer.h"
#include "URMVideoBufferManager.h"
#include "URMVertexBuffer.h"

#include "UD9Shader.h"

using namespace ung;
using namespace std;

//顶点结构
struct Vertex
{
	Vector3 mPos;
};

BOOST_AUTO_TEST_SUITE(suite_ud9_videobuffer)

BOOST_AUTO_TEST_CASE(case_ud9_videobuffer_1)
{
	auto& root = ung::Root::getInstance();

	bool ret{ false };

	auto pRS = root.getCurrentRenderSystem();

	String pt{"../../../Resource/Script/Shader/"};

	auto sd = UNG_NEW D9Shader(pt + "transform.vert");

	////vertex element
	//auto pVE = UNG_NEW VertexElement(0,0,VertexElementSemantic::VES_POSITION,
	//	VertexElementType::VET_FLOAT3);

	////vertex declaration
	//auto pVD = UNG_NEW VertexDeclaration();

	////添加一个element条目到顶点声明
	//pVD->addElement(pVE);

	//ret = pRS->createWindow("UNG", false, 800, 600, true);

	//root.run();
}

BOOST_AUTO_TEST_SUITE_END()

#endif//SUITE_UD9_VIDEOBUFFER