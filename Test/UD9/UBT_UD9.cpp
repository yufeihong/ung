//#define SUITE_UD9

#ifdef SUITE_UD9

#include "UBTConfig.h"

using namespace ung;
using namespace std;

BOOST_AUTO_TEST_SUITE(suite_ud9)

BOOST_AUTO_TEST_CASE(case_ud9_1)
{
	auto& root = ung::Root::getInstance();

	bool ret{ false };

	auto pRS = root.getCurrentRenderSystem();

	ret = pRS->createWindow("UNG", false, 800, 600, true);

	root.run();
}

BOOST_AUTO_TEST_SUITE_END()

#endif//SUITE_UD9