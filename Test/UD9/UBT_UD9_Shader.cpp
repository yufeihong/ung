//#define SUITE_UD9_SHADER

#ifdef SUITE_UD9_SHADER

#include "UBTConfig.h"

#include "URMVertexElement.h"
#include "URMVertexDeclaration.h"
#include "URMShader.h"
#include "URMViewport.h"
#include "UD9VertexBufferManager.h"
#include "UD9VertexBuffer.h"
#include "UD9RenderSystem.h"

#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"d3dx9d.lib")

using namespace ung;
using namespace std;

#define USE_TRANSFORMED_DATA

/*
The most important things with vertex performance is to use a 32-byte vertex, and to maintain 
good cache ordering.
*/
struct CUSTOMVERTEX
{
	FLOAT x, y, z;
};

SeparatedPointer<VertexBuffer> vb;
IDirect3DDevice9* pActiveDevice = NULL;

void render()
{
	auto vvbb = PolyCast<D9VertexBuffer*>(vb.get())->mVB;

	/*
	Binds a vertex buffer to a device data stream.
	HRESULT SetStreamSource(UINT StreamNumber,IDirect3DVertexBuffer9* pStreamData,
	UINT OffsetInBytes,UINT Stride);
	StreamNumber:
	Specifies the data stream, in the range from 0 to the maximum number of streams -1.
	pStreamData:
	Pointer to an IDirect3DVertexBuffer9 interface, representing the vertex buffer to bind to the 
	specified data stream.
	OffsetInBytes:
	Offset from the beginning of the stream to the beginning of the vertex data, in bytes. To find 
	out if the device supports stream offsets, see the D3DDEVCAPS2_STREAMOFFSET constant 
	in D3DDEVCAPS2.
	Stride:
	Stride of the component, in bytes.
	When a FVF vertex shader is used, the stride of the vertex stream must match the vertex 
	size, computed from the FVF. When a declaration is used, the stride should be greater than 
	or equal to the stream size computed from the declaration.
	When calling SetStreamSource, the stride is normally required to be equal to the vertex size.
	However, there are times when you may want to draw multiple instances of the same or 
	similar geometry (such as when using instancing to draw). For this case, use a zero stride to 
	tell the runtime not to increment the vertex buffer offset (ie: use the same vertex data for 
	all instances). For more information about instancing, see Efficiently Drawing Multiple 
	Instances of Geometry(Direct3D 9).
	*/
	auto ret = pActiveDevice->SetStreamSource(0, vvbb, 0, sizeof(CUSTOMVERTEX));
	BOOST_ASSERT(ret == D3D_OK);

	/*
	The drawing methods instruct the hardware to draw geometry.
	Essentially(本质上),the drawing methods dump geometry into the rendering pipeline 
	for processing.

	Renders a sequence of nonindexed, geometric primitives of the specified type from 
	the current set of data input streams.
	HRESULT DrawPrimitive(D3DPRIMITIVETYPE PrimitiveType,UINT StartVertex,
	UINT PrimitiveCount);
	PrimitiveType:
	Member of the D3DPRIMITIVETYPE enumerated type,describing the type of primitive 
	to render.
	StartVertex:
	Index of the first vertex to load.Beginning at StartVertex the correct number of vertices 
	will be read out of the vertex buffer.
	Index to an element in the vertex buffer that marks the starting point from which to 
	begin reading vertices.This parameter gives us the flexibility to only draw certain portions 
	of a vertex buffer.
	PrimitiveCount:
	Number of primitives to render.The maximum number of primitives allowed is determined 
	by checking the MaxPrimitiveCount member of the D3DCAPS9 structure.PrimitiveCount is 
	the number of primitives as determined by the primitive type.If it is a line list,each primitive 
	has two vertices.If it is a triangle list,each primitive has three vertices.
	*/
	ret = pActiveDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 1);
	BOOST_ASSERT(ret == D3D_OK);

	/*
	Based on indexing,renders the specified geometric primitive into an array of vertices.
	HRESULT DrawIndexedPrimitive(D3DPRIMITIVETYPE Type,INT BaseVertexIndex,
	UINT MinIndex,UINT NumVertices,UINT StartIndex,UINT PrimitiveCount);
	Type:
	Member of the D3DPRIMITIVETYPE enumerated type,describing the type of primitive to 
	render.D3DPT_POINTLIST is not supported with this method.
	BaseVertexIndex:
	Offset from the start of the vertex buffer to the first vertex.
	MinIndex:
	Minimum vertex index for vertices used during this call.This is a zero based index relative 
	to BaseVertexIndex.
	NumVertices:
	Number of vertices used during this call.The first vertex is located at index:
	BaseVertexIndex + MinIndex.
	StartIndex:
	Index of the first index to use when accesssing the vertex buffer.Beginning at StartIndex 
	to index vertices from the vertex buffer.
	PrimitiveCount:
	Number of primitives to render.The number of vertices used is a function of the primitive 
	count and the primitive type.The maximum number of primitives allowed is determined 
	by checking the MaxPrimitiveCount member of the D3DCAPS9 structure.
	If the method succeeds,the return value is D3D_OK.If the method fails,the return value 
	can be the following:D3DERR_INVALIDCALL.
	This method draws indexed primitives from the current set of data input streams.MinIndex 
	and all the indices in the index stream are relative to the BaseVertexIndex.
	The MinIndex and NumVertices parameters specify the range of vertex indices used for 
	each IDirect3DDevice9::DrawIndexedPrimitive call.These are used to optimize vertex 
	processing of indexed primitives by processing a sequential range of vertices prior to 
	indexing into these vertices. It is invalid for any indices used during this call to reference 
	any vertices outside of this range.
	*/
}

BOOST_AUTO_TEST_SUITE(suite_ud9_shader)

BOOST_AUTO_TEST_CASE(case_ud9_shader_1)
{
	auto& ung = Root::getInstance();

	auto currentRS = ung.getCurrentRenderSystem();

	currentRS->createWindow("UNG", false, 800, 600, true);

	//创建场景管理器
	auto pSM = ung.createSceneManager<ST_EXTERIOR>();

	//场景摄像机
	auto pCam = currentRS->createCamera("mainCamera",Vector3(0,0,5), Vector3(0,0,0),800 / 600.0);

	//视口
	auto viewport = pCam->createViewport(0, 0, 800, 600);

#ifdef USE_TRANSFORMED_DATA
	viewport->set();
#endif

	auto pRS = static_cast<D9RenderSystem*>(currentRS);

	pActiveDevice = pRS->getActiveDevice();

	String pt{ "../../../Resource/Script/Shader/" };
	String vertfullName,vertName;
#ifdef USE_TRANSFORMED_DATA
	vertfullName = pt + "transform.vert";
	vertName = "transform.vert";
#else
	vertfullName = pt + "untransform.vert";
	vertName = "untransform.vert";
#endif
	auto vs = currentRS->createVertexShader(vertfullName);
	auto ps = currentRS->createPixelShader(pt + "color.frag");

	CUSTOMVERTEX Vertices[] =
	{
#ifdef USE_TRANSFORMED_DATA
		{ -1.0f,-1.0f, 0.0f,},
		{  1.0f,-1.0f, 0.0f,},
		{  0.0f, 1.0f, 0.0f},
#else
		{ 150.0f,  50.0f, 0.5f,},
		{  50.0f, 250.0f, 0.5f,},
		{ 250.0f, 250.0f, 0.5f,},
#endif
	};

#ifdef USE_TRANSFORMED_DATA
	auto mat = pCam->getViewMatrix() * pCam->getProjectionMatrix();

	if (sizeof(mat[0][0]) > 4)
	{
		Mat4<float_type> fm = std::move(mat);

		vs->setConstant("view_proj_mat", fm.getAddress(),sizeof(fm));
	}
	else
	{
		vs->setConstant("view_proj_mat", mat.getAddress(),sizeof(mat));
	}
#endif

	auto vertexBufferManager = pRS->getD9VertexBufferManager();

#ifdef USE_TRANSFORMED_DATA
	auto elem1 = vertexBufferManager->createElement(0, 0, 
		VertexElementSemantic::VES_POSITION,
		VertexElementType::VET_FLOAT3);
#else
	auto elem1 = vertexBufferManager->createElement(0, 0,
		VertexElementSemantic::VES_POSITION_HCS,
		VertexElementType::VET_FLOAT3);
#endif

	auto decl = vertexBufferManager->createDeclaration();
	decl->addElement(elem1);
	decl->addElementFinish();
	decl->set();

#ifdef USE_TRANSFORMED_DATA
	vb = vertexBufferManager->createVertexBuffer(sizeof(CUSTOMVERTEX), 3,
		IVideoBuffer::Usage::VB_USAGE_DYNAMIC, false);
#else
	vb = vertexBufferManager->createVertexBuffer(sizeof(CUSTOMVERTEX), 3,
		IVideoBuffer::Usage::VB_USAGE_DYNAMIC | 
		IVideoBuffer::Usage::VB_USAGE_DONOTCLIP,
		false);
	pActiveDevice->SetRenderState(D3DRS_CLIPPING, false);
#endif

	vb->writeData(0, sizeof(CUSTOMVERTEX) * 3, Vertices);

	currentRS->useShader(vertName);
	currentRS->useShader("color.frag");

	pRS->setRender(render);

	Root::getInstance().run();

	/*
	手动调用的原因是，确保顶点缓冲区在D9RenderSystem::shutdown()之前被析构，等到把object给进行
	整理后，就不需要手动调用了。
	*/
	vb.destroy();
}

BOOST_AUTO_TEST_SUITE_END()

#endif//SUITE_UD9_SHADER