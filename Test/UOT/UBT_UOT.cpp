//#define SUITE_UOT

#ifdef SUITE_UOT

#include "UBTConfig.h"

using namespace ung;
using namespace std;

BOOST_AUTO_TEST_SUITE(suite_uot)

//邻居测试
BOOST_AUTO_TEST_CASE(case_uot_1)
{
	return;

	auto root = UNG_NEW OctreeNode(nullptr);

	root->createChildren();

	//打印root的8个孩子节点
	cout << "root的8个孩子节点:" << endl;
	for (int i = 0; i < 8; ++i)
	{
		cout << i << ":" << root->getChild(i) << endl;
	}
	cout << endl;

	//第0个孩子节点
	auto c0 = root->getChild(0);
	c0->createChildren();
	//打印root的第0个孩子节点的8个孩子节点
	cout << "root的第0个孩子节点的8个孩子节点:" << endl;
	for (int i = 0; i < 8; ++i)
	{
		cout << i << ":" << c0->getChild(i) << endl;
	}
	cout << endl;

	OctreeNode* neighborsCopy[8][6];
	cout << "root的第0个孩子节点的8个孩子节点的邻居:" << endl;
	for (int i = 0; i < 8; ++i)
	{
		for (int j = 0; j < 6; ++j)
		{
			cout << i << ":" << c0->getChild(i)->getNeighbor(j) << ",";

			//暂存
			neighborsCopy[i][j] = c0->getChild(i)->getNeighbor(j);
		}

		cout << endl;
	}
	cout << endl;

	//第1个孩子节点
	auto c1 = root->getChild(1);
	c1->createChildren();
	//打印root的第1个孩子节点的8个孩子节点
	cout << "root的第1个孩子节点的8个孩子节点:" << endl;
	for (int i = 0; i < 8; ++i)
	{
		cout << i << ":" << c1->getChild(i) << endl;
	}
	cout << endl;

	cout << "root的第1个孩子节点的8个孩子节点的邻居:" << endl;
	for (int i = 0; i < 8; ++i)
	{
		for (int j = 0; j < 6; ++j)
		{
			cout << i << ":" << c1->getChild(i)->getNeighbor(j) << ",";
		}

		cout << endl;
	}
	cout << endl;

	cout << "root的第0个孩子节点的8个孩子节点的邻居:" << endl;
	for (int i = 0; i < 8; ++i)
	{
		for (int j = 0; j < 6; ++j)
		{
			cout << i << ":" << c0->getChild(i)->getNeighbor(j) << ",";
		}

		cout << endl;
	}
	cout << endl;

	//销毁c1的孩子们
	c1->destroyChildren();
	//检查c0的8个孩子的邻居是否又恢复原状了
	for (int i = 0; i < 8; ++i)
	{
		for (int j = 0; j < 6; ++j)
		{
			BOOST_CHECK(c0->getChild(i)->getNeighbor(j) == neighborsCopy[i][j]);
		}
	}
}

//插入，移除对象
BOOST_AUTO_TEST_CASE(case_uot_2)
{
	auto& ung = Root::getInstance();

	{
		//创建场景管理器
		auto smPtr = ung.createSceneManager<ST_EXTERIOR>();

		//场景图根节点
		auto root = smPtr->getSceneRootNode();

		//获取空间管理器
		auto spatialPtr = smPtr->getSpatialManager();
		BOOST_CHECK(spatialPtr);

		//创建对象1
		auto object1 = smPtr->createObject("../../../Resource/Script/XML/Entities/test.xml", "object1");
		//设置对象的包围盒
		auto aabb1 = object1->getAABB();
		aabb1.setMin(Vector3(-1));
		aabb1.setMax(Vector3(1));
		//关联对象1到场景图根节点
		object1->attachToSceneNode(root);
		//更新场景图根节点
		root->update();
	}
}

BOOST_AUTO_TEST_CASE(case_uot_3)
{
	auto& ung = Root::getInstance();

	{
		//创建场景管理器
		auto smPtr = ung.createSceneManager<ST_EXTERIOR>();

		//创建一个射线查询
		Ray ray(Vector3(0, 2, 0), Vector3(1, 0, 0));
		auto rayQuery = smPtr->createRaySceneQuery(ray);
		//执行
		auto results = rayQuery->execute();
	}
}

BOOST_AUTO_TEST_SUITE_END()

#endif//SUITE_UOT