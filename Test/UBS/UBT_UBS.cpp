//#define SUITE_UBS

#ifdef SUITE_UBS

#include "UBTConfig.h"

using namespace ung;

BOOST_AUTO_TEST_SUITE(suite_ubs)

BOOST_AUTO_TEST_CASE(case_ubs_1)
{
	Platform::getInstance().printEndianness();
	Platform::getInstance().printProcessor();
}

BOOST_AUTO_TEST_SUITE_END()

#endif//SUITE_UBS