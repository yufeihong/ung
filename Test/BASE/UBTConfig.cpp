#include "UBTConfig.h"

//#pragma comment(lib,"freeimage.lib")
//
//#pragma comment (lib, "opengl32.lib")
//
//#ifdef NDEBUG
//#pragma comment (lib, "glew32.lib")
//#else
//#pragma comment (lib, "glew32d.lib")
//#endif
//
//#ifdef NDEBUG
//#pragma comment (lib, "freeglut_static.lib")
//#else
//#pragma comment (lib, "freeglut_staticd.lib")
//#endif
//
//#pragma comment(lib,"glfw3.lib")
//
//#pragma comment(lib,"zdll.lib")

#ifdef NDEBUG
//#pragma comment(lib,"tinyxml2.lib")
#else
//#pragma comment(lib,"tinyxml2_d.lib")
#endif

//------------------------------------------------------------

#if UNG_DEBUGMODE
#pragma comment(lib,"UNG_d.lib")
#else
#pragma comment(lib,"UNG.lib")
#endif

#if UNG_DEBUGMODE
#pragma comment(lib,"UOT_d.lib")
#else
#pragma comment(lib,"UOT.lib")
#endif

#if UNG_DEBUGMODE
#pragma comment(lib,"UD9_d.lib")
#else
#pragma comment(lib,"UD9.lib")
#endif

/*
当UNG的CPP代码发生改动，而UBT的代码没有发生改动的情况下，需要重新再链接一次UBT。
*/