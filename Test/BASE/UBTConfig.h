/*!
 * \brief
 * 配置文件。
 * \file UBTConfig.h
 *
 * \author Su Yang
 *
 * \date 2017/03/12
 */
#ifndef _UBT_CONFIG_H_
#define _UBT_CONFIG_H_

#define BOOST_TEST_INCLUDED
#ifndef _INCLUDE_BOOST_UNITTEST_HPP_
#include "boost/test/unit_test.hpp"
#define _INCLUDE_BOOST_UNITTEST_HPP_
#endif

#ifndef _INCLUDE_BOOST_TYPETRAITS_HPP_
#include "boost/type_traits.hpp"
#define _INCLUDE_BOOST_TYPETRAITS_HPP_
#endif

#ifndef _INCLUDE_BOOST_MPLLIST_HPP_
#include "boost/mpl/list.hpp"
#define _INCLUDE_BOOST_MPLLIST_HPP_
#endif

//------------------------------------------------------------

#ifndef _UBS_H_
#include "UBS.h"
#endif

#ifndef _UTM_H_
#include "UTM.h"
#endif

#ifndef _UMM_H_
#include "UMM.h"
#endif

#ifndef _UFC_H_
#include "UFC.h"
#endif

#ifndef _UES_H_
#include "UES.h"
#endif

#ifndef _UML_H_
#include "UML.h"
#endif

#ifndef _UPE_H_
#include "UPE.h"
#endif

#ifndef _URM_H_
#include "URM.h"
#endif

#ifndef _USM_H_
#include "USM.h"
#endif

#ifndef _UNG_H_
#include "UNG.h"
#endif

#ifndef _UOT_H_
#include "UOT.h"
#endif

#ifndef _UD9_H_
#include "UD9.h"
#endif

#ifndef _INCLUDE_TINYXML2_H_
//该头文件必须位于D3D头文件之后
#include "tinyxml2.h"
#define _INCLUDE_TINYXML2_H_
#endif
using namespace tinyxml2;

//------------------------------------------------------------

//检查两个给定浮点数是否相等(根据左侧数据来推断类型)
#define UBT_CHECK_EQUAL_FLOAT(left,right)						\
BOOST_CHECK(ung::MathImpl<boost::remove_reference<decltype(left)>::type>::isEqual(left,right));

//检查两个给定Vector是否相等
#define UBT_CHECK_EQUAL_VEC(left,right)	\
BOOST_CHECK(left == right);

//------------------------------------------------------------

#endif//_UBT_CONFIG_H_