#include "UBTConfig.h"

//全局测试夹具类
struct global_fixture
{
	global_fixture()
	{
#ifdef UNG_HIERARCHICAL_COMPILE
		auto& root = ung::Root::getInstance();
		root.init();
#endif
	}

	~global_fixture()
	{
#ifdef UNG_HIERARCHICAL_COMPILE
		auto& root = ung::Root::getInstance();
		root.close();
#endif
	}
};

//定义全局夹具
BOOST_GLOBAL_FIXTURE(global_fixture);