//#define SUITE_TRIVIAL

#ifdef SUITE_TRIVIAL

#include "UBTConfig.h"

#include "USM.h"

using namespace ung;

//-----------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------

BOOST_AUTO_TEST_SUITE(suite_trivial)

BOOST_AUTO_TEST_CASE(case_trivial_1)
{	
	Vector3 v1(-1, 0, 0);
	Vector3 v2(-1, 0, 1);
	Vector3 v3(-1, 1, 0);
	Plane plane(v1, v2, v3);
	auto d = plane.getD();

	int z = 0;
}

BOOST_AUTO_TEST_SUITE_END()

#endif//SUITE_TRIVIAL