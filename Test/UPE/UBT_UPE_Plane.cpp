//#define SUITE_UPE_PLANE

#ifdef SUITE_UPE_PLANE

#include "UBTConfig.h"

using namespace ung;

BOOST_AUTO_TEST_SUITE(suite_upe_plane)

BOOST_AUTO_TEST_CASE(case_upe_plane_1)
{
	//对平面进行变换
	Vector3 original = Vector3::ZERO;

	Matrix4 mat4;
	mat4.setTrans(Vector3(0,-1,0));

	Vector3 n1(0, 1, 0);
	real_type d1{ 1.0 };					//坐标系原点处于平面的负半空间
	Plane p1{ n1,d1 };

	//验证原点位于平面的负半空间
	auto dis1 = ungSignedDistanceFromPointToPlane(p1,original);
	UBT_CHECK_EQUAL_FLOAT(dis1, -d1);
	auto dis2 = p1.getSignedDistanceFromOriginToPlane();
	UBT_CHECK_EQUAL_FLOAT(dis2,dis1);

	p1.transform(mat4);

	//验证原点位于平面上
	auto dis3 = ungSignedDistanceFromPointToPlane(p1, original);
	UBT_CHECK_EQUAL_FLOAT(dis3, 0.0);

	int a = 0;
}

BOOST_AUTO_TEST_SUITE_END()

#endif//SUITE_UPE_PLANE