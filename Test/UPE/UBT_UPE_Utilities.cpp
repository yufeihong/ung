//#define SUITE_UPE_UTILITIES

#ifdef SUITE_UPE_UTILITIES

#include "UBTConfig.h"

using namespace ung;

BOOST_AUTO_TEST_SUITE(suite_upe_utilities)

BOOST_AUTO_TEST_CASE(case_upe_utilities_1)
{
	//判断给定点位于有向线段的那一侧
	Vector2 p(1.0, 2.0);
	Vector2 l(1.0, 1.0);
	auto s = ungPointSideOfLine(p, l);
	BOOST_CHECK(s > 0.0);

	Vector2 l1(-1.0, 1.0);
	auto s1 = ungPointSideOfLine(p, l1);
	BOOST_CHECK(s1 < 0.0);

	Vector2 p2(2.0, 2.0);
	Vector2 l2(1.0, 1.0);
	auto s2 = ungPointSideOfLine(p2, l2);
	BOOST_CHECK(Math::isZero(s2));

	//判断点在三角形那一侧
	Triangle t(Vector3(0, 5, 0), Vector3(0, 5, 1), Vector3(1, 5, 0));
	auto w = ungPointSideOfTriangle(t,Vector3(0,10,0));
	BOOST_CHECK(w < 0.0);

	Triangle t1(Vector3(1, 0, 5), Vector3(1, 1, 5),Vector3(0, 0, 4));
	auto w1 = ungPointSideOfTriangle(t1,Vector3(0,2,0));
	BOOST_CHECK(w1 > 0.0);

	Triangle t2(Vector3(0, 5, 0), Vector3(0, 5, 1), Vector3(1, 5, 0));
	auto w2 = ungPointSideOfTriangle(t2,Vector3(0,5,0));
	BOOST_CHECK(Math::isZero(w2));

	//点不在三角形内部，也返回共面值0
	w2 = ungPointSideOfTriangle(Triangle(Vector3(0, 0, -1), Vector3(-1, 0, 0), Vector3(1, 0, 0)), Vector3(10, 0, 0));
	BOOST_CHECK(Math::isZero(w2));

	//计算四面体体积
	Vector3 v1(-1, 0, 0), v2(1, 0, 0), v3(0, 0, -1);
	Vector3 v4(0,1,0);
	auto vo = ungSignedVolumeOfTetrahedron(v1, v2, v3, v4);
	UBT_CHECK_EQUAL_FLOAT(vo, 1 / 3);

	//判断点和圆的关系
	Vector2 c1(1.0, 0), c2(0.0, 1), c3(-1.0, 0);
	auto r1 = ungPointSideOfCircle(Vector2(0.5,0.5), c1, c2, c3);
	BOOST_CHECK(r1 > 0);
	auto r2 = ungPointSideOfCircle(Vector2(2,2), c1, c2, c3);
	BOOST_CHECK(r2 < 0);
	auto r3 = ungPointSideOfCircle(Vector2(-Math::calSqrt(0.5),-Math::calSqrt(0.5)), c1, c2, c3);
	UBT_CHECK_EQUAL_FLOAT(r3,0);

	//计算给定点在三角形上的质心坐标
	Vector3 p3(v1);
	real_type u5, v5, w5;
	auto b5 = ungTriangleBarycentric(Triangle(v1,v2,v3),p3,u5,v5,w5);
	BOOST_CHECK(b5);
	UBT_CHECK_EQUAL_FLOAT(u5, 1.0);
	UBT_CHECK_EQUAL_FLOAT(v5, 0.0);
	UBT_CHECK_EQUAL_FLOAT(w5, 0.0);

	p3 = Vector3(0.0,0.0,-0.5);
	b5 = ungTriangleBarycentric(Triangle(v1, v2, v3), p3, u5, v5, w5);
	BOOST_CHECK(b5);
	//P = uA + vB + wC
	BOOST_CHECK(p3 == Vector3(u5 * v1) + Vector3(v5 * v2) + Vector3(w5 * v3));

	p3 = Vector3(0.0, 0.0, 1.0);
	b5 = ungTriangleBarycentric(Triangle(v1, v2, v3), p3, u5, v5, w5);
	BOOST_CHECK(!b5);
	BOOST_CHECK(p3 == Vector3(u5 * v1) + Vector3(v5 * v2) + Vector3(w5 * v3));

	//计算给定点point在由a,b,c,d构成的四面体的质心坐标
	Vector3 tet1(-1,0,0), tet2(1,0,0), tet3(0,0,-1), tet4(0,1,0);
	Vector3 p4(0,0.5,-0.3);
	real_type u6, v6, w6, x6;
	auto b6 = ungTetrahedronBarycentric(tet1, tet2, tet3, tet4, p4, u6, v6, w6, x6);
	BOOST_CHECK(b6);
	BOOST_CHECK(p4 == Vector3(u6 * tet1 + v6 * tet2 + w6 * tet3 + x6 * tet4));

	b6 = ungTetrahedronBarycentric(tet1, tet2, tet3, tet4, Vector3(0,0.5,0.3), u6, v6, w6, x6);
	BOOST_CHECK(!b6);
	BOOST_CHECK(Vector3(0,0.5,0.3) == Vector3(u6 * tet1 + v6 * tet2 + w6 * tet3 + x6 * tet4));

	//使用质心坐标的投影不变性来测试一个点是否在三角形内
	auto b7 = ungIsPointInTriangle(Triangle(v1,v2,v3), Vector3(0,0,0));
	BOOST_CHECK(b7);
	b7 = ungIsPointInTriangle(Triangle(v1, v2, v3), Vector3(0, 0, 0.0001));
	BOOST_CHECK(!b7);

	//计算点到直线的距离
	auto dis1 = ungPointToStraightLineDistance(Vector3(0,0,0),Vector3(10,10,10),Vector3(-1,-1,-1));
	auto sd = Vector3(-1,-1,-1).perpendicularComponent(Vector3(10,10,10));
	UBT_CHECK_EQUAL_FLOAT(dis1, sd.length());

	dis1 = ungPointToStraightLineDistance(Vector3(1, 1, 1), Vector3(2, 2, 2), Vector3(3, 3, 3));
	UBT_CHECK_EQUAL_FLOAT(dis1, 0.0);

	//检查给定的3个点是否共线
	bool b8 = ungIsThreePointsCollinear(v1,v2,v3);
	BOOST_CHECK(!b8);
	b8 = ungIsThreePointsCollinear(v1, v1, v1);
	BOOST_CHECK(b8);

	//检查参数给定的4个顶点所构成的四边形是否是一个凸面
	Vector3 va(-5,-5,0), vb(5,-5,0), vc(-5,5,0), vd(5,5,0);
	auto b9 = ungIsQuadConvex(va, vb, vc, vd);
	BOOST_CHECK(!b9);
}

BOOST_AUTO_TEST_CASE(case_upe_utilities_2)
{
	//测试对AABB的旋转
	AABB aabb1(Vector3(-1, -1, -1), Vector3(1, 1, 1));
	AABB aabb2;
	auto r1 = aabb1.getHalfSize();
	//构造一个绕z轴旋转45度的矩阵
	Matrix4 mat1{};
	mat1.setRotateZ(Degree(45));
	aabb2 = aabb1 * mat1;
	auto r2 = aabb2.getHalfSize();
	aabb2 = aabb1 * mat1;
	auto r3 = aabb2.getHalfSize();
	aabb2 = aabb1 * mat1;
	auto r4 = aabb2.getHalfSize();
	aabb2 = aabb1 * mat1;
	auto r5 = aabb2.getHalfSize();
	aabb2 = aabb1 * mat1;
	auto r6 = aabb2.getHalfSize();
	aabb2 = aabb1 * mat1;
	auto r7 = aabb2.getHalfSize();
	aabb2 = aabb1 * mat1;
	auto r8 = aabb2.getHalfSize();
}

BOOST_AUTO_TEST_CASE(case_upe_utilities_3)
{
	//三角形上到空间中任意一点的最近点
	Triangle triangle1(Vector3(-10.0, 8.0, -3.0), Vector3(11.0, -30.0, 20.0), Vector3(40.0, 16.0, -11.0));
	Vector3 point1(47.8,50.5,80.0);
	Vector3 out1;
	ungClosestPointOnTriangleToPoint(triangle1, point1, out1);

	//四面体上到空间中任意一点的最近点
	Vector3 out2,out3;
	Vector3 a1(0,2,0), b1(-1,0,0), c1(0,0,1), d1(1,0,0);

	ungClosestPointOnTetrahedronToPoint(a1,b1,c1,d1,Vector3(0.2,0.3,0.4),out2);
	BOOST_CHECK(out2 == Vector3(0.2,0.3,0.4));

	ungClosestPointOnTetrahedronToPoint(a1, b1, c1, d1, Vector3(0.0, 0.3, 0.0), out2);
	BOOST_CHECK(out2 == Vector3(0.0, 0.3, 0.0));

	Triangle t1(a1,b1,c1);
	Vector3 point2(-0.5,0.5,0.5);
	ungClosestPointOnTetrahedronToPoint(a1,b1,c1,d1,point2,out2);
	BOOST_CHECK(ungPointSideOfTriangle(t1,out2) == 0);
	ungClosestPointOnTriangleToPoint(t1,point2,out3);
	BOOST_CHECK(out2 == out3);

	Triangle t2(a1, c1, d1);
	Vector3 point3(0.1, 10.0, 0.1);
	ungClosestPointOnTetrahedronToPoint(a1, b1, c1, d1, point3, out2);
	BOOST_CHECK(ungPointSideOfTriangle(t2, out2) == 0);
	ungClosestPointOnTriangleToPoint(t2, point3, out3);
	BOOST_CHECK(out2 == out3);

	Triangle t3(a1, d1,b1);
	Vector3 point4(0.1, 0.1, -10.0);
	ungClosestPointOnTetrahedronToPoint(a1, b1, c1, d1, point4, out2);
	BOOST_CHECK(ungPointSideOfTriangle(t3, out2) == 0);
	ungClosestPointOnTriangleToPoint(t3, point4, out3);
	BOOST_CHECK(out2 == out3);

	Triangle t4(b1, d1, c1);
	Vector3 point5(0.1, -10.1, 0.2);
	ungClosestPointOnTetrahedronToPoint(a1, b1, c1, d1, point5, out2);
	BOOST_CHECK(ungPointSideOfTriangle(t4, out2) == 0);
	ungClosestPointOnTriangleToPoint(t4, point5, out3);
	BOOST_CHECK(out2 == out3);
}

BOOST_AUTO_TEST_CASE(case_upe_utilities_4)
{
	//两条直线的最近点
	Vector3 p1(0,0,0), q1(1,0,0);
	Vector3 p2(0,2,0), q2(1,2,0);
	Vector3 out1, out2;
	ungClosestPointBetweenTwoStraightLines(p1,q1,p2,q2,out1,out2);

	p1 = Vector3(5.1, 4.8, 3.9);
	q1 = Vector3(4.7, 3.8,2.6);
	p2 = Vector3(1.6, 2.3, 0.4);
	q2 = Vector3(1.8, 7.2, 9.4);
	ungClosestPointBetweenTwoStraightLines(p1, q1, p2, q2, out1, out2);

	//原点到平面的有符号距离
	Plane plane(Vector3(0,1,0),2);
	auto dis1 = ungSignedDistanceFromPointToPlane(plane, Vector3(0, 0, 0));
	UBT_CHECK_EQUAL_FLOAT(dis1,-2.0);
}

BOOST_AUTO_TEST_SUITE_END()

#endif//SUITE_UPE_UTILITIES