//#define SUITE_UPE_COLLISIONDETECTION

#ifdef SUITE_UPE_COLLISIONDETECTION

#include "UBTConfig.h"

using namespace ung;

BOOST_AUTO_TEST_SUITE(suite_upe_collisiondetection)

BOOST_AUTO_TEST_CASE(case_upe_collisiondetection_1)
{
	//两线段相交
	Vector2 out{};
	bool b1 = CollisionDetection::intersects(Vector2(-1.0,0.0), Vector2(1.0,0.0), Vector2(0.0,1.0), Vector2(0.0,-1.0),out);
	BOOST_CHECK(b1);
	BOOST_CHECK(out == Vector2(0.0, 0.0));

	b1 = CollisionDetection::intersects(Vector2(0.0,2.0), Vector2(2.0,0.0), Vector2(0.0, 0.0), Vector2(2.0, 2.0), out);
	BOOST_CHECK(b1);
	BOOST_CHECK(out == Vector2(1.0, 1.0));

	//平面-AABB
	Plane plane2(Vector3(0, 1, 0), -2);
	AABB aabb(Vector3(-1,-1,-1), Vector3(1,1,1));
	auto aabb_radius = aabb.getRadius();
	auto b2 = CollisionDetection::intersects(plane2, aabb);
	BOOST_CHECK(!b2);

	Plane plane3(Vector3(0, 1, 0), -1);
	auto b3 = CollisionDetection::intersects(plane3, aabb);
	BOOST_CHECK(b3);

	Vector3 pn(1, 1, 1);
	pn.normalize();

	Plane plane4(pn, aabb_radius + 0.1);
	auto b4 = CollisionDetection::intersects(plane4, aabb);
	BOOST_CHECK(!b4);

	Plane plane5(pn, aabb_radius);
	auto b5 = CollisionDetection::intersects(plane5, aabb);
	BOOST_CHECK(b5);

	Plane plane6(pn, aabb_radius - 0.1);
	auto b6 = CollisionDetection::intersects(plane6, aabb);
	BOOST_CHECK(b6);

	Plane plane7(Vector3(-1, 1, 1).normalizeCopy(), aabb_radius + 0.1);
	auto b7 = CollisionDetection::intersects(plane7, aabb);
	BOOST_CHECK(!b7);

	Plane plane8(Vector3(-1, 1, 1).normalizeCopy(), aabb_radius);
	auto b8 = CollisionDetection::intersects(plane8, aabb);
	BOOST_CHECK(b8);

	Plane plane9(Vector3(-1, 1, 1).normalizeCopy(), aabb_radius - 0.1);
	auto b9 = CollisionDetection::intersects(plane9, aabb);
	BOOST_CHECK(b9);
}

BOOST_AUTO_TEST_CASE(case_upe_collisiondetection_2)
{
	//三角形-AABB
	AABB aabb(Vector3(-1,-1,-1), Vector3(1,1,1));
	Vector3 v0(-1,-1,1.1);
	Vector3 v1(1,-1,1.1);
	Vector3 v2(0,1,1.1);
	Triangle triangle1(v0,v1,v2);
	bool b1 = CollisionDetection::intersects(aabb, triangle1);
	BOOST_CHECK(!b1);

	v0 = Vector3(-1, -1, 1);
	v1 = Vector3(1, -1, 1.1);
	v2 = Vector3(0, 1, 1.1);
	bool b3 = ungIsPointInAABB(aabb, v0);
	Triangle triangle2(v0, v1, v2);
	bool b2 = CollisionDetection::intersects(aabb, triangle2);
	BOOST_CHECK(b2);

	v0 = Vector3(0, 1, 1);
	v1 = Vector3(0, 0, 1.1);
	v2 = Vector3(0, 1, 1.1);
	bool b4 = ungIsPointInAABB(aabb, v0);
	Triangle triangle3(v0, v1, v2);
	bool b5 = CollisionDetection::intersects(aabb, triangle3);
	BOOST_CHECK(b5);

	v0 = Vector3(0, 0.5, 1);
	v1 = Vector3(0, 0, 1.1);
	v2 = Vector3(0, 1, 1.1);
	bool b6 = ungIsPointInAABB(aabb, v0);
	Triangle triangle4(v0, v1, v2);
	bool b7 = CollisionDetection::intersects(aabb, triangle4);
	BOOST_CHECK(b7);

	v0 = Vector3(0, 0, -2);
	v1 = Vector3(0, 0, 2);
	v2 = Vector3(0, 2, 0);
	Triangle triangle5(v0, v1, v2);
	bool b8 = CollisionDetection::intersects(aabb, triangle5);
	BOOST_CHECK(b8);
}

BOOST_AUTO_TEST_CASE(case_upe_collisiondetection_3)
{
	bool ret{ false };

	//线段-线段(2D)
	Vector2 dummyOut;
	ret = CollisionDetection::intersects(Vector2(-1.0, 0), Vector2(0.0, 0),
		Vector2(0.0, 0), Vector2(0.0, 1.0), dummyOut);
	BOOST_CHECK(!ret);

	//共面三角形-共面三角形
	Vector3 v1[3];
	Vector3 v2[3];
	v1[0] = Vector3(-1,0,0);
	v1[1] = Vector3(0,0,0);
	v1[2] = Vector3(-1,1,0);
	v2[0] = Vector3(-0.1,0,0);
	v2[1] = Vector3(1,0,0);
	v2[2] = Vector3(1,1,0);
	Triangle coT1(v1[0], v1[1], v1[2]);
	Triangle coT2(v2[0], v2[1], v2[2]);

	ret = CollisionDetection::intersects(coT1, coT2, true);
	BOOST_CHECK(ret);

	v1[0] = Vector3(-1, 0, 0);
	v1[1] = Vector3(0, 0, 0);
	v1[2] = Vector3(-1, 1, 0);
	v2[0] = Vector3(-0.8, 0, 0);
	v2[1] = Vector3(0, 0, 0);
	v2[2] = Vector3(-0.8, 0.5, 0);
	Triangle coT3(v1[0], v1[1], v1[2]);
	Triangle coT4(v2[0], v2[1], v2[2]);

	ret = CollisionDetection::intersects(coT3, coT4, true);
	BOOST_CHECK(ret);

	//三角形-三角形
	Triangle t1(Vector3(0,10,0),Vector3(-10,-10,0),Vector3(10,-10,0));
	Triangle t2(Vector3(0,0,-1),Vector3(-1.0,0,1.0),Vector3(1.0,0,1.0));

	auto b = CollisionDetection::intersects(t1, t2);
	BOOST_CHECK(b);
	b = CollisionDetection::intersects(t2, t1);
	BOOST_CHECK(b);

	Triangle t3(Vector3(0, 10, 0), Vector3(-10, -10, 0), Vector3(10, -10, 0));
	Triangle t4(Vector3(0, 0, -3), Vector3(-1.0, 0, 1.0), Vector3(1.0, 0, 0.0));

	b = CollisionDetection::intersects(t3, t4);
	BOOST_CHECK(b);
	b = CollisionDetection::intersects(t4, t3);
	BOOST_CHECK(b);

	Triangle t5(Vector3(0, 10, 0), Vector3(-10, -10, 0), Vector3(10, -10, 0));
	Triangle t6(Vector3(0, 0, -3), Vector3(-1.0, 0, 0.0), Vector3(1.0, 0, 0.0));

	b = CollisionDetection::intersects(t5, t6);
	BOOST_CHECK(b);
	b = CollisionDetection::intersects(t6, t5);
	BOOST_CHECK(b);

	Triangle t7(Vector3(0, 10, 0), Vector3(-10, -10, 0), Vector3(10, -10, 0));
	Triangle t8(Vector3(0, 0, -3), Vector3(-1.0, 0, -0.1), Vector3(1.0, 0, -0.000001));

	b = CollisionDetection::intersects(t7, t8);
	BOOST_CHECK(!b);
	b = CollisionDetection::intersects(t8, t7);
	BOOST_CHECK(!b);

	Triangle t9(Vector3(0, 10, 0), Vector3(-10, -10, 0), Vector3(10, -10, 0));
	Triangle t10(Vector3(0, 0, -3), Vector3(-1.0, 0, 0.0), Vector3(1.0, 0, -0.1));

	b = CollisionDetection::intersects(t9, t10);
	BOOST_CHECK(b);
	b = CollisionDetection::intersects(t10, t9);
	BOOST_CHECK(b);
}

BOOST_AUTO_TEST_CASE(case_upe_collisiondetection_4)
{
	bool ret{ false };
	Vector3 out{};
	int32 entryIndex{ -1 }, exitIndex{ -1 };

	//射线-AABB
	Ray ray1(Vector3(1.1, 0, 0),Vector3(1,0,0));
	AABB box1(Vector3(-1,-1,-1), Vector3(1,1,1));
	ret = CollisionDetection::intersects(ray1, box1, out);
	BOOST_CHECK(!ret);

	Ray ray2(Vector3(1.1, 0, 0), Vector3(-1, 0, 0));
	ret = CollisionDetection::intersects(ray2, box1, out);
	BOOST_CHECK(ret);
	CollisionDetection::intersects(ray2, box1, out,entryIndex,exitIndex);
	BOOST_CHECK(entryIndex == 0);
	BOOST_CHECK(exitIndex == 3);

	//直线-三角形
	Triangle tri1(Vector3(-1,0,0), Vector3(1,0,0), Vector3(0,1,0));
	real_type u, v, w;
	ret = CollisionDetection::intersects(Vector3(0,1,1), Vector3(0,1,0), tri1, u,v,w);
	BOOST_CHECK(ret);

	//线段-三角形
	ret = CollisionDetection::intersects(LineSegment(Vector3(0, 0, 1), Vector3(0, 0.2, -1)), tri1, u, v, w);
	BOOST_CHECK(ret);

	//射线-三角形
	ret = CollisionDetection::intersects(Ray(Vector3(0, 0.2, 100), Vector3(0, 0, -1)), tri1, u, v, w);
	BOOST_CHECK(ret);
}

BOOST_AUTO_TEST_CASE(case_upe_collisiondetection_5)
{
	bool ret{ false };
	Vector3 out{};

	//线段-圆柱体
	Cylinder cy1(Vector3(0,1,0), Vector3(0,-1,0), 1.0);
	//内
	Vector3 start(0, 0, 0);
	Vector3 end(0.5, 0.5, 0);
	ret = CollisionDetection::intersects(LineSegment(start,end),cy1,out);
	BOOST_CHECK(ret);

	//表
	start = Vector3(1, 0, 0);
	end = Vector3(2, 0.0, 0);
	ret = CollisionDetection::intersects(LineSegment(start,end), cy1, out);
	BOOST_CHECK(ret);

	//top上
	start = Vector3(0, 1, 0);
	end = Vector3(0, 2, 0);
	ret = CollisionDetection::intersects(LineSegment(start, end), cy1, out);
	BOOST_CHECK(ret);

	//bottom上
	start = Vector3(0, -1, 0);
	end = Vector3(0, 2, 0);
	ret = CollisionDetection::intersects(LineSegment(start, end), cy1, out);
	BOOST_CHECK(ret);

	//top外
	start = Vector3(0, 1.1, 0);
	end = Vector3(0, 2, 0);
	ret = CollisionDetection::intersects(LineSegment(start, end), cy1, out);
	BOOST_CHECK(!ret);

	//bottom外
	start = Vector3(0, -1.1, 0);
	end = Vector3(0, -2, 0);
	ret = CollisionDetection::intersects(LineSegment(start, end), cy1, out);
	BOOST_CHECK(!ret);

	//弧面外
	start = Vector3(1.1, 0, 0);
	end = Vector3(2, 1, 0);
	ret = CollisionDetection::intersects(LineSegment(start, end), cy1, out);
	BOOST_CHECK(!ret);

	//top外
	start = Vector3(0, 5, 0);
	end = Vector3(0, 2, 0);
	ret = CollisionDetection::intersects(LineSegment(start, end), cy1, out);
	BOOST_CHECK(!ret);

	//bottom外
	start = Vector3(0, -6, 0);
	end = Vector3(0, -2, 0);
	ret = CollisionDetection::intersects(LineSegment(start, end), cy1, out);
	BOOST_CHECK(!ret);

	//弧面外
	start = Vector3(8, 0, 0);
	end = Vector3(2, 0.5, 0);
	ret = CollisionDetection::intersects(LineSegment(start, end), cy1, out);
	BOOST_CHECK(!ret);

	//top外
	start = Vector3(0, 5, 0);
	end = Vector3(0.5, 0, 0);
	ret = CollisionDetection::intersects(LineSegment(start, end), cy1, out);
	BOOST_CHECK(ret);

	//bottom外
	start = Vector3(0, -6, 0);
	end = Vector3(0.3, 2, 0);
	ret = CollisionDetection::intersects(LineSegment(start, end), cy1, out);
	BOOST_CHECK(ret);

	//弧面外
	start = Vector3(10, 0, 0);
	end = Vector3(-10, 0.0, 0);
	ret = CollisionDetection::intersects(LineSegment(start, end), cy1, out);
	BOOST_CHECK(ret);

	//把圆柱体放倒
	//弧面外
	start = Vector3(0, 0, -3);
	end = Vector3(-1.2, 0.1, 3);
	ret = CollisionDetection::intersects(LineSegment(start, end), 
		Cylinder(Vector3(5,0,0),Vector3(-5,0,0),1.8), out);
	BOOST_CHECK(ret);
}

BOOST_AUTO_TEST_CASE(case_upe_collisiondetection_6)
{
	bool ret{ false };
	Vector3 out{};

	//射线-圆柱体
	Cylinder cy1(Vector3(0, 1, 0), Vector3(0, -1, 0), 1.0);
	//弧面外
	Vector3 start(10, 0.5, 0);
	Vector3 dir(-2, 0, 0);
	ret = CollisionDetection::intersects(Ray(start,dir), cy1, out);
	BOOST_CHECK(ret);
	//bottom外
	start = Vector3(0, -6, 0);
	dir = Vector3(0.3, 2, 0);
	ret = CollisionDetection::intersects(Ray(start,dir), cy1, out);
	BOOST_CHECK(ret);

	//线段-凸多面体
	Vector3 entry, exit;
	std::shared_ptr<Plane> pT(UNG_NEW_SMART Plane(Vector3(0,1,0),1.0));
	std::shared_ptr<Plane> pB(UNG_NEW_SMART Plane(Vector3(0,-1,0),1.0));
	PlaneBoundedVolume pbv;
	pbv.addPlane(pT);
	pbv.addPlane(pB);
	ret = CollisionDetection::intersects(pbv,LineSegment(Vector3(0.5,4,0),Vector3(0.6,-7,0)),entry,exit);
	BOOST_CHECK(ret);

	//射线-凸多面体
	ret = CollisionDetection::intersects(pbv, Ray(Vector3(0.5, 4, 0), (Vector3(0.6, -7, 0) - Vector3(0.5, 4, 0)).normalizeCopy()), entry, exit);
	BOOST_CHECK(ret);

	//平面-平面
	Vector3 p0, lineDir;
	ret = CollisionDetection::intersects(Plane(Vector3(3,7,5),4.8),Plane(Vector3(2,6,9),3.9),p0,lineDir);
	BOOST_CHECK(ret);
}

BOOST_AUTO_TEST_CASE(case_upe_collisiondetection_7)
{
	//认为一个球体完全处于AABB内时，其碰撞检测为true
	Sphere s1(Vector3(0), 1.0);
	AABB box1(Vector3(-5), Vector3(5));

	auto ret = ungIsSphereFullyInsideAABB(s1, box1);
	BOOST_CHECK(ret);
	ret = CollisionDetection::intersects(s1, box1);
	BOOST_CHECK(ret);

	//认为一个AABB完全处于球体内时，其碰撞检测为true
	Sphere s2(Vector3(0), 10.0);
	AABB box2(Vector3(-1), Vector3(1));

	ret = ungIsAABBFullyInsideSphere(box2,s2);
	BOOST_CHECK(ret);
	ret = CollisionDetection::intersects(s2, box2);
	BOOST_CHECK(ret);

	//认为一个AABB完全处于另一个AABB内时，其碰撞检测为true
	ret = CollisionDetection::intersects(box1, box2);
	BOOST_CHECK(ret);
}

BOOST_AUTO_TEST_SUITE_END()

#endif//SUITE_UPE_COLLISIONDETECTION