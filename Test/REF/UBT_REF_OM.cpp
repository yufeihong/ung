/*!
 * \brief
 * 对对象模型的测试
 * \file UBT_REF_OM.cpp
 *
 * \author Su Yang
 *
 * \date 2017/03/21
 */
//#define SUITE_REF_OM

#ifdef SUITE_REF_OM

#include "UBTConfig.h"

struct Test1
{
	char c;					//1
};

struct Test2
{
	short s;					//2
	char c;					//1
};

struct Test3
{
	char c;					//1
	short s;					//2
};

struct Test4
{
	double d;				//8
	int i;						//4
	char c;					//1
	float f;					//4
	short s;					//2
};

struct Test5
{
	Test5() :
		d1(1.0),
		i1(2),
		c1('a'),
		f(3.0),
		c2('b'),
		c3('c'),
		i2(4),
		d2(5.0)
	{
	}

	double d1;				//8
	int i1;					//4
	char c1;					//1
	float f;					//4
	char c2;					//1
	char c3;					//1
	int i2;					//4
	double d2;				//8
};

BOOST_AUTO_TEST_SUITE(suite_ref_om)

//测试填充空隙和结构的对齐需求
BOOST_AUTO_TEST_CASE(case_ref_om_1)
{
	//计算Test1的字节数
	auto s1 = sizeof(Test1);
	//Test1为1个字节大小，编译器没有任何填充
	BOOST_CHECK(s1 == 1);

	//计算Test2的字节数
	auto s2 = sizeof(Test2);
	//Test2为4个字节大小，编译器在c后面填充了1个字节，这也说明了Test2的对齐需求为short的2个字节。
	BOOST_CHECK(s2 == 4);

	//计算Test3的字节数
	auto s3 = sizeof(Test3);
	//Test3为4个字节大小，编译器在c后面填充了1个字节，这也说明了Test3的对齐需求为short的2个字节。
	BOOST_CHECK(s3 == 4);

	/*
	Test2和Test3的示例说明，不论把char放置在short的前面还是后面，编译器都统一按照short的对齐需求
	来进行填充。
	这种行为可以理解为，如果把char放置在了short的后面，我们假设认为char就乖乖地占用1个字节就妥了，
	但是如果把Test2作为数组的元素呢？如果编译器不进行填充，那么当CPU读/写数组后面的元素的元素时，
	就可能需要掩码，移位，逻辑OR操作了。
	*/

	//计算Test4的字节数
	auto s4 = sizeof(Test4);
	//Test4的对齐需求为int/float的4个字节。
	BOOST_CHECK(s4 == 24);

	/*
	Test4的示例说明，编译器并没有按照字节数为8的double来设置对齐需求，而是按照int/float的4字节来
	设置了对齐需求，这应该是不同的编译器有不同的设置，或者和CPU的位宽有关系。
	*/

	//计算Test5的字节数
	auto s5 = sizeof(Test5);
	BOOST_CHECK(s5 == 40);
	Test5* p5 = new Test5;
	//d1的地址
	void* p_d1 = p5;
	UBT_CHECK_EQUAL_FLOAT(*(static_cast<double*>(p_d1)),1.0);
	//i1的地址
	//在取i1的地址之前，我们先试着让p5往后挪动1个字节看看
	void* p5_1 = p5 + 1;
	//检查一下，看看p5_1的实际地址，是不是真的就是p5的地址加了1个字节
	auto a5 = (unsigned long long)(p5);
	auto a5_1 = (unsigned long long)(p5_1);
	BOOST_CHECK((a5_1 - a5) == sizeof(Test5));
	/*
	我们发现，并不是我们所期待的那样，指针往后挪了1个字节，而是挪动了sizeof(Test5)，40个字节。
	为什么会这样，原因就是：Test5* p5。
	*/
	//那么我们重新取对象的开始地址
	char* pBeg = reinterpret_cast<char*>(new Test5);
	//取i1的地址
	void* p_i1 = pBeg + sizeof(double);
	BOOST_CHECK(*(static_cast<int*>(p_i1)) == 2);
	//取f的地址。必须把f前面的padding也给计算进去(double_8,int_4,char_4)
	void* p_f = pBeg + 16;
	BOOST_CHECK(*(static_cast<float*>(p_f)) == 3.0);
	/*
	这里，我们在取f的地址时，对char按照4个字节对齐来计算，但是这就要求我们知道当前编译器的对齐
	需求准确是几个字节。如果考虑到跨平台呢？
	所以，这种做法不具备通用型。最好通过接口来准确的返回。
	*/

	//取c3的地址，看看c2和c3是共用一个4字节空间呢？还是它们各自占用了一个4字节空间
	void* p_c3 = pBeg + 21;
	BOOST_CHECK(*(static_cast<char*>(p_c3)) == 'c');
	/*
	事实证明，编译器把c2和c3给紧密的放置在了一起。
	*/

	//取i2的地址
	void* p_i2 = pBeg + 24;
	BOOST_CHECK(*(static_cast<int*>(p_i2)) == 4);

	//取d2的地址
	void* p_d2 = pBeg + 32;
	UBT_CHECK_EQUAL_FLOAT(*(static_cast<double*>(p_d2)),5.0);

	/*
	通过取d2的地址，我们发现，i2竟然占用了8个字节！
	这是什么原因呢？
	试着取i2和d2中间的8个字节的后面4个字节，看看是什么。
	*/

	void* p_temp = pBeg + 28;
	auto vvv = *(static_cast<int*>(p_temp));
	//是垃圾数据

	/*
	综上：
	class中的数据成员要按照其类型字节数的大小，从大往小排序。
	当数据成员的排列参差不齐时，编译器可能会毫无理由的进行填充。
	编译器会紧密放置相邻的类型字节数小的数据成员。
	代码取地址时，不要依赖于编译器的对齐需求字节数，而是应该依赖于接口来准确地获取具体某个数据成员
	的地址。
	记住，地址后移时，指针类型必须为char*。
	*/
}

BOOST_AUTO_TEST_SUITE_END()

#endif//SUITE_REF_OM