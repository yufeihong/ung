//#define SUITE_UML_MATRIX3

#ifdef SUITE_UML_MATRIX3

#include "UBTConfig.h"

using namespace ung;

BOOST_AUTO_TEST_SUITE(suite_uml_matrix3)

//构造函数
BOOST_AUTO_TEST_CASE(case_uml_matrix3_1)
{
	Vector3 v1( 3.9,8.1,6.37 );

	Matrix3 m1( 3.54,2.59,12.77,
		9.05,6.1,47.375,
		1.3,6.561,4.91);

	//向量乘以矩阵
	auto v2 = v1 * m1;
	UBT_CHECK_EQUAL_VEC(v2, Vector3(95.392,101.30457,464.8172));

	Matrix3 m2( 7.54,1.59,2.77,
		11.05,7.1,4.375,
		12.3,1.561,7.91);

	BOOST_CHECK(m1 != m2);

	//矩阵相乘
	auto m3 = m1 * m2;

	Matrix3 m4(
		212.38209999999998, 43.951570000000004, 122.14775000000000,
		718.35449999,131.651875,426.49225,
		142.69405,56.31460999,71.143474999);
	BOOST_CHECK(m3 == m4);

	//乘以标量
	Matrix3 m5{1,2,3,4,5,6,7,8,9};
	m5 *= 2;

	//行列式
	auto d = m2.determinant();
	UBT_CHECK_EQUAL_FLOAT(d, 124.423351);
	//矩阵的转置行列式不变
	Matrix3 m18{};
	m2.transpose(m18);
	auto dt = m18.determinant();
	UBT_CHECK_EQUAL_FLOAT(dt,124.423351);
	Matrix3 m19{4,2,-2,-2,5,1,6,0,-4};
	auto dt1 = m19.determinant();
	UBT_CHECK_EQUAL_FLOAT(dt1, -24.0);
	//互换两行或两列，行列式为其原值的负
	Matrix3 m20{ -2,5,1,4,2,-2,6,0,-4 };
	auto dt2 = m20.determinant();
	UBT_CHECK_EQUAL_FLOAT(dt2, 24.0);
	//矩阵A和矩阵B的乘积的行列式，等于各自行列式的乘积。
	Matrix3 m21 = m18 * m20;
	auto dt3 = m21.determinant();
	UBT_CHECK_EQUAL_FLOAT(dt3, 124.423351 * 24.0);

	//逆矩阵
	Matrix3 m6;
	auto b = m2.inverse(m6);
	Matrix3 m7(
		0.396482047811106,-0.066329430397675,-0.102157271105807,
		-0.269989513463594,0.205511262913985,-0.019120205177563,
		-0.563245961764846,0.062585197532576,0.289049440566827);

	BOOST_CHECK(m6 == m7);

	//[]运算符
	Vector3 v4{m7[2]};
	UBT_CHECK_EQUAL_VEC(v4,Vector3(-0.563245961764846,0.062585197532576,0.289049440566827));

	ung::Matrix3 m17{};
	auto m00 = m17.getElement(0, 0);
	m00 = 2.0;
	//此时m17的第一个元素值，并没有改变，这是因为auto推断出来的不是引用，而是值
	auto& m00R = m17.getElement(0, 0);
	m00R = 2.0;

	//施密特正交化
	auto m8 = m2;
	m2.orthogonalization();
	//正交矩阵的行列式为1或-1
	auto d1 = m2.determinant();
	Matrix3 m2T;
	m2.transpose(m2T);
	auto m9 = m2 * m2T;
	BOOST_CHECK(m9 == Matrix3::IDENTITY);

	//沿任意方向缩放
	Vector3 v5{1.0,1.0,0.0};			//缩放方向
	Matrix3 m10 = Matrix3::makeScaleAnyDirection(v5,3.5);
	auto v6 = v5 * m10;				//对v5进行缩放
	BOOST_CHECK(v6 == Vector3(3.5,3.5,0.0));

	//绕x轴旋转r弧度
	Matrix3 m11 = Matrix3::makeRotateX(Radian(Constants::FOURTHPI));
	Vector3 v7{0.0,1.0,-1.0};
	auto v8 = v7 * m11;
	BOOST_CHECK(v8 == Vector3(0.0,v7.length(),0.0));

	//绕y轴旋转r弧度
	Matrix3 m12 = Matrix3::makeRotateY(Radian(Constants::FOURTHPI));
	Vector3 v9{ 1.0,0.0,1.0 };
	auto v10 = v9 * m12;
	BOOST_CHECK(v10 == Vector3(v9.length(),0.0 , 0.0));

	//绕z轴旋转r弧度
	Matrix3 m13 = Matrix3::makeRotateZ(Radian(Constants::FOURTHPI));
	Vector3 v11{ 1.0,1.0,0.0 };
	auto v12 = v11 * m13;
	BOOST_CHECK(v12 == Vector3(0.0, v11.length(), 0.0));

	//绕任意轴旋转r弧度
	Matrix3 m14 = Matrix3::makeRotateAnyAxis(Vector3(0.0,0.0,2.0),Radian(Constants::FOURTHPI));
	BOOST_CHECK(m14 == m13);

	//抽取旋转矩阵的轴和角度(弧度)
	Vector3 v13{};
	Radian hudu{};
	m14.extractAxisAngle(v13,hudu);
	BOOST_CHECK(v13 == Vector3(0.0,0.0,1.0));
	BOOST_CHECK(hudu == Radian(Constants::FOURTHPI));

	//正交投影
	Vector3 v14{ 1.0,1.0,1.0 };				//被投影的向量
	Vector3 v15{ 1.0,1.0,1.0 };				//投影方向
	Matrix3 m15 = Matrix3::makeOrthogonal(v15);
	Vector3 v16 = v14 * m15;
	BOOST_CHECK(v16 == Vector3::ZERO);
	Vector3 v17{0.0,0.0,-1.0};				//投影方向
	m15 = Matrix3::makeOrthogonal(v17);
	Vector3 v18 = v14 * m15;
	UBT_CHECK_EQUAL_FLOAT(v18.z, 0.0);

	//镜像(反射)
	Vector3 v19{0.0,0.0,1.0};
	Matrix3 m16 = Matrix3::makeReflectAnyPlane(v19);
	Vector3 v20 = v14 * m16;
	BOOST_CHECK(v20 == Vector3(1.0, 1.0, -1.0));
}

BOOST_AUTO_TEST_CASE(case_uml_matrix3_2)
{
	//从四元数来构建一个旋转矩阵，绕z轴旋转Constants::FOURTHPI
	Vector3 zAxis{ 0,0,1 };
	auto theta{ Constants::FOURTHPI };
	Quaternion q(zAxis,Radian(Constants::FOURTHPI));

	Vector3 axis1{};
	Radian r1;
	q.extractAxisAngle(axis1, r1);
	BOOST_CHECK(axis1 == zAxis);
	BOOST_CHECK(r1 == Radian(theta));

	Vector3 vec{ 1,1,0 };
	auto rotatedVec = q * vec;
	BOOST_CHECK(rotatedVec == Vector3(0,Math::calSqrt(2),0));

	//从四元数来构建一个旋转矩阵
	Vector3 axis2{};
	Radian r2;
	Matrix3 m1 = q.getMatrixOfRotation();
	m1.extractAxisAngle(axis2,r2);
	BOOST_CHECK(axis2 == zAxis);
	BOOST_CHECK(Radian(theta) == r2);
	rotatedVec = vec * m1;
	BOOST_CHECK(rotatedVec == Vector3(0,Math::calSqrt(2),0));

	//从旋转矩阵来构建一个四元数
	Quaternion q1 = m1.getQuaternionOfRotate();
	BOOST_CHECK(q1 == q);
}

BOOST_AUTO_TEST_SUITE_END()

#endif//SUITE_UML_MATRIX3