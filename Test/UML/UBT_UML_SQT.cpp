//#define SUITE_UML_SQT

#ifdef SUITE_UML_SQT

#include "UBTConfig.h"

using namespace ung;

BOOST_AUTO_TEST_SUITE(suite_uml_sqt)

BOOST_AUTO_TEST_CASE(case_uml_sqt_1)
{
	//定义一个xy平面的pos和向量
	Vector4 pos{ 1,1,0,1 };
	Vector3 vec{ 1,1,0 };

	//一个单位SQT
	SQT identitySQT{};
	BOOST_CHECK(identitySQT.isIdentity());

	//分别对pos和vec应用单位SQT
	auto p1 = pos * identitySQT;
	auto v1 = vec * identitySQT;
	//没有任何变化
	BOOST_CHECK(p1 == pos);
	BOOST_CHECK(v1 == vec);

	//一个仅包含缩放的SQT
	SQT scaleSQT{};
	scaleSQT.setScale(2);
	//分别对pos和vec应用仅包含缩放的SQT
	auto p2 = pos * scaleSQT;
	auto v2 = vec * scaleSQT;
	//pos和vec都发生了变化
	BOOST_CHECK(p2 == Vector4(2, 2, 0, 1));
	BOOST_CHECK(v2 == Vector3(2,2,0));

	//一个包含了缩放和旋转的SQT
	SQT srSQT{};
	srSQT.setScale(2);
	srSQT.setRotateD(Vector3(0, 0, 1), 45);
	//分别对pos和vec应用仅包含缩放的SQT
	auto p3 = pos * srSQT;
	auto v3 = vec * srSQT;
	//pos和vec都发生了变化
	BOOST_CHECK(p3 == Vector4(0, Math::calSqrt(8), 0, 1));
	BOOST_CHECK(v3 == Vector3(0, Math::calSqrt(8), 0));

	//一个仅包含平移的SQT
	SQT translateSQT{};
	translateSQT.setTranslate(1, 2, 3);
	//分别对pos和vec应用仅包含平移的SQT
	auto p4 = pos * translateSQT;
	auto v4 = vec * translateSQT;
	//pos发生了变化，而vec没有变化
	BOOST_CHECK(p4 == Vector4(2,3,3,1));
	BOOST_CHECK(v4 == vec);

	//一个SQT
	SQT sqt{};
	sqt.setTranslate(1, 2, 3);
	sqt.setScale(3);
	sqt.setRotateD(Vector3(0, 0, 1), 45);
	auto p5 = pos * sqt;
	auto v5 = vec * sqt;
	BOOST_CHECK(p5 == Vector4(1,2 + Math::calSqrt(18), 3, 1));
	BOOST_CHECK(v5 == Vector3(0,Math::calSqrt(18), 0));

	//连接两个SQTImpl
	SQT sqt1{};
	sqt1.setTranslate(1, 2, 3);
	SQT sqt2{};
	sqt2.setScale(3);
	SQT sqt3{};
	sqt3.setRotateD(Vector3(0, 0, 1), 45);
	auto sqt4 = sqt1 * sqt2 * sqt3;
	BOOST_CHECK(sqt4 == sqt);

	//*=
	SQT sqt5{};
	sqt5 *= sqt2;
	sqt5 *= sqt1;
	sqt5 *= sqt3;
	//连接顺序无所谓
	BOOST_CHECK(sqt5 == sqt);

	//SQT到仿射矩阵
	Matrix4 affine{};
	sqt5.toAffineMatrix(affine);
	auto p6 = pos * affine;
	auto v6 = vec * affine;
	BOOST_CHECK(p6 == p5);
	BOOST_CHECK(v6 == v5);
	Matrix4 sMat4{}, rMat4{}, tMat4{}, mat4{};
	sMat4.setScale(3);
	tMat4.setTrans(1, 2, 3);
	rMat4.setRotateZD(45);
	mat4 = sMat4 * rMat4 * tMat4;
	BOOST_CHECK(affine == mat4);

	//插值
	SQT start{}, end{};
	start.setRotateD(Vector3(0, 0, 1), 0);
	end.setRotateD(Vector3(0, 0, 1), 90);
	end.setScale(4);
	end.setTranslate(4, 4, 0);
	SQT middle = SQT::slerp(start, end, 0.5);
	auto p7 = pos * middle;
	auto v7 = vec * middle;
	BOOST_CHECK(p7 == Vector4(2,2 + Math::calSqrt(12.5),0,1));
	BOOST_CHECK(v7 == Vector3(0,Math::calSqrt(12.5),0));
}

BOOST_AUTO_TEST_SUITE_END()

#endif//SUITE_UML_SQT