#define SUITE_UML_MATRIX4

#ifdef SUITE_UML_MATRIX4

#include "UBTConfig.h"

#include "UBSFloat.h"

#include <d3dx9.h>
#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"d3dx9d.lib")

using namespace ung;

BOOST_AUTO_TEST_SUITE(suite_uml_matrix4)

BOOST_AUTO_TEST_CASE(case_uml_matrix4_1)
{
	//一个任意的点
	Vector4 pos( 3.7,2.81,5.09,1.0 );
	//一个任意的向量
	Vector4 vec( 8.53,3.06,2.1,0.0 );
	//一个任意的矩阵
	Matrix4 mat(
		1.5,4.7,2.6,0.0,
		5.2,2.5,7.8,0.0,
		6.3,9.04,3.5,0.0,
		1.0,2.0,3.0,1.0);

	//打印矩阵
	//std::cout << mat << std::endl;

	//向量乘以矩阵
	auto v1 = pos * mat;
	BOOST_CHECK(v1 == Vector4(53.229000000000006,72.428600000000003,52.352999999999994,1.000000000000000));

	//从一个Matrix3来构造一个Matrix4
	Matrix3 m1;
	Matrix4 m2(m1);
	Matrix4 m3 = m1;
	Matrix4 m4;
	m4 = m1;
	
	//获取某一行
	auto v2 = mat[2];
	BOOST_CHECK(v2 == Vector4(6.3, 9.04, 3.5, 0.0));

	//两个矩阵相乘
	Matrix4 m5(
		1.2,5.3,6.04,0.0,
		4.3,8.7,1.9,0.0,
		6.4,4.1,2.8,0.0,
		3.0,2.5,4.9,1.0);

	auto m6 = m5 * mat;
	Matrix4 m7(
		67.412000000000006,73.491600000000005,65.599999999999994,0,
		63.659999999999997,59.135999999999996,85.689999999999998,0,
		48.560000000000002,65.641999999999996,58.419999999999995,0,
		49.370000000000005,66.646000000000001,47.450000000000003,1.0000000);
	BOOST_CHECK(m6 == m7);

	//计算矩阵的行列式
	auto s1 = m7.determinant();
	//注意宏参数2的写法
#if UNG_USE_DOUBLE
	UBT_CHECK_EQUAL_FLOAT(s1, -2.805605403160000e+04);
#else
	UBT_CHECK_EQUAL_FLOAT(s1, -28056.0625);
#endif

	Matrix4 m15{};
	m7.transpose(m15);
	//转置矩阵的行列式不变
	auto s1t = m15.determinant();
#if UNG_USE_DOUBLE
	UBT_CHECK_EQUAL_FLOAT(s1t, -2.805605403160000e+04);
#else
	UBT_CHECK_EQUAL_FLOAT(s1t, -28056.0625);
#endif

	//计算矩阵的逆矩阵
	Matrix4 m8;
	bool ret = mat.inverse(m8);
	Matrix4 m9(
		-0.460752966523382,0.052623804699588,0.224997724658288,0,
		0.230816631330486,-0.083031322130197,0.013577448901793,0,
		0.233188954819920,0.119735194985595,-0.154350229548408,0,
		-0.700447160597348,-0.245766745395977,0.210898066183349,1.000000000);
	BOOST_CHECK(m8 == m9);

	//绕x轴旋转r弧度
	Matrix4 m10;
	m10.setRotateX(Radian(Constants::FOURTHPI));
	Vector4 v3{ 0.0,1.0,-1.0 ,0.0};
	auto v4 = v3 * m10;
	BOOST_CHECK(v4 == Vector4(0.0,Vector3(0.0,1.0,-1.0).length(),0.0,0.0));

	//view矩阵
	Vector3 eyeWorldPos{ 20.0, 3.0,5.0 };
	Vector3 targetWorldPos{10.0,2.0,8.0};
	Matrix4 viewMat = Matrix4::makeView(eyeWorldPos, targetWorldPos);
	Matrix4 m12(
		-0.287347885566,		-0.091325152995,		0.9534625892,		0.000000000,
		0.000000000,				0.99544416765,			0.09534625892,		0.000000000,
		-0.95782628522,			0.0273975458986,		-0.28603877677,		0.000000000,
		10.536089137,			-1.2968171725,			-17.925096677,		1.00000000);
	//BOOST_CHECK(viewMat == m12);
	//测试摄像机的朝向是正z轴还是负z轴
	Vector3 eyeWorldPos2{ 0.0, 0.0,0.0 };
	Vector3 targetWorldPos2{ 10.0,0.0,0.0 };
	Matrix4 viewMat2 = Matrix4::makeView(eyeWorldPos2, targetWorldPos2);
	auto targetWorldPos2InCameraLoaclSpace = targetWorldPos2 * viewMat2;
	//结果表明摄像机的朝向是-z轴
	//BOOST_CHECK(targetWorldPos2InCameraLoaclSpace == Vector3(0,0,-10));
	//再用Direct3D的API函数来进行验证
	D3DXVECTOR3 d9_eye(20.0, 3.0,5.0);
	D3DXVECTOR3 d9_at(10.0,2.0,8.0);
	D3DXVECTOR3 d9_up(0.0,1.0,0.0);
	D3DXMATRIX d9_view;

	//D3DXMatrixLookAtLH(&d9_view, &d9_eye, &d9_at, &d9_up);
	//下面使用右手
	D3DXMatrixLookAtRH(&d9_view, &d9_eye, &d9_at, &d9_up);
	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			BOOST_CHECK(Math::isEqual(viewMat[i][j], d9_view(i,j)));
		}
	}
	//说明D3DXMatrixLookAtRH()函数(右手)将摄像机移动到原点，且摄像机的朝向为-z轴(指向屏幕内)

	//D3D投影矩阵
	Matrix4 d3dProj = Matrix4::makePerspectiveD3D(800.0 / 600.0);
	Matrix4 m13(
		1.2990381056,	0.000000000,		0.000000000,		0.000000000,
		0.000000000,		1.7320508075,	0.000000000,		0.000000000,
		0.000000000,		0.000000000,		-1.00100100,		-1.00000000,
		0.000000000,		0.000000000,		-1.00100100,		0.000000000);
	BOOST_CHECK(d3dProj == m13);
	//再用Direct3D的API函数来进行验证
	real_type d9_fovY = Consts<real_type>::THIRDPI;
	//纵横比用于校正由方形的投影窗口到矩形的显示屏的映射而引发的畸变。
	real_type d9_aspect = 800.0 / 600.0;
	real_type d9_zn = 1.0;
	real_type d9_zf = 1000.0;
	D3DXMATRIX d9_proj;
	D3DXMatrixPerspectiveFovRH(&d9_proj, d9_fovY, d9_aspect, d9_zn, d9_zf);
	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			BOOST_CHECK(Math::isEqual(d3dProj[i][j], d9_proj(i, j)));
		}
	}

	//OpenGL投影矩阵
	Matrix4 openglProj = Matrix4::makePerspectiveOpenGL(800.0 / 600.0);
	Matrix4 m14(
		1.2990381056,	0.000000000,		0.000000000,		0.000000000,
		0.000000000,		1.7320508075,	0.000000000,		0.000000000,
		0.000000000,		0.000000000,		-1.00200200,		-1.00000000,
		0.000000000,		0.000000000,		-2.00200200,		0.000000000);
	BOOST_CHECK(openglProj == m14);

	//----------------------------------------------------------------------------------

	//获取相机的u,v,n
	Vector3 U;
	Vector3 V(0,1,0);
	//从目标位置指向相机位置，（之所以是从目标指向相机，是因为我们使用的是右手坐标系），视见方向为-N
	Vector3 N;
	N = eyeWorldPos - targetWorldPos;
	N.normalize();
	U = V.crossProductUnit(N);
	U.normalize();
	V = N.crossProductUnit(U);
	BOOST_CHECK(V.isUnitLength());
	//测试在裁剪空间中，平截头体的8个角顶点位置
	Vector3 centerDir = -N;

	real_type aspect = 800.0 / 600.0;
	real_type n = 1.0;
	real_type f = 1000.0;

	/*
	平截头体的8个角的索引:
	近：
	左上：0
	右上：1
	右下：2
	左下：3
	远：
	左上：4
	右上：5
	右下：6
	左下：7
	*/

	Radian halfY{ Consts<real_type>::THIRDPI * 0.5 };
	auto tanHalfY = Math::calTan(halfY);

	//近平面的h
	auto nH = tanHalfY * n * 2.0;
	auto nHalfH = nH / 2.0;
	//近平面的w
	auto nW = aspect * nH;
	auto nHalfW = nW / 2.0;
	//远平面的h
	auto fH = tanHalfY * f * 2.0;
	auto fHalfH = fH / 2.0;
	//远平面的w
	auto fW = aspect * fH;
	auto fHalfW = fW / 2.0;

	//近平面中心点
	Vector4 nCenter = Vector4(eyeWorldPos + centerDir * n,1.0);
	//远平面中心点
	Vector4 fCenter = Vector4(eyeWorldPos + centerDir * f,1.0);

	//角
	Vector4 corner0 = nCenter - U * nHalfW + V * nHalfH;
	Vector4 corner1 = nCenter + U * nHalfW + V * nHalfH;
	Vector4 corner2 = nCenter + U * nHalfW - V * nHalfH;
	Vector4 corner3 = nCenter - U * nHalfW - V * nHalfH;

	Vector4 corner4 = fCenter - U * fHalfW + V * fHalfH;
	Vector4 corner5 = fCenter + U * fHalfW + V * fHalfH;
	Vector4 corner6 = fCenter + U * fHalfW - V * fHalfH;
	Vector4 corner7 = fCenter - U * fHalfW - V * fHalfH;

	bool openglFlag{ true };
	//view和proj矩阵
	auto mVP = viewMat * openglProj;

	////对相机坐标进行变换
	//auto ep = Vector4(0.1,0,0, 1);
	//auto ep2 = ep * viewMat2;
	//auto ep3 = ep2 * openglProj;
	//ep3 /= ep3.w;

	//对近平面中心点进行变换
	auto nc = nCenter * mVP;
	nc /= nc.w;

	//对远平面中心点进行变换
	auto fc = fCenter * mVP;
	fc /= fc.w;

	//对8个角进行变换
	auto c0 = corner0 * mVP;
	c0 /= c0.w;
	auto c1 = corner1 * mVP;
	c1 /= c1.w;
	auto c2 = corner2 * mVP;
	c2 /= c2.w;
	auto c3 = corner3 * mVP;
	c3 /= c3.w;
	auto c4 = corner4 * mVP;
	c4 /= c4.w;
	auto c5 = corner5 * mVP;
	c5 /= c5.w;
	auto c6 = corner6 * mVP;
	c6 /= c6.w;
	auto c7 = corner7 * mVP;
	c7 /= c7.w;

	if (openglFlag)
	{
		BOOST_CHECK(c0 == Vector4(-1.0,1.0,-1.0,1.0));
		BOOST_CHECK(c1 == Vector4(1.0,1.0,-1.0,1.0));
		BOOST_CHECK(c2 == Vector4(1.0,-1.0,-1.0,1.0));
		BOOST_CHECK(c3 == Vector4(-1.0,-1.0,-1.0,1.0));
		BOOST_CHECK(c4 == Vector4(-1.0,1.0,1.0,1.0));
		BOOST_CHECK(c5 == Vector4(1.0,1.0,1.0,1.0));
		BOOST_CHECK(c6 == Vector4(1.0,-1.0,1.0,1.0));
		BOOST_CHECK(c7 == Vector4(-1.0,-1.0,1.0,1.0));
	}
	else
	{
		BOOST_CHECK(c0 == Vector4(-1.0, 1.0, 0.0,1.0));
		BOOST_CHECK(c1 == Vector4(1.0, 1.0, 0.0,1.0));
		BOOST_CHECK(c2 == Vector4(1.0, -1.0, 0.0,1.0));
		BOOST_CHECK(c3 == Vector4(-1.0, -1.0, 0.0,1.0));
		BOOST_CHECK(c4 == Vector4(-1.0, 1.0, 1.0,1.0));
		BOOST_CHECK(c5 == Vector4(1.0, 1.0, 1.0,1.0));
		BOOST_CHECK(c6 == Vector4(1.0, -1.0, 1.0,1.0));
		BOOST_CHECK(c7 == Vector4(-1.0, -1.0, 1.0,1.0));
	}

	int a = 0;
}

BOOST_AUTO_TEST_SUITE_END()

#endif//SUITE_UML_MATRIX4