//#define SUITE_UML_VEC4

#ifdef SUITE_UML_VEC4

#include "UBTConfig.h"

using namespace ung;

#include <random>
#include <functional>

using V = Vector4;

BOOST_AUTO_TEST_SUITE(suite_uml_vec4)

BOOST_AUTO_TEST_CASE(case_uml_vec4_1)
{
	V v1;
	UBT_CHECK_EQUAL_VEC(v1, V{});

	V v2(1.0,2.0,3.0,4.0);
	UBT_CHECK_EQUAL_FLOAT(v2.w,4.0);

	V v3(2.0);
	UBT_CHECK_EQUAL_FLOAT(v3.y,2.0);

	real_type ar[4]{ 3.0,2.0 ,1.0,0.0};
	V v4(ar, 4);
	UBT_CHECK_EQUAL_FLOAT(v4.x, 3.0);

	V v5(ar);
	UBT_CHECK_EQUAL_FLOAT(v4.x, 3.0);

	V v6(Vector2(1.0,2.0),3.0,4.0);
	UBT_CHECK_EQUAL_VEC(v6, V(1.0,2.0,3.0,4.0));

	V v7(Vector3(1.0,2.0,3.0),4.0);
	UBT_CHECK_EQUAL_VEC(v7, V(1.0,2.0,3.0,4.0));

	V v8(v7);
	BOOST_CHECK(v8 == v7);

	V v9;
	v9 = v8;
	BOOST_CHECK(v9 == v8);

	//移动构造函数
	V v10{ V(1.0,2.0,3.0,4.0) };
	BOOST_CHECK(v10 == V(1.0,2.0,3.0,4.0));

	//移动赋值运算符
	V v11;
	v11 = std::move(v10);
	BOOST_CHECK(v11 == V(1.0,2.0,3.0,4.0));
}

BOOST_AUTO_TEST_CASE(case_uml_vec4_2)
{
	//梅森旋转，均匀分布随机数发生器
	std::random_device rndDev;
	std::mt19937 eng(rndDev());
	std::uniform_real_distribution<float> dist(0.0, 10.0);
	auto gen = std::bind(dist, eng);

	//---------------

	/*
	usize loop{100000};												//10万

	SteadyClock_ms timer;

	//SIMD
	for (usize i = 0; i < loop; ++i)
	{
		V v(gen(),gen(),gen(),1.0);

		Matrix4 m(gen(), gen(), gen(), gen(),
			gen(), gen(), gen(), gen(),
			gen(), gen(), gen(), gen(),
			gen(), gen(), gen(), 1.0);

		auto ret = v * m;
	}

	std::cout << timer.elapsed() << std::endl;
	*/

	V v1(1.0, 2.0, 5.0,1.0);

	Matrix4 m4(1.7,4.8,3.5,0.0,
	4.3,8.1,3.6,0.0,
	7.4,3.9,1.2,0.0,
	5.0,2.0,4.0,1.0);
	auto v2 = v1 * m4;
	UBT_CHECK_EQUAL_VEC(v2, V(52.3,42.5,20.7,1.0));
}

BOOST_AUTO_TEST_SUITE_END()

#endif//SUITE_UML_VEC4