//#define SUITE_UML_QUATERNION

#ifdef SUITE_UML_QUATERNION

#include "UBTConfig.h"

using namespace ung;

BOOST_AUTO_TEST_SUITE(suite_uml_quaternion)

BOOST_AUTO_TEST_CASE(case_uml_quaternion_1)
{
	//四元数的大小
	auto size = sizeof(Quaternion);

	//轴，角
	Vector3 axis{};
	real_type angle{};

	//默认构造函数(代表零旋转)
	Quaternion q1{};
	q1.extractAxisAngle(axis, angle);
	BOOST_CHECK(axis == Vector3(1, 0, 0));
	UBT_CHECK_EQUAL_FLOAT(angle, 0.0);

	//两个四元数的“差”
	Quaternion q2(Vector3(0,0,1), Constants::HALFPI);
	Quaternion q3(Vector3(0, 0, 1), Constants::FOURTHPI);
	Quaternion diff = q2.difference(q3);
	diff.extractAxisAngle(axis, angle);
	BOOST_CHECK(axis == Vector3(0, 0, -1));
	UBT_CHECK_EQUAL_FLOAT(angle, Constants::FOURTHPI);
	Quaternion diff2 = q3.difference(q2);
	diff2.extractAxisAngle(axis, angle);
	BOOST_CHECK(axis == Vector3(0, 0, 1));
	UBT_CHECK_EQUAL_FLOAT(angle, Constants::FOURTHPI);

	//绕X轴旋转的四元数
	Quaternion rx{};
	rx.setRotateXD(45);
	BOOST_CHECK(rx.isUnit());
	Vector3 vx{ 0,1,-1 };
	auto vxRet = rx * vx;
	BOOST_CHECK(vxRet == Vector3(0,Math::calSqrt(2),0));
	//绕Y轴旋转的四元数
	Quaternion ry{};
	ry.setRotateYD(45);
	Vector3 vy{ 1,0,1 };
	auto vyRet = ry * vy;
	BOOST_CHECK(vyRet == Vector3(Math::calSqrt(2),0, 0));
	//绕Z轴旋转的四元数
	Quaternion rz{};
	rz.setRotateZD(45);
	Vector3 vz{ 1,1,0 };
	auto vzRet = rz * vz;
	BOOST_CHECK(vzRet == Vector3(0,Math::calSqrt(2), 0));
	//构建绕任意轴旋转的四元数
	Quaternion ra{};
	ra.setRotateAnyAxisD(Vector3(0,0,-1),45);
	Vector3 va{ 1,1,0 };
	auto vaRet = ra * va;
	BOOST_CHECK(vaRet == Vector3(Math::calSqrt(2),0, 0));

	//[]运算符
	UBT_CHECK_EQUAL_FLOAT(q1[0], 1);

	//球面线性插值
	Vector3 sv{ Math::calSqrt(2),0,0 };
	Quaternion startQ{Vector3(0,0,1),0};
	Quaternion endQ{Vector3(0,0,1),Constants::PI};
	Quaternion halfway0 = Quaternion::slerp(startQ, endQ, 1.0);
	halfway0.extractAxisAngle(axis, angle);
	BOOST_CHECK(axis == Vector3(1, 0, 0));				//halfway0是个单位四元数，所有抽取出来一个默认的正x轴
	UBT_CHECK_EQUAL_FLOAT(angle, 0);
	Quaternion halfway1 = Quaternion::slerp(startQ, endQ, 0.75);
	halfway1.extractAxisAngle(axis, angle);
	BOOST_CHECK(axis == Vector3(0,0, 1));
	UBT_CHECK_EQUAL_FLOAT(angle, Constants::FOURTHPI);
	Quaternion halfway2 = Quaternion::slerp(startQ, endQ, 0.5);
	halfway2.extractAxisAngle(axis, angle);
	BOOST_CHECK(axis == Vector3(0, 0, 1));
	UBT_CHECK_EQUAL_FLOAT(angle, Constants::HALFPI);
	Quaternion halfway3 = Quaternion::slerp(startQ, endQ, 0.25);
	halfway3.extractAxisAngle(axis, angle);
	BOOST_CHECK(axis == Vector3(0, 0, 1));
	UBT_CHECK_EQUAL_FLOAT(angle, Constants::THREEQUARTERSPI);
	Quaternion halfway4 = Quaternion::slerp(startQ, endQ, 0.0);
	halfway4.extractAxisAngle(axis, angle);
	BOOST_CHECK(axis == Vector3(0, 0, 1));
	UBT_CHECK_EQUAL_FLOAT(angle, Constants::PI);

	//连接两个四元数(通过两个四元数相乘)
	//第一个四元数绕z轴旋转45度
	Quaternion qz1{};
	qz1.setRotateZD(45);
	//第二个四元数绕z轴旋转-45度
	Quaternion qz2{};
	qz2.setRotateZD(-45);
	//连接
	auto lq1 = qz1 * qz2;
	//对向量进行变换
	Vector3 vv1{ 1,1,0 };
	auto vvRet = lq1 * vv1;
	BOOST_CHECK(vvRet == vv1);

	//*=运算符
	qz2 *= qz1;
	vvRet = qz2 * vv1;
	BOOST_CHECK(vvRet == vv1);
}

BOOST_AUTO_TEST_SUITE_END()

#endif//SUITE_UML_QUATERNION