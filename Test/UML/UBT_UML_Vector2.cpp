//#define SUITE_UML_VEC2

#ifdef SUITE_UML_VEC2

#include "UBTConfig.h"

using namespace ung;

BOOST_AUTO_TEST_SUITE(suite_uml_vec2)

//构造函数
BOOST_AUTO_TEST_CASE(case_uml_vec2_1)
{
	Vector2 v1;
	UBT_CHECK_EQUAL_FLOAT(v1.x, 0.0);
	UBT_CHECK_EQUAL_FLOAT(v1.y, 0.0);

	Vector2 v2(1.0);
	BOOST_CHECK(v2 == Vector2(1.0, 1.0));

	Vector2 v3(1.0, 2.0);
	UBT_CHECK_EQUAL_VEC(v3, Vector2(1.0, 2.0));

	real_type ar[2]{ 3.0,2.0};
	Vector2 v4(ar, 2);
	UBT_CHECK_EQUAL_VEC(v4, Vector2(3.0, 2.0));

	Vector2 v5(ar);
	UBT_CHECK_EQUAL_VEC(v5, Vector2(3.0, 2.0));

	Vector2 v6(v5);
	UBT_CHECK_EQUAL_VEC(v6, Vector2(3.0, 2.0));

	Vector2 v7(std::move(v6));
	UBT_CHECK_EQUAL_VEC(v7, Vector2(3.0, 2.0));
}

//赋值运算符
BOOST_AUTO_TEST_CASE(case_uml_vec2_2)
{
	Vector2 v1(1.0, 2.0);
	Vector2 v2;
	v2 = v1;
	UBT_CHECK_EQUAL_VEC(v2, Vector2(1.0, 2.0));

	Vector2 v3;
	v3 = std::move(v1);
	UBT_CHECK_EQUAL_VEC(v3, Vector2(1.0, 2.0));
}

//重载运算符
BOOST_AUTO_TEST_CASE(case_uml_vec2_3)
{
	Vector2 v1(1.0, 2.0);

	Vector2 v2(2.0, 3.0);
	BOOST_CHECK(v1 < v2);

	UBT_CHECK_EQUAL_VEC(v2, Vector2(2.0, 3.0));

	v2 += v1;
	UBT_CHECK_EQUAL_VEC(v2, Vector2(3.0, 5.0));

	v2 += 1.0;
	UBT_CHECK_EQUAL_VEC(v2, Vector2(4.0, 6.0));

	v2 -= 1.0;
	UBT_CHECK_EQUAL_VEC(v2, Vector2(3.0, 5.0));

	v2 -= v1;
	UBT_CHECK_EQUAL_VEC(v2, Vector2(2.0, 3.0));

	v2 *= 2.0;
	UBT_CHECK_EQUAL_VEC(v2, Vector2(4.0, 6.0));

	v2 /= 2.0;
	UBT_CHECK_EQUAL_VEC(v2, Vector2(2.0, 3.0));

	UBT_CHECK_EQUAL_VEC(-v2, Vector2(-2.0, -3.0));
}

//其它函数
BOOST_AUTO_TEST_CASE(case_uml_vec2_4)
{
	Vector2 v1(1.0, 2.0);
	v1.setZero();
	UBT_CHECK_EQUAL_VEC(v1, Vector2(0.0, 0.0));

	Vector2 v2(1.0, 2.0);

	v1.swap(v2);
	UBT_CHECK_EQUAL_VEC(v1, Vector2(1.0, 2.0));
	UBT_CHECK_EQUAL_VEC(v2, Vector2(0.0, 0.0));

	UBT_CHECK_EQUAL_FLOAT(v1.length(), Math::calSqrt(1.0 * 1.0 + 2.0 * 2.0));

	UBT_CHECK_EQUAL_FLOAT(v1.squaredLength(), 1.0 * 1.0 + 2.0 * 2.0);

	Vector2 v3(2.0, 4.0);
	UBT_CHECK_EQUAL_FLOAT(v1.distance(v3), Math::calSqrt(1.0 * 1.0 + 2.0 * 2.0));

	UBT_CHECK_EQUAL_FLOAT(v1.squaredDistance(v3), 1.0 * 1.0 + 2.0 * 2.0);

	UBT_CHECK_EQUAL_FLOAT(v1.dotProduct(v3), 2.0 + 8.0);

	Vector2 v5(3.0, 4.0);
	auto v6 = v5.projectionComponent(Vector2(1.0, 0.0));
	UBT_CHECK_EQUAL_VEC(v6, Vector2(3.0, 0.0));

	auto v7 = v5.perpendicularComponent(Vector2(1.0, 0.0));
	auto v8 = v6 + v7;
	UBT_CHECK_EQUAL_VEC(v8, v5);

	auto v9 = v5.normalizeCopy();
	UBT_CHECK_EQUAL_FLOAT(v9.squaredLength(), 1.0);

	v5.normalize();
	UBT_CHECK_EQUAL_FLOAT(v5.squaredLength(), 1.0);
	
	Vector2 v10(2.0, 5.0);
	Vector2 v11(-2.0, -5.0);
	UBT_CHECK_EQUAL_VEC(v10.midPoint(v11), Vector2::ZERO);

	Vector2 v12(2.7, 5.1);
	Vector2 v13(2.8, 5.8);
	v12.setFloor(v13);
	UBT_CHECK_EQUAL_VEC(v12, Vector2(2.7, 5.1));

	v12.setCeil(v13);
	UBT_CHECK_EQUAL_VEC(v12, Vector2(2.8, 5.8));

	auto v14 = v12.perpendicular();
	auto dotRet = v12.dotProduct(v14);
	UBT_CHECK_EQUAL_FLOAT(dotRet, 0.0);

	Vector2 point1(1.0, 0.0);
	Vector2 point2(0.0, 2.0);
	Vector2 vIn = point2 - point1;
	Vector2 vN = Vector2(1.0, 2.0) - point1;
	Vector2 vOut = vIn.reflect(vN);
	UBT_CHECK_EQUAL_VEC(vOut, Vector2(1.0,2.0).normalizeCopy());

	Vector2 v17(5.0, 5.0);
	auto arcRet = v17.angleBetween(Vector2(1.0, 0.0));
	UBT_CHECK_EQUAL_FLOAT(arcRet, Constants::FOURTHPI);

	Vector2 v18(3.1, -4.8);
	auto v19 = v18.primaryAxis();
	UBT_CHECK_EQUAL_VEC(v19, Vector2(0.0, -1.0));
}

BOOST_AUTO_TEST_SUITE_END()

#endif//SUITE_UML_VEC2