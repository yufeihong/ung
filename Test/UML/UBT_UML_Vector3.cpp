//#define SUITE_UML_VEC3

#ifdef SUITE_UML_VEC3

#include "UBTConfig.h"

using namespace ung;
using namespace boost;

BOOST_AUTO_TEST_SUITE(suite_uml_vec3)

typedef mpl::list<float, double> types;

//构造函数
BOOST_AUTO_TEST_CASE_TEMPLATE(case_uml_vec3_1,T,types)
{
	Vec3<T> v1;
	UBT_CHECK_EQUAL_FLOAT(v1.x, 0.0);
	UBT_CHECK_EQUAL_FLOAT(v1.y, 0.0);
	UBT_CHECK_EQUAL_FLOAT(v1.z, 0.0);

	Vec3<T> v2(1.0);
	BOOST_CHECK(v2 == Vec3<T>(1.0,1.0,1.0));

	Vec3<T> v3(1.0, 2.0, 3.0);
	UBT_CHECK_EQUAL_VEC(v3,Vec3<T>(1.0,2.0,3.0));

	T ar[3]{ 3.0,2.0,1.0 };
	Vec3<T> v4(ar, 3);
	UBT_CHECK_EQUAL_VEC(v4,Vec3<T>(3.0,2.0,1.0));

	Vec3<T> v8(Vec2<T>(0.0, 1.0), 2.0);
	UBT_CHECK_EQUAL_VEC(v8,Vec3<T>(0.0,1.0,2.0));

	Vec3<T> v5(ar);
	UBT_CHECK_EQUAL_VEC(v5, Vec3<T>(3.0, 2.0, 1.0));

	Vec3<T> v6(v5);
	UBT_CHECK_EQUAL_VEC(v6, Vec3<T>(3.0, 2.0, 1.0));

	Vec3<T> v7(std::move(v6));
	UBT_CHECK_EQUAL_VEC(v7, Vec3<T>(3.0, 2.0, 1.0));

	auto v9{ UNG_NEW Vec3<T> };
	delete v9;
}

//赋值运算符
BOOST_AUTO_TEST_CASE_TEMPLATE(case_uml_vec3_2, T, types)
{
	Vec3<T> v1(1.0, 2.0, 3.0);
	Vec3<T> v2;
	v2 = v1;
	UBT_CHECK_EQUAL_VEC(v2, Vec3<T>(1.0, 2.0, 3.0));

	Vec3<T> v3;
	v3 = std::move(v1);
	UBT_CHECK_EQUAL_VEC(v3, Vec3<T>(1.0, 2.0, 3.0));
}

//重载运算符
BOOST_AUTO_TEST_CASE_TEMPLATE(case_uml_vec3_3, T, types)
{
	Vec3<T> v1{1.0,2.0,3.0};

	Vec3<T> v2(2.0, 3.0, 4.0);
	BOOST_CHECK(v1 < v2);

	UBT_CHECK_EQUAL_VEC(v2, Vec3<T>(2.0, 3.0, 4.0));

	v2 += v1;
	UBT_CHECK_EQUAL_VEC(v2, Vec3<T>(3.0, 5.0, 7.0));

	v2 += 1.0;
	UBT_CHECK_EQUAL_VEC(v2, Vec3<T>(4.0, 6.0, 8.0));

	v2 -= 1.0;
	UBT_CHECK_EQUAL_VEC(v2, Vec3<T>(3.0, 5.0, 7.0));

	v2 -= v1;
	UBT_CHECK_EQUAL_VEC(v2, Vec3<T>(2.0, 3.0, 4.0));

	v2 *= v1;
	UBT_CHECK_EQUAL_VEC(v2, Vec3<T>(2.0, 6.0, 12.0));

	v2 /= v1;
	UBT_CHECK_EQUAL_VEC(v2, Vec3<T>(2.0, 3.0, 4.0));

	v2 *= 2.0;
	UBT_CHECK_EQUAL_VEC(v2, Vec3<T>(4.0, 6.0, 8.0));

	v2 /= 2.0;
	UBT_CHECK_EQUAL_VEC(v2, Vec3<T>(2.0, 3.0, 4.0));

	UBT_CHECK_EQUAL_VEC(-v2, Vec3<T>(-2.0, -3.0, -4.0));

	//
	Mat3<T> m3{};
	auto vvv = v1 * m3;
}

//其它函数
BOOST_AUTO_TEST_CASE_TEMPLATE(case_uml_vec3_4, T, types)
{
	Vec3<T> v1(1.0, 2.0, 3.0);
	v1.setZero();
	UBT_CHECK_EQUAL_VEC(v1, Vec3<T>(0.0, 0.0, 0.0));

	Vec3<T> v2(1.0, 2.0, 3.0);

	v1.swap(v2);
	UBT_CHECK_EQUAL_VEC(v1, Vec3<T>(1.0, 2.0, 3.0));
	UBT_CHECK_EQUAL_VEC(v2, Vec3<T>(0.0, 0.0, 0.0));

	UBT_CHECK_EQUAL_FLOAT(v1.length(), MathImpl<T>::calSqrt(1.0 * 1.0 + 2.0 * 2.0 + 3.0 * 3.0));

	UBT_CHECK_EQUAL_FLOAT(v1.squaredLength(), 1.0 * 1.0 + 2.0 * 2.0 + 3.0 * 3.0);

	Vec3<T> v3(2.0, 4.0, 6.0);
	UBT_CHECK_EQUAL_FLOAT(v1.distance(v3), MathImpl<T>::calSqrt(1.0 * 1.0 + 2.0 * 2.0 + 3.0 * 3.0));

	UBT_CHECK_EQUAL_FLOAT(v1.squaredDistance(v3), 1.0 * 1.0 + 2.0 * 2.0 + 3.0 * 3.0);

	UBT_CHECK_EQUAL_FLOAT(v1.dotProduct(v3),2.0 + 8.0 + 18.0);

	//Vec3<T> v4(-2.0, -4.0, -6.0);
	//UBT_CHECK_EQUAL_FLOAT(v1.absDotProduct(v4), 28.0);

	Vec3<T> v5(3.0,4.0,5.0);
	auto v6 = v5.projectionComponent(Vec3<T>(1.0,0.0,0.0));
	UBT_CHECK_EQUAL_VEC(v6,Vec3<T>(3.0,0.0,0.0));

	auto v7 = v5.perpendicularComponent(Vec3<T>(1.0, 0.0, 0.0));
	auto v8 = v6 + v7;
	UBT_CHECK_EQUAL_VEC(v8, v5);

	auto v9 = v5.normalizeCopy();
	UBT_CHECK_EQUAL_FLOAT(v9.squaredLength(), 1.0);

	v5.normalize();
	UBT_CHECK_EQUAL_FLOAT(v5.squaredLength(), 1.0);

	Vec3<T> v10(2.0,5.0,9.0);
	Vec3<T> v11(-2.0,-5.0,-9.0);
	UBT_CHECK_EQUAL_VEC(v10.midPoint(v11),Vec3<T>::ZERO);

	Vec3<T> v12(2.7,5.1,9.4);
	Vec3<T> v13(2.8,5.8,9.8);
	v12.setFloor(v13);
	UBT_CHECK_EQUAL_VEC(v12,Vec3<T>(2.7,5.1,9.4));

	v12.setCeil(v13);
	UBT_CHECK_EQUAL_VEC(v12,Vec3<T>(2.8,5.8,9.8));

	auto v14 = v12.perpendicular();
	auto dotRet = v12.dotProduct(v14);
	UBT_CHECK_EQUAL_FLOAT(dotRet,0.0);

	T tolerance = std::numeric_limits<T>::epsilon();
	Vec3<T> v15(5.7,-3.1,6.4);
	auto a = v12.length();
	auto b = v15.length();

	auto v16 = v12.crossProductUnit(v15);
	BOOST_CHECK(v16.isUnitLength());

	dotRet = v16.dotProduct(v12);
	UBT_CHECK_EQUAL_FLOAT(dotRet,0.0);
	dotRet = v16.dotProduct(v15);
	UBT_CHECK_EQUAL_FLOAT(dotRet, 0.0);

	Vec3<T> point1(1.0, 0.0, 0.0);
	Vec3<T> point2(0.0, 2.0, 0.0);
	Vec3<T> vIn = point2 - point1;
	Vec3<T> vN = Vec3<T>(1.0,2.0,0.0) - point1;
	Vec3<T> vOut = vIn.reflect(vN);
	BOOST_CHECK(vOut.isParallel(Vec3<T>(1.0, 2.0, 0.0)));

	Vec3<T> v17(5.0,5.0,0.0);
	auto arcRet = v17.angleBetween(Vec3<T>(1.0,0.0,0.0));
	UBT_CHECK_EQUAL_FLOAT(arcRet,Consts<T>::FOURTHPI);

	Vec3<T> v18(3.1, -4.8, 2.9);
	auto v19 = v18.primaryAxis();
	UBT_CHECK_EQUAL_VEC(v19,Vec3<T>(0.0,-1.0,0.0));
}

BOOST_AUTO_TEST_SUITE_END()

#endif//SUITE_UML_VEC3