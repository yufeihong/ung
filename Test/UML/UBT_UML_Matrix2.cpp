//#define SUITE_UML_MATRIX2

#ifdef SUITE_UML_MATRIX2

#include "UBTConfig.h"

using namespace ung;

BOOST_AUTO_TEST_SUITE(suite_uml_matrix2)

BOOST_AUTO_TEST_CASE(case_uml_matrix2_1)
{
	Matrix2 m1(4,1,5,-3);
	auto d1 = m1.determinant();
	UBT_CHECK_EQUAL_FLOAT(d1, -17);

	Matrix2 m2{},m3{};
	m1.inverse(m2);
	m3 = Matrix2(0.176470588235294,0.058823529411765,
		0.294117647058823,-0.235294117647059);
	BOOST_CHECK(m2 == m3);
}

BOOST_AUTO_TEST_SUITE_END()

#endif//SUITE_UML_MATRIX2