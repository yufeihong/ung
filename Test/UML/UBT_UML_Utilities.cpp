//#define SUITE_UML_UTILITIES

#ifdef SUITE_UML_UTILITIES

#include "UBTConfig.h"

using namespace ung;

BOOST_AUTO_TEST_SUITE(suite_uml_utilities)

BOOST_AUTO_TEST_CASE(case_uml_utilities_1)
{
	//三个不相关向量确定的平行六面体的体积
	auto v12 = ungSignedVolumeOfParallelepiped(Vector3(-2, 5, 1), Vector3(4, 2, -2), Vector3(6, 0, -4));
	UBT_CHECK_EQUAL_FLOAT(v12, 24);

	//标量三重积
	auto v13 = ungScalarTripleProduct(Vector3(-2, 5, 1), Vector3(4, 2, -2), Vector3(6, 0, -4));
	UBT_CHECK_EQUAL_FLOAT(v13, 24);

	//求解线性方程组
	/*
	2x + 3y = -1
	7x + y = 6
	*/
	Matrix2 A(2,3,7,1);
	Vector2 B(-1,6);
	Vector2 S{};
	ungSolvingLinearEquations2(A, B, S);
	BOOST_CHECK(S == Vector2(1, -1));

	Matrix2 A1(1.500000000000000,-2.600000000000000,
		4.800000000000000,3.100000000000000);
	Vector2 B1(5,7);
	Vector2 S1{};
	ungSolvingLinearEquations2(A1, B1, S1);
	BOOST_CHECK(S1 == Vector2(1.967308814944542,-0.788091068301226));

	Matrix3 A2(3,1,-1,1,2,4,-1,4,5);
	Vector3 B2(3.6,2.1,-1.4);
	Vector3 S2{};
	ungSolvingLinearEquations3(A2, B2, S2);
	BOOST_CHECK(S2 == Vector3(1.481818181818182,-0.460606060606061,0.384848484848485));

	Matrix4 A3(2.1,3.5,4.2,1.8,9.2,8.4,7.1,2.5,6.8,3.0,2.7,1.4,5.2,4.3,6.8,9.0);
	Vector4 B3(3.5,6.2,8.9,2.7);
	Vector4 S3{};
	ungSolvingLinearEquations4(A3, B3, S3);
	BOOST_CHECK(S3 == Vector4(1.989701994519495,
		-4.887977746480105,
		4.843372231167892,
		-2.173675248175399));
}

BOOST_AUTO_TEST_SUITE_END()

#endif//SUITE_UML_UTILITIES