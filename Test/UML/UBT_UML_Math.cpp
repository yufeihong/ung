//#define SUITE_UML_MATH

#ifdef SUITE_UML_MATH

#include "UBTConfig.h"

using namespace ung;

BOOST_AUTO_TEST_SUITE(suite_uml_math)

BOOST_AUTO_TEST_CASE(case_uml_math_1)
{
	//平方根
	auto v1 = Math::calSqrt(73.6);
	UBT_CHECK_EQUAL_FLOAT(v1,8.5790442);

	//平方根的倒数
	auto v2 = Math::calInvSqrt(49.58);
	UBT_CHECK_EQUAL_FLOAT(v2, 0.1420190);

	//约束至一个范围
	auto v3 = Math::calClamp(49.2, 30.0, 49.0);
	UBT_CHECK_EQUAL_FLOAT(v3,49.0);

	//sin
	auto v4 = Math::calSin(0.820305);
	UBT_CHECK_EQUAL_FLOAT(v4, 0.731353);

	//cos
	auto v5 = Math::calCos(1.029744);
	UBT_CHECK_EQUAL_FLOAT(v5, 0.5150382);

	//tan
	auto v6 = Math::calTan(1.58825);
	UBT_CHECK_EQUAL_FLOAT(v6, Math::calSin(1.58825) / Math::calCos(1.58825));

	//cot
	auto v7 = Math::calCot(1.58825);
	UBT_CHECK_EQUAL_FLOAT(v7, Math::calCos(1.58825) / Math::calSin(1.58825));

	//arcSin
	auto v8 = Math::calASin(v4);
	UBT_CHECK_EQUAL_FLOAT(v8,0.820305);

	//arcCos
	auto v9 = Math::calACos(v5);
	UBT_CHECK_EQUAL_FLOAT(v9, 1.029744);

	//度转换为弧度
	auto v10 = Math::degreeToRadian(90.0);
	UBT_CHECK_EQUAL_FLOAT(v10, Constants::HALFPI);

	//弧度转换为度
	auto v11 = Math::radianToDegree(Constants::FOURTHPI);
	UBT_CHECK_EQUAL_FLOAT(v11,45.0);
}

BOOST_AUTO_TEST_SUITE_END()

#endif//SUITE_UML_MATH