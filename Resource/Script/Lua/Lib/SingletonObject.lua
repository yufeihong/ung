--[[
There's always only one instance of the object.
Many games use the singleton object for the resource management system.
当创建了一个对象A和对象B后，修改对象A的同时也修改了对象B
--]]

local instance

local SingletonObject = function()
	if not instance then
		local var1 = 'ipsum'

		instance = {
		var2 = 'sit',

		lorem = function()
			return 'lorem'
		end,

		ipsum = function(self)
			return var1 .. self.var2
		end}
	end

	return instance
end

return SingletonObject
