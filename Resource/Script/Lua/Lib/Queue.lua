--[[
队列
由于采用了双索引，所以操作均为常数时间

用法：
local Queue = require"Queue"

--创建了一个空队列
local q = Queue.queue()
--放置一些元素到队列中，注意：元素'Lorem'将位于队列的头部，元素'amet'将位于队列的尾部
for _, element in ipairs {'Lorem','ipsum','dolor','sit','amet'} do
  q.push(element)
end

--通过迭代器来遍历队列，注意：迭代器会弹出所有的元素，遍历完成后队列为空
for element in q.iterator() do
  -- each queue element will be printed onto screen
  print(element)
end
--]]

local Queue = {}

local function queue()
  local out = {}
  --[[
  使用两个indices来避免不必要的overhead，first指向第一个元素，last指向最后一个元素
  这种方式可以确保插入操作和移除操作都在常数时间内
  --]]
  local first,last = 0,-1

  out.push = function(item)
    last = last + 1
    out[last] = item
  end

  --移除当前位于队列头部的元素，并且返回被移除的元素
  out.pop = function()
    if first <= last then
      local value = out[first]
      out[first] = nil
      first = first + 1
      return value
    end
  end

  --[[
  迭代器会移除，并且返回元素
  创建了一个新的闭包，这个闭包会被重复执行，直到pop函数返回空
  --]]
  out.iterator = function()
    return function()
      return out.pop()
    end
  end

  --设置元表中的__len field来获取队列正确的长度
  setmetatable(out,{__len = function() return (last - first + 1) end})

  return out
end

Queue.queue = queue

return Queue
