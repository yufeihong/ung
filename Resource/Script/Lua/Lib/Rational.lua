--[[
对有理数的实现
用法：
local Rat = require"rational"

local r1 = Rat.makeRat(3,4)
local r2 = Rat.makeRatStr("1/2")
local f1 = Rat.toNumber(r1)
local f2 = Rat.toNumber(r2)
print(r1)
print(r2)
print(f1)
print(f2)

关于require:
Modules are used with the require function that registers them in the global table modules.loaded.This table
contains the compiled code of every module used and ensures that each module is loaded only once.
--]]

--[[
名字空间
In this case,the module uses locally defned variables and functions.Every function intended for external use is
put into one table.This common table is used as an interface with the outer world and is returned at the end of
the module.
--]]
Rat = {}

--Returns A and B’s greatest common divisor(最大公约数):
local function GetGcd(A, B)
	local Remainder = A % B
	return Remainder == 0 and B or GetGcd(B, Remainder)
end

--Metamethods:
local function Add(A, B)
	return MakeRat(A.Numer * B.Denom + B.Numer * A.Denom,A.Denom * B.Denom)
end

local function Sub(A, B)
	return A + -B
end

local function Mul(A, B)
	return MakeRat(A.Numer * B.Numer, A.Denom * B.Denom)
end

local function Div(A, B)
	assert(B.Numer ~= 0,'Divison by zero')
	return A * MakeRat(B.Denom, B.Numer)
end

--添加负号
local function Unm(A)
	return MakeRat(-A.Numer, A.Denom)
end

--==
local function Eq(A, B)
--This and Lt work because MakeRat always makes sure the denominator is positive and reduces to lowest terms:
	return A.Numer == B.Numer and A.Denom == B.Denom
end

--<
local function Lt(A, B)
	local Diff = A - B
	return Diff.Numer < 0
end

local function ToString(R)
	return R.Numer .. '/' .. R.Denom
end

--持有元方法的元表
local RatMeta = {
__add = Add,
__sub = Sub,
__mul = Mul,
__div = Div,
__unm = Unm,
__eq = Eq,
__lt = Lt,
__tostring = ToString,
}

--该库所提供的三个接口方法:

--[[
从给定分子和分母来实例化一个有理数:
用法：local r = Rat.makeRat(3,4) print(r)
--]]
function Rat.makeRat(Numer, Denom)
	Numer, Denom = tonumber(Numer), tonumber(Denom)
	assert(Denom ~= 0, 'Denominator must be nonzero')
	assert(Numer == math.floor(Numer) and Denom == math.floor(Denom),'Numerator and denominator must be integers')

	--确保分母为正数:
	if Denom < 0 then
		Numer, Denom = -Numer, -Denom
	end

	--Reduce the fraction to its lowest terms:
	local Gcd = GetGcd(Numer, Denom)
	local R = {
	Numer = Numer / Gcd,
	Denom = Denom / Gcd}
	--设置Rat的元表，然后返回Rat
	return setmetatable(R,RatMeta)
end

--[[
从一个字符串来实例化一个有理数
用法：local r = Rat.makeRatStr("1/2") print(r)
--]]
function Rat.makeRatStr(Str)
	local Numer,Denom = string.match(Str,'^(%-?%d+)%/(%-?%d+)$')
	assert(Numer,'Couldn’t parse rational number')

	return Rat.makeRat(Numer, Denom)
end

--[[
转化为一个浮点数
用法：Rat.toNumber(r)
--]]
function Rat.toNumber(R)
	return R.Numer / R.Denom
end

return Rat
