--[[
队列中的每个元素必须是unique的
如果有两个元素的priority值相等，那么其在队列中的先后顺序是未定义的
暂未实现长度运算符#

常数时间：
获取最小或最大priority的元素

线性时间(最差情况下)：
插入，移除，更新元素priority

用法：
local PQueue = require"PriorityQueue"

local pq = PQueue.makePQueue()

local elements = {
	['Lorem'] = 20,
	['ipsum'] = 100,
	['dolor'] = 80,
	['sit'] = 30,
	['amet'] = 20,
}

--把一些元素放入到优先队列中
for element,priority in pairs(elements) do
	--这种方式可以用于更新元素的priority值
	pq[element] = priority
end

判断是否为空
priority_queue.empty()

--移除元素
pq.remove('Lorem')

--遍历所有的元素，从priority值最高的元素开始
while not pq.empty() do
	local element = pq.max()
	pq.remove(element)
	print(element)
end

获取最小或最大priority值的元素
local min_element = pq.min()
local max_element = pq.max()
--]]

local PriorityQueue = {}

--让ti和tr分别引用全局函数table.insert和table.remove
local ti = table.insert
local tr = table.remove

--从table中移除给定值的元素
local tr2 = function(t, v)
	for i = 1,#t do
		if t[i] == v then
			tr(t, i)
			break
		end
	end
end

local function pqueue()
	--interface table
	local t = {}

	--其中的entry为元素和其priority的pair，也用于获取元素的priority值
	local set = {}
	--这个容器中包含priority bags，每一个bag代表一个priority level。
	local r_set = {}
	--sorted list of priorities，用于从一个最大或最小priority bag来获取元素。
	local keys = {}

	--[[
	add element into storage, set its priority and sort keys
	e:element
	p:priority
	--]]
	local function addKV(e,p)
		--以元素为set table的key，priority值为值
		set[e] = p
		--create a new list for this priority
		if not r_set[p] then
			ti(keys,p)
			table.sort(keys)
			local k0 = {e}
			r_set[p] = k0

			setmetatable(k0,{__mode = 'v'})

		--add element into list for this priority
		else
			ti(r_set[p], e)
		end
	end

	--remove element from storage and sort keys
	local remove = function(k)
		local v = set[k]
		local prioritySet = r_set[v]
		tr2(prioritySet,k)
		if #prioritySet < 1 then
			tr2(keys, v)
			r_set[v] = nil
			table.sort(keys)
			set[k] = nil
		end
	end

	t.remove = remove

	--returns an element with the lowest priority
	t.min = function()
		local priority = keys[1]
		if priority then
			return r_set[priority][1] or {}
		else
			return {}
		end
	end

	--returns an element with the highest priority
	t.max = function()
		local priority = keys[#keys]
		if priority then
			return r_set[priority][1] or {}
		else
			return {}
		end
	end

	--is this queue empty?
	t.empty = function()
		return #keys < 1
	end

	--simple element iterator, retrieves elements with
	--the highest priority first
	t.iterate = function()
		return function()
			if not t.empty() then
				local element = t.max()
				t.remove(element)
				return element
			end
		end
	end

	--setup pqueue behavior
	setmetatable(t,{__index = set,__newindex = function(t, k, v)
		if not set[k] then
			--new item
			addKV(k, v)
		else
			--existing item
			remove(k)
			addKV(k, v)
		end
	end})

	return t
end

PriorityQueue.makePQueue = pqueue

return PriorityQueue
