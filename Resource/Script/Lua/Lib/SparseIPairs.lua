--[[
对ipairs的一个扩展，用于遍历稀疏数组
默认的ipairs函数要求数组的整数索引必须是连续的
ipairs_sparse从table的keys中构造了一系列新的索引，然后根据索引进行了排序，最后返回一个闭包，每次对闭包的调用都从
稀疏数组中返回了一个连续的索引和值的对

用法：
local SparseIPairs = require"SparseIPairs"
--table below contains unsorted sparse array
local t = {[10] = 'a', [2] = 'b', [5] = 'c', [100] = 'd', [99] = 'e'}

--每次对ipairs_sparse的调用都生成了一个叫做index的东西，其包含了一个整数和元素值的对
for i,v in SparseIPairs.ipairs(t) do
	print(i,v)
end
--]]

local SparseIPairs = {}

local function ipairs_sparse(t)
	--tmpIndex will hold sorted indices, otherwise this iterator would be no different from pairs iterator
	local tmpIndex = {}
	local index, _ = next(t)

	while index do
		tmpIndex[#tmpIndex+1] = index
		index, _ = next(t, index)
	end

	--sort table indices
	table.sort(tmpIndex)
	local j = 1
	return function()
		--get index value
		local i = tmpIndex[j]
		j = j + 1

		if i then
			return i, t[i]
		end
	end
end

SparseIPairs.ipairs = ipairs_sparse

return SparseIPairs
