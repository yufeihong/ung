--[[
A module in the form of an object.
This type of module is used less often,but it's useful if you need multiple instances of the same object.
This module type doesn't manipulate the global namespace. Every object you create uses its own local namespace.

用法：
local ObjectMaker = require"ObjectMaker"

----Create two instances of this module
obj1 = ObjectMaker()
obj2 = ObjectMaker()

print(obj1.var1)				--nil
print(obj1.var2)				--sit
print(obj1:lorem())				--lorem
print(obj1:ipsum())				--ipsumsit

print(obj1 ~= obj2)				--true
--]]

local ObjectMaker = function()
	local instance

	--[[
	生成的对象具有一个private成员变量
	Variable var1 is always hidden from the outside world and can be changed only by the exposed function.
	--]]
	local var1 = 'ipsum'

	instance = {
	--[[
	生成的对象具有一个public成员变量
	Variable var2 is freely accessible and can be modifed anytime.
	--]]
	var2 = 'sit',

	--生成的对象具有两个方法
	lorem = function()
		return 'lorem'
	end,

	ipsum = function(self)
		return var1 .. self.var2
	end}
	return instance
end

--Return a local variable,which contains an object interface.
return ObjectMaker
