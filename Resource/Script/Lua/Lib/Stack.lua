--[[
栈
采用闭包(closure)的方式来实现
操作都是在常数时间

用法：
local Stack = require"Stack"
--注意：因为是采用闭包的方式来实现的，所以这里后面的()是必须的
local s = Stack.stack()
--往栈中放入一些数据(其中'Lorem'将位于栈底部，'amet'将位于栈顶部)
for _,element in ipairs {'Lorem','ipsum','dolor','sit','amet'} do
  s.push(element)
end

--迭代器函数可以用来弹出元素，迭代一遍结束后，栈中的所有元素都被弹出了，栈为空
for element in s1.iterator() do
     print(element)
end
--]]

--table中的元素为被整数索引，并且没有holes
local Stack = {}

local function stack()
  local out = {}

  --push闭包
  out.push = function(item)
    out[#out+1] = item						--长度操作符，返回最后一个元素的整数索引值
  end

  --pop闭包
  out.pop = function()
    if #out>0 then
      return table.remove(out, #out)		--pop的意思就是把元素从table中给直接移除掉了，但是返回了被移除的元素
    end
  end

  --迭代器闭包
  out.iterator = function()
    return function()
      return out.pop()						--注意：这里把元素直接弹出了
    end
  end

  --返回一个空table，而这个table具有3个函数
  return out
end

Stack.stack = stack

return Stack
