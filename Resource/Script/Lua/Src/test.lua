local ObjectMaker = require"ObjectMaker"

obj1 = ObjectMaker()
obj2 = ObjectMaker()

print(obj1.var1)				--nil
print(obj1.var2)				--sit
print(obj1:lorem())				--lorem
print(obj1:ipsum())				--ipsumsit

print(obj1 ~= obj2)				--true
