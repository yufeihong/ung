
matrix view_proj_mat;

struct Input
{
	//语义指定了数据与特定寄存器之间的联系
	float3 position_local : POSITION;
};

struct Output
{
	//4D向量
	vector position_hcs : POSITION;
};

Output mainVS(Input input)
{
	//zero out all members
	Output output = (Output)0;
	
	float4 p = float4(input.position_local, 1.0);
	
	output.position_hcs = mul(p,view_proj_mat);
	
	return output;
}