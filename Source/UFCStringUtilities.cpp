#include "UFCStringUtilities.h"

#ifdef UFC_HIERARCHICAL_COMPILE

#include "boost/algorithm/string.hpp"
#include "boost/lexical_cast.hpp"
#include <iostream>
#ifndef WIN32_LEAN_AND_MEAN
#if UNG_PLATFORM == UNG_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#if !defined(NOMINMAX) && defined(_MSC_VER)
#define NOMINMAX																					//required to stop windows.h messing up std::min
#endif
#include <windows.h>
#endif
#endif
#include <stringapiset.h>

using namespace boost;
using namespace boost::algorithm::detail;

namespace ung
{
	void StringUtilities::toUpper(String& s)
	{
		to_upper(s);
	}

	String StringUtilities::toUpperCopy(const String& s)
	{
		return to_upper_copy(s);
	}

	void StringUtilities::toLower(String& s)
	{
		to_lower(s);
	}

	String StringUtilities::toLowerCopy(const String& s)
	{
		return to_lower_copy(s);
	}

	void StringUtilities::trimLeft(String& s)
	{
		trim_left(s);
	}

	void StringUtilities::trimRight(String& s)
	{
		trim_right(s);
	}

	void StringUtilities::trimLeftIf(String& s, uint16 mask)
	{
		is_classifiedF IsSpace(mask);
		trim_left_if(s, IsSpace);
	}

	void StringUtilities::trimRightIf(String& s, uint16 mask)
	{
		is_classifiedF IsSpace(mask);
		trim_right_if(s, IsSpace);
	}

	String StringUtilities::trimLeftCopy(const String& s)
	{
		return trim_left_copy(s);
	}

	String StringUtilities::trimRightCopy(const String& s)
	{
		return trim_right_copy(s);
	}

	String StringUtilities::trimLeftCopyIf(const String& s, uint16 mask)
	{
		is_classifiedF IsSpace(mask);
		return trim_left_copy_if(s, IsSpace);
	}

	String StringUtilities::trimRightCopyIf(const String& s, uint16 mask)
	{
		is_classifiedF IsSpace(mask);
		return trim_right_copy_if(s, IsSpace);
	}

	void StringUtilities::trimBoth(String& s)
	{
		trim(s);
	}

	void StringUtilities::trimBothIf(String& s, uint16 mask)
	{
		is_classifiedF IsSpace(mask);
		trim_if(s, IsSpace);
	}

	String StringUtilities::trimBothCopy(const String& s)
	{
		return trim_copy(s);
	}

	String StringUtilities::trimBothCopyIf(const String& s, uint16 mask)
	{
		is_classifiedF IsSpace(mask);
		return trim_copy_if(s, IsSpace);
	}

	bool StringUtilities::startWithSensitive(const String& s, const String& with)
	{
		return starts_with(s, with);
	}

	bool StringUtilities::endWithSensitive(const String& s, const String& with)
	{
		return ends_with(s, with);
	}

	bool StringUtilities::startWithInSensitive(const String& s, const String& with)
	{
		return istarts_with(s, with);
	}

	bool StringUtilities::endWithInSensitive(const String& s, const String& with)
	{
		return iends_with(s, with);
	}

	bool StringUtilities::containSensitive(const String& s, const String& con)
	{
		return contains(s, con);
	}

	bool StringUtilities::containInSensitive(const String& s, const String& con)
	{
		return icontains(s, con);
	}

	bool StringUtilities::equalSensitive(const String& s1, const String& s2)
	{
		return equals(s1, s2);
	}

	bool StringUtilities::equalInSensitive(const String& s1, const String& s2)
	{
		return iequals(s1, s2);
	}

	bool StringUtilities::lessSensitive(const String& s1, const String& s2)
	{
		return lexicographical_compare(s1, s2);
	}

	bool StringUtilities::lessInSensitive(const String& s1, const String& s2)
	{
		return ilexicographical_compare(s1, s2);
	}

	bool StringUtilities::allCheck(const String& s, uint16 pre)
	{
		is_classifiedF IsSpace(pre);
		return all(s, IsSpace);
	}

	uint32 StringUtilities::findLeftSensitive(String& s, const String& f)
	{
		iterator_range<String::iterator> range = find_first(s, f);
		return range.begin() - s.begin();
	}

	uint32 StringUtilities::findLeftInSensitive(String& s, const String& f)
	{
		iterator_range<String::iterator> range = ifind_first(s, f);
#if UNG_DEBUGMODE
		uint32 pos = range.begin() - s.begin();
#endif

		return range.begin() - s.begin();
	}

	uint32 StringUtilities::findRightSensitive(String& s, const String& f)
	{
		iterator_range<String::iterator> range = find_last(s, f);
		return range.begin() - s.begin();
	}

	uint32 StringUtilities::findRightInSensitive(String& s, const String& f)
	{
		iterator_range<String::iterator> range = ifind_last(s, f);
		return range.begin() - s.begin();
	}

	uint32 StringUtilities::findNthSensitive(String& s, const String& f,uint8 n)
	{
		iterator_range<String::iterator> range = find_nth(s, f,n);
		return range.begin() - s.begin();
	}

	uint32 StringUtilities::findNthInSensitive(String& s, const String& f,uint8 n)
	{
		iterator_range<String::iterator> range = ifind_nth(s, f,n);
		return range.begin() - s.begin();
	}

	String StringUtilities::getLeftN(String& s, uint8 n)
	{
		iterator_range<String::iterator> range = find_head(s, n);
		return String(range.begin(), range.end());
	}

	String StringUtilities::getRightN(String& s, uint8 n)
	{
		iterator_range<String::iterator> range = find_tail(s, n);
		return String(range.begin(), range.end());
	}

	String StringUtilities::getFirstMatchToken(String& s, IsType type)
	{
		is_classifiedF IsSpace(type);
		iterator_range<String::iterator> range = find_token(s, IsSpace);
		return String(range.begin(), range.end());
	}

	void StringUtilities::replaceLeftSensitive(String& s, const String& search, const String& replace)
	{
		replace_first(s, search, replace);
	}

	String StringUtilities::replaceLeftCopySensitive(String& s, const String& search, const String& replace)
	{
		return replace_first_copy(s, search, replace);
	}

	void StringUtilities::replaceLeftInSensitive(String& s, const String& search, const String& replace)
	{
		ireplace_first(s, search, replace);
	}

	String StringUtilities::replaceLeftCopyInSensitive(String& s, const String& search, const String& replace)
	{
		return ireplace_first_copy(s, search, replace);
	}

	void StringUtilities::eraseLeftSensitive(String& s, const String& search)
	{
		erase_first(s, search);
	}

	String StringUtilities::eraseLeftCopySensitive(String& s, const String& search)
	{
		return erase_first_copy(s, search);
	}

	void StringUtilities::eraseLeftInSensitive(String& s, const String& search)
	{
		ierase_first(s, search);
	}

	String StringUtilities::eraseLeftCopyInSensitive(String& s, const String& search)
	{
		return ierase_first_copy(s, search);
	}

	void StringUtilities::replaceRightSensitive(String& s, const String& search, const String& replace)
	{
		replace_last(s, search, replace);
	}

	String StringUtilities::replaceRightCopySensitive(String& s, const String& search, const String& replace)
	{
		return replace_last_copy(s, search, replace);
	}

	void StringUtilities::replaceRightInSensitive(String& s, const String& search, const String& replace)
	{
		ireplace_last(s, search, replace);
	}

	String StringUtilities::replaceRightCopyInSensitive(String& s, const String& search, const String& replace)
	{
		return ireplace_last_copy(s, search, replace);
	}

	void StringUtilities::eraseRightSensitive(String& s, const String& search)
	{
		erase_last(s, search);
	}

	String StringUtilities::eraseRightCopySensitive(String& s, const String& search)
	{
		return erase_last_copy(s, search);
	}

	void StringUtilities::eraseRightInSensitive(String& s, const String& search)
	{
		ierase_last(s, search);
	}

	String StringUtilities::eraseRightCopyInSensitive(String& s, const String& search)
	{
		return ierase_last_copy(s, search);
	}

	void StringUtilities::replaceNthSensitive(String& s, const String& search, uint8 n, const String& replace)
	{
		replace_nth(s, search, n, replace);
	}

	String StringUtilities::replaceNthCopySensitive(String& s, const String& search, uint8 n, const String& replace)
	{
		return replace_nth_copy(s, search, n, replace);
	}

	void StringUtilities::replaceNthInSensitive(String& s, const String& search, uint8 n, const String& replace)
	{
		ireplace_nth(s, search, n, replace);
	}

	String StringUtilities::replaceNthCopyInSensitive(String& s, const String& search, uint8 n, const String& replace)
	{
		return ireplace_nth_copy(s, search, n, replace);
	}

	void StringUtilities::eraseNthSensitive(String& s, const String& search, uint8 n)
	{
		erase_nth(s, search, n);
	}

	String StringUtilities::eraseNthCopySensitive(String& s, const String& search, uint8 n)
	{
		return erase_nth_copy(s, search, n);
	}

	void StringUtilities::eraseNthInSensitive(String& s, const String& search, uint8 n)
	{
		ierase_nth(s, search, n);
	}

	String StringUtilities::eraseNthCopyInSensitive(String& s, const String& search, uint8 n)
	{
		return ierase_nth_copy(s, search, n);
	}

	void StringUtilities::replaceAllSensitive(String& s, const String& search,const String& replace)
	{
		replace_all(s, search, replace);
	}

	String StringUtilities::replaceAllCopySensitive(String& s, const String& search, const String& replace)
	{
		return replace_all_copy(s, search, replace);
	}

	void StringUtilities::replaceAllInSensitive(String& s, const String& search,const String& replace)
	{
		ireplace_all(s, search, replace);
	}

	String StringUtilities::replaceAllCopyInSensitive(String& s, const String& search,const String& replace)
	{
		return ireplace_all_copy(s, search, replace);
	}

	void StringUtilities::eraseAllSensitive(String& s, const String& search)
	{
		erase_all(s, search);
	}

	String StringUtilities::eraseAllCopySensitive(String& s, const String& search)
	{
		return erase_all_copy(s, search);
	}

	void StringUtilities::eraseAllInSensitive(String& s, const String& search)
	{
		ierase_all(s, search);
	}

	String StringUtilities::eraseAllCopyInSensitive(String& s, const String& search)
	{
		return ierase_all_copy(s, search);
	}

	void StringUtilities::replaceLeftN(String& s, uint8 n, const String& replace)
	{
		replace_head(s, n, replace);
	}

	String StringUtilities::replaceLeftNCopy(String& s, uint8 n, const String& replace)
	{
		return replace_head_copy(s, n, replace);
	}

	void StringUtilities::eraseLeftN(String& s, uint8 n)
	{
		erase_head(s, n);
	}

	String StringUtilities::eraseLeftNCopy(String& s, uint8 n)
	{
		return erase_head_copy(s, n);
	}

	void StringUtilities::replaceRightN(String& s, uint8 n, const String& replace)
	{
		replace_tail(s, n, replace);
	}

	String StringUtilities::replaceRightNCopy(String& s, uint8 n, const String& replace)
	{
		return replace_tail_copy(s, n, replace);
	}

	void StringUtilities::eraseRightN(String& s, uint8 n)
	{
		erase_tail(s, n);
	}

	String StringUtilities::eraseRightNCopy(String& s, uint8 n)
	{
		return erase_tail_copy(s, n);
	}

	void StringUtilities::splitToList(STL_LIST(String) &result, String &input, const char *pred)
	{
		boost::split(result, input, boost::is_any_of(pred), boost::algorithm::token_compress_mode_type::token_compress_on);
	}

	void StringUtilities::splitToVector(STL_VECTOR(String) &result, String &input, const char *pred)
	{
		boost::split(result, input, boost::is_any_of(pred), boost::algorithm::token_compress_mode_type::token_compress_on);
	}

	void StringUtilities::splitToDeque(STL_DEQUE(String) &result, String &input, const char *pred)
	{
		boost::split(result, input, boost::is_any_of(pred), boost::algorithm::token_compress_mode_type::token_compress_on);
	}

	String StringUtilities::joinFromList(STL_LIST(String) &source, const char *separator)
	{
		return join(source, separator);
	}

	String StringUtilities::joinFromVector(STL_VECTOR(String) &source, const char *separator)
	{
		return join(source, separator);
	}

	String StringUtilities::joinFromDeque(STL_DEQUE(String) &source, const char *separator)
	{
		return join(source, separator);
	}

	//-------------------------------------------------------------------------------------------------------------------------------------------------

	int32 LexicalCast::stringToInt(const String& s)
	{
		try
		{
			return lexical_cast<int>(s);
		}
		catch (bad_lexical_cast& e)
		{
			std::cerr << "Lexical cast from string to int failed:" << e.what() << std::endl;
			throw(e);
		}
	}

	float LexicalCast::stringToFloat(const String& s)
	{
		try
		{
			return lexical_cast<float>(s);
		}
		catch (bad_lexical_cast& e)
		{
			std::cerr << "Lexical cast from string to float failed:" << e.what() << std::endl;
			throw(e);
		}
	}

	double LexicalCast::stringToDouble(const String& s)
	{
		try
		{
			return lexical_cast<double>(s);
		}
		catch (bad_lexical_cast& e)
		{
			std::cerr << "Lexical cast from string to double failed:" << e.what() << std::endl;
			throw(e);
		}
	}

	String LexicalCast::intToString(const int32 i)
	{
		try
		{
			return lexical_cast<String>(i);
		}
		catch (bad_lexical_cast& e)
		{
			std::cerr << "Lexical cast from int to string failed:" << e.what() << std::endl;
			throw(e);
		}
	}

	String LexicalCast::doubleToString(const double r)
	{
		try
		{
			return lexical_cast<String>(r);
		}
		catch (bad_lexical_cast& e)
		{
			std::cerr << "Lexical cast from double to string failed:" << e.what() << std::endl;
			throw(e);
		}
	}

	bool LexicalCast::convert(const char* source, wchar_t*& sink)
	{
		if (!source || sink)
		{
			return false;
		}

		//判断系统是否已经安装了代码页936
		CPINFOEX ci;
		if (GetCPInfoEx(CP_ACP, 0, &ci))
		{
			/*
			获取转换后的字符数
			CP_ACP表示系统默认的ANSI字符串代码页。
			对于简体中文操作系统而言，CP_ACP表示代码页936即GBK。
			*/
			auto len = MultiByteToWideChar(CP_ACP, 0, source, -1, NULL, 0);
			auto pW = new wchar_t[len];
			ZeroMemory(pW, len * sizeof(pW[0]));
			MultiByteToWideChar(CP_ACP, 0, source, -1, pW, len);

			sink = pW;

			return true;
		}

		return false;
	}

	bool LexicalCast::convert(const wchar_t * source, char *& sink)
	{
		if (!source || sink)
		{
			return false;
		}

		CPINFOEX ci;
		if (GetCPInfoEx(CP_ACP, 0, &ci))
		{
			auto len = WideCharToMultiByte(CP_ACP, 0, source, -1, NULL, 0, NULL, NULL);
			auto pA = new char[len];
			ZeroMemory(pA, len * sizeof(pA[0]));
			WideCharToMultiByte(CP_ACP, 0, source, -1, pA, len, NULL, NULL);

			sink = pA;

			return true;
		}

		return false;
	}

	bool LexicalCast::convert(String const & source, StringW & sink)
	{
		if (source.empty())
		{
			return false;
		}

		auto len = source.length();
		sink.resize(len, L' ');

		CPINFOEX ci;
		if (GetCPInfoEx(CP_ACP, 0, &ci))
		{
			MultiByteToWideChar(CP_ACP, 0, (const char*)source.c_str(), len, (wchar_t*)sink.c_str(), len);

			return true;
		}

		return false;
	}

	bool LexicalCast::convert(StringW const & source, String & sink)
	{
		if (source.empty())
		{
			return false;
		}

		auto pW = source.data();
		char* pA = nullptr;
		auto ret = convert(pW, pA);

		if (ret)
		{
			String temp(pA);
			sink = std::move(temp);

			delete[] pA;

			return true;
		}

		return false;
	}

	bool LexicalCast::convert(const char * source, StringW & sink)
	{
		wchar_t* pW = nullptr;

		auto ret = convert(source, pW);

		if (ret)
		{
			StringW temp(pW);

			sink = std::move(temp);

			delete[] pW;

			return true;
		}

		return false;
	}

	bool LexicalCast::convert(StringW const & source, char *& sink)
	{
		if (source.empty() || sink)
		{
			return false;
		}

		auto pW = source.data();

		auto ret = convert(pW,sink);
	
		if (ret)
		{
			return true;
		}

		return false;
	}

	bool LexicalCast::convert(const wchar_t * source, String & sink)
	{
		if (!source)
		{
			return false;
		}

		char* pA = nullptr;

		auto ret = convert(source, pA);

		if (ret)
		{
			String temp(pA);
			sink = std::move(temp);

			delete[] pA;

			return true;
		}

		return false;
	}

	bool LexicalCast::convert(String const & source, wchar_t *& sink)
	{
		if (source.empty() || sink)
		{
			return false;
		}

		auto pA = source.data();

		auto ret = convert(pA, sink);

		if (ret)
		{
			return true;
		}

		return false;
	}
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

/*
lexical:词汇.
lexical_cast库进行"字面量"的转换,可以进行字符串,整数/浮点数之间的字面转换.
*/