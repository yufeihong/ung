#include "UMMAllocator.h"

#ifdef UMM_HIERARCHICAL_COMPILE

#include <malloc.h>
#include "UMMBigDog.h"

namespace ung
{
	/*
	ascii:
	0---48
	9---57
	A---65
	a---97
	*/
	void AllocatorHelper::checkAddress(void* address)
	{
		BOOST_ASSERT(((unsigned long long)address % UMM_BYTESALIGNED ) == 0);
	}

	void* AllocatorHelper::mallocAligned(size_type bytes)
	{
		BOOST_ASSERT((bytes % UMM_BYTESALIGNED) == 0);

		auto result = _aligned_malloc(bytes,UMM_BYTESALIGNED);

#if UNG_DEBUGMODE
		if (result)
		{
			AllocatorHelper::checkAddress(result);
		}
#endif

		return result;
	}

	void AllocatorHelper::freeAligned(void* memblock)
	{
#if UNG_DEBUGMODE
		if (memblock)
		{
			AllocatorHelper::checkAddress(memblock);
		}
#endif

		_aligned_free(memblock);
	}

	AllocatorHelper::size_type AllocatorHelper::roundUp(size_type bytes)
	{
		auto alignedBytes = (bytes + UMM_BYTESALIGNED - 1) & ~(UMM_BYTESALIGNED - 1);

		return alignedBytes;
	}

	//---------------------------------------------------------------------------------

	//static，这里是其定义
	void(*MallocAllocator::oom_malloc_handler)();

	void* MallocAllocator::allocate(size_type bytes)
	{
		//字节对齐
		bytes = AllocatorHelper::roundUp(bytes);

		//从内存条分配
		auto result = AllocatorHelper::mallocAligned(bytes);

		//如果已经无法从内存条获取到参数所要求的空间
		if (!result)
		{
			//那么启动out-of-memory handler机制
			result = oom_malloc(bytes);
			/*
			这里，但凡能够返回一个地址给result，就说明out-of-memory机制成功了，否则，就会抛出异常
			或者陷入死循环了。
			*/
			BOOST_ASSERT(result);
		}

#if UNG_DEBUGMODE
		//如果成功了，再做记录
		if (result)
		{
			BigDog::getInstance().addLevelFirstMallocTimes();
			BigDog::getInstance().addLevelFirstHeapSize(bytes,true);
		}
#endif

		return result;
	}

	void MallocAllocator::deallocate(void* p, size_type bytes)
	{
		//字节对齐
		bytes = AllocatorHelper::roundUp(bytes);

		//释放至内存条
		AllocatorHelper::freeAligned(p);

#if UNG_DEBUGMODE
		BigDog::getInstance().addLevelFirstHeapSize(bytes,false);
#endif
	}

	void (*MallocAllocator::set_malloc_handler(void(*f)()))()
	{
		//保存old内存不足的处理例程
		void(*old)() = oom_malloc_handler;

		//设置新的内存不足处理例程
		oom_malloc_handler = f;

		//返回old内存不足的处理例程
		return old;
	}

	void* MallocAllocator::oom_malloc(size_type bytes)
	{
		//定义一个函数指针
		void(*my_malloc_handler)();
		void* result{nullptr};

		//给函数指针赋值
		my_malloc_handler = oom_malloc_handler;

		//如果“内存不足处理例程”并未被客端设定，那么就抛出异常信息，或利用exit(1)硬生生中止程序
		if (my_malloc_handler == nullptr)
		{
			throw std::bad_alloc();
		}

		//不断尝试释放，配置，再释放，再配置...
		for (;;)
		{
			//调用处理例程，企图释放内存(调用客户set的用于处理内存不足的例程)
			(*my_malloc_handler)();

			//调用处理例程后，再次尝试从内存条获取参数所要求的空间
			result = AllocatorHelper::mallocAligned(bytes);

			//如果调用处理例程后，从内存条获取空间成功了
			if (result)
			{
#if UNG_DEBUGMODE
				BigDog::getInstance().addLevelFirstMallocTimes();
				BigDog::getInstance().addLevelFirstHeapSize(bytes,true);
#endif

				return result;
			}

			//如果没有成功，那么继续循环。（其实走到这里应该已经山穷水尽了）

#if UNG_DEBUGMODE
			//当第一次内存不足处理例程被调用后，依然无法从内存条中获取到参数所要求的空间时，仅做一次report。
			static auto report_once{ false };
			if (!report_once)
			{
				std::cout << std::endl;
				std::cout << "!!!!!!!!!!---!!!!!!!!!!---!!!!!!!!!!" << std::endl;
				std::cout << "内存已经耗尽！陷入了死循环！" << std::endl;
				std::cout << "!!!!!!!!!!---!!!!!!!!!!---!!!!!!!!!!" << std::endl;
				std::cout << std::endl;

				report_once = true;
			}
#endif
		}
	}

	//-------------------------------------------------------------------------------------------------------------------------

	//起始时，内存池没有分配空间
	char* PoolAllocator::sStart = nullptr;
	char* PoolAllocator::sEnd = nullptr;
	typename PoolAllocator::size_type PoolAllocator::sHeapSize{};
	typename PoolAllocator::obj_type* volatile PoolAllocator::sFreeLists[UMM_FREELISTS_COUNT]{};

	void* PoolAllocator::allocate(size_type& bytes)
	{
		BOOST_ASSERT(bytes > 0);

		//对齐
		bytes = AllocatorHelper::roundUp(bytes);

		//如果区块大于UMM_LEVEL_TWO_MAX_BYTES bytes就调用第一级配置器
		if (bytes > UMM_LEVEL_TWO_MAX_BYTES)
		{
			auto mallocAllocatorResult = MallocAllocator::allocate(bytes);

			BOOST_ASSERT(mallocAllocatorResult);

#if UNG_DEBUGMODE
			AllocatorHelper::checkAddress(mallocAllocatorResult);
#endif

			return mallocAllocatorResult;
		}

		//小于或者等于UMM_LEVEL_TWO_MAX_BYTES bytes就检查对应的free list

		/*
		数组中元素的类型为obj_type* volatile，那么指向元素的指针就是obj_type* volatile*
		chooseFreeList():寻找UMM_FREELISTS_COUNT个free lists中合适的一个
		*/

#if UNG_DEBUGMODE
		auto arrayPos = calFreeListsIndex(bytes);
#endif

		//获取适当的空闲列表
		auto ppFreeList = chooseFreeList(bytes);
		//result：指向obj_type对象，也就是指向区块
		auto result{ *ppFreeList };
		/*
		如果没有找到可用的free list，那么就准备重新填充free list
		当发现free list中没有可用区块了时，调用refill()，准备为free list重新填充空间。
		新的空间取自内存池（经由getSpace()完成）
		*/
		if (!result)
		{
			//记住：刚开始每个空闲列表都是空的，里面一个各自都没有
			auto refillRet = refill(AllocatorHelper::roundUp(bytes));

			BOOST_ASSERT(refillRet);

#if UNG_DEBUGMODE
			//在refill()调用之后，计算内存池的剩余空间大小
			BigDog::getInstance().setPoolRemaining(sEnd - sStart);
			BigDog::getInstance().addFreeListsTouched(arrayPos);
			BigDog::getInstance().substractFreeListsCellNum(arrayPos);

			AllocatorHelper::checkAddress(refillRet);
#endif

			return refillRet;
		}
#if UNG_DEBUGMODE
		//说明成功了
		else
		{
			BigDog::getInstance().addFreeListsTouched(arrayPos);
			BigDog::getInstance().substractFreeListsCellNum(arrayPos);
		}
#endif
				
		//调整free list
		*ppFreeList = (**ppFreeList).mLink;						//**ppFreeList，得到obj_type对象

#if UNG_DEBUGMODE
		AllocatorHelper::checkAddress(result);
#endif

		BOOST_ASSERT(result);

		/*
		free list中的每个块，都是free的
		*/
		return result;
	}

	void PoolAllocator::deallocate(void* p, size_type bytes)
	{
		//UMM_BYTESALIGNED个字节对齐，否则会存在当时分配的多，而当释放时却少了几个字节
		bytes = AllocatorHelper::roundUp(bytes);

		if (bytes > UMM_LEVEL_TWO_MAX_BYTES)
		{
			MallocAllocator::deallocate(p, bytes);
		}
		else
		{
			auto ppFreeList = chooseFreeList(bytes);
			auto q = static_cast<obj_type*>(p);

			//回收后放置于开头
			q->mLink = *ppFreeList;
			*ppFreeList = q;

#if UNG_DEBUGMODE
			auto arrayPos = calFreeListsIndex(bytes);
			BigDog::getInstance().addFreeListsCellNum(arrayPos,1);
#endif
		}
	}

	typename PoolAllocator::size_type PoolAllocator::calFreeListsIndex(size_type bytes)
	{
		return ((bytes + UMM_BYTESALIGNED - 1) / UMM_BYTESALIGNED) - 1;
	}

	PoolAllocator::obj_type* volatile* PoolAllocator::chooseFreeList(size_type bytes)
	{
		return sFreeLists + calFreeListsIndex(bytes);
	}

	void* PoolAllocator::refill(size_type size)
	{
		//缺省取得20个新节点（新区块）
		size_type objNum{ 20 };

		//尝试取得objNum个区块作为free list的新节点
		char* chunk = getSpace(size, objNum);
		obj_type* volatile* ppFreeList;
		obj_type* result;
		obj_type* current_obj;
		obj_type* next_obj;
		int i;

		//如果只获得了一个区块，这个区块就分配给调用者使用，free list无新节点
		if (1 == objNum)
		{
#if UNG_DEBUGMODE
			/*
			虽然只获得了一个区块，而且这个区块还没有给到free list，就直接让拿走了，但是，因为从
			这个函数里面跳出去后，在其外面的函数中，其逻辑是：只要成功拿走了一个指向小格子的指针，
			就会减去相应索引的free list的一个小格子数量，所以，在这里先把这个小格子的数量给先加上，
			等到出去后，让外面的逻辑再减去。
			*/
			auto arrayPos = calFreeListsIndex(size);
			BigDog::getInstance().addFreeListsCellNum(arrayPos, objNum);
#endif
			return chunk;
		}
					
		//否则准备调整free list，纳入新节点
		ppFreeList = chooseFreeList(size);

#if UNG_DEBUGMODE
		auto arrayPos = calFreeListsIndex(size);
		BigDog::getInstance().addFreeListsCellNum(arrayPos,objNum);
#endif

		//在chunk空间内建立free list
		result = reinterpret_cast<obj_type*>(chunk);
		//导引free list指向新配置的空间（取自内存池）
		*ppFreeList = next_obj = reinterpret_cast<obj_type*>(chunk + size);
		/*
		将free list的各节点串接起来，从1开始，因为第0个将返回
		*/
		for (i = 1; ; i++)
		{
			current_obj = next_obj;
			next_obj = reinterpret_cast<obj_type*>(reinterpret_cast<char*>(next_obj) + size);
			if (objNum - 1 == i)				//i在不断增加
			{
				current_obj->mLink = nullptr;
				break;
			}
			else
			{
				current_obj->mLink = next_obj;
			}
		}

		return result;
	}

	char* PoolAllocator::getSpace(size_type size, size_type& num)
	{
		char* result;
		size_type totalBytes = size * num;
		size_type bytesLeft = sEnd - sStart;				//内存池剩余空间

		//内存池剩余空间完全满足需求量
		if (bytesLeft >= totalBytes)
		{
			result = sStart;
			sStart += totalBytes;

			return result;
		}
		//内存池剩余空间不能完全满足需求量，但足够供应一个（含）以上的区块
		else if (bytesLeft >= size)
		{
			num = (size_type)(bytesLeft / size);
			totalBytes = size * num;
			result = sStart;
			sStart += totalBytes;

			return result;
		}
		//内存池剩余空间连一个区块的大小都无法提供
		else
		{
			//试着让内存池中的残余零头还有利用价值，配给适当的free list
			if (bytesLeft > 0)
			{
				//寻找适当的free list
				auto ppFreeList = chooseFreeList(bytesLeft);

				//调整free list，将内存池中的残余空间编入
				((obj_type*)sStart)->mLink = *ppFreeList;
				*ppFreeList = (obj_type*)sStart;

#if UNG_DEBUGMODE
				//配给哪个索引了
				auto arrayPos = calFreeListsIndex(bytesLeft);
				//被配给的这个索引，其每个格子的字节数为
				auto cellBytes = (arrayPos + 1) * UMM_BYTESALIGNED;
				//配了几个
				auto count = bytesLeft / cellBytes;
				//必须整除
				BOOST_ASSERT((bytesLeft % cellBytes) == 0);
				BigDog::getInstance().addFreeListsCellNum(arrayPos,count);
#endif
			}
#if UNG_DEBUGMODE
			else
			{
				BOOST_ASSERT(sStart == sEnd);
			}
#endif

			sStart = sEnd = nullptr;

			//此时，内存池中已经没有水了，下面给其补充水

			/*
			内存池中新水量的大小为需求量的两倍，再加上一个随着配置次数增加而愈来愈大的附加量
			配置这么多，如果配置成功的话，会把一部分交给free list管理，剩余部分留给内存池储备，交给free list管理的部分，会从其中
			拿走一个区块去使用。
			*/
			size_type bytesToGet = 2 * totalBytes + AllocatorHelper::roundUp(sHeapSize >> 4);					//>> 4:除以2^4，也就是除以16
			
#if UNG_DEBUGMODE
			//检查bytesToGet必须为UMM_BYTESALIGNED的整数倍
			BOOST_ASSERT((bytesToGet % UMM_BYTESALIGNED) == 0);
#endif

			//从内存条中配置内存空间，用来补充内存池
			sStart = (char*)AllocatorHelper::mallocAligned(bytesToGet);

			//如果内存条中空间不足
			if (!sStart)
			{
				size_type i;
				obj_type* volatile* ppFreeList;
				obj_type* p;

				/*
				试着检视我们手上拥有的东西，搜寻适当的free list，所谓适当是指“尚有未用区块，且区块够大”之free list。
				我们不打算尝试去配置较小的区块，因为那在多进程（multi-process）机器上容易导致灾难
				*/
				for (i = size; i <= UMM_LEVEL_TWO_MAX_BYTES; i += UMM_BYTESALIGNED)
				{
					ppFreeList = chooseFreeList(i);
					p = *ppFreeList;
					//free list内尚有未用区块
					if (p)
					{
#if UNG_DEBUGMODE
						//在哪个索引的空闲列表中找到了可用的块
						auto arrayPos = calFreeListsIndex(i);
						BigDog::getInstance().substractFreeListsCellNum(arrayPos);
#endif

						//调整free list以释出未用区块
						*ppFreeList = p->mLink;
						sStart = (char*)p;
						//+i：只拿出了一个cell来给内存池使用
						sEnd = sStart + i;

						//再次调用自己
						return getSpace(size, num);
					}
				}

				//至此，malloc bytesToGet这么多字节失败后，并且还没有从空闲列表中找到合适的可以释放给内存池的cell

				//不要求那么多可以吗?
				for (i = 5; (sHeapSize >> i) > 0; ++i)
				{
					size_type smallBytes = totalBytes + AllocatorHelper::roundUp(sHeapSize >> i);

#if UNG_DEBUGMODE
					//检查smallBytes必须为UMM_BYTESALIGNED的整数倍
					BOOST_ASSERT((smallBytes % UMM_BYTESALIGNED) == 0);
#endif

					sStart = (char*)AllocatorHelper::mallocAligned(smallBytes);
					if (sStart)
					{
						sHeapSize += smallBytes;

#if UNG_DEBUGMODE
						BigDog::getInstance().addLevelSecondMallocTimes();
						BigDog::getInstance().addLevelSecondHeapSize(smallBytes);
#endif

						sEnd = sStart + smallBytes;

						//再次调用自己
						return getSpace(size, num);
					}
				}
					

#if UNG_DEBUGMODE
				//走到这里，说明已经山穷水尽了，整个system heap没有内存可用了
				std::cout << std::endl;
				std::cout << "**********---**********" << std::endl;
				std::cout << "***马上就要山穷水尽了***" << std::endl;
				std::cout << "**********---**********" << std::endl;
				std::cout << std::endl;
#endif

				//调用第一级配置器，看看out-of-memory机制能否尽点力，这会导致要么内存不足的情况获得改善，要么抛出异常
				//这里的参数bytesToGet或许有点太大了，应该给个小一点的。
				sStart = (char*)MallocAllocator::allocate(bytesToGet);
				/*
				这里，但凡能够返回一个地址给sStart，就说明out-of-memory机制成功了，否则，在MallocAllocator::allocate()
				里面就抛出异常或者陷入死循环了。
				*/
				BOOST_ASSERT(sStart);
			}

			sHeapSize += bytesToGet;

#if UNG_DEBUGMODE
			BigDog::getInstance().addLevelSecondMallocTimes();
			BigDog::getInstance().addLevelSecondHeapSize(bytesToGet);
#endif

			sEnd = sStart + bytesToGet;

			//再次调用自己
			return getSpace(size, num);
		}
	}
}//namespace ung

#endif//UMM_HIERARCHICAL_COMPILE

/*
void* malloc(size_t size);
malloc returns a void pointer to the allocated space, or NULL if there is insufficient(不足的) 
memory available. To return a pointer to a type other than void, use a type cast on the 
return value. The storage space pointed to by the return value is guaranteed to be suitably(适当的) 
aligned for storage of any type of object that has an alignment requirement less than or 
equal to that of the fundamental alignment. (In Visual C++, the fundamental alignment 
is the alignment that's required for a double, or 8 bytes. In code that targets 64-bit platforms,
it’s 16 bytes.) Use _aligned_malloc to allocate storage for objects that have a larger 
alignment requirement—for example, the SSE types __m128 and __m256, and types 
that are declared by using __declspec(align(``n``)) where n is greater than 8. If size is 
0, malloc allocates a zero-length item in the heap and returns a valid pointer to that 
item. Always check the return from malloc, even if the amount of memory requested is 
small.
The malloc function allocates a memory block of at least(最少这么多字节) size bytes. The 
block may be larger(可能分配的空间要比参数给定的大) than size bytes because of the space 
that's required for alignment and maintenance information.
*/

/*
<malloc.h>
void* _aligned_malloc(size_t size,size_t alignment);
size:Size of the requested memory allocation.
alignment:The alignment value, which must be an integer power of 2.
Allocates memory on a specified alignment boundary.
Return:A pointer to the memory block that was allocated or NULL if the operation 
failed.The pointer is a multiple of alignment.

void _aligned_free(void *memblock);
Frees a block of memory that was allocated with _aligned_malloc or _aligned_offset_malloc.
If memblock is a NULL pointer, this function simply performs no actions.
*/

/*
volatile限定符
volatile的确切含义与机器有关，只能通过阅读编译器文档来理解。要想让使用了volatile的程序在移植到新机器或新编译器后仍然有效，通常需要对该程序进行某些改变。
直接处理硬件的程序常常包含这样的数据元素，它们的值由程序直接控制之外的过程控制。例如，程序可能包含一个由系统时钟定时更新的变量。当变量的值可能在程序的
控制或检测之外被改变时，应该将该对象声明为volatile。关键字volatile告诉编译器不应对这样的对象进行优化。
volatile限定符的用法和const很相似。
const和volatile限定符互相没什么影响，某种类型可能既是const的也是volatile的，此时它同时具有二者的属性。
只有volatile的成员函数才能被volatile的对象调用。
我们可以声明volatile指针，指向volatile对象的指针以及指向volatile对象的volatile指针。
和const一样，我们只能将一个volatile对象的地址（或者拷贝一个指向volatile类型的指针）赋给一个指向volatile的指针。同时，只有当某个引用是volatile的时，我们
才能使用一个volatile对象初始化该引用。
合成的拷贝对volatile对象无效
const和volatile的一个重要区别是我们不能使用合成的拷贝/移动构造函数及赋值运算符初始化volatile对象或从volatile对象赋值。合成的成员接受的形参类型是（非volatile）
常量引用，显然我们不能把一个非volatile引用绑定到一个volatile对象上。
如果一个类希望拷贝，移动或赋值它的volatile对象，则该类必须自定义拷贝或移动操作。例如，我们可以将形参类型指定为const volatile引用。
尽管我们可以为volatile对象定义拷贝和赋值操作，但是问题是拷贝volatile对象是否有意义呢？
*/

/*
定义static成员变量：
A* B::pA = nullptr;																//不存在调用A类的构造函数的情况
A B::mA;																			//在main()函数调用之前，会调用其构造函数
*/