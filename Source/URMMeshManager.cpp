#include "URMMeshManager.h"

#ifdef URM_HIERARCHICAL_COMPILE

#include "URMMesh.h"
#include "URMMeshParser.h"
#include "URMMaterialManager.h"

namespace ung
{
	String meshPath("../../../../Resource/Media/Models/");

	MeshManager::MeshManager()
	{
	}

	MeshManager::~MeshManager()
	{
	}

	std::shared_ptr<Mesh> MeshManager::createMesh(String const & meshName)
	{
		BOOST_ASSERT(!meshName.empty() && "mesh name invalid.");

		String name = meshPath + meshName;

#if UNG_DEBUGMODE
		const char* nameCStr = name.c_str();

		auto validRet = Filesystem::getInstance().isRegularFile(nameCStr);
		BOOST_ASSERT(validRet && "not a file.");

		auto extRet = Filesystem::getInstance().hasExtension(nameCStr);
		BOOST_ASSERT(extRet && "does not have extension.");

		String extension = Filesystem::getInstance().getFileExtension(nameCStr);
#endif

		auto getRet = getMesh(name);

		BOOST_ASSERT(!getRet && String(name + " has been created.").c_str());

		std::shared_ptr<Mesh> meshPtr(UNG_NEW_SMART Mesh(name));

		BOOST_ASSERT(meshPtr && "failed to create mesh.");

		addMesh(meshName,meshPtr);

		return meshPtr;
	}

	void MeshManager::buildMesh(std::shared_ptr<Mesh> meshPtr)
	{
		BOOST_ASSERT(meshPtr);

		meshPtr->build();
	}

	std::shared_ptr<Mesh> MeshManager::getMesh(String const & meshName)
	{
		BOOST_ASSERT(!meshName.empty() && "mesh name invalid.");

		String name = meshPath + meshName;

		auto findRet = mMeshs.find(name);

		if (findRet == mMeshs.end())
		{
			return nullptr;
		}

		return findRet->second;
	}

	std::shared_ptr<const Mesh> MeshManager::getMesh(String const & meshName) const
	{
		BOOST_ASSERT(!meshName.empty() && "mesh name invalid.");

		String name = meshPath + meshName;

		auto findRet = mMeshs.find(name);

		if (findRet == mMeshs.end())
		{
			return nullptr;
		}

		return findRet->second;
	}

	usize MeshManager::getMeshCount() const
	{
		return mMeshs.size();
	}

	void MeshManager::addMesh(String const& meshName,std::shared_ptr<Mesh> meshPtr)
	{
		String name = meshPath + meshName;

		mMeshs[name] = meshPtr;
	}

	void MeshManager::removeMesh(String const & meshName)
	{
		BOOST_ASSERT(!meshName.empty() && "mesh name invalid.");

		String name = meshPath + meshName;

#if UNG_DEBUGMODE
		auto getRet = getMesh(name);

		BOOST_ASSERT(getRet && "this mesh has not been created.");
#endif

		auto eraseRet = mMeshs.erase(name);

		BOOST_ASSERT(eraseRet == 1);
	}
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE