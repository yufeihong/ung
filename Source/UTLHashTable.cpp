/*
散列表的实现叫做散列（hashing）。
散列是一种用于以常数平均时间执行插入，删除和查找的技术。但是，那些需要元素间任何排序信息的操作将不会得到有效的支持。
每个关键字被映射到从0到TableSize-1这个范围中的某个数，并且被放到适当的单元中。这个映射就叫做散列函数（hash function）。
*/

/*
hash可以计算任意C++对象的散列值。位于名字空间boost，boost/functional/hash.hpp中。
hash是一个非常简单的函数对象，类摘要如下：
template<typename T>
struct hash : std::unary_function<T, std::size_t>							//标准单参函数对象
{
	std::size_t operator()(T const& val) const;									//计算val的散列值
};
hash完全符合C++11标准，对C++数据类型的支持非常全面（并且有扩展），包括：
1：char/wchar_t(但不支持C++11的char16_t/char32_t)
2：short/int/long等各种整数类型
3：bool类型
4：float/real_type等浮点数类型
5：long long和long real_type(如果编译器支持)
6：指针和数组
7：std::string/std::wstring
8：扩展支持pair,complex结构和array,vector,list等标准容器
hash库不支持上述以外的其他类型，包括标准容器适配器（stack，queue，priority_queue）和所有boost容器(bimap,circular_buffer等)。如果企图用hash计算
不支持的类型，那么会导致编译错误。

除了直接计算对象的散列值，hash更常见的用途是被用作无序容器的模板参数，作为容器计算散列值的一个策略对象：
boost::unordered_set<int,boost::hash<int>> us;
boost::unordered_map<int,string,boost::hash<int>> um;

实现原理：
hash函数对象实际上只是一个简单的包装类，真正的散列值计算实现是在boost名字空间里的hash_value()函数，而hash_value()则又针对各种数据类型定义了不同
的重载形式，最后hash再针对不同的类型使用模板特化来调用hash_value()函数。
hash_value()函数的声明通常是下面的形式：
template<typename T>
std::size_t hash_value(T const&);
例如，对于基本数据类型int，hash的实现如下：
inline std::size_t hash_value(int v)													//注意，这个不是模板函数，而是重载
{
	return static_cast<std::size_t>(v);												//计算散列值
}

template<>
struct hash<int> : public std::unary_function<int, std::size_t>			//模板特化
{
	std::size_t operator()(int v) const
	{
		return boost::hash_value(v);													//调用散列函数
	}
};

扩展hash：
使用hash库提供的对基本数据类型计算散列值的能力和一些辅助函数，我们可以实现对自定义类型计算散列值。
class person
{
private:
	int id;
	string name;
	unsigned int age;

public:
	person(int a, const char* b, unsigned int c) :
		id(a),
		name(b),
		age(c)
	{
	}

	size_t hash_value() const																//自定义的散列计算函数，也可以是其它的名字
	{
		return hash<int>()(id);															//只调用hash函数对象，根据id计算散列值
	}

	friend bool operator==(person const& l, person const& r)
	{
		return l.id == r.id;
	}
};

size_t hash_value(person const& p)
{
	return p.hash_value();
}
这样就可以把hash函数对象应用于我们自定义的person对象了：
person p(1, "adam", 20);
cout << hash<person>()(p) << endl;												//可正确执行
如果要把person对象放入无序容器，除了实现hash外，还需要定义operator==。

组合散列值
如果要对多个目标计算散列值，那么可以使用hash库提供的辅助函数hash_combine()和hash_range()来组合散列值，它们的声明如下：
template<typename T>
void hash_combine(size_t& seed, T const& v);

template<typename It>
size_t hash_range(It first, It last);

template<typename It>
void hash_range(size_t& seed,It first, It last);

使用hash_combine()可以为person类定制新的散列计算方法：
size_t hash_value() const
{
	size_t seed = 925;																		//可以是任意的整数
	hash_combine(seed, id);
	hash_combine(seed, name);
	hash_combine(seed, age);

	return seed;
}
散列值的计算与hash_combine()的运算顺序有关，即使是对同样的元素，如果计算顺序不同，那么最后得到的散列值也会不同。

hash_range()是另外一种组合散列值的方式，它对一个迭代器区间内的所有元素调用hash_combine()计算散列值，如果不给初始seed值，那么seed默认为0。
vector<int> v{ 1,2,5,8,15 };
size_t hv = hash_range(v.begin(), v.end());

unordered_set<int> us{ 1,2,5,8,15 };
hv = hash_range(us.begin(),us.end());
灵活使用hash_combine()和hash_range()，我们就可以对任意C++对象使用任意策略计算散列值（实际上hash库对标准容器的处理就用了这两个函数）。
class demo_class
{
private:
	vector<string> v;
	int x;

public:
	size_t hash_value()
	{
		size_t seed = 0;
		hash_combine(seed, x);
		hash_range(seed, v.rbegin(), v.rend());

		return seed;
	}
};
*/

/*
二叉搜索树具有对数平均时间的表现，但这样的表现构造在一个假设上：输入数据有足够的随机性。
hash table（散列表），这种结构在插入，删除，搜寻等操作上具有“常数平均时间”的表现，而且这种表现是以统计为基础，不需仰赖输入元素的随机性。
hash table可提供对任何有名项（named item）的存取操作和删除操作。由于操作对象是有名项，所以hash table也可被视为一种字典结构（dictionary）。这种结构的
用意在于提供常数时间之基本操作。

开链（separate chaining）
这种做法是在每一个表格元素中维护一个list。虽然针对list而进行的搜寻只能是一种线性操作，但如果list够短，速度还是够快。

称hash table表格内的元素为桶（bucket）。
*/