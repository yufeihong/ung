#include "UBSPlatform.h"

#ifdef UBS_HIERARCHICAL_COMPILE

#include <iostream>

namespace ung
{
	/*
	用法：
	Platform::getInstance().printProcessor();
	Platform::getInstance().printEndianness();
	*/

	Platform::Platform()
	{
	}

	Platform::~Platform()
	{
	}

	bool Platform::isBigEndian()
	{
		float x = 1.0f;
		unsigned char first_byte;

		/*
		在缓冲区之间复制字节.
		*/
		memcpy(&first_byte, &x, 1);

		return first_byte != 0;
	}

	void Platform::printProcessor()
	{
#if defined(__i386) || defined(__i386__) || defined(_M_IX86)									\
|| defined(__amd64) || defined(__amd64__)  || defined(_M_AMD64)							\
|| defined(__x86_64) || defined(__x86_64__) || defined(_M_X64)
		std::cout << "Processor: x86 or x64" << std::endl;
#elif defined(__ia64) || defined(__ia64__) || defined(_M_IA64)
		std::cout << "Processor: ia64" << std::endl;
#elif defined(__powerpc) || defined(__powerpc__) || defined(__POWERPC__)			\
|| defined(__ppc) || defined(__ppc__) || defined(__PPC__)
		std::cout << "Processor: PowerPC" << std::endl;
#elif defined(__m68k) || defined(__m68k__)															\
|| defined(__mc68000) || defined(__mc68000__)														\
		std::cout << "Processor: Motorola 68K" << std::endl;
#else
		std::cout << "Processor: Unknown" << std::endl;
#endif
	}

	void Platform::printEndianness()
	{
		if (isBigEndian())
		{
			std::cout << "This platform is big-endian." << std::endl;
		}
		else
		{
			std::cout << "This platform is little-endian." << std::endl;
		}

#ifdef BOOST_BIG_ENDIAN
		std::cout << "BOOST_BIG_ENDIAN is defined." << std::endl;
#endif

#ifdef BOOST_LITTLE_ENDIAN
		std::cout << "BOOST_LITTTLE_ENDIAN is defined." << std::endl;
#endif
	}
}//namespace ung

#endif//UBS_HIERARCHICAL_COMPILE

/*
头文件std::fstream定义了三个类型来支持文件IO:std::ifstream从一个给定文件读取数据，std::ofstream向一个给定文件写入数据，std::fstream读写给定文件。
这些类型提供的操作和cin,std::cout对象的操作一样。特别是，可以用IO运算符>>和<<来读写文件，可以用getline从一个std::ifstream读取数据。
除了继承自std::iostream类型的行为之外，std::fstream中定义的类型还增加了一些新的成员来管理与流关联的文件。我们可以对std::fstream,std::ifstream和std::ofstream对
象调用这些操作，但不能对其他IO类型调用这些操作。
std::fstream fstrm;
创建一个未绑定的文件流。std::fstream是头文件std::fstream中定义的一个类型。
std::fstream fstrm(s);
创建一个std::fstream，并打开名为s的文件。s可以是string类型，或者是一个指向C风格字符串的指针。这些构造函数都是explicit的。默认的文件模式mode依
赖于std::fstream的类型。
std::fstream fstrm(s,mode);
与前一个构造函数类似，但按指定mode打开文件。
fstrm.open(s);
打开名为s的文件，并将文件与fstrm绑定。s可以是一个string或一个指向C风格字符串的指针。默认的文件mode依赖与std::fstream的类型，返回void。
fstrm.close();
关闭与fstrm绑定的文件。返回void。
fstrm.is_open();
返回一个bool值，指出与fstrm关联的文件是否成功打开且尚未关闭。
*/

//template<typename T> using sharedPtr = std::shared_ptr<T>;
/*
std::shared_ptr作为参数或者返回值传递的时候，应该尽量传递引用。这样能保证和拷贝普通指针或传递对象引用所用的时间基本相同。
否则，拷贝std::shared_ptr的话，在Debug模式下测试，所用时间是拷贝普通指针或传递对象引用所用时间的10倍。

在Debug模式下，通过std::shared_ptr指针去访问其所指对象所用的时间是普通指针访问或通过引用访问所需时间的8-10倍。

在Release模式下，通过传递std::shared_ptr的引用，然后再访问其所指的元素，所用的时间是普通指针或通过引用所需时间的2-3倍。
*/
//using std::shared_ptr;//一个std::shared_ptr对象为8个字节
//using std::unique_ptr;//一个std::unique_ptr对象为4个字节

//namespace S = std;//别名(alias)

/*
右手坐标系。x轴正方向朝左；y轴正方向朝上；z轴正方向朝外。
左，右手坐标系可以互相转换，最简单的方法是只翻转一个轴的符号。
*/

/*
物体坐标系是和特定物体相关联的坐标系。
每个物体都有它们独立的坐标系。当物体移动或改变方向时，和该物体相关联的坐标系将随之移动或改变方向。
物体坐标系也被称作模型坐标系，因为模型顶点的坐标都是在模型坐标系中描述的。

世界坐标系是一个特殊的坐标系，它建立了描述其他坐标系所需要的参考框架。从另一方面说，能够用世界坐标系描述其他坐标系的位置，而不能用更大的，
外部的坐标系来描述世界坐标系。

惯性坐标系
为了简化世界坐标系和物体坐标系的相互转换，人们引入了一种新的坐标系，称作惯性坐标系，意思是在世界坐标系到物体坐标系的“半途”。
惯性坐标系的原点和物体坐标系的原点重合，但惯性坐标系的轴平行于世界坐标系的轴。
为什么要引入惯性坐标系呢？因为从物体坐标系转换到惯性坐标系只需旋转，从惯性坐标系转换到世界坐标系只需要平移。
*/

/*
坐标系转换
考虑将物体坐标系本身转换到世界坐标系，这样就可以得到转换任意点的一般方法。
首先，旋转物体坐标系到惯性坐标系。可以想象这个转换是将物体坐标轴旋转到和惯性坐标系重合。
第二步，将惯性坐标系转换到世界坐标系。可以想象这个转换是将惯性坐标系原点移至世界坐标系原点。
举例：最终发现将物体坐标轴顺时针旋转45度，向下向左平移就转换到了世界坐标系。而从物体上某点来看，则恰恰相反，物体坐标系中的一个点逆时针
旋转45度，向上向右平移就转换到了世界坐标系。
*/

/*
一般来说，类型float和double分别有7和16个有效位。
执行浮点数运算应该选用double，这是因为float通常精度不够而且双精度浮点数和单精度浮点数的计算代价相差无几。事实上，对于某些机器来说，双
精度运算甚至比单精度还快。
*/

/*
constexpr变量:
在一个复杂系统中，很难(几乎肯定不能)分辨一个初始值到底是不是常量表达式。

C++11新标准规定，允许将变量声明为constexpr类型以便由编译器来验证变量的值是否是一个常量表达式。
声明为constexpr的变量一定是一个常量，而且必须用常量表达式初始化：
constexpr int mf = 20;//20是常量表达式。
constexpr int limit = mf + 1;//mf + 1是常量表达式。
constexpr int sz = size();//只有当size是一个constexpr函数时才是一条正确的声明语句。
新标准允许定义一种特殊的constexpr函数，这种函数应该足够简单以使得编译时就可以计算其结果，这样就能用constexpr函数去初
始化constexpr变量了。

一般来说，如果你认定变量是一个常量表达式，那么就把它声明成constexpr类型。

指针和constexpr:
必须明确一点，在constexpr声明中如果定义了一个指针，限定符constexpr仅对指针有效，与指针所指的对象无关：
const int *p = nullptr;//p是一个指向整型常量的指针。
constexpr int *q = nullptr;//q是一个指向整数的常量指针。
p和q的类型相差甚远，p是一个指向常量的指针，而q是一个常量指针，其中的关键在于constexpr把它所定义的对象置为了顶层const。

与其它常量指针类似，constexpr指针既可以指向常量也可以指向一个非常量：
constexpr int *np = nullptr;//np是一个指向整数的常量指针，其值为空。
int j = 0;
constexpr int i = 42;//i的类型是整型常量。
//i和j都必须定义在函数体之外
constexpr const int *p = &i;//p是常量指针，指向整型常量i。
constexpr int *p1 = &j;//p1是常量指针，指向整数j。

constexpr函数:
constexpr函数是指能用于常量表达式的函数。
定义constexpr函数要遵循几项约定：
函数的返回类型及所有形参的类型都得是字面值类型，而且函数中必须有且只有一条return语句。
constexpr int new_sz() { return 42;}
constexpr int foo = new_sz();//正确，foo是一个常量表达式
执行该初始化任务时，编译器把对constexpr函数的调用替换成其结果值。为了能在编译过程中随时展开，constexpr函数被隐式地指
定为内联函数。

constexpr函数不一定返回常量表达式。
constexpr size_t scale(size_t cnt) {return new_sz() * cnt;}
如果arg是常量表达式，则scale(arg)也是常量表达式。
当scale的实参是常量表达式时，它的返回值也是常量表达式。反之则不然。
int arr[scale(2)];//正确，scale(2)是常量表达式。
int i = 2;//i不是常量表达式。
int a2[scale(i)];//错误，scale(i)不是常量表达式。

内联函数和constexpr函数通常定义在头文件中。和其他函数不一样，内联函数和constexpr函数可以在程序中多次定义。毕竟，编译
器要想展开函数仅有函数声明是不够的，还需要函数的定义。

constexpr构造函数:
我们知道constexpr函数的参数和返回值必须是字面值类型。
除了算术类型、引用和指针外，某些类也是字面值类型。

和其他类不同，字面值类型的类可能含有constexpr函数成员。这样的成员必须符合constexpr函数的所有要求，它们是隐式const的。

数据成员都是字面值类型的聚合类是字面值常量类。如果一个类不是聚合类，但它符合下述要求，则它也是一个字面值常量类：
1：数据成员都必须是字面值类型。
2：类必须至少含有一个constexpr构造函数。
3：如果一个数据成员含有类内初始值，则内置类型成员的初始值必须是一条常量表达式，或者如果成员属于某种类类型，则初始值必
须使用成员自己的constexpr构造函数。
4：类必须使用析构函数的默认定义，该成员负责销毁类的对象。

尽管构造函数不能是const的，但是字面值常量类的构造函数可以是constexpr函数。事实上，一个字面值常量类必须至少提供一个
constexpr构造函数。

inline和constexpr的函数模板:
函数模板可以声明为inline或constexpr的，如同非模板函数一样。inline或constexpr说明符放在模板参数列表之后，返回类型之前：
//正确，inline说明符跟在模板参数列表之后
template <typename T> inline T min(const T&, const T&);
//错误，inline说明符的位置不正确
inline template <typename T> T min(const T&, const T&);
*/

/*
声明和定义：
一个声明就是一个定义，除非：
1：它声明了一个没有具体说明函数体的函数。
2：它包含了一个extern说明符，并且没有初始化程序或函数体。
3：它是一个类定义内的静态类数据成员的声明。
4：它是一个类名的声明。
5：它是一个typedef声明。

一个定义就是一个声明，除非：
1：它定义了一个静态类数据成员。
2：它定义了一个non-inline成员函数。

在C++中，声明和定义的区别在于：在一个给定的作用域中重复一个声明是合法的；相比之下，在程序中使用的每个实体（例如，
类、对象、枚举、函数等）必须只有一个定义。

内部链接和外部链接：
编译一个.c文件时，C预处理器（C Pre-Processor,CPP）首先（递归地）包含头文件，形成包含所有必要信息的单个源文件。
之后，编译这个中间文件（称为编译单元）生成一个和根文件名字一样的.o文件（称为目标文件）。链接程序把各种编译单元
内产生的符号链接起来，生成一个可执行程序。有两种不同的链接：内部链接和外部链接。

如果一个名字对于它的编译单元来说是局部的，并且在链接时与在其他编译单元中定义的标识符名称不冲突，那么这个名字有内
部链接。

内部链接意味着对这个定义的访问受到当前编译单元的限制。也就是说，一个有内部链接的定义对于任何其他编译单元都是不可
见的，因此在链接过程中不能用来解析未定义的符号。
例如：
static int x;
虽然在文件作用域中定义，但是关键字static决定了链接是内部的。
上面是静态数据，静态函数也是内部链接。
枚举也是内部链接：
enum Boolean {No,Yes};
枚举是定义（不只是声明），但是它们自己绝对不会把符号引入到.o文件中。为了让内部链接定义影响一个程序的其他部分，它们
必须放置在头文件而不是.c文件中。
用内部链接定义的一个重要的例子就是类的定义。类的描述是一个定义，而不是一个声明；因此，它不能够在同一个作用域的编译
单元内重复定义。在单个编译单元外部使用的类必须在一个头文件中定义。
内联函数的定义是用内部链接定义的另一个例子。

如果一个名字有外部链接，那么在多文件程序中，在链接时这个名字可以和其他编译单元交互。
外部链接意味着这个定义不局限于单个的编译单元。在.o文件中，具有外部链接的定义产生外部符号，这些外部符号可以被所有
其他的编译单元访问，用来解析其他编译单元中未定义的符号。这种外部符号在整个程序中必须是唯一的，否则这个程序不能链接。

静态类数据成员（在类定义内声明）有外部链接。
静态类数据成员只是一个声明，但是它在.c文件中的定义有外部链接。

非内联成员函数（包括静态成员）有外部链接，非内联、非静态free函数（即非成员函数）也一样。
class Point
{
private:
	int d_x;
	int d_y;

public:
	Point(int x,int y) :								//内部链接
	d_x(x),
	d_y(y)
	{
	}

	int x() const										//内部链接
	{
		return d_x;
	}
	int y() const										//内部链接
	{
		return d_y;
	}
}；														//内部链接

inline int operator==(const Point& left,const Point& right)			//内部链接
{
	return left.x() == right.x() && left.y() == right.y();
}

//非内联成员函数
Point& Point::operator+=(const Point& right)								//外部链接
{
	d_x += right.d_x;
	d_y += right.d_y;
	return *this;
}

//非内联free函数																			//内部链接
Point operator+=(const Point& left,const Point& right)
{
	return Point(left.x() + right.x(),left.y() + right.y());
}

函数：
1：非内联的自由函数								//外部链接
2：生来就是给别人调用的成员函数				//外部链接
3：内联的自由函数									//内部链接
4：内联的成员函数									//内部链接

C++编译器会在任何可能的地方把一个内联函数体直接替换成函数调用，而不将任何符号引入.o文件。有时（出于各种原因，
例如递归或动态绑定）编译器会选择制定一个内联函数的静态拷贝。这个静态拷贝只将一个局部符号引入当前的.o文件，这个
符号不能与外部符号交互。

一个声明只对当前的编译单元有用，所以声明本身根本未将任何内容引入到.o文件。
int f();					//bad idea
extern int i;			//bad idea
struct S
{
	int g();				//fine
};
这些声明本身并没有影响由此而产生的.o文件内容。每个声明只是命名了一个外部符号，让当前编译单元在需要的时候能够获取
相应全局定义的访问。这实际上是符号名（例如，调用函数）的使用而不是声明本身，这将导致在.o文件中加入一个未定义的符号。

类声明和类定义对于.o文件都毫无用处，它们只对当前的编译单元有用。

C++语言处理枚举和类的方式不同。
在C++中，未定义就声明一个枚举是不可能的。类声明经常用来代替预处理#include指令，以便在未定义之前就声明一个类。
*/

/*
头文件：
在C++中，在一个头文件的作用域内放置一个带有内部链接的定义，例如静态函数或者静态数据，是合法的。可是这种做法并不
理想。这些文件作用域的定义不仅污染了全局名字空间，而且在有静态数据和函数的情况下，它们在每个包含头文件的编译单元中
消耗数据空间。甚至数据在文件作用域内声明为const时也可能会引起同样的问题，如果这个常量的地址曾经被占用就更是如此。

头文件中可以/不可以出现的内容：
//radio.h
#ifndef INCLUDED_RADIO
#define INCLUDED_RADIO

int z;												//illegal,external data definition
extern int LENGTH = 10;				//illegal,external data definition
const int WIDTH = 5;					//avoid,constant data definition
static int y;									//avoid,static data definition
static void func() {}						//avoid,static function definition

class Radio
{
	static int s_count;						//fine,static member declaration
	static const real_type S_PI;			//fine,static const member dec.
	int d_size;									//fine,member data definition

public:
	int size() const;						//fine,member function declaration
};

inline int Radio::size() const			//fine,inline function definition
{
	return d_size;
}

int Radio::s_count;						//illegal,static member definition
real_type Radio::S_PI = 3.14;			//illegal,static const member def.
int Radio::size() const {}				//illegal,member function definition

#endif

实现文件(.c)
有时我们会选择定义一些函数和数据用于我们自己的实现，而不想这种实现暴露在编译单元的外部。带有内部链接而不带有外部链接
的定义可能会出现在.c文件的文件作用域中，但不会影响全局（符号）名字空间。在.c文件的文件作用域内应该避免定义那些未被
声明为静态的数据和函数。例如：
//file.c
int i;												//external linkage
int max(int a,int b)						//external linkage
{
	return a > b ? a : b;
}
上面的定义有外部链接而且可能在全局名字空间中与其他类似的名字存在潜在的冲突。因为内联函数和静态自由函数有内部链接，
这些种类的函数可以在.c文件的文件作用域内定义并且不会污染全局名字空间。

枚举定义、声明为static的非成员对象，以及（默认）const数据定义也有内部链接。在.c文件的文件作用域中定义所有这些实体
都是安全的。例如：
//file3.c
#include <math.h>

class Link;																		//internal

enum {START_SIZE = 1,GROW_FACTOR = 2};					//internal

const real_type PI_SQ = M_PI * M_PI;									//internal

static const char* names[] = {"Ntran","Ptran"};				//internal

static Link* s_root_p;														//internal
Link* const s_first_p = s_root_p;										//internal
像typedef声明和预处理宏指令等其他结构，不会在.o文件中引入输出符号。它们也可能出现在.c文件的作用域内而不会影响全局
名字空间。

静态类和文件作用域常量数据是无状态的。

带有内部链接的定义限制在单个的编译单元中，不能影响其他的编译单元，除非把它放在一个头文件中。这样的定义可以存在与.c文件
的文件作用域内，不会影响全局（符号）名字空间。
在链接时，带有外部链接的定义可以用来解析其他编译单元中未定义的符号。绝大多数情况下，把这样的定义放在头文件中是一个程序
设计的错误。

类成员布局：
生成函数(CREATOR)生成对象或消除对象。注意，operator=不是一个生成函数，但却是（习惯上）第一个操纵函数。
操纵函数（MANIPULATORS）就是非常量成员函数。访问函数(ACCESSORS)是常量成员函数。

某些用户错误的认为一个对象只不过就是一个有数据成员的公共数据结构，所以才采用这种风格：每一个数据成员都即有一个get
函数(访问函数)又有一个set函数(操纵函数)---这种风格本身可能会阻碍封装接口的合理创建，在合理封装的接口中，数据成员没有
必要透明地反映在对象的行为中。

Uses-In-Interface关系：
一旦一个函数在其参数表中命名一个类型或者在返回一个值时命名一个类型，就称这个函数在其接口中使用了该类型。
如果在声明一个函数时涉及某个类型，那么就说在该函数的接口中使用了该类型。

Uses-In-The-Implementation：
如果在一个函数的定义中涉及了一个类型，那么就说在这个函数的实现中使用了该类型。
如果一个类型：
1：被用在类的成员函数中。
2：在类的数据成员的声明中被涉及。
3：是类的私有基类。
那么这个类的实现中就使用了这个类型。

如果一个类的任何成员函数（包括私有成员函数）在其接口或实现中命名了一个类型，就认为在该类的逻辑实现中使用了该类型。

HasA和HoldsA:
当一个类X嵌入类型T的一个（私有）数据成员时，通常称为HasA。
尽管类X包含一个数据成员，但是数据成员的类型只是从T派生而来（例如：T*或T&），我们仍然认为在X的逻辑实现中使用了T。有时会
把这种内部使用称为HoldsA。

WasA：
私有地继承一个类型是在一个类的逻辑实现中使用该类型的另一种方式。
私有继承是一种技术，只可以用来传送基类属性的一个子集。
私有继承是一种实现细节，但是公有继承和受保护继承却不是。
非私有继承会引入客户端程序通过编程可以访问的信息。

继承与分层：
继承是逻辑层次结构的一种形式，分层是另一种形式。
如果一个类在其实现中实质地使用了一个类型，则该类在该类型上分层。
分层是把更小、更简单、更原始的类型建成更大、更复杂、更精密的类型的过程。通常，分层通过组合（例如：HasA或HoldsA）发生，但是
实质性使用的任何形式（即引起物理依赖的任何使用）都被认为是分层。
对继承来说，越是特殊、具体的类越是依赖一般、抽象的类。
对分层来说，在较高抽象层次上的类依赖较低抽象层次上的类。
*/

/*
成员数据访问：
封装是面向对象设计的一个重要工具。封装意味着将一组低层次的信息集合在一起，使它们以一种紧密耦合的密切方式潜在地相互作用。信息
隐藏用限制外部环境与类的内部细节的交互来促进代码的实现，而这些细节是与类所支持的抽象无关的。
保持所有数据成员私有，并提供适当的访问函数和操纵函数，这允许我们自己去改变内部表示，而不必重新编写客户端程序的代码。
直接（可写地）访问数据很容易让对象处于不一致的状态。只提供一个函数接口就可以授予类的作者必要的控制级别，以确保其对象的完整性。
提供操纵函数和访问函数也给予开发者插入临时代码的机会（例如，用于调试的print语句、用于性能调整的引用计数，以及用于可靠性的assert
语句）。
注意，对于本身被完全隐藏的（不是私有地隐藏在另一个类的内部，就是局部地隐藏在一个.c文件的内部）一个结构体（或类）的数据成员的
公共访问是一个单独的问题，不适用于上述规则。当数据成员不是私有时，通过使用关键字struct而不是class来表示有意缺少的封装更合适。
有人主张使用受保护的数据以方便来自派生类的任意访问。但是从可维护性的角度看，受保护的访问与公共访问是一样的，因为任何想要得到
受保护数据的人，只要稍微增加一点派生一个类的工作即可。不像友元关系，友元关系明确地表示了谁有权访问私有细节，而受保护类型数据
极大地违背了封装的原则。
适用于公共接口的观点同样也适用于受保护接口。通过将受保护的接口和公共的接口看成是独立但同等重要的，基类的作者可以保留可维护性。
保持所有数据成员的私有并提供合适的受保护的函数，将使得对基类实现的改变独立于任何派生类。

全局数据：
避免在文件作用域内包含带有外部链接的数据。
文件作用域中带有外部链接的数据，与存在与其他编译单元中的全局变量有冲突的危险。
1：将所有全局变量放入一个结构中。
2：然后将它们私有化并添加静态访问函数。
由于所有的接口函数都是静态的，所以没有必要为使用这个类实例化一个对象。将默认构造函数声明为私有并不实现，这样可以强制采用这种
使用模式。

自由函数：
自由函数也会对全局名字空间形成威胁。
如果一个自由函数在一个.h文件中定义为带有内部链接或者在一个.c文件中定义为带有外部链接，那么在程序集成过程中自由函数可能会与
有相同名称（和签名）的另一个函数定义相冲突。运算符函数是一个例外。
自由函数总能分组到一个只包含静态函数的工具类（结构体）中。
自由运算符函数不能嵌套在类中。这不是一个严重的问题，因为自由运算符要求至少有一个参数是用户自定义类型。因此自由运算符冲突的
可能性很小，而且这种冲突在实践中通常构不成问题。

枚举、typedef和常量数据：
枚举类型、typedef和（默认的）文件作用域常量数据都有内部链接。人们经常在头文件的文件作用域内声明常量、枚举或typedef，这是错误的。
//paint.h
enum Color {RED,GREEN,BLUE,ORANGE,YELLOW};				//bad idea
//juice.h
enum Fruit {APPLE,ORANGE,GRAPE,CRANBERRY};				//bad idea
这两个枚举可能不是同一个开发者写的，但是很可能在某一天包含在同一个文件中，导致ORANGE有二义性，无法解析。
如果在单独的类中定义这两个枚举，我们就可以很容易地使用域解析消除二义性问题。Paint::Orange或Juice::Orange。
基于类似的原因，typedef和常量数据也应该放在头文件的类作用域内。大多数常量数据是整形的，并且嵌套枚举可以很好地在类的作用域中
提供整形常量。其他的常量类型（如，real_type,string)必须是类的静态成员，并且在.c文件中初始化。

预处理宏：
除非作为包含卫哨，否则应该避免在头文件中使用预处理宏。
由于宏不是C++的一部分，它们不能够被放置在一个类的作用域内。

头文件中的名字：
在一个.h文件作用域中只应该声明类、结构体、联合体和自由运算符函数。
在.h文件作用域中只应该定义类、结构体、联合体和内联（成员或自由运算符）函数。
//driver.h
#ifndef INCLUDED_DRIVER
#define INCLUDED_DRIVER

#ifndef INCLUDED_NIFTY													//fine,redundant include guard
#include "nifty.h"
#endif

#define PI 3.14																	//avoid,macro constant
#define MIN(X) ((X)<(Y)?(X):(Y))										//avoid,macro function

class ostream;																	//fine,class declaration
struct DriverInit;																//fine,class declaration
union Uaw;																		//fine,class declaration

extern int globalVarible;														//avoid,external data declaration
static int fileScopeVariable;												//avoid,internal data definition
const int BUFFER_SIZE = 256;											//avoid,const data definition
enum Boolean {ZERO,ONE};												//avoid,enumeration at file scope
typedef long BigInt;															//avoid,typedef at file scope

class Driver
{
	enum Color {RED,GREEN};											//fine,enumeration in class scope
	typedef int (Driver::*PMF)();											//fine,typedef in class scope
	static int s_count;															//fine,static member declaration
	int d_size;																		//fine,member data definition

	struct Pnt
	{
		short int d_x,d_y;
		Pnt(int x,int y) :
		d_x(x),d_y(y) {}
	};																					//fine,private struct definition

	friend DriverInit;															//fine,friend declaration

public:
	int static round(real_type d);												//fine,static member function declaration
	void setSize(int size);													//fine,member function declaration
	int cmp(const Driver&) const;										//fine,const member function declaration
};

static class DriverInit
{
} driverInit;																		//special case

int min(int x,int y);															//avoid,free function declaration

inline int max(int x,int y)													//avoid,free inline function definition
{
	return x >y ? x : y;
}

inline void Driver::setSize(int size)										//fine,inline member function declaration
{
	d_size = size;
}

ostream& operator<<(ostream& o,const Driver& d);			//fine,free operator function declaration

inline int operator==(const Driver& lhs,const Driver& rhs)	//fine,free inline operator function declaration
{
	return compare(lhs,rhs) == 0;
}

inline int Driver::round(real_type d)										//fine,inline static member function definition
{
	return d < 0 ? -int(0.5 - d) : int (0.5 + d);
}

#endif//INCLUDED_DRIVER

冗余包含卫哨：
在每个头文件的预处理器包含指示符周围放置冗余的（外部的）包含卫哨。
冗余包含卫哨很难看，但是没有实质性的危害。不使用冗余包含卫哨要冒编译时间复杂度达到二次方（即O(N^2))的风险。
注意，冗余卫哨在.c文件中不是必要的。如果在.c文件中未能谨慎地复制#include指示符，对于N个不同的.h文件，（病态的）最坏情况下
的性能是2N，问题规模仍然是线性的（即O(N))。

在新的C++项目中没有必要使用全局变量。我们可以通过将变量作为私有静态成员放置在一个类的作用域中，并提供公共静态成员函数访问
它们来系统地消除全局变量。但是，对这些模块的过度依赖是一种不良设计的症状。

自由函数，特别是那些不操作任何用户自定义类型的函数，在系统集成时很可能与其他的函数冲突。在类的作用域中嵌套这种函数作为静态
成员几乎能够消除冲突的危险。

枚举、typedef以及常量数据也可能威胁全局名字空间。通过将枚举类型嵌套在类作用域中，任何歧义都可以通过作用域解析来消除。文件作用域
中的typedef看起来很像类，而且在大型项目中极难发现。通过将typedef嵌套在类作用域中，它们就变的相对容易追踪。在头文件中定义的一个
整数常量，最好是用类作用域中的一个枚举值表示。其他常量类型可以通过使它们成为某个类的静态常量成员来限定其范围。

预处理宏对人和机器来说都是难以理解的。由于不是C++语言的一部分，所以宏不被作用域约束，并且，如果放置在一个头文件中，宏可能与
系统中的任一文件的任一标识符相冲突。因此，宏不应该出现在头文件中，除非作为包含卫哨。

从总体来看，除类、结构体、联合体和自由运算符外，我们应该避免在一个头文件中引入任何内容到作用域。当然，我们允许在头文件中定义
内联成员函数。

通过在头文件中使用冗余卫哨包围include指令，我们可以确保一个头文件在同一编译单元最多被打开两次。
*/