#include "URMVertexElement.h"

#ifdef URM_HIERARCHICAL_COMPILE

#include "UFCColorValue.h"
#include "UIFIRenderSystem.h"
#include "URMUtilities.h"

//这里要调整
#include "UNGRoot.h"

namespace ung
{
	VertexElement::VertexElement(uint8 stream, uint8 offset,VertexElementSemantic semantic,VertexElementType theType, uint8 index) :
		mStream(stream),
		mOffset(offset),
		mSemantic(semantic),
		mType(ungProcessVertexElementType(theType)),
		mUsageIndex(index)
	{
	}

	VertexElement::~VertexElement()
	{
	}

	VertexElement::VertexElement(VertexElement const & rightInst) noexcept :
		mStream(rightInst.mStream),
		mOffset(rightInst.mOffset),
		mSemantic(rightInst.mSemantic),
		mType(rightInst.mType),
		mUsageIndex(rightInst.mUsageIndex)
	{
	}

	VertexElement& VertexElement::operator=(VertexElement const & rightInst) noexcept
	{
		if (this != &rightInst)
		{
			mStream = rightInst.mStream;
			mOffset = rightInst.mOffset;
			mSemantic = rightInst.mSemantic;
			mType = rightInst.mType;
			mUsageIndex = rightInst.mUsageIndex;
		}

		return *this;
	}

	VertexElement::VertexElement(VertexElement && moveInst) noexcept :
		mStream(moveInst.mStream),
		mOffset(moveInst.mOffset),
		mSemantic(moveInst.mSemantic),
		mType(moveInst.mType),
		mUsageIndex(moveInst.mUsageIndex)
	{
	}

	VertexElement & VertexElement::operator=(VertexElement && moveInst) noexcept
	{
		if (this != &moveInst)
		{
			mStream = moveInst.mStream;
			mOffset = moveInst.mOffset;
			mSemantic = moveInst.mSemantic;
			mType = moveInst.mType;
			mUsageIndex = moveInst.mUsageIndex;
		}

		return *this;
	}

	VertexElement* VertexElement::clone() const
	{
		auto ret = UNG_NEW VertexElement(*this);

		return ret;
	}

	uint8 VertexElement::getStream() const
	{
		return mStream;
	}

	uint8 VertexElement::getOffset() const
	{
		return mOffset;
	}

	VertexElementType VertexElement::getType() const
	{
		return mType;
	}

	VertexElementSemantic VertexElement::getSemantic() const
	{
		return mSemantic;
	}

	uint8 VertexElement::getUsageIndex() const
	{
		return mUsageIndex;
	}

	uint32 VertexElement::getSize() const
	{
		return getTypeSize(mType);
	}

	uint32 VertexElement::getTypeSize(VertexElementType etype)
	{
		switch (etype)
		{
		case VertexElementType::VET_COLOR:
		case VertexElementType::VET_COLOR_ABGR:
		case VertexElementType::VET_COLOR_ARGB:
			return sizeof(RGBA);
		case VertexElementType::VET_FLOAT1:
			return sizeof(float);
		case VertexElementType::VET_FLOAT2:
			return sizeof(float) * 2;
		case VertexElementType::VET_FLOAT3:
			return sizeof(float) * 3;
		case VertexElementType::VET_FLOAT4:
			return sizeof(float) * 4;
		}
		
		UNG_EXCEPTION("Invalid vertex element type.");
	}

	uint32 VertexElement::getTypeCount(VertexElementType etype)
	{
		switch (etype)
		{
		case VertexElementType::VET_COLOR:
		case VertexElementType::VET_COLOR_ABGR:
		case VertexElementType::VET_COLOR_ARGB:
			return 1;
		case VertexElementType::VET_FLOAT1:
			return 1;
		case VertexElementType::VET_FLOAT2:
			return 2;
		case VertexElementType::VET_FLOAT3:
			return 3;
		case VertexElementType::VET_FLOAT4:
			return 4;
		}

		UNG_EXCEPTION("Invalid vertex element type.");
	}

	bool VertexElement::operator==(const VertexElement& rightInst) const
	{
		if (mType != rightInst.mType || mUsageIndex != rightInst.mUsageIndex || 
			mOffset != rightInst.mOffset || mSemantic != rightInst.mSemantic || mStream != rightInst.mStream)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	bool VertexElement::operator!=(const VertexElement& rightInst) const
	{
		return !(*this == rightInst);
	}
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE