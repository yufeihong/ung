#include "UPECustom.h"

#ifdef UPE_HIERARCHICAL_COMPILE

namespace ung
{
	PhysicsCustom::PhysicsCustom()
	{
	}
	PhysicsCustom::~PhysicsCustom()
	{
	}
	void PhysicsCustom::initialize()
	{
	}
	void PhysicsCustom::update()
	{
	}
	void PhysicsCustom::addSphere(real_type radius, WeakObjectPtr object)
	{
	}
	void PhysicsCustom::addBox(Vector3 const & min, Vector3 const & max, WeakObjectPtr object)
	{
	}
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE