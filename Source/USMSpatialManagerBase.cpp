#include "USMSpatialManagerBase.h"

#ifdef USM_HIERARCHICAL_COMPILE

#pragma warning(push)

//返回局部变量的引用
#pragma warning(disable : 4172)

namespace ung
{
	ISpatialManagerFactory * SpatialManagerBase::getCreator() const
	{
		return nullptr;
	}

	String const & SpatialManagerBase::getFactoryIdentify() const
	{
		return EMPTY_STRING;
	}

	SceneTypeMask SpatialManagerBase::getFactoryMask() const
	{
		return SceneTypeMask();
	}

	String const & SpatialManagerBase::getManagerName() const
	{
		return EMPTY_STRING;
	}

	ISpatialNode * SpatialManagerBase::createNode(ISpatialNode * parent)
	{
		return nullptr;
	}

	void SpatialManagerBase::destroyNode(ISpatialNode * node)
	{
	}

	ISpatialNode * SpatialManagerBase::getRootNode() const
	{
		return nullptr;
	}

	ISpatialNode * SpatialManagerBase::placeObject(StrongIObjectPtr objectPtr)
	{
		return nullptr;
	}

	void SpatialManagerBase::takeAwayObject(StrongIObjectPtr objectPtr)
	{
	}

	StrongISceneQueryPtr SpatialManagerBase::createRaySceneQuery(Ray const & ray)
	{
		return StrongISceneQueryPtr();
	}

	StrongISceneQueryPtr SpatialManagerBase::createSphereSceneQuery(Sphere const & sphere)
	{
		return StrongISceneQueryPtr();
	}

	StrongISceneQueryPtr SpatialManagerBase::createAABBSceneQuery(AABB const & box)
	{
		return StrongISceneQueryPtr();
	}

	StrongISceneQueryPtr SpatialManagerBase::createCameraSceneQuery(Camera const & camera)
	{
		return StrongISceneQueryPtr();
	}
}//namespace ung

#pragma warning(pop)

#endif//USM_HIERARCHICAL_COMPILE