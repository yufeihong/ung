#include "USMObject.h"

#ifdef USM_HIERARCHICAL_COMPILE

#include "USMObjectComponent.h"
#include "USMTransformComponent.h"
#include "USMSceneNode.h"
#include "USMSpatialManagerBase.h"
#include "..\API\Include\USMObject.h"

using namespace tinyxml2;

namespace ung
{
	Object::Object(StrongISceneManagerPtr smPtr,String const& objectName) :
		mName(objectName.empty() ? UniqueID().toString() : objectName),
		mComponents("Object_mComponents"),
		mManualCallDestroyFlag(false),
		mSceneManager(smPtr),
		mSpatialNode(nullptr),
		mBox(Vector3(-1),Vector3(1))
	{
	}

	Object::~Object()
	{
		//必须手工调用场景销毁对象的函数
		BOOST_ASSERT(mManualCallDestroyFlag);

		std::cout << "销毁对象:" << mName << std::endl;
	}

	String const& Object::getName() const
	{
		return mName;
	}

	void Object::attachToSceneNode(StrongISceneNodePtr sceneNodePtr)
	{
		BOOST_ASSERT(sceneNodePtr);

		sceneNodePtr->attachObject(shared_from_this());
	}

	void Object::detachFromSceneNode(bool remove)
	{
		if (!isAttachSceneNode())
		{
			return;
		}

		//移除场景图节点中存储的该对象的引用
		mAttachedSceneNode.lock()->detachObject(shared_from_this(),remove);
	}

	bool Object::isAttachSceneNode() const
	{
		return mAttachedSceneNode.expired() ? false : true;
	}

	WeakISceneNodePtr Object::getAttachedNode() const
	{
		//如果对象没有关联场景图节点
		if (!isAttachSceneNode())
		{
			return WeakISceneNodePtr{};
		}

		return mAttachedSceneNode;
	}

	void Object::preInit(XMLElement* pRoot)
	{
		//设置实体的type和资源路径
	}

	void Object::postInit()
	{
		for (auto it = mComponents.begin(); it != mComponents.end(); ++it)
		{
			it->second->postInit();
		}
	}

	/*
	The destroy() function is called when you want to destroy the actor. The actor holds onto 
	strong references to each of its components, but the components also need to hold onto 
	strong references to the actor.Having a circular reference can potentially cause memory 
	leaks. It’s not easily avoided since some components may still need to access the actor 
	during destruction time. If weak pointers were used instead, it would cause a crash 
	whenever the component destructor tried to access the actor. The actor gets destroyed 
	when all strong references are released, which means all weak references are immediately 
	made invalid. The result is that the component’s weak reference to the actor is no longer 
	valid and can’t be used. Since both references need to be strong references, the circular 
	reference chain has to be explicitly broken. The destroy() function takes care of this by 
	explicitly clearing out the component map.
	*/
	void Object::destroy()
	{
		/*
		触发每个组件的析构函数，然后在组件析构的时候，组件所持有的对象的强引用将会被释放。
		*/
		mComponents.clear();

		mManualCallDestroyFlag = true;
	}

	void Object::setAttachedSceneNode(WeakISceneNodePtr sceneNodePtr)
	{
		mAttachedSceneNode = sceneNodePtr;
	}

	void Object::addComponent(StrongIObjectComponentPtr pComponent)
	{
		//组件的名字
		auto componentName = pComponent->getName();
		mComponents.save(componentName, pComponent);
	}

	std::shared_ptr<IMovable> Object::getTransformComponent()
	{
		auto transformComponent = getComponent<TransformComponent>("TransformComponent");
		BOOST_ASSERT(!transformComponent.expired());

		return transformComponent.lock();
	}

	void Object::update()
	{
		//更新该对象的所有组件
		auto beg = mComponents.begin();
		auto end = mComponents.end();
		while (beg != end)
		{
			//这里必须有括号
			(*beg).second->update();

			++beg;
		}
	}

	void Object::adjustmentHangInSpatial()
	{
		/*
		一个对象，只有被关联到一个场景节点的时候，这个对象才会同时被空间管理器所管理。当对象脱离了
		场景节点或未曾关联到一个场景节点的时候，那么这个对象是不会被渲染的，也不会参与各种碰撞，所
		以这时候，该对象也不被空间管理器所管理。
		*/

		//获取场景管理器中的空间管理器
		BOOST_ASSERT(!mSceneManager.expired());
		auto spatialManager = mSceneManager.lock()->getSpatialManager();

		//关联到场景节点的状态
		if (isAttachSceneNode())
		{
			//将对象放置到空间管理器中
			mSpatialNode = spatialManager->placeObject(shared_from_this());
		}
		//未关联到场景节点的状态
		else
		{
			//从空间管理器中拿走对象
			spatialManager->takeAwayObject(shared_from_this());
			mSpatialNode = nullptr;
		}
	}

	ISpatialNode * Object::getAttachedSpatialNode() const
	{
		return mSpatialNode;
	}

	real_type Object::getRadius() const
	{
		return mBox.getRadius();
	}

	AABB const & Object::getAABB() const
	{
		return mBox;
	}

	AABB Object::getWorldAABB()
	{
		AABB temp{};

		//对象从模型空间到世界空间的变换
		auto toWorld = getTransformComponent()->getToWorldMatrix();

		temp = mBox * toWorld;

		return temp;
	}
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE

/*
Lua doesn’t have to know anything about the internals of the actor system;it just knows
that it has a value it can use to tell the actor system to do something with a specific actor.
*/