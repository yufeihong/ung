/*
效率说明：
Debug：
new，delete同样数量(千万级别以上)，使用内存池技术所消耗的时间是使用全局new和delete的二分之一。
Release：
是五分之一。
样本数量越大，使用内存池技术的效果越明显。

shared_ptr效率低下，不要大量使用。

比较：
Debug：
a：智能指针使用std::make_shared<testClass>()。
b：智能指针使用std::shared_ptr<testClass> p(new testClass)。
b比a约少用10% ~ 15%的时间，因此建议使用std::shared_ptr<testClass> p(new testClass)方法。
*/

/*
基于各种原因，多态行为乃是面向对象编程中最重要的性质，因此这些小型对象不能存储在stack内，只能位于free store（自由空间）中。
一般而言，对于通过new配得的每一块内存，其用于簿记管理的部分达到数个（4-32个）bytes。

在标准C++中，你其实可以藉由两种方式重载系统缺省的operator delete：
void operator delete(void* p);
void operator delete(void* p,std::size_t size);
如果采用第一形式，意味着你不在意“待释区块之大小”。
编译器自动提供对象大小的时候，根本没有任何额外开销。编译器会即时产生一些代码用以计算对象大小。
*/

/*
让shared_ptr搭配内存池的使用方法：
void func()
{
	shared_ptr<demo_class> p(new demo_class);
}

int main()
{
	func();

	return 0;
}
会调用demo_class重载的new方法，当离开作用域时，会调用demo_class重载的delete方法

------------------------------------------------------------------------------------------------------------------------------------------------------------

shared_ptr和new结合使用:
如果我们不初始化一个智能指针，它就会被初始化为一个空指针。
我们还可以用new返回的指针来初始化智能指针：
shared_ptr<double> p1;																			//shared_ptr可以指向一个double
shared_ptr<int> p2(new int(42));																//p2指向一个值为42的int
接受指针参数的智能指针构造函数是explicit的。因此，我们不能将一个内置指针隐式转换为一个智能指针，必须使用直接初始化形式来初始化一个智能指针：
shared_ptr<int> p1 = new int(1024);														//错误，必须使用直接初始化形式
shared_ptr<int> p2(new int(1024));															//正确，使用了直接初始化形式
p1的初始化隐式地要求编译器用一个new返回的int*来创建一个shared_ptr。由于我们不能进行内置指针到智能指针间的隐式转换，因此这条初始化语句时错误的。出于相
同的原因，一个返回shared_ptr的函数不能在其返回语句中隐式转换一个普通指针：
shared_ptr<int> clone(int p)
{
	return new int(p);																				//错误，隐式转换为shared_ptr<int>
}
我们必须将shared_ptr显式绑定到一个想要返回的指针上：
shared_ptr<int> clone(int p)
{
	return shared_ptr<int>(new int(p));														//正确，显式地用int*创建shared_ptr<int>
}

默认情况下，一个用来初始化智能指针的普通指针必须指向动态内存，因为智能指针默认使用delete释放它所关联的对象。我们可以将智能指针绑定到一个指向其他类型的资
源的指针上，但是为了这样做，必须提供自己的操作来替代delete。

定义和改变shared_ptr的其他方法:
shared_ptr<T> p(q)
p管理内置指针q所指向的对象。q必须指向new分配的内存，且能够转换为T*类型。

shared_ptr<T> p(u)
p从unique_ptr u那里接管了对象的所有权。将u置为空。

shared_ptr<T> p(q,d)
p接管了内置指针q所指向的对象的所有权。q必须能转换为T*类型。p将使用可调用对象d来代替delete。

shared_ptr<T> p(p2,d)
p是shared_ptr p2的拷贝，唯一的区别是p将用可调用对象d来代替delete。

p.reset(),p.reset(q),p.reset(q,d)
若p是唯一指向其对象的shared_ptr，reset会释放此对象。若传递了可选的参数内置指针q，会令p指向q，否则会将p置为空。若还传递了参数d，将会调用d而不是delete来
释放q。

------------------------------------------------------------------------------------------------------------------------------------------------------------

不要混合使用普通指针和智能指针：
shared_ptr可以协调对象的析构，但这仅限于其自身的拷贝(也是shared_ptr)之间。这也是为什么我们推荐使用make_shared而不是new的原因。这样，我们就能在分配对
象的同时就将shared_ptr与之绑定，从而避免了无意中将同一块内存绑定到多个独立创建的shared_ptr上。
考虑下面对shared_ptr进行操作的函数：
//在函数被调用时ptr被创建并初始化
void process(shared_ptr<int> ptr)
{
	//使用 ptr
}//ptr离开作用域，被销毁
process的参数是传值方式传递的，因此实参会被拷贝到ptr中。拷贝一个shared_ptr会递增其引用计数，因此，在process运行过程中，引用计数至少为2.当process结束时，
ptr的引用计数会递减，但不会变为0.因此，当局部变量ptr被销毁时，ptr指向的内存不会被释放。
使用此函数的正确方法是传递给它一个shared_ptr：
shared_ptr<int> p(new int(42));															//引用计数为1
process(p);																						//拷贝p会递增它的引用计数；在process中引用计数值为2
int i = *p;																							//正确，引用计数值为1
虽然不能传递给process一个内置指针，但可以传递给它一个(临时的)shared_ptr，这个shared_ptr是用一个内置指针显式构造的。但是，这样做很可能会导致错误：
int *x(new int(1024));																			//危险，x是一个普通指针，不是一个智能指针
process(x);																							//错误，不能将int*转换为一个shared_ptr<int>
process(shared_ptr<int>(x));																//合法的，但内存会被释放！
int j = *x;																							//未定义的，x是一个空悬指针！
在上面的调用中，我们将一个临时shared_ptr传递给process。当这个调用所在的表达式结束时，这个临时对象就被销毁了。销毁这个临时变量会递减引用计数，此时引用计
数就变为0了。因此，当临时对象被销毁时，它所指向的内存会被释放。
但x继续指向(已经释放的)内存，从而变成一个空悬指针。如果试图使用x的值，其行为是未定义的。

当将一个shared_ptr绑定到一个普通指针时，我们就将内存的管理责任交给了这个shared_ptr。一旦这样做了，我们就不应该再使用内置指针来访问shared_ptr所指向的内
存了。

------------------------------------------------------------------------------------------------------------------------------------------------------------

不要使用get初始化另一个智能指针或为智能指针赋值：
智能指针类型定义了一个名为get的函数，它返回一个内置指针，指向智能指针管理的对象。此函数是为了这样一种情况而设计的：我们需要向不能使用智能指针的代码传递一个
内置指针。使用get返回的指针的代码不能delete此指针。

虽然编译器不会给出错误信息，但将另一个智能指针也绑定到get返回的指针上时错误的：
shared_ptr<int> p(new int(42));																//引用计数为1
int *q = p.get();																						//ok: but don't use q in any way that might delete its pointer.(这里中文版翻译略有混淆)
//新程序块
{
//未定义：两个独立的shared_ptr指向相同的内存。
shared_ptr<int>(q);
}//程序块结束，q被销毁，它指向的内存被释放
int foo = *p;																							//未定义：p指向的内存已经被释放了
在本例中，p和q指向相同的内存。由于它们是相互独立创建的，因此各自的引用计数都是1。当q所在的程序块结束时，q被销毁，这会导致q指向的内存被释放。从而p变成了一
个空悬指针，意味着当我们试图使用p时，将发生未定义的行为。而且，当p被销毁时，这块内存会被第二次delete。

get用来将指针的访问权限传递给代码，你只有在确定代码不会delete指针的情况下，才能使用get。特别是，永远不要用get初始化另一个智能指针或者为另一个智能指针赋值。

------------------------------------------------------------------------------------------------------------------------------------------------------------

其他shared_ptr操作：
我们可以用reset来将一个新的指针赋予一个shared_ptr：
p = new int(1024);																					//错误，不能将一个指针赋予shared_ptr
p.reset(new int(1024));																			//正确，p指向一个新对象
与赋值类似，reset会更新引用计数，如果需要的话，会释放p指向的对象。reset成员经常与unique一起使用，来控制多个shared_ptr共享的对象。在改变底层对象之前，我们
检查自己是否是当前对象仅有的用户。如果不是，在改变之前要制作一份新的拷贝：
if (!p.unique())
{
	p.reset(new string(*p));																		//我们不是唯一用户，分配新的拷贝
}
*p += newVal;																						//现在我们知道自己是唯一的用户，可以改变对象的值。
*/

/*
术语:new表达式与operator new函数
标准库函数operator new和operator delete的名字容易让人误解.和其他operator函数不同(比如operator=),这两个函数并没有重载new表达式或
delete表达式.实际上,我们根本无法自定义new表达式或delete表达式的行为.
一条new表达式的执行过程总是先调用operator new函数以获取内存空间,然后在得到的内存空间中构造对象.与之相反,一条delete表达式的执行过程总
是先销毁对象,然后调用operator delete函数释放对象所占的空间.
我们提供新的operator new函数和operator delete函数的目的在于改变内存分配的方式,但是不管怎样,我们都不能改变new运算符和delete运算符的基
本含义.

1，分配一块足够大的，原始的，未命名的内存空间以便存储特定类型的对象（或者对象的数组）
2，编译器运行相应的构造函数以构造这些对象，并为其传入初始值
3，对象被分配了空间并构造完成,返回一个指向该对象的指针.

当编译器调用operator new时,把存储指定类型对象所需的字节数传给size_t形参;当调用operator new[]时,传入函数的则是存储数组中所有元素所需的空间
void* MemoryPool::operator new(size_t size);

***************************************************************************************************************

1，对p所指的对象或者所指的数组中的元素执行对应的析构函数
2，编译器调用名为operator delete(或者operator delete[])的函数释放内存空间.

当我们将operator delete或operator delete[]定义成类的成员时,该函数可以包含另外一个类型为size_t的形参.此时,该形参的初始值是第一个形参所
指对象的字节数.size_t形参可用于删除继承体系中的对象.如果基类有一个虚析构函数,则传递给operator delete的字节数将因待删除指针所指对象的动
态类型不同而有所区别.而且,实际运行的operator delete函数版本也由对象的动态类型决定.

void MemoryPool::operator delete(void* p, size_t size) noexcept;
*/

/*
设计原则：
1：认为智能指针不使用内存池。
2：考虑到小型区块所可能造成的内存破碎问题，这里设计了双层级配置器。
第一级配置器直接使用malloc()和free()。
第二级配置器视情况采用不同的策略：
a，当配置区块超过UMM_LEVEL_TWO_MAX_BYTES bytes时，视之为“足够大”，便调用第一级配置器。
b，当配置区块小于UMM_LEVEL_TWO_MAX_BYTES bytes时，视之为“过小”，采用内存池方式，而不再求助于第一级配置器。此法称之为次层配置（sub-allocation）
第二级配置器维护了UMM_FREELISTS_COUNT个链表，来负责UMM_FREELISTS_COUNT种小型区块的配置能力。
*/

/*
所谓C++ new handler机制是，你可以要求系统在内存配置需求无法被满足时，调用一个你所指定的函数。换句话说，一旦::operator new无法完成任务，在抛出
std::bad_alloc异常状态之前，会先调用由客户端指定的处理例程。该处理例程通常被称为new-handler。

小额区块带来的其实不仅是内存碎片，配置时的额外负担（overhead）也是一个大问题。

次层配置
每次配置一大块内存，并维护对应之自由链表。下次若再有相同大小的内存需求，就直接从free-lists中拔出。如果客户端释还小额区块，就由配置器回收到free-lists
中。为了方便管理，第二级配置器会主动将任何小额区块的内存需求量上调至UMM_BYTESALIGNED的倍数，并维护UMM_FREELISTS_COUNT个free-lists，各自管理大小分别为8,16,24,32,40,48,56,64,72,80,
88,96,104,112,120,128bytes...的小额区块。
*/

/*
是否为2的整数次幂：
(v & (v - 1)) == 0
*/

/*
C++语言定义了两个运算符来分配和释放动态内存.
默认情况下,动态分配的对象是默认初始化的,这意味着内置类型或组合类型的对象的值将是未定义的,而类类型对象将用默认构造函数进行
初始化.
我们也可以通过()来进行值初始化.对于定义了自己的构造函数的类类型来说,使用值初始化是没有意义的,不管采用什么形式,对象都会通过
默认构造函数来初始化.但对于内置类型,两种形式的差别就很大了,值初始化的内置类型对象有着良好定义的值,而默认初始化的对象的值
则是未定义的.

值初始化是一种初始化过程.
内置类型初始化为0.
类类型由类的默认构造函数初始化.只有当类包含默认构造函数时,该类的对象才会被值初始化.
对于容器的初始化来说,如果只说明了容器的大小而没有指定初始值的话,就会执行值初始化,此时编译器会生成一个值,而容器的元素被初始化为该值.
*/

/*
如果成员是const、引用或者属于某种未提供默认构造函数的类类型,我们必须通过构造函数初始值列表为这些成员提供初始值.
随着构造函数体一开始执行,初始化就完成了.我们初始化const或者引用类型的数据成员的唯一机会就是通过构造函数初始值.
*/

/*
在class里面,需要注意数据成员的排序.
整个结构的对齐需求等同于其成员中的最大对齐需求.
*/

/*
C++标准库定义了一组类,用于报告标准库函数遇到的问题.这些异常类也可以在用户编写的程序中使用,它们分别定义在4个头文件中:
exception头文件定义了最通用的异常类exception.它只报告异常的发生,不提供任何额外信息.
stdexcept头文件定义了几种常用的异常类.
new头文件定义了bad_alloc异常类型.
type_info头文件定义了bad_cast异常类型.
*/
/*
一旦一个程序用光了它所有可用的内存,new表达式就会失败.默认情况下,如果new不能分配所要求的内存空间,它会抛出一个类型为
bad_alloc的异常.
*/

/*
静态成员函数不与任何对象绑定在一起,它们不包含this指针.作为结果,静态成员函数不能声明成const的,而且我们也不能在static函数体内使用
this指针.这一限制既适用于this的显式使用,也对调用非静态成员的隐式使用有效.
虽然静态成员不属于类的某个对象,但是我们仍然可以使用类的对象、引用或者指针来访问静态成员.
当在类的外部定义静态成员时,不能重复static关键字,该关键字只出现在类内部的声明语句.
*/

/*
成员函数不用通过作用域运算符就能直接使用静态成员.
*/

/*
通过在成员的声明之前加上关键字static使得其与类关联在一起.
类的静态成员存在于任何对象之外,对象中不包含任何与静态数据成员有关的数据.
因为静态数据成员不属于类的任何一个对象,所以它们并不是在创建类的对象时被定义的.这意味着它们不是由类的构造函数初始化的.而且一般来说,
我们不能在类的内部初始化静态成员.相反的,必须在类的外部定义和初始化每个静态成员.
类似于全局变量,静态数据成员定义在任何函数之外.因此一旦它被定义,就将一直存在于程序的整个生命周期中.
*/

/*
重载new和delete
尽管我们说能够"重载new和delete",但是实际上重载这两个运算符与重载其他运算符的过程大不相同.要想真正掌握重载new和delete的方法,首先要
对new表达式和delete表达式的工作机理有更多了解.

当我们使用一条new表达式时:
new表达式
string *sp = new string("a value");													//分配并初始化一个string对象
string *arr = new string[10];															//分配10个默认初始化的string对象
实际执行了三步操作.第一步,new表达式调用一个operator new(或者operator new[])的标准库函数.该函数分配一块足够大的、原始的、未命名的内
存空间以便存储特定类型的对象(或者对象的数组).第二步,编译器运行相应的构造函数以构造这些对象,并为其传入初始值.第三步,对象被分配了空间并
构造完成,返回一个指向该对象的指针.

当我们使用一条delete表达式删除一个动态分配的对象时:
delete sp;																						//销毁*sp,然后释放sp指向的内存空间
delete [] arr;																					//销毁数组中的元素,然后释放对应的内存空间
实际执行了两步操作.第一步,对sp所指的对象或者arr所指的数组中的元素执行对应的析构函数.第二步,编译器调用名为operator delete(或者
operator delete[])的标准库函数释放内存空间.

如果应用程序希望控制内存分配的过程,则它们需要定义自己的operator new函数和operator delete函数.即使在标准库中已经存在这两个函数的定义,
我们仍旧可以定义自己的版本.编译器不会对这种重复的定义提出异议,相反,编译器将使用我们自定义的版本替换标准库定义的版本.

应用程序可以在全局作用域中定义operator new函数和operator delete函数,也可以将它们定义为成员函数.当编译器发现一条new表达式或delete表达
式后,将在程序中查找可供调用的operator函数.如果被分配(释放)的对象是类类型,则编译器首先在类及其基类的作用域中查找.此时如果该类含有
operator new成员或operator delete成员,则相应的表达式将调用这些成员.否则,编译器在全局作用域查找匹配的函数.此时如果编译器找到了用户自
定义的版本,则使用该版本执行new表达式或delete表达式;如果没找到,则使用标准库定义的版本.
我们可以使用作用域运算符令new表达式或delete表达式忽略定义在类中的函数,直接执行全局作用域中的版本.例如,::new只在全局作用域中查找匹配的
operator new函数,::delete与之类似.
*/

/*
operator new接口和operator delete接口
标准库定义了operator new函数和operator delete函数的8个重载版本.其中前4个版本可能抛出bad_alloc异常,后4个版本则不会抛出异常:
//这些版本可能抛出异常
void *operator new(size_t);																//分配一个对象
void *operator new[](size_t);															//分配一个数组
void operator delete(void*) noexcept;												//释放一个对象
void operator delete[](void*) noexcept;												//释放一个数组

//这些版本承诺不会抛出异常
void *operator new(size_t, nothrow_t&) noexcept;
void *operator new[](size_t, nothrow_t&) noexcept;
void operator delete(void*, nothrow_t&) noexcept;
void operator delete[](void*, nothrow_t&) noexcept;
类型nothrow_t是定义在new头文件中的一个struct,在这个类型中不包含任何成员.new头文件还定义了一个名为nothrow的const对象,用户可以通过这
个对象请求new的非抛出版本.与析构函数类似,operator delete也不允许抛出异常.当我们重载这些运算符时,必须使用noexcept异常说明符指定其不
抛出异常.

应用程序可以自定义上面函数版本中的任意一个,前提是自定义的版本必须位于全局作用域或者类作用域中.当我们将上述运算符函数定义成类的成员时,它
们是隐式静态的.我们无须显式地声明static,当然这么做也不会引发错误.因为operator new用在对象构造之前而operator delete用在对象销毁之后,所
以这两个成员(new和delete)必须是静态的,而且它们不能操纵类的任何数据成员.

对于operator new函数或者operator new[]函数来说,它的返回类型必须是void*,第一个形参的类型必须是size_t且该形参不能含有默认实参.当我们为
一个对象分配空间时使用operator new;为一个数组分配空间时使用operator new[].当编译器调用operator new时,把存储指定类型对象所需的字节数
传给size_t形参;当调用operator new[]时,传入函数的则是存储数组中所有元素所需的空间.

如果我们想要自定义operator new函数,则可以为它提供额外的形参.此时,用到这些自定义函数的new表达式必须使用new的定位形式将实参传给新增的
新参.尽管在一般情况下我们可以自定义具有任何形参的operator new,但是下面这个函数却无论如何不能被用户重载:
void *operator new(size_t, void*);													//不允许重新定义这个版本
这种形式只供标准库使用,不能被用户重新定义.

对于operator delete函数或者operator delete[]函数来书,它们的返回类型必须是void,第一个形参的类型必须是void*.执行一条delete表达式将调用
相应的operator函数,并用指向待释放内存的指针来初始化void*形参.

当我们将operator delete或operator delete[]定义成类的成员时,该函数可以包含另外一个类型为size_t的形参.此时,该形参的初始值是第一个形参所
指对象的字节数.size_t形参可用于删除继承体系中的对象.如果基类有一个虚析构函数,则传递给operator delete的字节数将因待删除指针所指对象的动
态类型不同而有所区别.而且,实际运行的operator delete函数版本也由对象的动态类型决定.

术语:new表达式与operator new函数
标准库函数operator new和operator delete的名字容易让人误解.和其他operator函数不同(比如operator=),这两个函数并没有重载new表达式或
delete表达式.实际上,我们根本无法自定义new表达式或delete表达式的行为.
一条new表达式的执行过程总是先调用operator new函数以获取内存空间,然后在得到的内存空间中构造对象.与之相反,一条delete表达式的执行过程总
是先销毁对象,然后调用operator delete函数释放对象所占的空间.
我们提供新的operator new函数和operator delete函数的目的在于改变内存分配的方式,但是不管怎样,我们都不能改变new运算符和delete运算符的基
本含义.
*/

/*
malloc函数与free函数
当你定义了自己的全局operator new和operator delete后,这两个函数必须以某种方式执行分配内存与释放内存的操作.也许你的初衷仅仅是使用一个特
殊定制的内存分配器,但是这两个函数还应该同时满足某些测试的目的,即检验其分配内存的方式是否与常规方式类似.
为此,我们可以使用名为malloc和free的函数,C++从C语言中继承了这些函数,并将其定义在cstdlib头文件中.
malloc函数接受一个表示待分配字节数的size_t,返回指向分配空间的指针或者返回0以表示分配失败.free函数接受一个void*,它是malloc返回的指针的
副本,free将相关内存返回给系统.调用free(0)没有任何意义.
*/

/*
如果没有继承，类只是具有一些相关行为的数据结构。这只是对过程语言的一大改进，而继承则开辟了完全不同的新天地。通过继承，可以在已有
类的基础上创建新类。这样，类就成为可重用和可扩展的组件。
“是一个”关系是实际对象在继承层次中的存在模式。

class Super
{
public:
	Super();
	void someMethod();

protected:
	int mProtectedInt;

private:
	int mPrivateInt;
};

class Sub : public Super
{
public:
	Sub();
	void someOtherMethod();
};
客户对于继承的看法
对于客户或者代码的其他部分而言，Sub类型的对象仍然是一个Super类型的对象，因为Sub从Super继承。这意味着Super的所有public方法和
数据成员，和Sub的所有public方法和数据成员都是可供使用的。
要知道继承的运行方式是单向的，这一点很重要。Sub类与Super类具有明确的关系，但是Super类并不知道与Sub类有关的任何信息。这意味着
Super类型的对象不支持Sub的public方法和数据成员，因为Super不是Sub。
Super mySuper;
mySuper.someOtherMethod();											//Error,Super doesn't have a someOtherMethod()
从其他代码的观点来看,一个对象既属于定义它的类,又属于所有基类.
Super* superPointer = new Sub();										//Create sub,store it in super pointer
然而，不能通过Super指针（或引用）调用Sub类的方法。下面的代码无法运行：
superPointer->someOtherMethod();
编译器会报错，因为尽管对象是Sub类型，并且定义了someOtherMethod()方法，但编译器只是将它看成Super类型，而Super类型没有定义
someOtherMethod()方法。

从派生类的角度分析继承
派生类可以访问在基类中声明的public、protected方法和数据成员,就好像这些方法和数据成员是派生类自己的,因为从技术上讲,它们属于派生类。
例如，Sub中someOtherMethod()的实现可以使用在Super中声明的数据成员mProtectedInt。使用基类的数据成员和方法与使用派生类中的数据
成员和方法并无不同之处。
如果类将数据成员和方法声明为protected,派生类就可以访问它们.如果声明为private,派生类就不能访问.
private访问说明符可以控制派生类与基类的交互方式.应该将所有数据成员都声明为private,如果希望任何代码都可以访问这些数据成员,那么就提供
public的获取器和设置器.如果仅希望派生类访问它们,那么就提供protected的获取器和设置器.把数据成员默认设置为private的原因是，这会提供最高
级别的封装，这意味着可以改变数据的表示方式，而public或protected接口保持不变。不直接访问数据成员，也可以在public或protected设置器中
方便地添加对输入数据的检查。方法也应默认设置为private，只有需要公开的方法才设置为public，只有派生类需要访问的方法才设置为protected。
从派生类的观点看，基类的所有public，protected数据成员和方法都是可用的。

禁用继承
C++允许将类标记为final,这意味着继承这个类会导致编译错误.将类标记为final的方法是直接在类名的后面使用final关键字.

************************************************************************************************************

重写(override)方法:
1,将所有方法都设置为virtual,除了构造函数.
只有在基类中声明为virtual的方法才能被派生类正确地重写.注意,在方法定义中不需要重复使用virtual关键字.一旦将方法或析构函数标记为virtual,
它们在所有派生类中就一直是virtual,即使在派生类中删除了virtual关键字,也同样如此.
2,重写方法的语法.
为了重写某个方法,需要在派生类定义中重新声明这个方法,就像在基类中声明的那样,并在派生类的实现文件中提供新的定义.建议在重写方法的声明
末尾添加override关键字.
3,禁用重写.
C++允许将方法标记为final,这意味着无法在派生类中重写这个方法.“final”函数必须是虚函数。

记住，即使基类的引用或指针知道这实际上是一个派生类，也无法访问没有在基类中定义的派生类方法或者成员。下面的代码无法编译，因为Super引用
没有someOtherMethod()方法：
Sub mySub;
Super& ref = mySub;
mySub.someOtherMethod();																//This is fine
ref.someOtherMethod();																	//Error

非指针非引用对象无法正确处理派生类的特征信息。可以将Sub转换为Super，或者将Sub赋值给Super，因为Sub是一个Super。然而，此时这个对象
将遗失派生类的所有信息：
Sub mySub;
Super assignedObject = mySub;														//Assigns a Sub to a Super
assignedObject.someMethod();															//Call Super's version of someMethod().即便Sub已经override了这个方法
注意：基类的指针或者引用指向派生类对象时，派生类保留其重写方法。但是通过类型转换将派生类对象转换为基类对象时，就会丢失其特征。重写方法和
派生类数据的丢失称为截断（slicing）。

************************************************************************************************************

父类构造函数:
对象创建顺序：
1，如果某个类具有基类,执行基类的默认构造函数.除非在ctor-initializer中调用了基类构造函数,此时调用这个构造函数,而不是默认构造函数.
2，类的非静态数据成员按照声明的顺序创建.
3，执行该类的构造函数.
可以递归使用这些规则,如果类有祖父类,祖父类就在父类之前初始化,以此类推.
class Something
{
public:
	Something()
	{
		cout << "2";
	}
};

class Parent
{
public:
	Parent()
	{
		cout << "1";
	}
};

class Child : public Parent
{
public:
	Child()
	{
		cout << "3";
	}

private:
	Something mDataMember;
};

int main()
{
	Child myChild;

	return 0;
}
创建myChild对象时，首先调用Parent的构造函数，输出字符串"1"。随后，初始化mDataMember，调用Something构造函数输出字符串"2"。
最后调用Child的构造函数，输出"3"。
注意：Parent的构造函数是自动调用的。C++将自动调用父类的默认构造函数（如果存在的话）。如果父类的默认构造函数不存在，或者存在默认
构造函数但希望使用其他构造函数，可在构造函数初始化器中像初始化数据成员那样链接构造函数。
下面的代码显示了没有默认构造函数的Super版本。相关版本的Sub必须显式地告诉编译器如何调用Super的构造函数，否则代码将无法编译：
class Super
{
public:
	Super(int i);
};

class Sub : public Super
{
public:
	Sub();
};

Sub::Sub() :
	Super(7)
{
}

从派生类向基类传递构造函数的参数很正常,毫无问题,但是无法传递数据成员.如果这么做,代码可以编译,但是记住在调用基类构造函数之后才会
初始化数据成员.

虚方法的行为在构造函数中是不同的,如果派生类重写了基类中的虚方法,从基类构造函数中调用该方法,就会调用该虚方法的基类实现,而不是派生类中
的重写版本.

父类的析构函数:
由于析构函数没有参数,因此始终可以自动调用父类的析构函数.析构函数的调用顺序刚好与构造函数相反.
1，调用类的析构函数.
2，销毁类的数据成员,与创建的顺序相反.
3，如果有父类,调用父类的析构函数.

所有析构函数都应该是virtual的,否则,如果使用delete删除一个实际指向派生类的基类指针,析构函数调用链将被破坏.将会只调用父类的析构函数,因为
析构函数没有声明为virtual,结果是没有调用子类的析构函数,也没有调用其数据成员的析构函数.
将所有析构函数声明为virtual.编译器生成的默认析构函数不是virtual,因此应该定义自己的虚析构函数,至少在父类中应该这么做.
class Something
{
public:
	Something()
	{
		cout << "2";
	}

	~Something()																			//Should be virtual,but will work
	{
		cout << "2";
	}
};

class Parent
{
public:
	Parent()
	{
		cout << "1";
	}

	~Parent()																				//BUG,Make this virtual
	{
		cout << "1";
	}
};

class Child : public Parent
{
public:
	Child()
	{
		cout << "3";
	}

	~Child()																					//Should be virtual,but will work
	{
		cout << "3";
	}

private:
	Something mDataMember;
};

int main()
{
	Parent* ptr = new Child();
	delete ptr;

	return 0;
}
代码的输出很短，是"1231"。当delete ptr时，只调用了Parent的析构函数，因为析构函数没有声明为virtual。结果是没有调用Child的析构函数，
也没有调用其数据成员的析构函数。

与构造函数一样,在析构函数中调用虚方法时,虚方法的行为将有所不同.如果派生类重写了基类中的虚方法,在基类的析构函数中调用该方法,会执行该
虚方法的基类实现,而不是派生类的重写版本.

************************************************************************************************************

使用父类方法：
在派生类中重写方法时，将有效地替换原始方法。然而，方法的父类版本仍然存在，仍然可以使用这些方法。例如，某个重写方法可能除了完成父类实现完成
的任务之外，还会完成一些其他任务。
class WeatherPrediction
{
public:
	virtual std::string getTemperature() const;
};

class MyWeatherPrediction : public WeatherPrediction
{
public:
	virtual std::string getTemperature() const override;
};

std::string MyWeatherPrediction::getTemperature() const
{
	//Note:\u00B0 is the ISO/IEC 10646 representation of the degree symbol.
	return getTemperature() + "\u00B0";																		//BUG
}
然而，这行代码无法运行，根据C++的名称解析规则，首先解析局部作用域，然后是类作用域，根据这个顺序，函数中调用的是MyWeatherPrediction::getTemperature()。
其结果是无限递归，直到耗尽堆栈空间（某些编译器在编译时，会发现这种错误并报错）。
为了让代码运行，需要使用作用域解析运算符：
std::string MyWeatherPrediction::getTemperature() const
{
	return WeatherPrediction::getTemperature() + "\u00B0";
}
注意：Microsoft Visual C++支持__super关键字（两条下划线）。这个关键字允许编写如下代码：
return __super::getTemperature() + "\u00B0";
在C++中，调用当前方法的父类版本是一种常见的操作。如果存在派生类链，每个派生类都可能想执行基类中已经定义的操作，同时添加自己的附加功能。
另一个例子：
class Book
{
public:
	virtual ~Book()
	{
	}

	virtual string getDescription() const
	{
	return "Book";
	}

	virtual int getHeight() const
	{
	return 120;
	}
};

class Paperback : public Book
{
public:
	virtual string getDescription() const override
	{
		return "Paperback " + Book::getDescription();
	}
};

class Romance : public Paperback
{
public:
	virtual string getDescription() const override
	{
		return "Romance " + Paperback::getDescription();
	}

	virtual int getHeight() const override
	{
		return Paperback::getHeight() / 2;
	}
};

class Technical : public Book
{
public:
	virtual string getDescription() const override
	{
		return "Technical " + Book::getDescription();
	}
};

int main()
{
	Romance novel;
	Book book;
	cout << novel.getDescription() << endl;																//Outputs "Romance Paperback Book"
	cout << book.getDescription() << endl;																//Outputs "Book"
	cout << novel.getHeight() << endl;																		//Outputs "60"
	cout << book.getHeight() << endl;																		//Outputs "120"

	return 0;
}
Book类定义了一个virtual getHeight()方法，返回120。只有Romance类重写了这个方法：调用了父类Paperback的getHeight()，然后将结果除以2，如下所示：
virtual int getHeight() const override
{
	return Paperback::getHeight() / 2;
}
然而，Paperback没有重写getHeight()，因此C++会沿着类层次结构向上寻找实现了getHeight()的类。在前面的示例中，Paperback::getHeight()将接卸为：
Book::getHeight()。

************************************************************************************************************

向上转型和向下转型:
对象可以转换为父类对象,或者赋值给父类.如果类型转换或赋值是对某个普通对象执行,会产生截断.然而,如果用派生类对基类的指针或者引用赋值,不
会产生截断.
如果打算进行向下转型,应该使用dynamic_cast,以使用对象内建的类型信息,拒绝没有意义的类型转换.如果针对某个指针的dynamic_cast失败,这个
指针的值就是nullptr,而不是指向某个无意义的数据.如果针对对象引用的dynamic_cast失败,将抛出std::bad_cast异常.仅在必要的情况下才使用向
下转型,一定要使用dynamic_cast.

************************************************************************************************************

纯虚方法和抽象基类
因为有了纯虚方法，所以这个类是抽象基类，因为是抽象基类，所以无法构建这种类型的对象。
抽象基类，可以定义构造函数作为占位符用，但是virtual析构函数是必须的，因为它是一个基类。

如果派生类没有实现从父类继承的所有纯虚方法,那么派生类也是抽象的.

************************************************************************************************************

修改方法的返回类型:
在C++中,如果原始的返回类型是某个类的指针或者引用,重写的方法可以将返回类型改为派生类的指针或者引用.这种类型称为协变返回类型.如果基类
和派生类处于平行层次结构中,使用这个特性可以带来方便.平行层次结构是指,一个层次结构与另一个类层次结构没有相交,但是存在联系.例如:
class Super_A
{
	...
};

class Sub_A : public Super_A
{
	...
};

class Super_B
{
public:
	virtual Super_A func();
};

class Sub_B : public Super_B
{
public:
	virtual Sub_A func() override;
};

************************************************************************************************************

修改方法的参数:
如果在派生类的定义中使用父类虚方法的名称,但参数与父类中同名方法的参数不同,那么这不是重写父类的方法,而是创建一个新方法.
class Super
{
public:
	Super();
	virtual void someMethod();
};

class Sub : public Super
{
public:
	Sub();
	virtual void someMethod(int i);																		//Compiles,but doesn'st override

	virtual void someOtherMethod();
};

void Sub::someMethod(int i)
{
	cout << "This is Sub's version of someMethod with argument" << i << "." << endl;
}
上面的类定义可以编译，但没有重写someMethod()方法。因为参数不同，所创建的是一个只存在于Sub中的新方法。如果需要someMethod()方法采用int参数，
并且只将这个方法应用于Sub类对象，上面的代码没有问题。
实际上，C++标准指出，当Sub定义了这个方法时，原始的方法被隐藏。线面的代码无法编译，因为没有参数的someMethod()方法不再存在：
Sub mySub;
mySub.someMethod();																							//Error,Won't compile because original method is hidden
如果希望重写基类中的someMethod()方法，就应使用override指定符标记Sub的someMethod(int)声明。这样编译器就会报错，指出someMethod(int)没有重写
基类中的方法。
可以使用using关键字显式地在派生类中包含这个方法的基类定义:
class Super
{
public:
	Super();
	virtual void someMethod();
};

class Sub : public Super
{
public:
	Sub();
	using Super::someMethod;																			//Explicitly "inherits" the Super version
	virtual void someMethod(int i);																		//Adds a new version of someMethod

	virtual void someOtherMethod();
};
注意：派生类的方法与基类方法同名但参数不同的情况很少见。

************************************************************************************************************

override关键字：
有时候，可能会偶然创建一个新的虚方法，而不是重写基类的方法。下面的Super和Sub类，其中Sub类正确地重写了someMethod()：
class Super
{
public:
	Super();

	virtual void someMethod(double d);
};

class Sub : public Super
{
public:
	Sub();

	virtual void someMethod(double d);
};
可以通过引用调用someMethod()：
Sub mySub;
Super& ref = mySub;
ref.someMethod(1.1);																				//Calls Sub's version of someMethod()
代码正确地调用Sub类重写的someMethod()。现在假定重写someMethod()时，使用整数（而不是双精度）做参数，如下所示：
class Sub : public Super
{
public:
	Sub();

	virtual void someMethod(int i);
};
我们知道这些代码没有重写someMethod()，而是创建了一个新的虚方法。如果试图像下面的代码那样通过引用调用someMethod()，将会调用Super的someMethod()而不是
Sub中定义的那个方法。
Sub mySub;
Super& ref = mySub;
ref.someMethod(1.1);																				//Calls Super's version of someMethod()
如果修改了Super类但忘记更新所有派生类，就会发生这类问题，这样实际上就是创建了一个新的虚方法，而不是正确地重写了这个方法。
可以用override关键字避免这种情况：
class Sub : public Super
{
public:
	Sub();

	virtual void someMethod(int i) override;
};
Sub的定义将导致编译器错误，因为override关键字表明，重写Super类的someMethod()方法，但在Super类中的someMethod()方法只接收双精度参数，而不接受整数。
重命名基类中的某个方法，但忘记重命名派生类中的重写方法时，就会出现上述问题。
建议只要想重写基类方法，就使用这个关键字。

************************************************************************************************************

继承的构造函数
可以在派生类中使用using关键字显式地包含基类中定义的方法。这适用于普通类方法，也适用于构造函数，允许在派生类中继承基类的构造函数。
class Super
{
public:
	Super(const std::string& str);
};

class Sub : public Super
{
public:
	Sub(int i);
};
只能使用Super提供的构造函数构建Super对象，这个构造函数需要一个字符串参数。另一方面，只能用Sub的构造函数创建Sub对象，这个构造函数需要一个
整数作为参数。不能使用Super类中接收字符串的构造函数创建Sub对象。例如：
Super super("Hello");																	//OK,calls string based Super ctor
Sub sup1(1);																				//OK,calls integer based Sub ctor
Sub sub2("Hello");																		//Error,Sub does not inherit string Super ctor
如果想要使用基于字符串的Super构造函数构建Sub对象，可在Sub类中显式地继承Super构造函数，如下所示：
class Sub : public Super
{
public:
	using Super::Super;
	Sub(int i);
};
现在可通过下面两种方法构建Sub对象：
Sub sup1(1);																				//OK,calls integer based Sub ctor
Sub sub2("Hello");																		//OK,calls inherited string based Super ctor
Sub类定义的构造函数可以与从Super类继承的一个构造函数有相同的参数列表。与所有的重写一样，此时Sub类的构造函数优先级高于继承的构造函数。在下面
的示例中，Sub类使用using关键字继承了Super类的所有构造函数。然而，由于Sub类定义了一个使用浮点数做参数的构造函数，从Super继承的采用浮点数做
参数的构造函数被重写。
class Super
{
public:
	Super(const std::string& str);
	Super(float f);
};

class Sub : public Super
{
public:
	using Super::Super;
	Sub(float f);																			//Overrides inherited float based Super ctor
};
根据这个定义，可以用下面的代码创建Sub对象：
Sub sub1("Hello");																		//OK,calls inherited string based Super ctor
Sub sub2(1.23f);																		//OK,calls float based Sub ctor
使用using子句从基类继承构造函数有一些限制。
1，当从基类继承构造函数时，会继承全部的构造函数，而不可能只继承基类的部分构造函数。
2，第二个限制与多重继承有关。如果一个基类的某个构造函数与另一个基类的构造函数具有相同的参数列表，就不可能从基类继承构造函数，因为那样会导致歧义。为解决
这个问题，Sub类必须显式地定义冲突的构造函数。
class Super1
{
public:
	Super1(float f);
};

class Super2
{
public:
	Super2(const std::string& str);
	Super2(float f);
};

class Sub : public Super1,public Super2
{
public:
	using Super1::Super1;
	using Super2::Super2;
	Sub(char c);
};
Sub中的第一条using语句继承了Super1的构造函数。这意味着Sub具有了如下构造函数：
Sub(float f);																					//Inherited from Super1
Sub中的第二条using语句试图继承Super2的全部构造函数。然而，这会导致编译器错误，因为这意味着Sub拥有第二个Sub(float f)构造函数。为了解决这个问题，可
在Sub类中显式声明冲突的构造函数，如下所示：
class Sub : public Super1,public Super2
{
public:
	using Super1::Super1;
	using Super2::Super2;
	Sub(char c);
	Sub(float f);
};
现在，Sub类显式地声明了一个采用浮点数类型做参数的构造函数，从而解决了歧义问题。如果愿意，在Sub类中显式声明的用浮点数做参数的构造函数仍然可以在初始化器
中调用Super1和Super2的构造函数，如下所示：
Sub::Sub(float f) :
	Super1(f),
	Super2(f)
{
}
当使用继承的构造函数时，要确保所有的成员变量都正确地初始化。例如，考虑下面的Super和Sub类的新定义。这个示例没有正确地初始化mInt数据成员，在任何情况下这
都是一个严重错误：
class Super
{
public:
	Super(const std::string& str) :
		mStr(str)
	{
	}

private:
	std::string mStr;
};

class Sub : public Super
{
public:
	using Super::Super;
	Sub(int i) :
		Super(""),
		mInt(i)
	{
	}

private:
	int mInt;
};
可采用如下方法创建一个Sub对象：
Sub s1(2);
这条语句将调用Sub(int i)构造函数，这个构造函数将初始化Sub类的mInt数据成员，并调用Super构造函数，用空字符串初始化mStr数据成员。
由于Sub类继承了Super的构造函数，还可以按照下面的方式创建一个Sub对象：
Sub s2("Hello");
这条语句调用从Super继承的构造函数。然而，从Super继承的构造函数只初始化了Super类的mStr成员变量，没有初始化Sub类的mInt成员变量，mInt处于未初始化状态。
通常不建议这么做。
解决方法是使用类内成员初始化器。下面的代码使用类内成员初始化器将mInt初始化为0。Sub(int i)构造函数仍然可以修改这一初始化，将mInt初始化为参数i的值。
class Sub : public Super
{
public:
	using Super::Super;
	Sub(int i) :
		Super(""),
		mInt(i)
	{
	}

private:
	int mInt = 0;
};

************************************************************************************************************

重写方法时的特殊情况:
1,静态基类方法
在C++中,不能重写静态方法.首先,方法不可能既是静态的又是虚的.如果派生类中存在的静态方法与基类中的静态方法同名,实际上这是两个独立的
方法.
class Super
{
public:
	static void func();
};

class Sub : public Super
{
public:
	static void func();
};
这两个方法毫无关系.
当用类访问这些方法时,如:Super::func(),Sub::func(),一切都很正常.当涉及到对象时,这一行为就不是那么明显了.在C++中,可以使用对象
调用静态方法,但由于方法是静态的,因此没有this指针,也无法访问对象本身,因此使用对象调用静态方法,等价于使用class_name::method()调用
静态方法.
Sub subObj;
Super& superRef = subObj;
subObj.func();
superRef.func();
func()的第一次调用显然调用了Sub的版本,因为调用它的对象显式地声明为了Sub.第二个调用,这个对象是一个Super引用,但指向的是一个Sub对象,
在此情况下,会调用Super版本的func().原因是当调用静态方法时,C++不关心对象实际是什么,只关心编译时的类型,而这里编译时的类型为Super
的引用.
静态方法属于定义它的类名称,而不属于特定的对象.当类中的方法调用静态方法时,所调用的版本是通过正常的名称解析来决定的.当使用对象调用时,
对象实际上并不涉及调用,只是用来判断编译时的类型.

2,重载基类方法
当指定名称和一组参数,以重写某个方法时,编译器隐式地隐藏基类中的同名方法的所有其他实例.其想法为:如果重写了给定名称的某个方法,可能是
想重写所有的同名方法,只是忘记这么做了,因此应该作为错误处理.例如下面的Sub类,它重写了一个方法,而没有重写相关的重载方法:
class Super
{
public:
	virtual void func()
	{
		cout << "super" << endl;
	}
	virtual void func(int);
};

class Sub : public Super
{
public:
	virtual void func() override
	{
		cout << "sub" << endl;
	}
};
如果试图用Sub对象调用以int做参数的函数版本,代码将无法编译,因为没有显式地重写这个方法:
Sub mySub;
mySub.func(42);		//错误
然而,使用Sub对象访问该版本的方法是可行的.只需要使用指向Super对象的指针或者引用:
Super* supPtr = &mySub;
supPtr->func(42);
如果只想改变一个方法,可以使用using关键字避免重载该方法的所有版本.在下面的代码中,Sub类定义使用了从Super继承的一个func()版本,并显式地
重写了另一个版本:
Class Sub : public Super
{
public:
	using Super::func;					//Sub类显式地说明:我将接收父类其他所有的重载方法.
	virtual void func() override
	{
		cout << "sub" << endl;
	}
};
using子句存在一定风险。假定在Super中添加了第三个overload()方法，本来应该在Sub中重写这个方法。但是由于使用了using子句，在派生类中没有
重写这个方法不会当作错误。
注意：为了避免歧义bug，应该重写重载方法的所有版本，可以显式重写，也可以使用using关键字，但要留意使用using关键字的风险。

3,重写基类private方法.
派生类无法调用父类的private方法,并不意味着无法重写这个方法.实际上，在C++中，重写private或者protected方法是一种常见模式。这种模式允许派生类
定义自己的“独特性”，在基类中会引用这种独特性。
class MilesEstimator
{
public:
	virtual int getMilesLeft() const
	{
		return getMilesPerGallon() * getGallonsLeft();
	}

	virtual void setGallonsLeft(int inValue)
	{
		mGallonsLeft = inValue;
	}

	virtual int  getGallonsLeft() const
	{
		return mGallonsLeft;
	}

private:
	int mGallonsLeft;
	virtual int getMilesPerGallon() const
	{
		return 20;
	}
};
getMilesLeft()方法根据两个方法的返回结果执行计算。下面的代码使用MilesEstimator计算2加仑汽油可以行驶的里程：
MilesEstimator myMilesEstimator;
myMilesEstimator.setGallonsLeft(2);
cout << "I can go " << myMilesEstimator.getMilesLeft() << " more miles." << endl;
代码的输出如下：
I can go 40 more miles.
我们可以引入不同类型的车辆。现有的MilesEstimator假定所有的汽车燃烧1加仑的汽油可以跑20英里，这个值是从一个单独的方法返回的，
因此派生类可以重写这个方法。如下：
class EfficientCarMilesEstimator : public MilesEstimator
{
private:
	virtual int getMilesPerGallon() const override
	{
		return 35;
	}
};
通过重写这个private方法,子类完全修改了没有更改的现有public方法的行为.基类中的getMilesLeft()方法将自动调用private getMilesPerGallon()方法的重写版本.
EfficientCarMilesEstimator myEstimator;
myEstimator.setGallonsLeft(2);
cout << "I can go " << myEstimator.getMilesLeft() << " more miles." << endl;
此时的输出表明了重写的功能：
I can go 70 more miles.
注意：重写private或者protected方法可以在不做重大改动的情况下改变类的某些特性.

4,基类方法具有默认参数
派生类在重写的方法中提供了不同的默认参数.
class Super
{
public:
	virtual void func(int i = 2);
};

class Sub : public Super
{
public:
	virtual void func(int i = 7) override;
};
如果Sub对象调用func()将执行Sub版本的func(),默认参数为7.
如果Super对象调用func(),将执行Super版本的func(),默认参数为2.
然而,如果使用实际指向Sub对象的Super指针或者Super引用调用func(),将调用Sub版本的func(),但使用Super的默认参数2.
这种行为的原因是C++根据描述对象的表达式类型在编译时绑定默认参数,而不是根据实际的对象类型绑定参数.在C++中,默认参数不会被"继承".
当重写具有默认参数的方法时,也应该提供默认参数,这个参数的值应该与基类版本相同.建议使用符号常量做默认值，这样可以在派生类中使用同一个
符号常量。

5,派生类方法具有不同的访问级别
在派生类中重新定义访问限制:
class Super
{
public:
	virtual void func()
	{
		cout << "super" << endl;
	}
};

class Sub : public Super
{
protected:
	virtual void func() override
	{
		cout << "sub" << endl;
	}
};
Sub类中将基类的func()方法重写为protected版本.任何客户代码视图使用Sub对象调用func()都会导致编译错误:
Sub mySub;
mySub.func();																										//错误,尝试访问protected方法
然而,这个方法并不是完全protected的.可以使用Super引用或者指针访问被认为是protected的方法:
Super& ref = mySub;
ref.func();
将输出:sub
这说明在派生类中将方法设置为protected实际上是重写了这个方法(因为可以正确地调用这个方法的派生类版本).
注意:无法(也没有很好的理由)限制访问父类的public方法.

在派生类中放宽访问限制是比较容易(也更有意义)的.最简单的方法是提供一个public方法调用基类的protected方法:
class Super
{
protected:
	virtual void func_protected()
	{
		cout << "hello" << endl;
	}
};

class Sub : public Super
{
public:
	virtual void func()
	{
		func_protected();
	}
};
调用Sub对象func()方法的客户代码可以有效地访问Super类的protected方法.当然,这并没有真正改变func_protected()的访问级别,只是提供了访问
这个方法的public方式.
也可以在Sub派生类中显式地重写func_protected(),并将这个方法设置为public访问:
class Super
{
protected:
	virtual void func()
	{
		cout << "hello" << endl;
	}
};

class Sub : public Super
{
public:
	virtual void func() override
	{
		cout << "hello world" << endl;
	}
};
如果使用Sub对象调用func()方法,将输出hello world
mySub.func();																													//hello world
然而,在此情况下,基类中的func()方法仍然是protected的,因为使用指针或者引用调用Super的func()方法将无法编译:
Super& ref = mySub;
Super* ptr = &mySub;
ref.func();																															//错误,尝试访问protected方法
ptr->func();																														//错误,尝试访问protected方法
注意:修改方法访问级别的唯一真正有用的方式是对protected方法提供较为宽松的访问限制.

************************************************************************************************************

派生类中的复制构造函数和赋值运算符
我们知道,当在类中使用了动态内存分配时,提供复制构造函数和赋值运算符是一个好的变成习惯.当定义派生类时,必须注意复制构造函数和operator=.
如果派生类没有任何需要使用非默认复制构造函数或者operator=的特殊数据(通常是指针),无论基类是否有这样的数据,都不需要它们.如果派生类
省略了复制构造函数或者operator=,派生类中的数据成员就使用默认的复制构造函数或者operator=,基类中的数据成员使用基类的复制构造函数或者
operator=.
另一方面,如果在派生类中指定了复制构造函数,就需要显式地链接到父类的复制构造函数.如果不这么做,将使用默认构造函数(不是复制构造函数)初
始化对象的父类部分.
class Super
{
public:
	Super();
	Super(const Super& sup);
};
class Sub : public Super
{
public:
	Sub();
	Sub(const Sub& sub);
};
Sub::Sub(const Sub& sub) :
	Super(sub)
{
}
与此类似,如果派生类重写了operator=,则需要调用父类版本的operator=.
Sub& Sub::operator=(const Sub& sub)
{
	if(this == &sub)
	{
		return *this;
	}

	Super::operator=(sub);			//调用父类的operator=

	return *this;
}

************************************************************************************************************

virtual的真相:
我们曾经说过只有虚方法才能正确地重写.使用定语"正确"的原因是如果方法不是虚的,也可以试着重写这个方法,但是这样做会导致微秒的错误.
1,隐藏而不是重写:
下面的代码显示了一个基类和一个派生类，每个类都有一个方法。派生类试图重写基类的方法，但是在基类中没有将这个方法声明为虚的。
class Super
{
public:
	void func()
	{
		cout << "super" << endl;
	}
};
class Sub : public Super
{
public:
	void func()
	{
		cout << "sub" << endl;
	}
};
试着用Sub对象调用func()方法好像没有问题:
Sub mySub;
mySub.func();																							//输出sub
然而,由于这个方法不是虚的,因此实际上没有被重写.相反,Sub类创建了一个新的方法,名称也是func(),这个方法与Super类的func()方法完全没有
关系.为证实这一点,只需用Super的指针或者引用调用这个方法:
Super& ref = mySub;
ref.func();																									//输出super
这是因为ref变量是一个Super引用,并且省略了virtual关键字.当调用func()方法时,只是执行了Super的func()方法.由于不是虚方法,就不需要考虑
派生类是否重写了这个方法.
注意:试图重写非虚方法将"隐藏"基类定义的方法,并且这个重写的方法只能在派生类环境中使用.

2,如果实现virtual
为了理解如何避免隐藏方法,需要了解virtual关键字的真正作用.在C++编译类时,会创建一个包含类中所有方法的二进制对象.在非虚情况下,将控制
交给正确方法的代码是硬编码,此时会根据编译时的类型调用方法.
如果方法声明为virtual,会使用名为虚表(vtable)的特定内存区域调用正确的实现.每个具有一个或者多个虚方法的类都有一张虚表,这种类的每个对象
都包含指向虚表的指针,这个虚表包含了指向虚方法实现的指针.通过这种方法,当使用某个对象调用方法时,指针也进入虚表,然后根据实际的对象类型执
行正确版本的方法.

注意:对于当今的CPU而言,"虚",对性能的影响可以用十亿份之一秒来度量,将来的CPU会使时间进一步缩短.在多数应用程序中,无法察觉到使用虚方法
和不使用虚方法带来的性能差别,因此应该将所有方法声明为virtual,包括析构函数.构造函数不需要也无法声明为virtual，因为在创建对象时，总会明确
地指定类。

唯一允许不把析构函数声明为virtual的例外情况是，类标记为final。

************************************************************************************************************

运行时类型工具
RTTI(Run Time Type Information)提供了许多有用的特性,来判断对象所属的类.
其中一种特性是dynamic_cast,可以在OO层次结构中进行安全的类型转换.如果在没有虚方法的类上使用dynamic_cast,会导致编译错误.
第二个特性是typeid运算符,这个运算符可以在运行时查询对象,从而判别对象的类型.类至少有一个虚方法,typeid运算符才能正常运行.typeid运算符
的主要价值之一在于日志和调试.

************************************************************************************************************

非public继承
将派生类与父类的关系声明为protected,意味着在派生类中,父类所有的public方法和数据成员都成为protected的.与此类似,指定private访问意味着
父类所有的public,protected方法和数据成员在派生类中都成为private的.
使用这种方法同一降低父类的访问级别有许多原因，但多数原因都是层次结构的设计缺陷。有时候会滥用这一语言特性，经常与多重继承一起实现类的
“组件”。

************************************************************************************************************

虚基类
class Animal
{
public:
	virtual void eat() = 0;
	virtual void sleep()
	{
		cout << "zzz..." << endl;
	}
};

class Dog : public virtual Animal
{
public:
	virtual void bark()
	{
		cout << "woof!" << endl;
	}

	virtual void eat() override
	{
		cout << "The dog has eaten." << endl;
	}
};

class Bird : public virtual Animal
{
public:
	virtual void chirp()
	{
		cout << "chirp!" << endl;
	}

	virtual void eat() override
	{
		cout << "The bird has eaten." << endl;
	}
};

class DogBird : public Dog, public Bird
{
public:
	virtual void eat() override
	{
		Dog::eat();
	}
};

int main()
{
	DogBird myConfusedAnimal;
	myConfusedAnimal.sleep();																	//Not ambiguous because Animal is virtual

	return 0;
}
Animal被作为虚基类，DogBird就只有Animal的一个子对象，因此调用sleep()就不存在歧义。
注意：虚基类是在类层次结构中避免歧义的好办法。
*/

/*
内存管理：
使用相同形式的new和delete：
typedef string AddressLines[4];
由于AddressLines是个数组，如果这样使用new：
string* pal = new AddressLines;																//传回一个string*，就像new string[4]一样
就必须匹配“数组形式”的delete：
delete pal;																								//行为未定义
delete[] pal;																							//OK
为了避免诸如此类的错误，最好尽量不要对数组型别做typedef动作。

记得在destructor中以delete对付pointer members：
在class中，每加上一个pointer member时，几乎总是需要配合以下每一件事情：
1，在每一个constructors中将该指针初始化。如果没有任何一个constructor会将内存配置给该指针，那么指针应该初始化为nullptr。
2，在assignment运算符中将指针原有的内存删除，重新配置一块。
3，在destructor中delete这个指针。
注意：删除一个nullptr指针是安全的（什么也没做）。因此，如果你的constructors，assignment运算符，以及其它member functions都使class的每一个pointer member
“要么指向有效内存，要么就是nullptr”，那么你可以放心大胆地在destructor中将它们delete掉，不需在意是否曾经对此指针使用过new。（也就是说，即便没有在构造函数中
给某个指针new出一块内存空间，也可以毫无顾忌的在析构函数中delete这个指针）。
注意：绝对不要delete一个传递而来的指针。换句话说，你的析构函数通常不应该使用delete，除非你的class members正是当初使用new的人。（也就是说，你只能，只应该
在你的析构函数中delete属于你自己的指针，而绝对不能delete别人的指针，那么别人的某个指针曾经传递进了你的类里面）。

为内存不足的状况预做准备：
我们会要求当内存需求无法获得满足时，让系统调用你所指定的一个函数。这样的策略仰赖一个公约：当operator new无法满足需求时，它会在抛出异常之前先调用一个client
专属的错误处理函数，此函数通常称为new-handler。
为了指定这个所谓的“内存不足处理函数，new-handler”，clients必须调用set_new_handler，这是头文件<new>提供的一个函数，用法如下：
typedef void (*new_handler)();
new_handler set_new_handler(new_handler p) throw();
new_handler是个typedef，表现出一个函数指针，该函数没有参数也没有传回值。而set_new_handler是个函数，需要一个new_handler参数并传回一个new_handler。
set_new_handler的参数是个指针，指向的函数正是当operator new无法配置足够内存时，应该去调用的函数。set_new_handler的传回值是一个函数指针，指向先前登
录过的new-handler。
当operator new无法满足内存需求时，它会不止一次地调用new-handler函数，它会不断地调用，直到找到足够的内存为止。
一个设计良好的new-handler函数必须完成以下事情之一：
a，让更多内存可用。这或许能够让operator new的下一次内存配置行为成功。实现此策略的方法之一就是在程序起始时配置一大块内存，然后在new-handler第一次被调用
时释放之。如此的释放动作常常伴随某种警告信息，告诉用户目前的内存已经处于低水位，再来的内存需求可能会失败，除非有更多内存恢复自由身。
b，安装一个不同的new-handler。如果目前的new-handler无法让更多内存可用，或许它知道另一个new-handler手上握有比较多的资源。果真如此，目前的new-handler
就可以安装另一个new-handler以取代自己（只要再调用一次set_new_handler即可）。当operator new下次调用new-handler函数时，它会调用最新安装的那个。（此
旋律的一个变奏是，让new-handler修改自己的行为，如此一来，当它下次再被调用时，行为便不相同。完成此事的方法之一就是令new-handler修改会影响它自己行为的
那些static资料或global资料）
c，卸除这个new-handler，也就是说，将nullptr指针传给set_new_handler。一旦没有安装任何new-handler，operator new就会在内存配置失败时抛出一个型别为
std::bad_alloc的异常。
d，抛出一个异常，型别为std::bad_alloc（或其派生型别）。这样的异常不会被operator new捕捉，所以它们会传送到最初提出内存需求的那个点上。
e，不回返，直接调用abort或exit。
解释class D : public B<D>：
此设计之base class部分，让derived classes继承它们需要的set_new_handler和operator new函数。
此设计之template部分，确保每一个继承而来的class拥有一个不同的static data member。
过去的标准是当operator new无法满足内存需求时传回0，现在是抛出一个std::bad_alloc型别的异常。为了兼容过去的规则，所以现在依然保留了“nothrow”形式。

撰写operator new和operator delete时应遵行的公约：
有一点相当重要：你的函数的行为应该与缺省的operator new保持一致。
C++标准要求，即使用户要求的是0字节内存，operator new也应该传回一个合法的指针。
C++保证，删除一个nullptr指针永远是安全的。

如果你写了一个operator new，请对应写一个operator delete：
如果被删除的对象系派生自一个缺乏virtual destructor的base class，那么C++传递给operator delete的size_t数值可能是不正确的。这个理由足以让你永远不会忘记为
你的base classes加上一个virtual destructor。
Memory leak问题是在“配置内存后，所有指向该内存的指针都遗失了”时才发生。
*/

/*
声明	非虚拟函数			的目的:是为了让派生类	继承接口				和					实现.
声明	一般虚函数			的目的:是为了让派生类	继承接口				和					缺省行为.
声明	纯虚函数				的目的:是为了让派生类	只继承接口.
*/

/*
继承关系与面向对象设计：
确定你的public inheritance，模塑出“isa”的关系：
以C++完成面向对象程序设计，最重要的一个规则是：public inheritance（公开继承）意味着“是一种（isa）”的关系。把这个规则牢牢地烙印在你的心中！
class Student : public Person
“Student是一种Person”这个事实并不意味着“Student数组是一种Person数组”。
在正方形和矩形的例子中，以public inheritance来模塑它们之间的关系是错误的。

区分接口继承（interface inheritance）和实现继承（implementation inheritance）：
最明确而直接易懂的继承概念（也就是public inheritance），但是你会发现它分为两类：函数接口的继承和函数实现的继承。
a，声明一个纯虚函数的目的是为了让derived classes只继承其接口。
我们可以为纯虚函数提供定义，不过调用它的唯一途径是“指定其完整的class名称”（此即所谓的静态调用）。
b，声明一般（非纯）虚函数的目的，是为了让derived classes继承该函数的接口和缺省行为。
有些人反对以不同的函数分别提供接口和缺省行为，我们可以利用“纯虚函数必须在subclasses中重新声明，但是纯虚函数也可以拥有自己的定义”这个事实。
c，声明非虚函数的目的是为了令derived classes继承函数的接口及其实现。

绝对不要重新定义继承而来的非虚函数：
非虚函数如B::f和D::f都是静态绑定。意思是由于pB的型别是pointer-to-B，通过pB调用的非虚函数总是B所定义的版本，纵使pB指向一个型别为“B所派生之class”
的对象。

绝对不要重新定义继承而来的缺省参数值：
对象的静态型别，是程序声明它时所采用的型别。
对象的动态型别，是对象目前所代表（参考到）的型别。
虚函数是动态绑定，而缺省参数值却是静态绑定。意思是你可能会在调用一个定义于derived class内的虚函数的同时，却使用base class为它所指定的缺省参数值。

避免在继承体系中做向下转型（cast down）动作：
向下转型可以以数种方法消除之。最佳办法就是将转型动作以虚函数的调用取代，并且让每一个虚函数有一个“无任何动作”的缺省实现代码，以便应用在并不想要施行
该虚函数的任何classes身上。

通过layering技术来模塑has-a或is-implemented-in-terms-of的关系：
所谓layering，是以一个class为本，建立另一个class，并令所谓layering class（外层）内含所谓layered class（内层）的对象作为data member。

区分inheritance和templates：
型别T会影响class的行为吗？
如果不影响，你可以使用template。
如果影响，你就必须使用虚函数，并因而使用继承机制。
template应该用来产生一群classes，其中对象型别不会影响class的函数行为。
inheritance应该用于一群classes身上，其中对象型别会影响class的函数行为。

明智地运用private inheritance（私有继承）：
公有继承的话，编译器会在必要的时候（为了让函数调用成功）将D隐式转换为B。
私有继承，编译器通常不会自动将一个子类对象转换为一个基类对象。
私有继承而来的所有成员，在子类中都会变成private属性，纵使它们在基类中原本是protected或public属性。
private inheritance意味着“根据某物实现（implemented-in-terms-of）”。如果你让D以private方式继承B，这么做的原因是，你想采用已经撰写于class B内的某些代码，
而不是因为B和D的对象有任何概念关系存在。所以private inheritance纯粹只是一种实现上的技术，与观念无关。private inheritance意味着只有实现部分被继承，接口部分
应略去。如果D以private方式继承B，意思是D对象根据B对象实现而得，再没有其他意思了。private inheritance在软件设计层面上并没有意义，其意义只在于软件实现层面。
layering的意义也是这样。你如何在两者之间取舍呢？答案很简单：尽可能使用layering，必要时才使用private inheritance。何为“必要”？即当protected members和/或
虚函数牵扯进来的时候。

说出你的意思并了解你所说的每一句话：
1，共同的base class意味着共同的特性。
2，公有继承意味着“是一种（isa）”。
3，私有继承意味着“根据某物实现”。
4，Layering意味着“有一个（has-a）”或“根据某物实现”。
公有继承时，下面的才成立：
5，纯虚函数意味着：只有其函数接口会被继承。如果B声明了一个纯虚函数f，则D必须继承f的接口，（设D为具象子类）必须提供自己的实现代码。
6，一般（非纯）虚函数意味着：函数的接口及缺省实现代码都会被继承。如果B声明了一个一般虚函数f，D必须继承f的接口，也可以继承f的缺省实现代码。
7，非虚函数意味着：此函数的接口和其实现代码都会被继承。如果B声明了一个非虚函数f，D必须同时继承f的接口和实现代码。
*/

/*
C++程序中的存储空间可以分为静态/全局存储区，以及栈区和堆区。
静态/全局存储区和栈区的大小一般在程序编译阶段决定。
C++的运行库提供了默认的全局new/new[]和delete/delete[]的实现。
在32位的Windows系统中，一个进程可以访问的内存空间是4GB，但可以用来动态分配的最大内存是2GB。
*/

/*
 继承和动态绑定对程序的编写有两方面的影响:
 1:我们可以更容易地定义与其他类相似但不完全相同的新类.
 2:在使用这些彼此相似的类时,我们可以在一定程度上忽略掉它们的区别,而以统一的方式使用它们的对象.

 C++11新标准允许派生类显式地注明它将使用哪个成员函数改写基类的虚函数,具体措施是在该函数的形参列表之后增加一个override(覆盖)
 关键字.

 函数的运行版本由实参决定,即在运行时选择函数的版本,所以动态绑定有时又被称为运行时绑定(run-time binding).
 记住:在C++语言中,当我们使用基类的引用(或指针)调用一个虚函数时将发生动态绑定.

 基类通过在其成员函数的声明语句之前加上关键字virtual使得该函数执行动态绑定.任何构造函数之外的非静态函数都可以是虚函数.关键字
 virtual只能出现在类内部的声明语句之前而不能用于类外部的函数定义.如果基类把一个函数声明成虚函数,则该函数在派生类中隐式地也是虚
 函数.

 成员函数如果没被声明为虚函数,则其解析过程发生在编译时而非运行时.

 访问说明符的作用是控制派生类从基类继承而来的成员是否对派生类的用户可见.
 如果一个派生是公有的,则基类的公有成员也是派生类接口的组成部分.此外,我们能将公有派生类型的对象绑定到基类的引用或指针上.

 一个派生类对象包含多个组成部分:一个含有派生类自己定义的(非静态)成员的子对象,以及一个与该派生类继承的基类对应的子对象,如果有
 多个基类,那么这样的子对象也有多个.
 C++标准并没有明确规定派生类的对象在内存中如何分布.
 因为在派生类对象中含有与其基类对应的组成部分,所以我们能把派生类的对象当成基类对象来使用,而且我们也能将基类的指针或引用绑定到派
 生类对象中的基类部分上.
 这种转换通常称为派生类到基类的(derived-to-base)类型转换.和其他类型转换一样,编译器会隐式地执行派生类到基类的转换.
 这种隐式特性意味着我们可以把派生类对象或者派生类对象的引用用在需要基类引用的地方;同样的,我们也可以把派生类对象的指针用在需要基
 类指针的地方.
 记住:在派生类对象中含有与其基类对应的组成部分,这一事实是继承的关键所在.

 尽管在派生类对象中含有从基类继承而来的成员,但是派生类并不能直接初始化这些成员.和其他创建了基类对象的代码一样,派生类也必须使
 用基类的构造函数来初始化它的基类部分.
 记住:每个类控制它自己的成员初始化过程.
 派生类对象的基类部分与派生类对象自己的数据成员都是在构造函数的初始化阶段执行初始化操作的.类似于我们初始化成员的过程,派生类构造
 函数同样是通过构造函数初始化列表来将实参传递给基类构造函数的.
 记住:首先初始化基类的部分,然后按照声明的顺序依次初始化派生类的成员.

 派生类可以访问基类的公有成员和受保护成员.
 派生类的作用域嵌套在基类的作用域之内.因此,对于派生类的一个成员来说,它使用派生类成员的方式与使用基类成员的方式没什么不同.
 关键概念:遵循基类的接口
 必须明确一点:每个类负责定义各自的接口.要想与类的对象交互必须使用该类的接口,即使这个对象是派生类的基类部分也是如此.
 因此,派生类对象不能直接初始化基类的成员.尽管从语法上来说我们可以在派生类构造函数体内给它的公有或受保护的基类成员赋值,但是最
 好不要这么做.和使用基类的其他场合一样,派生类应该遵循基类的接口,并且通过调用基类的构造函数来初始化那些从基类中继承而来的成员.

 继承与静态成员:
 如果基类定义了一个静态成员,则在整个继承体系中只存在该成员的唯一定义.不论从基类中派生出来多少个派生类,对于每个静态成员来说都
 只存在唯一的实例.
 class Base
 {
 public:
	static void statmem();
 };
 class Derived : public Base
 {
	void f(const Derived&);
 };
 静态成员遵循通用的访问控制规则,如果基类中的成员是private的,则派生类无权访问它.假设某静态成员是可访问的,则我们既能通过基类使
 用它也能通过派生类使用它:
 void Derived::f(const Derived &derived_obj)
 {
	 Base::statmem();  //正确,Base定义了statmem
	 Derived::statmem(); //正确,Derived继承了statmem
	 //正确,派生类的对象能访问基类的静态成员
	 derived_obj.statmem(); //通过Derived对象访问
	 statmem();  //通过this对象访问
 }

 一个类是基类,同时它也可以是一个派生类:
 class Base
 {
	...
 };
 class D1 : public Base
 {
	...
 };
 class D2 : public D1
 {
	...
 };
 在这个继承关系中,Base是D1的直接基类(direct base),同时是D2的间接基类(indirect base).直接基类出现在派生列表中,而间接基类由
 派生类通过其直接基类继承而来.
 每个类都会继承直接基类的所有成员.对于一个最终的派生类来说,它会继承其直接基类的成员;该直接基类的成员又含有其基类的成员;以此
 类推直至继承链的顶端.因此,最终的派生类将包含它的直接基类的子对象以及每个间接基类的子对象.

 有时我们会定义这样一种类,我们不希望其他类继承它,或者不想考虑它是否适合作为一个基类.为了实现这一目的,C++11新标准提供了一种
 防止继承发生的方法,即在类名后跟一个关键字final:
 class NoDerived final			//NoDerived不能作为基类
 {
 };

 可以将基类的指针或引用绑定到派生类对象上有一层极为重要的含义:当使用基类的引用(或指针)时,实际上我们并不清楚该引用(或指针)所绑定
 对象的真实类型.该对象可能是基类的对象,也可能是派生类的对象.
 和内置指针一样,智能指针类也支持派生类向基类的类型转换,这意味着我们可以将一个派生类对象的指针存储在一个基类的智能指针内.

 静态类型与动态类型:
 当我们使用存在继承关系的类型时,必须将一个变量或其他表达式的静态类型(static type)与该表达式表示对象的动态类型(dynamic type)区分
 开来.
 表达式的静态类型在编译时总是已知的,它是变量声明时的类型或表达式生成的类型.动态类型则是变量或表达式表示的内存中的对象的类型.动
 态类型知道运行时才可知.
 如果表达式既不是引用也不是指针,则它的动态类型永远与静态类型一致.

 不存在从基类向派生类的隐式类型转换:
 之所以存在派生类向基类的类型转换是因为每个派生类对象都包含一个基类部分,而基类的引用或指针可以绑定到该基类部分上.
 一个基类的对象既可以以独立的形式存在,也可以作为派生类对象的一部分存在.
 编译器在编译时无法确定某个特定的转换在运行时是否安全,这是因为编译器只能通过检查指针或引用的静态类型来推断该转换是否合法.如果
 在基类中含有一个或多个虚函数,我们可以使用dynamic_cast请求一个类型转换,该转换的安全检查将在运行时执行.同样,如果我们已知某
 个基类向派生类的转换是安全的,则我们可以使用static_cast来强制覆盖掉编译器的检查工作.

 在对象之间不存在类型转换:
 派生类向基类的自动类型转换只对指针或引用类型有效,在派生类类型和基类类型之间不存在这样的转换.
 很多时候,我们确实希望将派生类对象转换成它的基类类型,但是这种转换的实际发生过程往往与我们期望的有所差别.
 请注意,当我们初始化或赋值一个类类型的对象时,实际上是在调用某个函数.当执行初始化时,我们调用构造函数,而当执行赋值操作时,我们
 调用赋值运算符.这些成员通常都包含一个参数,该参数的类型是类类型的const版本的引用.

 因为这些成员接受引用作为参数,所以派生类向基类的转换允许我们给基类的拷贝/移动操作传递一个派生类的对象.这些操作不是虚函数.当我
 们给基类的构造函数传递一个派生类对象时,实际运行的构造函数是基类中定义的那个,显然该构造函数只能处理基类自己的成员.类似的,如果
 我们将一个派生类对象赋值给一个基类对象,则实际运行的赋值运算符也是基类中定义的那个,该运算符同样只能处理基类自己的成员.
 当我们用一个派生类对象为一个基类对象初始化或赋值时,只有该派生类对象中的基类部分会被拷贝、移动或赋值,它的派生类部分将被忽略掉.

 对虚函数的调用可能在运行时才被解析
 当某个虚函数通过指针或引用调用时,编译器产生的代码直到运行时才能确定应该调用哪个版本的函数.被调用的函数是与绑定到指针或引用上的
 对象的动态类型相匹配的那一个.
 必须要搞清楚的一点是,动态绑定只有当我们通过指针或引用调用虚函数时才会发生.
 当我们通过一个具有普通类型(非引用非指针)的表达式调用虚函数时,在编译时就会将调用的版本确定下来.

 C++的多态性:
 我们把具有继承关系的多个类型称为多态类型,因为我们能使用这些类型的"多种形式"而无须在意它们的差异.引用或指针的静态类型与动态类型
 不同这一事实正是C++语言支持多态性的根本所在.
 当我们使用基类的引用或指针调用基类中定义的一个函数时,我们并不知道该函数真正作用的对象是什么类型,因为它可能是一个基类的对象也可
 能是一个派生类的对象.如果该函数是虚函数,则直到运行时才会决定到底执行哪个版本,判断的依据是引用或指针所绑定的对象的真实类型.
 另一方面,对非虚函数的调用在编译时进行绑定.类似的,通过对象进行的函数(虚函数或非虚函数)调用也是在编译时绑定.对象的类型是确定不
 变的,我们无论如何都不可能令对象的动态类型与静态类型不一致.因此,通过对象进行的函数调用将在编译时绑定到该对象所属类中的函数版本
 上.
 记住:当且仅当对通过指针或引用调用虚函数时,才会在运行时解析该调用,也只有在这种情况下对象的动态类型才有可能与静态类型不同.

 派生类中虚函数的返回类型也必须与基类函数匹配.该规则存在一个例外,当类的虚函数返回类型是类本身的指针或引用时,上述规则无效.也就
 是说,如果D由B派生得到,则基类的虚函数可以返回B*而派生类的对应函数可以返回D*,只不过这样的返回类型要求从D到B的类型转换是可访
 问的.

 final和override说明符:
 派生类如果定义了一个函数与基类中虚函数的名字相同但是形参列表不同,这仍然是合法的行为.编译器将认为新定义的这个函数与基类中原有的
 函数是相互独立的.这时,派生类的函数并没有覆盖掉基类中的版本.就实际的编程习惯而言,这种声明往往意味着发生了错误,因为我们可能原
 本希望派生类能覆盖掉基类中的虚函数,但是一不小心把形参列表弄错了.
 要想调试并发现这样的错误显然非常困难.在C++11新标准中我们可以使用override关键字来说明派生类中的虚函数.这么做的好处是在使得程
 序员的意图更加清晰的同时让编译器可以为我们发现一些错误,后者在编程实践中显得更加重要.如果我们使用override标记了某个函数,但该
 函数并没有覆盖已存在的虚函数,此时编译器将报错.
 我们还能把某个函数指定为final,如果我们已经把函数定义成final了,则之后任何尝试覆盖该函数的操作都将引发错误.
 final和override说明符出现在形参列表(包括任何const或引用修饰符)以及尾置返回类型之后.

 虚函数与默认实参:
 和其他函数一样,虚函数也可以拥有默认实参.如果某次函数调用使用了默认实参,则该实参由本次调用的静态类型决定.
 换句话说,如果我们通过基类的引用或指针调用函数,则使用基类中定义的默认实参,即使实际运行的是派生类中的函数版本也是如此.此时,传
 入派生类函数的将是基类函数定义的默认实参.如果派生类函数依赖不同的实参,则程序结果将与我们的预期不符.
 如果虚函数使用默认实参,则基类和派生类中定义的默认实参最好一致.

 回避虚函数的机制:
 在某些情况下,我们希望对虚函数的调用不要进行动态绑定,而是强迫其执行虚函数的某个特定版本.使用作用域运算符可以实现这一目的,例如
 下面的代码:
 //强行调用基类中定义的函数版本而不管baseP的动态类型到底是什么
 double undiscounted = baseP->Quote::net_price(42);
 该代码强行调用Quote的net_price函数,而不管baseP实际指向的对象类型到底是什么.该调用将在编译时完成解析.
 记住:通常情况下,只有成员函数(或友元)中的代码才需要使用作用域运算符来回避虚函数的机制.
 什么时候我们需要回避虚函数的默认机制呢?通常是当一个派生类的虚函数调用它覆盖的基类的虚函数版本时.在此情况下,基类的版本通常完成
 继承层次中所有类型都要做的共同任务,而派生类中定义的版本需要执行一些与派生类本身密切相关的操作.
 如果一个派生类虚函数需要调用它的基类版本,但是没有使用作用域运算符,则在运行时该调用将被解析为对派生类版本自身的调用,从而导致无
 限递归.

 我们也可以为纯虚函数提供定义,不过函数体必须定义在类的外部.也就是说,我们不能在类的内部为一个=0的函数提供函数体.

 派生类构造函数只初始化它的直接基类,每个类各自控制其对象的初始化过程.

 一个类使用protected关键字来声明那些它希望与派生类分享但是不想被其他公共访问使用的成员.
 1:和私有成员类似,受保护的成员对于类的用户来说是不可访问的.
 2:和公有成员类似,受保护的成语对于派生类的成员和友元来说是可访问的.
 3:派生类的成员或友元只能通过派生类对象来访问基类的受保护成员.派生类对于一个基类对象中的受保护成员没有任何访问特权.

 派生访问说明符对于派生类的成员(及友元)能否访问其直接基类的成员没什么影响.对基类成员的访问权限只与基类中的访问说明符有关.
 派生访问说明符的目的是控制派生类用户(包括派生类的派生类在内)对于基类成员的访问权限.

 派生类向基类转换的可访问性:
 派生类向基类的转换是否可访问由使用该转换的代码决定,同时派生类的派生访问说明符也会有影响.
 假定D继承自B:
 1:只有当D公有地继承B时,用户代码才能使用派生类向基类的转换;如果D继承B的方式是受保护的或者私有的,则用户代码不能使用该转换.
 2:不论D以什么方式继承B,D的成员函数和友元都能使用派生类向基类的转换;派生类向其直接基类的类型转换对于派生类的成员和友元来说永
 远是可访问的.
 3:如果D继承B的方式是公有的或者受保护的,则D的派生类的成员和友元可以使用D向B的类型转换;反之,如果D继承B的方式是私有的,则不
 能使用.

 不考虑继承的话,我们可以认为一个类有两种不同的用户:普通用户和类的实现者.
 其中,普通用户编写的代码使用类的对象,这部分代码只能访问类的公有(接口)成员;实现者则负责编写类的成员和友元的代码,成员和友元既能
 访问类的共有部分,也能访问类的私有(实现)部分.
 如果进一步考虑继承的话就会出现第三种用户,即派生类.基类把它希望派生类能够使用的部分声明成受保护的.普通用户不能访问受保护的成
 员,而派生类及其友元仍旧不能访问私有成员.
 和其他类一样,基类应该将其接口成员声明为公有的;同时将属于其实现的部分分成两组:一组可供派生类访问,另一组只能由基类及基类的友元
 访问.对于前者应该声明为受保护的,这样派生类就能在实现自己的功能时使用基类的这些操作和数据;对于后者应该声明为私有的.

 不能继承友元关系;每个类负责控制各自成员的访问权限.

 改变个别成员的可访问性:
 有时我们需要改变派生类继承的某个名字的访问级别,通过使用using声明可以达到这一目的:
 class Base
 {
 public:
	 std::size_t size() const
	 {
		return n;
	 }

protected:
	 std::size_t n;
 };
 class Derived : private Base			//注意:private继承
 {
public:
	 //保持对象尺寸相关的成员的访问级别
	 using Base::size;

protected:
	 using Base::n;
 };
 因为Derived使用了私有继承,所以继承而来的成员size和n(在默认情况下)是Derived的私有成员.然而,我们使用using声明语句改变了这些
 成员的可访问性.改变之后,Derived的用户将可以使用size成员,而Derived的派生类将能使用n.
 通过在类的内部使用using声明语句,我们可以将该类的直接或间接基类中的任何可访问成员(例如,非私有成员)标记出来.using声明语句中名
 字的访问权限由该using声明语句之前的访问说明符来决定.也就是说,如果一条using声明语句出现在类的private部分,则该名字只能被类的
 成员和友元访问;如果using声明语句位于public部分,则类的所有用户都能访问它;如果using声明语句位于protected部分,则该名字对于成
 员、友元和派生类是可访问的.
 记住:派生类只能为那些它可以访问的名字提供using声明.

 继承中的类作用域:
 每个类定义自己的作用域,在这个作用域内我们定义类的成员.当存在继承关系时,派生类的作用域嵌套在其基类的作用域之内.如果一个名字在
 派生类的作用域内无法正确解析,则编译器将继续在外层的基类作用域中寻找该名字的定义.
 派生类的作用域位于基类作用域之内这一事实可能有点儿出人意料,毕竟在我们的程序文本中派生类和基类的定义是相互分离开来的.不过也恰恰
 因为类作用域有这种继承嵌套的关系,所以派生类才能像使用自己的成员一样使用基类的成员.

 在编译时进行名字查找:
 一个对象、引用或指针的静态类型决定了该对象的哪些成员是可见的.即使静态类型与动态类型可能不一致(当使用基类的引用或指针时会发生这
 种情况),但是我们能使用哪些成员仍然是由静态类型决定的.

 和其他作用域一样,派生类也能重用定义在其直接基类或间接基类中的名字,此时定义在内层作用域(即派生类)的名字将隐藏定义在外层作用域
 (即基类)的名字.

 名字查找与继承:
 理解函数调用的解析过程对于理解C++的继承至关重要,假定我们调用p->mem()(或者obj.mem()),则依次执行以下4个步骤:
 1:首先确定p(或obj)的静态类型.因为我们调用的是一个成员,所以该类型必然是类类型.
 2:在p(或obj)的静态类型对应的类中查找mem.如果找不到,则依次在直接基类中不断查找直至到达继承链的顶端.如果找遍了该类及其基类
 仍然找不到,则编译器将报错.
 3:一旦找到了mem,就进行常规的类型检查以确认对于当前找到的mem,本次调用是否合法.
 4:假设调用合法,则编译器将根据调用的是否是虚函数而产生不同的代码:
 ---如果mem是虚函数且我们是通过引用或指针进行的调用,则编译器产生的代码将在运行时确定到底运行该虚函数的哪个版本,依据是对象的
 动态类型.
 ---反之,如果mem不是虚函数或者我们是通过对象(而非引用或指针)进行的调用,则编译器将产生一个常规函数调用.

 名字查找先于类型检查:
 声明在内层作用域的函数并不会重载声明在外层作用域的函数.因此,定义派生类中的函数也不会重载其基类中的成员.和其他作用域一样,如
 果派生类(即内层作用域)的成员与基类(即外层作用域)的某个成员同名,则派生类将在其作用域内隐藏该基类成员.即使派生类成员和基类成员的
 形参列表不一致,基类成员也仍然会被隐藏掉:
 struct Base
 {
	int memfcn();
 };
 struct Derived : Base
 {
	int memfcn(int);  //隐藏基类的memfcn
 };
 Derived d; Base b;
 b.memfcn();  //调用Base::memfcn
 d.memfcn(10);  //调用Derived::memfcn
 d.memfcn();  //错误,参数列表为空的memfcn被隐藏了
 d.Base::memfcn(); //正确,调用Base::memfcn
 Derived中的memfcn声明隐藏了Base中的memfcn声明.在上面的代码中前两条调用语句容易理解,第一个通过Base对象b进行的调用执行基
 类的版本;类似的,第二个通过d进行的调用执行Derived的版本;第三条调用语句有点特殊,d.memfcn()是非法的.
 为了解析这条调用语句,编译器首先在Derived中查找名字memfcn;因为Derived确实定义了一个名为memfcn的成员,所以查找过程终止.一
 旦名字找到,编译器就不再继续查找了.Derived中的memfcn版本需要一个int实参,而当前的调用语句无法提供任何实参,所以该调用语句是
 错误的.
 因为名字查找先于类型检查,所以我们可以理解为什么基类与派生类中的虚函数必须有相同的形参列表了.假如基类与派生类的虚函数接受的实
 参不同,则我们就无法通过基类的引用或指针调用派生类的虚函数了.

 虚析构函数:
 继承关系对基类拷贝控制最直接的影响是基类通常应该定义一个虚析构函数,这样我们就能动态分配继承体系中的对象了.
 当我们delete一个动态分配的对象的指针时将执行析构函数.如果该指针指向继承体系中的某个类型,则有可能出现指针的静态类型与被删除对
 象的动态类型不符的情况.
 如果基类的析构函数不是虚函数,则delete一个指向派生类对象的基类指针将产生未定义的行为.
 之前我们知道一条经验准则,即如果一个类需要析构函数,那么它也同样需要拷贝和赋值操作.基类的析构函数并不遵循上述准则,它是一个重要
 的例外.一个基类总是需要析构函数,而且它能将析构函数设定为虚函数.此时,该析构函数为了成为虚函数而令内容为空,我们显然无法由此
 推断该基类还需要赋值运算符或拷贝构造函数.
 虚析构函数将阻止合成移动操作
 基类需要一个虚析构函数这一事实还会对基类和派生类的定义产生另外一个间接的影响:如果一个类定义了析构函数,即使它通过=default的形
 式使用了合成的版本,编译器也不会为这个类合成移动操作.
 */

/*
Pimpl惯用法:支持在公有接口中完全隐藏内部细节.从本质上讲,它支持将私有的成员数据和方法转移到.cpp文件中.(构建接口和实现分离)
"Pimpl","pointer to implementation(指向实现的指针)"
该技巧可以避免在头文件中暴露私有细节.是促进API接口和实现完全分离的重要机制.
Pimpl并不是严格意义上的设计模式,这种惯用法可以看作是桥接设计模式的一种特例.

私有指针,该指针指向隐藏的实现类.
将类的数据成员定义为指向某个已经声明过的类型的指针.这里的类型仅仅作为名字引入,并没有被完整地定义,因此我们就可以将该类型的定义隐藏
在.cpp文件中.这通常称为不透明指针,因为用户无法看到它所指向的对象细节.
本质上,Pimpl是一种同时在逻辑上和物理上隐藏私有数据成员与函数的办法.

将Impl类声明为私有内嵌类是为了避免与该实现相关的符号污染全局命名空间,将其声明为私有的表示它不会污染类的共有API.

在Impl类中放置多少逻辑?
将所有私有成员和私有方法放置在Impl类中.这样可以保持数据和操作这些数据的方法的封装性,从而避免在公有头文件中声明私有方法.
不能在实现类中隐藏私有虚方法.它们必须出现在公有类中,以保证任何派生类都能够覆盖它们.
虽然可以将公有类传递给需要使用它的实现类的方法,但必要时可以在实现类中增加指回公有类的指针,以便Impl类调用公有方法.

如果没有为类显式定义拷贝构造函数和拷贝赋值操作符,编译器会默认创建.但是这种默认的构造函数只能执行对象的浅复制.这不利于采用Pimpl惯用
法的类,因为这意味着如果客户复制了对象,则这两个对象将指向同一个mImpl实现对象.这样一来,两个对象可能在析构函数中尝试删除同
一个mImpl对象,就会导致崩溃.
不过本类已经在单例基类中做了处理.

采用Pimpl的对象大小从不改变,因为对象总是单个指针的大小.对私有成员变量做任何修改都只影响隐藏在.cpp文件内部的实现类的大小.如果对实现做出重大
改变时,对象的二进制表示可以不变.

使用Pimpl惯用法,编译器将不再能够捕获const方法中对成员变量的修改.这是由于成员变量现在存在于独立的对象中.编译器仅检查const方法中的
mImpl指针值是否发生了变化,而不检查mImpl指向的任何成员.
事实上,采用Pimpl惯用法的类中的每个成员函数都可以定义为const的(除了构造函数和析构函数).
*/

/*
这里我们也可以使用std::shared_ptr.
一个std::unique_ptr"拥有"它所指向的对象.与std::shared_ptr不同,某个时刻只能有一个std::unique_ptr指向一个给定对象.当std::unique_ptr被销毁时,它所指向
的对象也被销毁.
与std::shared_ptr不同,没有类似std::make_shared的标准库函数返回一个std::unique_ptr.当我们定义一个std::unique_ptr时,需要将其绑定到一个new返回的指针
上.类似std::shared_ptr,初始化std::unique_ptr必须采用直接初始化形式.
std::unique_ptr<double> p1;  //可以指向一个double的std::unique_ptr
std::unique_ptr<int> p2(new int(42)); //p2指向一个值为42的int
由于一个std::unique_ptr拥有它指向的对象,因此std::unique_ptr不支持普通的拷贝或赋值操作:
std::unique_ptr<string> p1(new string("Stegosaurus"));
std::unique_ptr<string> p2(p1);  //错误,std::unique_ptr不支持拷贝
std::unique_ptr<string> p3;
p3 = p2;  //错误,std::unique_ptr不支持赋值

std::unique_ptr操作
std::unique_ptr<T> u1,std::unique_ptr<T,D> u2
空std::unique_ptr,可以指向类型为T的对象.u1会使用delete来释放它的指针;u2会使用一个类型为D的可调用对象来释放它的指针.
std::unique_ptr<T,D> u(d)
空std::unique_ptr,指向类型为T的对象,用类型为D的对象d代替delete.
u = nullptr
释放u指向的对象,将u置为空.
u.release()
u放弃对指针的控制权,返回指针,并将u置为空.
u.reset()
释放u指向的对象.
u.reset(q),u.reset(nullptr)
如果提供了内置指针q,令u指向这个对象,否则将u置为空.

虽然我们不能拷贝或赋值std::unique_ptr,但可以通过调用release或reset将指针的所有权从一个(非const)std::unique_ptr转移给另一个unique:
//将所有权从p1转移给p2
std::unique_ptr<string> p2(p1.release()); //release将p1置为空
std::unique_ptr<string> p3(new string("Trex"));
//将所有权从p3转移给p2
p2.reset(p3.release()); //reset释放了p2原来指向的内存
release成员返回std::unique_ptr当前保存的指针并将其置为空.因此,p2被初始化为p1原来保存的指针,而p1被置为空.
reset成员接受一个可选的指针参数,令std::unique_ptr重新指向给定的指针.如果std::unique_ptr不为空,它原来指向的对象被释放.因此,对p2调用reset释
放了string所使用的内存,将p3对指针的所有权转移给p2,并将p3置为空.
调用release会切断std::unique_ptr和它原来管理的对象间的联系.release返回的指针通常被用来初始化另一个智能指针或给另一个智能指针赋值.在本例
中,管理内存的责任简单地从一个智能指针转移给另一个.但是,如果我们不用另一个智能指针来保存release返回的指针,我们的程序就要负责资源的
释放:
p2.release();  //错误,p2不会释放内存,而且我们丢失了指针.
auto p = p2.release(); //正确,但我们必须记得delete(p).

传递std::unique_ptr参数和返回std::unique_ptr
不能拷贝std::unique_ptr的规则有一个例外:我们可以拷贝或赋值一个将要被销毁的std::unique_ptr.最常见的例子是从函数返回一个std::unique_ptr:
std::unique_ptr<int> clone(int p)
{
	//正确,从int*创建一个std::unique_ptr<int>.
	return std::unique_ptr<int>(new int(p));
}
还可以返回一个局部对象的拷贝:
std::unique_ptr<int> clone(int p)
{
	std::unique_ptr<int> ret(new int (p));
	//. . .
	return ret;
}
对于两段代码,编译器都知道要返回的对象将要被销毁.在此情况下,编译器执行一种特殊的"拷贝".
*/

/*
常量对象/指向常量对象的指针，不能调用非const成员函数。

指向基类的指针（new 子类得来）无法调用子类新添加的函数。

向下转型，可以通过static_cast（这没问题，只要确实子类是从基类派生而来的）。static_cast通常无需付出，或付出极少的运行期代价。
dynamic_cast也可以向下转型，但不同于static_cast的是，仅用于对多态类型进行向下转型，也就是说，被转型的表达式类型必须是一个指向带有虚函数的类类型的
指针，并且执行运行期检查工作，来判定转型的正确性，dynamic_cast需要付出显著的运行期开销。
dynamic_cast支持对引用的向下转型，但失败的话，将抛出std::bad_cast异常（因为不存在空引用）。
为了发现“肯定哪儿出错了”，所以，应该用dynamic_cast去向下转型引用，而不是指针。
*/

/*
大多数游戏制造商都要求游戏通过“浸泡测试（soak tests）”。他们将游戏置于demo模式连续地跑上好几天。
多数情况下，内存碎片化或者内存泄露是导致游戏宕机的原因。
*/