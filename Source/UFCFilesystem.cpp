#include "UFCFilesystem.h"

#ifdef UFC_HIERARCHICAL_COMPILE

#include "UFCStringUtilities.h"
#include "UFCLogManager.h"
#define BOOST_FILESYSTEM_NO_DEPRECATED
#include "boost/filesystem.hpp"
#include "boost/filesystem/fstream.hpp"
#include "boost/foreach.hpp"

namespace bf = boost::filesystem;

namespace ung
{
	Filesystem::Filesystem()
	{
		UNG_LOG("Create file system.");
	}

	bool Filesystem::isExists(char const* fileFullName) const
	{
		return bf::exists(fileFullName);
	}

	bool Filesystem::isDirectory(char const* dirName) const
	{
		return bf::is_directory(dirName);
	}

	bool Filesystem::isRegularFile(char const* fileFullName) const
	{
		return bf::is_regular_file(fileFullName);
	}

	uintmax Filesystem::getFileSize(char const* fileFullName) const
	{
		if (!isExists(fileFullName) || !isRegularFile(fileFullName))
		{
			BOOST_ASSERT(!"fail to get file size.");
		}

		/*
		uintmax_t file_size(const path& p);
		The reported Linux and Windows sizes are different because the Linux tests used "\n" line endings, while the Windows tests 
		used "\r\n" line endings.
		*/
		return bf::file_size(fileFullName);
	}

	std::shared_ptr<STL_VECTOR(String)> Filesystem::listFileNameRecursive(char const* dirName) const
	{
		std::shared_ptr<STL_VECTOR(String)> ptr = std::make_shared<STL_VECTOR(String)>();

		for (auto&& x : bf::recursive_directory_iterator(bf::path(dirName)))
		{
			if (isDirectory(x.path().string().data()))
			{
				continue;
			}

			ptr->push_back(x.path().filename().string());
		}

		return ptr;
	}

	std::shared_ptr<STL_VECTOR(String)> Filesystem::listFileFullNameRecursive(char const* dirName) const
	{
		std::shared_ptr<STL_VECTOR(String)> ptr = std::make_shared<STL_VECTOR(String)>();

		for (auto&& x : bf::recursive_directory_iterator(bf::path(dirName)))
		{
			if (isDirectory(x.path().string().data()))
			{
				continue;
			}

			ptr->push_back(x.path().string());
		}

		return ptr;
	}

	bool Filesystem::isEmpty(char const* str) const
	{
		return bf::path(str).empty();
	}

	bool Filesystem::isAbsolute(char const* str) const
	{
		return bf::path(str).is_absolute();
	}

	bool Filesystem::hasRootPath(char const* str) const
	{
		return bf::path(str).has_root_path();
	}

	bool Filesystem::hasParentPath(char const* str) const
	{
		return bf::path(str).has_parent_path();
	}

	bool Filesystem::hasFileName(char const* str) const
	{
		return bf::path(str).has_filename();
	}

	bool Filesystem::hasStem(char const* str) const
	{
		return bf::path(str).has_stem();
	}

	bool Filesystem::hasExtension(char const* str) const
	{
		return bf::path(str).has_extension();
	}

	String Filesystem::getRootPath(char const* str) const
	{
		return bf::path(str).root_path().string();
	}

	String Filesystem::getParentPath(char const* str) const
	{
		return bf::path(str).parent_path().string();
	}

	String Filesystem::getFileName(char const* fileFullName) const
	{
		return bf::path(fileFullName).filename().string();
	}

	String Filesystem::getFileStem(char const* fileFullName) const
	{
		return bf::path(fileFullName).stem().string();
	}

	String Filesystem::getFileExtension(char const* fileFullName) const
	{
		auto ext = bf::path(fileFullName).extension().string();
		//删除后缀名中包含的"."
		StringUtilities::eraseLeftN(ext, 1);
		
		return ext;
	}

	uintmax Filesystem::getSpaceFree(char const* dir) const
	{
		return bf::space(dir).free;
	}

	bool Filesystem::createDir(char const* dir) const
	{
		return bf::create_directories(dir);
	}

	void Filesystem::processDirSeparator(String& str)
	{
		//路径不能为空
		BOOST_ASSERT(!str.empty());

		//文件系统
		auto& fsRef = Filesystem::getInstance();

		//转换为小写
		StringUtilities::toLower(str);

		//替换路径中的"\\","//"为"/"
		StringUtilities::replaceAllInSensitive(str, "\\", "/");
		StringUtilities::replaceAllInSensitive(str, "//", "/");

		//必须以"./"或"../"开头
		if (!StringUtilities::startWithInSensitive(str, "./") && !StringUtilities::startWithInSensitive(str, "../"))
		{
			str = String("./") + str;
		}

		//如果是目录的话
		if (fsRef.isDirectory(str.data()))
		{
			//目录必须以"/"结尾
			if (!StringUtilities::endWithInSensitive(str, "/"))
			{
				str = str + String("/");
			}
		}
	}
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

/*
目录、文件处理是脚本语言如Shell、Perl、Python所擅长的领域，C++语言缺乏对操作系统中文件的查询和操作能力。
filesystem库是一个可移植的文件系统操作库，使用POSIX标准表示文件系统的路径，接口很类似标准库的容器和迭代器，使C++具有了类似脚本语言
的功能，可以跨平台操作目录、文件。

filesystem库的核心类是path，它屏蔽了不同文件系统的差异，使用可移植的POSIX语法提供了通用的目录、路径表示，并且支持POSIX的符号链接概
念。
*/