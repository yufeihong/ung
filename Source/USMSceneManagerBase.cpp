#include "USMSceneManagerBase.h"

#ifdef USM_HIERARCHICAL_COMPILE

#pragma warning(push)

//返回局部变量的引用
#pragma warning(disable : 4172)

namespace ung
{
	String const & SceneManagerBase::getName() const
	{
		return EMPTY_STRING;
	}

	SceneType SceneManagerBase::getSceneType() const
	{
		return SceneType();
	}

	StrongISceneNodePtr SceneManagerBase::getSceneRootNode() const
	{
		return StrongISceneNodePtr();
	}

	StrongIObjectPtr SceneManagerBase::createObject(String const & objectDataFileFullName, String const & objectName)
	{
		return StrongIObjectPtr();
	}

	void SceneManagerBase::destroyObject(String const & objectName,bool remove)
	{
	}

	void SceneManagerBase::destroyObject(StrongIObjectPtr objectPtr,bool remove)
	{
	}

	StrongIObjectPtr SceneManagerBase::getObject(String const & objectName)
	{
		return StrongIObjectPtr();
	}

	uint32 SceneManagerBase::getAllObjectCount() const
	{
		return uint32();
	}

	uint32 SceneManagerBase::getAllSceneNodeCount() const
	{
		return uint32();
	}

	StrongISceneNodePtr SceneManagerBase::createSceneNode(WeakISceneNodePtr parentPtr, String const & nodeName)
	{
		return StrongISceneNodePtr();
	}

	void SceneManagerBase::destroySceneNode(String const & nodeName)
	{
	}

	void SceneManagerBase::destroySceneNode(StrongISceneNodePtr nodePtr)
	{
	}

	ISpatialManager * SceneManagerBase::getSpatialManager() const
	{
		return nullptr;
	}

	StrongISceneQueryPtr SceneManagerBase::createRaySceneQuery(Ray const & ray)
	{
		return StrongISceneQueryPtr();
	}

	StrongISceneQueryPtr SceneManagerBase::createSphereSceneQuery(Sphere const & sphere)
	{
		return StrongISceneQueryPtr();
	}

	StrongISceneQueryPtr SceneManagerBase::createAABBSceneQuery(AABB const & box)
	{
		return StrongISceneQueryPtr();
	}

	StrongISceneQueryPtr SceneManagerBase::createCameraSceneQuery(Camera const & camera)
	{
		return StrongISceneQueryPtr();
	}
}//namespace ung

#pragma warning(pop)

#endif//USM_HIERARCHICAL_COMPILE

/*
场景(scene)是物体或模型的集合。
*/