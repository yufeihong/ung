#include "USMObjectComponent.h"

#ifdef USM_HIERARCHICAL_COMPILE

namespace ung
{
	ObjectComponent::ObjectComponent(String const& name) :
		mName(name)
	{
	}

	ObjectComponent::~ObjectComponent()
	{
		//因为组件中对其所属的对象是“强引用”
		mOwner.reset();
	}

	void ObjectComponent::init(tinyxml2::XMLElement * pComponentElement)
	{
	}

	void ObjectComponent::postInit()
	{
	}

	void ObjectComponent::update()
	{
	}

	String const & ObjectComponent::getName() const
	{
		return mName;
	}

	void ObjectComponent::setOwner(StrongIObjectPtr pOwner)
	{
		mOwner = pOwner;
	}
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE

/*
Game Actors and Component Architecture:
解决继承体系的各种别扭问题。

角色集成架构系统参见图Knowledge/USM/01.png

Let’s say you build the previous system for your first-person shooter game. It would probably 
work just fine for a while. Now let’s say the designer comes up to you and asks you to make 
a new kind of pickup, a mana pickup that has an animation. You can’t derive from Pickup 
since it doesn’t include any of the animation code, and you can’t derive from AnimatingActor 
since that doesn’t include any of the functionality needed for pickups.

One option would be to derive from both classes via multiple inheritance, but that would be 
disastrous(灾难性的). You would have to use a virtual base class to avoid the dreaded diamond 
of death.

角色继承-钻石架构参见图Knowledge/USM/02.png

钻石型多重继承，如果没有采用虚继承的话，基类的数据成员就会在子类中形成多个拷贝。
If at all possible, try to never use multiple inheritance unless every base class you’re deriving 
from has nothing but pure virtual functions.

Another possibility is to shuffle around the hierarchy and make Pickup inherit from 
AnimatingActor. This would solve the problem,but it means that all pickups have to carry 
around the weight of the animation system, which is most likely nontrivial.

Go back and take a look at the architecture again and notice how all of those subclasses are 
really just trying to add a new feature to the actor. If you can encapsulate each of those 
features into a component and compose a final object made up of those components, you can 
get the same functionality as the old class hierarchy but still have the flexibility to make 
changes.

What’s even better is that these components are built up at runtime(通过在运行时读取外部xml数据),
so you can add and remove them during the course of the game.You can’t do that with the 
old inheritance model!

The components have a base class that the actor maintains a reference to as well as a subclass
interface that represents the responsibility of that component. Each subclass of that interface
is an implementation of that responsibility. For example, you might have one interface class
called AiComponent, which has several different implementations for different kinds of AI. The
important thing to note is that each component interface has a unique identifier, and each
actor is only allowed to have one class of a particular responsibility. That means you could
have two AiComponent subclasses,but you could replace an existing one with a new one,
allowing you to change the actor’s behavior at runtime.

角色组件架构系统参见图Knowledge/USM/03.png

Whenever a system needs access to a component, it asks the actor for that interface and gets 
a pointer to the appropriate interface object. The lowest level of the tree defines the behavior 
for that component. It’s important to note that no outside system ever gets a pointer directly 
to the concrete class. You would never have a system know about Ammo or Health directly.

Not all things with position need to be rendered, and not everything that needs to be rendered
needs a shader.Try to have each component handle exactly one thing.

Creating Actors and Components:
All actors are created using a factory. The factory’s job is to take an XML resource(数据驱动),
parse it,and return a fully initialized actor complete with all the appropriate components.

All actors are defined with an XML data file. This data file allows you to define a component
configuration and any default values for that component.

If you decide that this actor needs to have a brain, you can easily add an AI component
without changing a single line of code.That’s the power of data-driven(数据驱动) development.

Keep in mind that these actor XML files define the template for a type of actor, not a specific
actor instance.

The XML file only defines the definition. You can think of it as defining a class for this type of
actor.

组件间数据共享的两种方式：
Direct Access:
The first way to share data is by directly accessing the component interface.Each component 
stores a pointer back to the owning actor, so it’s a simple matter of asking the actor for the 
component.

weak_ptr<Pickup> pWeakPickup = pActor->GetComponent<Pickup>(Pickup::COMPONENT_ID);
shared_ptr<Pickup> pPickup = MakeStrongPtr(pWeakPickup);

pPickup will now either contain a strong reference to the Pickup component for pActor or it 
will be empty. If it’s empty, it means pActor doesn’t have a Pickup component. It’s important 
to always run this check and never make assumptions.

Events:
If you really want to decouple your components, another method is to use an event system.The
actor acts as a messaging service that its components (and other systems) can use to post
messages about important events. Each component registers which events it cares about, and
when the actor receives a message, it distributes it to the appropriate components.

For example, let’s say the AI component wants to move the actor. It just posts a message
requesting the move to a new position, and the actor tells the appropriate components. The
AI component doesn’t have to know, nor does it care, which components receive the message.

This situation certainly keeps components from being decoupled from one another,but it also
raises a few concerns. Sometimes it’s important to know which component is answering the
message and in which order. Say you post a move message, and the renderable component
receives it first. It updates its internal positions, and everything is fine. Then the physics
component receives the new position and detects it as being invalid. Now what? The physics
system could send an event to disregard the old position and give the new position, but this
could cause an oscillation where the AI component and physics component are battling each
other trying to move the actor. The actor will mostly appear to vibrate, jumping back and
forth between two positions.

The Best of Both Worlds:
The best solution to these problems is to use a mixture of the two communication methods.Events
are great for broadcasting things that other components may or may not care about, and
direct access is great when you need to directly tell something to a specific component.
*/