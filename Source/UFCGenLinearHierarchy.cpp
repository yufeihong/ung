/*
˵����
template <class T, class Base>
class EventHandler : public Base
{
public:
	virtual void OnEvent(T& obj, int eventId)
	{
	}
};

class Window
{};

class Button
{};

class ScrollBar
{};

class Mouse
{};

typedef GenLinearHierarchy<TYPELIST_3(Window, Button, ScrollBar),EventHandler> MyEventHandler;
MyEventHandler ehObj;

using top = EventHandler<Mouse, EmptyType>;
ret = UFC_SUPERSUBCLASS(top, MyEventHandler);//false

using top0 = EmptyType;
ret = UFC_SUPERSUBCLASS(top0, MyEventHandler);//true

using top1 = EventHandler<ScrollBar, EmptyType>;
ret = UFC_SUPERSUBCLASS(top1, MyEventHandler);

using top15 = GenLinearHierarchy<TYPELIST_1(ScrollBar), EventHandler>;
ret = UFC_SUPERSUBCLASS(top15, MyEventHandler);

using top2 = EventHandler<Button, GenLinearHierarchy<TYPELIST_1(ScrollBar), EventHandler>>;
ret = UFC_SUPERSUBCLASS(top2, MyEventHandler);

using top25 = GenLinearHierarchy<TYPELIST_2(Button, ScrollBar), EventHandler>;
ret = UFC_SUPERSUBCLASS(top25, MyEventHandler);

using top3 = EventHandler<Window, GenLinearHierarchy<TYPELIST_2(Button, ScrollBar), EventHandler>>;
ret = UFC_SUPERSUBCLASS(top3, MyEventHandler);

auto wC = static_cast<EventHandler<ScrollBar, EmptyType>&>(ehObj);
auto wCType = typeid(wC).name();
*/