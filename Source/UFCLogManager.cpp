#include "UFCLogManager.h"

#ifdef UFC_HIERARCHICAL_COMPILE

namespace ung
{
	typename LogManager::Helper LogManager::mHelper;

	LogManager::LogManager()
	{
		//不要在这里使用UNG_LOG宏
	}

	LogManager::~LogManager()
	{
		UNG_PRINT_ONE_LINE("==========析构LogManager==========");
	}

	void LogManager::logRecord(String& mes,LOG_LEVEL_TYPE level)
	{
		mLog.logRecord(mes,level);
	}

	void LogManager::logRecord(const char* mes,LOG_LEVEL_TYPE level)
	{
		mLog.logRecord(mes,level);
	}

	void LogManager::logException(const char* mes,LOG_LEVEL_TYPE level)
	{
		mLog.logException(mes,level);
	}

	void LogManager::logException(String& mes,LOG_LEVEL_TYPE level)
	{
		mLog.logException(mes,level);
	}
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

/*
参考：
#include "boost/container/detail/singleton.hpp"

认为在main之前和之后都是仅有一个线程在运行。

仅支持有默认构造函数的类。
*/

/*
单例设计模式用来确保一个类仅存在一个实例.
该模式亦提供对此唯一实例的全局访问点.可以认为单例是一种更加优雅的全局变量.不过相对于全局变量,它还提供了一些优点,因为:
1:确保一个类只创建一个实例.
2:为对象分配和销毁提供控制.
3:支持线程安全地访问对象的全局状态.
4:避免污染全局命名空间.

不同编译单元中的非局部静态对象的初始化顺序是未定义的。
也就是说使用非局部静态变量初始化单例是很危险的。非局部对象是指声明在函数之外的对象。静态对象包括全局对象以及在类、函数或文件作用域内
声明为静态的对象。因此，初始化单例的途径之一是在类的方法中创建静态变量。

如果线程安全的性能对程序来说至关重要，那么可以考虑避免使用惰性实例化模型，而在启动时就初始化单例。例如，可以在main()调用之前初始化，
或者使用互斥锁保护。
1：静态初始化。
静态初始化器在main()之前调用，通常可以假定程序这时仍是单线程的。因此可以将创建单例实例作为静态初始化器的一部分，从而避免了使用互斥锁
的需求。这需要确保构造函数不依赖其他.cpp文件中的非局部静态变量。在singleton.cpp文件中添加以下静态初始化调用，以确保在main()调用之前
创建实例：
static Singleton& obj = Singleton::GetInstance();
2：向库中添加一个初始化例程。
*/

/*
Singletons(单件)实作技术
GoF著作中对Singleton的描述只是这么简単:保证一个 class只有一个实体(instance),并为它提供一个全局访问点(globalaccesspoint)”。
“提供一个全局访问点”具有微妙含义:从客户角度来看, Singleton对象“拥有自己”。客户产生singleton时并不需要什么特殊步骤。Singleton对象负责自
身的诞生和摧毀。Singleton生命期(lifetime)的管理是实作Singleton时最伤脑筋的地方。

静态数据+静态函数!=Singleton
最主要的问题是,静态函数不能成为虚函数,这么一来, 如果不开放源码, 就很难让外界改变其行为。
这种做法的更隐廠问题是, 它使得初始化(initialization)和清理(cleanup)工作变得困难 。

用以支持Singletons的一些 C++基本手法
通常，在C++中，Singletons系通过以下手法的某些变化加以实现：
//Singleton.h
class Singleton
{
public：
	static Singleton* Instance()																		//unique point of access
	{
		if（!pInstance_）
			pInstance_ =new Singleton;
		return pInstance_;
	)
	...
private：
	Singleton();																							//Prevent clients from creating a new Singleton
	Singleton(const Singleton&);																	//Prevent clients from creating a copy of the Singleton
	static Singleton* pInstance_;																	//The one and only instance
};
//Singleton.cpp
Singleton* Singleton::pInstance_ =0;															//不存在调用构造函数的情况，因为是指针

由于构造函数都是private,客端无法产生Singletons,但Singleton自身的成员函数——更明确地说是Instance(),则没有这个限制。因此,Singleton对象
的唯一性在编译期就得以实施。这是C++实现Singleton设计模式的精髓所在。

将前例的pInstance_指针替换为一个完整的singleton对象，藉此简化事情，是一种不幸的诱惑：
//Singleton.h
class Singleton
{
public：
	static Singleton* Instance()																		//Unique  point  of  access
	{
		return &instance;
	}

	int DoSomething()；

private：
	static Singleton instance_;
};
//Singleton.cpp
Singleton Singleton::instance_;																		//在main()函数调用之前，会调用其构造函数
这不是个好做法。虽然instance_（角色如同前例的pInstance_）是Singleton的静态成员，但这两个版本有个重要差异：instance_被动态初始化（通
过执行期间的Singleton构造函数调用），pInstance_则受益于静态初始化（其型别并无构造函数可通过编译期常量来初始化）。

要知道，程序的第一条assembly（汇编语言）语句被执行之前，编译器就已经完成了静态初始化(通常静态初始化相关数值或动作(static initializers)就
位于“内含可执行程序”的文件中，所以，程序被装载(loading)之际也就是初始化之时）。然而面对不同编译单元中的动态初始化对象，C++并未定义其
间的初始化顺序，这就是麻烦的主要根源。请看：
//SomeFile．cpp
#include "Singleton.h"
int global=Singleton::Instance()->DoSomething();
由于无法确保编译器一定先将instance初始化，所以全局变量global的初值设定式中对Singleton::Instance的调用有可能传回一个尚未构造的对象。这
意味着你无法保证任何外部对象所使用的instance是一个己被正确初始化的对象。

实施“Singleton的唯一性”
另一个小小改进是：让Instance()传回reference而非指针。如果传回指针，调用端（接收端）有可能将它delete掉。为了将这种可能性降至最低，传
回reference比较更安全些：
//inside class Singleton
static Singleton& Instance()

你不能将唯一的Singleton对象赋予（指派）给另
一个对象，因为不可以存在两个Singleton对象。对Singleton对象来说，任何赋值动作都是对自身赋值，没有任何意义，所以值得将assignment
（赋值）操作符禁掉（做法是：将它声明为private并且根本不实作它）。
最后一个保护措施是：将析构函数声明为private。有了这个措施，拥有Singleton对象指针者，就不会（无法）意外删除之。
增加以上所有措施之后，Singleton的接口看起来像这样：
class Singleton
{
	static Singleton& Instance();
	...
private:
	Singleton();
	Singleton(const  Singleton&);
	Singleton& operator= (const  Singleton&);
	~Singleton();
};

摧毁Singleton:
事实上就算Singleton未被删除，也不会造成内存泄漏(memory leak)，因为只有当你分配了累积性数据(accumulating data)并丢失对它的所有
reference时，内存泄漏才会发生。这里并不属于上述情况，因为没有什么累积性的东西，而且直到程序结束前我们还保存着对我们所分配的内存的相关
认识。此外，当一个进程(process)终止时，所有现代化操作系统都能够将进程所用的内存完全释放.避免资源泄漏的唯一正确做法是在程序关闭（结束）
时期删除Singleton对象。问题在于我们必须谨慎选择删除时机，确保Singleton对象被摧毁后不会再有任何人去取用它。摧毁Singleton的最简单方案是
仰赖语言机制。以下代码演示Singlefon的另一种实作法。其中Instance()并未使用动态分配和静态指针，而是使用了一个局部静态变量：
Singleton& Singleton::Instance()
{
	static Singleton obj;																					//执行期才初始化
	return obj;
}
要知道，函数内的static对象在该函数第一次执行时
被初始化(之前2个都是在程序的第一条汇编语言之前就初始化了)。请不要把“执行期才初始化”的static变量和“通过编译期常量加以初始化”的基本
型static变量混淆了。例如：
int  Fun()
{
	static int x=100;																							//这就是“通过编译期常量加以初始化”
	return ++x;
}
这种情况下x的初始化会在程序中的任何一行被执行之前完成，而且通常是在程序装载期间。Fun()第一次被调用前，x早就被设为100了。但如果初始值
不是一个编译期常量，抑或静态变量是个拥有构造函数的对象，那么变量的初始化将发生于执行期，也就是执行流程第一次行经其定义式之时。

此外，编译器会产生一些代码，使得初始化之后，执行期相关机制会登记需被析构的变量.

atexit()由标准C程序库提供，让你得以注册一些在程序结束之际自动被调用的函数，且其被调用次序为后进先出(适用于：局部静态)（LIFO,根据定义,
C++对象析构以LIFO方式进行，先产生的对象后摧毁。当然，以new和delete管理的对象不遵守这一规则).
atexit()的标记式(signature)是：
//Takes a pointer to function
//Returns 0 if successful, or a nonzero value if an error occurs.
int  atexit (void (*pFun)());
编译器会自动产生出DestroySingleton函数（它被执行后会摧毁_buffer内存内的Singleton对象）并将其地址传给atexit()。
atexit()如何运作？每次被调用，它的参数会被压入C runtime library所维护的一个私有stack内。程序结束之际，执行期相关机制便会调用那些经由
atexit C）登记的函数。

1：静态初始化，是在第一条汇编语句之前。
2：动态初始化，是在程序开始运行后。
3：执行期的动态初始化，其顺序未定。
4：执行期动态初始化的对象，其释放顺序是后进先出。

Dead（失效的）Reference问题：
如果我们以Meyers singletons实现，程序并不正确。举个例子，假设Keyboard成功构造之后Display初始化失败，于是Display的构造函数会产生一个
Log记录错误，而且程序准备结束。此时语言规则发挥作用：执行期相关机制会摧毁局部静态对象，摧毁次序和生成次序相反。因而Log会在Keyboard之
前被摧毁。但万一Keyboard关闭失败并向Log报告错误，Log::Instance()会不明就理地回传一个reference，指向一个已被摧毁的Log对象的“空壳”。
于是程序步入了“行为不确定”的阴暗国土。这就是所谓的dead-reference问题。

我们希望无论Log何时创建，它一定得在Keyboard和Display之后被摧毁，这样才能收集二者析构时的错误报告。

一个设计合理的Singleton至少应该执行“dead-reference检测”。为做到这一点，我们可以藉由一个成员变量static bool destroyed_来追踪析构行为.
其值一开始为false,Singleton析构函数会将它设为true。
除了“产生Singleton对象并传回其reference”之外，Singleton::Instance()如今身兼另一项任务——检测dead reference。让我们采用所谓“一个函
数一项职责”的设计原则，因此定义出三个独立的成员函数：Create()高效产生singleton对象；OnDeadReference()负责处理错误；众所周知的
Instance()则用以访问唯一的Singleton对象。这其中只有Instance()被声明为public。
//Singleton.h
class Singleton
{
public :
	static Singleton& Instance()
	{
		if (!plnstance)
		{
			//Check for dead reference
			if(destroyed_)
			{
				OnDeadReference();
			}
			else
			{
				//First call--initialize
				Create();
			}
		}

		return *plnstance ;
	}
private :
	//Create a new Singleton and store a pointer to it in plnstance
	static void Create()
	{
		//Task: initialize pInstance_
		static Singleton theInstance;
		pInstance_ = &theInstance;
	}

	//Gets called if dead reference detected
	static void OnDeadReference()
	{
		throw std::runtime_error("Dead Reference Detected");
	}

	virtual ~Singleton()
	{
		pInstance_ = 0;
		destroyed = true;
	}

	//Data
	static Singleton *pInstance_;
	static bool destroyed_;
	...disabled "tors/operator=..."
};
//Singleton.cpp
Singleton* Singleton::pInstance_ = 0;
bool Singleton::destroyed = false;
很好，可以正常运作!只要程序结束，Singleton的析构函数就会被调用，于是将pInstance_设为0并将destroyed_设为true。如果此后有某个寿命更长
的对象试图取用这个singleton，执行流程会到达OnDeadReference()，于是抛出一个型别为runtime_error的异常。这个方案廉价、简单，而且不失
效率。

解决Dead Reference问题(I)：Phoenix Singleton
我们希望Log无时无刻不存在，不论它当初何时构造。极端情况下我们甚至需要再次产生Log（尽管它已被摧毁），这样就可以随时用它。这就是
Phoenix Singleton设计模式背后的思想。

Keyboard和Display还是保持“一般的”Singletons，Log则成为Phoenix Singleton。带有静态变量的Phoenix Singleton，其实作手法非常简单。一
旦检测到dead reference，我们便在旧躯壳中产生一个新的Singleton对象（C++保证这种可能，因为静态对象的内存在整个程序生命期间都会保留着）。
我们也通过atexit()登记这个新对象的析构函数。我们不必修改Instance()，仅有的修改出现在OnDeadReference()函数中。
class Singleton
{
	...as before...
	void KiIIPhoenixSingleton();																			//Added
};

void SingleLon::OnDeadReference()
{
	//Obtain the shell of the destroyed singleton
	Create();

	//Now pInstance_ points to the "ashes" of the singleton-the raw memory that the singleton was seated in.
	//Create a new singleton at thar address
	new(pInstance_) Singleton;
	//Queue this new object's destruction
	atexit(KillPhoenixSingleton);
	//Reset destroyed_
	destroyed_ = false;
}

void Singleton::KillPhoenixSingleton()
{
	//Make all ashes again
	//call the destructor by hand.
	//It will set pInstance_ to zero and destroyed_ to true
	pInstance_->-Singleton();
}
OnDeadReference()所使用的new操作符是所谓placement new操作符，它并不分配内存，而是在某个地址上（本例的pInstance_构造一个新对象.
上面的Singleton增加了一个新成员函数KillPhoenjySingleton()。既然我们通过new来令Phoenix Singleton重生，我们就不再能够像对待静态变量那
样靠编译器技巧摧毁它。我们既然手工建造了它，就必须手工摧毁它，atexit(KillPhoenixSingleton)保证了这一点。

解决Dead Reference问题(II):带寿命的Singletons:
如果你的Singleton持有状态(state)，那个状态会在“析构---创建”周期中丢失。如果要经由Phoenix策略来实现concrete Singleton，实作者就必须特
别注意保持“析构”和“重新构造”两个时刻间的状态。
Log必须拥有“比Keyboard和Display更长”的寿命。我们希望有一种简单方法来控制各种Singletons的寿命。

SetLongevity()接受两个参数，一个是reference，指向任意型别对象，另一个是整数值，代表寿命：
//Takes a reference to an object allocated with new and the longevity of that object.
template<typename T>
void SetLongevity(T* pDynObject,unsigned int longevity);
这个函数保证，和其他所有寿命较短的对象相比，pDynObject存在的时问比较长。当程序结束时，所有通过SetLongevity()登记的对象便会根据寿命长
短被依序删除。
对那些“寿命受编译器控制”的对象（例如一般全局对象、static对象、auto对象）来说，你无法运用SetLongevity()。编译器已经自动产生了一些代码来
摧毁这些对缘，如果你又调用SetLongevity()，它们就会被摧毁两次。SetLongevity针对的只是“经由new分配而得”的对象。此外，对某个对象调用
SetLongevity()，表示你不会对那个对象调用delete。注意：这里不是不能调用delete，而是，比如，如果，所有的单件都是在运行时，通过new获得
的，那么这些单件的析构，就完全和atexit()函数没有一毛钱关系了，我们只要将来控制delete排序就可以了，比如可以内部刻意持有一个shared_ptr。
但是如果用户定义了一个静态的单件指针，然后将这个指针静态初始化为nullptr，然后在运行时给这个指针通过new出来的地址来赋值，那么将来这个
指针还是会牵扯到atexit()函数。不过话说回来，把这个shared_ptr给保存到哪里呢？容器？容器也有析构的时候，静态对象？那么又牵扯到了atexit()函数。

“双检测锁定”(Double-Checked Locking)模式:
Singleton& Singleton::Instance()
{
	Lock guard（mutex_）；
	if（!pInstance_）
	{
		pInstance_ = new Singleton;
	)
	return *pInstance_;
}
这就消除了竞态条件：当某个线程对pInstance_赋值咐，其他所有线程会止步于guard构造函数中。
本法之不足在于缺乏效率。每次Instance()执行都会引发同步对象(synchronization object)的加锁与解锁，即使竞态条件只在生命期内出现一次。

以下代码也希望能够成为一个解法。它试图避免额外开销：
Singleton&  Singleton::Instance()
{
	if  (!pInstance_)
	{
		Lock guard(mutex_)；
		pInstance_ = new Singleton;
	}

	return *pInstance_;
}
现在，额外开销的确不见了，但竞态条件又回来了。第一个线程通过了if测试，但正当它准备进入“同步区段”时，OS调度器中断了这个线程并将控制权转
给另一个线程。后者通过了1f测试（一点也不令人惊讶，它发现了一个null指针）并进入同步区段，而后结束执行。当第一个线程重新醒来时，它也进入
同步区段，但为时已晚，程序中于是构造出两个Singleton对象，这像是个没有答案的脑筋急转弯问题，但事实上的确有一个十分简单优雅的解法——所
谓“双检测锁定”。
想法很简单：首先进行检测，然后进入同步码，然后再次检测。但这一次指针要么已被初始化，要么就是null。下面的代码可以帮助你理解和领略“双检测
锁定”模式。啊，的确，计算机工程中也存在着“美”。
Singleton&  Singleton::Instance()
{
	if（!pInstance_）									//1
	{														//2
		Guard myGuard(lock_);					//3
		if  (!pInstance_)								//4
		{
			pInstance_ = new Singleton;
		}
	}

	return *pInstance_;
}
假设某个线程的控制流程进入了模糊区（注释第2行），此处可能有数个线程同时进入。但同步区则是“同一时刻只会有一个线程进入”。到了注释第3行，
模糊不复存在。一切都无比清晰：指针要么已经完全初始化，要么根本没有被初始化。第一个进入的线程会初始化指针变量，其他所有线程都会在注释第
4行的检测行动中失败，因而不会产生任何东西。

第一个检测快速而粗糙。如果Singleton对象存在，你可以得到它。否则就需要做进一步检测。
第二个检测缓慢而精确：它判断Singleton是否确实被初始化，如果没有，这个线程就会负责对它初始化。这就是“双检测锁定”模式。

RISC（精简指令集）机器的编译器有一个所谓的code arranger，它会重新排列编译器所产生出来的汇编语言指令，使代码能够最佳运用RISC处理器的
平行(parallel)特性（例如RISC机器可以同时执行个load动作和个add动作）。
“重新排列指令”是RISC处理器能够达到优化的一个主要因素，它甚至可以使速度加倍。但它也可能破坏“双检测锁定”模式：编译器有叫能在锁定mutex之
前先执行第二个if(!pInstance_)测试，于是竞念条件(race condition)再度出现。
故，至少你应该在pInstance_前添加volatile饰词，因为合理的编译器会为volatile对象产生出恰当而明确的代码。
将volatile饰词运用于某个型别身上，相当于告诉编译器：那个型别的值有可能被多个线程修改。知道了这一点，编译器就会避免某些优化措施（例如“将
数值保存于内部暂存器中”等等），以免导致多线程程序工作紊乱。最安全的选择便是：将pInstance_定义为volatile T*，既可用于多线程环境，也不会
伤及单线程程序。但是另一方面，在单线程模型中你可能的确想获得那些优化结果，所以T*将是pInstance_的最适当型别。这就是“pInstance_的实际
型别要由ThreadingModel策略来决定”的原因。
*/