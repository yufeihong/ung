#include "URMMaterialManager.h"

#ifdef URM_HIERARCHICAL_COMPILE

#include "URMMaterial.h"

namespace ung
{
	static String materialPath("../../../../Resource/Media/Materials/Scripts/");

	MaterialManager::MaterialManager()
	{

	}

	MaterialManager::~MaterialManager()
	{

	}

	std::shared_ptr<Material> MaterialManager::createMaterial(String const& materialName)
	{
		BOOST_ASSERT(!materialName.empty());

		String name = materialPath + materialName;

#if UFC_DEBUGMODE
		const char* fileNameCStr = name.c_str();

		auto validRet = Filesystem::getInstance().isRegularFile(fileNameCStr);
		BOOST_ASSERT(validRet && "not a file.");

		auto extRet = Filesystem::getInstance().hasExtension(fileNameCStr);
		BOOST_ASSERT(extRet && "does not have extension.");

		String extension = Filesystem::getInstance().getFileExtension(fileNameCStr);
#endif

		std::shared_ptr<Material> materialPtr(UNG_NEW_SMART Material(name));

		BOOST_ASSERT(materialPtr);

		addMaterial(name,materialPtr);

		return materialPtr;
	}

	void MaterialManager::destroyMaterial(String const & materialName)
	{
		BOOST_ASSERT(!materialName.empty() && "material name invalid.");

		String name = materialPath + materialName;

		removeMaterial(name);
	}

	std::shared_ptr<Material> MaterialManager::getMaterial(String const & materialName)
	{
		BOOST_ASSERT(!materialName.empty() && "material name invalid.");

		String name = materialPath + materialName;

		auto findRet = mMaterials.find(name);

		if (findRet == mMaterials.end())
		{
			return nullptr;
		}

		return findRet->second;
	}

	std::shared_ptr<const Material> MaterialManager::getMaterial(String const & materialName) const
	{
		BOOST_ASSERT(!materialName.empty() && "material name invalid.");

		String name = materialPath + materialName;

		auto findRet = mMaterials.find(name);

		if (findRet == mMaterials.end())
		{
			return nullptr;
		}

		return findRet->second;
	}

	uint32 MaterialManager::getMaterialCount() const
	{
		return mMaterials.size();
	}

	void MaterialManager::addMaterial(String const& materialFullName,std::shared_ptr<Material> materialPtr)
	{
#if UNG_DEBUGMODE
		auto getRet = getMaterial(materialFullName);
		BOOST_ASSERT(!getRet);
#endif

		mMaterials[materialFullName] = materialPtr;
	}

	void MaterialManager::removeMaterial(String const & materialName)
	{
		BOOST_ASSERT(!materialName.empty() && "material name invalid.");

#if UNG_DEBUGMODE
		auto getRet = getMaterial(materialName);

		BOOST_ASSERT(getRet && "this material has not been created.");
#endif

		auto eraseRet = mMaterials.erase(materialName);

		BOOST_ASSERT(eraseRet == 1);
	}
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE