#include "UFCColorValueDef.h"						//包含def文件

#ifdef UFC_HIERARCHICAL_COMPILE

namespace ung
{
	/*
	显式实例化的定义
	这里必须导出类，否则会链接错误。
	*/
	template class UngExport ColorValue<float>;
	template class UngExport ColorValue<double>;
}//UFC_HIERARCHICAL_COMPILE

#endif//UFC_HIERARCHICAL_COMPILE

/*
Direct3D:
RGB数据可用两种不同的结构来保存。
第一种是D3DCOLOR,它实际上是DWORD类型，共有32位。D3DCOLOR类型中的各位被分成四个8位
项(section)，每项存储了一种颜色分量的亮度值。(Alpha分量在最高位)
由于每种颜色分量均占用一个字节，所以每个分量的亮度值范围都在[0,255]区间内。接近0的值表示低亮度，
接近255的值表示高亮度。
要指定每个颜色分量的值，并将其插入到D3DCOLOR类型的恰当位置上，需要借助位运算。Direct3D提供了
D3DCOLOR_ARGB，D3DCOLOR_RGBA和D3DCOLOR_XRGB等宏帮助我们完成这样的工作。例如：
D3DCOLOR Red = D3DCOLOR_ARGB(255,255,0,0);
另一种是D3DCOLORVALUE。在该结构中，用单精度浮点数来度量每个颜色分量的亮度值，亮度值的范围为：
0.0~1.0(0.0表示没有亮度，1.0表示亮度最大)。该结构仅包含4个成员变量，而无任何方法。
我们也可以用结构D3DXCOLOR来替代D3DCOLORVALUE，前者包含了丰富的方法。
*/