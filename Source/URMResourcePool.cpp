#include "URMResourcePool.h"

#ifdef URM_HIERARCHICAL_COMPILE

#include "URMMesh.h"

namespace ung
{
	ResourcePool::ResourcePool()
	{
	}

	ResourcePool::~ResourcePool()
	{
		mPool.clear();
	}

	void ResourcePool::put(String const& fullName, pointer ptr)
	{
		BOOST_ASSERT(ptr);

#if UNG_DEBUGMODE
		auto findRet = take(fullName);
		BOOST_ASSERT(!findRet);
#endif

		mPool[fullName] = ptr;
	}

	typename ResourcePool::pointer ResourcePool::take(String const& fullName)
	{
		auto findRet = mPool.find(fullName);

		if (findRet == mPool.end())
		{
			return nullptr;
		}

		return findRet->second;
	}

	void ResourcePool::erase(String const & fullName)
	{
#if UNG_DEBUGMODE
		auto findRet = take(fullName);
		BOOST_ASSERT(findRet);
#endif

		auto eraseRet = mPool.erase(fullName);

		BOOST_ASSERT(eraseRet == 1);
	}

	void ResourcePool::erase(pointer ptr)
	{
		erase(ptr->getFullName());
	}

	uint32 ResourcePool::getCount() const
	{
		return mPool.size();
	}
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

/*
The Resource Cache:
Resource cache��a piece of technology that will sit on top of your resource files and manage 
the memory and the process of loading resources when you need them. Even better, a 
resource cache should be able to predict resource requirements before you need them.

Most of the bits you��ll need to display the next frame or play the next set of sounds are 
probably ones you��ve used recently.

A cache miss occurs when a game asks for the data associated with a resource and it isn��t 
there. The game has to wait while the hard drive or the optical media wakes up and reads 
the data. Cache misses can come in three types:
The first is a compulsory(ǿ���Ե�) miss, one that happens when the desired data is first 
requested and now has its first opportunity to load.
The second is a capacity miss, which happens when the cache is out of space and must throw 
something out to load in the desired data.
A conflict miss is the third type, which is a miss that could have been avoided,but the system 
was given hints that the data was no longer needed, and it was preemptively thrown out.
*/