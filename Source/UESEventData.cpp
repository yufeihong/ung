#include "UESEventData.h"

#ifdef UES_HIERARCHICAL_COMPILE

namespace ung
{
	EventData::EventData() :
		mTimeStamp(0.0)
	{
	}

	EventData::EventData(real_type timeStamp) :
		mTimeStamp(timeStamp)
	{
	}

	EventData::~EventData()
	{
	}

	real_type EventData::getTimeStamp() const
	{
		return mTimeStamp;
	}
}//namespace ung

#endif//UES_HIERARCHICAL_COMPILE