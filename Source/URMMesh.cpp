#include "URMMesh.h"

#ifdef URM_HIERARCHICAL_COMPILE

#include "URMMeshParserHelper.h"
#include "boost/signals2.hpp"

namespace ung
{
	std::shared_ptr<Resource> Mesh::creatorCallback(String const& fullName)
	{
		std::shared_ptr<Resource> mesh(UNG_NEW_SMART Mesh(fullName));

		BOOST_ASSERT(mesh);

		return mesh;
	}

	Mesh::Mesh(String const& fullName) :
		Resource(fullName,"Mesh")
	{
	}

	Mesh::~Mesh()
	{
	}

	void Mesh::build()
	{
		BOOST_ASSERT(!mIsLoaded);

		/*
		shared_from_this():这里不传递this，而是传递一个shared pointer，可以确保不会出现task还没有运行结束，但是对象的内存空间已经被释放的情况。
		*/
		auto bindLoad = std::bind(&Mesh::load, shared_from_this());
		auto fut = ThreadPool::getInstance().submit(bindLoad);
		setFuture(std::move(fut));
	}

	String const & Mesh::getDataFormat() const
	{
		/*
		这里调用isLoaded()的意思是：要想获取这个数据，就必须等待线程池中的某个线程完成载入任务。
		这里也可以使用递归锁，但是那样的话，当载入任务早已完成后，主线程每次访问这里的数据，都需要加一把锁，而调用isLoaded()就不会有效率影响，因为在
		isLoaded()函数中做了特殊处理，所谓的特殊处理，是建立在数据成员mIsLoaded不会被线程池中的线程访问的基础上。
		*/
		isLoaded();

		return mDataFormat;
	}

	String const & Mesh::getCoordSystem() const
	{
		isLoaded();

		return mCoordSystem;
	}

	usize const Mesh::getFaceCount() const
	{
		isLoaded();

		return mFaceCount;
	}

	usize const Mesh::getVertexCount() const
	{
		isLoaded();

		return mVertexCount;
	}

	usize const Mesh::getSubMeshCount() const
	{
		isLoaded();

		return mSubMeshCount;
	}

	STL_VECTOR(Vector3) const& Mesh::getPositions() const
	{
		isLoaded();

		return mPositions;
	}

	STL_VECTOR(Vector4) const& Mesh::getTangents() const
	{
		isLoaded();

		return mTangents;
	}

	STL_VECTOR(Vector3) const& Mesh::getBitangents() const
	{
		isLoaded();

		return mBitangents;
	}

	STL_VECTOR(Vector3) const& Mesh::getNormals() const
	{
		isLoaded();

		return mNormals;
	}

	STL_VECTOR(Vector2) const& Mesh::getUVs() const
	{
		isLoaded();

		return mUVs;
	}

	STL_VECTOR(std::shared_ptr<SubMesh>) const& Mesh::getSubMeshs() const
	{
		isLoaded();

		return mSubMeshs;
	}

	uint32 Mesh::getIndicesCount(uint32 subMeshIndex) const
	{
		isLoaded();

		BOOST_ASSERT(subMeshIndex < mSubMeshCount);

		return mSubMeshs[subMeshIndex]->getIndicesCount();
	}

	STL_VECTOR(uint32) const& Mesh::getIndices(uint32 subMeshIndex) const
	{
		isLoaded();

		BOOST_ASSERT(subMeshIndex < mSubMeshCount);

		return mSubMeshs[subMeshIndex]->getIndices();
	}

	String const& Mesh::getTrianglesMode(uint32 subMeshIndex) const
	{
		isLoaded();

		BOOST_ASSERT(subMeshIndex < mSubMeshCount);

		return mSubMeshs[subMeshIndex]->getTrianglesMode();
	}

	bool Mesh::load()
	{
		std::lock_guard<std::recursive_mutex> lg(mRecursiveMutex);

		using namespace boost::property_tree;

		ptree tree;
		readFile(&tree);

		mCoordSystem = tree.get<String>("Scene.CoordSystem");
		mDataFormat = tree.get<String>("Scene.Format");

		ptree currentMesh = tree.get_child("Scene.Mesh");

		mFaceCount = LexicalCast::stringToInt(currentMesh.get<String>("FaceCount"));
		mVertexCount = LexicalCast::stringToInt(currentMesh.get<String>("VertexCount"));

		//遍历Mesh.Positions
		for (auto& elem : currentMesh.get_child("Positions"))
		{
			mPositions.push_back(MeshParserHelper::stringToDVector3(elem.second.get_value<String>()));
		}
		BOOST_ASSERT(mPositions.size() == mVertexCount);

		//遍历Mesh.Tangents
		for (auto& elem : currentMesh.get_child("Tangents"))
		{
			mTangents.push_back(MeshParserHelper::stringToDVector4(elem.second.get_value<String>()));
		}
		BOOST_ASSERT(mTangents.size() == mVertexCount);

		//遍历Mesh.Bitangents
		for (auto& elem : currentMesh.get_child("Bitangents"))
		{
			mBitangents.push_back(MeshParserHelper::stringToDVector3(elem.second.get_value<String>()));
		}
		BOOST_ASSERT(mBitangents.size() == mVertexCount);

		//遍历Mesh.Normals
		for (auto& elem : currentMesh.get_child("Normals"))
		{
			mNormals.push_back(MeshParserHelper::stringToDVector3(elem.second.get_value<String>()));
		}
		BOOST_ASSERT(mNormals.size() == mVertexCount);

		//遍历Mesh.UVs
		for (auto& elem : currentMesh.get_child("UVs"))
		{
			mUVs.push_back(MeshParserHelper::stringToDVector2(elem.second.get_value<String>()));
		}
		BOOST_ASSERT(mUVs.size() == mVertexCount);

		//当前Mesh.SubMeshs中的所有SubMesh节点
		STL_VECTOR(ptree) subMeshsVec;
		for (auto& elem : currentMesh.get_child("SubMeshs"))
		{
			subMeshsVec.push_back(elem.second);
		}
		BOOST_ASSERT(subMeshsVec.size() == LexicalCast::stringToInt(tree.get<String>("Scene.MaterialCount")));

		mSubMeshCount = subMeshsVec.size();
		//遍历所有的SubMesh节点
		for (ufast j = 0; j < subMeshsVec.size(); ++j)
		{
			ptree currentSubMesh = subMeshsVec[j];

			//当前的SubMesh对象
			auto subMeshPtr = createSubMesh();

			String materialName = currentSubMesh.get<String>("MaterialName");
			//StringUtilities::toLower(materialName);
			subMeshPtr->setMaterialName(materialName);
			subMeshPtr->setTrianglesMode(currentSubMesh.get<String>("TrianglesMode"));
			subMeshPtr->setFaceCount(LexicalCast::stringToInt(currentSubMesh.get<String>("FaceCount")));
			subMeshPtr->setVertexCount(LexicalCast::stringToInt(currentSubMesh.get<String>("VertexCount")));
			subMeshPtr->setIndexCount(LexicalCast::stringToInt(currentSubMesh.get<String>("IndexCount")));

			//Indices
			for (auto& elem : currentSubMesh.get_child("Indices"))
			{
				subMeshPtr->addIndices(LexicalCast::stringToInt(elem.second.get_value<String>()));
			}

			using namespace boost::signals2;
			auto bindObject = std::bind(&SubMesh::buildMaterial, subMeshPtr);
			signal<void()> sig;
			sig.connect(bindObject);
			sig();
			//buildMaterial();
		}

		return true;
	}

	std::shared_ptr<SubMesh> Mesh::createSubMesh()
	{
		/*
		这里不能调用isLoaded()，原因是线程池中的某个线程要访问这里的数据，如果调用了isLoaded()，那么isLoaded()会等待线程池中的那个线程结束，这样就会
		造成死锁。而使用递归锁则没有问题，当线程池中的那个线程正在访问这里的数据的时候，其他线程（比如主线程）就等待这个锁释放后再访问。
		*/
		std::lock_guard<std::recursive_mutex> lg(mRecursiveMutex);

		std::shared_ptr<SubMesh> subMeshPtr(UNG_NEW_SMART SubMesh);

		BOOST_ASSERT(subMeshPtr);

		mSubMeshs.push_back(subMeshPtr);

		return subMeshPtr;
	}

	void Mesh::setFuture(std::future<bool>&& fut)
	{
		//只有主线程访问mNakedFuture，所以无需同步
		mNakedFuture = std::move(fut);
		//mNakedFuture = fut.share();

		//BOOST_ASSERT(!fut.valid());
		//BOOST_ASSERT(mNakedFuture.valid());

		//using namespace boost::signals2;

		//for (ufast i = 0; i < mSubMeshCount; ++i)
		//{
		//	auto bindObject = std::bind(&SubMesh::setFuture, mSubMeshs[i],mNakedFuture);
		//	//当signal析构时，将自动断开所有插槽连接，相当于调用disconnect_all_slots()。
		//	signal<void()> sig;
		//	sig.connect(bindObject);
		//	sig();
		//}
	}

	bool Mesh::isLoaded() const
	{
		/*
		线程池中的线程，不会访问mIsLoaded，这样，即便线程池中的线程正在执行task，而其它线程需要访问isLoaded()，读取到的mIsLoaded值也为false。
		*/
		if (mIsLoaded)
		{
			return mIsLoaded;
		}

		//auto a = mNakedFuture.valid();
		BOOST_ASSERT(mNakedFuture.valid());

		/*
		Class shared_future<>：
		get()是个const成员函数，返回一个const reference指向“存储于shared state”的值，这意味着你必须确定“被返回的reference”的寿命短于shared state。
		在这里，主线程可能会block。
		*/
		//bool const& getRet = mNakedFuture.get();														//阻塞
		bool getRet = mNakedFuture.get();
		BOOST_ASSERT(!mNakedFuture.valid());

		BOOST_ASSERT(getRet);

		//至此，线程池中的task已经结束，只有主线程访问，故，无需lock
		BOOST_ASSERT(!mIsLoaded);
		mIsLoaded = getRet;

		return mIsLoaded;
	}
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

/*
局部static数据，如果是POD类型的话，其执行值初始化，比如：局部：static int a;那么a为0。
如果在成员函数中定义一个局部static对象，在其他某地方定义了两个这个类的对象，比如object1和object2。
那么假如：
static int a;
++a;
当object1调用这个成员函数的时候，a开始为0，然后a为1。
当object2再调用这个成员函数的时候，a开始为1，然后a为2。
*/

/*
std::enable_shared_from_this：
provide member functions that create shared_ptr to this。

从std::enable_shared_from_this<>派生你自己的class，表现出“被shared pointer管理”的对象。
然后你就可以使用一个派生的成员函数shared_from_this()建立起一个源自this的正确shared_ptr。
注意：你不能在构造函数内调用shared_from_this()。

template<class Ty>
class enable_shared_from_this
{
public:
	shared_ptr<Ty> shared_from_this();
	shared_ptr<const Ty> shared_from_this() const;

protected:
	enable_shared_from_this();
	enable_shared_from_this(const enable_shared_from_this&);
	enable_shared_from_this& operator=(const enable_shared_from_this&);
	~enable_shared_from_this();
};
Objects derived from enable_shared_from_this can use the shared_from_this methods in member functions to create shared_ptr owners of the 
instance that share ownership with existing shared_ptr owners. Otherwise, if you create a new shared_ptr by using this, it is distinct(不同的) from 
existing shared_ptr owners, which can lead to invalid references or cause the object to be deleted more than once.
The constructors, destructor, and assignment operator are protected to help prevent accidental misuse. The template argument type Ty must be the 
type of the derived class.

struct base : public std::enable_shared_from_this<base>
{
	int val;

	shared_ptr<base> share_more()
	{
		return shared_from_this();
	}
};

int main()
{
	auto sp1 = make_shared<base>();
	auto sp2 = sp1->share_more();

	sp1->val = 3;
	cout << "sp2->val == " << sp2->val << endl;									//sp2->val == 3

	return 0;
}
*/

/*
shared pointer并非是线程安全的。
因此，为避免data race造成的不明确行为，当你在多个线程中以shared pointer指向同一对象，你必须使用mutex或lock等技术。
不过，当某个线程改变对象时，其他线程读取其使用次数并不会导致data race，虽然读到的值有可能不是最新的。现实中，我们的确可以让一个线程检查使用次数而
让另一个线程处理对象。
相当于寻常pointer之原子性C-Style接口，为shared pointer设计的重载版本允许并发处理shared pointer。
注意：并发访问的是pointer，而非其指向的值。
*/

/*
f.share():
产生一个shared_future带有当前状态，并令f的状态失效。
如果调用析构函数的那个future是某一shared future的最后拥有者，而相关的task已启动但尚未结束，析构函数会造成block，直到任务结束。
*/

/*
lock的效率说明：
std::recursive_mutex recursiveMutex;
void func1()
{
	std::lock_guard<std::recursive_mutex> lg(recursiveMutex);

	static int a = 0;
	++a;
}

void func2()
{
	static int a = 0;
	++a;
}
分别调用两个函数，并且都是仅在主线程中去调用，也就是说，没有block的消耗，仅查看lock的消耗，那么func1所消耗的时间是func2的15 ~ 20倍。
*/