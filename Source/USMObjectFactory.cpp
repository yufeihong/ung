#include "USMObjectFactory.h"

#ifdef USM_HIERARCHICAL_COMPILE

#include "USMObject.h"
#include "USMObjectComponent.h"
#include "USMTransformComponent.h"
#include "USMPhysicsComponent.h"

using namespace tinyxml2;

namespace ung
{
	ObjectFactory::ObjectFactory()
	{
		mComponentFactory.registerCreator<TransformComponent>("TransformComponent");
		mComponentFactory.registerCreator<PhysicsComponent>("PhysicsComponent");
		//mComponentFactory.registerCreator<MeshRenderComponent>(ObjectComponent::GetIdFromName(MeshRenderComponent::g_Name));
		//mComponentFactory.registerCreator<SphereRenderComponent>(ObjectComponent::GetIdFromName(SphereRenderComponent::g_Name));
		
		//mComponentFactory.registerCreator<TeapotRenderComponent>(ObjectComponent::GetIdFromName(TeapotRenderComponent::g_Name));
		//mComponentFactory.registerCreator<GridRenderComponent>(ObjectComponent::GetIdFromName(GridRenderComponent::g_Name));
		//mComponentFactory.registerCreator<LightRenderComponent>(ObjectComponent::GetIdFromName(LightRenderComponent::g_Name));
		//mComponentFactory.registerCreator<SkyRenderComponent>(ObjectComponent::GetIdFromName(SkyRenderComponent::g_Name));
		//mComponentFactory.registerCreator<AudioComponent>(ObjectComponent::GetIdFromName(AudioComponent::g_Name));

		////FUTURE WORK - certainly don't need to do this now, but the following stuff should be in a TeapotWarsActorFactory, eh?
		//mComponentFactory.registerCreator<AmmoPickup>(ObjectComponent::GetIdFromName(AmmoPickup::g_Name));
		//mComponentFactory.registerCreator<HealthPickup>(ObjectComponent::GetIdFromName(HealthPickup::g_Name));
		//mComponentFactory.registerCreator<BaseScriptComponent>(ObjectComponent::GetIdFromName(BaseScriptComponent::g_Name));
	}

	ObjectFactory::~ObjectFactory()
	{
	}

	StrongIObjectPtr ObjectFactory::createObject(StrongISceneManagerPtr sm,String const& objectDataFileFullName,String const& objectName)
	{
		//检查文件全名
		ungCheckFileFullName(objectDataFileFullName);

		//创建一个xml对象
		XMLDocument doc;

		//检查文件是否有效

		//载入并解析
		doc.LoadFile(objectDataFileFullName.data());
		BOOST_ASSERT(doc.ErrorID() == 0);

		/*
		获取xml的根节点
		XMLElement* tinyxml2::XMLDocument::RootElement():
		Return the root element of DOM.Equivalent to FirstChildElement().To get the first node,use FirstChild().
		*/
		XMLElement* pRoot = doc.RootElement();
		BOOST_ASSERT(pRoot);

		//创建对象实例
		StrongObjectPtr pObject;
		/*
		当内存池报告这里发生了内存泄露的话，那是因为没有销毁对象，请通过场景管理器来销毁创建
		的对象。
		*/
		if (objectName.empty())
		{
			pObject.reset(UNG_NEW_SMART Object(sm));
		}
		else
		{
			pObject.reset(UNG_NEW_SMART Object(sm,objectName));
		}
		BOOST_ASSERT(pObject);

		//在添加组件之前，进行一些初始化工作
		pObject->preInit(pRoot);

		/*
		遍历所有的child element
		const XMLElement* tinyxml2::XMLNode::FirstChildElement(const char* value = 0) const:
		Get the first child element, or optionally the first child element with the specified name.

		const XMLElement* NextSiblingElement(const char* value=0) const:
		Get the next (right) sibling element of this node,with an optionally supplied name.
		*/
		for (XMLElement* pComponentElement = pRoot->FirstChildElement(); pComponentElement; pComponentElement = pComponentElement->NextSiblingElement())
		{
			//创建组件
			StrongIObjectComponentPtr pComponent(createComponent(pComponentElement));
			BOOST_ASSERT(pComponent);

			//添加组件到对象的容器中
			pObject->addComponent(pComponent);

			//设置组件所属的对象
			pComponent->setOwner(pObject);
		}

		//Get the initial transform of the transform component set before the other components (like PhysicsComponent) read it.
		//std::shared_ptr<TransformComponent> pTransformComponent = ungGetStrongPtr(pEntity->getComponent<TransformComponent>("TransformComponent"));
		//if (pInitialTransform && pTransformComponent)
		//{
		//	pTransformComponent->SetPosition(pInitialTransform->GetPosition());
		//}

		//now that the actor has been fully created, run the post init phase
		pObject->postInit();

		return pObject;
	}

	StrongIObjectComponentPtr ObjectFactory::createComponent(XMLElement* pComponentElement)
	{
		/*
		获取Element的名字
		const char* Name() const:
		Get the name of an element (which is the Value() of the node.)
		*/
		String componentName(pComponentElement->Name());

		//通过工厂来创建组件
		StrongIObjectComponentPtr pComponent(mComponentFactory.create(componentName));
		BOOST_ASSERT(pComponent);

		//初始化上面创建的组件
		pComponent->init(pComponentElement);

		return pComponent;
	}
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE