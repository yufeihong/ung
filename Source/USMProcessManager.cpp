#include "USMProcessManager.h"

#ifdef USM_HIERARCHICAL_COMPILE

#include "USMProcess.h"

namespace ung
{

	ProcessManager::ProcessManager()
	{
	}

	ProcessManager::~ProcessManager()
	{
		clearAllProcesses();
	}

	std::pair<ufast, ufast> ProcessManager::updateAllProcesses(ufast deltaMS)
	{
		ufast successCount = 0;
		ufast failCount = 0;

		auto it = mProcesses.begin();
		while (it != mProcesses.end())
		{
			StrongProcessPtr pCurrProcess = (*it);

			//保存迭代器，以防万一我们要移除掉这个“过程”
			auto thisIt = it;
			++it;

			//初始化“过程”。(设置其状态为RUNNING，这样在当前帧该“过程”就可以运行)
			if (pCurrProcess->getState() == Process::INITIALIZED)
			{
				pCurrProcess->onInit();
			}

			//更新“过程”
			if (pCurrProcess->getState() == Process::RUNNING)
			{
				pCurrProcess->onUpdate(deltaMS);
			}

			//如果“过程”已经dead
			if (pCurrProcess->isDead())
			{
				//调用其退出函数
				switch (pCurrProcess->getState())
				{
					case Process::SUCCEEDED:
					{
						pCurrProcess->onSuccess();
						StrongProcessPtr pChild = pCurrProcess->removeChild();
						if (pChild)
						{
							addProcess(pChild);
						}
						else
						{
							//only counts if the whole chain completed
							++successCount;
						}
						break;
					}

					case Process::ABORTED:
					{
						pCurrProcess->onAbort();
						++failCount;
						break;
					}
				}

				//remove the process and destroy it
				mProcesses.erase(thisIt);
			}
		}

		return std::make_pair(successCount, failCount);
	}

	ProcessManager::WeakProcessPtr ProcessManager::addProcess(StrongProcessPtr pProcess)
	{
		mProcesses.push_front(pProcess);

		return WeakProcessPtr(pProcess);
	}

	void ProcessManager::abortAllProcesses(bool immediate)
	{
		auto it = mProcesses.begin();
		while (it != mProcesses.end())
		{
			auto tempIt = it;
			++it;

			StrongProcessPtr pProcess = *tempIt;
			if (pProcess->isAlive())
			{
				pProcess->setState(Process::ABORTED);

				if (immediate)
				{
					pProcess->onAbort();
					mProcesses.erase(tempIt);
				}
			}
		}
	}

	ufast ProcessManager::getProcessCount() const
	{
		return mProcesses.size();
	}

	void ProcessManager::clearAllProcesses()
	{
		mProcesses.clear();
	}
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE