#include "URMMaterial.h"

#ifdef URM_HIERARCHICAL_COMPILE

#include "URMMeshParserHelper.h"
#include "URMTextureManager.h"
#include "URMImage.h"
#include "boost/signals2.hpp"
#if UNG_DEBUGMODE
#include "UFCFilesystem.h"
#endif

//“ung::int32”: 将值强制为布尔值“true”或“false”(性能警告)
#pragma warning(disable : 4800)

namespace ung
{
	std::shared_ptr<Resource> Material::creatorCallback(String const& fullName)
	{
		std::shared_ptr<Resource> mat(UNG_NEW_SMART Material(fullName));

		BOOST_ASSERT(mat);

		return mat;
	}

	Material::Material() :
		Resource("../../../../Resource/Media/Materials/Properties/blank.mtl","Material")
	{
	}

	Material::Material(String const& fullName) :
		Resource(fullName,"Material")
	{
	}

	Material::~Material()
	{

	}

	void Material::build()
	{
		BOOST_ASSERT(!mIsLoaded);

		auto bindLoad = std::bind(&Material::load, shared_from_this());
		auto fut = ThreadPool::getInstance().submit(bindLoad);
		setFuture(fut);
	}

	Vector3 const & Material::getAmbient() const
	{
		isLoaded();
		
		return mAmbient;
	}

	Vector3 const & Material::getDiffuse() const
	{
		isLoaded();
		
		return mDiffuse;
	}

	Vector3 const & Material::getSpecular() const
	{
		isLoaded();
		
		return mSpecular;
	}

	real_type const & Material::getShininess() const
	{
		isLoaded();
		
		return mShininess;
	}

	real_type const & Material::getShininessStrength() const
	{
		isLoaded();
		
		return mShininessStrength;
	}

	real_type const & Material::getTransparency() const
	{
		isLoaded();
		
		return mTransparency;
	}

	String const & Material::getBlendMode() const
	{
		isLoaded();
		
		return mBlendMode;
	}

	bool const & Material::getTwoSide() const
	{
		isLoaded();
		
		return mTwoSide;
	}

	uint8 const & Material::getTextureCount() const
	{
		isLoaded();
		
		return mTextureCount;
	}

	STL_VECTOR(String) const& Material::getTextureNames() const
	{
		isLoaded();
		
		return mTextureNames;
	}

	void Material::setFuture(std::future<bool>& fut)
	{
		mNakedFuture = std::move(fut);

		BOOST_ASSERT(mNakedFuture.valid());
	}

	bool Material::load()
	{
		std::lock_guard<std::recursive_mutex> lg(mRecursiveMutex);

		using namespace boost::property_tree;

		ptree tree;
		readFile(&tree);

		//mFullName = tree.get<String>("Material.Name");
		BOOST_ASSERT(getFullName() == tree.get<String>("Material.Name"));
		//StringUtilities::toLower(mFullName);
		mAmbient = MeshParserHelper::stringToDVector3(tree.get<String>("Material.Ambient"));
		mDiffuse = MeshParserHelper::stringToDVector3(tree.get<String>("Material.Diffuse"));
		mSpecular = MeshParserHelper::stringToDVector3(tree.get<String>("Material.Specular"));
		mShininess = LexicalCast::stringToDouble(tree.get<String>("Material.Shininess"));
		mShininessStrength = LexicalCast::stringToDouble(tree.get<String>("Material.ShininessStrength"));
		mTransparency = LexicalCast::stringToDouble(tree.get<String>("Material.Transparency"));
		mBlendMode = tree.get<String>("Material.BlendMode");
		mTwoSide = static_cast<bool>(LexicalCast::stringToInt(tree.get<String>("Material.TwoSide")));
		mTextureCount = LexicalCast::stringToInt(tree.get<String>("Material.TextureCount"));

		//遍历Material.Textures
		using namespace boost::signals2;
		for (auto& elem : tree.get_child("Material.Textures"))
		{
			String textureName = elem.second.get_value<String>();
			StringUtilities::toLower(textureName);
			mTextureNames.push_back(textureName);

			//这里的参数，不能依靠默认参数
			auto bindObject = std::bind(&Image::build, std::move(TextureManager::getInstance().createTexture(textureName)));
			signal<void()> sig;
			sig.connect(bindObject);
			sig();
		}
		BOOST_ASSERT(mTextureNames.size() == mTextureCount);

		return true;
	}

	bool Material::isLoaded() const
	{
		if (mIsLoaded)
		{
			return mIsLoaded;
		}

		BOOST_ASSERT(mNakedFuture.valid());

		bool const& getRet = mNakedFuture.get();

		BOOST_ASSERT(getRet);

		BOOST_ASSERT(!mIsLoaded);
		mIsLoaded = getRet;

		//using namespace boost::signals2;

		//for (ufast i = 0; i < mTextureCount; ++i)
		//{
		//	std::shared_ptr<Image> texturePtr(UNG_NEW_SMART Image(mTextureNames[i]));

		//	//这里的参数，不能依靠默认参数
		//	auto bindObject = std::bind(&Image::build, texturePtr,true);
		//	signal<void()> sig;
		//	sig.connect(bindObject);
		//	sig();
		//}

		return mIsLoaded;
	}
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

/*
四种影响最终颜色的材质属性,以及它们的含义.
环境反射:近似的模拟了场景中的全局辐射.也就是用来近似模拟所有光在场景中不断散射的结果.材质中有相应
的属性来代表这种环境反射颜色.
漫反射:这种颜色是接收到直接从光源发射的光之后产生的,"漫反射"这个词来源于现实世界,描述光被物体反射到
各个方向的效果(换句话说就是散射).
放射:指的是自发光物体所拥有的颜色.这里有一个有趣的话题,因为在局部辐射光照模型中,放射光只能照亮自己
却不能对周围任何物体产生影响,有时候这种效果却可以让人觉得极其虚幻(设想一下,在你的房间里有一个灼热
的物体,却不会照亮周围任何东西,你就会了解这种感觉了).
镜面反射:描述了物体对被光照后的"高光"效果.设想一下假如你有一个红色皮球,而且擦得光亮.现在把它放在一
个高瓦度的灯泡下面,你就会在上面看到一个被称为镜面高光的亮斑.这是因为光线被光滑的表面直接反射到你的
眼睛中的缘故.

在这里镜面反射的高光的颜色是可以被用户定义的.这是因为在现实世界中,和环境光,漫反射以及辐射光不同,镜面
反射高光的颜色除了材质自身和光的颜色之外也受到周围其他的一些因素的影响.所以这里允许通过手动调整,使
用较低的代价来达到对真实世界的简单模拟.另外我们也可以手动调整镜面高光的反射能量,简单的说就是可以用
参数来控制红色皮球上"光斑"的大小,同时调节物体发亮的程度:能量大的时候,光斑变得又小又亮.

材质细节等级(Material LoD)
计算机图形学中的细节等级(Level of detail)这个术语经常是用来描述几何体复杂度等级和摄像机距离的关系.
同样的,这种描述也可以类似的用在材质上.例如说,你可以通过脚本把当前材质定义成多层纹理(用不同的方法来
混合它们),并且同时拥有顶点和片断两种GPU着色程序.这样在一个近的距离,便会在你的模型上面表现出"无与伦
比"的漂亮材质细节.然而,当这个模型在屏幕上缩小到1、2像素的时候(就是说距离变远了),你认为这样做还值得么?
答案明显是不!解决这个问题的方法就是使用材质细节等级(LoD)管理.好地解决方案是利用Ung可以让你在不
同的细节等级中使用不同的技术实现,进而更有效的利用当前GPU的资源.为了实现这个效果,你需要首先定义各个
细节等级所对应的实际距离,然后把相应的技术索引到这些等级中去.这里鼓励你在每个细节等级中尽可能多的设
置不同的技术实现,以供给不同的方案以及硬件选择出最适用的实现.
*/

/*
函数对象
在类中，可以重载函数调用运算符，使类的对象可以取代函数指针。这些对象称为函数对象，或称为仿函数。
C++提供了一些预定义的仿函数类，这些类定义在<functional>头文件中，执行最常用的回调操作。

算术函数对象
C++提供了5类二元算术运算符的仿函数类模板：plus,minus,multiplies,divides和modulus。此外还提供了一元的取反操作。
这些类对操作数的类型上模板化，是实际运算符的包装。

透明运算符仿函数
C++14支持透明运算符仿函数，允许忽略模板类型参数。例如，可以只指定multiplies<>()，而不是multiplies<int>()。
注意：建议总是使用透明运算符仿函数。

比较函数对象
C++语言提供了所有标准的比较：equal_to,not_equal_to,less,greater,less_equal,greater_equal。

逻辑函数对象
C++为3个逻辑操作提供了函数对象类：logical_not(operator!),logical_and(operator&&)和logical_or(operator||)。

按位函数对象
C++为所有按位操作添加了函数对象：bit_and(operator&),bit_or(operator|)和bit_xor(operator^)。
C++14增加了bit_not(operator~)。

函数对象适配器
在使用标准提供的基本函数对象时，往往会有不搭配的感觉。
函数适配器对函数组合（functional composition）提供了一些支持，即能够将函数组合在一起，以精确提供所需的行为。
1，绑定器（binder）
绑定器可用于将函数的参数绑定至特定的值。为此要使用<functional>头文件定义的std::bind()，它允许采用灵活的方式绑定函数的参数。既可以将函数的参数绑定
至固定值，甚至还能够重新安排函数参数的顺序。
举例，假定有一个func()函数，它接受两个参数：
void func(int num,const string& str)
{
	cout << "func(" << num << ", " << str << ")" << endl;
}
下面的代码演示了如何通过bind()将func()函数的第二个参数绑定至一个固定值myString。结果保存在f1()中。没有绑定至指定值的参数应该标记为_1,_2和_3等。这些
都定义在std::placeholders名称空间中。在f1()的定义中，_1指定了调用func()时，f1()的第一个参数应该出现的位置。之后，就可以用一个整形参数调用f1():
string myString = "abc";
auto f1 = bind(func,placeholders::_1,myString);
f1(16);
输出：
func(16,abc)

bind()还可以用于重新排列参数的顺序，如下列代码所示。_2指定了func()调用时，f2()的第二个参数应该出现的位置。换句话说，f2()绑定的意义是：f2()的第一个参数
将成为函数func()的第二个参数，f2()的第二个参数成为函数func()的第一个参数。
auto f2 = bind(func,placeholders::_2,placeholders::_1);
f2("Test",32);
输出
func(32,Test)

<functional>头文件定义了std::ref()和std::cref()辅助函数，它们分别用于绑定引用或const引用。例如，假定有如下函数：
void increment(int& value)
{
	++value;
}
如果调用了这个函数，如下所示，index的值就是1：
int index = 0;
increment(index);
如果使用bind()调用它，如下所示：index的值就不递增，因为建立了index的一个副本，并把这个副本的引用绑定到increment()函数的第一个参数上：
int index = 0;
auto incr = bind(increment,index);
incr();
使用std::ref()正确传递对应的应用，会递增index:
int index = 0;
auto incr = bind(increment,ref(index));
incr();
结合重载函数使用时，绑定参数会出现一个小问题。假设有下面两个名为overloaded()的重载函数。一个接受整数参数，另一个接受浮点数参数：
void overloaded(int num)
{
}
void overloaded(float f)
{
}
如果要对这些重载的函数使用bind()。那么必须显式地指定绑定这两个重载中的哪一个。下面的代码无法成功编译：
auto f3 = bind(overloaded,placeholders::_1);																	//ERROR
如果需要绑定接受浮点数参数的重载函数的参数，需要使用以下语法：
auto f4 = bind(void(*)(float)overloaded,placeholders::_1);												//OK

举例，通过find_if()算法找出序列中第一个大于等于100的元素：
auto endIter = end(myVector);
auto it = find_if(begin(myVector),endIter,bind(greater_equal<>(),placeholders::_1,100));

警告：在C++11之前有bind2nd()和bind1st()。两者都被C++11废弃，请改用lambda表达式和bind()。

2，取反器(negator)
取反器是类似于绑定器的函数，但是取反器计算谓词结果的反结果。
not1()计算作为参数传入的谓词的每个调用结果的反结果。not1()中的“1”表示这个操作数必须是一元函数（即接受一个参数的函数）。如果操作数是二元函数（即接受
两个参数的函数），那么必须改用not2()。

3，调用成员函数
假设有一个对象容器，有时需要传递一个指向类方法的指针作为算法的回调。例如，假设要对序列中的每个string调用empty()方法，找到string vector中的第一个空
string。然而，如果将指向string::empty()的指针传递给find_if()，这个算法无法知道接受的是指向方法的指针，而不是普通函数指针或仿函数。调用方法指针的代码
和调用普通函数指针的代码是不一样的，因为前者必须在对象的上下文内调用。
C++提供了mem_fn()转换函数，在传递给算法之前可以对函数指针调用这个函数。
void findEmptyString(const vector<string>& strings)
{
	auto endIter = end(strings);
	auto it = find_if(begin(strings),endIter,mem_fn(&string::empty));

	if(it == endIter)
	{
		cout << "No empty strings!" << endl;
	}
	else
	{
		cout << "Empty string at position: " << static_cast<int>(it - begin(strings)) << endl;
	}
}
mem_fn()生成一个用作find_if()回调的函数对象。如果容器内保存的不是对象本身，而是对象指针，mem_fn()的使用方法也完全一样。

编写自己的函数对象
如果需要完成不适合用lambda表达式执行的更复杂的任务，那么可以编写自己的函数对象，来执行预定义仿函数不能执行的更特定的任务。如果需要将函数适配器用于
这些仿函数，还必须提供特定的typedef。实现这一点最简单的方法是：根据是接受一个参数还是两个参数，从unary_function或binary_function派生自己的函数对象
类。这两个类定义在<functional>中，根据它们提供的函数的参数类型和返回类型进行模板化。
举例：
class myIsDigit : public unary_function<char, bool>
{
public:
	bool operator()(char c) const
	{
		return ::isdigit(c) != 0;
	}
};

bool isNumber(const string& str)
{
	auto it = find_if(begin(str),end(str),not1(myIsDigit()));

	return it == end(str);
}
注意，myIsDigit类中重载的函数调用运算符必须是const，才能将这个类的对象传递给find_if()。
警告：算法可以生成函数对象谓词的多份副本，并对不同的元素调用不同的副本。函数调用运算符必须为const，因此，编写仿函数时，不能让仿函数依赖在调用之间保持
一致的对象的任何内部状态。

在C++11之前，在函数作用域内局部定义的类不能用作模板参数。这个限制已经取消了。
bool isNumber(const string& str)
{
	class myIsDigit : public unary_function<char, bool>
	{
	public:
		bool operator()(char c) const
		{
			return ::isdigit(c) != 0;
		}
	};

	auto it = find_if(begin(str),end(str),not1(myIsDigit()));

	return it == end(str);
}
*/

/*
函数与回调
result_of，使用了复杂的技巧来自动推导函数的返回值类型
ref，可以包装对象的引用，在传递参数时消除对象拷贝的代价，或者将不可拷贝的对象变为可以拷贝。
bind，是C++标准库中函数适配器的增强，可以适配任意的可调用对象，包括函数指针，函数引用和函数对象，把它们变成一个新的函数对象。
function，对C++中函数指针类型的增强，它能够容纳任意的可调用对象，可以配合bind使用。
signals2，它实现了威力强大的观察者模式。

result_of
result_of是一个很小但很有用的组件，可以帮助程序员确定一个调用表达式的返回类型，主要用于泛型编程和其他Boost库组件，它已被收入C++11标准。
result_of位于名字空间boost，需要包含头文件"boost/utility/result_of.hpp"
所谓“调用表达式”，是指一个含有operator()的表达式，函数调用或函数对象调用都可以称为调用表达式，而result_of以模板元函数的方式确定这个表达式所返回的类型。
如果编译器支持C++11的decltype关键字，那么它就会利用decltype的能力。
typedef real_type (*Func)(real_type d);
这行代码定义了一个函数指针类型Func。
Func func = sqrt;
这行代码声明了Func的一个实例（变量）func，一个具体的函数指针，并把它赋值为sqrt---C标准库中的开平方数学函数。那么
result_of<Func(real_type)>::type x = func(5.0);
x的类型将被推导为real_type。
上面的代码不是很具有吸引力，用auto也可以完成同样的功能。但是，当处于一个泛型上下文之中，周围没有真实的类型，而且没有表达式的时候，auto就无能为力了，例如
template<typename T,typename T1>
??? call_func(T t,T1 t1)																				//T是个可调用的类型
{
	return t(t1);
}
无论如何，auto都派不上用场，这里不存在任何赋值表达式，只有函数调用式。
这正是result_of发挥威力的机会，它可以正确推导出返回类型，像这样：
template<typename T,typename T1>
typename result_of<T(T1)>::type call_func(T t,T1 t1)
{
	return t(t1);
}

int main()
{
	typedef real_type (*Func)(real_type d);
	Func func = sqrt;

	auto x = call_func(func,5.0);																	//赋值表达式，可以用auto
}

ref
STL和Boost中的算法和函数大量使用了函数对象作为判断式或谓词参数，而这些参数都是传值语义，算法或函数在内部保留函数对象的拷贝并使用。
一般情况下传值语义都是可行的，但也有很多特殊情况：作为参数的函数对象拷贝代价过高（具有复杂的内部状态），或者不希望拷贝对象，甚至拷贝是不可行的（noncopyable,
单件）。
ref应用代理模式，引入对象引用的包装器概念解决了这个问题，同样被收入C++11标准。它位于名字空间boost，需要头文件"boost/ref.hpp"。
可以让容器安全地持有被包装的引用对象。

bind
位于名字空间boost，"boost/bind.hpp"
已经被收入C++11标准。
bind接受的第一个参数必须是一个可调用对象f，包括函数，函数指针，函数对象和成员函数指针，之后bind接受最多9个参数，参数的数量必须与f的参数数量相等，这些
参数将被传递给f作为入参。
绑定完成后，bind会返回一个函数对象，它内部保存了f的拷贝，具有operator()，返回值类型被自动推导为f的返回值类型。在发生调用时，这个函数对象将把之前存储的
参数转发给f完成调用。

例如，有一个函数func，它的形式是：
func(a1,a2)
那么，它将等价于一个具有无参operator()的bind函数对象调用：
bind(func,a1,a2)()
这是bind最简单的形式。bind表达式存储了func和a1，a2的拷贝，产生了一个临时函数对象。因为func接受两个参数，而a1和a2都是实参，因此临时函数对象将具有一个
无参的operator()。当operator()调用发生时函数对象把a1，a2的拷贝传递给func，完成真正的函数调用。
bind的真正威力在于它的占位符，占位符可以取代bind中参数的位置，在发生函数调用时才接受真正的参数。

绑定普通函数
必须在绑定表达式中提供函数要求的所有参数，无论是真实参数还是占位符均可以。

绑定成员函数
类的成员函数不同于普通函数，因为成员函数指针不能直接调用operator()，它必须被绑定到一个对象或者指针，然后才能得到this指针进而调用成员函数。因此bind需要
牺牲一个占位符的位置，要求用户提供一个类的实例，引用或者指针，通过对象作为第一个参数来调用成员函数。（实际上成员函数的第一个隐含的参数就是对象指针）
这意味着使用成员函数时只能最多绑定8个参数。
class Demo
{
public:
	int f(int a, int b)
	{
		return a + b;
	}
};

Demo a, &ra = a;																					//实例，引用
Demo* p = &a;																						//指针

cout << bind(&Demo::f, a, _1, 20)(10) << endl;
cout << bind(&Demo::f, ra, _2, _1)(10,20) << endl;
cout << bind(&Demo::f, a, p, _1,_2)(10,20) << endl;

我们必须在成员函数前加上取地址操作符&，表明这是一个成员函数指针，否则会无法通过编译。
bind同样支持绑定虚拟成员函数，用法与非虚函数相同，虚函数的行为将由实际调用发生时的实例来决定。

绑定成员变量
bind的另一个对类的操作是它可以绑定public成员变量，就像是一个选择器，用法与绑定成员函数类似，只需要把成员变量名像一个成员函数一样去使用。
举例，假设我们要获取其x坐标：
vector<point> v(10);
vector<int> v2(10);
transform(v.begin(), v.end(), v2.begin(), bind(&point::x,_1));
代码中的bind(&point::x,_1)取出point对象的成员变量x，transform算法调用bind表达式操作容器v，逐个把成员变量填入到v2中。
使用bind，可以实现SGI_STL/STLport中的非标准函数适配器select1st和select2nd的功能，直接选择出pair对象的first和second成员：
typedef pair<int, string> pair_t;
pair_t p(123, "string");
cout << bind(&pair_t::first, p)() << endl;
cout << bind(&pair_t::second, p)() << endl;

绑定函数对象
bind不仅能够绑定函数和函数指针，也能够绑定任意的函数对象，包括标准库中的所有预定义的函数对象。
如果函数对象有内部类型定义result_type，那么bind可以自动推导出返回值类型，用法与绑定普通函数一样。但如果函数对象没有定义result_type，则需要用模板参数
指明返回类型。
标准库和Boost库中的大部分函数对象都具有result_type定义，因此不需要特别的形式就可以直接使用bind，例如：
bind(std::greater<int>(),_1,10);																	//检查x > 10
bind(plus<int>(),_1,_2);																				//执行x + y
bind(modulus<int>(),_1,3);																			//执行x % 3
对于自定义函数对象，如果没有result_type类型定义，例如：
class Demo
{
public:
	int operator()(int a,int b)
	{
		return a + b;
	}
};
那么我们必须指明bind的返回值类型，像这样：
cout << bind<int>(Demo(),_1,_2)(10,20) << endl;

存储bind表达式
很多时候我们需要把写好的bind表达式存储起来，以便稍后再度使用。例如：
auto x = bind(greater<int>(),_1,_2);
cout << x(10,20) << endl;
function库也可以达到同样的目的，但它的功能更加强大。

嵌套绑定
bind可以嵌套，一个bind表达式生成的函数对象可以被另一个bind再绑定，从而实现类似f(g(x))的形式。
如果我们有f(x)和g(x)两个函数，那么f(g(x))的bind表达式就是：
bind(f,bind(g,_1))(x);

操作符重载
bind生成的函数对象重载了比较操作符和逻辑非操作符，可因此把多个bind绑定式组合起来，形成一个复杂的逻辑表达式，配合标准库算法可以实现语法简单但语义复杂
的操作。
例如，假设我们有一个存储有理数rational的vector，那么使用bind可以执行许多具有复杂逻辑的操作：
using namespace boost::assign;
typedef rational<int> ri;
vector<ri> v = list_of(ri(1,2))(ri(3,4))(ri(5,6));

//删除所有分子为1的有理数
remove_if(v.begin(),v.end(),bind(&ri::numerator,_1) == 1);
assert(v[0].numerator() == 3);

//使用find_if算法查找分子是1的有理数（不存在）
assert(find_if(v.begin(),v.end(),bind(&ri::numerator,_1) == 1) == v.end());

//查找分子大于3且分母小于8的有理数
BOOST_AUTO(pos,find_if(v.begin(),v.end(),bind(&ri::numerator,_1) > 3 && bind(&ri::denominator,_1) < 8));
cout << *pos << endl;

很多情况下复杂的逻辑判断可以使用函数内部类来就地定义，形式上要比bind组合清晰：
struct pred
{
	bool operator()(ri &r)
	{
		return r.numerator() > 3 && r.denominator() < 8);
	}
};
pos = find_if(v.begin(),v.end(),pred());

绑定非标准函数
bind的标准形式不支持使用了不同的调用方式(__stdcall,__fastcall,extern "C")的函数，通常bind把它们看作函数对象，需要显式地指定bind的返回类型才能绑定。
也可以在头文件"boost/bind.hpp"之前加上BOOST_BIND_ENABLE_STDCALL,BOOST_BIND_ENABLE_FASTCALL,BOOST_BIND_ENABLE_PASCAL等宏，
明确地告诉bind支持这些调用方式。

function
function可以容纳一个函数对象，也有点像函数指针类型的泛化。
它以对象的形式封装了原始的函数指针或函数对象，能够容纳任意符合函数签名的可调用对象。因此，它可以被用于回调机制，暂时保管函数或函数对象，在之后需要的
时候再调用，使回调机制拥有更多的弹性。它已经被收入C++11标准。
function可以配合bind使用，存储bind表达式的结果，使bind可以被多次调用。
位于名字空间boost，"boost/function.hpp"

function的构造函数可以接受任意符合模板中声明的函数类型的可调用对象，如函数指针和函数对象，也可以是另一个function对象的引用，之后在内部存储一份它的拷贝。
无参的构造函数或者传入空指针构造将创建一个空的function对象，不持有任何可调用物，调用空的function对象将抛出bad_function_call异常，因此在使用function
前最好检测一下它的有效性。可以用empty()测试function是否为空，或者用重载操作符operator!来测试。function对象也可以在一个bool语境中直接测试它是否为空，
它是类型安全的。
function的其余成员函数功能如下：
clear()可以直接将function对象置空，它与使用operator=赋值0具有同样的效果。
模板成员函数target()可以返回function对象内部持有的可调用物Functor的指针，如果function为空则返回空指针nullptr。
contains()可以检测function是否持有一个Functor对象。
function提供了operator()，它把传入的参数转交给内部保存的可调用物，完成真正的函数调用。

function重载了operator==和operator!=，可以与被包装的函数或函数对象进行比较。例如：
function<int(int,int)> func(f);
assert(func == f);
如果function存储的是函数对象，那么要求函数对象必须重载了operator==，是可比较的。
两个function对象不能使用==和!=直接比较，这是特意的。因为function存在bool的隐式转换，function定义了两个function对象的operator==但没有实现，企图比较
两个function对象会导致编译错误。

function这种能够容纳任意可调用对象的能力是非常重要的，在编写泛型代码的时候尤其有用，它使我们可以接受任意的函数或函数对象，增加程序的灵活性。
与原始的函数指针相比，function对象的体积要稍微大一点（3个指针的大小），速度要稍微慢一点（10%左右的性能差距）
int f(int a, int b)
{
	return a + b;
}

int main()
{
	function<int(int, int)> func;																					//无参构造一个function对象
	assert(!func);																										//此时function不持有任何对象

	func = f;																											//func存储了函数f
	if (func)																												//function可以转换为bool值
	{
		cout << func(10, 20);																						//调用function的operator()
	}

	func = 0;																											//function清空，相当于clear()
}

只要函数签名一致，function也可以存储成员函数和函数对象，或者是bind表达式的结果。
struct Demo
{
	int add(int a, int b)
	{
		return a + b;
	}

	int operator()(int x) const
	{
		return x * x;
	}
};
存储成员函数时可以直接在function声明的函数签名式中指定类的类型，然后用bind绑定成员函数：
function<int(Demo&, int, int)> func1;
func1 = bind(&Demo::add, _1, _2, _3);
Demo obj;
cout << func1(obj, 10, 20);
也可以在函数类型中仅写出成员函数的签名，在bind时直接绑定类的实例：
function<int(int, int)> func2;
func2 = bind(&Demo::add, &obj, _1, _2);
cout << func2(10, 20);

function使用拷贝语义保存参数，但参数很大时拷贝的代价很高，或者有时候不能拷贝参数。这是就可以使用ref，它允许以引用的方式传递参数，能够降低拷贝的代价。
function并不要求ref提供operator()，因为它能够自动识别包装类reference_wrapper<T>，并调用get()方法获得被包装的对象。
Demo obj;
function<int(int)> func;
func = cref(obj);
cout << func(10);
注意，这里使用的是cref()，它是一个常引用包装，因此只能调用const成员函数。
function能够直接调用被ref包装的函数对象。下面的代码具有内部状态：
template<typename T>
struct summary
{
	typedef void result_type;

	summary(T v = T()) :
		mSum(v)
	{
	}

	void operator()(T const& x)
	{
		mSum += x;
	}

	T mSum;																														//内部状态
};
标准库算法使用拷贝语义，算法内部的改变不能影响到原对象。
int main()
{
	using namespace boost::assign;
	vector<int> v = (list_of(1),3,5,7,9);

	summary<int> obj;																											//有状态的函数对象
	function<void(int const&)> func(ref(obj));

	std::for_each(v.begin(),v.end(),func);
	cout << obj.mSum << endl;																							//函数对象的状态被改变
}

function可以容纳任意符合函数签名的可调用物，因此它非常适合代替函数指针，存储用于回调的函数。
class Demo
{
private:
	typedef function<void(int)> func_type;
	func_type func;
	int value;

public:
	Demo(int i) :
		value(i)
	{
	}

	template<typename CallBack>
	void accept(CallBack f)																											//赋值，接受回调函数
	{
		func = f;
	}

	void run()																															//用于调用回调函数
	{
		func(value);
	}
};

void callback_func(int i)																											//被回调的函数
{
	cout << "callback_func:" << i * 2 << endl;
}

int main()
{
	Demo obj;
	obj.accept(callback_func);																									//接受回调函数
	obj.run();																															//调用回调函数
}
使用普通的C函数进行回调并不能体现function的好处，下面演示一个带状态的函数对象，并使用ref传递引用。
也就是说，用函数对象来替代普通的C函数：
class functionObj
{
private:
	int x;

public:
	functionObj(int i) :
		x(i)
	{
	}

	void operator()(int i)
	{
		cout << "functionObj:" << i * x++ << endl;																			//先做乘法，然后递增
	}
};

int main()
{
	Demo obj(10);
	functionObj fObj(2);
	obj.accept(ref(fObj));																												//使用ref包装函数对象
	obj.run();																																//输出functionObj:20
	obj.run();																																//输出functionObj:30
}
Demo类因为使用了function作为内部可调用物的存储，因此不用做任何改变，即可接受函数指针也可接受函数对象，给使用者提供了方便。

function还可以搭配bind，把bind表达式作为回调函数，可以接受类成员函数，或者把不符合函数签名的函数bind为可接受的形式。
obj.accept(bind(&class::f,classObj,_1));
通过上面的几个代码片段，我们可以看到function用于回调的好处，它无需改变回调的接口就可以解耦客户代码，使客户代码不必绑死在一种回调形式上。

有时候auto可以近似地取代function，但它们的实现有很大的不同。function类似一个容器，可以容纳任意有operator()的类型（函数指针，函数对象，lambda表达式），
它是运行时的，可以任意拷贝，赋值，存储其他可调用物。而auto/BOOST_AUTO仅是在编译期推导出的一个静态类型变量，它很难再赋以其他值，也无法容纳其他的类型，
不能用于泛型编程。
当需要存储一个可调用物用于回调的时候，最好使用function，它具有更多的灵活性，特别是把回调作为类的一个成员的时候我们只能使用function。
auto也有它的优点，它的类型是在编译期推导的，没有运行时的开销，效率上要比function略好一点。但它声明的变量不能存储其他类型的可调用物，不具有灵活性，只能
用于有限范围的延后回调。

signals2
signals2基于Boost的signals库，实现了线程安全的观察者模式。在signals2库中，观察者模式被称为信号/插槽(signals and slots)，它是一种函数回调机制，一个信号
关联了多个插槽，当信号发出时，所有关联它的插槽都会被调用。其另一个名称是事件处理机制(event/event handler)，它可以很好地解耦一组互相协作的类，signals2
以库的形式为C++增加了这个重要的功能。
"boost/signals2.hpp"

signals2库的核心是signal类。
template<typename Signature,typename Combiner = boost::signals2::optional_last_value<R>,typename Group = int,typename GroupCompare = std::less<Group>>
class signal : public boost::signals2::signal_base
{
};
第一个模板参数Signature的含义与function的一模一样，也是一个函数类型签名，表示可被signal调用的函数（插槽，事件处理handler）。例如：
signal<void(int,double)>
第二个模板参数Combiner是一个函数对象，它被称为“合并器”，用来组合所有插槽的调用结果，默认是optional_last_value<R>，它使用optional库返回最后一个被调用
的插槽的返回值。
第三个模板参数Group是插槽编组的类型，缺省使用int来标记组号，也可以改为string等类型。
第四个模板参数GroupCompare与Group配合使用，用来确定编组的排序准则，默认是升序std::less<Group>，因此要求Group必须定义了operator<。

signal继承自signal_base，而signal_base又继承自noncopyable，因此signal是不可拷贝的，如果把signal作为自定义类的成员变量，那么自定义类也将是不可拷贝的，
除非使用shared_ptr来包装它。

signal最重要的操作函数是插槽管理connect()函数，它把插槽连接到信号上，相当于为信号（事件）增加了一个处理的handler。
插槽可以是任意的可调用对象，包括函数指针，函数对象，以及它们的bind表达式和function对象。signal内部使用function作为容器来保存这些可调用对象。连接时可以
指定组号也可以不指定组号，当信号发生时将依据组号的排序准则依次调用插槽函数。
如果连接成功，connect()将返回一个connection对象，表示了信号与插槽之间的连接关系，它是一个轻量级的对象，可以处理两者间的连接，如断开，重连接或者测试
连接状态。
成员函数disconnect()可以断开插槽与信号的连接，它有两种形式：传递组号将断开该组的所有插槽，传递一个插槽对象将仅断开该插槽。函数disconnect_all_slots()
可以一次性断开信号的所有插槽连接。
当前信号所连接的插槽数量可以用num_slots()获得，成员函数empty()相当于num_slots == 0，但它的执行效率比num_slots()高。disconnect_all_slots()的后果
就是令empty()返回true。
signal提供operator()，可以接受最多9个参数。当operator()被外界调用时意味着产生一个信号（事件），从而导致信号所关联的所有插槽被调用。插槽调用的结果使用
合并处理后返回，默认情况下是一个optional对象。
成员函数combiner()和set_combiner()分别用于获取和设置合并器对象，通过signal的构造函数也可以在创建的时候就传入一个合并器的实例。但通常我们可以直接使用
缺省构造函数创建模板参数列表中指定的合并器对象，除非你想改用其他的合并方式。
当signal析构时，将自动断开所有插槽连接，相当于调用disconnect_all_slots()。

signal就像是一个增强的function对象，它可以容纳（使用connect()连接）多个符合模板参数中函数签名类型的函数（插槽），形成一个插槽链表，然后在信号发生时一起
调用。
例如，有下面两个函数，它们可以被用作插槽：
void slots1()
{
	cout << "slots1 called" << endl;
}
void slots2()
{
	cout << "slots2 called" << endl;
}
除了类名字不同，signal的声明语法与function几乎一模一样：
signal<void()> sig;																											//指定插槽类型void()，其他模板参数使用缺省值
然后我们就可以使用connect()来连接插槽，最后用operator()来产生信号：
int main()
{
	signal<void()> sig;																										//一个信号对象

	sig.connect(&slots1);																									//连接插槽
	sig.connect(&slots2);

	sig();																															//调用operator()，产生信号（事件），触发插槽调用
}
在连接插槽时我们省略了connect()的第二个参数connect_position，它的缺省值是at_back，表示插槽将插入到信号插槽链表的尾部，因此slots2将在slots1之后被
调用。也可以在连接slots2的时候明确地传入at_front位置标志。

connect()函数的另一个重载形式可以在连接时指定插槽所在的组号，缺省情况下组号是int类型。组号不一定要从0开始连续编号，它可以是任意的数值，离散的，负值都
允许。
如果在连接的时候指定组号，那么每个编组的插槽将是有一个插槽链表，形成一个略微有些复杂的二维链表，它们的顺序规则如下：
1，各编组的调用顺序由组号从小到大决定
2，每个编组的插槽链表内部的插入顺序用at_back和at_front指定
3，未被编组的插槽如果位置标志是at_front，将在所有的编组之前调用
4，未被编组的插槽如果位置标志是at_back，将在所有的编组之后调用
sig.connect(5,&slots1,at_back);																								//5,组号

signal如function一样，不仅可以把输入参数转发给所有插槽，也可以传回插槽的返回值。默认情况下signal使用合并器optional_last_value<R>，它将使用optional
对象返回最后被调用的插槽的返回值。
template<int N>
struct slots
{
	int operator()(int x)
	{
		cout << "slot " << N << " called" << endl;
		return x * N;
	}
};

signal<int(int)> sig;

sig.connect(slots<10>());
sig.connect(slots<20>());
sig.connect(slots<50>());

signal的operator()调用这时需要传入一个整数参数，这个参数会被signal存储一个拷贝，然后转发给各个插槽。最后signal将返回插槽链表末尾slots<50>()的计算结果，
它是一个optional对象，必须用解引用操作符*来获得值，即：
cout << *sig(2);																													//输出100

缺省的合并器optional_last_value<R>并没有太多的意义，它通常用在我们不关心插槽返回值或者返回值是void的时候。但大多数时候，插槽的返回值都是有意义的，需要
以某种方式处理多个插槽的返回值。
signal允许用户自定义合并器来处理插槽的返回值，把多个插槽的返回值合并为一个结果返回给用户。合并器应该是一个函数对象（不是函数或者函数指针），具有类似如下
的形式：
template<typename T>
class combiner
{
public:
	typedef T result_type;

	template<typename InputIterator>
	result_type operator()(InputIterator,InputIterator) const;
};
combiner类的调用操作符operator()的返回值类型可以是任意类型，完全由用户指定，不一定必须是optional或者是插槽的返回值类型。operator()的模板参数InputIterator
是插槽链表的返回值迭代器，可以使用它来遍历所有插槽的返回值，进行所需的处理。
下面例子，使用pair返回所有插槽的返回值之和以及其中的最大值：
template<typename T>
class combiner
{
private:
	T v;

public:
	typedef std::pair<T, T> result_type;

	combiner(T t = T()) :
		v(t)
	{
	}

	template<typename InputIterator>
	result_type operator()(InputIterator begin, InputIterator end) const
	{
		if (begin == end)
		{
			return result_type();
		}

		vector<T> vec(begin, end);																								//使用容器保存插槽调用结果

		T sum = accumulate(vec.begin(), vec.end(), v);
		T max = *std::max_element(vec.begin(), vec.end());

		return result_type(sum, max);
	}
};
使用自定义合并器的时候我们需要改写signal的声明，在模板参数列表中增加第二个模板参数---合并器类型：
signal<int(int),combiner<int>> sig;
在这里我们没有向构造函数传递合并器的实例，因为signal的构造函数会缺省构造出一个实例，相当于：
signal<int(int),combiner<int>> sig(combiner<int>());
如果我们不适用signal的缺省构造函数，而是在构造signal时传入一个合并器的实例，那么signal将使用这个合并器的拷贝处理返回值。

信号与插槽的连接并不要求是永久的，当信号调用完插槽后，有可能需要把插槽从信号中断开，再连接到其他的信号上去。
要断开一个插槽，插槽必须能够进行等价比较，对于函数对象来说就是重载一个等价语义的operator==。
使用signal自身管理插槽有些不方便，因为它必须知道与它连接的所有插槽的信息，还要求插槽对象必须是可等价比较的。

signals2库提供另外一种较为灵活的连接管理方式：使用connection对象。
每当signal使用connect()连接插槽时，它就会返回一个connection对象。connection对象像是信号与插槽连接关系的一个句柄，可以管理连接。
connection是可拷贝可赋值的，它也重载了比较操作符，因而可以被安全地放入标准序列容器或者关联容器中，成员函数disconnect()和connected()分别用来与信号
断开连接和检测连接状态。例如：
int main()
{
	signal<int(int)> sig;

	connection c1 = sig.connect(0,slots<10>());
	connection c2 = sig.connect(0,slots<20>());
	connection c3 = sig.connect(1,slots<30>());

	c1.disconnect();																											//断开第一个连接
	assert(sig.num_slots() == 2);
	assert(!c1.connected());
	assert(c2.connected());
}

另外一种连接管理对象是scoped_connection，它是connection的子类，提供RAII功能：插槽与信号的连接仅在作用域内生效，当离开作用域时连接就会自动断开。
connection不提供"重新连接"这样的函数，插槽与信号的连接一旦断开就不能再连接起来。但可以暂时地阻塞插槽与信号的连接，当信号发生时被阻塞的插槽将不会
被调用，connection对象的blocked()函数可以检测插槽是否被阻塞。但被阻塞的插槽并没有断开与信号的连接，在需要的时候可以随时解除阻塞。
connection对象自身没有阻塞的功能，它需要一个辅助类shared_connection_block，它将阻塞connection对象，直到它被析构或者显式调用unblock()函数。
下面的代码演示连接阻塞：
int main()
{
	signal<int(int)> sig;

	connection c1 = sig.connect(0, slots<10>());
	connection c2 = sig.connect(0, slots<20>());

	assert(sig.num_slots() == 2);																						//有两个插槽连接

	sig(2);																														//调用这两个已经连接的插槽

	cout << "begin blocking..." << endl;

	{
		shared_connection_block block(c1);																			//阻塞c1连接
		assert(sig.num_slots() == 2);																					//仍然有两个连接
		assert(c1.blocked());																								//c1被阻塞
		sig(2);																													//只有一个插槽会被调用
	}																																//离开作用域，阻塞自动解除

	cout << "end blocking..." << endl;

	assert(!c1.blocked());
	sig(2);																														//可以调用两个插槽
}

存在一个问题：如果插槽在与信号建立连接后被意外地销毁了，那么信号调用将发生未定义行为。
signals2::slot模板类可以自动管理插槽的连接，但通常我们不直接使用它，而是使用signal的内部slot_type类型，它已经定义好了该signal所使用的slot的模板参数。
要使用自动管理连接的功能，在信号连接时我们不能直接连接插槽，而是要用slot的构造函数包装插槽，然后再用成员函数track()来跟踪插槽使用的资源。
int main()
{
	typedef signal<int(int)> signal_type;
	signal_type sig;

	sig.connect(slots<10>());																							//连接一个普通的插槽
	shared_ptr<slots<20>> p(new slots<20>);

	sig.connect(signal::slot_type(ref(*p)).track(p));

	p.reset();																													//销毁插槽
	assert(sig.num_slots() == 1);
	sig(1);																														//只有一个插槽被调用
}
slot的构造函数有一个有趣的地方是支持bind表达式相同的语法，这样可以就地绑定函数，避免使用bind表达式的构造成本。
int main()
{
	typedef signal<int(int)> signal_type;
	typedef signal_type::slot_type slot_type;

	signal_type sig;

	shared_ptr<slots<10>> p1(new slots<10>);
	shared_ptr<slots<20>> p2(new slots<20>);

	function<int(int)> func = ref(*p1);																				//function存储引用

	sig.connect(slot_type(func).track(p1));																			//直接跟踪function
	sig.connect(slot_type(&slots<20>::operator(), p2.get(), _1).track(p2));							//使用bind语法，直接绑定

	p1.reset();
	p2.reset();
	assert(sig.num_slots() == 0);
	sig(1);																														//不发生任何插槽调用
}
注意：在使用bind语法时我们必须传递给slot原始指针，否则slot会持有一个shared_ptr的拷贝，导致引用计数增加，妨碍了shared_ptr的资源管理。

下面的示例演示一个完整的观察者模式：客人按门铃，门铃响，护士开门，婴儿哭闹。(护士和婴儿之间没关系)
class ring
{
public:
	typedef signal<void()> signal_type;
	typedef signal_type::slot_type slot_type;

	connection connect(const slot_type& s)
	{
		return mAlarm.connect(s);
	}

	void press()
	{
		cout << "Ring alarm..." << endl;
		mAlarm();																													//调用signal，发出信号，引发插槽调用
	}

private:
	signal_type mAlarm;
};

typedef variate_generator<rand48, uniform_smallint<>> bool_rand;
bool_rand g_rand(rand48(time(0)), uniform_smallint<>(0, 100));

extern char const nurse1[] = "Mary";
extern char const nurse2[] = "Kate";
//注意：模板参数使用了char const*，所以实例化时字符串必须被声明成extern。也可以用boost.mpl库里的编译期字符串类mpl::string。
template<char const* name>
class nurse
{
private:
	bool_rand& rand;																											//随机数发生器

public:
	nurse() :
		rand(g_rand)
	{
	}

	void action()
	{
		cout << name;
		if (rand() > 30)
		{
			cout << " wakeup and open door." << endl;
		}
		else
		{
			cout << " is sleeping..." << endl;
		}
	}
};

extern char const baby1[] = "Tom";
extern char const baby2[] = "Jerry";

template<char const* name>
class baby
{
private:
	bool_rand& rand;

public:
	baby() :
		rand(g_rand)
	{
	}

	void action()
	{
		cout << "Baby " << name;
		if (rand() > 50)
		{
			cout << " wakeup and crying loudly..." << endl;
		}
		else
		{
			cout << " is sleeping sweetly..." << endl;
		}
	}
};

class guest
{
public:
	void press(ring& r)
	{
		cout << "A guest press the ring." << endl;
		r.press();
	}
};

int main()
{
	ring r;
	nurse<nurse1> n1;
	nurse<nurse2> n2;
	baby<baby1> b1;
	baby<baby2> b2;
	guest g;

	//把护士、婴儿与门铃连接起来
	r.connect(bind(&nurse<nurse1>::action,n1));
	r.connect(bind(&nurse<nurse2>::action,n2));
	r.connect(bind(&baby<baby1>::action,b1));
	r.connect(bind(&baby<baby2>::action,b2));

	//客人按门铃，触发一系列事件
	g.press(r);

	return 0;
}

signal是noncopyable的子类，这意味着它不能被拷贝或者赋值，因此，如果我们的自定义类有signal成员变量，那么自定义类也将是不能拷贝的。解决办法是使用
shared_ptr<signal<Signature>>作为类的成员，shared_ptr可以很好地管理signal的共享语义。

因为signal会自动把解引用操作转换为插槽调用，所以自定义合并器某种程度上也相当于一个插槽调度器。程序可以不要求所有的插槽被调用，只选择那些符合特定条件
的插槽，比如当一个插槽的返回值满足要求后就终止迭代，不再调用剩余的插槽。
下面的合并器当得到一个大于100的返回值时就停止插槽调用：
class combiner
{
public:
	typedef bool result_type;

	template<typename InputIterator>
	result_type operator()(InputIterator begin, InputIterator end) const
	{
		while (begin != end)
		{
			if (*begin > 100)
			{
				return true;
			}
		}

		return false;
	}
};

signal模板参数列表的最后一个类型参数是互斥量Mutex，默认值是signals2::mutex，它会自动检测编译器的线程支持程度，根据操作系统自动决定要使用的系统互斥量
对象（UNIX下使用pthread_mutex，Windows下使用临界区）。通常mutex都工作的很好，不需要改变它。
signal对象在创建时会自动创建一个mutex保护内部状态，每一个插槽连接时也会创建出一个新的mutex，当信号或插槽被调用时mutex都会自动锁定，因此signal可以很好
地工作于多线程环境。
connection和shared_connection_block也是线程安全的，但用于自动连接管理的slot类不是线程安全的。
signals2库中还有一个dummy_mutex，它是一个空的mutex类，把它作为模板参数可以使signals2变成非线程安全的版本，由于不使用锁定速度会稍微快一些。
由于mutex是signal的最后一个模板参数，要指定它需要写出很多缺省的类型，signals2使用模板元函数的方式可以便利地完成这个工作：
typedef signal_type<int(int),keywords::mutex_type<dummy_mutex>>::type signal_type;

默认情况下插槽与信号signal，连接connection都是无关的，它自己无法处理与信号的连接，只能由信号或者连接时返回的connection对象来管理，但有的时候我们又必须
让插槽自己管理连接。
signals2库提供了connect_extended()函数和extended_slot_type类型定义，可以让插槽接受一个额外的connection对象以处理连接。为了使用这个功能，需要修改插槽
的声明，使它能够接受connection对象，例如：
template<int N>
struct slots
{
	int operator()(const connection& conn,int x)
	{
		cout << "conn = " << conn.connected() << endl;

		return x * N;
	}
};
在连接插槽时必须使用extended_slot_type的类bind语法，用占位符_1向插槽传递connection对象。
int main()
{
	typedef signal<int(int)> signal_type;
	typedef signal_type::extended_slot_type slot_type;

	signal_type sig;

	//_1是connection对象，_2是插槽实际使用的参数
	sig.connect_extended(slot_type(&slots<10>::operator(),slots<10>(),_1,_2));
	sig.connect_extended(slot_type(&slots<20>::operator(),slots<20>(),_1,_2));
	sig(3);																																			//整数3将作为_2的实际参数传递给插槽
}
*/

/*
Class encapsulates rendering properties of an object.Encapsulates ALL aspects of the visual appearance(视觉外观的所有方面) of an object.It also
includes other flags which might not be traditionally(传统) thought of as material properties such as culling modes and depth buffer settings,
but these affect the appearance of the rendered object and are convenient to attach to the material since it keeps all the settings in one place.
This is different to Direct3D which treats a material as just the color components (diffuse,specular) and not texture maps etc.An material can
be thought of as equivalent to a 'Shader'.A Material can be rendered in multiple different ways depending on the hardware available. You may
configure a Material to use high-complexity fragment shaders,but these won't work on every card; therefore a Technique is an approach to
creating the visual effect you are looking for. You are advised to create fallback techniques with lower hardware requirements if you decide to
use advanced features. In addition,you also might want lower-detail techniques for distant geometry.Each technique can be made up of multiple
passes. A fixed-function pass may combine multiple texture layers using multitexrtuing,but Ung can break that into multiple passes automatically
if the active card cannot handle that many simultaneous(同时) textures. Programmable passes,however,cannot be split down automatically,so if
the active graphics card cannot handle the technique which contains these passes,Ung will try to find another technique which the card can do.
If,at the end of the day,the card cannot handle any of the techniques which are listed for the material,the engine will render the geometry plain
(素) white,which should alert you to the problem.
*/

/*
观察者模式支持组件解耦且避免了循环依赖.

MVC架构模式要求业务逻辑(Model,模型)独立于用户界面(View,视图),控制器(Controller),接收用户输入并协调另外两者.
MVC支持应用程序的功能模块化,并具有以下优点:
1:模型和视图组件的隔离,就可以实现多个用户界面,并且使这些界面能够重用公共的业务逻辑核心.
2:避免了因多份UI实现而构建多份重复的底层模型代码的问题.
3:模型和视图代码的解耦简化了为核心业务逻辑代码编写单元测试的工作.
4:组件的模块化允许核心逻辑开发者和GUI开发者并行工作,且互不影响.

以一个复选框按钮为例:
按钮当前的"打开/关闭"状态存储在模型中.
视图在屏幕上绘制出按钮的当前状态.
当用户点击按钮时,控制器更新模型状态和视图显示.

MVC架构模式促使核心业务逻辑(模型)与用户界面(视图)分离.它还隔离了控制器逻辑,控制器逻辑会引起模型的改变并更新视图.

视图代码能够调用模型代码(发现最新状态并更新UI),但是反之不成立.模型代码不应该获知任何视图代码的编译时信息(因为这样做会把模型绑定在
一个视图上).控制器和视图都依赖模型,但是模型代码既不依赖控制器代码,也不依赖视图代码.

在简单的应用程序中,控制器能够基于用户输入改变模型,并将这些改变传递给视图以更新UI.但在真实的应用程序中,通常需要通过更新视图来反映底层
模型的其他变化.这种需求是合理的,因为改变模型的某个方面可能引起其他具有依赖关系的模型的状态改变.这就要求模型代码在状态改变时通知视图层.
但如刚才所说,模型代码不能静态绑定和调用视图代码.这就是出现观察者模式的原因,它可以帮助我们解决此问题.

观察者模式是"发布/订阅"范式(Publish/Subscribe,简写做pub/sub)的一个具体实例.这些技术定义了对象之间一对多的依赖,使得发布者对象能够
将它的状态变化通知给所有的订阅对象,而又不直接依赖于订阅者对象.

实现观察者模式的典型做法是引入两个概念:主题(Subject)和观察者(Observer),也称作发布者和订阅者.一个或多个观察者注册主题中感兴趣的
事件,之后主题会在自身状态发生变化时通知所有注册的观察者.注意:在销毁观察者对象之前,必须先取消订阅此观察者对象,否则下次通知会导致程序
崩溃.
*/