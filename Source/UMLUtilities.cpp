#include "UMLUtilities.h"

#ifdef UML_HIERARCHICAL_COMPILE

#include "UMLVec2.h"
#include "UMLVec3.h"
#include "UMLVec4.h"
#include "UMLMat2.h"
#include "UMLMat3.h"
#include "UMLMat4.h"

namespace ung
{
	bool UNG_STDCALL ungIsPow2(uint32 i)
	{
		return ((i - 1) & i) == 0;
	}

	uint32 UNG_STDCALL ungLog2(uint32 n)
	{
		BOOST_ASSERT(n != 0);
		BOOST_ASSERT(ungIsPow2(n));

		uint32 log = 0;
		while (true)
		{
			n >>= 1;
			if (n == 0)
			{
				break;
			}
			log++;
		}

		return log;
	}

	real_type UNG_STDCALL ungVector3DotProduct(Vector3 const & v1, Vector3 const & v2)
	{
		return v1.dotProduct(v2);
	}

	void UNG_STDCALL ungVector3CrossProduct(Vector3 const & left, Vector3 const & right, Vector3 & cross)
	{
		cross = left.crossProduct(right);
	}

	Vector3& UNG_STDCALL ungTransformVec(Vector3& vec3Out, Vector3 const& vec3In, Matrix4 const& mat4In)
	{
		Vector3 vIn(vec3In);

		vec3Out.x = vIn.x * mat4In.m[0][0] + vIn.y * mat4In.m[1][0] + vIn.z * mat4In.m[2][0];
		vec3Out.y = vIn.x * mat4In.m[0][1] + vIn.y * mat4In.m[1][1] + vIn.z * mat4In.m[2][1];
		vec3Out.z = vIn.x * mat4In.m[0][2] + vIn.y * mat4In.m[1][2] + vIn.z * mat4In.m[2][2];

		return vec3Out;
	}

	Vector3& UNG_STDCALL ungTransformPos(Vector3& vec3Out, Vector3 const& vec3In, Matrix4 const& mat4In)
	{
		Vector3 vIn(vec3In);

		real_type w = 1.0;

		vec3Out.x = vIn.x * mat4In.m[0][0] + vIn.y * mat4In.m[1][0] + vIn.z * mat4In.m[2][0] + mat4In.m[3][0];
		vec3Out.y = vIn.x * mat4In.m[0][1] + vIn.y * mat4In.m[1][1] + vIn.z * mat4In.m[2][1] + mat4In.m[3][1];
		vec3Out.z = vIn.x * mat4In.m[0][2] + vIn.y * mat4In.m[1][2] + vIn.z * mat4In.m[2][2] + mat4In.m[3][2];
		w = vIn.x * mat4In.m[0][3] + vIn.y * mat4In.m[1][3] + vIn.z * mat4In.m[2][3] + mat4In.m[3][3];

		BOOST_ASSERT(w);

		real_type wInv = 1.0 / w;

		vec3Out.x *= wInv;
		vec3Out.y *= wInv;
		vec3Out.z *= wInv;

		return vec3Out;
	}

	real_type UNG_STDCALL ungScalarTripleProduct(Vector3 const & u, Vector3 const & v, Vector3 const & w)
	{
		//(u × v) · w = u · (v × w)
		return u.crossProduct(v).dotProduct(w);
	}

	real_type UNG_STDCALL ungSignedVolumeOfParallelepiped(Vector3 const& v1, Vector3 const& v2, Vector3 const& v3)
	{
		Matrix3 temp{ v1,v2,v3 };

		return temp.determinant();
	}

	bool UNG_STDCALL ungSolvingLinearEquations2(Matrix2 const& coefficientMatrix, Vector2 const& constantVector, Vector2& out)
	{
		//因为转置矩阵的行列式和其原矩阵的行列式相等，所以这里不对矩阵进行转置操作
		auto det = coefficientMatrix.determinant();					//coefficientMatrix是列为主

		//if (Math::isZero(det))
		if (det == 0.0)
		{
			return false;
		}

		auto invDet = 1.0 / det;

		//对每一列进行替换

		//行为主
		//x:第一行被常数向量替换
		Matrix2 X{ constantVector.x,constantVector.y,coefficientMatrix[0][1],coefficientMatrix[1][1] };
		//y:第二行被常数向量替换
		Matrix2 Y{ coefficientMatrix[0][0],coefficientMatrix[1][0],constantVector.x,constantVector.y };
		auto detX = X.determinant();
		auto detY = Y.determinant();

		//克莱姆法则
		out.x = detX * invDet;
		out.y = detY * invDet;

		return true;
	}

	bool UNG_STDCALL ungSolvingLinearEquations3(Matrix3 const& coefficientMatrix, Vector3 const& constantVector, Vector3& out)
	{
		auto det = coefficientMatrix.determinant();

		//if (Math::isZero(det))
		if (det == 0.0)
		{
			return false;
		}

		auto invDet = 1.0 / det;

		Matrix3 X{ constantVector,coefficientMatrix.getColumn(1),coefficientMatrix.getColumn(2) };
		Matrix3 Y{ coefficientMatrix.getColumn(0),constantVector,coefficientMatrix.getColumn(2) };
		Matrix3 Z{ coefficientMatrix.getColumn(0),coefficientMatrix.getColumn(1),constantVector };
		auto detX = X.determinant();
		auto detY = Y.determinant();
		auto detZ = Z.determinant();

		out.x = detX * invDet;
		out.y = detY * invDet;
		out.z = detZ * invDet;

		return true;
	}

	bool UNG_STDCALL ungSolvingLinearEquations4(Matrix4 const& coefficientMatrix, Vector4 const& constantVector, Vector4& out)
	{
		auto det = coefficientMatrix.determinant();

		//if (Math::isZero(det))
		if (det == 0.0)
		{
			return false;
		}

		auto invDet = 1.0 / det;

		Matrix4 X{ constantVector,coefficientMatrix.getColumn(1),coefficientMatrix.getColumn(2),coefficientMatrix.getColumn(3) };
		Matrix4 Y{ coefficientMatrix.getColumn(0),constantVector,coefficientMatrix.getColumn(2),coefficientMatrix.getColumn(3) };
		Matrix4 Z{ coefficientMatrix.getColumn(0),coefficientMatrix.getColumn(1),constantVector,coefficientMatrix.getColumn(3) };
		Matrix4 W{ coefficientMatrix.getColumn(0),coefficientMatrix.getColumn(1),coefficientMatrix.getColumn(2),constantVector };
		auto detX = X.determinant();
		auto detY = Y.determinant();
		auto detZ = Z.determinant();
		auto detW = W.determinant();

		out.x = detX * invDet;
		out.y = detY * invDet;
		out.z = detZ * invDet;
		out.w = detW * invDet;

		return true;
	}
}//namespace ung

#endif//UML_HIERARCHICAL_COMPILE