#include "UPEConfig.h"

#ifdef UPE_HIERARCHICAL_COMPILE

#if UNG_USE_DOUBLE
#if UNG_DEBUGMODE
#pragma comment(lib,"double/BulletDynamics_d.lib")
#pragma comment(lib,"double/BulletCollision_d.lib")
#pragma comment(lib,"double/LinearMath_d.lib")
#else
#pragma comment(lib,"double/BulletDynamics.lib")
#pragma comment(lib,"double/BulletCollision.lib")
#pragma comment(lib,"double/LinearMath.lib")
#endif
#else
#if UNG_DEBUGMODE
#pragma comment(lib,"float/BulletDynamics_d.lib")
#pragma comment(lib,"float/BulletCollision_d.lib")
#pragma comment(lib,"float/LinearMath_d.lib")
#else
#pragma comment(lib,"float/BulletDynamics.lib")
#pragma comment(lib,"float/BulletCollision.lib")
#pragma comment(lib,"float/LinearMath.lib")
#endif
#endif

namespace
{
	//warning LNK4221: 此对象文件未定义任何之前未定义的公共符号，因此任何耗用此库的链接操作都不会使用此文件。
	int UPE_DUMMY_LNK_4221 = 0;
}//namespace

#endif//UPE_HIERARCHICAL_COMPILE

/*
bullet示例：
#include "BulletDynamics/Dynamics/btDynamicsWorld.h"

//convenience header file
#include "btBulletDynamicsCommon.h"

#pragma comment(lib,"BulletDynamics_d.lib")
#pragma comment(lib,"BulletCollision_d.lib")
#pragma comment(lib,"LinearMath_d.lib")

//---------------------------------------------------------------------

class GameObject : public MemoryPool<GameObject>
{
public:
	GameObject(btCollisionShape* pShape, float mass, const btVector3 &color, const btVector3 &initialPosition = btVector3(0, 0, 0), const btQuaternion &initialRotation = btQuaternion(0, 0, 1, 1));

	~GameObject();

	btCollisionShape* GetShape();

	btRigidBody* GetRigidBody();

	btMotionState* GetMotionState();

	void GetTransform(btScalar* transform);

	btVector3 GetColor();

	void SetColor(const btVector3 &color);

protected:
	//三个基本元素
	btCollisionShape* m_pShape;
	btRigidBody* m_pBody;
	TransformationHook* m_pMotionState;

	btVector3 m_color;
};

GameObject::GameObject(btCollisionShape* pShape, float mass, const btVector3 &color,
	const btVector3 &initialPosition, const btQuaternion &initialRotation)
{
	m_pShape = pShape;

	m_color = color;

	//Create the initial transform
	btTransform transform;
	transform.setIdentity();
	transform.setOrigin(initialPosition);
	transform.setRotation(initialRotation);

	//Create the motion state from the initial transform
	//从最初处来获取hook
	m_pMotionState = new TransformationHook(transform);

	//Calculate the local inertia(局部惯性)
	//局部惯性，用于表达“大地”。
	btVector3 localInertia(0, 0, 0);

	//Objects of infinite mass can't move or rotate
	if (mass != 0.0f)
	{
		pShape->calculateLocalInertia(mass, localInertia);
	}

	//参数中多了“局部惯性”
	btRigidBody::btRigidBodyConstructionInfo cInfo(mass, m_pMotionState, pShape, localInertia);

	//Create the rigid body
	m_pBody = new btRigidBody(cInfo);
}

GameObject::~GameObject()
{
	delete m_pBody;
	delete m_pMotionState;
	delete m_pShape;
}

btCollisionShape* GameObject::GetShape()
{
	return m_pShape;
}

btRigidBody* GameObject::GetRigidBody()
{
	return m_pBody;
}

btMotionState* GameObject::GetMotionState()
{
	return m_pMotionState;
}

void GameObject::GetTransform(btScalar* transform)
{
	if (m_pMotionState)
	{
		m_pMotionState->grabWorldTransform(transform);
	}
}

btVector3 GameObject::GetColor()
{
	return m_color;
}

void GameObject::SetColor(const btVector3 &color)
{
	m_color = color;
}

//----------------------------------------------------------------------------

#include <set>

//一个辅助结构
struct RayResult
{
	btRigidBody* pBody;
	btVector3 hitPoint;
};

class myApp : public BaseFreeglutApplication
{
public:
	myApp();

	virtual ~myApp();

	void drawBox(btVector3 const& halfSize);

	void initializePhysics();
	void shutdownPhysics();

	void createObjects();
	void renderScene();
	void updateScene(real_type dt);

	typedef std::vector<GameObject*> GameObjects;

	void DrawShape(btScalar* transform, const btCollisionShape* pShape, const btVector3 &color);
	
	GameObject* CreateGameObject(btCollisionShape* pShape,
		const float &mass,
		const btVector3 &color = btVector3(1.0f, 1.0f, 1.0f),
		const btVector3 &initialPosition = btVector3(0.0f, 0.0f, 0.0f),
		const btQuaternion &initialRotation = btQuaternion(0, 0, 1, 1));

	void ShootBox(const btVector3 &direction);
	void DestroyGameObject(btRigidBody* pBody);
	GameObject* FindGameObject(btRigidBody* pBody);

	//Picking functions
	btVector3 GetPickingRay(int x, int y);
	bool Raycast(const btVector3 &startPosition, const btVector3 &direction, RayResult &output);

	//Constraint functions
	void CreatePickingConstraint(int x, int y);
	void RemovePickingConstraint();

	//convenient typedefs for collision events
	typedef std::pair<const btRigidBody*, const btRigidBody*> CollisionPair;
	typedef std::set<CollisionPair> CollisionPairs;
	//collision event functions
	void CheckForCollisionEvents();
	//When we detect a collision or separation, we will want some way to inform the game
	//logic of it. These two functions will do the job nicely:
	virtual void CollisionEvent(btRigidBody* pBody0, btRigidBody * pBody1);
	virtual void SeparationEvent(btRigidBody * pBody0, btRigidBody * pBody1);

	//------------------override---------------

	virtual void initialize() override;
	virtual void keyboard(unsigned char key, int x, int y) override;
	virtual void mouse(int button, int state, int x, int y) override;
	virtual void motion(int x, int y) override;
	virtual void idle() override;

private:
	//Core Bullet components
	btBroadphaseInterface* m_pBroadphase;
	btCollisionConfiguration* m_pCollisionConfiguration;
	btCollisionDispatcher* m_pDispatcher;
	btConstraintSolver* m_pSolver;
	btDynamicsWorld* m_pWorld;

	DebugDrawer* m_pDebugDrawer;

	//Constraint variables
	btRigidBody* m_pPickedBody;					//the body we picked up
	btTypedConstraint*  m_pPickConstraint;		//the constraint,the body is attached to
	//the distance from the camera to the hit point (so we can move the object up, down,
	//left and right from our view)
	btScalar m_oldPickingDist;

	//collision event variables
	CollisionPairs m_pairsLastUpdate;

	GameObjects m_objects;
	SteadyClock_s mTimer;

	//our box to lift
	GameObject* m_pBox;
	//a simple trigger volume
	btCollisionObject* m_pTrigger;
};

myApp::myApp() :
	m_pBroadphase(nullptr),
	m_pCollisionConfiguration(nullptr),
	m_pDispatcher(nullptr),
	m_pSolver(nullptr),
	m_pWorld(nullptr),
	m_pDebugDrawer(nullptr),
	m_pPickedBody(nullptr),
	m_pPickConstraint(nullptr)
{
	mTimer.restart();
}

//At the end of the program you delete all objects in the reverse order of creation.
//remove the rigidbodies from the dynamics world and delete them
for (i=dynamicsWorld ->getNumCollisionObjects()-1; i>=0 ;i--)
{
	btCollisionObject* obj = dynamicsWorld ->getCollisionObjectArray()[i];
	btRigidBody* body = btRigidBody::upcast(obj);
	if (body && body->getMotionState())
	{
		delete body->getMotionState();
	}

	dynamicsWorld ->removeCollisionObject(obj);
	delete obj;
}

//delete collision shapes
for (int j=0;j<collisionShapes.size();j++)
{
	btCollisionShape* shape = collisionShapes[j];
	collisionShapes[j] = 0;
	delete shape;
}

//delete dynamics world
delete dynamicsWorld;

//delete solver
delete solver;

//delete broadphase
delete overlappingPairCache;

//delete dispatcher
delete dispatcher;

delete collisionConfiguration;

//next line is optional: it will be cleared by the destructor when the array goes out of scope
//collisionShapes.clear();
myApp::~myApp()
{
	//Shutdown the physics system
	shutdownPhysics();
}

void myApp::drawBox(btVector3 const& halfSize)
{
	ManualBox box({ halfSize.getX(),halfSize.getY(),halfSize.getZ() });
	
	auto vertices = box.getPositions();

	auto indices = box.getIndices();

	//The glBegin() and glEnd() functions are the two important OpenGL commands
	//that work together to define the starting and ending points (known as delimiters(分隔符))
	//for the construction of a primitive shape. The glBegin() function requires a single
	//argument that specifies the type of primitive to render.
	glBegin(GL_TRIANGLES);

	for (uint32 i = 0; i < box.getIndicesCount(); i += 3)
	{
		const Vector3& v0 = vertices[indices[i]];
		const Vector3& v1 = vertices[indices[i + 1]];
		const Vector3& v2 = vertices[indices[i + 2]];
		ManualTriangle t(v0,v1,v2);

		//glNormal3f():sets the normal for the subsequent(随后的) vertices.
		glNormal3f(t.getFaceNormal().x, t.getFaceNormal().y, t.getFaceNormal().z);

		//glVertex3f():defines the position of a vertex in space.
		glVertex3f(v0.x, v0.y, v0.z);
		glVertex3f(v1.x, v1.y, v1.z);
		glVertex3f(v2.x, v2.y, v2.z);
	}

	//stop processing vertices
	glEnd();
}

void myApp::initializePhysics()
{
	//Create the collision configuration
	//collision configuration contains default setup for memory , collision setup. Advanced
	//users can create their own configuration.
	m_pCollisionConfiguration = new btDefaultCollisionConfiguration();
	//Create the dispatcher
	//use the default collision dispatcher. For parallel processing you can use a diffent
	//dispatcher(see Extras / BulletMultiThreaded)
	m_pDispatcher = new btCollisionDispatcher(m_pCollisionConfiguration);
	//Create the broadphase
	//btDbvtBroadphase is a good general purpose broadphase. You can also try out
	//btAxis3Sweep
	m_pBroadphase = new btDbvtBroadphase();
	//Create the constraint solver
	//the default constraint solver. For parallel processing you can use a different solver
	//(see Extras / BulletMultiThreaded)
	m_pSolver = new btSequentialImpulseConstraintSolver();
	//Create the world
	m_pWorld = new btDiscreteDynamicsWorld(m_pDispatcher, m_pBroadphase, m_pSolver, m_pCollisionConfiguration);

	m_pWorld ->setGravity(btVector3(0,-10,0));

	createObjects();
}

void myApp::shutdownPhysics()
{
	delete m_pWorld;
	m_pWorld = nullptr;
	delete m_pSolver;
	m_pSolver = nullptr;
	delete m_pBroadphase;
	m_pBroadphase = nullptr;
	delete m_pDispatcher;
	m_pDispatcher = nullptr;
	delete m_pCollisionConfiguration;
	m_pCollisionConfiguration = nullptr;
}

void myApp::createObjects()
{
	//create a ground plane
	CreateGameObject(new btBoxShape(btVector3(1, 50, 50)), 0, btVector3(0.2f, 0.6f, 0.6f), btVector3(0.0f, 0.0f, 0.0f));
	
	////红
	//CreateGameObject(new btBoxShape(btVector3(1, 1, 1)), 1.0, 
	//	btVector3(1.0f, 0.0f, 0.0f), btVector3(0.0f, 10.0f, 0.0f));
	//蓝(位于红的右边)
	CreateGameObject(new btBoxShape(btVector3(1, 1, 1)), 1.0, 
		btVector3(0.0f, 0.0f, 1.0f), btVector3(1.25f, 30.0f, 0.0f));

	//create our red box, but store the pointer for future usage
	m_pBox = CreateGameObject(new btBoxShape(btVector3(1, 1, 1)), 1.0, btVector3(1.0f, 0.0f, 0.0f), btVector3(0.0f, 10.0f, 0.0f));

	//create a trigger volume
	m_pTrigger = new btCollisionObject();
	//create a box for the trigger's shape
	m_pTrigger->setCollisionShape(new btBoxShape(btVector3(1, 0.25, 1)));
	//set the trigger's position
	btTransform triggerTrans;
	triggerTrans.setIdentity();
	triggerTrans.setOrigin(btVector3(0, 1.5, 0));
	m_pTrigger->setWorldTransform(triggerTrans);
	//flag the trigger to ignore contact responses

	//Disabling contact response:
	//There is no specific class required to build a trigger volume, but there is an essential
	//flag which we can apply to any object:  CF_NO_CONTACT_RESPONSE . This flag disables
	//all contact response, informing Bullet that it should not calculate any physical
	//response when other objects collide with the flagged object. This does not prevent it
	//from performing broad and narrow phase collision detection and informing us when
	//an overlap occurs, hence our  CollisionEvent() and  CollisionSeparation()
	//functions will still be called even for objects flagged in this way. The only difference
	//is that other objects will pass through it unhindered(不受阻碍).
	m_pTrigger->setCollisionFlags(btCollisionObject::CF_NO_CONTACT_RESPONSE);
	//add the trigger to our world
	m_pWorld->addCollisionObject(m_pTrigger);

	//The previous code creates a trigger volume hovering just above the ground plane.
	//We don't want these trigger volumes to be rendered during runtime since these
	//kinds of triggers usually remain invisible to the player. So we avoided using our
	//CreateGameObject() function (which would have added it to the list of objects
	//and automatically render it), and instead we built it manually.
	//However, even though it is invisible to the player, we can still observe it through
	//the debug renderer. If we enable wireframe mode (the W key), Bullet will draw the
	//shape for us so that we can visualize the trigger volume in the space.
}

void myApp::renderScene()
{
	btScalar transform[16];

	for (GameObjects::iterator i = m_objects.begin(); i != m_objects.end(); ++i)
	{
		GameObject* pObj = *i;

		pObj->GetTransform(transform);

		DrawShape(transform, pObj->GetShape(), pObj->GetColor());
	}

	//In each render call, we will ask the world object to render the
	//debug information through a call to debugDrawWorld().This will cause the world
	//to determine what needs to be rendered, based on which debug flags are set in  m_
	//debugFlags.
	m_pWorld->debugDrawWorld();
}

//print positions of all objects
for (int j=dynamicsWorld ->getNumCollisionObjects()-1; j>=0 ;j--)
{
	btCollisionObject* obj = dynamicsWorld ->getCollisionObjectArray()[j];
	btRigidBody* body = btRigidBody::upcast(obj);

	btTransform trans;
	if (body && body->getMotionState())
	{
		body->getMotionState()->getWorldTransform(trans);
	}
	else
	{
		trans = obj->getWorldTransform();
	}

	printf("world pos object %d = %f,%f,%f\n",j,float(trans.getOrigin().getX()),float(
	trans.getOrigin().getY()),float(trans.getOrigin().getZ()));
}

void myApp::updateScene(real_type dt)
{
	//check if the world object exists
	if (m_pWorld)
	{
		//step the simulation through time.

		//The stepSimulation() function also accepts two more parameters; the second
		//parameter is the maximum number of substeps that can be calculated this iteration,
		//and the third is the desired frequency of step calculations.
		//If we ask Bullet to perform calculations at a rate of 60 Hz (the third parameter – 60
		//Hz is also the default), but one second has gone by (maybe our application froze
		//for a moment), Bullet will make 60 separate step calculations before returning. This
		//prevents Bullet from jumping every object for one full second in time all at once, and
		//possibly missing some collisions.
		//However, calculating so many iterations at once could take a long time to process,
		//causing our simulation to slow down for a while as it tries to catch up. To solve this,
		//we can use the second parameter to limit the maximum number of steps it's allowed
		//to take in case one of these spikes occur. In this case, the world's physics will appear
		//to slow down, but it also means that your application won't suffer from a long period
		//of low frame rates.
		//In addition, the function returns the number of actual steps that took place, which
		//will either be the maximum, or less if it processed everything quickly enough.
		m_pWorld->stepSimulation(dt);

		//check for any new collisions/separations
		CheckForCollisionEvents();
	}
}

void myApp::initialize()
{
	BaseFreeglutApplication::initialize();

	//Initialize the physics system
	initializePhysics();

	//create the debug drawer
	m_pDebugDrawer = new DebugDrawer();
	//set the initial debug level to 0
	m_pDebugDrawer->setDebugMode(0);
	//add the debug drawer to the world

	//The  DebugDrawer class must be handed over to the world object through a call to setDebugDrawer()
	m_pWorld->setDebugDrawer(m_pDebugDrawer);
}

void myApp::keyboard(unsigned char key, int x, int y)
{
	BaseFreeglutApplication::keyboard(key, x, y);

	switch (key)
	{
		case 'w':
			//toggle wireframe debug drawing
			m_pDebugDrawer->toggleDebugFlag(btIDebugDraw::DBG_DrawWireframe);
			break;
		
		case 'b':
			//toggle AABB debug drawing
			m_pDebugDrawer->toggleDebugFlag(btIDebugDraw::DBG_DrawAabb);
			break;

		case 'd':
		{
			//create a temp object to store the raycast result
			RayResult result;
			//use our picking ray to perform a raycast and tell us the first rigid body with which it collides.

			//Raycasting in Bullet is handled through the btDynamicsWorld object's rayTest()
			//function. We provide the starting point (as a btVector3 ), the direction (btVector3),
			//and an object to store the raycast data inside, which should be one of two different
			//classes that inherit from RayResultCallback.The object could either be:
			//1,ClosestRayResultCallback , which gives the closest collision that the ray
			//detected from the start location
			//2,AllHitsRayResultCallback , which gives an array filled with all of the
			//collisions the ray detected

			//Which object we want to use will depend on whether we want only the closest hit,
			//or all of them. We will be using  ClosestRayResultCallback , which contains useful
			//data and member functions for the collision point, such as:
			//1,hasHit(),which returns a boolean value and tells us if there was a collision
			//between the ray and any physics object
			//2,m_collisionObject,which is the  btCollisionObject our ray hit
			//3,m_hitPointWorld,which is the coordinate in world space where the ray
			//detected a collision

			//The Raycast() function in source code takes a picking ray and an empty
			//output  RayResult structure, uses it to create a ClosestRayResultCallback , and
			//then performs a raycast test. If the raycast was successful, the function fills out the
			//structure and returns true, allowing us to check the success or failure of the raycast
			//outside of this function.

			//Notice the special case to avoid picking static objects, such as our
			//ground plane. When we gave our ground plane a mass of zero,
			//Bullet automatically set the static flag for us, allowing us to check
			//for it at a later date.
			if (!Raycast(TO_BT_VECTOR3(mCameraPosition), GetPickingRay(x, y), result))
			{
				return;
			}

			//destroy the corresponding game object
			DestroyGameObject(result.pBody);
			break;
		}
	}
}

void myApp::mouse(int button, int state, int x, int y)
{
	switch (button)
	{
		//鼠标左键
		case 0:
		{
			if (state == 0)
			{
				//按下
				//Create the picking constraint when we click the LMB
				CreatePickingConstraint(x, y);
			}
			else
			{
				//弹起
				//Remove the picking constraint when we release the LMB
				RemovePickingConstraint();
			}
			break;
		}

		//鼠标右键
		case 2:
		{
			//按下(如果持续按下状态，则只执行一次)
			if (state == 0)
			{
				ShootBox(GetPickingRay(x, y));
			}
			break;
		}
	}
}

//Updates the position of the constraint while we're still holding down the left mouse button.
void myApp::motion(int x, int y)
{
	//Did we pick a body with the LMB?
	if (m_pPickedBody)
	{
		btGeneric6DofConstraint* pickCon = static_cast<btGeneric6DofConstraint*>(m_pPickConstraint);
		if (!pickCon)
		{
			return;
		}

		//use another picking ray to get the target direction
		btVector3 dir = GetPickingRay(x, y) - TO_BT_VECTOR3(mCameraPosition);
		dir.normalize();

		//use the same distance as when we originally picked the object
		dir *= m_oldPickingDist;
		btVector3 newPivot = TO_BT_VECTOR3(mCameraPosition) + dir;

		//set the position of the constraint

		//It was possible to get data from the constraint in a form
		//which is relative to one of the two objects involved in the constraint (called A and
		//B). We use the getFrameOffsetA() function to get the transform position of the
		//constraint relative to the first object, and then update it with the new value. This is
		//the equivalent to updating the position of the constraint's pivot point. Thus in the
		//next simulation step, the constraint will attempt to move the box to the new position
		//of the mouse, keeping the same distance from the camera as when it was first picked.
		pickCon->getFrameOffsetA().setOrigin(newPivot);
	}
}

void myApp::idle()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	auto elaps = mTimer.elapsed();
	mTimer.restart();

	updateScene(elaps);

	updateCamera();

	renderScene();

	glutSwapBuffers();
}

void myApp::DrawShape(btScalar* transform, const btCollisionShape* pShape, const btVector3 &color)
{
	glColor3f(color.x(), color.y(), color.z());

	//glPushMatrix() and glPopMatrix() are another pair of OpenGL delimiter
	//functions that work together in much the same way as glBegin() and glEnd()
	//do. They are used to control the matrix stack, which is very helpful while
	//drawing multiple objects, and objects that are meant to be connected together.
	//Transformation matrices can be combined to get a resultant
	//transformation, and if we have multiple objects that share a similar transformation,
	//we can optimize our processing time by sharing information through the matrix
	//stack, instead of recalculating the same value over and over again.
	//This feature is particularly useful when we have object hierarchies such as a knight
	//riding on top of a steed(骏马), or moons orbiting planets, which themselves orbit stars.
	//This is the basic concept of a Scene Graph in 3D rendering.The function to multiply the 
	//current matrix stack by a given matrix is glMultMatrixf()
	glPushMatrix();
	glMultMatrixf(transform);

	//make a different draw call based on the object type
	switch (pShape->getShapeType())
	{
		//an internal enum used by Bullet for boxes
		case BOX_SHAPE_PROXYTYPE:
		{
			//assume the shape is a box, and typecast it
			const btBoxShape* box = static_cast<const btBoxShape*>(pShape);
			//get the 'halfSize' of the box
			btVector3 halfSize = box->getHalfExtentsWithMargin();
			//draw the box
			drawBox(halfSize);
			break;
		}

		default:
			break;
		}

		glPopMatrix();
	}

GameObject* myApp::CreateGameObject(btCollisionShape* pShape, const float &mass, const btVector3 &color, const btVector3 &initialPosition, const btQuaternion &initialRotation)
{
	GameObject* pObject = new GameObject(pShape, mass, color, initialPosition, initialRotation);

	m_objects.push_back(pObject);

	//check if the world object is valid
	if (m_pWorld)
	{
		//add the object's rigid body to the world
		//只要把刚体给放到world中就可以了。
		m_pWorld->addRigidBody(pObject->GetRigidBody());
	}

	return pObject;
}

btVector3 myApp::GetPickingRay(int x, int y)
{
	//calculate the field-of-view
	float tanFov = 1.0f / mNearPlane;
	float fov = btScalar(2.0) * btAtan(tanFov);

	//get a ray pointing forward from the camera and extend it to the far plane
	btVector3 rayFrom = TO_BT_VECTOR3(mCameraPosition);
	Vector3 temp1(mCameraTarget - mCameraPosition);
	btVector3 rayForward = TO_BT_VECTOR3(temp1);
	rayForward.normalize();
	rayForward *= mFarPlane;

	//find the horizontal and vertical vectors relative to the current camera view
	btVector3 ver = TO_BT_VECTOR3(mUpVector);
	btVector3 hor = rayForward.cross(ver);
	hor.normalize();
	ver = hor.cross(rayForward);
	ver.normalize();
	hor *= 2.f * mFarPlane * tanFov;
	ver *= 2.f * mFarPlane * tanFov;

	//calculate the aspect ratio
	btScalar aspect = mScreenWidth / (btScalar)mScreenHeight;

	//adjust the forward-ray based on the X/Y coordinates that were clicked
	hor *= aspect;
	btVector3 rayToCenter = rayFrom + rayForward;
	btVector3 dHor = hor * 1.f / float(mScreenWidth);
	btVector3 dVert = ver * 1.f / float(mScreenHeight);
	btVector3 rayTo = rayToCenter - 0.5f * hor + 0.5f * ver;
	rayTo += btScalar(x) * dHor;
	rayTo -= btScalar(y) * dVert;

	//return the final result
	//return a  btVector3 in world coordinates that points forward from the camera in the corresponding direction.
	return rayTo;
}

void myApp::ShootBox(const btVector3 &direction)
{
	//create a new box object
	GameObject* pObject = CreateGameObject(new btBoxShape(btVector3(1, 1, 1)), 1,
		btVector3(0.4f, 0.f, 0.4f), TO_BT_VECTOR3(mCameraPosition));

	//calculate the velocity
	btVector3 velocity = direction;
	velocity.normalize();
	velocity *= 25.0f;

	//set the linear velocity of the box
	//calling setLinearVelocity() function on the object's rigid body after creating it.
	//This function sets the magnitude and direction of the object's linear motion.

	//setAngularVelocity():is used to set the object's rotational velocity.
	pObject->GetRigidBody()->setLinearVelocity(velocity);
}

bool myApp::Raycast(const btVector3 &startPosition, const btVector3 &direction, RayResult &output)
{
	if (!m_pWorld)
	{
		return false;
	}

	//get the picking ray from where we clicked
	btVector3 rayTo = direction;
	btVector3 rayFrom = TO_BT_VECTOR3(mCameraPosition);

	//Create our raycast callback object
	btCollisionWorld::ClosestRayResultCallback rayCallback(rayFrom, rayTo);

	//Perform the raycast
	m_pWorld->rayTest(rayFrom, rayTo, rayCallback);

	//Did we hit something?
	if (rayCallback.hasHit())
	{
		//if so, get the rigid body we hit
		btRigidBody* pBody = (btRigidBody*)btRigidBody::upcast(rayCallback.m_collisionObject);
		if (!pBody)
		{
			return false;
		}

		//prevent us from picking objects like the ground plane
		if (pBody->isStaticObject() || pBody->isKinematicObject())
		{
			return false;
		}

		//set the result data
		output.pBody = pBody;
		output.hitPoint = rayCallback.m_hitPointWorld;

		return true;
	}

	//we didn't hit anything
	return false;
}

void myApp::CreatePickingConstraint(int x, int y)
{
	if (!m_pWorld)
	{
		return;
	}

	//perform a raycast and return if it fails
	RayResult output;
	if (!Raycast(TO_BT_VECTOR3(mCameraPosition), GetPickingRay(x, y), output))
	{
		return;
	}

	//store the body for future reference
	m_pPickedBody = output.pBody;

	//prevent the picked object from falling asleep
	//we're ensuring the picked object doesn't fall asleep while attached to our constraint.
	m_pPickedBody->setActivationState(DISABLE_DEACTIVATION);

	//get the hit position relative to the body we hit
	//create the constraint at the exact point of the click

	//重要：
	//Constraints must be defined in local space coordinates, for example, let's say we
	//have two objects positioned at (0,3,0) and (0,10,0) in world space coordinates.
	//But, from the first object's perspective, it is always positioned at (0,0,0) in its own
	//local space, regardless of where it is in world space. Also, as far as the first box is
	//concerned, the other box is positioned at (0,7,0) in its local space. Meanwhile, from
	//the second object's perspective, it is also positioned at (0,0,0) in its local space, and
	//the other box is located at (0,-7,0) in its local space.

	//It's possible to obtain these values mathematically by multiplying the vector
	//representing a point in world space by the inverse of an object's transformation
	//matrix. Therefore in the preceding code, we multiply the hit point by the inverse
	//transform of the box's center of mass, giving us the hit point coordinates from the
	//box's local space perspective.
	btVector3 localPivot = m_pPickedBody->getCenterOfMassTransform().inverse() * output.hitPoint;

	//create a transform for the pivot point
	btTransform pivot;
	pivot.setIdentity();
	pivot.setOrigin(localPivot);

	//create our constraint object

	//The constraint requires us to provide the body in question, the pivot point (again, in
	//local space coordinates), and a  bool value. This boolean tells the constraint whether
	//to store various pieces of data relative to object A (the rigid body) or object B (the
	//constraint's pivot point in this case, but could also be a second rigid body). This
	//becomes important when using the constraint later.
	btGeneric6DofConstraint* dof6 = new btGeneric6DofConstraint(*m_pPickedBody, pivot, true);
	bool bLimitAngularMotion = true;
	if (bLimitAngularMotion)
	{
		//calling the  setAngularUpperLimit() and  setAngularLowerLimit()
		//functions with zero's  btVector3 s add a rotational limitation to the box while it is
		//attached to this constraint, preventing it from rotating.
		dof6->setAngularLowerLimit(btVector3(0, 0, 0));
		dof6->setAngularUpperLimit(btVector3(0, 0, 0));
	}

	//add the constraint to the world
	//Much like rigid bodies, it's not enough to create the object; we must also inform the
	//world of its existence, hence we call the addConstraint() function. The second
	//parameter disables the collisions between the two linked bodies. Since we don't have
	//two bodies in this constraint (we have a body and a pivot point), it would be wise to
	//tell Bullet to save itself some effort by setting the value to  true . If we had two rigid
	//bodies connected via a weak constraint and were interested in having them collide,
	//we would want to set this value to  false
	m_pWorld->addConstraint(dof6, true);

	//store a pointer to our constraint
	m_pPickConstraint = dof6;

	//define the 'strength' of our constraint (each axis)
	float cfm = 0.5f;
	dof6->setParam(BT_CONSTRAINT_STOP_CFM, cfm, 0);
	dof6->setParam(BT_CONSTRAINT_STOP_CFM, cfm, 1);
	dof6->setParam(BT_CONSTRAINT_STOP_CFM, cfm, 2);
	dof6->setParam(BT_CONSTRAINT_STOP_CFM, cfm, 3);
	dof6->setParam(BT_CONSTRAINT_STOP_CFM, cfm, 4);
	dof6->setParam(BT_CONSTRAINT_STOP_CFM, cfm, 5);

	//define the 'error reduction' of our constraint (each axis)
	float erp = 0.5f;
	dof6->setParam(BT_CONSTRAINT_STOP_ERP, erp, 0);
	dof6->setParam(BT_CONSTRAINT_STOP_ERP, erp, 1);
	dof6->setParam(BT_CONSTRAINT_STOP_ERP, erp, 2);
	dof6->setParam(BT_CONSTRAINT_STOP_ERP, erp, 3);
	dof6->setParam(BT_CONSTRAINT_STOP_ERP, erp, 4);
	dof6->setParam(BT_CONSTRAINT_STOP_ERP, erp, 5);

	//This is where things get a little weird. The setParam() function sets the value of
	//a number of different constraint variables, two of which are used in the preceding
	//code. It is called a total of twelve times, since there are three axes, two directions for
	//each axis (positive and negative), and two different types of variable to edit (3x2x2 =
	//12). The two aforementioned variables are CFM (Constraint Force Mixing) and ERP
	//(Error Reduction Parameter).

	//CFM is essentially a measure of the strength of the constraint. A value of 0 means a
	//perfectly rigid constraint, while increasing values make the constraint more spring
	//like, up to a value of 1 where it has no effect at all.

	//ERP represents the fraction（分数） of how much joint error will be used in the next
	//simulation step. Many constraints could be working in unison（一致地） to create a complex
	//interaction (imagine a rope bridge, which can be simulated by a attaching a bunch of
	//springs connected together) and ERP is used to determine how much of the previous
	//data will affect the calculation of future data. This is a difficult concept to explain
	//in such a short space, but imagine that we have multiple constraints acting on the
	//same object, each forcing the others into breaking their own rules. ERP is then the
	//priority(优先) of this constraint relative to the others, and helps determine who has higher
	//importance during these types of complex constraint scenarios.

	//save this data for future reference
	m_oldPickingDist = (output.hitPoint - TO_BT_VECTOR3(mCameraPosition)).length();
}

//Makes sure that we have an existing constraint before attempting to destroy it.
//If so, we must remove it from the world, destroy the object in memory, nullify
//the pointers. The re-enable the ability of the picked up object to go back to sleep.
void myApp::RemovePickingConstraint()
{
	//exit in erroneous(错误的) situations
	if (!m_pPickConstraint || !m_pWorld)
	{
		return;
	}

	//remove the constraint from the world
	m_pWorld->removeConstraint(m_pPickConstraint);

	//delete the constraint object
	delete m_pPickConstraint;

	//reactivate the body
	m_pPickedBody->forceActivationState(ACTIVE_TAG);
	m_pPickedBody->setDeactivationTime(0.f);

	//clear the pointers
	m_pPickConstraint = 0;
	m_pPickedBody = 0;
}

void myApp::DestroyGameObject(btRigidBody* pBody)
{
	//Before we can destroy the picked rigid body we need to know what GameObject that
	//corresponds to. We will have to search through our list of game objects, comparing
	//their rigid bodies with the picked one, until we find it. Then, and only then, is it safe
	//to destroy it.
	for (GameObjects::iterator iter = m_objects.begin(); iter != m_objects.end(); ++iter)
	{
		if ((*iter)->GetRigidBody() == pBody)
		{
			GameObject* pObject = *iter;
			//remove the rigid body from the world
			m_pWorld->removeRigidBody(pObject->GetRigidBody());
			//erase the object from the list
			m_objects.erase(iter);
			//delete the object from memory
			delete pObject;

			return;
		}
	}
}

//Our goals are simple:determine if a pair of objects have either collided or separated during
//the step,and if so, broadcast the corresponding event. The basic process is as follows:
//1,For each manifold, check if the two objects are touching (the number of contact points will
//be greater than zero).
//2,If so, add the pair to a list of pairs that we found in this step.
//3,If the same pair was not detected during the previous step, broadcast a collision event.
//4,Once we've finished checking the manifolds, create another list of collision objects that
//contains only the missing collision pairs between the previous step and this step.
//5,For each pair that is missing, broadcast a separation event.
//6,Overwrite the list of collision pairs from the previous step, with the list we created for this step.
void myApp::CheckForCollisionEvents()
{
	//keep a list of the collision pairs we found during the current update
	CollisionPairs pairsThisUpdate;

	//iterate through all of the manifolds in the dispatcher
	for (int i = 0; i < m_pDispatcher->getNumManifolds(); ++i)
	{
		//get the manifold
		btPersistentManifold* pManifold = m_pDispatcher->getManifoldByIndexInternal(i);

		//ignore manifolds that have no contact points.
		if (pManifold->getNumContacts() > 0)
		{
			//get the two rigid bodies involved in the collision
			const btRigidBody* pBody0 = static_cast<const btRigidBody*>(pManifold->getBody0());
			const btRigidBody* pBody1 = static_cast<const btRigidBody*>(pManifold->getBody1());

			//always create the pair in a predictable order(use the pointer value..)
			bool const swapped = pBody0 > pBody1;
			const btRigidBody* pSortedBodyA = swapped ? pBody1 : pBody0;
			const btRigidBody* pSortedBodyB = swapped ? pBody0 : pBody1;

			//create the pair
			CollisionPair thisPair = std::make_pair(pSortedBodyA, pSortedBodyB);

			//insert the pair into the current list
			pairsThisUpdate.insert(thisPair);

			//if this pair doesn't exist in the list from the previous update, it is a new pair and we must send a collision event
			if (m_pairsLastUpdate.find(thisPair) == m_pairsLastUpdate.end())
			{
				CollisionEvent((btRigidBody*)pBody0, (btRigidBody*)pBody1);
			}
		}
	}

	//create another list for pairs that were removed this update
	CollisionPairs removedPairs;

	//this handy function gets the difference beween two sets. It takes the difference between
	//collision pairs from the last update, and this update and pushes them into the removed 
	//pairs list

	//std::set_difference returns only objects pairs that are present in the first set, but missing
	//from the second set. Note that it does not return new object pairs from the second set.
	std::set_difference(m_pairsLastUpdate.begin(), m_pairsLastUpdate.end(),
	pairsThisUpdate.begin(), pairsThisUpdate.end(),
	std::inserter(removedPairs, removedPairs.begin()));

	//iterate through all of the removed pairs sending separation events for them
	for (CollisionPairs::const_iterator iter = removedPairs.begin(); iter != removedPairs.end(); ++iter)
	{
		SeparationEvent((btRigidBody*)iter->first, (btRigidBody*)iter->second);
	}

	//in the next iteration we'll want to compare against the pairs we found in this iteration
	m_pairsLastUpdate = pairsThisUpdate;
}

//we think about the order of events for a moment, it begins to make sense:
//1,When the first box collides with the ground plane, this turns both objects
//(the box and the ground plane) white.
//2,The second box then collides with the first turning the second box white,while the first box 
//stays white.
//3,Next, the second box separates from the first box, meaning both objects turn black.
//4,Finally, the second box collides with the ground plane, turning the box white once again.
void myApp::CollisionEvent(btRigidBody * pBody0, btRigidBody * pBody1)
{
	////find the two colliding objects
	//GameObject* pObj0 = FindGameObject(pBody0);
	//GameObject* pObj1 = FindGameObject(pBody1);

	////exit if we didn't find anything
	//if (!pObj0 || !pObj1) return;

	////set their colors to white
	//pObj0->SetColor(btVector3(1.0, 1.0, 1.0));
	//pObj1->SetColor(btVector3(1.0, 1.0, 1.0));

	//checks if the two objects involved are the box and the trigger, and if so, it spawns a large 
	//box besides it. Note that we don't necessarily know if  pBody0 or  pBody1 represents 
	//either object, so we need to check both pointers:
	//did the box collide with the trigger?
	if (pBody0 == m_pBox->GetRigidBody() && pBody1 == m_pTrigger ||
	pBody1 == m_pBox->GetRigidBody() && pBody0 == m_pTrigger)
	{
		//if yes, create a big green box nearby
		CreateGameObject(new btBoxShape(btVector3(2, 2, 2)), 2.0, btVector3(0.3, 0.7, 0.3), btVector3(5, 10, 0));
	}
}

void myApp::SeparationEvent(btRigidBody * pBody0, btRigidBody * pBody1)
{
	////get the two separating objects
	//GameObject* pObj0 = FindGameObject((btRigidBody*)pBody0);
	//GameObject* pObj1 = FindGameObject((btRigidBody*)pBody1);

	////exit if we didn't find anything
	//if (!pObj0 || !pObj1)
	//{
	//	return;
	//}

	////set their colors to black
	//pObj0->SetColor(btVector3(0.0, 0.0, 0.0));
	//pObj1->SetColor(btVector3(0.0, 0.0, 0.0));
}

GameObject* myApp::FindGameObject(btRigidBody* pBody)
{
	//search through our list of gameobjects finding the one with a rigid body that matches the given one
	for (GameObjects::iterator iter = m_objects.begin(); iter != m_objects.end(); ++iter)
	{
		if ((*iter)->GetRigidBody() == pBody)
		{
			//found the body, so return the corresponding game object
			return *iter;
		}
	}

	return 0;
}

#include "BulletDynamics/Dynamics/btDynamicsWorld.h"

int main(int argc,char** argv)
{
	myApp demo;

	freeglutGo(argc, argv, 800, 600, "bullet", &demo);

	//ThreadPool::getInstance().closePool();
}
*/

/*
Constraints
These objects limit the range of motion of one object relative to another, giving us the power
to create some very interesting and unique gameplay situations.

Understanding constraints
Constraints, in their most basic form, are the rules which limit the range of motion
of an object relative to some specific object or point in space. For example, think of
a desk chair(一张办公椅). It is made up of multiple parts, but if we push the base of the chair, the
rest must move with it. The same happens if we push the top section; so even though
the chair is made of multiple pieces, they are constrained to one another by a handful
of rules.

Constraints can be used to also simulate the independent rotation of the desk
chair's top section relative to the base. The top section is able to rotate around an
axis without any dependence on what the bottom section is doing. This constraint
is simulated by hooking the top section to an invisible point, and only allowing
rotation around a single axis about that point.

Constraints can vary in how strongly they influence their target objects. A strong
constraint enforces its limitation on movement at all times as strongly as it can. So,
if two objects are connected by a very strong, rigid constraint, it is the equivalent of
being attached together by invisible and unbreakable glue. In other words, if one
object is moved one unit in space, then the other must move one unit in space to
follow it.

Weaker constraints are more like springs. Under the same scenario, the first object
might move one unit in space, but the second moves somewhat less, causing the
two objects to come closer together, or pushed further apart. In addition, the more
they are pushed away from their resting position, the harder the constraint pulls
them back; if we recall our Newtonian physics, this is much like how a simple
spring functions.

Picking up objects:
A feature of most games is to allow the player to pick up and move the objects around
with the mouse cursor or touch screen (also useful for debugging and testing!). There
are several ways to achieve this, such as with forces, or updating the rigid body's
transform each iteration, but we would like to use a constraint to achieve this effect.
The idea is to use our existing raycasting functionality to detect which object was
selected and the exact point of a mouse click. We then create a new constraint at that
point and attach it to the selected object. Then, every time we move the mouse (while
the mouse button is still held down), we update the position of the constraint. The
expectation being that our selected object would move with the constraint, and keep
the same relative position until it is freed from its influence.

There are a handful of different objects which Bullet provides in order to implement
the constraint system. We'll cover the btGenericDof6Constraint object, the
most generic of the available options (hence the name). Its purpose is to give us an
interface to limit the six degrees of freedom (Dof6 for short) of an object; these refer to
the three axes of both linear and angular motion. This constraint can either be used to
hook two rigid bodies together, or hook a single object to a single point in space.
*/

/*
Explaining the persistent manifolds:
Persistent manifolds are the objects that store information between pairs of objects
that pass the broad phase.The broad phase returns a shortlist of the object pairs that might
be touching, but are not necessarily(不一定) touching. They could still be a short distance
apart from one another, so the existence(存在，实体) of a manifold does not imply a collision.
Once you have the manifolds, there's still a little more work to do to verify if there is
a collision between the object pair.

One of the most common mistakes made with the Bullet physics engine is to assume(假定，认为)
that the existence of a manifold is enough to signal a collision. This results in detecting collision
events a couple of frames too early (while the objects are still approaching one another) and
detecting separation(分离) events too late (once they've separated far enough away that they
no longer pass the broad phase). This often results in a desire to blame Bullet for being sluggish,
when the fault lies with the user's original assumptions. Be warned!

Manifolds reside within(居住在) the collision dispatcher,and Bullet keeps the same manifolds
in memory for as long as the same object pairs keep passing the broad phase.This is useful if
you want to keep querying the same contact information between pairs of objects over time.
This is where the persistent part comes in, which serves to optimize the memory allocation
process by minimizing how often the manifolds are created and destroyed.

The manifold class in question is  btPersistentManifold and we can gain access
to the manifold list through the collision dispatcher's getNumManifolds() and
getManifoldByIndexInternal() functions.
Each manifold contains a handful of different functions and member variables
to make use of, but the ones we're most interested in for now are getBody0() ,
getBody1(),and getNumContacts().These functions return the two bodies in
the object pair that passed the broad phase, and the number of contacts detected
between them. We will use these functions to verify if a collision has actually taken
place, and send the involved objects through an event.
*/

/*
Managing the collision event:
There are essentially two ways to handle collision events: either send an event every
update while two objects are touching (and continuously while they're still touching),
or send events both when the objects collide and when the objects separate.

In almost all cases it is wiser to pick the latter option, since it is simply an optimized
version of the first. If we know when the objects start and stop touching, then we
can assume that the objects are still touching between those two moments in time.
So long as the system also informs us of peculiar cases(特殊情况) in separation (such as if one
object is destroyed, or teleports away(传送走) while they're still touching), then we have
everything we need for a collision event system.
*/

/*
Building trigger volumes
Imagine we want an invisible volume of space, and when the player stumbles into(绊倒) it,
it triggers a trap or a cutscene(动画).
This effect is achieved in Bullet by simply disabling the contact responses(禁用接触响应) for any
given rigid body.
*/

/*
Understanding the object motion:
Acceleration can be applied in Bullet through the use of forces.
applyForce()
applyTorque()
applyImpulse()
applyTorqueImpulse()
applyCentralForce()
applyCentralImpulse()
Forces (such as gravity) continuously accelerate an object in a given direction, but do not
affect their rotation.Meanwhile, torque is the rotational equivalent of a force, applying a
rotational acceleration to an object causing it to rotate in place around its center of mass.
Hence,  applyForce() and  applyTorque() provide the means for applying these effects,
respectively.

Meanwhile, the difference between forces and impulses is that impulses are forces
that are independent of time. For instance, if we applied a force to an object for a
single step, the resultant acceleration on that object would depend on how much
time had passed during that step. Thus, two computers running at slightly different
time steps would see two completely different resultant velocities of the object after
the same action. This would be very bad for a networked game, and equally bad for
a single player game that suffered a sudden spike in activity that increased the step
time temporarily.

However, applying an impulse for a single step would give us the exact same
result on both computers because the resultant velocity is calculated without any
dependence on time. Thus, if we want to apply an instantaneous(瞬时) force, it is better to
use  applyImpulse() . Whereas, if we want to move objects over several iterations,
then it is better to use  applyForce() . Similarly, applying a Torque Impulse is an
identical concept, except it applies a rotational impulse. Hence, we would use
applyTorqueImpulse() if we wanted an instantaneous rotational kick.

Finally, the difference between  applyCentralForce() and  applyForce() is simply
that the former always applies the force to the center of mass of the object, while the
latter requires us to provide a position relative to the center of mass (which could
always default to the center of mass, anyway). Basically, the  Central functions are
there for convenience, while the rest are more flexible since in the real world if we
pushed a box on its edge we would expect it to move linearly (force), but also rotate a
little (torque) as it moved. The same distinction(区别) applies to applyCentralImpulse()
and applyImpulse().

Knowing all of this, if we follow the pattern of function names we may notice that
applyCentralTorque() is missing. This is because there's no such thing in the laws
of physics. A torque must always be applied at an offset from the center of mass,
since a central torque would simply be a linear force.

Applying forces:
*/