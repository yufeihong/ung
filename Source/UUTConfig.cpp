#include "UUTConfig.h"

#ifdef UUT_HIERARCHICAL_COMPILE

#pragma comment (lib, "opengl32.lib")

#pragma comment (lib, "glew32.lib")

#if UNG_DEBUGMODE
#pragma comment (lib, "freeglut_staticd.lib")
#else
#pragma comment (lib, "freeglut_static.lib")
#endif

#pragma comment(lib,"glfw3.lib")

namespace
{
	//warning LNK4221: 此对象文件未定义任何之前未定义的公共符号，因此任何耗用此库的链接操作都不会使用此文件。
	int UUT_DUMMY_LNK_4221 = 0;
}//namespace

#endif//UUT_HIERARCHICAL_COMPILE