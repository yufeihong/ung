#include "URMSubMesh.h"

#ifdef URM_HIERARCHICAL_COMPILE

#include "boost/signals2.hpp"
#include "URMMaterialManager.h"

namespace ung
{
	SubMesh::SubMesh()
	{

	}

	SubMesh::~SubMesh()
	{

	}

	String const& SubMesh::getMaterialName() const
	{
		//isLoaded();

		return mMaterialName;
	}

	String const & SubMesh::getTrianglesMode() const
	{
		//isLoaded();

		return mTrianglesMode;
	}

	int32 const SubMesh::getFaceCount() const
	{
		//isLoaded();

		return mFaceCount;
	}

	int32 const SubMesh::getVertexCount() const
	{
		//isLoaded();

		return mVertexCount;
	}

	int32 const SubMesh::getIndicesCount() const
	{
		//isLoaded();

		return mIndexCount;
	}

	STL_VECTOR(uint32) const& SubMesh::getIndices() const
	{
		//isLoaded();

		return mIndices;
	}

	bool SubMesh::hasMaterial() const
	{
		//isLoaded();

		return (mMaterialName != String("nil"));
	}

	void SubMesh::setMaterialName(String const& materialName)
	{
		//std::lock_guard<std::recursive_mutex> lg(mRecursiveMutex);

		mMaterialName = materialName;
	}

	void SubMesh::setTrianglesMode(String const & trianglesMode)
	{
		//std::lock_guard<std::recursive_mutex> lg(mRecursiveMutex);

		mTrianglesMode = trianglesMode;
	}

	void SubMesh::setFaceCount(usize faceCount)
	{
		//std::lock_guard<std::recursive_mutex> lg(mRecursiveMutex);

		mFaceCount = faceCount;
	}

	void SubMesh::setVertexCount(usize vertexCount)
	{
		//std::lock_guard<std::recursive_mutex> lg(mRecursiveMutex);

		mVertexCount = vertexCount;
	}

	void SubMesh::setIndexCount(usize indexCount)
	{
		//std::lock_guard<std::recursive_mutex> lg(mRecursiveMutex);

		mIndexCount = indexCount;
	}

	void SubMesh::addIndices(uint32 index)
	{
		//std::lock_guard<std::recursive_mutex> lg(mRecursiveMutex);

		mIndices.push_back(index);
	}

	void SubMesh::setFuture(std::shared_future<bool> sft)
	{
		//mNakedFuture = sft;
	}

	bool SubMesh::isLoaded() const
	{
		/*
		if (mIsLoaded)
		{
			return mIsLoaded;
		}

		BOOST_ASSERT(mNakedFuture.valid());

		bool const& getRet = mNakedFuture.get();

		BOOST_ASSERT(getRet);
		BOOST_ASSERT(!mIsLoaded);
		mIsLoaded = getRet;
		*/

		//using namespace boost::signals2;

		//auto bindObject = std::bind(&SubMesh::buildMaterial, shared_from_this());
		//signal<void()> sig;
		//sig.connect(bindObject);
		//sig();

		////buildMaterial();

		return mIsLoaded;
	}

	void SubMesh::buildMaterial() const
	{
		auto materialPtr = MaterialManager::getInstance().createMaterial(mMaterialName);

		materialPtr->build();
	}
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

/*
SubMesh,This is because different parts of the mesh may use different materials or use different vertex formats,such that a rendering state
change is required between them.3D objects in the scene share the SubMesh instances,and have the option of overriding their material
differences on a per-object basis if required.
*/

/*
享元模式中的对象通过在多个持有者中并发地共享相同的实例以实现重用。它避免了因在不同上下文中使用
相同对象而导致的重复内存使用。
*/