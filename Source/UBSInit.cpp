#include "UBSInit.h"

#ifdef UBS_HIERARCHICAL_COMPILE

#include "boost/assert.hpp"
#include "boost/algorithm/string.hpp"
#define BOOST_FILESYSTEM_NO_DEPRECATED
#include "boost/filesystem.hpp"
#include "tinyxml2.h"
using namespace tinyxml2;

#if UNG_DEBUGMODE
#pragma comment(lib,"tinyxml2_d.lib")
#else
#pragma comment(lib,"tinyxml2.lib")
#endif

namespace ung
{
	String global_plugins_path;

	void UNG_STDCALL ubsInit()
	{
		XMLDocument doc;
		XMLElement* root;

		doc.LoadFile("../../../Resource/Script/XML/Config/config.xml");
		BOOST_ASSERT(doc.ErrorID() == 0);

		root = doc.RootElement();
		BOOST_ASSERT(root);

		XMLElement* plugins_path_element = root;
		XMLElement* debug_release_element = {};
		
#if UNG_DEBUGMODE
		debug_release_element = plugins_path_element->FirstChildElement("debug");
#else
		debug_release_element = plugins_path_element->FirstChildElement("release");
#endif
		BOOST_ASSERT(debug_release_element);
		const char* plugins_path = debug_release_element->GetText();

		global_plugins_path = plugins_path;

		//路径不能为空
		BOOST_ASSERT(!global_plugins_path.empty());
		//转换为小写
		boost::to_lower(global_plugins_path);
		//替换路径中的"\\","//"为"/"
		boost::ireplace_all(global_plugins_path, "\\", "/");
		//如果是相对路径的话
		if (!boost::filesystem::path(global_plugins_path).is_absolute())
		{
			//必须以"./"或"../"开头
			if (!boost::istarts_with(global_plugins_path, "./") && !boost::istarts_with(global_plugins_path, "../"))
			{
				global_plugins_path = String("./") + global_plugins_path;
			}
		}
		//如果是目录的话
		if (boost::filesystem::is_directory(global_plugins_path.data()))
		{
			//目录必须以"/"结尾
			if (!boost::iends_with(global_plugins_path, "/"))
			{
				global_plugins_path = global_plugins_path + String("/");
			}
		}
	}
}//namespace ung

#endif//UBS_HIERARCHICAL_COMPILE