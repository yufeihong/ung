/*
public和virtual关键字顺序随意.
*/

/*
C++标准中定义了一个异常基类std::exception和try/catch/throw异常处理机制,std::exception又派生出若干子类,用以描述不同种类的异常,如
bad_alloc,bad_cast,out_of_range等,共同构建了C++异常处理框架.
*/

/*
boost exception库针对标准库中异常类的缺陷进行了强化,提供<<操作符重载,可以向异常传入任意数据,有助于增加异常的信息和表达力.
exception的重要能力在于其友元操作符<<,可以存储error_info对象的信息,存入的信息可以用自由函数get_error_info<>()随时再取出来,这个函数
返回一个存储数据的指针,如果exception里没有这种类型的信息则返回空指针.
exception特意没有从std::exception继承.对boost exception总应该采用虚继承.
*/

/*
#define DEFINE_ERROR_INFO(type,name) \
typedef boost::error_info<struct tag_##name, type> name
宏DEFINE_ERROR_INFO接受两个参数,type是它要存储的类型,name是所需要的错误信息类型名,使用预处理命令##创建了error_info所需要的标
签类.它的使用方法很简单,就像是声明一个变量:
DEFINE_ERROR_INFO(int, err_no);
在宏展开后它相当于:
typedef boost::error_info<struct tag_err_no, int> err_no;
*/

/*
exception库支持在线程间传递异常,这需要使用它的clone能力.
使用enable_current_exception()包装异常对象或者使用throw_exception()都能够包装异常对象使之可以被clone.
当发生异常时,线程在需要catch块调用函数current_exception()得到当前异常对象的指针exception_ptr对象,它指向异常对象的拷贝,是线程
安全的,可以被多个线程同时拥有并发修改.rethrow_exception()可以重新抛出异常.

void throw_work()																										//线程工作函数
{
	throw_exception(std::exception("test"));
}

int main()
{
	try
	{
		throw_work();																									//启动一个线程,可能抛出异常
	}
	catch (...)//使用省略号作为异常声明,称为捕获所有异常(catch-all).可以与任意类型的异常匹配.
	{
		exception_ptr e = current_exception();
		std::cout << current_exception_diagnostic_information();
	}
}
*/

/*
冗余包含哨位(include guard)
#ifndef _MYFILE_H_
#define _MYFILE_H_
//头文件的实际内容...
#endif
“打开一个头文件，验证#ifndef并扫描结束符#endif”这个过程非常耗时。在某些情况下，采用冗余的包含哨位可以相当显著地加快编译速度：
#ifndef _MYFILE_H_
#include "myfile.h"
#endif
上面，我们不是简单地#include头文件，而是通过测试头文件里“相同的”哨位符号来守卫这个#include。这当然是冗余的，因为当头文件第一次被包含时，同一个条件（在这个
例子中为#ifndef _MYFILE_H_）将被测试两次，即发生于此处的#include之前，以及该头文件自身之内。然而，对于后续的包含来说，这个冗余的哨位可以防止#include指令
被执行，从而防止头文件被不必要地“打开”和“扫描”。对于大型应用程序来说，使用它们可以成倍地加快编译速度。
*/

/*
只需要在代码中使用BOOST_CURRENT_FUNCTION宏,就可以获得包含该宏的外围函数名称,它表现为一个包含完整函数声明的编译期字符串.
*/

/*
宏BOOST_STRINGIZE可以将任意字面量转换为字符串.它是一个宏,意味着它仅能用于编译期(编译前的预处理),不支持运行时转换.
*/

/*
C++标准允许直接在类声明中为静态整型成员变量赋初始值,但并不是所有编译器都实现了这个特性.BOOST_STATIC_CONSTANT
*/