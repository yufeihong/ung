/*
Typelists
Typelists是一个用来操作一大群型别的C++工具。就像lists对数值提供各种基本操作一样，Typelists对型别也提供相同操作。

Typelists的必要性
有时候你必须针对某些型别重复撰写相同的代码，而且template无法帮上忙。假设你需要实作一个Abstract Factory。Abstract Factory规定你必须针对设计期间已知
的一群型别中的每一个型别定义一个虚函数，像这样：
class WidgetFactory
{
public:
	virtual Window* CreateWindow() = 0;
	virtual Button* CreateButton() = 0;
	virtual ScrollBar* CreateScrollBar() = 0;
};
如果你想将Abstract Factory的概念泛化，并将它纳入程序库中，你必须让使用者得以产生针对任意型别（而不只是Window，Button和ScrollBar）的工厂。template
无法支持这一特性。
如果你不试图泛化基础概念，就不太有机会泛化这些概念的具象实体。这是很重要的原则。如果你没能将本质（essence）泛化，你仍然得和本质所派生的具象实体纠缠
奋战。在Abstract Factory中，虽然抽象的base class十分简单，但你会在实作各种各样factories时遇到很多烦人又重复的代码。
template无法拥有不定量参数。
虚函数不可以是templates。

定义Typelists
template <class T, class U>
struct Typelist
{
	typedef T Head;
	typedef U Tail;
};
Typelists持有两个型别，我们可通过其内部型别Head和Tail加以访问。
下面是有着三个char型别变体的Typelists：
typedef Typelists<char,Typelists<signed char,unsigned char>> CharList;
Typelists内部没有任何数值（value）：它们的实体是空的，不含任何状态（state），也未定义任何函数。执行期间Typelists不带任何数值。它们存在的理由只是为了携带
型别信息。因此，对Typelists的任何处理都一定发生在编译期。Typelists并未打算被具现化。
Typelist接受两个参数，所以我们总是可以藉由“将其中一个参数置换为另一个Typelist”来达到无限延伸的目的。
然而有个小问题，虽然我们可以表达出持有两个型别或更多型别的Typelist，但我们无法表达出持有零个或一个型别的Typelist。我们需要一个null list type。
现在我来定制一个习惯：每个Typelist都必须以NullType结尾。NullType可被视为一个结束记号，类似传统C字符串的\0功能。
现在我们可以定义一个只持有单一元素的Typelist如下：
typedef Typelist<int,NullType> OneTypeOnly;

计算长度
大部分Typelist操作函数的基本概念是，开拓“递归式（recursive）template”，这是指某种template以其本身具现体当做其定义的一部分。当这么做时，它们会传递一个
不同的template引数列表。这种形式所产生的递归将止于一个“被作为边界情况运用”的特化体（explicit specialization）。
template<typename TList>
struct Length;

template<>
struct Length<NullType>
{
	enum{value = 0};
};

template<typename T,typename U>
struct Length<Typelist<T, U>>
{
	enum{value = 1 + Length<U>::value};
};
null typelist的长度为0，其他typelist的长度是Tail的长度加1。
上述第一版本是Length全特化，只匹配NullType。第二版本是Length偏特化，可匹配任何Typelist<T,U>，包括“复合型（compound）typelist”---亦即U本身又是个
Typelist<V,W>。第二份特化完成了递归式运算。它将value定义为数值1（用来将Head T计算进去）加上Tail的长度。当Tail变成NullType时，吻合第一份特化定义，于是
停止递归并巧妙地传回结果。

间奏曲
我们的“C++编译期编程”工具是：template，编译期整数计算，typedef。
1，template，更明确地说是指模板特化---提供编译期间的if叙述。
2，整数计算，提供真实的数值计算能力，用以从型别转为数值。所有编译期数值都是不可变的，一旦你为它定义了一个整数常数，例如一个枚举值，就不能再改变它（也就是
说不能重新赋予他值）。
3，typedef可被视为用来引进“具名的型别常数”。它们也是定义之后就被冻结---你不能将typedef定义的符号重新定义为另一个型别。

编译期计算有两个特点，使它根本上不兼容于迭代。所谓迭代是持有一个迭代器并改变它，直到某些条件吻合。由于编译期间我们并没有“可资变化的任何东西”，所以无法实现
迭代。

索引式访问
通过索引来访问typelist元素，这将使typelist的访问线性化。
当然了，索引必须是个编译期数值。
TypeAt:
输入：typelist TList，索引值i
输出：内部某型别Result
如果TList不为null，且i为0，那么Result就是TList的头部。
否则
		如果TList不为null且i不为0，那么Result就是“将TypeAt施行于TList尾端，同时索引为i - 1”的结果
		否则，逾界（out-of-bound）访问，造成编译错误。
template <class TList, unsigned int index>
struct TypeAt;

template <class Head, class Tail>
struct TypeAt<Typelist<Head, Tail>, 0>
{
	typedef Head Result;
};

template <class Head, class Tail, unsigned int i>
struct TypeAt<Typelist<Head, Tail>, i>
{
	typedef typename TypeAt<Tail, i - 1>::Result Result;
};

查找typelist
IndexOf:
输入：typelist TList，type T
输出：内部编译期常数value
如果TList是NullType，令value为-1
否则
		如果TList的头端是T，令value为0
		否则
				将IndexOf施行于“TList尾端”，并将结果置于一个暂时变量temp
				如果temp为-1，令value为-1
				否则令value为1 + temp
template <class TList, class T>
struct IndexOf;

template <class T>
struct IndexOf<NullType, T>
{
	enum { value = -1 };
};

template <class T, class Tail>
struct IndexOf<Typelist<T, Tail>, T>
{
	enum { value = 0 };
};

template <class Head, class Tail, class T>
struct IndexOf<Typelist<Head, Tail>, T>
{
private:
	enum { temp = IndexOf<Tail, T>::value };

public:
	enum { value = (temp == -1 ? -1 : 1 + temp) };
};

附加元素至typelist
将“某个type或整个typelist”加入typelist。修改typelist是不可能的，但我们将以by value方式传回一个我们所期望的新typelist。
Append:
输入：typelist TList,type or typelist T
输出：内部某型别Result
如果TList是NullType而且T是NullType，那么令Result为NullType
否则
		如果TList是NullType，且T是个type（而非typelist），那么Result将是“只含唯一元素T”的一个typelist
		否则
				如果TList是NullType，且T是一个typelist，那么Result便是T本身。
				否则，如果TList是non-null，那么Result将是个typelist，以TList::Head为其头端，并以“T附加至TList::Tail”的结果为其尾端
template <class TList, class T>
struct Append;

template <>
struct Append<NullType, NullType>
{
typedef NullType Result;
};

template <class T>
struct Append<NullType, T>
{
typedef Typelist<T, NullType> Result;
};

template <class Head, class Tail>
struct Append<NullType, Typelist<Head, Tail> >
{
typedef Typelist<Head, Tail> Result;
};

template <class Head, class Tail, class T>
struct Append<Typelist<Head, Tail>, T>
{
typedef Typelist<Head,typename Append<Tail, T>::Result> Result;
};

移除typelist中的某个元素
Erase:只移除第一个出现者
输入：typelist TList，type T
输出：内部某型别Result
如果TList是NullType，那么Result就是NullType
否则
		如果T等同于TList::Head，那么Result就是TList::Tail
		否则，Result将是一个typelist，它以TList::Head为头端，并以“将Erase施行于TList::Tail”所得结果为尾端
template <class TList, class T>
struct Erase;

template <class T>																						//Specialization 1
struct Erase<NullType, T>
{
	typedef NullType Result;
};

template <class T, class Tail>																		//Specialization 2
struct Erase<Typelist<T, Tail>, T>
{
	typedef Tail Result;
};

template <class Head, class Tail, class T>														//Specialization 3
struct Erase<Typelist<Head, Tail>, T>
{
	typedef Typelist<Head,typename Erase<Tail, T>::Result>
		Result;
};

EraseAll，它会移除typelist中某个型别的所有出现个体。其实作手法类似Erase，唯一不同的是，发现移除对象后算法并不停止下来，而是继续查找下一个符合条件的元素
并删除之，直到list尾端。

移除重复元素（Erasing Duplicates）
NoDuplicates:
输入：typelist TList
输出：内部某型别Result
如果TList是NullType，那么就令Result为NullType
否则
		将NoDuplicates施行于TList::Tail身上，获得一个暂时的typelist L1
		将Erase施行于L1，第二个参数为TList::Head，获得L2
		Result是个typelist，其头端为TList::Head，尾端为L2

取代typelist中的某个元素
Replace:
输入：typelist TList，type T（被取代者），以及type U（取代者）
输出：内部某型别Result
如果TList是NullType，那么就令Result为NullType
否则
		如果TList的头端是T，那么Result将是一个typelist，以U为其头端并以TList::Tail为其尾端。
		否则Result是一个typelist，以TList::Head为其头端，并以“Replace施行于TList”的结果为其尾端

为typelist局部更换次序（Partially Ordering）
假设我们打算根据继承关系进行排序，例如希望派生型别出现在基础型别之前。
DerivedToFront
输入：typelist TList
输出：内部某型别Result
如果TList是NullType，那么就令Result为NullType
否则
		从TList::Head到TList::Tail，找出最末端派生型别，存储于暂时变量TheMostDerived中。
		以TList::Head取代TList::Tail中的TheMostDerived，获得L。
		建立一个typelist，以TheMostDerived为其头端，以L为其尾端。

MostDerived算法接受一个typelist和一个Base型别，传回typelist中Base的最深层派生型别（如果找不到任何派生型别，就传回Base自己）。
MostDerived:
输入：typelist TList,type T
输出：内部某型别Result
如果TList是NullType，令Result为T。
否则
		将MostDerived施行于TList::Tail和T身上，获得一个Candidate。
		如果TList::Head派生自Candidate，令Result为TList::Head。
		否则，令Result为Candidate。

运用Typelists自动产生Classes
以typelist作为代码生成机制。这么一来我们就不必手写太多代码，而是驱动编译器帮我们自动产生代码。这些概念采用了C++最具威力的特性之一，一个不存在于
任何其他语言的特性---template template parameters。
截至目前，typelist的操作都没有产生真正的代码，运作过程只产生typelists，types或编译期常数（例如Length）。让我们尽可能产生一些真正的代码，也就是真正
能够在编译结果中留下足迹的东西。
typelist对象本身没什么用，它们缺乏执行期状态（state）和机能（functionality）。从typelist中产生classes是很重要的编程需求。应用程序编写者有时需要以一种
“typelist所指示的方向”来编写classes---填以虚函数，数据声明或函数实作。

产生“散乱的继承体系（scattered hierarchies）”
将typelist里的每一个型别套用于一个由用户提供的基本template身上。
使用者只需自行定义一个单一参数的template即可。
template<typename TList,template<typename> class Unit>
class GenScatterHierarchy;

//GenScatterHierarchy specialization:Typelist to Unit
template<typename T1,typename T2,template<typename> class Unit>
class GenScatterHierarchy<TypeList<T1, T2>,Unit> : public GenScatterHierarchy<T1,Unit>,public GenScatterHierarchy<T2,Unit>
{
};

//Pass an atomic type(nontypelist) to Unit
template <class AtomicType, template <class> class Unit>
class GenScatterHierarchy : public Unit<AtomicType>
{
};

//Do nothing for NullType
template <template <class> class Unit>
class GenScatterHierarchy<NullType, Unit>
{
};
GenScatterHierarchy做了什么？
如果其第一引数是个atomic type(单一型别，相对于typelist)，GenScatterHierarchy便把该型别传给Unit，然后继承Unit<T>。
如果GenScatterHierarchy的第一引数是个typelist TList，就递归产生GenScatterHierarchy<TList::Head,Unit>和GenScatterHierarchy<TList::Tail,Unit>
并继承此二者。
GenScatterHierarchy<NullType,Unit>则是个空类。
最终，一个GenScatterHierarchy具现体会继承Unit“对typelist中每一个型别”的具现体。
举例：
template<typename T>
struct Holder
{
	T value;
};

typedef GenScatterHierarchy<TypeList<int,string,Widget>,Holder> WidgetInfo;

GenScatterHierarchy的要旨是：它藉由重复具现化一个你所提供的class template（视之为模型），为你产生一个class继承体系，然后将所有这样产生出来的classes
聚焦至一个leaf class，亦即上面的WidgetInfo。

由于继承了Holder<int>,Holder<string>和Holder<Widget>,WidgetInfo因而针对它们（亦即typelist中的每个型别）各自拥有一个成员变量value。
空类GenScatterHierarchy<NullType,Holder>会被优化掉，不会在这个复合对象中占据实体位置。

WidgetInfo obj;
string name = (static_cast<Holder<string>&>(obj)).value;
其中的显式转型是必要的，以消除成员变量名称value的可能含糊意义，否则编译器不知道你打算取用哪一个value。

//Access a base class by type name
template<typename T,typename TList,template<typename> class Unit>
Unit<T>& Field(GenScatterHierarchy<TList,Unit>& obj)
{
	return obj;
}
Field依赖derived-to-base隐式转换。如果你调用Field<Widget>(obj)，编译器会知道Holder<Widget>是WidgetInfo的base class并只传回复合对象中该成分
的reference。
上述Field，当你的typelist内含重复型别时，你不能使用，因为WidgetInfo确实通过了不同的路径继承了（比如）Holder<int>两次，所以编译器会抱怨出现模棱两可
（歧义）情况。

因此，我们需要一个“以索引选择GenScatterHierarchy实体栏位（fields）”的方法，而非通过型别名称。
我们必须在编译期分派（dispatch）索引值零（用以访问typelist Head）和非零（用以访问typelist Tail）。
Int2Type可以直接将不同常数转换为不同型别。

template<typename TList,template<typename> class Unit>
Unit<TList::Head>& FieldHelper(GenScatterHierarchy<TList,Unit>& obj,Int2Type<0>)
{
	GenScatterHierarchy<TList::Head, Unit>& leftBase = obj;

	return leftBase;
}

template<int i,typename TList,template<typename> class Unit>
Unit<TypeAt<TList, i>::Result>& FieldHelper(GenScatterHierarchy<TList, Unit>& obj, Int2Type<i>)
{
	GenScatterHierarchy<TList::Tail,Unit>& rightBase = obj;

	return FieldHelper(rightBase, Int2Type<i - 1>());
}

template<int i,typename TList,template<typename> class Unit>
Unit<TypeAt<TList, i>::Result>& Field(GenScatterHierarchy<TList,Unit>& obj)
{
	return FieldHelper(obj, Int2Type<i>());
}
名为FieldHelper的两个重载函数做了实际工作。第一版本接受一个型别为Int2Type<0>的参数，第二版本接受的型别是Int2Type<any integer>。因此，第一版本
回传的对象相当于Unit<T1>&，第二版本回传的是typelist之中被索引标示出来的型别。第二版本递归调用自己的一个特化体，传入GenScatterHierarchy的右侧base 
class和Int2Type<i - 1>。这么做是因为，对任何非零值N而言，typelist内第N个栏位其实就是Tail的第N - 1个栏位（N = 0的情况则由第一个重载版本负责处理）。

WidgetInfo obj;
int x = Field<0>(obj).value;																		//first int
int y = Field<1>(obj).value;																		//second int

你可以运用GenScatterHierarchy对typelist中的每一个types产生一些虚函数。

产生Tuples
Tuples很适合用来产生一个无任何成员函数的无名结构。

产生线性继承体系
下面这个例子，它定义了一个事件处理接口（event handler interface），其中只定义了一个成员函数OnEvent:
template<typename T>
class EventHandler
{
public:
	virtual void OnEvent(const T&, int eventID) = 0;
	virtual ~EventHandler()
	{
	}
};
为了策略正确性，EventHandler还定义了一个虚析构函数，这是绝对必要的。

我们可以运用GenScatterHierarchy传法（distribute）给typelist里任何型别的一个EventHandler：
typedef GenScatterHierarchy<TYPELIST_3(Window,Button,ScrollBar),EventHandler> WidgetEventHandler;

GenScatterHierarchy的缺点是它使用了多重继承。如果你很在乎对象大小的优化，那么GenScatterHierarchy也许就不那么好了，因为WidgetEventHandler内有三个
指向虚函数表（vtables）的指针，一个指针针对一个EventHandler函数实体。
如果希望获得最佳空间使用率，应该把所有虚函数声明到WidgetEventHandler里头，但这会破坏代码产生机会。
解决办法是，藉由单一继承（而非多重继承），这样的话WidgetEventHandler只有一个vtable指针。
template<typename T,typename Base>
class EventHandler : public Base
{
	virtual void OnEvent(const T&, int eventID) = 0;
};

typedef GenLinearHierarchy<TYPELIST_3(Window,Button,ScrollBar),EventHandler> MyEventHandler;

线性继承体系比较高效，散乱继承体系则具备一个有用性质：使用者自定义的template所具现出来的所有实体，都是最后产出之class的父类。

GenLinearHierarchy会将typelist TList内的每一个型别传入Unit当作其第一参数，以此具现化Unit。很重要的一点：Unit必须以public方式继承其第二template参数。

重载函数Field允许使用者“根据型别名称”和“根据索引”访问继承体系中的任一节点。共8个重载版本，包含const和non-const版本、“根据型别”和“根据索引”版本，以及
“针对GenScatterHierarchy”和“针对GenLinearHierarchy”版本。
Field<Type>(obj)传回一个reference指向某Unit具现体，后者相应于特定型别Type。
Field<index>(obj)传回一个reference指向某Unit具现体，后者相应于“以常数index标记出来的那个型别”。
*/
