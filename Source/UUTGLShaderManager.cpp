#include "UUTGLShaderManager.h"

#ifdef UUT_HIERARCHICAL_COMPILE

#include "UUTGLShader.h"
#include "UUTGLProgram.h"

namespace ung
{
	ShaderManager::~ShaderManager()
	{
	}

	std::shared_ptr<GLProgram> ShaderManager::createProgram(String const& materialFileName)
	{
#if UNG_DEBUGMODE
		//检查材质文件是否有效
		//auto validRet = Filesystem::getInstance().isRegularFile(materialFileName.c_str());
		//BOOST_ASSERT(validRet);
#endif

		//查询是否已经存在
		std::shared_ptr<GLProgram> program = getGLProgram(materialFileName);

		if (program)
		{
			return program;
		}

		//创建program
		program = std::make_shared<GLProgram>();

		//从材质中得到着色器程序的文件名
		String vertexName("../../../../Resource/Media/Materials/Shaders/GLSL/triangles140.vert");
		String fragmentName("../../../../Resource/Media/Materials/Shaders/GLSL/triangles140.frag");
		//这里需要检查材质中必须包含顶点程序和片元程序

		//创建shader
		auto vs = createShader(vertexName);
		auto ps = createShader(fragmentName);

		//存储shader
		insertShader(vertexName, vs);
		insertShader(fragmentName, ps);

		//program attach 着色器程序
		program->attach(vs);
		program->attach(ps);

		//存储program
		insertProgram(materialFileName, program);

		return program;
	}

	void ShaderManager::linkProgram(std::shared_ptr<GLProgram> program)
	{
		program->link();
	}

	void ShaderManager::useProgram(std::shared_ptr<GLProgram> program)
	{
		program->use();
	}

	std::shared_ptr<GLShader> ShaderManager::createShader(String const& fileName)
	{
#if UNG_DEBUGMODE
		//检查着色器程序文件是否有效
		auto validRet = Filesystem::getInstance().isRegularFile(fileName.c_str());
		BOOST_ASSERT(validRet);
#endif

		//查询是否已经存在
		std::shared_ptr<GLShader> shader = getGLShader(fileName);

		if (shader)
		{
			return shader;
		}

		//创建shader
		shader = std::make_shared<GLShader>(fileName);

		//读取着色器程序文件
		shader->read(fileName);

		//编译shader
		shader->compile();

		return shader;
	}

	void ShaderManager::insertShader(String const & shaderName, std::shared_ptr<GLShader> shader)
	{
		auto insertRet = mShaders.insert({shaderName,shader});
		BOOST_ASSERT(insertRet.second);
	}

	void ShaderManager::eraseShader(String const & shaderName)
	{
		auto eraseRet = mShaders.erase(shaderName);
		BOOST_ASSERT(eraseRet);
	}

	void ShaderManager::eraseAllShader()
	{
		mShaders.clear();
		BOOST_ASSERT(!mShaders.size());
	}

	std::shared_ptr<GLShader> ShaderManager::getGLShader(String const& shaderName)
	{
		auto ret = mShaders.find(shaderName);

		auto end = mShaders.end();
		if (ret != end)
		{
			return ret->second;
		}

		return nullptr;
	}

	std::shared_ptr<GLProgram> ShaderManager::getGLProgram(String const& materialName)
	{
		auto ret = mPrograms.find(materialName);

		auto end = mPrograms.end();
		if (ret != end)
		{
			return ret->second;
		}

		return nullptr;
	}

	void ShaderManager::insertProgram(String const & materialName, std::shared_ptr<GLProgram> program)
	{
		auto insertRet = mPrograms.insert({ materialName,program });
		BOOST_ASSERT(insertRet.second);
	}

	void ShaderManager::eraseProgram(String const & materialName)
	{
		auto eraseRet = mPrograms.erase(materialName);
		BOOST_ASSERT(eraseRet);
	}

	void ShaderManager::eraseAllProgram()
	{
		mPrograms.clear();
		BOOST_ASSERT(!mPrograms.size());
	}
}//namespace ung

#endif//UUT_HIERARCHICAL_COMPILE