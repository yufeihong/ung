#include "USMCamera.h"

#ifdef USM_HIERARCHICAL_COMPILE

#include "UNGRoot.h"
#include "UIFIRenderSystem.h"
#include "URMViewport.h"

namespace ung
{
	Camera::Camera(String const& name,Vector3 const & pos, Vector3 const & lookat, real_type aspect, Radian fovY, real_type nDis, real_type fDis) :
		mName(name),
		mPosition(pos),
		mLookAt(lookat),
		mFrustum(aspect,fovY,nDis,fDis),
		mViewport(nullptr)
	{
		BOOST_ASSERT(!mName.empty());

		updateViewMatrix();
	}

	Camera::~Camera()
	{
		UNG_DEL(mViewport);
	}

	String const & Camera::getName() const
	{
		return mName;
	}

	void Camera::setPosition(Vector3 const & pos)
	{
		mPosition = pos;

		updateViewMatrix();
	}

	Vector3 const & Camera::getPosition() const
	{
		return mPosition;
	}

	void Camera::setLookAt(Vector3 const & lookat)
	{
		mLookAt = lookat;

		updateViewMatrix();
	}

	Vector3 const & Camera::getLookAt() const
	{
		return mLookAt;
	}

	void Camera::setFOVy(const Radian & fovy)
	{
		mFrustum.setFOVy(fovy);
	}

	const Radian & Camera::getFOVy() const
	{
		return mFrustum.getFOVy();
	}

	void Camera::setNearClipDistance(real_type nearDist)
	{
		mFrustum.setNearClipDistance(nearDist);
	}

	real_type Camera::getNearClipDistance() const
	{
		return mFrustum.getFarClipDistance();
	}

	void Camera::setFarClipDistance(real_type farDist)
	{
		mFrustum.setFarClipDistance(farDist);
	}

	real_type Camera::getFarClipDistance() const
	{
		return mFrustum.getFarClipDistance();
	}

	void Camera::setAspectRatio(real_type ratio)
	{
		mFrustum.setAspectRatio(ratio);
	}

	real_type Camera::getAspectRatio() const
	{
		return mFrustum.getAspectRatio();
	}

	real_type Camera::getNearPlaneWidth() const
	{
		return mFrustum.getNearPlaneWidth();
	}

	real_type Camera::getNearPlaneHeight() const
	{
		return mFrustum.getNearPlaneHeight();
	}

	real_type Camera::getFarPlaneWidth() const
	{
		return mFrustum.getFarPlaneWidth();
	}

	real_type Camera::getFarPlaneHeight() const
	{
		return mFrustum.getFarPlaneHeight();
	}

	Vector3 Camera::getRightDir() const
	{
		Vector3 U;
		Vector3 V(0, 1, 0);
		//从目标位置指向相机位置，（之所以是从目标指向相机，是因为我们使用的是右手坐标系），视见方向为-N
		Vector3 N;
		N = mPosition - mLookAt;
		N.normalize();
		U = V.crossProductUnit(N);
		U.normalize();

		return U;
	}

	Vector3 Camera::getUpDir() const
	{
		Vector3 U;
		Vector3 V(0, 1, 0);
		//从目标位置指向相机位置，（之所以是从目标指向相机，是因为我们使用的是右手坐标系），视见方向为-N
		Vector3 N;
		N = mPosition - mLookAt;
		N.normalize();
		U = V.crossProductUnit(N);
		U.normalize();
		V = N.crossProductUnit(U);

		BOOST_ASSERT(V.isUnitLength());

		return V;
	}

	Vector3 Camera::getLookDir() const
	{
		//从目标位置指向相机位置，（之所以是从目标指向相机，是因为我们使用的是右手坐标系），视见方向为-N
		Vector3 N;
		N = mPosition - mLookAt;
		N.normalize();

		return -N;
	}

	Vector3 Camera::getNearPlaneCenter() const
	{
		return mPosition + getLookDir() * getNearClipDistance();
	}

	Vector3 Camera::getFarPlaneCenter() const
	{
		return mPosition + getLookDir() * getFarClipDistance();
	}

	Vector3 Camera::getIndexedCorner(uint32 index) const
	{
		BOOST_ASSERT(index >= 0 && index < 8);

		Vector3 corner{};

		auto nCenter = getNearPlaneCenter();
		auto fCenter = getFarPlaneCenter();

		auto nearHalfW = getNearPlaneWidth() * 0.5;
		auto nearHalfH = getNearPlaneHeight() * 0.5;
		auto farHalfW = getFarPlaneWidth() * 0.5;
		auto farHalfH = getFarPlaneHeight() * 0.5;

		auto U = getRightDir();
		auto V = getUpDir();

		auto uNearFactor = U * nearHalfW;
		auto vNearFactor = V * nearHalfH;
		auto uFarFactor = U * farHalfW;
		auto vFarFactor = V * farHalfH;

		switch (index)
		{
		case 0:
			corner = nCenter - uNearFactor + vNearFactor;
			break;
		case 1:
			corner = nCenter + uNearFactor + vNearFactor;
			break;
		case 2:
			corner = nCenter + uNearFactor - vNearFactor;
			break;
		case 3:
			corner = nCenter - uNearFactor - vNearFactor;
			break;

		case 4:
			corner = fCenter - uFarFactor + vFarFactor;
			break;
		case 5:
			corner = fCenter + uFarFactor + vFarFactor;
			break;
		case 6:
			corner = fCenter + uFarFactor - vFarFactor;
			break;
		case 7:
			corner = fCenter - uFarFactor - vFarFactor;
			break;
		}

#if UNG_DEBUGMODE
		//验证
		//裁剪空间中的角
		Vector4 clipSpaceCorner{};

		auto vpMat = mView * getProjectionMatrix();

		clipSpaceCorner = Vector4(corner, 1.0) * vpMat;
		clipSpaceCorner /= clipSpaceCorner.w;

		auto renderSystemType = Root::getInstance().getCurrentRenderSystemType();

		if (renderSystemType == RenderSystemType::RST_D3D)
		{
			switch (index)
			{
			case 0:
				BOOST_ASSERT(clipSpaceCorner == Vector4(-1.0, 1.0, 0.0, 1.0));
				break;
			case 1:
				BOOST_ASSERT(clipSpaceCorner == Vector4(1.0, 1.0, 0.0, 1.0));
				break;
			case 2:
				BOOST_ASSERT(clipSpaceCorner == Vector4(1.0, -1.0, 0.0, 1.0));
				break;
			case 3:
				BOOST_ASSERT(clipSpaceCorner == Vector4(-1.0, -1.0, 0.0, 1.0));
				break;

			case 4:
				BOOST_ASSERT(clipSpaceCorner == Vector4(-1.0, 1.0, 1.0, 1.0));
				break;
			case 5:
				BOOST_ASSERT(clipSpaceCorner == Vector4(1.0, 1.0, 1.0, 1.0));
				break;
			case 6:
				BOOST_ASSERT(clipSpaceCorner == Vector4(1.0, -1.0, 1.0, 1.0));
				break;
			case 7:
				BOOST_ASSERT(clipSpaceCorner == Vector4(-1.0, -1.0, 1.0, 1.0));
				break;
			}
		}
		else if (renderSystemType == RenderSystemType::RST_GL)
		{
			switch (index)
			{
			case 0:
				BOOST_ASSERT(clipSpaceCorner == Vector4(-1.0, 1.0, -1.0, 1.0));
				break;
			case 1:
				BOOST_ASSERT(clipSpaceCorner == Vector4(1.0, 1.0, -1.0, 1.0));
				break;
			case 2:
				BOOST_ASSERT(clipSpaceCorner == Vector4(1.0, -1.0, -1.0, 1.0));
				break;
			case 3:
				BOOST_ASSERT(clipSpaceCorner == Vector4(-1.0, -1.0, -1.0, 1.0));
				break;

			case 4:
				BOOST_ASSERT(clipSpaceCorner == Vector4(-1.0, 1.0, 1.0, 1.0));
				break;
			case 5:
				BOOST_ASSERT(clipSpaceCorner == Vector4(1.0, 1.0, 1.0, 1.0));
				break;
			case 6:
				BOOST_ASSERT(clipSpaceCorner == Vector4(1.0, -1.0, 1.0, 1.0));
				break;
			case 7:
				BOOST_ASSERT(clipSpaceCorner == Vector4(-1.0, -1.0, 1.0, 1.0));
				break;
			}
		}
#endif

		return corner;
	}

	Matrix4 const & Camera::getViewMatrix() const
	{
		return mView;
	}

	Viewport* Camera::getAttachedViewport() const
	{
		BOOST_ASSERT(mViewport);

		return mViewport;
	}

	void Camera::updateViewMatrix()
	{
		mView = Matrix4::makeView(mPosition, mLookAt);
	}
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE

/*
A viewpoint from which the scene will be rendered.

Renders scenes from a camera viewpoint into a buffer of some sort(某种buffer),normally a window or a texture (a subclass of RenderTarget).

Cameras support both perspective projection and orthographic projection.

Each camera carries with it a style of rendering,(e.g. full textured,flat shaded,wireframe),field of view,rendering distances etc,allowing you to create
complex multi-window views if required.

In addition(此外,另外),more than one camera can point at a single render target if required,each rendering to a subset of the target,allowing split
screen and picture-in-picture views.

Cameras maintain their own aspect ratios,field of view,and frustum,and project co-ordinates into a space measured from -1 to 1 in x and y,and 0 to
1 in z.
At render time,the camera will be rendering to a Viewport which will translate these parametric(参数) co-ordinates into real screen co-ordinates.
Obviously it is advisable that the viewport has the same aspect ratio as the camera to avoid distortion.

Note that a Camera can be attached to a SceneNode.If this is done the Camera will combine(结合,合并) it's own position/orientation settings with it's
parent SceneNode.This is useful for implementing more complex Camera/object relationships i.e. having a camera attached to a world object.

摄像机既可以被连接到场景的节点上也可以存在于自由的空间中
摄像机可以在没有挂接到场景节点的情况下可以直接放置在场景中,也可以不依赖节点进行自身的旋转移动等操作.
需要注意的是,在把摄像机放置在节点上操作的时候,需要关闭其"锁定偏移轴(use fixed yaw axis)"设置,否则摄像机将会维持固定的"向上"状态,导致无法进行滚动(Roll)操
作.
*/

/*
Camera's local coordinate system(called view space or the camera coordinate system).
*/