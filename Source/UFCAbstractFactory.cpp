/*
Abstract Factory（抽象工厂）：
抽象工厂是一个接口，用来生成一族系“相互关联”或“相互依赖”的多态对象。

举例：你要为游戏提供一个初等级别和一个高难度级别，这两大类型的types构成了两个族系（family）。游戏期间你一定会使用这两个族系中的某一族对象，但绝不会同时
使用它们。
定义一个Enemy base class，并从它派生出两个精确接口：Soldier,Monster。然后再从这些接口派生出SillySoldier,SillyMonster用于初等级别，以及BadSoldier,BadMonster
用于高难度级别。值得注意的是，在你的游戏中，SillyMonster具现体和BadMonster具现体绝不会同时存在。这两大类型的types构成了两个族系。
Soldier : public Enemy
Monster : public Enemy
SillySoldier : public Soldier
BadSoldier : public Soldier
SillyMonster : public Monster
BadMonster : public Monster

class AbstractEnemyFactory
{
public:
	virtual Soldier* MakeSoldier() = 0;
	virtual Monster* MakeMonster() = 0;
};
然后你便可以针对不同的难度级别，实作出一个具体的enemy factory，这个factory将根据游戏策略规定去生成enemies。
class EasyLevelEnemyFactory : public AbstractEnemyFactory
{
public:
	virtual Soldier* MakeSoldier()
	{
		return new SillySoldier;
	}

	virtual Monster* MakeMonster()
	{
		return new SillyMonster;
	}
};
class DieHardLevelEnemyFactory : public AbstractEnemyFactory
{
public:
	virtual Soldier* MakeSoldier()
	{
		return new BadSoldier;
	}

	virtual Monster* MakeMonster()
	{
		return new BadMonster;
	}
};
最终，你可以通过合适的具象类(EasyLevelEnemyFactory或DieHardLevelEnemyFactory)来初始化pointer to AbstractEnemyFactory。
这是抽象工厂模式的一个典型应用。
Soldier，Monster被称为abstract products（抽象产品）。
SillySoldier,SillyMonster,BadSoldier,BadMonster被称为concrete products（具体产品）。
抽象工厂的主要缺点是，它对型别的需求很强烈：abstract factory base class必须知晓每一个待生成的abstract product。上面例子中的每一个concrete factory class
都与它将生成的concrete products相依。
下面提供一个抽象工厂的泛型实现：它降低了静态依存性，而又无损型别安全。

一个泛化的抽象工厂接口：
GenScatterHierarchy class template，它会“以某个typelist中的每一个型别作为template参数”，将用户提供的base template逐一具现出来。通过这样的结构，GenScatterHierarchy
的最终具现体继承了“用户提供之template”的所有具现体。你可以先定义一个接口，用来生成某种类型的对象，然后通过GenScatterHierarchy将该接口应用至多个型别。
template<typename T>
class AbstractFactoryUnit
{
public:
	virtual ~AbstractFactoryUnit()
	{
	}

	virtual T* DoCreate(Type2Type<T>) = 0;
};
Type2Type是个简单的template，其唯一用途是消除重载函数的歧义（模棱两可，ambiguity）。但是带有歧义的函数在哪儿呢？AbstractFactoryUnit只定义了一个DoCreate()
函数啊。答案是，同一个继承体系中有数个AbstractFactoryUnit具现体，对于所生成的各个DoCreate()重载函数，Type2Type<T>有助于消除歧义。
template <typename TList,template <typename> class Unit = AbstractFactoryUnit>
class AbstractFactory : public GenScatterHierarchy<TList, Unit>
{
public:
	typedef TList ProductList;

	template <typename T>
	T* create()
	{
		Unit<T>& unit = *this;

		return unit.doCreate(Type2Type<T>());
	}
};
正是因为Type2Type发挥了作用，create()才知道该调用哪一个doCreate()函数。
typedef AbstractFactory<TYPELIST_2(Soldier,Monster)> AbstractEnemyFactory;
相当于：AbstractEnemyFactory : public AbstractFactoryUnit<Soldier>,public AbstractFactoryUnit<Monster>。
每一个AbstractFactoryUnit具现体都定义了一个纯虚函数create(),所以AbstractEnemyFactory有两个create()重载函数。
和
class AbstractEnemyFactory
{
public:
	virtual Soldier* MakeSoldier() = 0;
	virtual Monster* MakeMonster() = 0;
};
几乎是一样的。
AbstractFactory的template成员函数create()是一个分派器（dispatcher，发送器），它将生成请求分派（发送）给相应的base class：
AbstractEnemyFactory* p = ...;
Monster* pMonster = p->create<Monster>();
AbstractEnemyFactory是一个高度“粒度化（granular）”接口，你可以将reference to AbstractEnemyFactory自动转换为一个reference to AbstractFactory<Soldier>
或AbstractFactory<Monster>。这么一来，你就只需要将这个factory的某个小小子单元传给程序各部分。假设某个模块（例如xxx.cpp)只需生成Soldiers，你可以藉由
pointer to-或reference to- AbstractFactory<Soldier>来和该模块通讯，于是xxx.cpp就不会和Monster之间产生耦合（couple）关系。
自动生成的AbstractEnemyFactory的第二个优点是：你可以将实作自动化。

实作出Abstract Factory：
建造一个“初等级别”的concrete factory：
typedef ConcreteFactory<AbstractEnemyFactory,OpNewFactoryUnit,TYPELIST_2(SillySoldier,SillyMonster)> EasyLevelEnemyFactory;
1，AbstractEnemyFactory提供的是“欲实现的abstract factory接口”，并隐式提供了一组产品。
2，OpNewFactoryUnit是个policy，规定如何实际生成对象。
3，typelist提供了“这个factory所要生成的concrete classes”的集合。这个typelist中的每一个具象型别都对应于AbstractFactory的typelist中具有相同索引的抽象型别。
例如SillyMonster（索引1）是Monster（在AbstractEnemyFactory的定义中具有相同索引）的具象型别。
ConcreteFactory应该继承OpNewFactoryUnit（它负责实作doCreate）,后者应配合typelist中的每一个型别各自产生一个具现体。在这里GenLinearHierarchy class 
template可以起很好的作用，它可以为我们管理“生成具现体”的所有细节。
AbstractEnemyFactory必须是继承体系的根源（root）。所有的doCreate()函数实作码和最终的EasyLevelEnemyFactory都必须从它派生。OpNewFactoryUnit的每一
个具现体都会改写AbstractEnemyFactory所定义的两个doCreate()纯虚函数中的一个。
GenLinearHierarchy要求OpNewFactoryUnit接受另一个参数并从它派生（GenLinearHierarchy利用这第二个template参数产生串状继承结构）。
template <typename ConcreteProduct, typename Base>
class OpNewFactoryUnit : public Base
{
	typedef typename Base::ProductList BaseProductList;

protected:
	typedef typename BaseProductList::Tail ProductList;

public:
	typedef typename BaseProductList::Head AbstractProduct;
	ConcreteProduct* doCreate(Type2Type<AbstractProduct>)
	{
		return new ConcreteProduct;
	}
};

一个Prototype-Based Abstract Factory实作品：
设计模式Prototype描述了一种“从prototype（原型对象）来生成对象”的方法。你可以通过克隆prototype来获得新对象。其要点完全在于：用来克隆的函数是虚函数。
产生多态对象的根本问题是“虚构造函数两难问题”：如果完全从无到有产生对象，就得知道待生对象的型别信息，但是多态却要求不要知道确切型别。
*/