/*
泛化仿函数（generalized functors）
泛化仿函数是一种威力强大的抽象概念，用以降低对象之间的联系。
泛化仿函数对那种“有必要将请求（requests）存储于对象内”的设计特别有用。设计模式Command用来描述“经过封装之请求”，泛化仿函数便是遵循这一模式。
泛化仿函数是“将C++所允许之任何处理请求（processing invocation）封装起来”后，获得的“具备型别安全性质”的高级对象。
1，可封装任何处理请求，因为它可接受函数指针，成员函数指针，仿函数，甚至其他泛化仿函数---连同它们的某些引数或全部引数。
2，具备型别安全性，因为它绝不会将错误的引数型别匹配到错误的函数上。
3，是一种带有“value语义”的对象，因为它充分支持拷贝，赋值和传值（pass by value）。泛化仿函数允许被任意拷贝，并且不会暴露其虚成员函数。
藉由泛化仿函数，你可以将“处理请求”存储为数值，作为参数来传递，并在远离其创建点之处调用它们。它们是函数指针的更高级版本。函数指针和泛化仿函数之间的本质
区别在于：后者可以存储状态（state），并可以调用成员函数。

Command设计模式
Command模式的目的是为了“将请求封装于对象之内”。Command对象是一个“和其实际执行者分开存储”的工件。
Command模式最重要的目的是降低系统中两个部分（请求者invoker和接收者receiver）之间的依存性。
典型的行为次序应该像下面这样：
1，应用程序（客户端）产生一个ConcreteCommand对象，并传给它足够信息以备执行某项任务。客户端会影响ConcreteCommand的状态。
2，应用程序将ConcreteCommand对象中的Command接口传给invoker（请求者），由它保存这个接口。
3，此后，一旦调用者认为执行时机已到，便启动Command的Execute()虚函数。虚拟调用机制会将这个调用动作发送给ConcreteCommand对象，由后者处理细节。ConcreteCommand
找到Receiver对象（任务执行者），并通过该对象实际进行处理（例如，调用其Action()成员函数）。另一种情况是由ConcreteCommand对象全权处理，此时不存在
Receiver概念。
invoker（请求者）可以好整以暇地调用Execute()。最重要的是执行期间你可以替换invoker所保存的Command对象，从而将各式各样的行为安插到invoker对象中。
两件事情值得注意：
1，invoker不知道工作是如何完成的。例如，使用排序算法时你也不必知道它是如何实作出来的。Command的特别之处在于，invoker甚至不知道Command对象将会
做何种处理（与此对比的是，你当然预期得到排序算法的效果）。invoker只有在某个条件成立时才会调用它所保存的Command接口中的Execute()。另一方面，receiver
也不知道其Action()成员函数是否被invoker或其他什么对象调用。因此，Command对象确保了invoker和receiver之间的重要分离关系：它们之间或许完全不互见，但
可通过Commands进行交流。
2，让我们从时序（timing）的角度来观察Command模式。在一般编程任务中，如果想执行一个动作，你得将某个对象、该对象的某个成员函数、该函数的引数组装成一个
调用。“启动这样一个调用”的时刻和“收集这个调用所需元素（对象、函数、引数）”的时刻，在概念上是无法区分的。但在Command模式中，invoker拥有调用动作的必要
元素，却将调用动作无限延期。
在Command模式中，收集“某处理动作所需环境”的时刻和执行该动作的时刻并不相同。在两个时刻之间，程序将该处理请求当作一个对象来保存和传递。如果没有这种时序
上的需要，就不会有Command模式的存在。从这个角度看，Command对象的存在归因于时序问题：由于你需要延后处理，所以你得有个对象将请求保存至那个时刻。
这些分析反映了Command模式的两个重要特点：
1，接口分离。invoker和receiver分离。
2，时间分离。Command保存了一个整装待发的处理请求，供将来运用。
ConcreteCommand对象可以将必要环境的一部分当作自身状态保存起来，并在执行Execute()期间取用其中部分消息。ConcreteCommand保存的环境越多，其独立性就
越强。
从实作角度来看，我们可以划分两类concrete Command classes。
1，仅仅将工作委托给receiver，它们所做的事情只不过是对着一个Receiver对象调用其某个成员函数。我们称此为转发式命令（forwarding commands）。
2，完成更复杂的工作，它们会调用其他对象的成员函数，也会嵌入一些单纯转发以外的逻辑。我们称此为主动式命令（active commands）。
由于“转发式命令”的行为特征很像函数指针及仿函数，所以我们称之为泛化仿函数。
我们的目标是设计一个Functor class template，用以封装任何对象、该对象的任何成员函数，以及成员函数的任何引数。一旦被执行起来，Functor将调用所有这些小部件，
形成一个函数调用。
你必须编写大量小型concrete Command classes（分别对应于应用程序的每一个动作：CmdAddUser,CmdDeleteUser,CmdModifyUser...），其中每个class都有一个
平平淡淡的Execute()成员函数，用来转调用另一对象的某个成员函数。如果使用泛型Functor class,就可以将调用转发给任何对象的任何成员函数。

真实世界中的Command
关于Command模式，一个熟知的例子是窗口环境的设计问题。
窗口系统的设计者需要一种泛化的方法，将用户的操作（例如，鼠标点击，键盘敲击）转达给应用程序。当用户点击按钮、选择功能表上的项目、或执行其他类似动作时，窗口
系统必须将这些消息通知给相应的底部程序逻辑。从窗口系统的角度来看，[Tools]选单下的[Options]命令不可包含任何特殊含义。如果它真有特殊含义，应用程序就会被
绑死在一个非常死板的框架里头。想要将窗口系统和应用程序彼此分离，一个有效办法便是使用Command对象来传递用户动作。Commands充当泛用型交通工具的角色，
将用户动作从窗口传送至应用程序逻辑上。
在这个窗口实例中，invokers是UI（使用者接口）相关元素（例如按钮、检核框、选单、窗口部件），receivers是负责回应接口命令的对象（例如某个对话框，或应用程序
本身）。
Command对象构成了UI和应用程序共同使用的“混合语言”。Command提供了双重灵活性。首先，在不改变应用程序逻辑的情况下，你可以插入新式的UI元素。这就是
所谓“可换肤（skinnable）”概念，你可以在不改变产品本身设计的情况下添加“新皮肤”。表层皮肤不含任何架构，它们只提供“Commands安插槽位”以及正确执行这些
Commands的必要知识。其次，在不同的应用程序中，你可以轻易复用相同的UI元素。

C++中的可调用体
“转发式命令”实际上是一个超强的callback，一个泛化的callback，所谓callback是一个指针，指向一个“可传递，并随时可被调用”的函数。举例：
void Foo();
void Bar();

int main()
{
	//Define a pointer to a function
	void (*pF)() = &Foo;
	Foo();																											//call Foo directly
	Bar();																											//call Bar directly

	(*pF)();																											//call Foo via pF
	void (*pF2)() = pF;																						//create a copy of pF
	pF = &Bar;																										//change pF to point to Bar
	(*pF)();																											//call Bar via pF
	(*pF2)();																										//call Foo via pF2
}
调用Foo和调用(*pF)有本质上的不同：后一种情况下你可以拷贝和修改函数指针。你可以取得一个函数指针将它保存于某处，并于适当时候调用它---这和“转发式命令”类似，
本质上是一个“和其执行者分开存储，并于日后被处理”的物体。编译器提供了一种语法上的简化：(*pF)()等同于pF()。但(*pF)()更能反映实际发生的情况---首先是pF被
提领(dereference)，然后将function call操作符(operator())施行于上。
事实上callback是很多窗口系统运用Command模式时的一种C风格做法。例如，在每个选单项和每个窗口部件(widget)中都保存这样一个callback。当用户执行某项操作
（例如点击某个窗口部件）时，该部件会调用这个callback。部件本身不知道callback实际上将做些什么。

Functor Class Template骨干
在C++中，由于拥有权（ownership）方面的原因，指向多态型别的普通指针并不严格具备高阶语义。为了解除Functor使用者对其生命周期的管理负担，Functor最好具备
“value”语义，也就是具备定义明确的拷贝和赋值操作。Functor确实拥有一份多态实作，但隐藏在内部，我们称那个实作基类（implementation base class）为FunctorImpl。
Command模式中的Command::Execute()应该成为C++中的一个使用者自定义的operator()。函数调用操作符具备“执行”或“做某事”的精确含义。一个转发式Functor
不但会委托（delegates）可调用体，它本身也是一个可调用体。这就造成“一个Functor能够拥有其他Functors”。所以从现在开始，Functor构成了可调用体集合的一部分：
这使我们将来得以用更一致的方式来看待事物。

在泛型世界中，参数的个数应该是任意的，参数的型别也应该是任意的。我们将引数个数限制在15个以下。C++不允许同名而参数个数不同的templates存在，也就是说下
面的代码是不合法的：
//Functor with no arguments
template<typename ResultType>
class Functor
{
	...
};
//Functor with one argument
template<typename ResultType,typename Parm1>
class Functor
{
	...
};
Functor的参数型别组成了一个型别集，所以TypeList用在这里非常合适，比如：
//Functor with any number and types of arguments
template<typename ResultType,typename TList>
class Functor
{
	...
};
被Functor包覆的那个多态类FunctorImpl具有和Functor一样的template参数：
template<typename R,typename TList>
class FunctorImpl;
FunctorImpl定义出一个用以将“函数调用行为”抽象化的多态接口。
template<typename R>
class FunctorImpl<R, NullType>
{
public:
	virtual R operator()() = 0;
	virtual FunctorImpl* clone() const = 0;
	virtual ~FunctorImpl() {}
};

template<typename R,typename P1>
class FunctorImpl<R, TYPELIST_1(P1)>
{
public:
	virtual R operator()(P1) = 0;
	virtual FunctorImpl* clone() const = 0;
	virtual ~FunctorImpl() {}
};

template<typename R, typename P1,typename P2>
class FunctorImpl<R, TYPELIST_2(P1,P2)>
{
public:
	virtual R operator()(P1,P2) = 0;
	virtual FunctorImpl* clone() const = 0;
	virtual ~FunctorImpl() {}
};
这些FunctorImpl classes都是原始那个FunctorImpl template的偏特化。运用偏特化技术可以让我们定义出不同版本的FunctorImpl---具体取决于typelist内的元素个数。
clone()的目的是为了产生FunctorImpl对象的一份多态拷贝，虚析构函数让我们得以在FunctorImpl指针上调用delete，摧毁FunctorImpl的派生对象。

Functor遵循典型的handle-body实现手法：
template<typename R,typename TList>
class Functor
{
public:
	Functor();
	Functor(Functor const&);
	Functor& operator=(Functor const&);
	explicit Functor(std::unique_ptr<Impl> spImpl);

	...

private:
	//Handy type definition for the body type
	typedef FunctorImpl<R, TList> Impl;
	std::unique_ptr<Impl> spImpl_;																				//指向body型别的指针
};
这里不需要明白撰写析构函数，因为智能指针会自动清除资源。
Functor还定义了一个扩展（extension）构造函数，接受一个unique_ptr指向FunctorImpl。这个构造函数允许你“定义FunctorImpl的派生类，然后通过指向那些类的
指针”，直接将Functors初始化。
为什么以unique_ptr为引数，而不采用一般指针呢？因为从外部看来，“根据unique_ptr完成构造”可向外界明白透露出“Functor取得了FunctorImpl对象拥有权”的消息。

实现“转发式”（Forwarding）Functor::operator()
Functor需要一个可以转发至FunctorImpl::operator()的operator()。我们可以使用曾经在FunctorImpl中使用过的相同手法：针对不同的参数个数提供一组相应的
Functor偏特化版本，但是此法在这里不适用，因此Functor定义了大量代码。如果仅仅因为operator()就要复制所有那些代码，将造成很大的浪费。
template<typename R,typename TList>
class Functor
{
	typedef TList ParmList;
	typedef typename TypeAtNonStrict<TList, 0, EmptyType>::Result Parm1;
	typedef typename TypeAtNonStrict<TList, 1, EmptyType>::Result Parm2;

	...as above...
};
TypeAtNonStrict是个template，用以取得typelist中某个位置上的型别。如果无所得，回传值（一个内部类TypeAtNonStrict<...>::Result）将被核定为TypeAtNonStrict
的第三个template引数。我们选择EmptyType作为第三引数，顾名思义，那是一个“在大括号之间未含任何代码”的class。总而言之，ParmN是typelist的第N个型别，但
如果typelist元素个数少于N，获得的结果将是EmptyType。
为实作出operator()，我们可以针对任意数量的参数，在Functor定义式中定义出所有版本的operator()：
template<typename R,typename TList>
class Functor
{
	...as above...

public:
	R operator()()
	{
		return (*spImpl_)();
	}

	R operator()(Parm1 p1)
	{
		return (*spImpl_)(p1);
	}

	R operator()(Parm1 p1,Parm2 p2)
	{
		return (*spImpl_)(p1,p2);
	}
};
对于某个给定的Functor具现体（instantiation），以上只有一个operator()是正确的，其他都存在编译期错误。你可以预期Functor完全没被编译出来。这是因为，每个
FunctorImpl特化版本只定义了一个operator()，而不是像Functor那样定义了一大群。这个技巧依赖一个事实：如果template的成员函数未曾被真正用上，C++不会将
它具现化。
举例：
//Define a Functor that accepts an int and a real_type and returns a real_type
Functor<real_type, TYPELIST_2(int, real_type)> myFunctor;
//Invoke it,operator()(int,real_type) is generated
real_type result = myFunctor(4, 5.6);

//Wrong invocation
real_type result = myFunctor();																								//Error
//operator()() is invalid because FunctorImpl<real_type,TYPELIST_2(int, real_type)> does not define one
所以，我们不需要针对0,1,2...个参数一一对Functor进行偏特化，拿将产生大量重复代码。我们只需定义所有operator()版本，再由编译器产生唯一被使用的那个。

处理仿函数
任何class实体只要定义有operator()，都是仿函数。
“以仿函数Fun之对象为参数”的Functor构造函数，是个“被Fun参数化”的templated构造函数：（为了用于Command模式）
template<typename R,typename TList>
class Functor
{
	...as above...

public:
	template<typename Fun>
	Functor(Fun const& fun);
};
为实作出这个构造函数，我们需要一个从FunctorImpl<R,TList>派生而来的class template FunctorHandler，其中保存了一个型别为Fun的对象，并将operator()转发
给该对象。
为避免对FunctorHandler定义太多的template参数，我让Functor具现体本身成为一个template参数。这个参数集合了其他所有参数，因为它供应内部typedef。
template<typename ParentFunctor,typename Fun>
class FunctorHandler : public FunctorImpl<typename ParentFunctor::ResultType, typename ParentFunctor::ParmList>
{
public:
	typedef typename ParentFunctor::ResultType ResultType;

	FunctorHandler(Fun const& fun) :
		fun_(fun)
	{
	}

	FunctorHandler* clone() const
	{
		return new FunctorHandler(*this);
	}

	ResultType operator()()
	{
		return fun_();
	}

	ResultType operator()(typename ParentFunctor::Parm1 p1)
	{
		return fun_(p1);
	}

	ResultType operator()(typename ParentFunctor::Parm1 p1,typename ParentFunctor::Parm2 p2)
	{
		return fun_(p1,p2);
	}

private:
	Fun fun_;
};
FunctorHandler看上去很像Functor，都将请求（requests）转发给所保存的一个成员变量。两者的主要差异在于前者系通过实值（value）而非指针来保存仿函数。这是
因为仿函数一般都得是“带有正规拷贝语义”的非多态型别。
有了FunctorHandler的声明后：
template<typename R,typename TList>
template<typename Fun>
Functor<R,TList>::Functor(Fun const& fun) :
	spImpl_(new FunctorHandler<Functor,Fun>(fun));
{
}
注意：一旦进入Functor的这个构造函数，我们就可以通过template参数Fun了解全部的型别知识。一旦离开这个构造函数，在Functor运用范围内，型别信息就丢失了，因为
Functor所知道的全部只有spImpl_，它指向基类FunctorImpl。这种“型别信息丢失现象”很有趣：构造函数知道型别，而且像工厂那样经由多态行为转换该型别。型别信息
保存在FunctorImpl指针的动态型别中。
//Define a test functor
struct TestFunctor
{
	void operator()(int i,real_type d)
	{
		cout << "TestFunctor::operator()(" << i << ", " << d << ") called." << endl;
	}
};

int main()
{
	TestFunctor f;
	Functor<void,TYPELIST_2(int,real_type)> cmd(f);
	cmd(4,4.5);
}
输出：
TestFunctor::operator()(4,4.5) called.

做一个，送一个
为什么不从“支持一般函数指针”开始呢？
走到这里，我们已经完成了对一般函数的支持。
void TestFunction(int i,real_type d)
{
	cout << "TestFunction(" << i << ", " << d << ") called." << endl;
}

int main()
{
	Functor<void,TYPELIST_2(int,real_type)> cmd(TestFunction);
	cmd(4,4.5);
}
这份意外的惊喜要归功于template引数推导。当编译器看到需要从TestFunction构造出Functor时，它别无选择地执行前述的templated构造函数。于是编译器运用template
参数void(&)(int,real_type)，亦即TestFunction的型别，将此templated构造函数具现化。这个构造函数会具现出FunctorHandler<Functor<...>,void(&)(int,real_type)>，
造成FunctorHandler中的fun_的型别也是void(&)(int,real_type)。当你调用FunctorHandler<...>::operator()时，调用会被转发至fun_()---这是“通过函数指针调用函数”
的合法语句。因此，FunctorHandler基于两个事实自然而然地支持了函数指针：1，函数指针和仿函数在语法上的相似性，2，C++的型别推导机制。
但是有一个问题，如果你重载了TestFunction（或重载传给Functor<...>的任何函数），你就得想办法消除歧义性。原因是是TestFunction被重载，其名称（符号）所代表
的型别就不再有明确定义。
void TestFunction(int);
编译器会抱怨说它无法确定该使用哪一个TestFunction重载版本，因为两个函数同名，该名称不再有识别功能。
有两种解决办法：1，使用赋值，2，使用转型：
int main()
{
	//Typedef used for convenience
	typedef void (*TpFun)(int,real_type);

	//Method 1:use an assignment
	TpFun pF = TestFunction;
	Functor<void,int,real_type> cmd1(pF);																				//OK
	cmd1(4,4.5);

	//Method 2:use a cast
	Functor<void,int,real_type> cmd2(static_cast<TpFun>(TestFunction));									//OK
	cmd2(4,4.5);
}
赋值和静态转型都可以让编译器知道：你感兴趣的实际上是“参数为int和real_type，返回型别为void”的那个TestFunction函数。

引数和返回型别的转换
我们希望这样：
//Ignore arguments
const char* TestFunction(real_type,real_type)
{
	static const char buffer[] = "Hello,world!";
	
	//It's safe to return a pointer to a static buffer
	return buffer;
}

int main()
{
	Functor<string,TYPELIST_2(int,int)> cmd(TestFunction);
	//Should print "world!"
	cout << cmd(10,10).substr(7);
}
虽然main()中所使用的与实际的TestFunction稍有不同，但还是应该能够绑定于Functor<string,TYPELIST_2(int,int)>身上。之所以会有这样的期望，因为隐式转型。
如果Functor不支持C++的这种转型，就不成其为严格的functor。

不必添加任何代码，我们就可以满足这一新要求，答案仍旧在于我们对FunctorHandler的定义方式。
让我们看看，在template具现化过程尘埃落定之后，上述实例中的代码都做了些什么。
函数：string Functor<...>::operator()(int i,int j)
会转发（转调用）至虚函数：
string FunctorHandler<...>::operator()(int i,int j)
而这个虚函数的实作码最终会调用：
return fun_(i,j);
其中fun_的型别是const char* (*)(real_type,real_type)并被核定为TestFunction。

FunctorHandler的泛用性和灵活性展示了“代码自动生成”的强大威力。
在泛型程序设计中，template引数对其相应参数的语法替换是很典型的情况。template的处理发生在编译之前，这让你得以在源码层级有所动作。面向对象程序设计与此
并无不同，它的威力来自于“名称”和“实值”的后期（编译后）绑定。因此，面向对象编程支持二进制组件形式的复用，泛型编程支持源码层级的复用。与二进制码相较，源码
天生具有更多信息和更高级别，所以泛型编程支持更丰富的构件，但其代价是较弱的执行期动态性。

处理Pointer to member function(成员函数指针)
当你向调用成员函数指针时，除了引数之外，你还必须传递一个对象。
class Parrot
{
public:
	void Eat()
	{
		cout << "Eat..." << endl;
	}

	void Speak()
	{
		cout << "Speak..." << endl;
	}
};

int main()
{
	//Define a type:pointer to a member function of Parrot
	typedef void (Parrot::*TpMemFun)();

	//Create an object of that type and initialize it with the address of Parrot::Eat
	TpMemFun pActivity = &Parrot::Eat;

	//Create a Parrot
	Parrot obj;
	//and a pointer to it
	Parrot* pObj = &obj;

	//Invoke the member function stored in Activity via an object.
	(obj.*pActivity)();

	//Same,via pointer
	(pObj->*pActivity)();

	//Change the activity
	pActivity = &Parrot::Speak;

	(obj.*pActivity)();
}
在C++中，每个对象都有型别，但operator->*和operator.*的运行结果是唯一的例外。
虽然你可以获得一般函数的reference，却无法获得成员函数的reference。

现在让我们开始为Functor提供“成员函数指针”的绑定支持
template <class ParentFunctor, typename PointerToObj,typename PointerToMemFn>
class MemFunHandler : public FunctorImpl<typename ParentFunctor::ResultType,typename ParentFunctor::ParmList>
{
public:
	typedef typename ParentFunctor::ResultType ResultType;

	MemFunHandler(const PointerToObj& pObj, PointerToMemFn pMemFn) :
	pObj_(pObj),
	pMemFn_(pMemFn)
	{
	}

	MemFunHandler* clone() const
	{
		return new MemFunHandler(*this);
	}

	ResultType operator()()
	{
		return ((*pObj_).*pMemFn_)();
	}

	ResultType operator()(typename ParentFunctor::Parm1 p1)
	{
		return ((*pObj_).*pMemFn_)(p1);
	}

	ResultType operator()(typename ParentFunctor::Parm1 p1,typename ParentFunctor::Parm2 p2)
	{
		return ((*pObj_).*pMemFn_)(p1, p2);
	}

private:
	PointerToObj pObj_;
	PointerToMemFn pMemFn_;
};
为什么将指针型别PointerToObj作为模板参数，而不是对象本身呢？
1，考虑到智能指针
2，考虑到指向const对象的指针

绑定（binding）
我们可能想将某种型别的Functor转换为另一种。绑定就是这样一种转换：假设有个Functor取两个整数作为参数，你想将其中一个整数绑定为某固定值，只让另一个变化。
绑定会产出一个“只取单一整数”的Functor，因为另一个是固定的，因而也是可知的。
让我们将Functor视为一个计算，将它的引数看作是执行该计算所需的环境。Functor可以藉由保存“函数指针”和“成员函数指针”的方式将计算过程延后，但是Functor所
保存的只是计算，并没有保存和计算相关的环境。“绑定”可以让Functor将部分环境连同计算一起保存下来，并逐步降低调用时刻所需的环境需求。
*/