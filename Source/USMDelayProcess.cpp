#include "USMDelayProcess.h"

#ifdef USM_HIERARCHICAL_COMPILE

namespace ung
{

	DelayProcess::DelayProcess(uint32 timeToDelay) :
		mTimeToDelay(timeToDelay),
		mTimeDelayedSoFar(0)
	{
	}

	DelayProcess::~DelayProcess()
	{
	}

	void DelayProcess::onUpdate(ufast deltaMS)
	{
		mTimeDelayedSoFar += deltaMS;
		if (mTimeDelayedSoFar >= mTimeToDelay)
		{
			succeed();
		}
	}
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE

/*
�÷���
//delay for 3 seconds
StrongProcessPtr pDelay(new DelayProcess(3000));
processManager.addProcess(pDelay);

//The KaboomProcess will wait for the DelayProcess.(Kaboom:��ը)
StrongProcessPtr pKaboom(new KaboomProcess());
pDelay->AttachChild(pKaboom);
*/