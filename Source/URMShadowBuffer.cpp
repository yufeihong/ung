#include "URMShadowBuffer.h"

#ifdef URM_HIERARCHICAL_COMPILE

namespace ung
{
	ShadowVertexBuffer::ShadowVertexBuffer(uint32 vertexSize, uint32 numVertices)
	{
		mSizeInBytes = vertexSize * numVertices;

		mData = ungMallocAlignedBuffer(mSizeInBytes);
	}

	ShadowVertexBuffer::~ShadowVertexBuffer()
	{
		ungFreeAlignedBuffer(mData);
	}

	void* ShadowVertexBuffer::lock(LockOptions options)
	{
		return lock(0,mSizeInBytes,options);
	}

	void* ShadowVertexBuffer::lock(uint32 offset, uint32 length, LockOptions options)
	{
		return lockImpl(offset,length,options);
	}

	void ShadowVertexBuffer::unlock()
	{
		unlockImpl();
	}

	void ShadowVertexBuffer::readData(uint32 offset, uint32 length, void* pDest)
	{
		BOOST_ASSERT((offset + length) <= mSizeInBytes);

		memcpy(pDest,mData + offset,length);
	}

	void ShadowVertexBuffer::writeData(uint32 offset, uint32 length, const void* pSource, bool discardWholeBuffer)
	{
		BOOST_ASSERT((offset + length) <= mSizeInBytes);

		memcpy(mData + offset, pSource, length);
	}

	void * ShadowVertexBuffer::lockImpl(uint32 offset, uint32 length, LockOptions options)
	{
		void* ret{ nullptr };

		//是否越界
		if ((offset + length) > mSizeInBytes)
		{
			UNG_EXCEPTION("Lock request out of bounds.");
		}

		mIsLocked = true;

		return mData + offset;
	}

	void ShadowVertexBuffer::unlockImpl()
	{
		mIsLocked = false;
	}

	//-------------------------------------------------------------------------------------------------

	ShadowIndexBuffer::ShadowIndexBuffer(uint32 indexSize, uint32 numIndices)
	{
		mSizeInBytes = indexSize * numIndices;

		mData = ungMallocAlignedBuffer(mSizeInBytes);
	}

	ShadowIndexBuffer::~ShadowIndexBuffer()
	{
		ungFreeAlignedBuffer(mData);
	}

	void * ShadowIndexBuffer::lock(LockOptions options)
	{
		return lock(0,mSizeInBytes,options);
	}

	void * ShadowIndexBuffer::lock(uint32 offset, uint32 length, LockOptions options)
	{
		return lockImpl(offset,length,options);
	}

	void ShadowIndexBuffer::unlock()
	{
		unlockImpl();
	}

	void ShadowIndexBuffer::readData(uint32 offset, uint32 length, void * pDest)
	{
		BOOST_ASSERT((offset + length) <= mSizeInBytes);

		memcpy(pDest, mData + offset, length);
	}

	void ShadowIndexBuffer::writeData(uint32 offset, uint32 length, const void * pSource, bool discardWholeBuffer)
	{
		BOOST_ASSERT((offset + length) <= mSizeInBytes);

		memcpy(mData + offset, pSource, length);
	}

	void* ShadowIndexBuffer::lockImpl(uint32 offset, uint32 length, LockOptions options)
	{
		void* ret{ nullptr };

		//是否越界
		if ((offset + length) > mSizeInBytes)
		{
			UNG_EXCEPTION("Lock request out of bounds.");
		}

		mIsLocked = true;

		return mData + offset;
	}

	void ShadowIndexBuffer::unlockImpl()
	{
		mIsLocked = false;
	}
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE