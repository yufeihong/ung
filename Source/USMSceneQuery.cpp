#include "USMSceneQuery.h"

#ifdef USM_HIERARCHICAL_COMPILE

namespace ung
{
	SceneQuery::SceneQuery(ISpatialManager* spatialManager) :
		mSpatialManager(spatialManager)
	{
	}

	SceneQuery::~SceneQuery()
	{
	}

	void SceneQuery::setInstanceMask(uint32 mask)
	{
		mInstanceMask = mask;
	}

	uint32 SceneQuery::getInstanceMask()
	{
		return mInstanceMask;
	}

	void SceneQuery::setTypeMask(uint32 mask)
	{
		mTypeMask = mask;
	}

	uint32 SceneQuery::getTypeMask()
	{
		return mTypeMask;
	}

	//--------------------------------------------------------------

	SingleSceneQueryResults::SingleSceneQueryResults() :
		mDistance(0.0)
	{
	}

	WeakIObjectPtr SingleSceneQueryResults::getSingleObject() const
	{
		return mObject;
	}

	real_type SingleSceneQueryResults::getRayDistance() const
	{
		return mDistance;
	}

	void SingleSceneQueryResults::clearResults()
	{
		mObject.reset();
		mDistance = 0.0;
	}

	bool SingleSceneQueryResults::operator<(SingleSceneQueryResults const& other)
	{
		return mDistance < other.mDistance;
	}

	SingleSceneQuery::SingleSceneQuery(ISpatialManager* spatialManager) :
		SceneQuery(spatialManager),
		mResults(UNG_NEW SingleSceneQueryResults)
	{
	}

	SingleSceneQuery::~SingleSceneQuery()
	{
		UNG_DEL(mResults);
	}

	ISceneQueryResults * SingleSceneQuery::getResults()
	{
		return mResults;
	}

	void SingleSceneQuery::clearResults()
	{
		mResults->clearResults();
	}

	//--------------------------------------------------------------

	void RangeSceneQueryResults::getRangeObjects(STL_LIST(WeakIObjectPtr)& objects) const
	{
		objects = mObjects;
	}

	void RangeSceneQueryResults::clearResults()
	{
		mObjects.clear();
	}

	RangeSceneQuery::RangeSceneQuery(ISpatialManager* spatialManager) :
		SceneQuery(spatialManager),
		mResults(UNG_NEW RangeSceneQueryResults)
	{
	}

	RangeSceneQuery::~RangeSceneQuery()
	{
		UNG_DEL(mResults);
	}

	ISceneQueryResults* RangeSceneQuery::getResults()
	{
		return mResults;
	}

	void RangeSceneQuery::clearResults()
	{
		mResults->clearResults();
	}
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE