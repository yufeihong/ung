#include "UFCDLL.h"

#ifdef UFC_HIERARCHICAL_COMPILE

#include "UFCLogManager.h"

/*
可以使用两种方法来编写Windows程序：MFC和SDK。
*/
#ifndef WIN32_LEAN_AND_MEAN
#if UNG_PLATFORM == UNG_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN																//不使用MFC
#if !defined(NOMINMAX) && defined(_MSC_VER)
#define NOMINMAX																					//required to stop windows.h messing up std::min
#endif
#include <windows.h>
#endif
#endif

namespace ung
{
	DLL::DLL(const String& name) :
		mName(name),
		mHInstance(nullptr)
	{
	}

	DLL::~DLL()
	{
	}

	void DLL::load()
	{
		UNG_LOG("Loading library " + mName);

		String name = mName;

		mHInstance = (DLL_HANDLE)DLL_LOAD(name.c_str());
		BOOST_ASSERT(mHInstance && "Could not load dynamic library.");
	}

	void DLL::unload()
	{
		UNG_LOG("Unloading library " + mName);

		if (DLL_UNLOAD(mHInstance))
		{
			BOOST_ASSERT(0 && "Could not unload dynamic library.");
		}
	}

	const String& DLL::getName() const
	{
		return mName;
	}

	void* DLL::getSymbol(const String& strName) const throw()
	{
		return (void*)DLL_GETSYM(mHInstance, strName.c_str());
	}

	String DLL::getLastLoadingError()
	{
#if UNG_PLATFORM == UNG_PLATFORM_WIN32
		LPVOID lpMsgBuf;
		FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			nullptr,
			GetLastError(),
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPTSTR)&lpMsgBuf,
			0,
			nullptr);
		String ret = (char*)lpMsgBuf;
		LocalFree(lpMsgBuf);
		return ret;
#elif UNG_PLATFORM == UNG_PLATFORM_LINUX || UNG_PLATFORM == UNG_PLATFORM_APPLE
		return String(dlerror());
#else
		return String("");
#endif
	}
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE