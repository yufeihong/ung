#include "USMManualGeometry.h"

#ifdef USM_HIERARCHICAL_COMPILE

namespace ung
{
	ManualGeometry::ManualGeometry()
	{
	}

	ManualGeometry::~ManualGeometry()
	{
	}

	//---------------------------------------------------------------------------------------------

	ManualTriangle::ManualTriangle(Vector3 const & v0, Vector3 const & v1, Vector3 const & v2)
	{
		mVertices.at(0) = v0;
		mVertices.at(1) = v1;
		mVertices.at(2) = v2;

		mIndices = { 0,1,2 };

		mFaceNormal = (mVertices.at(0) - mVertices.at(2)).crossProductUnit(mVertices.at(1) - mVertices.at(2));
		mFaceNormal.normalize();
	}

	ManualTriangle::~ManualTriangle()
	{
	}

	Vector3 const* ManualTriangle::getPositions() const
	{
		return mVertices.data();
	}

	uint32 const* ManualTriangle::getIndices() const
	{
		return mIndices.data();
	}

	uint32 ManualTriangle::getVerticesCount() const
	{
		return 3;
	}

	uint32 ManualTriangle::getIndicesCount() const
	{
		return 3;
	}

	uint32 ManualTriangle::getTrianglesCount() const
	{
		return 1;
	}

	Vector3 const & ManualTriangle::getFaceNormal() const
	{
		return mFaceNormal;
	}

	//---------------------------------------------------------------------------------------------

	ManualBox::ManualBox(Vector3 const & halfSize)
	{
		real_type halfWidth = halfSize.x;
		real_type halfHeight = halfSize.y;
		real_type halfDepth = halfSize.z;

		mVertices = {
			//顶部四个顶点，逆时针排序
			Vector3(halfWidth,halfHeight,halfDepth),						//0
			Vector3(halfWidth,halfHeight,-halfDepth),						//1
			Vector3(-halfWidth,halfHeight,-halfDepth),					//2
			Vector3(-halfWidth,halfHeight,halfDepth),						//3
			//底部四个顶点，顺时针排序(从外表面观察)
			Vector3(halfWidth,-halfHeight,halfDepth),						//4
			Vector3(halfWidth,-halfHeight,-halfDepth),					//5
			Vector3(-halfWidth,-halfHeight,-halfDepth),					//6
			Vector3(-halfWidth,-halfHeight,	halfDepth) };				//7

		mIndices = {
			//顶部
			0,1,2,
			2,3,0,
			//前面
			3,7,4,
			4,0,3,
			//底部
			7,6,5,
			5,4,7,
			//后面
			1,5,6,
			6,2,1,
			//左侧
			2,6,7,
			7,3,2,
			//右侧
			0,4,5,
			5,1,0};
	}

	ManualBox::~ManualBox()
	{
	}

	Vector3 const * ManualBox::getPositions() const
	{
		return mVertices.data();
	}

	uint32 const * ManualBox::getIndices() const
	{
		return mIndices.data();
	}

	uint32 ManualBox::getVerticesCount() const
	{
		return 8;
	}

	uint32 ManualBox::getIndicesCount() const
	{
		return 36;
	}

	uint32 ManualBox::getTrianglesCount() const
	{
		return 12;
	}
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE