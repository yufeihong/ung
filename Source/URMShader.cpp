#include "URMShader.h"

#ifdef URM_HIERARCHICAL_COMPILE

namespace ung
{
	Shader::Shader(String const & fullName,ShaderType type) :
		mFullName(fullName),
		mType(type),
		mParser(UNG_NEW Parser(fullName))
	{
		mShaderName = mParser->getFileName();
	}

	Shader::~Shader()
	{
	}

	ShaderType Shader::getType() const
	{
		return mType;
	}

	String const& Shader::getFullName() const
	{
		return mFullName;
	}

	String const& Shader::getShaderName() const
	{
		return mShaderName;
	}
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE