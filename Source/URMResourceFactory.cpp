#include "URMResourceFactory.h"

#ifdef URM_HIERARCHICAL_COMPILE

#include "URMResourcePool.h"
#include "URMResource.h"

namespace ung
{
	ResourceFactory::ResourceFactory()
	{
	}

	ResourceFactory::~ResourceFactory()
	{
		mCreators.clear();
	}

	std::shared_ptr<Resource> ResourceFactory::createResource(String const& resourceType,String const& resourceFileName)
	{
		String fullName = fixFullName(resourceType,resourceFileName);

		std::shared_ptr<Resource> retPtr = nullptr;

		//在池中找
		retPtr = ResourcePool::getInstance().take(fullName);
		if (retPtr)
		{
			return retPtr;
		}
		
		//当前池中没有找到
		auto findRet = mCreators.find(resourceType);
		if (findRet != mCreators.end())
		{
			//调用回调函数来构造具体资源对象
			retPtr = (findRet->second)(fullName);

			//放入池中
			ResourcePool::getInstance().put(fullName,retPtr);
		}

		return retPtr;
	}

	void ResourceFactory::registerCreator(String const& resourceType, creatorType creator)
	{
		BOOST_ASSERT(!resourceType.empty());

		mCreators[resourceType] = creator;
	}

	void ResourceFactory::unregisterCreator(String const& resourceType)
	{
		BOOST_ASSERT(!resourceType.empty());

		auto findRet = mCreators.find(resourceType);
		if (findRet != mCreators.end())
		{
			auto ret = mCreators.erase(resourceType);
			BOOST_ASSERT(ret == 1);
		}
	}

	const ung::String ResourceFactory::fixFullName(String const& resourceType, String const& resourceFileName)
	{
		BOOST_ASSERT(!resourceType.empty());
		BOOST_ASSERT(!resourceFileName.empty());

		static const String imagePath{ "../../../../Resource/Media/Materials/Textures/" };
		static const String materialPath{ "../../../../Resource/Media/Materials/Properties/" };
		static const String meshPath{ "../../../../Resource/Media/Models/" };

		String fullName;

		if (resourceType == String("Image"))
		{
			fullName = imagePath + resourceFileName;
		}
		else if (resourceType == String("Material"))
		{
			fullName = materialPath + resourceFileName;
		}
		else if (resourceType == String("Mesh"))
		{
			fullName = meshPath + resourceFileName;
		}

		//检查
#if UNG_DEBUGMODE
		//检查full name是否为空
		BOOST_ASSERT(!fullName.empty() && "full name is empty.");

		//转换为C字符串
		const char* fileFullNameCStr = fullName.c_str();

		//检查是否包含文件名
		auto hasFileName = Filesystem::getInstance().hasFileName(fileFullNameCStr);
		BOOST_ASSERT(hasFileName && "does not include file name.");

		//检查是否存在
		auto isExists = Filesystem::getInstance().isExists(fileFullNameCStr);
		BOOST_ASSERT(isExists && "does not exists.");

		//检查是否为一个普通文件
		auto isRegularFile = Filesystem::getInstance().isRegularFile(fileFullNameCStr);
		BOOST_ASSERT(isRegularFile && "not a file.");

		//检查是否有扩展名
		auto hasExtension = Filesystem::getInstance().hasExtension(fileFullNameCStr);
		BOOST_ASSERT(hasExtension && "does not have extension.");
#endif//UNG_DEBUGMODE

		return fullName;
	}
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE