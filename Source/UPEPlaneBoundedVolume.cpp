#include "UPEPlaneBoundedVolume.h"

#ifdef UPE_HIERARCHICAL_COMPILE

#include "UPEPlane.h"
#include "UPEAABB.h"
#include "UPESphere.h"

namespace ung
{
	PlaneBoundedVolume::PlaneBoundedVolume()
	{
	}

	PlaneBoundedVolume::~PlaneBoundedVolume()
	{
	}

	void PlaneBoundedVolume::addPlane(std::shared_ptr<Plane> planePtr)
	{
		mPlanes.push_back(planePtr);
	}

	bool PlaneBoundedVolume::contain(Sphere const & sphere)
	{
		//auto beg = mPlanes.begin();
		//auto end = mPlanes.end();

		//while (beg != end)
		//{
		//	auto plane = *beg;
		//	if (plane->getSide(sphere.getCenter(),sphere.getRadius()) == Plane::side_negative)
		//	{
		//		return false;
		//	}

		//	++beg;
		//}

		return true;
	}

	std::pair<PlaneBoundedVolume::planes_type::iterator, PlaneBoundedVolume::planes_type::iterator> PlaneBoundedVolume::getIterator()
	{
		return std::pair<planes_type::iterator, planes_type::iterator>(std::begin(mPlanes),std::end(mPlanes));
	}

	std::pair<PlaneBoundedVolume::planes_type::const_iterator, PlaneBoundedVolume::planes_type::const_iterator> PlaneBoundedVolume::getIterator() const
	{
		return std::pair<planes_type::const_iterator, planes_type::const_iterator>(std::cbegin(mPlanes),std::cend(mPlanes));
	}
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE