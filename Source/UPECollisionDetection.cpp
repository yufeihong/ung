#include "UPECollisionDetection.h"

#ifdef UPE_HIERARCHICAL_COMPILE

#include "UPELineSegment.h"
#include "UPERay.h"
#include "UPEPlane.h"
#include "UPESphere.h"
#include "UPEAABB.h"
#include "UPETriangle.h"
#include "UPEQuadrilateral.h"
#include "UPECapsule.h"
#include "UPELozenge.h"
#include "UPECone.h"
#include "UPECylinder.h"
#include "UPEPlaneBoundedVolume.h"
#include "UPEUtilities.h"

#include <limits>
#include <utility>

namespace ung
{
	bool CollisionDetection::intersects(Vector3 const & p1, Vector3 const & q1, Vector3 const & p2, Vector3 const & q2,Vector3& out)
	{
		Vector3 out1{}, out2{};
		ungClosestPointBetweenTwoStraightLines(p1,q1,p2,q2,out1,out2);

		auto v = out2 - out1;
		auto vLen = v.squaredLength();

		if (Math::isZero(vLen))
		{
			out = out1;

			return true;
		}

		return false;
	}

	bool CollisionDetection::intersects(Vector3 const & p, Vector3 const & q, Triangle const & triangle, real_type & u, real_type & v, real_type & w)
	{
		/*
		Instead of explicitly computing R(交点R) for use in the sidedness test(三角形为逆时针环绕时，
		交点R始终位于各边ab,bc,ca的左边), the test can be performed directly with the line PQ 
		against the triangle edges.Consider the scalar triple products:
		u = [PQ PC PB]
		v = [PQ PA PC]
		w = [PQ PB PA]
		当三角形成逆时针环绕方向时，当直线PQ位于边BC，CA和AB的左侧时，u >= 0,v >= 0,w >= 0分
		别返回真值。
		若标量三重积具备相同的符号(此处忽略0值)，则PQ穿越三角形内部。
		在计算交点时，u，v，w分别正比于u*,v*,和w*：
		u* = ku = [PR PC PB]
		v* = kv = [PR PA PC]
		w* = kw = [PR PB PA]
		其中k = |PR| / |PQ|
		这里u*,v*,和w*正比于四面体RBCP,RCAP,和 RABP的体积。由于四面体具有相同的高度值，因而体积值
		分别正比于底面三角形RBC,RCA和RAB。
		*/

		auto vertices = triangle.getVertices();
		auto a = vertices[0];
		auto b = vertices[1];
		auto c = vertices[2];

		auto pq = q - p;
		auto pa = a - p;
		auto pb = b - p;
		auto pc = c - p;

		/*
		Test if pq is inside the edges bc, ca and ab.
		Done by testing that the signed tetrahedral(四面体) volumes, computed using scalar triple 
		products(标量三重积),are all positive.
		这里计算标量三重积可以进行优化(共用某些叉积或点积值)
		*/
		u = ungScalarTripleProduct(pq, pc, pb);
		if (u < 0.0)
		{
			return false;
		}
		v = ungScalarTripleProduct(pq, pa, pc);
		if (v < 0.0)
		{
			return false;
		}
		w = ungScalarTripleProduct(pq, pb, pa);
		if (w < 0.0)
		{
			return false;
		}

		//当u = v = w = 0.0时，直线PQ位于三角形的平面内部
		if (Math::isZero(u) && Math::isZero(v) && Math::isZero(w))
		{
			return true;
		}

		/*
		Compute the barycentric coordinates (u, v, w) determining the intersection point r:
		r = u * a + v * b + w * c
		*/
		auto denom = 1.0 / (u + v + w);
		u *= denom;
		v *= denom;
		w *= denom;
		BOOST_ASSERT(Math::isEqual(w,(1.0 - u - v)));

		/*
		当直线PQ与三角形ABC相交时，可以通过显式计算直线L(t) = P + t(Q - P)与ABC平面的相交t值，
		并且丢弃0 <= t <= 1之外的相交状态,从而得到线段与三角形的相交测试。
		*/

		return true;
	}

	bool CollisionDetection::intersects(Vector3 const & p, Vector3 const & q, Quadrilateral const & quad, Vector3 & out)
	{
		/*
		若一点位于ABCD内部，则必定位于三角形ABC或三角形DAC内部。
		边CA为两个三角形的公共边，对该边首先进行测试可以有效地确定交点R所处的三角形区域。
		例如，如果交点R位于边CA的左方，则R必定处于三角形DAC的外部，因此，只需针对三角形ABC加以
		测试并计算其中的两条剩余边。
		无论交点R位于边CA的哪一侧，都只需测试3条边。
		*/

		auto vertices = quad.getVertices();
		auto a = vertices[0];
		auto b = vertices[1];
		auto c = vertices[2];
		auto d = vertices[3];

		auto pq = q - p;
		auto pa = a - p;
		auto pb = b - p;
		auto pc = c - p;

		//Determine which triangle to test against by testing against diagonal(对角线) first.
		auto m = pc.crossProduct(pq);
		//ScalarTriple(pq, pa, pc);
		auto v = pa.dotProduct(m);

		if (v >= 0.0)
		{
			//Test intersection against triangle abc
			//ScalarTriple(pq, pc, pb);
			auto u = -(pb.dotProduct(m));
			if (u < 0.0)
			{
				return false;
			}
			auto w = ungScalarTripleProduct(pq, pb, pa);
			if (w < 0.0)
			{
				return false;
			}

			//Compute r, r = u * a + v * b + w * c,from barycentric coordinates (u, v, w)
			auto denom = 1.0 / (u + v + w);
			u *= denom;
			v *= denom;
			w *= denom;
			BOOST_ASSERT(Math::isEqual(w,1.0 - u - v));

			out = u * a + v * b + w * c;
		}
		else
		{
			//Test intersection against triangle dac
			auto pd = d - p;
			//ScalarTriple(pq, pd, pc);
			auto u = pd.dotProduct(m);
			if (u < 0.0)
			{
				return false;
			}
			auto w = ungScalarTripleProduct(pq, pa, pd);
			if (w < 0.0)
			{
				return false;
			}
			v = -v;

			//Compute r,r = u * a + v * d + w * c,from barycentric coordinates (u, v, w)
			auto denom = 1.0 / (u + v + w);
			u *= denom;
			v *= denom;
			w *= denom;
			BOOST_ASSERT(Math::isEqual(w,1.0 - u - v));
			out = u * a + v * d + w * c;
		}

		return true;
	}

	bool CollisionDetection::intersects(Vector2 const & start1, Vector2 const & end1, Vector2 const & start2, Vector2 const & end2,Vector2& out)
	{
		/*
		两线段相交仅当两端点彼此位于线段的两侧。
		例如，若线段AB和CD相交，端点A，B一定位于线段CD的两侧。端点C，D也一定位于线段AB的两侧。
		*/

		/*
		Returns 2 times the signed triangle area.
		The result is positive if abc is ccw, negative if abc is cw, zero if abc is degenerate(退化).
		*/
		auto signed2DTriangleArea = [](Vector2 const& a, Vector2 const& b, Vector2 const& c)
		{
			return (a.x - c.x) * (b.y - c.y) - (a.y - c.y) * (b.x - c.x);
		};

		auto a = start1;
		auto b = end1;
		auto c = start2;
		auto d = end2;

		//Sign of areas correspond to which side of ab points c and d are
		auto a1 = signed2DTriangleArea(a, b, d);			//Compute winding of abd (+ or -)
		auto a2 = signed2DTriangleArea(a, b, c);			//To intersect, must have sign opposite of a1
		
		//If c and d are on different sides of ab, areas have different signs
		if (a1 * a2 < 0.0)
		{
			//Compute signs for a and b with respect to segment cd
			auto a3 = signed2DTriangleArea(c, d, a);		//Compute winding of cda (+ or -)
			/*
			Since area is constant a1 - a2 = a3 - a4, or a4 = a3 + a2 - a1
			auto a4 = signed2DTriangleArea(c, d, b);	//Must have opposite sign of a3
			*/
			auto a4 = a3 + a2 - a1;

			//Points a and b on different sides of cd if areas have different signs
			if (a3 * a4 < 0.0)
			{
				/*
				线段相交了。
				通过解方程:
				a + t(b - a) = c + t(d - c)
				来计算交点
				t = (c - a) / (-a + b + c - d);
				但是这种方法，有可能会产生分母或分母的某个分量为0.0的情况。
				*/

				//面积的比值可以代表质心坐标的比值
				auto t = a3 / (a3 - a4);
				out = a + t * (b - a);

				return true;
			}
		}

		//Segments not intersecting (or collinear)
		return false;
	}

	bool CollisionDetection::intersects(LineSegment const & lineSegment, Plane const & plane, Vector3 & out)
	{
		/*
		(n · X) = d
		S(t) = A + t (B - A) for 0 ≤ t ≤ 1
		(n · (A + t (B - A))) = d				(利用S(t) = A + t (B - A)替换(n · X) = d)
		n · A + t n · (B - A) = d				(点积扩展)
		t n · (B - A) = d - n · A
		t = (d - n · A) / (n · (B - a))
		将t值带入线段的参数化方程，并计算得到交点Q:
		Q = A + [(d - n · A)(n · (B - A))](B - A)
		*/

		auto a = lineSegment.getStart();
		auto b = lineSegment.getEnd();
		auto d = plane.getD();
		auto n = plane.getN();

		auto ab = b - a;

		/*
		相交时t值为：
		(d - n · A) / (n · (B - a))
		其中(n · (B - a)为判别式。
		若判别式为负值，则平面面向线段。
		若判别式为正值，则平面背向线段。
		若判别式为0，则二者平行。
		*/
		auto denominator = n.dotProduct(ab);
		if (Math::isZero(denominator))
		{
			return false;
		}

		auto t = (d - n.dotProduct(a)) / denominator;

		//If t in [0,1] compute and return intersection point
		if (t >= 0.0 && t <= 1.0)
		{
			out = a + t * ab;

			return true;
		}

		return false;
	}

	bool CollisionDetection::intersects(LineSegment const & lineSegment, Triangle const & triangle, real_type & u, real_type & v, real_type & w)
	{
		/*
		三角形：
		T(v, w) = A + v(B - A) + w(C - A),with u = 1 - v - w.
		T is inside ABC if v ≥ 0,w ≥ 0, and v + w ≤ 1.
		线段：
		R(t) = P + t(Q - P), 0 ≤ t ≤ 1
		令T(v, w) = R(t)，可以求解到,t,v,w，然后确定这些变量是否位于相交状态所需的范围内。

		T(v, w) = R(t)
		A + v(B - A) + w(C - A) = P + t(Q - P)
		(P - Q)t + (B - A)v + (C - A)w = P - A
		构造出一个线性方程组，利用克莱姆法则来求解t,v,w
		t = det[(P - A) (B - A) (C - A)] / det[(P - Q) (B - A) (C - A)]
		v = det[(P - Q) (P - A) (C - A)] / det[(P - Q) (B - A) (C - A)]
		w = det[(P - Q) (B - A) (P - A)] / det[(P - Q) (B - A) (C - A)]
		由于det[a b c] = a · (b × c)，所以上式可以化简为：
		t = (P - A) · n / d
		v = (C - A) · e / d
		w = -(B - A) · e / d
		其中：
		n = (B - A) × (C - A)
		d = (P - Q) · n
		e = (P - Q) × (P - A)

		注意：
		若d < 0，则线段背离指向该三角形。
		若d = 0，则线段平行于该三角形平面。
		*/

		auto p = lineSegment.getStart();
		auto q = lineSegment.getEnd();

		auto vertices = triangle.getVertices();
		auto a = vertices[0];
		auto b = vertices[1];
		auto c = vertices[2];

		auto ab = b - a;
		auto ac = c - a;
		auto qp = p - q;
		auto ap = p - a;

		//三角形法线
		auto n = ab.crossProduct(ac);

		/*
		Compute denominator d.
		If d <= 0, segment is parallel to or points away from triangle, so exit early.
		*/
		auto d = qp.dotProduct(n);
		if (d <= 0.0)
		{
			return false;
		}

		/*
		Compute intersection t value of pq with plane of triangle.
		A ray intersects if 0 <= t.
		Segment intersects if 0 <= t <= 1.
		Delay dividing by d until intersection has been found to pierce triangle.
		*/
		auto t = ap.dotProduct(n);
		if (t < 0.0)
		{
			return false;
		}
		//For segment.Exclude this for a ray test.
		if (t > d)						//t > d,因为没有除d
		{
			return false;
		}

		//Compute barycentric coordinate components and test if within bounds
		auto e = qp.crossProduct(ap);
		v = ac.dotProduct(e);
		if (v < 0.0 || v > d)
		{
			return false;
		}
		w = -(ab.dotProduct(e));
		if (w < 0.0 || v + w > d)
		{
			return false;
		}

		/*
		Segment/ray intersects triangle.
		Perform delayed division and compute the last barycentric coordinate component.
		*/
		auto ood = 1.0 / d;
		//t:可以用于确定线段上的交点
		t *= ood;
		//u,v,w:用于确定三角形上的交点
		v *= ood;
		w *= ood;
		u = 1.0 - v - w;

#if UNG_DEBUGMODE
		//验证：线段上的交点等于三角形上的交点
		auto slIsec = p + t * (q - p);
		auto triIsce = u * a + v * b + w * c;
		BOOST_ASSERT(slIsec == triIsce);
#endif

		return true;
	}

	bool CollisionDetection::intersects(LineSegment const & lineSegment, Sphere const & sphere, Vector3 & out)
	{
		/*
		代码同射线与球体的相交检测。
		令p = start,d = (end - start) / |end - start|
		出现相交时，需要确认t是否 <= |end - start|，从而保证交点位于线段之内
		*/

		auto p = lineSegment.getStart();
		auto d = lineSegment.getDir();
		//这里d必须为单位向量
		BOOST_ASSERT(d.isUnitLength());
		auto center = sphere.getCenter();
		auto radius = sphere.getRadius();
		
		auto m = p - center;
		float b = m.dotProduct(d);
		float c = m.dotProduct(m) - radius * radius;

		if (c > 0.0 && b > 0.0)
		{
			return false;
		}

		float discr = b * b - c;

		if (discr < 0.0)
		{
			return false;
		}

		auto t = -b - Math::calSqrt(discr);

		if (t < 0.0)
		{
			t = 0.0;
		}

		/*
		如果t > |end - start|，那么说明交点位于线段之外。
		这里t的合法范围是：[0.0,lineSegmentLength]，因为方向向量为单位向量。
		*/
		if (t * t > lineSegment.getSquaredLength())
		{
			return false;
		}

		out = p + t * d;

		return true;
	}

	bool CollisionDetection::intersects(LineSegment const & lineSegment, PlaneBoundedVolume const & planeBV, Vector3 & entry, Vector3 & exit)
	{
		/*
		可以将凸多面体看做半空间集合的交集。
		针对每一个半空间对线段实施剪裁处理。
		经过剪裁处理后，查看剩余线段的长度。

		相交时t值为：
		(d - n · A) / (n · (B - a))
		其中(n · (B - a)为判别式。
		若判别式为负值，则平面面向线段。
		若判别式为正值，则平面背向线段。
		若判别式为0，则二者平行。
		*/

		auto a = lineSegment.getStart();
		auto b = lineSegment.getEnd();

		auto iterator = planeBV.getIterator();
		auto beg = iterator.first;
		auto end = iterator.second;

		//Compute direction vector for the segment
		auto d = b - a;

		/*
		Set initial interval to being the whole segment.
		For a ray,tLast should be set to +FLT_MAX.
		For a line,tFirst should be set to –FLT_MAX
		*/
		auto tFirst = 0.0;
		auto tLast = 1.0;

		//Intersect segment against each plane
		while (beg != end)
		{
			auto plane = *beg;
			auto pN = plane->getN();
			auto pD = plane->getD();

			auto denom = pN.dotProduct(d);
			auto dist = pD - pN.dotProduct(a);

			//Test if segment runs parallel to the plane
			if (Math::isZero(denom))
			{
				//If so, return “no intersection” if segment lies outside plane
				if (dist > 0.0)
				{
					return false;
				}
			}
			else
			{
				//Compute parameterized t value for intersection with current plane
				auto t = dist / denom;

				//平面面向线段
				if (denom < 0.0)
				{
					//When entering halfspace,update tFirst if t is larger
					if (t > tFirst)
					{
						tFirst = t;
					}
				}
				//平面背向线段
				else
				{
					//When exiting halfspace,update tLast if t is smaller
					if (t < tLast)
					{
						tLast = t;
					}
				}

				/*
				如果线段要和凸多面体发生碰撞，那么线段必然会先和某个平面的正面相交。如果先和某个平面的
				背面相交了，那么线段也就不会和凸多面体相交了，线段会直接离多面体而去。这就是为什么这里
				的判断没有放在循环体外面来进行检测的原因了。
				Exit with “no intersection” if intersection becomes empty.
				*/
				if (tFirst > tLast)
				{
					return false;
				}
			}

			++beg;
		}

		//A nonzero logical intersection, so the segment intersects the polyhedron
		entry = a + tFirst * d;
		exit = a + tLast * d;

		return true;
	}

	bool CollisionDetection::intersects(LineSegment const & lineSegment, Cylinder const & cylinder, Vector3 & out)
	{
		/*
		若X为圆柱体表面上一点，则需要满足下列隐式方程：
		(v - w) · (v - w) - r ^ 2 = 0, where v = X - P, d = Q - P, and w = ((v · d) / (d · d))d
		上述方程表明，若从P至X的向量v中减去w(v中平行于d的分量)，则针对X的结果向量，其长度必定等于
		r且位于圆柱体表面上。
		对于由顶点A，B定义的直线L(t) = A + t(B - A)，其与圆柱体的交点可将L(t)代入上式中的X项并求解
		t计算得到。同时，将v = L(t) - P = (A - P) + t(B -A)记为v = m + tn,即m = A - P,n = B - A
		经过计算，上述方程转化为：
		(n · n - ((n · d) ^ 2) / (d · d))(t ^ 2) + 2(m · n - (n · d)(m · d) / d · d)t + 
		m · m - ((m · d) ^ 2) / d · d - (r ^ 2) = 0
		该式已推导验证，其中二次项系数为：
		(n · n - ((n · d) ^ 2) / (d · d))
		一次项系数为：
		2(m · n - (n · d)(m · d) / d · d)
		常数项为：
		m · m - ((m · d) ^ 2) / d · d - (r ^ 2)
		令d为单位向量，那么：
		((d · d)(n · n) - ((n · d) ^ 2)(t ^ 2) + 2((d · d)(m · n) - (n · d)(m · d)t + 
		(d · d)((m · m) - (r ^ 2)) - ((m · d) ^ 2) = 0
		当d为单位向量时，圆柱体的方程可以表示为：
		(v × d) · (v × d) = r ^ 2，解释该表示法：
		v = X - P, d = Q - P
		v x d为垂直于圆柱体中心线的带(X - P)垂直分量长度的向量，因为d为单位向量。所以这个垂直分量的
		长度就是半径r。
		关于t的二次方程，可以表示为：
		a(t ^ 2) + 2bt + c = 0
		其中：
		a = (d · d)(n · n) - ((n · d) ^ 2)
		b = (d · d)(m · n) - (n · d)(m · d)
		c = (d · d)((m · m) - (r ^ 2)) - ((m · d) ^ 2)
		基于拉格朗日恒等式:(a×b)·(c×d) = (a·c)(b·d) - (a·d)(b·c)，上述项可以写为：
		a = (d × n) · (d × n)
		b = (d × m) · (d × n)
		c = (d × m) · (d × m) - (r ^ 2)(d · d)
		上面的a,b,c已经推导验证。
		上式表明，若a = 0，则d与n平行(两个向量平行，其叉积为0)
		c的符号，常用于指出顶点A位于圆柱体表面的内部(c < 0)还是外部(c > 0)
		这里求解t的方式为：
		t = (-b ± √(b ^ 2 - ac)) / a
		其中判别式b ^ 2 - ac决定了二次方程中实根的数量。
		若判别式为负值，则方程无实根，即直线不与圆柱体相交。
		若判别式为正值，则方程有两个实根，较小根对应直线进入圆柱面时的t值，较大根对应直线离开圆柱面
		时的t值。
		若判别式为0，则方程只有一个根，即直线在t值处与圆柱面相切于一点。
		
		若(方向)直线并非背离指向底面平面，则需要进行该平面的相交计算，进而执行圆柱体的面相交测试。
		这里给定一个有效的t值，若(L(t) - P) · d < 0或(m · d) + t(n · d) < 0，则L(t)位于底面P的外部。
		m为A- P,n为B - A，< 0为钝角，所以L(t)位于平面P的外部。
		当L(t)位于圆柱体的外部时，若n · d ≤ 0，则L背离或平行于底面P。
		仅当n · d > 0时，才需执行底面P的相交测试。
		令底面P的平面方程为：
		(X - P) · d = 0
		将L(t)代入X并计算平面交点处的t值为t = -(m · d) / (n · d)。若(L(t) - P) · (L(t) - P) ≤ r ^ 2，则
		交点位于底面内部。
		类似地，当(L(t) - P) · d > d · d,或(m · d) + t(n · d) > d · d时，L(t)位于底面Q平面的外部。由于
		L(t)位于圆柱体底面Q的外部，则，仅当n · d < 0时才进行底面Q的相交测试：
		当n · d >= 0时，直线与圆柱体之间不存在相交。此处，底面Q的平面方程为(X - Q) · d = 0，且交点
		处的t值为：t = ((d · d) - (m · d)) / (n · d)。另外，当(L(t) - Q) · (L(t) - Q) ≤ r ^ 2时，交点位于
		底面内部。
		*/

		auto top = cylinder.getTopCenter();
		auto bottom = cylinder.getBottomCenter();
		auto radius = cylinder.getRadius();
		//圆柱体中心线的方向
		auto centerLineDir = bottom - top;
		centerLineDir.normalize();

		auto start = lineSegment.getStart();
		auto end = lineSegment.getEnd();
		//这里线段的方向向量，不能为单位向量，因为在推导过程中，没有把其看做单位向量
		auto dir = end - start;

		//判断线段的起点与圆柱体的关系
		auto spatialRelationship = ungPointSideOfCylinder(cylinder, start);

		//如果线段的起点位于圆柱体内或圆柱体的表面上
		if (spatialRelationship == 0)
		{
			out = start;

			return true;
		}

		//如果线段的起点位于top面的外部
		if (spatialRelationship == 2)
		{
			//如果线段的方向背离圆柱体
			if (dir.dotProduct(centerLineDir) < 0.0)
			{
				return false;
			}

			//计算线段和top平面的交点
			Vector3 sec{};
			bool ret = intersects(lineSegment, Plane(-centerLineDir, top),sec);
			if (!ret)
			{
				return false;
			}

			//计算线段和top平面交点到top点的距离的平方
			auto sqDis = sec.squaredDistance(top);

			if (sqDis > radius * radius)
			{
				return false;
			}

			out = sec;

#if UNG_DEBUGMODE
			//验证out在圆柱体内或其表面上
			auto verifyRet = ungPointSideOfCylinder(cylinder, out);
			BOOST_ASSERT(verifyRet == 0);
#endif

			return true;
		}

		//如果线段的起点位于bottom面的外部
		if (spatialRelationship == 3)
		{
			//如果线段的方向背离圆柱体
			if (dir.dotProduct(-centerLineDir) < 0.0)
			{
				return false;
			}

			//计算线段和bottom平面的交点
			Vector3 sec{};
			bool ret = intersects(lineSegment, Plane(centerLineDir, bottom), sec);
			if (!ret)
			{
				return false;
			}

			//计算线段和bottom平面交点到bottom点的距离的平方
			auto sqDis = sec.squaredDistance(bottom);

			if (sqDis > radius * radius)
			{
				return false;
			}

			out = sec;

#if UNG_DEBUGMODE
			//验证out在圆柱体内或其表面上
			auto verifyRet = ungPointSideOfCylinder(cylinder, out);
			BOOST_ASSERT(verifyRet == 0);
#endif

			return true;
		}

		//至此，说明线段的起点位于圆柱体弧面的外部，接下来，解方程

		/*
		a = (n · n) - ((n · d) ^ 2)
		b = (m · n) - (n · d)(m · d)
		c = ((m · m) - (r ^ 2)) - ((m · d) ^ 2)
		t = (-b ± √(b ^ 2 - ac)) / a
		*/
		auto n = dir;
		auto nn = n.squaredLength();
		auto d = centerLineDir;
		auto nd = dir.dotProduct(d);
		auto m = start - top;
		auto mn = m.dotProduct(n);
		auto md = m.dotProduct(d);
		auto mm = m.squaredLength();

		auto a = nn - nd * nd;
		auto b = mn - nd * md;
		auto c = mm - radius * radius - md * md;

		auto discriminant = b * b - a * c;

		if (discriminant < 0.0)
		{
			return false;
		}

		auto base = Math::calSqrt(discriminant);

		auto t1 = (-b - base) / a;
		auto t2 = (-b + base) / a;
		auto t = t1 < t2 ? t1 : t2;

		if (t < 0.0 || t > 1.0)
		{
			return false;
		}

		out = lineSegment.getPoint(t);

#if UNG_DEBUGMODE
		//验证out在圆柱体内或其表面上
		auto verifyRet = ungPointSideOfCylinder(cylinder, out);
		BOOST_ASSERT(verifyRet == 0);
#endif

		return true;
	}

	bool CollisionDetection::intersects(const Ray& ray, const Plane& plane,Vector3& out)
	{
		/*
		射线方程：
		p(t) = p0 + tD;
		平面方程：
		n · p = d
		那么：
		n · (p0 + tD) = d
		分配律：
		n · p0 + n · tD = d
		n · tD = d - (n · p0)
		结合律：
		t(n · D) = d - (n · p0)
		解出t值：
		t = (d - (n · p0)) / (n · D)
		若t < 0，则射线与平面不相交
		否则，将t代入射线方程即可求得交点p(t)：
		p(t) = p0 + tD

		If the ray is parallel to the plane, then the denominator n · D is zero and there is no 
		intersection.
		If the value for t is out of range, then the ray does not intersect the plane.
		We may also wish to intersect only with the front of the plane. In this case, we say there 
		is an intersection only if the ray points in a direction opposite to the normal of the plane 
		(i.e., D · n < 0).
		*/

		Vector3 p0 = ray.getOrigin();
		Vector3 D = ray.getDirection();
		BOOST_ASSERT(D.isUnitLength());

		Vector3 n = plane.getN();
		BOOST_ASSERT(n.isUnitLength());
		real_type d = plane.getD();

		auto nD = n.dotProduct(D);

		//射线只与平面的正面进行碰撞检测
		if (nD >= 0.0)
		{
			return false;
		}

		real_type t = (d - (n.dotProduct(p0))) / nD;

		if (t < 0)
		{
			return false;
		}

		out = p0 + t * D;

		return true;
	}

	bool CollisionDetection::intersects(Ray const & ray, Triangle const & triangle, real_type & u, real_type & v, real_type & w)
	{
		/*
		算法同线段-三角形，只是不对线段的终点进行限制。
		*/

		auto p = ray.getOrigin();

		auto vertices = triangle.getVertices();
		auto a = vertices[0];
		auto b = vertices[1];
		auto c = vertices[2];

		auto ab = b - a;
		auto ac = c - a;
		auto qp = -(ray.getDirection());
		auto ap = p - a;

		//三角形法线
		auto n = ab.crossProduct(ac);

		/*
		Compute denominator d.
		If d <= 0, ray is parallel to or points away from triangle, so exit early.
		*/
		auto d = qp.dotProduct(n);
		if (d <= 0.0)
		{
			return false;
		}

		/*
		Compute intersection t value of pq with plane of triangle.
		A ray intersects if 0 <= t.
		Segment intersects if 0 <= t <= 1.
		Delay dividing by d until intersection has been found to pierce triangle.
		*/
		auto t = ap.dotProduct(n);
		if (t < 0.0)
		{
			return false;
		}

		//Compute barycentric coordinate components and test if within bounds
		auto e = qp.crossProduct(ap);
		v = ac.dotProduct(e);
		if (v < 0.0 || v > d)
		{
			return false;
		}
		w = -(ab.dotProduct(e));
		if (w < 0.0 || v + w > d)
		{
			return false;
		}

		/*
		Segment/ray intersects triangle.
		Perform delayed division and compute the last barycentric coordinate component.
		*/
		auto ood = 1.0 / d;
		//t:可以用于确定射线上的交点
		t *= ood;
		//u,v,w:用于确定三角形上的交点
		v *= ood;
		w *= ood;
		u = 1.0 - v - w;

#if UNG_DEBUGMODE
		//验证：射线上的交点等于三角形上的交点
		auto rayIsec = p + t * ray.getDirection();
		auto triIsce = u * a + v * b + w * c;
		BOOST_ASSERT(rayIsec == triIsce);
#endif

		return true;
	}

	bool CollisionDetection::intersects(Ray const & ray, Sphere const & sphere, Vector3 & out)
	{
		/*
		R(t) = P + td, t ≥ 0, where P is the ray origin and d a normalized direction vector,|d| = 1.
		Let the sphere boundary be defined by (X - C) · (X - C) = r ^ 2,where C is the sphere 
		center and r its radius.
		To find the t value at which the ray intersects the surface of the sphere, R(t) is substituted 
		for X, giving (P + td - C) · (P + td - C) = r ^ 2.

		Let m = P - C, then:
		(m + t d) · (m + t d) = r ^ 2								(substituting m = P - C)
		(d · d)t ^ 2 + 2(m · d)t + (m · m) = r ^ 2			(expanding the dot product)
		t ^ 2 + 2(m · d)t + (m · m) - r2 = 0					(simplifying d · d = 1;)

		对于二次方程t ^ 2 + 2bt + c = 0,解为：
		t = -b ± √(b ^ 2 - c)
		这里，b = m · d and c = (m · m) - r ^ 2
		二次方程的解包含3种结果，这取决于判别式d = b ^ 2 - c。
		若d < 0，则方程无实根，射线不与球体相交。
		若d = 0，则方程存在一个实根，射线与球体相切于一点。
		若d > 0，则方程存在两个实根，射线与球体相交两次，一次进入，一次离开。其中较小的t值为：
		t = -b - √(b ^ 2 - c)
		然而，要注意一个无效的相交状态，即射线起始于球体外部且背向球体。

		如果d不是单位向量，方程则变为：
		(d · d)t ^ 2 +2(m · d)t + (m · m) - r ^ 2 = 0，这一类形如：
		at ^ 2 + 2bt + c = 0的二次方程，解为：
		t = (-b ± √(b ^ 2 - ac)) / a，判别式为：d = b ^ 2 - ac
		*/

		auto p = ray.getOrigin();
		auto d = ray.getDirection();
		//这里d必须为单位向量
		BOOST_ASSERT(d.isUnitLength());
		auto center = sphere.getCenter();
		auto radius = sphere.getRadius();
		
		auto m = p - center;
		float b = m.dotProduct(d);
		float c = m.dotProduct(m) - radius * radius;

		/*
		Exit if r’s origin outside s (c > 0) and r pointing away from s (b > 0)
		c > 0:
		c = (m · m) - r ^ 2
		m = P - C
		所以射线的起始点位于球体外部。
		b > 0:
		b = m · d
		也就是说从球心到射线起点的向量和射线的方向向量的夹角为锐角，所以射线的延展方向为远离球体
		*/
		if (c > 0.0 && b > 0.0)
		{
			return false;
		}

		//根号下
		float discr = b * b - c;

		//A negative discriminant corresponds to ray missing sphere
		if (discr < 0.0)
		{
			return false;
		}

		//Ray now found to intersect sphere, compute smallest t value of intersection
		auto t = -b - Math::calSqrt(discr);

		//If t is negative, ray started inside sphere so clamp t to zero
		if (t < 0.0)
		{
			t = 0.0;
		}

		out = p + t * d;

		return true;
	}

	bool CollisionDetection::intersects(const Ray & ray, const AABB & box,Vector3& out)
	{
		/*
		If the overall largest tnear value i.e,. intersection with the near slab, is greater than the 
		smallest tfar value (intersection with far slab) then the ray misses the box, else it hits the 
		box.

		将射线的参数化方程R(t) = P + td，带入到slab平面方程X · ni = di中求解。最终得到：
		t = (d - P · ni) / (d · ni)
		对于AABB，各法线向量中均有两个分量为0，因此，给定P = (px, py, pz) and d = (dx, dy, dz)，
		则针对垂直于x轴的平面，上述表达式可以化简为：t = (d - px) / dx，其中d为x轴上平面的相应位置。
		*/

		auto largestTNear = -FLT_MAX;
		auto smallestTFar = FLT_MAX;

		auto p = ray.getOrigin();
		auto d = ray.getDirection();

		auto min = box.getMin();
		auto max = box.getMax();

		//For all three slabs(each pair of planes)
		for (int32 i = 0; i < 3; ++i)
		{
			//如果射线平行于当前轴
			if (Math::isZero(d[i]))
			{
				/*
				Ray is parallel to slab.No hit if origin not within slab
				为了避免射线平行于slab平面所产生的除0错误。
				如果射线原点的当前轴分量不在当前slab对之间，那么返回false。
				*/
				if (p[i] < min[i] || p[i] > max[i])
				{
					return false;
				}
			}
			else
			{
				//Compute intersection t value of ray with near and far plane of slab
				auto ood = 1.0 / d[i];
				auto t1 = (min[i] - p[i]) * ood;
				auto t2 = (max[i] - p[i]) * ood;

				//Make t1 be intersection with near plane,t2 with far plane.
				if (t1 > t2)
				{
					std::swap(t1, t2);
				}

				//Compute the intersection of slab intersection intervals
				if (t1 > largestTNear)
				{
					//want largest Tnear
					largestTNear = t1;
				}
				if (t2 < smallestTFar)
				{
					//want smallest Tfar
					smallestTFar = t2;
				}

				//Exit with no collision.
				if (largestTNear > smallestTFar)
				{
					return false;
				}

				//if box is behind ray
				if (smallestTFar < 0.0)
				{
					return false;
				}
			}
		}

		/*
		largestTNear：交点
		smallestTFar：离开点
		*/
		out = p + d * largestTNear;

		return true;
	}

	bool CollisionDetection::intersects(const Ray & ray, const AABB & box, Vector3 & out, int32 & entryIndex, int32 & exitIndex)
	{
		/*
		对常规射线-AABB算法的修改细节：
		1,当largestTNear和smallestTFar被赋值的时候，设置入口和出口索引值。
		*/

		auto largestTNear = -FLT_MAX;
		auto smallestTFar = FLT_MAX;

		auto p = ray.getOrigin();
		auto d = ray.getDirection();

		auto min = box.getMin();
		auto max = box.getMax();

		//For all three slabs(each pair of planes)
		for (int32 i = 0; i < 3; ++i)
		{
			//如果射线平行于当前轴
			if (Math::isZero(d[i]))
			{
				/*
				Ray is parallel to slab.No hit if origin not within slab
				为了避免射线平行于slab平面所产生的除0错误。
				如果射线原点的当前轴分量不在当前slab对之间，那么返回false。
				*/
				if (p[i] < min[i] || p[i] > max[i])
				{
					return false;
				}
			}
			else
			{
				//Compute intersection t value of ray with near and far plane of slab
				auto ood = 1.0 / d[i];
				auto t1 = (min[i] - p[i]) * ood;
				auto t2 = (max[i] - p[i]) * ood;

				//Make t1 be intersection with near plane,t2 with far plane.
				if (t1 > t2)
				{
					std::swap(t1, t2);
				}

				//Compute the intersection of slab intersection intervals
				if (t1 > largestTNear)
				{
					//want largest Tnear
					largestTNear = t1;

					//更新入口索引
					entryIndex = i;
				}
				if (t2 < smallestTFar)
				{
					//want smallest Tfar
					smallestTFar = t2;

					//更新出口索引
					exitIndex = i + 3;
				}

				//Exit with no collision.
				if (largestTNear > smallestTFar)
				{
					return false;
				}

				//if box is behind ray
				if (smallestTFar < 0.0)
				{
					return false;
				}
			}
		}

		/*
		largestTNear：交点
		smallestTFar：离开点
		*/
		out = p + d * largestTNear;

		return true;
	}

	bool CollisionDetection::intersects(Ray const & ray, PlaneBoundedVolume const & planeBV, Vector3 & entry, Vector3 & exit)
	{
		/*
		线段-凸多面体算法修改细节：
		1,auto tLast = FLT_MAX;
		*/

		auto a = ray.getOrigin();
		auto d = ray.getDirection();

		auto iterator = planeBV.getIterator();
		auto beg = iterator.first;
		auto end = iterator.second;

		/*
		Set initial interval to being the whole ray.
		For a ray,tLast should be set to +FLT_MAX.
		For a line,tFirst should be set to –FLT_MAX
		*/
		auto tFirst = 0.0;
		auto tLast = FLT_MAX;

		//Intersect ray against each plane
		while (beg != end)
		{
			auto plane = *beg;
			auto pN = plane->getN();
			auto pD = plane->getD();

			auto denom = pN.dotProduct(d);
			auto dist = pD - pN.dotProduct(a);

			//Test if ray runs parallel to the plane
			if (Math::isZero(denom))
			{
				//If so, return “no intersection” if ray lies outside plane
				if (dist > 0.0)
				{
					return false;
				}
			}
			else
			{
				//Compute parameterized t value for intersection with current plane
				auto t = dist / denom;

				//平面面向射线
				if (denom < 0.0)
				{
					//When entering halfspace,update tFirst if t is larger
					if (t > tFirst)
					{
						tFirst = t;
					}
				}
				//平面背向射线
				else
				{
					//When exiting halfspace,update tLast if t is smaller
					if (t < tLast)
					{
						tLast = t;
					}
				}

				/*
				Exit with “no intersection” if intersection becomes empty.
				*/
				if (tFirst > tLast)
				{
					return false;
				}
			}

			++beg;
		}

		/*
		A nonzero logical intersection, so the ray intersects the polyhedron.
		在计算判别式时，没有把长度给除去，所以长度保存在了t值中，所以这里可以用t乘以一个单位长度的
		方向d。
		*/
		entry = a + tFirst * d;
		exit = a + tLast * d;

		return true;
	}

	bool CollisionDetection::intersects(Ray const & ray, Cylinder const & cylinder, Vector3 & out)
	{
		/*
		对线段-圆柱体算法的修改细节：
		1：令线段的方向向量为单位向量。
		2：对t的范围限制为[0.0,+∞)

		关于t的二次方程，可以表示为：
		a(t ^ 2) + 2bt + c = 0
		其中：
		a = 1.0 - ((n · d) ^ 2)
		b = (m · n) - (n · d)(m · d)
		c = ((m · m) - (r ^ 2)) - ((m · d) ^ 2)
		其中：
		m = A - P
		n，为射线的方向向量，长度为1.0
		d，为圆柱体bottom - top，长度为1.0
		*/

		auto top = cylinder.getTopCenter();
		auto bottom = cylinder.getBottomCenter();
		auto radius = cylinder.getRadius();
		//圆柱体中心线的方向
		auto centerLineDir = bottom - top;
		centerLineDir.normalize();

		auto start = ray.getOrigin();
		//射线的方向向量，长度为1.0
		auto dir = ray.getDirection();
		BOOST_ASSERT(dir.isUnitLength());

		//判断射线的起点与圆柱体的关系
		auto spatialRelationship = ungPointSideOfCylinder(cylinder, start);

		//如果射线的起点位于圆柱体内或圆柱体的表面上
		if (spatialRelationship == 0)
		{
			out = start;

			return true;
		}

		//如果射线的起点位于top面的外部
		if (spatialRelationship == 2)
		{
			//如果射线的方向背离圆柱体
			if (dir.dotProduct(centerLineDir) < 0.0)
			{
				return false;
			}

			//计算射线和top平面的交点
			Vector3 sec{};
			bool ret = intersects(ray, Plane(-centerLineDir, top),sec);
			if (!ret)
			{
				return false;
			}

			//计算射线和top平面交点到top点的距离的平方
			auto sqDis = sec.squaredDistance(top);

			if (sqDis > radius * radius)
			{
				return false;
			}

			out = sec;

#if UNG_DEBUGMODE
			//验证out在圆柱体内或其表面上
			auto verifyRet = ungPointSideOfCylinder(cylinder, out);
			BOOST_ASSERT(verifyRet == 0);
#endif

			return true;
		}

		//如果射线的起点位于bottom面的外部
		if (spatialRelationship == 3)
		{
			//如果射线的方向背离圆柱体
			if (dir.dotProduct(-centerLineDir) < 0.0)
			{
				return false;
			}

			//计算射线和bottom平面的交点
			Vector3 sec{};
			bool ret = intersects(ray, Plane(centerLineDir, bottom), sec);
			if (!ret)
			{
				return false;
			}

			//计算射线和bottom平面交点到bottom点的距离的平方
			auto sqDis = sec.squaredDistance(bottom);

			if (sqDis > radius * radius)
			{
				return false;
			}

			out = sec;

#if UNG_DEBUGMODE
			//验证out在圆柱体内或其表面上
			auto verifyRet = ungPointSideOfCylinder(cylinder, out);
			BOOST_ASSERT(verifyRet == 0);
#endif

			return true;
		}

		//至此，说明射线的起点位于圆柱体弧面的外部，接下来，解方程

		/*
		a = 1.0 - ((n · d) ^ 2)
		b = (m · n) - (n · d)(m · d)
		c = ((m · m) - (r ^ 2)) - ((m · d) ^ 2)
		其中：
		m = A - P
		n，为射线的方向向量，长度为1.0
		d，为圆柱体bottom - top，长度为1.0
		t = (-b ± √(b ^ 2 - ac)) / a
		*/
		auto n = dir;
		auto d = centerLineDir;
		auto nd = dir.dotProduct(d);
		auto m = start - top;
		auto mn = m.dotProduct(n);
		auto md = m.dotProduct(d);
		auto mm = m.squaredLength();

		auto a = 1.0 - nd * nd;
		auto b = mn - nd * md;
		auto c = mm - radius * radius - md * md;

		auto discriminant = b * b - a * c;

		if (discriminant < 0.0)
		{
			return false;
		}

		auto base = Math::calSqrt(discriminant);

		auto t1 = (-b - base) / a;
		auto t2 = (-b + base) / a;
		auto t = t1 < t2 ? t1 : t2;

		if (t < 0.0)
		{
			return false;
		}

		out = ray.getPoint(t);

#if UNG_DEBUGMODE
		//验证out在圆柱体内或其表面上
		auto verifyRet = ungPointSideOfCylinder(cylinder, out);
		BOOST_ASSERT(verifyRet == 0);
#endif

		return true;
	}

	bool CollisionDetection::intersects(Plane const & plane1, Plane const & plane2, Vector3 & p0, Vector3 & lineDir)
	{
		/*
		一种获取交线上一点的方法是：在垂直于交线的平面上来表示该点。由此可知，该平面将“持有”n1,n2，
		且找到相关的k1,k2值，那么有P = k1n1 + k2n2，其中P就是我们要找的点。
		那么，我们令P同时满足两个平面(plane1,plane2)的平面方程:
		n1 · (k1n1 + k2n2) = d1
		n2 · (k1n1 + k2n2) = d2
		->
		k1(n1 · n1) + k2(n1 · n2) = d1
		k1(n1 · n2) + k2(n2 · n2) = d2
		这样，根据克莱姆法则来求解线性方程组，就可以得到k1,k2。
		k1 = (d1(n2 · n2) - d2(n1 · n2)) / denom
		k2 = (d2(n1 · n1) - d1(n1 · n2)) / denom,
		where
		denom = (n1 · n1)(n2 · n2) - (n1 · n2) ^ 2
		根据拉格朗日恒等式可以看出，分母为：
		denom = (n1×n2)·(n1×n2)
		denom = d · d,(d为交线的方向)
		再利用：u × (v × w) = (u · w)v - (v · w)u
		P = k1n1 + k2n2可进一步化简为：
		P = [(d1(n2 · n2) - d2(n1 · n2))n1 + (d2(n1 · n1) - d1(n1 · n2))n2] / denom
		两边乘以denom:
		P denom = (d1(n2 · n2) - d2(n1 · n2))n1 + (d2(n1 · n1) - d1(n1 · n2))n2
		标量乘法分配:
		P denom = (d1(n2 · n2)n1 - d2(n1 · n2)n1) + (d2(n1 · n1)n2 - d1(n1 · n2)n2)
		合并同类项：
		P denom = d1((n2 · n2)n1 - (n1 · n2)n2) + d2((n1 · n1)n2 - (n1 · n2)n1)
		利用上面的向量恒等式：
		P denom = d1(n2 × (n1 × n2)) + d2(n1 × (n2 × n1))
		改变符号以使叉积格式相同：
		P denom = d1(n2 × (n1 × n2)) - d2(n1 × (n1 × n2))
		合并同类项：
		P denom = (d1n2 - d2n1) × (n1 × n2)
		用d替换n1 × n2：
		P = (d1n2 - d2n1) × d / denom

		L = P + td中，各项的最终结果为：
		P = (d1n2 - d2n1) × d / (d · d),d = n1 × n2
		*/

		auto plane1N = plane1.getN();
		auto plane2N = plane2.getN();
		auto plane1D = plane1.getD();
		auto plane2D = plane2.getD();

		lineDir = plane1N.crossProduct(plane2N);

		auto denom = lineDir.dotProduct(lineDir);
		if (Math::isZero(denom))
		{
			return false;
		}

		//Compute point on intersection line
		p0 = ((plane1D * plane2N - plane2D * plane1N).crossProduct(lineDir)) / denom;

#if UNG_DEBUGMODE
		//验证
		Vector3 p00, lineD;
		bool verifyRet = ungIntersectionLineOfTwoPlanes(plane1, plane2, p00, lineD);
		BOOST_ASSERT(verifyRet);
		//两个交线方向相同
		auto mustOne = (lineDir.normalizeCopy()).dotProduct((lineD.normalizeCopy()));
		BOOST_ASSERT(Math::isEqual(mustOne, 1.0));
		//p0到p00和lineD所确定的直线的距离为0.0
		auto mustZero = ungPointToStraightLineDistance(p00,p00 + lineD,p0);
		BOOST_ASSERT(Math::isZero(mustZero));
		//p00到p0和lineDir所确定的直线的距离为0.0
		mustZero = ungPointToStraightLineDistance(p0, p0 + lineDir,p00);
		BOOST_ASSERT(Math::isZero(mustZero));
#endif

		return true;
	}

	bool CollisionDetection::intersects(Plane const & plane, LineSegment const & lineSegment, Vector3 & out)
	{
		return intersects(lineSegment, plane, out);
	}

	bool CollisionDetection::intersects(const Plane & plane, const Ray & ray, Vector3 & out)
	{
		return intersects(ray,plane,out);
	}

	bool CollisionDetection::intersects(const Plane & p, const Sphere & s)
	{
		return intersects(s, p);
	}

	bool CollisionDetection::intersects(const Plane& plane, const AABB& box)
	{
		/*
		令平面P由方程(n · X) = d定义，则盒体B与平面P的相交测试完全可以采用分离轴测试的方案。
		这里，只需测试与平面法线n平行的轴。
		由于平面无限扩展，因而不存在边-边组合测试。
		与盒体面法线对应的轴也可以从测试中排除，因为平面的无限扩展性表明，如果盒体的一个面不
		与测试平面平行，则二者总是存在相交的几率。
		既然平行于n的直线都可以作为分离轴，那么令直线L穿过盒体的中心，L的直线方程定义为：
		L(t) = C + tn
		当t等于0时，即为盒体中心C在直线L上的投影。
		由于盒体关于其中心点对称，则其在L上的投影也将生成一个对称区间[C - rn, C + rn]
		现在B与P的相交问题转化为：
		计算投影区间的半径r，并检测B中心点至P的距离是否小于r。
		*/

		auto n = plane.getN();

		auto c = box.getCenter();
		auto e = box.getHalfSize();

		//通过计算各项绝对值并求和得到n向的正向幅度
		auto r = Math::calAbs(e.x * n.x) + 
			Math::calAbs(e.y * n.y) + 
			Math::calAbs(e.z * n.z);

		auto dist = ungSignedDistanceFromPointToPlane(plane, c);

		/*
		Intersection occurs when distance s falls within [-r,+r] interval
		构建平面时，对法线进行规范化会产生一定的误差。(小数点后15,16位)
		*/
		auto ret = Math::isLessThanEqual(Math::calAbs(dist), r);

		return ret;
	}

	bool CollisionDetection::intersects(const Plane & plane, const Cone & cone)
	{
		auto n = plane.getN();
		auto top = cone.getTop();
		auto dir = cone.getDir();
		auto height = cone.getHeight();
		auto radius = cone.getRadius();

		//朝-n方向
		auto helperNN = (n.crossProduct(dir)).crossProduct(dir);

		//朝n方向
		auto helperPN = -helperNN;
		BOOST_ASSERT(helperPN == -n.crossProduct(dir).crossProduct(dir));

		//圆锥体的顶尖到平面的有符号距离
		auto topDis = ungSignedDistanceFromPointToPlane(plane, top);

		//圆锥体底面上朝平面-n方向的最远点
		auto nq = top + height * dir + radius * helperNN;
		auto nqDis = ungSignedDistanceFromPointToPlane(plane, nq);

		//圆锥体底面上朝平面n方向的最远点
		auto pq = top + height * dir + radius * helperPN;
		auto pqDis = ungSignedDistanceFromPointToPlane(plane, pq);

		//如果没有被平面穿越，而只是和平面接触
		if (Math::isZero(topDis) || Math::isZero(nqDis) || Math::isZero(pqDis))
		{
			return true;
		}

		//如果顶尖位于平面的正半空间
		if (topDis > 0.0)
		{
			if (nqDis < 0.0)
			{
				return true;
			}
		}
		//如果顶尖位于平面的负半空间
		else
		{
			if (pqDis > 0.0)
			{
				return true;
			}
		}

		return false;
	}

	bool CollisionDetection::intersects(const Plane & p, const Sphere & s, bool negativeHalfSpace)
	{
		return intersects(s, p, negativeHalfSpace);
	}

	bool CollisionDetection::intersects(const Cone & cone, const Plane & plane, bool negativeHalfSpace)
	{
		return intersects(plane, cone, negativeHalfSpace);
	}

	bool CollisionDetection::intersects(Triangle const & triangle, Vector3 const & p, Vector3 const & q, real_type & u, real_type & v, real_type & w)
	{
		return intersects(p, q, triangle, u, v, w);
	}

	bool CollisionDetection::intersects(Triangle const & triangle, LineSegment const & lineSegment, real_type & u, real_type & v, real_type & w)
	{
		return intersects(lineSegment, triangle, u, v, w);
	}

	bool CollisionDetection::intersects(Triangle const & triangle, Ray const & ray, real_type & u, real_type & v, real_type & w)
	{
		return intersects(ray, triangle, u, v, w);
	}

	bool CollisionDetection::intersects(const Triangle & triangle1, const Triangle & triangle2, bool coplanar)
	{
		/*
		If the triangles are co-planar, they are projected onto the axis-aligned plane where the 
		areas of the triangles are maximized.
		Then a simple two-dimensional triangle-triangle overlap test is performed.
		First, test all closed edges of T1 for intersection with the edges of T2.If any intersection is 
		found, then the triangles intersect.
		Otherwise, we must test if T1 is totally contained in T2 or vice versa.This can be done by 
		performing a point-in-triangle test for one vertex of T1 against T2 and vice versa.
		*/

		BOOST_ASSERT(coplanar);

		//三角形1的顶点
		auto v1 = triangle1.getVertices();
		//三角形2的顶点
		auto v2 = triangle2.getVertices();

#if UNG_DEBUGMODE
		//验证参数所指定的两个三角形共面
		auto isCoplanar = ungIsTwoTrianglescoplanar(triangle1, triangle2);
		BOOST_ASSERT(isCoplanar);
#endif

		//平面
		Plane plane(v1[0],v1[1],v1[2]);
		//平面法线
		auto N = plane.getN();

		//用于判断舍弃分量
		uint8 first,second;
		if (Math::calAbs(N.x) > Math::calAbs(N.y))
		{
			if (Math::calAbs(N.x) > Math::calAbs(N.z))
			{
				first = 1;					//x is greatest
				second = 2;
			}
			else
			{
				first = 0;					//z is greatest
				second = 1;
			}
		}
		else
		{
			if (Math::calAbs(N.z) > Math::calAbs(N.y))
			{
				first = 0;					//z is greatest
				second = 1;
			}
			else
			{
				first = 0;					//y is greatest
				second = 2;
			}
		}

		//投影后两个三角形的顶点
		Vector2 pv1[3];
		pv1[0] = Vector2(v1[0][first],v1[0][second]);
		pv1[1] = Vector2(v1[1][first],v1[1][second]);
		pv1[2] = Vector2(v1[2][first],v1[2][second]);
		Vector2 pv2[3];
		pv2[0] = Vector2(v2[0][first], v2[0][second]);
		pv2[1] = Vector2(v2[1][first], v2[1][second]);
		pv2[2] = Vector2(v2[2][first], v2[2][second]);

		//线段和线段相交测试
		Vector2 dummyOut;
		auto ret = intersects(pv1[0], pv1[1], pv2[0], pv2[1],dummyOut);
		if (ret) return true;
		ret = intersects(pv1[0], pv1[1], pv2[1], pv2[2], dummyOut);
		if (ret) return true;
		ret = intersects(pv1[0], pv1[1], pv2[2], pv2[0], dummyOut);
		if (ret) return true;

		ret = intersects(pv1[1], pv1[2], pv2[0], pv2[1], dummyOut);
		if (ret) return true;
		ret = intersects(pv1[1], pv1[2], pv2[1], pv2[2], dummyOut);
		if (ret) return true;
		ret = intersects(pv1[1], pv1[2], pv2[2], pv2[0], dummyOut);
		if (ret) return true;

		ret = intersects(pv1[2], pv1[0], pv2[0], pv2[1], dummyOut);
		if (ret) return true;
		ret = intersects(pv1[2], pv1[0], pv2[1], pv2[2], dummyOut);
		if (ret) return true;
		ret = intersects(pv1[2], pv1[0], pv2[2], pv2[0], dummyOut);
		if (ret) return true;

		//测试三角形1是否完全位于三角形2内
		bool inside = ungIsTriangleContainedInTriangle(triangle2,triangle1);
		if (inside)
		{
			return true;
		}
		//测试三角形2是否完全位于三角形1内
		inside = ungIsTriangleContainedInTriangle(triangle1, triangle2);
		if (inside)
		{
			return true;
		}

		return false;
	}

	bool CollisionDetection::intersects(Triangle const& A, Triangle const& B)
	{
		//判断一个三角形是否完全位于另一个三角形所构成的平面的某个半空间内
		auto toPlaneA = ungIsTriangleFullyInsideHalfSpaceOfPlane(Plane(A), B);
		if (!Math::isZero(toPlaneA))
		{
			return false;
		}
		auto toPlaneB = ungIsTriangleFullyInsideHalfSpaceOfPlane(Plane(B), A);
		if (!Math::isZero(toPlaneB))
		{
			return false;
		}

		//如果两个三角形共面
		if (ungIsTwoTrianglescoplanar(A, B))
		{
			return CollisionDetection::intersects(A, B, true);
		}

		/*
		至此，三角形A和B所构成的平面必然相交。
		The intersection of planeA and planeB is a line, L = O + tD, where D = N1 x N2 is the direction of the
		line and O is some point on it.
		Note that due to our previous calculations and rejections,both triangles are guaranteed to
		intersect L(并不保证两个三角形相交，而是每个三角形都将和L相交).
		These intersections form(注意：不是from) intervals on L, and if these intervals overlap, the
		triangles overlap as well.
		*/

		auto verticesA = A.getVertices();
		auto a0 = verticesA[0];
		auto a1 = verticesA[1];
		auto a2 = verticesA[2];

		auto verticesB = B.getVertices();
		auto b0 = verticesB[0];
		auto b1 = verticesB[1];
		auto b2 = verticesB[2];

		//两个平面的法线
		auto aN = (a1 - a0).crossProduct(a2 - a0);
		auto bN = (b1 - b0).crossProduct(b2 - b0);

		//相交线的方向
		auto lineDir = aN.crossProduct(bN);
		lineDir.normalize();

		//找到相交线方向的最大分量
		auto projIndex = 0;
		auto maxLineDirComponent = Math::calAbs(lineDir.x);
		auto lineDirY = Math::calAbs(lineDir.y);
		auto lineDirZ = Math::calAbs(lineDir.z);
		if (lineDirY > maxLineDirComponent)
		{
			maxLineDirComponent = lineDirY;
			projIndex = 1;
		}
		if (lineDirZ > maxLineDirComponent)
		{
			maxLineDirComponent = lineDirZ;
			projIndex = 2;
		}

		//根据相交线法线的最大分量，来取两个三角形顶点的分量(顶点在相交线的投影线(某个x,y,z轴)上的投影一维坐标值)
		auto pa0 = a0[projIndex];
		auto pa1 = a1[projIndex];
		auto pa2 = a2[projIndex];

		auto pb0 = b0[projIndex];
		auto pb1 = b1[projIndex];
		auto pb2 = b2[projIndex];

		//计算三角形A的三个顶点到planeB的有符号距离(存在冗余计算，可以优化)
		Plane planeB(B);
		real_type disa0 = ungSignedDistanceFromPointToPlane(planeB, a0);
		real_type disa1 = ungSignedDistanceFromPointToPlane(planeB, a1);
		real_type disa2 = ungSignedDistanceFromPointToPlane(planeB, a2);
		//计算三角形B的三个顶点到planeA的有符号距离(存在冗余计算，可以优化)
		Plane planeA(A);
		real_type disb0 = ungSignedDistanceFromPointToPlane(planeA, b0);
		real_type disb1 = ungSignedDistanceFromPointToPlane(planeA, b1);
		real_type disb2 = ungSignedDistanceFromPointToPlane(planeA, b2);

		//寻找特殊顶点(与其它两个顶点不在同一侧)的索引
		auto findSpecial = [](real_type a, real_type b, real_type c,int32& findSpecialIndex)
		{
			auto zeroCount = 0;
			if (a == 0.0)
			{
				++zeroCount;
			}

			if (b == 0.0)
			{
				++zeroCount;
			}

			if (c == 0.0)
			{
				++zeroCount;
			}

			if (zeroCount == 0)
			{
				if (a * b > 0.0)
				{
					findSpecialIndex = 3;
				}
				else if (a * c > 0.0)
				{
					findSpecialIndex = 2;
				}
				else if (b * c > 0.0)
				{
					findSpecialIndex = 1;
				}
			}
			else if (zeroCount == 1)
			{
				if (a == 0.0)
				{
					findSpecialIndex = 1;

					if (b * c < 0.0)
					{
						findSpecialIndex = 2;			//或者3
					}
				}

				if (b == 0.0)
				{
					findSpecialIndex = 2;

					if (a * c < 0.0)
					{
						findSpecialIndex = 1;			//或者3
					}
				}

				if (c == 0.0)
				{
					findSpecialIndex = 3;

					if (a * b < 0.0)
					{
						findSpecialIndex = 1;			//或者2
					}
				}
			}
			else if (zeroCount == 2)
			{
				if (a != 0.0)
				{
					findSpecialIndex = 1;
				}
				else if(b != 0.0)
				{
					findSpecialIndex = 2;
				}
				else if (c != 0.0)
				{
					findSpecialIndex = 3;
				}
			}

			BOOST_ASSERT(findSpecialIndex != -1);
		};

		//计算投影区间
		auto computeInterval = [](real_type p0, real_type p1, real_type p2,
			real_type dis0, real_type dis1, real_type dis2, real_type indexOfSpecialVertex,
			real_type& t1, real_type& t2)
		{
			//令p0为特殊顶点
			if (indexOfSpecialVertex == 2)
			{
				std::swap(p0, p1);
				std::swap(dis0, dis1);
			}
			else if (indexOfSpecialVertex == 3)
			{
				std::swap(p0, p2);
				std::swap(dis0, dis2);
			}

			//p1和p2在同一侧
			t1 = p1 + (p0 - p1) * (dis1 / (dis1 - dis0));
			t2 = p2 + (p0 - p2) * (dis2 / (dis2 - dis0));

#if UNG_DEBUGMODE
			//用另一种计算方法来验证
			auto t11 = p0 + (p1 - p0) * dis0 / (dis0 - dis1);
			auto t22 = p0 + (p2 - p0) * dis0 / (dis0 - dis2);
			BOOST_ASSERT(Math::isEqual(t1, t11));
			BOOST_ASSERT(Math::isEqual(t2, t22));
#endif

			//令t1 <= t2
			if (t1 > t2)
			{
				std::swap(t1, t2);
			}
		};

		/*
		寻找三角形A的特殊顶点。
		specialIndexOfA:1-a0,2-a1,3-a2
		*/
		int32 specialIndexOfA = -1;
		findSpecial(disa0, disa1, disa2,specialIndexOfA);
		//计算三角形A的投影区间
		real_type ta1{}, ta2{};
		computeInterval(pa0, pa1, pa2, disa0, disa1, disa2,specialIndexOfA,ta1,ta2);

#if UNG_DEBUGMODE
		//验证
		//两平面的交线为:
		Vector3 pointInLine{};
		Vector3 lineN{};
		bool ret = ungIntersectionLineOfTwoPlanes(planeA, planeB, pointInLine, lineN);
		BOOST_ASSERT(ret);
		BOOST_ASSERT(lineN == lineDir);
#endif

#if UNG_DEBUGMODE
		//验证
		if (specialIndexOfA == 2)
		{
			std::swap(a0, a1);
		}
		else if (specialIndexOfA == 3)
		{
			std::swap(a0, a2);
		}

		//直线a1 - a0与两平面的交线的交点为：
		Vector3 isce1{}, isce2{};
		ret = CollisionDetection::intersects(a0, a1, pointInLine, pointInLine + lineN,isce1);
		BOOST_ASSERT(ret);
		//交点的投影分量坐标等于ta1
		BOOST_ASSERT(Math::isEqual(isce1[projIndex], ta1));
		//直线a2 - a0与两平面的交线的交点为：
		ret = CollisionDetection::intersects(a0, a2, pointInLine, pointInLine + lineN, isce2);
		BOOST_ASSERT(ret);
		//交点的投影分量坐标等于ta2
		BOOST_ASSERT(Math::isEqual(isce2[projIndex], ta2));
#endif

		/*
		寻找三角形B的特殊顶点。
		specialIndexOfB:1-b0,2-b1,3-b2
		*/
		int32 specialIndexOfB = -1;
		findSpecial(disb0, disb1, disb2, specialIndexOfB);
		//计算三角形B的投影区间
		real_type tb1{}, tb2{};
		computeInterval(pb0, pb1, pb2, disb0, disb1, disb2, specialIndexOfB, tb1, tb2);

#if UNG_DEBUGMODE
		//验证
		if (specialIndexOfB == 2)
		{
			std::swap(b0, b1);
		}
		else if (specialIndexOfB == 3)
		{
			std::swap(b0, b2);
		}

		//直线b1 - b0与两平面的交线的交点为：
		ret = CollisionDetection::intersects(b0, b1, pointInLine, pointInLine + lineN, isce1);
		BOOST_ASSERT(ret);
		//交点的投影分量坐标等于tb1
		BOOST_ASSERT(Math::isEqual(isce1[projIndex], tb1));
		//直线b2 - b0与两平面的交线的交点为：
		ret = CollisionDetection::intersects(b0, b2, pointInLine, pointInLine + lineN, isce2);
		BOOST_ASSERT(ret);
		//交点的投影分量坐标等于tb2
		BOOST_ASSERT(Math::isEqual(isce2[projIndex], tb2));
#endif

		if (ta2 < tb1 || tb2 < ta1)
		{
			return false;
		}

		return true;
	}

	bool CollisionDetection::intersects(Triangle const & triangle, const Sphere & sphere)
	{
		return intersects(sphere, triangle);
	}

	bool CollisionDetection::intersects(const Triangle & triangle, const AABB & box)
	{
		return intersects(box, triangle);
	}

	bool CollisionDetection::intersects(Quadrilateral const & quad, Vector3 const & p, Vector3 const & q, Vector3 & out)
	{
		return intersects(p, q, quad, out);
	}

	bool CollisionDetection::intersects(Sphere const & sphere, LineSegment const & lineSegment, Vector3 & out)
	{
		return intersects(lineSegment, sphere, out);
	}

	bool CollisionDetection::intersects(Sphere const & sphere, Ray const & ray, Vector3 & out)
	{
		return intersects(ray,sphere,out);
	}

	bool CollisionDetection::intersects(const Sphere& s, const Plane& p)
	{
		return (Math::calAbs(ungSignedDistanceFromPointToPlane(p, s.getCenter())) <= s.getRadius());
	}

	bool CollisionDetection::intersects(const Sphere & s, const Plane & p, bool negativeHalfSpace)
	{
		BOOST_ASSERT(negativeHalfSpace);

		return ungSignedDistanceFromPointToPlane(p, s.getCenter()) <= s.getRadius();
	}

	bool CollisionDetection::intersects(const Sphere & sphere, Triangle const & triangle)
	{
		auto center = sphere.getCenter();
		auto radius = sphere.getRadius();

		//Find point P on triangle ABC closest to sphere center
		Vector3 closestPoint{};
		ungClosestPointOnTriangleToPoint(triangle,center,closestPoint);

		/*
		Sphere and triangle intersect if the (squared) distance from sphere center to point p is 
		less than the (squared) sphere radius.
		*/
		return closestPoint.squaredDistance(center) <= radius * radius;
	}

	bool CollisionDetection::intersects(const Sphere& sphere, const AABB& box)
	{
		return intersects(box, sphere);
	}

	bool CollisionDetection::intersects(const Sphere & sphere, const Capsule & capsule)
	{
		/*
		所有的球扫掠体测试都可以以相同的方式加以表达。
		首先计算内部图元结构之间的距离，然后用该距离与半径和进行比较。
		任意两类球扫掠体测试的唯一区别在于：
		内部图元结构之间的距离计算方法不同。
		球扫掠体的一个比较有用的的性质是：
		内部图元结构之间的距离计算并不依赖其类型，可以很容易地构造混合类型之间的测试。
		*/

		//球的球心和半径
		auto center = sphere.getCenter();
		auto sphereRadius = sphere.getRadius();

		//胶囊中心线段的起点和终点
		auto start = capsule.getStart();
		auto end = capsule.getEnd();
		//胶囊的半径
		auto capsuleRadius = capsule.getRadius();
		
		//Compute (squared) distance between sphere center and capsule line segment
		real_type dist2 = ungPointToLineSegmentSquaredDistance(start,end,center);
		//If (squared) distance smaller than (squared) sum of radii, they collide
		real_type r = sphereRadius + capsuleRadius;

		return dist2 <= r * r;
	}

	bool CollisionDetection::intersects(const AABB & box,const Ray & ray,Vector3& out)
	{
		return intersects(ray,box,out);
	}

	bool CollisionDetection::intersects(const AABB & box, const Ray & ray, Vector3 & out, int32 & entryIndex, int32 & exitIndex)
	{
		return intersects(ray,box,out,entryIndex,exitIndex);
	}

	bool CollisionDetection::intersects(const AABB & box, const Plane & plane)
	{
		return intersects(plane, box);
	}

	bool CollisionDetection::intersects(const AABB & box, const Triangle & triangle)
	{
		/*
		通过检查三角形的三个顶点，是否最少有一个位于AABB内部的方法是错误的。
		*/
		
		/*
		三角形T与包围盒B之间的相交测试可通过分离轴测试这一方案得以高效实现。
		其中，需要考察13条轴上的投影情况：
		1，二者之间边-边组合形成的9条叉积轴。
		2，AABB上的3条面法线。
		3，三角形上的一条面法线。
		上述13轴测试方案，也可应用于AABB与OBB之间的相交测试
		*/

		auto min = box.getMin();
		auto max = box.getMax();

		auto v = triangle.getVertices();
		
		//Compute box center and extents
		auto c = (min + max) * 0.5;
		auto e0 = (max.x - min.x) * 0.5;
		auto e1 = (max.y - min.y) * 0.5;
		auto e2 = (max.z - min.z) * 0.5;

		//AABB的3条面法线
		Vector3 u[3];
		u[0] = Vector3(1.0,0.0,0.0);
		u[1] = Vector3(0.0,1.0,0.0);
		u[2] = Vector3(0.0,0.0,1.0);

		//Translate triangle as conceptually moving AABB to origin
		for (uint8 i = 0; i < 3; ++i)
		{
			v[i] -= c;
		}

		//Compute edge vectors for triangle
		Vector3 f[3];
		f[0] = v[1] - v[0];
		f[1] = v[2] - v[1];
		f[2] = v[0] - v[2];

		//9条轴
		auto cal9Axis = [](Vector3 const& u,Vector3 const& f)
		{
			Vector3 out{};
			ungVector3CrossProduct(u, f,out);
			
			return out;
		};

		//计算在给定轴n上，盒体的投影半径
		auto calR = [&](Vector3 const& n)
		{
			//这个表达式可以进行简化
			return e0 * Math::calAbs(ungVector3DotProduct(u[0], n)) +
				e1 * Math::calAbs(ungVector3DotProduct(u[1], n)) +
				e2 * Math::calAbs(ungVector3DotProduct(u[2], n));
		};

		/*
		将三角形T投影至轴n上时，其投影区间范围为：[min(p0,p1,p2),max(p0,p1,p2)]，其中p0,p1,p2
		为原点至三角形顶点在n上的投影点之间的距离。
		where p0, p1, and p2 are the distances from the origin to the projections of the triangle 
		vertices onto n.
		也就是说，是从原点到3个投影点之间的距离。
		*/
		auto calDisFromOriginToProjections = [](Vector3 const& v,Vector3 const& axis)
		{
			return ungVector3DotProduct(v, axis);
		};

		/*
		对B与T中9条边组合形成的叉积轴进行测试。
		由于u0,u1,u2是基础向量，9条轴可简化为：
		a00 = u0 × f0 = (1, 0, 0) × f0 = (0, -f0.z, f0.y)
		a01 = u0 × f1 = (1, 0, 0) × f1 = (0, -f1.z, f1.y)
		a02 = u0 × f2 = (1, 0, 0) × f2 = (0, -f2.z, f2.y)
		a10 = u1 × f0 = (0, 1, 0) × f0 = ( f0.z, 0, -f0.x)
		a11 = u1 × f1 = (0, 1, 0) × f1 = ( f1.z, 0, -f1.x)
		a12 = u1 × f2 = (0, 1, 0) × f2 = ( f2.z, 0, -f2.x)
		a20 = u2 × f0 = (0, 0, 1) × f0 = (-f0.y, f0.x, 0)
		a21 = u2 × f1 = (0, 0, 1) × f1 = (-f1.y, f1.x, 0)
		a22 = u2 × f2 = (0, 0, 1) × f2 = (-f2.y, f2.x, 0)
		*/
		for (uint8 i = 0; i < 3; ++i)
		{
			for (uint8 j = 0; j < 3; ++j)
			{
				auto axis = cal9Axis(u[i],f[j]);

				//可以简化，简化后会使下面的std::max和std::min不用嵌套。
				auto p0 = calDisFromOriginToProjections(v[0], axis);
				auto p1 = calDisFromOriginToProjections(v[1], axis);
				auto p2 = calDisFromOriginToProjections(v[2], axis);

				auto r = calR(axis);

				/*
				在给定分离轴上，如果投影区间[-r,r]与区间[min(p0,p1,p2),max(p0,p1,p2)]无交集，则三角形与
				AABB不相交。
				*/
				if (std::max(std::max(p0, p1), p2) < -r || std::min(std::min(p0, p1), p2) > r)
				{
					return false;
				}
			}
		}
		
		/*
		针对AABB的3条面法线u[0],u[1],u[2]进行测试。
		计算三角形的AABB，并测试与B的相交性。若两个AABB之间不相交，则T与B之间也不相交。
		Test the three axes corresponding to the face normals of AABB b.
		Exit if:
		[-e0, e0] and [min(v0.x,v1.x,v2.x),max(v0.x,v1.x,v2.x)] do not overlap
		[-e1, e1] and [min(v0.y,v1.y,v2.y), max(v0.y,v1.y,v2.y)] do not overlap
		[-e2, e2] and [min(v0.z,v1.z,v2.z), max(v0.z,v1.z,v2.z)] do not overlap
		*/
		if (std::max(std::max(v[0].x, v[1].x), v[2].x) < -e0 || std::min(std::min(v[0].x, v[1].x), v[2].x) > e0)
		{
			return false;
		}
		if (std::max(std::max(v[0].y, v[1].y), v[2].y) < -e1 || std::min(std::min(v[0].y, v[1].y), v[2].y) > e1)
		{
			return false;
		}
		if (std::max(std::max(v[0].z, v[1].z), v[2].z) < -e2 || std::min(std::min(v[0].z, v[1].z), v[2].z) > e2)
		{
			return false;
		}

		/*
		若相应轴与三角形面法线平行，则测试转化为AABB是否与三角形所构建的平面相交。
		Test separating axis corresponding to triangle face normal.
		*/
		auto pn = f[0].crossProduct(f[1]);
		Plane p(pn,v[0]);

#if UNG_DEBUGMODE
		//验证三角形的3个顶点是否在构建的平面上
		auto pointToPlaneSignedDis0 = ungSignedDistanceFromPointToPlane(p,v[0]);
		auto pointToPlaneSignedDis1 = ungSignedDistanceFromPointToPlane(p,v[1]);
		auto pointToPlaneSignedDis2 = ungSignedDistanceFromPointToPlane(p,v[2]);
		BOOST_ASSERT(Math::isZero(pointToPlaneSignedDis0));
		BOOST_ASSERT(Math::isZero(pointToPlaneSignedDis1));
		BOOST_ASSERT(Math::isZero(pointToPlaneSignedDis2));
		//即便这里的assert能够通过，也会有一点点误差(小数点后15,16位)
#endif

		return intersects(p, box);
	}

	bool CollisionDetection::intersects(const AABB& box, const Sphere& sphere)
	{
		//球心
		auto center = sphere.getCenter();
		//球半径
		auto radius = sphere.getRadius();

		//找到AABB中距离球心最近的一点
		Vector3 cp{};
		ungClosestPointOnAABBToPoint(box, center, cp);

		//检查最近点和球心的距离是否小于球的半径
		auto ret = cp.squaredDistance(center) <= radius * radius;

#if UNG_DEBUGMODE
		//验证
		//球心到AABB的距离的平方
		auto sqDis = ungPointToAABBSquaredDistance(box, center);
		auto ret2 = sqDis <= radius * radius;
		BOOST_ASSERT(ret == ret2);
#endif

		return ret;
	}

	bool CollisionDetection::intersects(const AABB& box1, const AABB& box2)
	{
		auto leftMin = box1.getMin();
		auto leftMax = box1.getMax();
		auto rightMin = box2.getMin();
		auto rightMax = box2.getMax();

		if ((leftMin.x > rightMax.x) || (leftMin.y > rightMax.y) || (leftMin.z > rightMax.z))
		{
			return false;
		}

		if ((leftMax.x < rightMin.x) || (leftMax.y < rightMin.y) || (leftMax.z < rightMin.z))
		{
			return false;
		}

		return true;
	}

	bool CollisionDetection::intersects(PlaneBoundedVolume const & planeBV, LineSegment const & lineSegment, Vector3 & entry, Vector3 & exit)
	{
		return intersects(lineSegment, planeBV, entry, exit);
	}

	bool CollisionDetection::intersects(PlaneBoundedVolume const & planeBV, Ray const & ray, Vector3 & entry, Vector3 & exit)
	{
		return intersects(ray, planeBV, entry, exit);
	}

	bool CollisionDetection::intersects(Cylinder const & cylinder, LineSegment const & lineSegment, Vector3 & out)
	{
		return intersects(lineSegment, cylinder, out);
	}

	bool CollisionDetection::intersects(Cylinder const & cylinder, Ray const & ray, Vector3 & out)
	{
		return intersects(ray, cylinder, out);
	}

	bool CollisionDetection::intersects(const Cone & cone, const Plane & plane)
	{
		return intersects(plane, cone);
	}

	bool CollisionDetection::intersects(const Plane & plane, const Cone & cone, bool negativeHalfSpace)
	{
		BOOST_ASSERT(negativeHalfSpace);

		auto top = cone.getTop();

		if (ungSignedDistanceFromPointToPlane(plane, top) < 0.0)
		{
			return true;
		}

		//平面法线
		auto n = plane.getN();

		//圆锥体的方向
		auto dir = cone.getDir();

		//位于圆锥体的底面上，且沿平面的-n方向的最远点
		auto q = top + cone.getHeight() * dir + cone.getRadius() * ((n.crossProduct(dir)).crossProduct(dir));

		if (ungSignedDistanceFromPointToPlane(plane, q) < 0.0)
		{
			return true;
		}

		return false;
	}

	bool CollisionDetection::intersects(const Capsule & capsule, const Sphere & sphere)
	{
		return intersects(sphere,capsule);
	}

	bool CollisionDetection::intersects(const Capsule & capsule1, const Capsule & capsule2)
	{
		////Compute (squared) distance between the inner structures of the capsules
		//float s, t;
		//Point c1, c2;
		//float dist2 = ClosestPtSegmentSegment(capsule1.a, capsule1.b,
		//	capsule2.a, capsule2.b, s, t, c1, c2);
		////If (squared) distance smaller than (squared) sum of radii, they collide
		//float radius = capsule1.r + capsule2.r;
		//return dist2 <= radius * radius;

		return false;
	}
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE