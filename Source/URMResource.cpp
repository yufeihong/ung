#include "URMResource.h"

#ifdef URM_HIERARCHICAL_COMPILE

#include "boost/interprocess/mapped_region.hpp"
#include "boost/interprocess/file_mapping.hpp"
#include "boost/property_tree/ini_parser.hpp"
#include "boost/property_tree/info_parser.hpp"
#pragma warning(disable:4715)		//json_parser.hpp:不是所有的控件路径都返回值
#include "boost/property_tree/json_parser.hpp"
#include "boost/property_tree/xml_parser.hpp"

namespace ung
{
	Resource::Resource() :
		mSize(0)
	{
	}

	Resource::Resource(String const& fullName,String const& resourceType) :
		mFullName(fullName),
		mType(resourceType)
	{
		//得到扩展名
		mExtension = Filesystem::getInstance().getFileExtension(mFullName.c_str());

		//得到文件的大小
		mSize = Filesystem::getInstance().getFileSize(mFullName.c_str());
	}

	Resource::~Resource()
	{
	}

	String const& Resource::getFullName() const
	{
		/*
		线程池中的线程只是读取mFullName，而没有修改mFullName。
		*/
		return mFullName;
	}

	String const& Resource::getType() const
	{
		return mType;
	}

	String const& Resource::getExtension() const
	{
		return mExtension;
	}

	uint32 const& Resource::getSize() const
	{
		return mSize;
	}

	void Resource::readFile(void* treePtr)
	{
		using namespace boost::interprocess;
		//map的这个过程很快，花费的时间几乎可以忽略不计
		mapped_region region{ file_mapping(mFullName.data(),read_only),read_only,0, 0 };
		char* source = reinterpret_cast<char*>(region.get_address());
		BOOST_ASSERT(source);
		ufast size = region.get_size();
		BOOST_ASSERT(size == mSize);

		using namespace boost::property_tree;

		ptree tree = *(reinterpret_cast<ptree*>(treePtr));

		String script(source, source + size);
		std::istringstream file(script);
		//String ext = getExtension();
		//ptree tree;
		if (StringUtilities::equalInSensitive(mExtension, "ini"))
		{
			read_ini(file, tree);
		}
		else if (StringUtilities::equalInSensitive(mExtension, "info"))
		{
			read_info(file, tree);
		}
		else if (StringUtilities::equalInSensitive(mExtension, "json"))
		{
			read_json(file, tree);
		}
		else if (StringUtilities::equalInSensitive(mExtension, "xml"))
		{
			read_xml(file, tree, xml_parser::trim_whitespace);
		}
		else
		{
			BOOST_ASSERT(false && "Unrecognized extension.");
		}
	}
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

/*
A single-layer DVD can hold 4.7GB, and a single layer of a Blu-ray disc can hold up to 25GB.
*/

/*
Sound and Music Data:
Sound formats in digital audio are commonly stored in either mono or stereo(单声道或立体声),
sampled at different frequencies, and accurate to either 8 or 16 bits per sample.The effect of 
mono or stereo on the resulting playback and storage size is obvious.Stereo sound takes 
twice as much space to store but provides left and right channel waveforms.

Digital audio is created by sampling a waveform and converting it into discrete 8- or 16-bit 
values that approximate the original waveform. This works because the human ear has a 
relatively narrow range of sensitivity: 20Hz to 20,000Hz. It’s no surprise that the common 
frequencies for storing WAV files are 44KHz, 22KHz, and 11KHz.

It turns out that telephone conversations are 8-bit values sampled at 8KHz,after the original 
waveform has been filtered to remove frequencies higher than 3.4MHz.Music on CDs is first 
filtered to remove sounds higher than 22KHz and then sampled at 16-bit 44KHz.
Use lower sampling rates for digital audio in your game to simulate telephone conversations 
or talking over shortwave radio(短波收音机).

Using Different Audio Frequencies with Digital Formats:
Format									Quality			Size per Second			Size per Minute
44.1KHz 16-bit stereo WAV		CD quality		172KB/second				10MB/minute
128Kbps stereo MP3 Near			CD quality		17KB/second				1MB/minute
22.05KHz 16-bit stereo WAV		FM Radio		86KB/second				5MB/minute
64Kbps stereo MP3					FM Radio		9KB/second				540KB/minute
11.025KHz 16-bit mono WAV	AM Radio		43KB/second				2.5MB/minute
11.025KHz 8-bit mono WAV		Telephone		21KB/second				1.25MB/minute
*/

/*
Video and Prerendered Cinematics(预渲染的过场动画):
There are two techniques worth considering for incorporating cinematic sequences.
Some games will shoot live video segments and simply play them back. The file is usually an 
enormous AVI file that would fill up a good portion of your optical media. That file is usually 
compressed into something more usable by the game.
The second approach uses the game engine itself. Most games create their animated sequences 
in 3ds Max or Maya and export the animations and camera motion. The animations can be 
played back by loading a relatively tiny animation file and pumping the animations through 
the rendering engine. The only media you have to store beyond that is the sound and 3D 
models for the characters and environment. If you have tons of cinematic sequences, doing 
them in-game like this is the way to go. Lots of story-heavy games are going this direction 
because it is more efficient than storing that much prerendered video.

Matching Bit Rates(匹配码率) with CD-ROM/DVD Speeds:
Technology							Bit Rate
1x CD								150 Kbps
1x DVD								1,385 Kbps
32x CD								4,800 Kbps
16x DVD							2.21 Mbps
1x Blu-ray							36 Mbps
8x Blu-ray							288 Mbps
*/

/*
Packaging Resources into a Single File:
It’s a serious mistake(这是一个严重的错误) to store every game asset, such as a texture or 
sound effect, in its own file. Separating thousands of assets in their own files wastes valuable 
storage space and makes it impossible to get your load times faster.

Hard drives are logically organized into blocks or clusters that have surprisingly(出奇的) large 
sizes.Most hard drives in the gigabit range have cluster sizes of 16KB–32KB.File systems like 
FAT32 and NTFS were written to store a maximum of one file per cluster to enable optimal 
storage of the directory structure.This means that if you have 500 sound effect files, each 
1/2-second long and recorded at 44KHz mono,you’ll have 5.13MB of wasted space on the 
hard disk:
0.5 seconds * 44KHz mono = 22,000 bytes
32,768 bytes minimum cluster size – 22,000 bytes in each file = 10,768 bytes wasted per file
10,768 bytes wasted in each file * 500 files = 5.13MB wasted space

You can easily get around this problem by packing your game assets into a single file.

Other Benefits of Packaging Resources:
The biggest advantage of combining your resources by far is load time optimization.Opening 
files is an extremely slow operation on most operating systems. The full filename must be 
parsed, the directory structure traversed, the hardware must locate and read a number of 
blocks into the operating system read cache, and more. This can cause multiple seeks,
depending on the organization of the media. Another advantage is security. You can use a 
proprietary logical organization(专有的逻辑组织) of the file that will hamper armchair hackers 
from getting to your art and sounds.

A great trick is to keep indexes or file headers in memory while the resource file is open.These 
are usually placed at the beginning or end of a file, and on large files the index might be a 
considerable physical distance away from your data. Read the index once and keep it around 
to save yourself that extra, and very time consuming, media seek.
*/

/*
Zlib:Open Source Compression:
If you need a lossless(无损) compression/decompression system for your game,a good choice 
that has stood the test of time(经受住了时间的考验) is Zlib.Typical compression ratios with Zlib 
are 2:1 to 5:1,depending on the data stream.

Zip files store their table of contents, or file directory, at the end of the file. If you read the 
file, the TZipDirHeader at the very end of the file contains data members such as a special 
signature and the number of files stored in the Zip file. Just before the TZipDirHeader, there 
is an array of structures, one for each file, which stores data members such as the name of 
the file, the type of compression, and the size of the file before and after compression. Each 
file in the Zip file has a local header stored just before the compressed file data. It stores 
much of the same data as the TZipDirFileHeader structure.

The basic premise(前提) of the solution is to open a Zip file, read the directory into memory,
and use it to index the rest of the file.
*/