#include "UUTGLShader.h"

#ifdef UUT_HIERARCHICAL_COMPILE

namespace ung
{
	GLShader::GLShader(String const& fileName) :
		mType(0),
		mName(EMPTY_STRING),
		mShaderObject(0),
		mParser(new Parser(fileName))
	{
	}

	GLShader::~GLShader()
	{
		mType = 0;
		glDeleteShader(mShaderObject);
	}

	void GLShader::read(String const& fileName)
	{
		GLenum type;
		auto fileExt = mParser->getExtension();
		if (StringUtilities::equalInSensitive(fileExt, String("vert")))
		{
			type = GL_VERTEX_SHADER;
		}
		else if (StringUtilities::equalInSensitive(fileExt, String("frag")))
		{
			type = GL_FRAGMENT_SHADER;
		}

		BOOST_ASSERT(type == GL_VERTEX_SHADER || type == GL_FRAGMENT_SHADER ||
			type == GL_TESS_CONTROL_SHADER || type == GL_TESS_EVALUATION_SHADER ||
			type == GL_GEOMETRY_SHADER || type == GL_COMPUTE_SHADER);

		mType = type;

		mName = fileName;

		//创建一个空的shader object
		mShaderObject = glCreateShader(mType);
		//替换shader object中的source
		GLchar const* source = mParser->getSource();
		GLint length = mParser->getSize();
		glShaderSource(mShaderObject, 1, &source, &length);
		BOOST_ASSERT(glIsShader(mShaderObject));

		//释放parser，这样就可以释放掉映射到内存中的空间
		mParser.reset();			//注意：这里如果写成：mParser.release()，那么mParser不再管理其的空间，内存不会释放，而我们也失去了对空间的引用。
		BOOST_ASSERT(!mParser);
	}

	void GLShader::compile()
	{
		BOOST_ASSERT(mType);

		glCompileShader(mShaderObject);

		GLint compiled;
		glGetShaderiv(mShaderObject, GL_COMPILE_STATUS, &compiled);
		BOOST_ASSERT(compiled);
	}

	GLuint const GLShader::getShaderObject() const
	{
		return mShaderObject;
	}
}//namespace ung

#endif//UUT_HIERARCHICAL_COMPILE