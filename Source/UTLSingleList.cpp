/*
根据STL的习惯，插入操作会将新元素插入于指定位置之前，而非之后。然而作为一个单向链表，SList没有任何方便的办法可以指出前一个位置，因此它必须从头
找起。换句话说，除了SList起点处附近的区域之外，在其它位置上采用insert或erase操作函数，都属于不智之举。这便是SList相较于List之下的大缺点。为此，
SList特别提供了insert_after()和erase_after()供灵活运用。基于同样的(效率)考虑，SList不提供push_back()，只提供push_front()。
*/

/*
赋值和swap:
赋值运算符将其左边容器中的全部元素替换为右边容器中元素的拷贝：
c1 = c2;										//将才1的内容替换为c2中元素的拷贝。
c1 = {a,b,c};								//复制后，c1大小为3。
第一个赋值运算后，左边容器将与右边容器相等。如果两个容器原来大小不同，赋值运算后两者的大小都与右边容器的原大小相同。第二个赋值运算后，c1的size变为3，即花括号列
表中值的数目。

与内置数组不同，标准库array类型允许赋值。赋值号左右两边的运算对象必须具有相同的类型。
array<int,10> a1 = {0,1,2,3,4,5,6,7,8,9};
array<int,10> a2 = {0};				//所有元素值均为0。
a1 = a2;									//替换a1中的元素。
a2 = {0};									//错误，不能将一个花括号列表赋予数组。
由于右边运算对象的大小可能与左边运算对象的大小不同，因此array类型不支持assign,也不允许用花括号包围的值列表进行赋值。
下表列出的与赋值相关的运算符可用于所有容器
c1 = c2						将c1中的元素替换为c2中元素的拷贝。c1和c2必须具有相同的类型。
c = {a,b,c...}				将c1中元素替换为初始化列表中元素的拷贝(array不适用)。
swap(c1,c2)				交换c1和c2中的元素。c1和c2必须具有相同的类型。swap通常比从c2向c1拷贝元素快的多。
c1.swap(c2)				同上
assign操作不适用于关联容器和array
seq.assign(b,e)			将seq中的元素替换为迭代器b和e所表示的范围中的元素。迭代器b和e不能指向seq中的元素。
seq.assign(il)				将seq中的元素替换为初始化列表il中的元素。
seq.assign(n,t)			将seq中的元素替换为n个值为t的元素。
赋值相关运算会导致指向左边容器内部的迭代器、引用和指针失效。而swap操作将容器内容交换不会导致指向容器的迭代器、引用和指针失效。(容器类型为array和string的情况除外)
使用assign(仅顺序容器)
赋值运算符要求左边和右边的运算对象具有相同的类型。它将右边运算对象中所有元素拷贝到左边运算对象中。顺序容器(array除外)还定义了一个名为assign的成员，允许我们从一个
不同但相容的类型赋值，或者从容器的一个子序列赋值。assign操作用参数所指定的元素(的拷贝)替换左边容器中的所有元素。例如，我们可以用assign实现将一个vector中的一段
char*值赋予一个list中的string：
list<string> names;
vector<const char*> oldstyle;
names = oldstyle;															//错误：容器类型不匹配。
names.assign(oldstyle.cbegin(),oldstyle.cend());					//正确，可以将const char*转换为string。
注意，由于其旧元素被替换，因此传递给assign的迭代器不能指向调用assign的容器。

assign的第二个版本接受一个整型值和一个元素值。它用指定书目且具有相同给定值的元素替换容器中原有的元素：
//等价于slist1.clear();
//后跟slist1.insert(slist1.begin(),10,"Hiya!");
list<string> slist1(1);														//1个元素，为空string。
slist1.assign(10,"Hiya!")													//10个元素，每个都是"Hiya!"。

使用swap
swap操作交换两个相同类型容器的内容。调用swap后，两个容器中的元素将会交换：
vector<string> svec1(10);												//10个元素的vector。
vector<string> svec2(24);												//24个元素的vector。
swap(svec1,svec2);
调用swap后，svec1将包含24个string元素，svec2将包含10个string。
除array外，交换两个容器内容的操作保证会很快---元素本身并未交换，swap只是交换了两个容器的内部数据结构。
除array外，swap不对任何元素进行拷贝、删除或插入操作，因此可以保证在常数时间内完成。
元素不会被移动的事实意味着，除string外，指向容器的迭代器、引用和指针在swap操作之后都不会失效。它们仍指向swap操作之前所指向的那些元素。但是，在swap之后，这些
元素已经属于不同的容器了。例如，假定iter在swap之前指向svec1[3]的string，那么在swap之后它指向svec2[3]的元素。与其他容器不同，对一个string调用swap会导致迭代
器、引用和指针失效。
与其他容器不同，swap两个array会真正交换它们的元素。因此，交换两个array所需的时间与array中元素的数目成正比。
对于array，在swap操作之后，指针、引用和迭代器所绑定的元素保持不变，但元素值已经与另一个array中对应元素的值进行了交换。
在新标准中，容器既提供成员函数版本的swap，也提供非成员版本的swap。而早期标准库版本只提供成员函数版本的swap。非成员版本的swap在泛型编程中是非常重要的。统一
使用非成员版本的swap是一个好习惯。

交换操作:
除了定义拷贝控制成员，管理资源的类通常还定义一个名为swap的函数。对于那些与重排元素顺序的算法一起使用的类，定义swap是非常重要的。这类算法在需要交换两个元素时
会调用swap。
如果一个类定义了自己的swap，那么算法将使用类自定义版本。否则，算法将使用标准库定义的swap。
为了交换两个对象我们需要进行一次拷贝和两次赋值。
HasPtr temp = v1;					//make a temporary copy of the value of v1
v1 = v2;								//assign the value of v2 to v1
v2 = temp;								//assign the saved value of v1 to v2
这段代码将原来v1中的string拷贝了两次，第一次是HasPtr的拷贝构造函数将v1拷贝给temp，第二次是赋值运算符将temp赋予v2。将v2赋予v1的语句拷贝了原来v2中的string。
理论上，这些内存分配都是不必要的。我们更希望swap交换指针，而不是分配string的新副本。即，我们希望这样交换两个HasPtr：
string *temp = v1.ps;				//make a temporary copy of the pointer in v1.ps
v1.ps = v2.ps;						//assign the pointer in v2.ps to v1.ps
v2.ps = temp;							//assign the saved pointer in v1.ps to v2.ps

编写我们自己的swap函数：
class HasPtr
{
	friend void swap(HasPtr&, HasPtr&);
	//other members as in § 13.2.1 (p. 511)
};
inline void swap(HasPtr &lhs, HasPtr &rhs)
{
	using std::swap;
	swap(lhs.ps, rhs.ps);			//swap the pointers, not the string data
	swap(lhs.i, rhs.i);				//swap the int members
}
我们首先将swap定义为friend，以便能访问HasPtr的(private的)数据成员。由于swap的存在就是为了优化代码，我们将其声明为inline函数。swap的函数体对给定对象的每个数据
成员调用swap。
与拷贝控制成员不同，swap并不是必要的。但是，对于分配了资源的类，定义swap可能是一种很重要的优化手段。

swap函数应该调用swap，而不是std::swap
此代码中有一个很重要的微妙之处：虽然这一点在这个特殊的例子中并不重要，但在一般情况下它非常重要---swap函数中调用的swap不是std::swap。在本例中，数据成员是内置
类型的，而内置类型是没有特定版本的swap的，所以在本例中，对swap的调用会调用标准库std::swap。但是，如果一个类的成员有自己类型特定的swap函数，调用std::swap就
是错误的了。例如，假定我们有另一个命名为Foo的类，它有一个类型为HasPtr的成员h。如果我们未定义Foo版本的swap，那么就会使用标准库版本的swap。如上面所见标准库
swap对HasPtr管理的string进行了不必要的拷贝。
我们可以为Foo编写一个swap函数，来避免这些拷贝。但是，如果这样编写Foo版本的swap：
void swap(Foo& lhs,Foo& rhs)
{
	//错误，这个函数使用了标准库版本的swap，而不是HasPtr版本
	std::swap(lhs.h,rhs.h);
	//交换类型Foo的其他成员
}
此编码会编译通过，且正常运行。但是，使用此版本与简单使用默认版本的swap并没有任何性能差异。问题在于我们显式地调用了标准库版本的swap。但是，我们不希望使用std中的
版本，我们希望调用为HasPtr对象定义的版本。
正确的swap函数如下所示：
void swap(Foo& lhs,Foo& rhs)
{
	using std::swap;
	swap(lhs.h,rhs.h);					//使用HasPtr版本的swap
	//交换类型Foo的其他成员
}
每个swap调用应该都是未加限定的。即，每个调用都应该是swap，而不是std::swap。如果存在类型特定的swap版本，其匹配程度会优于std中定义的版本。因此，如果存在类型
特定的swap版本，swap调用会与之匹配。如果不存在类型特定的版本，则会使用std中的版本(假定作用域中有using声明)。

在赋值运算符中使用swap
定义swap的类通常用swap来定义它们的赋值运算符。这些运算符使用了一种名为拷贝并交换(copy and swap)的技术。这种技术将左侧运算对象与右侧运算对象的一个副本进行交
换：
//注意rhs是按值传递的，意味着HasPtr的拷贝构造函数将右侧运算对象中的string拷贝到rhs。
HasPtr& HasPtr::operator=(HasPtr rhs)
{
	//交换左侧运算对象和局部变量rhs的内容
	swap(*this, rhs);						//rhs现在指向本对象曾经使用的内存
	return *this;							//rhs被销毁，从而delete了rhs中的指针。
}
在这个版本的赋值运算符中，参数并不是一个引用，我们将右侧运算对象以传值方式传递给了赋值运算符。因此，rhs是右侧运算对象的一个副本。参数传递时拷贝HasPtr的操作会分配
该对象的string的一个新副本。
在赋值运算符的函数体中，我们调用swap来交换rhs和*this中的数据成员。这个调用将左侧运算对象中原来保存的指针存入rhs中，并将rhs中原来的指针存入*this中。因此，在
swap调用之后，*this中的指针成员将指向新分配的string---右侧运算对象中string的一个副本。
当赋值运算符结束时，rhs被销毁，HasPtr的析构函数将执行。此析构函数delete rhs现在指向的内存，即，释放掉左侧运算对象中原来的内存。
这个技术的有趣之处是它自动处理了自赋值情况且天然就是异常安全的。它通过在改变左侧运算对象之前拷贝右侧运算对象保证了自赋值的正确，这与我们在原来的赋值运算符中使用
的方法是一致的。它保证异常安全的方法也与原来的赋值运算符实现一样。代码中唯一可能抛出异常的是拷贝构造函数中的new表达式。如果真发生了异常，它也会在我们改变左侧运
算对象之前发生。
使用拷贝和交换的赋值运算符自动就是异常安全的，且能正确处理自赋值。

对象移动:
新标准的一个最主要的特性是可以移动而非拷贝对象的能力。
很多情况下都会发生对象拷贝。在其中某些情况下，对象拷贝后就立即被销毁了。在这些情况下，移动而非拷贝对象会大幅度提升性能。
在旧版本的标准库中，容器中所保存的类必须是可拷贝的。但在新标准中，我们可以用容器保存不可拷贝的类型，只要它们能被移动即可。

标准库容器、string和shared_ptr类既支持移动也支持拷贝。IO类和unique_ptr类可以移动但不能拷贝。

右值引用:
为了支持移动操作，新标准引入了一种新的引用类型---右值引用(rvalue reference)。所谓右值引用就是必须绑定到右值的引用。我们通过&&而不是&来获得
右值引用。右值引用有一个重要的性质---只能绑定到一个将要销毁的对象。因此，我们可以自由地将一个右值引用的资源“移动”到另一个对象中。
左值和右值是表达式的属性。一些表达式生成或要求左值，而另外一些则生成或要求右值。一般而言，一个左值表达式表示的是一个对象的身份，而一个右值表达
式表示的是对象的值。
类似任何引用，一个右值引用也不过是某个对象的另一个名字而已。对于常规引用(左值引用)，我们不能将其绑定到要求转换的表达式、字面常量或是返回右值
的表达式。右值引用有着完全相反的绑定特性：我们可以将一个右值引用绑定到这类表达式上，但不能将一个右值引用直接绑定到一个左值上：
int i = 42;
int &r = i;						//正确，r引用i
int &&rr = i;					//错误，不能将一个右值引用绑定到一个左值上
int &r2 = i * 42;				//错误，i * 42是一个右值
const int &r3 = i * 42;		//正确，我们可以将一个const的引用绑定到一个右值上
int &&rr2 = i * 42;			//正确，将rr2绑定到乘法结果上
返回左值引用的函数，连同赋值、下标、解引用和前置递增/递减运算符，都是返回左值的表达式的例子。
返回非引用类型的函数、连同算术、关系、位以及后置递增/递减运算符，都生成右值。我们不能将一个左值引用绑定到这类表达式上，但我们可以将一个const的左
值引用或者一个右值引用绑定到这类表达式上。

左值持久；右值短暂
考察左值和右值表达式的列表，两者相互区别之处就很明显了：左值有持久的状态，而右值要么是字面常量，要么是在表达式求值过程中创建的临时对象。
由于右值引用只能绑定到临时对象，我们得知：
1：所引用的对象将要被销毁。
2：该对象没有其他用户。
这两个特性意味着：使用右值引用的代码可以自由地接管所引用的对象的资源。
右值引用指向将要被销毁的对象。因此，我们可以从绑定到右值引用的对象“窃取”状态。

变量是左值
变量可以看做只有一个运算对象而没有运算符的表达式，虽然我们很少这样看待变量。类似其他任何表达式，变量表达式也有左值/右值属性。变量表达式都是左值。带
来的结果就是，我们不能将一个右值引用绑定到一个右值引用类型的变量上，这有些令人惊讶：
int &&rr1 = 42;				//正确，字面常量是右值
int &&rr2 = rr1;				//错误，表达式rr1是左值。
其实有了右值表示临时对象这一观察结果，变量是左值这一特性并不令人惊讶。毕竟，变量是持久的，直至离开作用域时才被销毁。
记住：变量是左值，因此我们不能将一个右值引用直接绑定到一个变量上，即使这个变量是右值引用类型也不行。

标准库move函数
虽然不能将一个右值引用直接绑定到一个左值上，但我们可以显式地将一个左值转换为对应的右值引用类型。我们还可以通过调用一个名为move的新标准库函数来获
得绑定到左值上的右值引用，此函数定义在头文件utility中。
int &&rr3 = std::move(rr1);
move调用告诉编译器，我们有一个左值，但我们希望像一个右值一样处理它。我们必须认识到，调用move就意味着承诺：除了对rr1赋值或销毁它外，我们将不再使
用它。在调用move之后，我们不能对移后源对象的值做任何假设。
记住：我们可以销毁一个移后源对象，也可以赋予它新值，但不能使用一个移后源对象的值。
与大多数标准库名字的使用不同，对move我们不提供using声明。我们直接调用std::move而不是move。这样做可以避免潜在的名字冲突。

移动构造函数和移动赋值运算符
类似string类(及其他标准库类)，如果我们自己的类也同时支持移动和拷贝，那么也能从中受益。为了让我们自己的类型支持移动操作，需要为其定义移动构造函数和移
动赋值运算符。这两个成员类似对应的拷贝操作，但它们从给定对象“窃取”资源而不是拷贝资源。

类似拷贝构造函数，移动构造函数的第一个参数是该类类型的一个引用。不同于拷贝构造函数的是，这个引用参数在移动构造函数中是一个右值引用。与拷贝构造函数一
样，任何额外的参数都必须有默认实参。

除了完成资源移动，移动构造函数还必须确保移后源对象处于这样一个状态---销毁它是无害的。特别是，一旦资源完成移动，源对象必须不再指向被移动的资源---这些
资源的所有权已经归属新创建的对象。
StrVec::StrVec(StrVec &&s) noexcept :										//移动操作不应该抛出任何异常
elements(s.elements), first_free(s.first_free),cap(s.cap)				//成员初始化器接管s中的资源
{
	s.elements = s.first_free = s.cap = nullptr;								//令s进入这样的状态---对其运行析构函数是安全的。
}

noexcept，它通知标准库我们的构造函数不抛出任何异常。在一个构造函数中，noexcept出现在参数列表和初始化列表开始的冒号之间。
我们必须在类头文件的声明中和定义中(如果定义在类外的话)都指定noexcept。
不抛出异常的移动构造函数和移动赋值运算符必须标记为noexcept。

与拷贝构造函数不同，移动构造函数不分配任何新内存。它接管给定的StrVec中的内存。在接管内存之后，它将给定对象中的指针都置为nullptr。这样就完成了从给定对象的
移动操作，此对象将继续存在。最终，移后源对象会被销毁，意味着将在其上运行析构函数。StrVec的析构函数在first_free上调用deallocate。如果我们忘记了改变
s.first_free，则销毁移后源对象就会释放掉我们刚刚移动的内存。

移动操作、标准库容器和异常
由于移动操作“窃取”资源，它通常不分配任何资源。因此，移动操作通常不会抛出任何异常。当编写一个不抛出异常的移动操作时，我们应该将此事通知标准库。我们将看到，除
非标准库知道我们的移动构造函数不会抛出异常，否则它会认为移动我们的类对象时可能会抛出异常，并且为了处理这种可能性而做一些额外的工作。
我们需要指出一个移动操作不抛出异常，这是因为两个相互关联的事实：首先，虽然移动操作通常不抛出异常，但抛出异常也是允许的。其次，标准库容器能对异常发生时其自身
的行为提供保障。例如，vector保证，如果我们调用push_back时发生异常，vector自身不会发生改变。现在让我们思考push_back内部发生了什么。类似对应的StrVec操作，
对一个vector调用push_back可能要求为vector重新分配内存空间。当重新分配vector的内存时，vector将元素从旧空间移动到新内存中。
移动一个对象通常会改变它的值。如果重新分配过程使用了移动构造函数，且在移动了部分而不是全部元素后抛出了一个异常，就会产生问题。旧空间中的移动源元素已经被改变
了，而新空间中未构造的元素可能尚不存在。在此情况下，vector将不能满足自身保持不变的要求。
另一方面，如果vector使用了拷贝构造函数且发生了异常，它可以很容易地满足要求。在此情况下，当在新内存中构造元素时，旧元素保持不变。如果此时发生了异常，vector可
以释放新分配的(但还未成功构造的)内存并返回。vector原有的元素仍然存在。
为了避免这种潜在问题，除非vector知道元素类型的移动构造函数不会抛出异常，否则在重新分配内存的过程中，它就必须使用拷贝构造函数而不是移动构造函数。如果希望在
vector重新分配内存这类情况下对我们自定义类型的对象进行移动而不是拷贝，就必须显式地告诉标准库我们的移动构造函数可以安全使用。我们通过将移动构造函数(及移动赋
值运算符)标记为noexcept来做到这一点。

移动赋值运算符
移动赋值运算符执行与析构函数和移动构造函数相同的工作。与移动构造函数一样，如果我们的移动赋值运算符不抛出任何异常，我们就应该将它标记为noexcept。类似拷贝赋
值运算符，移动赋值运算符必须正确处理自赋值：
StrVec &StrVec::operator=(StrVec &&rhs) noexcept
{
	//直接检测自赋值
	if (this != &rhs)
	{
		free();																				//释放已有元素
		elements = rhs.elements;														//从rhs接管资源
		first_free = rhs.first_free;
		cap = rhs.cap;
		
		rhs.elements = rhs.first_free = rhs.cap = nullptr;					//将rhs置于可解析状态
	}

	return *this;
}
在此例中，我们直接检查this指针与rhs的地址是否相同。如果相同，右侧和左侧运算对象指向相同的对象，我们不需要做任何事情。否则，我们释放左侧运算对象所使用的内存，并
接管给定对象的内存。与移动构造函数一样，我们将rhs中的指针置为nullptr。
我们费心地去检查自赋值情况看起来有些奇怪。毕竟，移动赋值运算符需要右侧运算对象的一个右值。我们进行检查的原因是此右值可能是move调用的返回结果。与其他任何赋值运
算符一样，关键点是我们不能在使用右侧运算对象的资源之前就释放左侧运算对象的资源(可能是相同的资源)。

移后源对象必须可析构
从一个对象移动数据并不会销毁此对象，但有时在移动操作完成后，源对象被销毁。因此，当我们编写一个移动操作时，必须确保移后源对象进入一个可析构的状态。我们的StrVec的
移动操作满足这一要求，这是通过将移后源对象的指针成员设置为nullptr来实现的。
除了将移后源对象置为析构安全的状态之外，移动操作还必须保证对象仍然是有效的。一般来说，对象有效就是指可以安全地为其赋予新值或者可以安全地使用而不依赖其当前值。另
一方面，移动操作对移后源对象中留下的值没有任何要求。因此，我们的程序不应该依赖于移后源对象中的数据。
例如，当我们从一个标准库string或容器对象移动数据时，我们知道移后源对象仍然保持有效。因此，我们可以对它执行诸如empty或size这些操作。但是，我们不知道将会得到什么
结果。我们可能期望一个移后源对象是空的，但这并没有保证。
我们的StrVec类的移动操作将移后源对象置于与默认初始化的对象相同的状态。因此，我们可以继续对移后源对象执行所有的StrVec操作，与任何其他默认初始化的对象一样。
在移动操作后，移后源对象必须保持有效的、可析构的状态，但是用户不能对其值进行任何假设。

合成的移动操作
与处理拷贝构造函数和拷贝赋值运算符一样，编译器也会合成移动构造函数和移动赋值运算符。但是，合成移动操作的条件与合成拷贝操作的条件大不相同。

如果我们不声明自己的拷贝构造函数或拷贝赋值运算符，编译器总会为我们合成这些操作。拷贝操作要么被定义为逐成员拷贝，要么被定义为对象赋值，要么被定义为删除的函数。

与拷贝操作不同，编译器根本不会为某些类合成移动操作。特别是，如果一个类定义了自己的拷贝构造函数、拷贝赋值运算符或者析构函数，编译器就不会为它合成移动构造函数和移
动赋值运算符了。因此，某些类就没有移动构造函数或移动赋值运算符。如果一个类没有移动操作，通过正常的函数匹配，类会使用对应的拷贝操作来代替移动操作。

只有当一个类没有定义任何自己版本的拷贝控制成员，且类的每个非static数据成员都可以移动时，编译器才会为它合成移动构造函数或移动赋值运算符。编译器可以移动内置类型的
成员。如果一个成员是类类型，且该类有对应的移动操作，编译器也能移动这个成员：
//编译器会为X和hasX合成移动操作
struct X
{
	int i;											//内置类型可以移动
	std::string s;								//string定义了自己的移动操作
};
struct hasX
{
	X mem;										//X有合成的移动操作
};
X x, x2 = std::move(x);					//使用合成的移动构造函数
hasX hx, hx2 = std::move(hx);			//使用合成的移动构造函数

与拷贝操作不同，移动操作永远不会隐式定义为删除的函数。但是，如果我们显式地要求编译器生成=default的移动操作，且编译器不能移动所有成员，则编译器会将移动操作定义为
删除的函数。除了一个重要例外，什么时候将合成的移动操作定义为删除的函数遵循与定义删除的合成拷贝操作类似的原则：
1：与拷贝构造函数不同，移动构造函数被定义为删除的函数的条件是：有类成员定义了自己的拷贝构造函数且未定义移动构造函数，或者是有类成员未定义自己的拷贝构造函数且编译
器不能为其合成移动构造函数。移动赋值运算符的情况类似。
2：如果有类成员的移动构造函数或移动赋值运算符被定义为删除的或是不能访问的，则类的移动构造函数或移动赋值运算符被定义为删除的。
3：类似拷贝构造函数，如果类的析构函数被定义为删除的或不可访问的，则类的移动构造函数被定义为删除的。
4：类似拷贝赋值运算符，如果有类成员是const的或是引用，则类的移动赋值运算符被定义为删除的。

例如，假定Y是一个类，它定义了自己的拷贝构造函数但是未定义自己的移动构造函数：
struct hasY
{
	hasY() = default;
	hasY(hasY&&) = default;
	Y mem;										//hasY将有一个删除的移动构造函数
};
hasY hy, hy2 = std::move(hy);			//错误，移动构造函数时删除的
编译器可以拷贝类型为Y的对象，但不能移动它们。类hasY显式地要求一个移动构造函数，但编译器无法为其生成。因此，hasY会有一个删除的移动构造函数。如果hasY忽略了移动
构造函数的声明，则编译器根本不能为它合成一个。如果移动操作可能被定义为删除的函数，编译器就不会合成它们。

移动操作和合成的拷贝控制成员间还有最后一个相互作用关系：一个类是否定义了自己的移动操作对拷贝操作如何合成有影响。如果类定义了一个移动构造函数和/或一个移动赋值运
算符，则该类的合成拷贝构造函数和拷贝赋值运算符会被定义为删除的。

记住：定义了一个移动构造函数或移动赋值运算符的类必须也定义自己的拷贝操作。否则，这些成员默认地被定义为删除的。

移动右值，拷贝左值
如果一个类既有移动构造函数，也有拷贝构造函数，编译器使用普通的函数匹配规则来确定使用哪个构造函数。赋值操作的情况类似。
例如，在StrVec类中，拷贝构造函数接受一个const StrVec的引用。因此，它可以用于任何可以转换为StrVec的类型。而移动构造函数接受一个StrVec&&，因此只能用于实参是
(非static)右值的情形：
StrVec v1, v2;
v1 = v2;									//v2是左值；使用拷贝赋值
StrVec getVec(istream &);				//getVec返回一个右值
v2 = getVec(cin);						//getVec(cin)是一个右值；使用移动赋值
在第一个赋值中，我们将v2传递给赋值运算符。v2的类型是StrVec，表达式v2是一个左值。因此移动版本的赋值运算符是不可行的，因为我们不能隐式地将一个右值引用绑定到一个
左值。因此，这个赋值语句使用拷贝赋值运算符。
在第二个赋值中，我们赋予v2的是getVec调用的结果。此表达式是一个右值。在此情况下，两个赋值运算符都是可行的---将getVec的结果绑定到两个运算符的参数都是允许的。调用
拷贝赋值运算符需要进行一次到const的转换，而StrVec&&则是精确匹配。因此，第二个赋值会使用移动赋值运算符。

如果没有移动构造函数，右值也被拷贝
如果一个类有一个拷贝构造函数但未定义移动构造函数，会发生什么呢？在此情况下，编译器不会合成移动构造函数，这意味着此类将有拷贝构造函数但不会有移动构造函数。如果一
个类没有移动构造函数，函数匹配规则保证该类型的对象会被拷贝，即使我们试图通过调用move来移动它们时也是如此：
class Foo
{
public:
	Foo() = default;
	Foo(const Foo&);					//拷贝构造函数
	//其他成员定义，但Foo未定义移动构造函数
};
Foo x;
Foo y(x);									//拷贝构造函数；x是一个左值
Foo z(std::move(x));					//拷贝构造函数，因为未定义移动构造函数
在对z进行初始化时，我们调用了move(x)，它返回一个绑定到x的Foo&&。Foo的拷贝构造函数是可行的，因为我们可以将一个Foo&&转换为一个const Foo&。因此，z的初始化将
使用Foo的拷贝构造函数。

值得注意的是，用拷贝构造函数代替移动构造函数几乎肯定是安全的(赋值运算符的情况类似)。一般情况下，拷贝构造函数满足对应的移动构造函数的要求：它会拷贝给定对象，并将
原对象置于有效状态。实际上，拷贝构造函数甚至都不会改变原对象的值。

如果一个类有一个可用的拷贝构造函数而没有移动构造函数，则其对象是通过拷贝构造函数来“移动”的。拷贝赋值运算符和移动赋值运算符的情况类似。

拷贝并交换赋值运算符和移动操作
class HasPtr
{
public:
	//添加的移动构造函数
	HasPtr(HasPtr &&p) noexcept :
	ps(p.ps),
	i(p.i)
	{
		p.ps = 0;
	}
	//赋值运算符既是移动赋值运算符，也是拷贝赋值运算符
	HasPtr& operator=(HasPtr rhs)
	{
		swap(*this, rhs);
		return *this;
	}
	//其他成员的定义
};
我们为类添加了一个移动构造函数，它接管了给定实参的值。构造函数体将给定的HasPtr的指针置为0，从而确保销毁移后源对象是安全的。此函数不会抛出异常，因此我们将其标记
为noexcept。
现在上我们观察赋值运算符。此运算符有一个非引用参数，这意味着此参数要进行拷贝初始化。依赖于实参的类型，拷贝初始化要么使用拷贝构造函数，要么使用移动构造函数---左
值被拷贝，右值被移动。因此，单一的赋值运算符就实现了拷贝赋值运算符和移动赋值运算符两种功能。
例如，假定hp和hp2都是HasPtr对象：
hp = hp2; //hp2是一个左值；hp2通过拷贝构造函数来拷贝
hp = std::move(hp2); //移动构造函数移动hp2
在第一个赋值中，右侧运算对象是一个左值，因此移动构造函数是不可行的。rhs将使用拷贝构造函数来初始化。拷贝构造函数将分配一个新string，并拷贝hp2指向的string。
在第二个赋值中，我们调用std::move将一个右值引用绑定到hp2上。在此情况下，拷贝构造函数和移动构造函数都是可行的。但是，由于实参是一个右值引用，移动构造函数时精
确匹配的。移动构造函数从hp2拷贝指针，而不会分配任何内存。
不管使用的是拷贝构造函数还是移动构造函数，赋值运算符的函数体swap两个运算对象的状态。交换HasPtr会交换两个对象的指针(及int)成员。在swap之后，rhs中的指针将指向原
来左侧运算对象所拥有的string。当rhs离开其作用域时，这个string将被销毁。

三/五法则
所有五个拷贝控制成员应该看做一个整体：一般来说，如果一个类定义了任何一个拷贝操作，它就应该定义所有五个操作。如前所述，某些类必须定义拷贝构造函数、拷贝赋值运算
符和析构函数才能正确工作。这些类通常拥有一个资源，而拷贝成员必须拷贝此资源。一般来说，拷贝一个资源会导致一些额外开销。在这种拷贝并非必要的情况下，定义了移动构
造函数和移动赋值运算符的类就可以避免此问题。

移动迭代器
如果我们能调用uninitialized_copy来构造新分配的内存，将比循环更为简单。但是uninitialized_copy恰如其名：它对元素进行拷贝操作。标准库中并没有类似的函数将对象“移动”
到未构造的内存中。
新标准库中定义了一种移动迭代器(move iterator)适配器。一个移动迭代器通过改变给定迭代器的解引用运算符的行为来适配此迭代器。一般来说，一个迭代器的解引用运算符返回
一个指向元素的左值。与其他迭代器不同，移动迭代器的解引用运算符生成一个右值引用。
我们通过标准库的make_move_iterator函数将一个普通迭代器转换为一个移动迭代器。此函数接受一个迭代器参数，返回一个移动迭代器。
原迭代器的所有其他操作在移动迭代器中都照常工作。由于移动迭代器支持正常的迭代器操作，我们可以将一对移动迭代器传递给算法。特别是，可以将移动迭代器传递给
uninitialized_copy：
void StrVec::reallocate()
{
	//分配大小两倍于当前规模的内存空间
	auto newcapacity = size() ? 2 * size() : 1;
	auto first = alloc.allocate(newcapacity);
	//移动元素
	auto last = uninitialized_copy(make_move_iterator(begin()),
	make_move_iterator(end()),
	first);
	free();											//释放旧空间
	elements = first;								//更新指针
	first_free = last;
	cap = elements + newcapacity;
}
uninitialized_copy对输入序列中的每个元素调用construct来将元素“拷贝”到目的位置。此算法使用迭代器的解引用运算符从输入序列中提取元素。由于我们传递给它的是移动迭代
器，因此解引用运算符生成的是一个右值引用，这意味着construct将使用移动构造函数来构造元素。
值得注意的是，标准库不保证哪些算法使用移动迭代器，哪些不适用。由于移动一个对象可能毁掉原对象，因此你只有在确信算法在为一个元素赋值或将其传递给一个用户定义的函数
后不再访问它时，才能将移动迭代器传递给算法。

不要随意使用移动操作
由于一个移后源对象具有不确定的状态，对其调用std::move是危险的。当我们调用move时，必须绝对确认移后源对象没有其他用户。
通过在类代码中小心地使用move，可以大幅度提升性能。而如果随意在普通用户代码(与类实现代码相对)中使用移动操作，很可能导致莫名其妙的、难以查找的错误，而难以提升应
用程序性能。
在移动构造函数和移动赋值运算符这些类实现代码之外的地方，只有当你确信需要进行移动操作且移动操作是安全的，才可以使用std::move。

右值引用和成员函数:
除了构造函数和赋值运算符之外，如果一个成员函数同时提供拷贝和移动版本，它也能从中受益。这种允许移动的成员函数通常使用与拷贝/移动构造函数和赋值运算符相同的参数模
式---一个版本接受一个指向const的左值引用，第二个版本接受一个指向非const的右值引用。
例如，定义了push_back的标准库容器提供两个版本：一个版本有一个右值引用参数，而另一个版本有一个const左值引用。假定X是元素类型，那么这些容器就会定义以下两个
push_back版本：
void push_back(const X&);					//拷贝：绑定到任意类型的X
void push_back(X&&);							//移动：只能绑定到类型X的可修改的右值(非constant右值)
我们可以将能转换为类型X的任何对象传递给第一个版本的push_back。此版本从其参数拷贝数据。对于第二个版本，我们只可以传递给它非const的右值。此版本对于非const的右值
是精确匹配的，因此当我们传递一个可修改的右值时，编译器会选择运行这个版本。此版本会从其参数窃取数据。
一般来说，我们不需要为函数操作定义接受一个const X&&或是一个(普通的)X&参数的版本。当我们希望从实参“窃取”数据时，通常传递一个右值引用。为了达到这一目的，实参不能
是const的。类似的，从一个对象进行拷贝的操作不应该改变该对象。因此，通常不需要定义一个接受一个(普通的)X&参数的版本。
区分移动和拷贝的重载函数通常有一个版本接受一个const T&，而另一个版本接受一个T&&。

作为一个跟具体的例子，我们将为StrVec类定义另一个版本的push_back():
class StrVec
{
public:
	void push_back(const std::string&);							//copy the element
	void push_back(std::string&&);									//move the element
};
void StrVec::push_back(const string& s)
{
	chk_n_alloc();														//ensure that there is room for another element
	//construct a copy of s in the element to which first_free points
	alloc.construct(first_free++, s);
}
void StrVec::push_back(string &&s)
{
	chk_n_alloc();														//reallocates the StrVec if necessary
	alloc.construct(first_free++,std::move(s));
}
这两个成员几乎是相同的。差别在于右值引用版本调用move来将其参数传递给construct。如前所述，construct函数使用其第二个和随后的实参的类型来确定使用哪个构造函数。由
于move返回一个右值引用，传递给construct的实参类型是string&&。因此，会使用string的移动构造函数来构造新元素。
当我们调用push_back时，实参类型决定了新元素是拷贝还是移动到容器中：
StrVec vec;																	//empty StrVec
string s = "some string or another";
vec.push_back(s);														//calls push_back(const string&)
vec.push_back("done");												//calls push_back(string&&)
这些调用的差别在于实参是一个左值还是一个右值(从“done”创建的临时string)，具体调用哪个版本据此来决定。

右值和左值引用成员函数
通常，我们在一个对象上调用成员函数，而不管该对象是一个左值还是一个右值。例如：
string s1 = "a value", s2 = "another";
auto n = (s1 + s2).find('a');
此例中，我们在一个string右值上调用find成员，该string右值是通过连接两个string而得到的。有时，右值的使用方式可能令人惊讶：
s1  + s2 = "wow!";
此处我们对两个string的链接结果---一个右值，进行了赋值。

在旧标准中，我们没有办法阻止这种使用方式。为了维持向后兼容性，新标准库类仍然允许向右值赋值。但是，我们可能希望在自己的类中阻止这种用法。在此情况下，我们希望强制
左侧运算对象(即，this指向的对象)是一个左值。

我们指出this的左值/右值属性的方式与定义const成员函数相同，即，在参数列表后放置一个引用限定符(reference qualifier)：
class Foo
{
public:
	Foo &operator=(const Foo&) &;						//只能向可修改的左值赋值
	//Foo的其他参数
};
Foo &Foo::operator=(const Foo &rhs) &
{
	//执行将rhs赋予本对象所需的工作
	return *this;
}
引用限定符可以是&或&&，分别指出this可以指向一个左值或右值。类似const限定符，引用限定符只能用于(非static)成员函数，且必须同时出现在函数的声明和定义中。
对于&限定的函数，我们只能将它用于左值；对于&&限定的函数，只能用于右值：
Foo &retFoo();													//返回一个引用；retFoo调用是一个左值
Foo retVal();														//返回一个值；retVal调用是一个右值
Foo i, j;																//i和j是左值
i = j;																	//正确，i是左值
retFoo() = j;														//正确，retFoo()返回一个左值
retVal() = j;														//错误，retVal()返回一个右值
i = retVal();														//正确，我们可以将一个右值作为赋值操作的右侧运算对象
一个函数可以同时用const和引用限定。在此情况下，引用限定符必须跟随在const限定符之后：
class Foo
{
public:
	Foo someMem() & const;									//错误，const限定符必须在前
	Foo anotherMem() const &;								//正确
};

重载和引用函数
就像一个成员函数可以根据是否有const来区分其重载版本一样，引用限定符也可以区分重载版本。而且，我们可以综合引用限定符和const来区分一个成员函数的重载版本。例如，我
们将为Foo定义一个名为data的vector成员和一个名为sorted的成员函数，sorted返回一个Foo对象的副本，其中vector已被排序：
class Foo
{
public:
	Foo sorted() &&;												//可用于改变的右值
	Foo sorted() const &;										//可用于任何类型的Foo
	//Foo的其他成员的定义
private:
	vector<int> data;
};
//本对象为右值，因此可以原址排序
Foo Foo::sorted() &&
{
	sort(data.begin(), data.end());
	return *this;
}
//本对象是const或是一个左值，哪种情况我们都不能对其进行原址排序
Foo Foo::sorted() const &
{
	Foo ret(*this);												//拷贝一个副本
	sort(ret.data.begin(), ret.data.end());				//排序副本
	return ret;														//返回副本
}
当我们对一个右值执行sorted时，它可以安全地直接对data成员进行排序。对象是一个右值，意味着没有其他用户，因此我们可以改变对象。当对一个const右值或一个左值执行
sorted时，我们不能改变对象，因此就需要在排序前拷贝data。
编译器会根据调用sorted的对象的左值/右值属性来确定使用哪个sorted版本：
retVal().sorted();													//retVal()是一个右值,调用Foo::sorted() &&
retFoo().sorted();												//retFoo()是一个左值,调用Foo::sorted() const &
当我们没有定义const成员函数时，可以定义两个版本，唯一的差别是一个版本有const限定而另一个没有。引用限定的函数则不一样。如果我们定义两个或两个以上具有相同名字和
相同参数列表的成员函数，就必须对所有函数都加上引用限定符，或者所有都不加：
class Foo
{
public:
	Foo sorted() &&;
	Foo sorted() const;											//错误，必须加上引用限定符
	//Comp是函数类型的类型别名
	//此函数类型可以用来比较int值
	using Comp = bool(const int&, const int&);
	Foo sorted(Comp*);										//正确，不同的参数列表
	Foo sorted(Comp*) const;								//正确，两个版本都没有引用限定符
};
本例中声明了一个没有参数的const版本的sorted，此声明时错误的。因为Foo类中还有一个无参的sorted版本，它有一个引用限定符，因此const版本也必须有引用限定符。另一方
面，接受一个比较操作指针的sorted版本是没问题的，因为两个函数都没有引用限定符。
如果一个成员函数有引用限定符，则具有相同参数列表的所有版本都必须有引用限定符。

模板实参推断和引用:
为了理解如何从函数调用进行类型推断，考虑下面的例子：
template <typename T> void f(T &p);
其中函数参数p是一个模板类型参数T的引用，非常重要的是记住两点：编译器会应用正常的引用绑定规则：const是底层的，不是顶层的。

从左值引用函数参数推断类型
当一个函数参数是模板类型参数的一个普通(左值)引用时(即，形如T&)，绑定规则告诉我们，只能传递给它一个左值(如，一个变量或一个返回引用类型的表达式)。实参可以是const
类型，也可以不是。如果实参是const的，则T将被推断为const类型：
template <typename T> void f1(T&);						//实参必须是一个左值
//对f1的调用使用实参所引用的类型作为模板参数类型
f1(i);																	//i是一个int，模板参数类型T是int
f1(ci);																//ci是一个const int，模板参数T是const int
f1(5);																//错误，传递给一个&参数的实参必须是一个左值

如果一个函数参数的类型是const T&，正常的绑定规则告诉我们可以传递给它任何类型的实参---一个对象(const或非const)、一个临时对象或是一个字面常量值。当函数参数本身是
const时，T的类型推断的结果不会是一个const类型。const已经是函数参数类型的一部分；因此，它不会也是模板参数类型的一部分：
template <typename T> void f2(const T&);				//可以接受一个右值
//f2中的参数是const &；实参中的const是无关的
//在每个调用中，f2的函数参数都被推断为const int&
f2(i);																	//i是一个int；模板参数T是int
f2(ci);																//ci是一个const int，但模板参数T是int
f2(5);																//一个const &参数可以绑定到一个右值，T是int

从右值引用函数参数推断类型
当一个函数参数是一个右值引用(即，形如T&&)时，正常绑定规则告诉我们可以传递给它一个右值。当我们这样做时，类型推断过程类似普通左值引用函数参数的推断过程。推断出的
T的类型是该右值实参的类型：
template <typename T> void f3(T&&);
f3(42);																//实参是一个int类型的右值，模板参数T是int

引用折叠和右值引用参数
假定i是一个int对象，我们可能认为像f3(i)这样的调用是不合法的。毕竟，i是一个左值，而通常我们不能将一个右值引用绑定到一个左值上。但是C++语言在正常绑定规则之外定义了
两个例外规则，允许这种绑定。这两个例外规则是move这种标准库设施正确工作的基础。
第一个例外规则影响右值引用参数的推断如何进行。当我们将一个左值(如i)传递给函数的右值引用参数，且此右值引用指向模板类型参数(如T&&)时，编译器推断模板类型参数为实参
的左值引用类型。因此，当我们调用f3(i)时，编译器推断T的类型为int&，而非int。
T被推断为int&看起来好像意味着f3的函数参数应该是一个类型int&的右值引用。通常，我们不能(直接)定义一个引用的引用。但是，通过类型别名或通过模板类型参数间接定义是可以
的。
在这种情况下，我们可以使用第二个例外绑定规则：如果我们间接创建一个引用的引用，则这些引用形成了“折叠”。在所有情况下(除了一个例外)，引用会折叠成一个普通的左值引用
类型。
在新标准中，折叠规则扩展到右值引用。只在一种特殊情况下引用会折叠成右值引用：右值引用的右值引用。即，对于一个给定类型X：
X& &、X& &&和X&& &都折叠成类型X&
类型X&& &&折叠成X&&
记住：引用折叠只能应用于间接创建的引用的引用，如类型别名或模板参数。
如果将引用折叠规则和右值引用的特殊类型推断规则组合在一起，则意味着我们可以对一个左值调用f3.当我们将一个左值传递给f3的(右值引用)函数参数时，编译器推断T为一个左值
引用类型：
f3(i);																	//实参时一个左值，模板参数T是int&
f3(ci);																//实参是一个左值，模板参数T是一个const int&
当一个模板参数T被推断为引用类型时，折叠规则告诉我们函数参数T&&折叠为一个左值引用类型。例如，f3(i)的实例化结果可能像下面这样：
//无效代码，只是用于演示目的
void f3<int&>(int& &&);										//当T是int&时，函数参数为int& &&
f3的函数参数是T&&且T是int&，因此T&&是int& &&，会折叠成int&。因此，即使f3的函数参数形式是一个右值引用(即，T&&)，此调用也会用一个左值引用类型(即，int&)实例化
f3：
void f3<int&>(int&);											//当T是int&时，函数参数折叠为int&
这两个规则导致了两个重要结果：
1：如果一个函数参数是一个指向模板类型参数的右值引用(如，T&&)，则它可以被绑定到一个左值；且
2：如果实参是一个左值，则推断出的模板实参类型将是一个左值引用，且函数参数将被实例化为一个(普通)左值引用参数(T&)
另外值得注意的是，这两个规则暗示，我们可以将任意类型的实参传递给T&&类型的函数参数。对于这种类型的参数，(显然)可以传递给它右值，而如我们刚刚看到的，也可以传递给
它左值。
记住：如果一个函数参数是指向模板参数类型的右值引用(如，T&&)，则可以传递给它任意类型的实参。如果将一个左值传递给这样的参数，则函数参数被实例化为一个普通的左值引
用(T&)。

编写接受右值引用参数的模板函数
模板参数可以推断为一个引用类型，这一特性对模板内的代码可能有令人惊讶的影响：
template <typename T> void f3(T&& val)
{
	T t = val;														//拷贝还是绑定一个引用？
	t = fcn(t);														//赋值只改变t还是即改变t又改变val？
	if (val == t)													//若T是引用类型，则一直为true
	{
	}
}
当我们对一个右值调用f3时，例如字面常量42，T为int。在此情况下，局部变量t的类型为int，且通过拷贝参数val的值被初始化。当我们对t赋值时，参数val保持不变。
另一方面，当我们对一个左值i调用f3时，则T为int&。当我们定义并初始化局部变量t时，赋予它类型int&。因此，对t的初始化将其绑定到val。当我们对t赋值时，也同时改变了val的
值。在f3的这个实例化版本中，if判断永远得到true。
当代码中涉及的类型可能是普通(非引用)类型，也可能是引用类型时，编写正确的代码就变得异常困难(虽然remove_reference这样的类型转换可能会有帮助)。
在实际中，右值引用通常用于两种情况：模板转发其实参或模板被重载。
目前应该注意的是，使用右值引用的函数模板通常用来进行重载：
template <typename T> void f(T&&);						//绑定到非const右值
template <typename T> void f(const T&);				//左值和const右值
与非模板函数一样，第一个版本将绑定到可修改的右值，而第二个版本将绑定到左值或const右值。

理解std::move：
标准库move函数是使用右值引用的模板的一个很好的例子。
虽然不能直接将一个右值引用绑定到一个左值上，但可以用move获得一个绑定到左值上的右值引用。由于move本质上可以接受任何类型的实参，因此我们不会惊讶于它是一个函数
模板。

std::move是如何定义的
标准库是这样定义move的：
//在返回类型和类型转换中也要用到typename
template <typename T>
typename remove_reference<T>::type&& move(T&& t)
{
	return static_cast<typename remove_reference<T>::type&&>(t);
}
这段代码很短，但其中有些微妙之处。首先，move的函数参数T&&是一个指向模板类型参数的右值引用。通过引用折叠，此参数可以与任何类型的实参匹配。特别是，我们既可以传
递给move一个左值，也可以传递给它一个右值：
string s1("hi!"), s2;
s2 = std::move(string("bye!"));							//正确，从一个右值移动数据
s2 = std::move(s1);											//正确，但在赋值之后，s1的值是不确定的

std::move是如何工作的
在第一个赋值中，传递给move的实参是string的构造函数的右值结果---string("bye!")。当向一个右值引用函数参数传递一个右值时，由实参推断出的类型为被引用的类型。因此，
在std::move(string("bye!"))中：
1：推断出的T的类型为string。
2：因此，remove_reference用string进行实例化。
3：remove_reference<string>的type成员是string。
4：move的返回类型是string&&。
5：move的函数参数t的类型为string&&。
因此，这个调用实例化move<string>，即函数
string&& move(string &&t)
函数体返回static_cast<string&&>(t)。t的类型已经是string&&，于是类型转换什么都不做。因此，此调用的结果就是它所接受的右值引用。
现在考虑第二个赋值，它调用了std::move()。在此调用中，传递给move的实参是一个左值，这样：
1：推断出的T的类型为string&(string的引用，而非普通string)。
2：因此，remove_reference用string&进行实例化。
3：remove_reference<string&>的type成员是string。
4：move的返回类型仍是string&&。
5：move的函数参数t实例化为string& &&，会折叠为string&。
因此，这个调用实例化move<string&>，即
string&& move(string &t)
这正是我们所寻求的---我们希望将一个右值引用绑定到一个左值。这个实例的函数体返回static_cast<string&&>(t)。在此情况下，t的类型为string&，cast将其转换为string&&。

从一个左值static_cast到一个右值引用是允许的
通常情况下，static_cast只能用于其他合法的类型转换。但是，这里又有一条针对右值引用的特许规则：虽然不能隐式地将一个左值转换为右值引用，但我们可以用static_cast显式
地将一个左值转换为一个右值引用。
对于操作右值引用的代码来说，将一个右值引用绑定到一个左值的特性允许它们截断左值。
一方面，通过允许进行这样的转换，C++语言认可了这种用法。但另一方面，通过强制使用static_cast，C++语言试图阻止我们意外地进行这种转换。
最后，虽然我们可以直接编写这种类型转换代码，但使用标准库move函数是容易得多的方式。而且，统一使用std::move使得我们在程序中查找潜在的截断左值的代码变得很容易。

转发:
某些函数需要将其一个或多个实参连同类型不变地转发给其他函数。在此情况下，我们需要保持被转发实参的所有性质，包括实参类型是否是const的以及实参是左值还是右值。

作为一个例子，我们将编写一个函数，它接受一个可调用表达式和两个额外实参。我们的函数将调用给定的可调用对象，将两个额外参数逆序传递给它。下面是我们的翻转函数的初步
模样：
//接受一个可调用对象和另外两个参数的模板
//对“翻转”的参数调用给定的可调用对象
//flip1是一个不完整的实现：顶层const和引用丢失了
template <typename F, typename T1, typename T2>
void flip1(F f, T1 t1, T2 t2)
{
	f(t2, t1);
}
这个函数一般情况下工作的很好，但当我们希望用它调用一个接受引用参数的函数时就会出现问题：
void f(int v1, int &v2) //注意v2是一个引用
{
	cout << v1 << " " << ++v2 << endl;
}
在这段代码中，f改变了绑定到v2的实参的值。但是，如果我们通过flip1调用f，f所做的改变就不会影响实参：
f(42,  i);													//f改变了实参i
flip1(f, j, 42);												//通过flip1调用f不会改变j
问题在于j被传递给flip1的参数t1。此参数是一个普通的、非引用的类型int，而非int&。因此，这个flip1调用会实例化为：
void flip1(void(*fcn)(int, int&), int t1, int t2);
j的值被拷贝到t1中。f中的引用参数被绑定到t1，而非j，从而其改变不会影响j。

定义能保持类型信息的函数参数
为了通过翻转函数传递一个引用，我们需要重写函数，使其参数能保持给定实参的“左值性”。更进一步，可以想到我们也希望保持参数的const属性。

通过将一个函数参数定义为一个指向模板类型参数的右值引用，我们可以保持其对应实参的所有类型信息。而使用引用参数(无论是左值还是右值)使得我们可以保持const属性，因为
在引用类型中的const是底层的。如果我们将函数参数定义为T1&&和T2&&，通过引用折叠就可以保持翻转实参的左值/右值属性。
template <typename F, typename T1, typename T2>
void flip2(F f, T1 &&t1, T2 &&t2)
{
	f(t2, t1);
}
与较早的版本一样，如果我们调用flip2(f,j,42)，将传递给参数t1一个左值j。但是在flip2中，推断出的T1的类型为int&，这意味着t1的类型会折叠为int&。由于是引用类型，t1被绑
定到j上。当flip2调用f时，f中的引用参数v2被绑定到t1，也就是被绑定到j。当f递增v2时，它也同时改变了j的值。

记住：如果一个函数参数是指向模板类型参数的右值引用(如T&&)，它对应的实参的const属性和左值/右值属性将得到保持。

这个版本的flip2解决了一半问题。它对于接受一个左值引用的函数工作得很好，但不能用于接受右值引用参数的函数。例如：
void g(int &&i, int& j)
{
	cout << i << " " << j << endl;
}
如果我们试图通过flip2调用g，则参数t2将被传递给g的右值引用参数。即使我们传递一个右值给flip2：
flip2(g,  i, 42);											//错误，不能从一个左值实例化int&&
传递给g的将是flip2中名为t2的参数。函数参数与其他任何变量一样，都是左值表达式。因此，flip2中对g的调用将传递给g的右值引用参数一个左值。

在调用中使用std::forward保持类型信息
我们可以使用一个名为forward的新标准库设施来传递flip2的参数，它能保持原始实参的类型。类似move，forward定义在头文件utility中。与move不同，forward必须通过显式模
板实参来调用。forward返回该显式实参类型的右值引用。即，forward<T>的返回类型是T&&。

通常情况下，我们使用forward传递那些定义为模板类型参数的右值引用的函数参数。通过其返回类型上的引用折叠，forward可以保持给定实参的左值/右值属性：
template <typename Type> intermediary(Type &&arg)
{
	finalFcn(std::forward<Type>(arg));
	//...
}
本例中我们使用Type作为forward的显式模板实参类型，它是从arg推断出来的。由于arg是一个模板类型参数的右值引用，Type将表示传递给arg的实参的所有类型信息。如果实参
是一个右值，则Type是一个普通(非引用)类型，forward<Type>将返回Type&&。如果实参是一个左值，则通过引用折叠，Type本身是一个左值引用类型。在此情况下，返回类型
是一个指向左值引用类型的右值引用。再次对forward<Type>的返回类型进行引用折叠，将返回一个左值引用类型。

记住：当用于一个指向模板参数类型的右值引用函数参数(T&&)时，forward会保持实参类型的所有细节。

使用forward，我们可以再次重写翻转函数：
template <typename F, typename T1, typename T2>
void flip(F f, T1 &&t1, T2 &&t2)
{
	f(std::forward<T2>(t2), std::forward<T1>(t1));
}
如果我们调用flip(g,i,42),i将以int&类型传递给g，42将以int&类型传递给g。

记住：与std::move相同，对std::forward不适用using声明时一个好主意。

可变参数模板:
一个可变参数模板(variadic template)就是一个接受可变数目参数的模板函数或模板类。可变数目的参数被称为参数包(parameter packet)。存在两种参数包：模板参数包，表示零
个或多个模板参数；函数参数包，表示零个或多个函数参数。

我们用一个省略号来指出一个模板参数或函数参数表示一个包。在一个模板参数列表中，class...或typename...指出接下来的参数表示零个或多个类型的列表；一个类型名后面跟一个
省略号表示零个或多个给定类型的非类型参数的列表。在函数参数列表中，如果一个参数的类型是一个模板参数包，则此参数也是一个函数参数包。例如：
//Args是一个模板参数包；rest是一个函数参数包
//Args表示零个或多个模板类型参数
//rest表示零个或多个函数参数
template <typename T, typename... Args>
void foo(const T &t, const Args& ... rest);
声明了foo是一个可变参数函数模板，它有一个名为T的类型参数，和一个名为Args的模板参数包。这个包表示零个或多个额外的类型参数。foo的函数参数列表包含一个const &类型
的参数，指向T的类型，还包含一个名为rest的函数参数包，此包表示零个或多个函数参数。

与往常一样，编译器从函数的实参推断模板参数类型。对于一个可变参数模板，编译器还会推断包中参数的数目。例如，给定下面的调用：
int i = 0; real_type d = 3.14; string s = "how now brown cow";
foo(i, s, 42, d);												//包中有三个参数
foo(s, 42, "hi");												//包中有两个参数
foo(d, s);														//包中有一个参数
foo("hi");														//空包
编译器会为foo实例化出四个不同的版本：
void foo(const int&, const string&, const int&, const real_type&);
void foo(const string&, const int&, const char[3]&);
void foo(const real_type&, const string&);
void foo(const char[3]&);
在每个实例中，T的类型都是从第一个实参的类型推断出来的。剩下的实参(如果有的话)提供函数额外实参的数目和类型。

sizeof...运算符
当我们需要知道包中有多少元素时，可以使用sizeof...运算符。类似sizeof，sizeof...也返回一个常量表达式，而且不会对其实参求值：
template<typename ... Args> void g(Args ... args)
{
	cout << sizeof...(Args) << endl;					//类型参数的数目
	cout << sizeof...(args) << endl;					//函数参数的数目
}

编写可变参数函数模板
我们可以使用一个initializer_list来定义一个可接受可变数目实参的函数。但是，所有实参必须具有相同的类型(或它们的类型可以转换为同一个公共类型)。当我们既不知道想要处理
的实参的数目也不知道它们的类型时，可变参数函数是很有用的。
作为一个例子，我们将定义一个函数。我们首先定义一个名为print的函数，它在一个给定流上打印给定实参列表的内容。
可变参数函数通常是递归的。第一步调用处理包中的第一个实参，然后用剩余实参调用自身。我们的print函数也是这样的模式，每次递归调用将第二个实参打印到第一个实参表示的流
中。为了终止递归，我们还需要定义一个非可变参数的print函数，它接受一个流和一个对象：
//用来终止递归并打印最后一个元素的函数
//此函数必须在可变参数版本的print定义之前声明
template<typename T>
ostream &print(ostream &os, const T &t)
{
	return os << t;																	//包中最后一个元素之后不打印分隔符
}
//包中除了最后一个元素之外的其他元素都会调用这个版本的print
template <typename T, typename... Args>
ostream &print(ostream &os, const T &t, const Args&... rest)
{
	os << t << ", ";																	//打印第一个实参
	return print(os, rest...);														//递归调用，打印其他实参
}
第一个版本的print负责终止递归并打印初始调用中的最后一个实参。第二个版本的print是可变参数版本，它打印绑定到t的实参，并调用自身来打印函数参数包中的剩余值。

这段程序的关键部分是可变参数函数中对print的调用：
return print(os, rest...);															//递归调用，打印其他实参
我们的可变参数版本的print函数接受三个参数：一个ostream&，一个const T&和一个参数包。而此调用只传递了两个实参。其结果是rest中的第一个实参被绑定到t，剩余实参形成
下一个print调用的参数包。因此，在每个调用中，包中的第一个实参被移出，成为绑定到t的实参。即，给定：
print(cout,  i, s, 42);																//包中有两个参数
递归会执行如下：
调用                           t                  rest...
--------------------------------------------------------
print(cout,i,s,42)         i                  s,42
print(cout,s,42)          s                  42
print(cout,42)调用非可变参数版本的print

前两个调用只能与可变参数版本的print匹配，非可变参数版是不可行的，因为这两个调用分别传递四个和三个实参，而非可变参数print只接受两个实参。
对于最后一次递归调用print(cout,42)，两个print版本都是可行的。这个调用传递两个实参，第一个实参的类型为ostream&。因此，可变参数版本的print可以实例化为只接受两个参
数：一个是ostream&参数，另一个是const T&参数。
对于最后一个调用，两个函数提供同样好的匹配。但是，非可变参数模板比可变参数模板更特例化，因此编译器选择非可变参数版本。

当定义可变参数版本的print时，非可变参数版本的声明必须在作用域中。否则，可变参数版本会无限递归。

包扩展
对于一个参数包，除了获取其大小外，我们能对它做的唯一的事情就是扩展(expand)它。当扩展一个包时，我们还要提供用于每个扩展元素的模式(pattern)。扩展一个包就是将它分
解为构成的元素，对每个元素应用模式，获得扩展后的列表。我们通过在模式右边放一个省略号(...)来触发扩展操作。

例如，我们的print函数包含两个扩展：
template <typename T, typename... Args>
ostream & print(ostream &os, const T &t, const Args&... rest)					//扩展Args
{
	os << t << ", ";
	return print(os, rest...);																	//扩展rest
}
第一个扩展操作扩展模板参数包，为print生成函数参数列表。第二个扩展操作出现在对print的调用中。此模式为print调用生成实参列表。

对Args的扩展中，编译器将模式const Args&应用到模板参数包Args中的每个元素。因此，此模式的扩展结果是一个逗号分隔的零个或多个类型的列表，每个类型都形如const type&。
例如：
print(cout,  i, s, 42);																			//包中有两个参数
最后两个实参的类型和模式一起确定了尾置参数的类型。此调用被实例化为：
ostream& print(ostream&,const int&,const string&,const int&);

第二个扩展发生在对print的(递归)调用中。在此情况下，模式是函数参数包的名字(即rest)。此模式扩展出一个由包中元素组成的、逗号分隔的列表。因此，这个调用等价于：
print(os,  s, 42);

理解包扩展
print中的函数参数包扩展仅仅将包扩展为其构成元素，C++语言还允许更复杂的扩展模式。例如，我们可以编写第二个可变参数函数，对其每个实参调用debug_rep，然后调用
print打印结果string：
//在print调用中对每个实参调用debug_rep
template <typename... Args>
ostream &errorMsg(ostream &os, const Args&... rest)
{
	//print(os, debug_rep(a1), debug_rep(a2), ..., debug_rep(an)
	return print(os, debug_rep(rest)...);
}
这个print调用使用了模式debug_rep(rest)。此模式表示我们希望对函数参数包rest中的每个元素调用debug_rep。扩展结果将是一个逗号分隔的debug_rep调用列表。即，下面调
用：
errorMsg(cerr,  fcnName, code.num(), otherData, "other",item);
就好像我们这样编写代码一样：
print(cerr,  debug_rep(fcnName), debug_rep(code.num()),
debug_rep(otherData), debug_rep("otherData"),
debug_rep(item));

与之相对，下面的模式会编译失败
//将包传递给debug_rep; print(os, debug_rep(a1, a2, ..., an))
print(os, debug_rep(rest...));												//错误，此调用无匹配函数
这段代码的问题是我们在debug_rep调用中扩展了rest，它等价于：
print(cerr,  debug_rep(fcnName, code.num(),otherData, "otherData", item));
在这个扩展中，我们试图用一个五个实参的列表来调用debug_rep，但并不存在与此调用匹配的debug_rep版本。debug_rep函数不是可变参数的，而且没有哪个debug_rep版本
接受五个参数。

记住：扩展中的模式会独立地应用于包中的每个元素。

转发参数包:
在新标准下，我们可以组合使用可变参数模板与forward机制来编写函数，实现将其实参不变地传递给其他函数。作为例子，我们将为StrVec类添加一个emplace_back成员。标准库
容器的emplace_back成员是一个可变参数成员模板，它用其实参在容器管理的内存空间中直接构造一个元素。
我们为StrVec设计的emplace_back版本也应该是可变参数的，因为string有多个构造函数，参数各不相同。由于我们希望能使用string的移动构造函数，因此还需要保持传递给
emplace_back的实参的所有类型信息。
如我们所见，保持类型信息是一个两阶段的过程。首先，为了保持实参中的类型信息，必须将emplace_back的函数参数定义为模板类型参数的右值引用：
class StrVec
{
public:
	template <class... Args> void emplace_back(Args&&...);
	//remaining members
};
模板参数包扩展中的模式是&&，意味着每个函数参数将是一个指向其对应实参的右值引用。

其次，当emplace_back将这些实参传递给construct时，我们必须使用forward来保持实参的原始类型：
template <class... Args>
inline
void StrVec::emplace_back(Args&&... args)
{
	chk_n_alloc();															//如果需要的话重新分配StrVec内存空间
	alloc.construct(first_free++,
	std::forward<Args>(args)...);
}
emplace_back的函数体调用了chk_n_alloc来确保有足够的空间容纳一个新元素，然后调用了construct在first_free指向的位置中创建了一个元素。construct调用中的扩展为：
std::forward<Args>(args)...
它既扩展了模板参数包Args，也扩展了函数参数包args。此模式生成如下形式的元素
std::forward<Ti>(ti)
其中Ti表示模板参数包中第i个元素的类型，ti表示函数参数包中第i个元素。例如，假定svec是一个StrVec，如果我们调用
svec.emplace_back(10,  'c');												//将cccccccccc添加为新的尾元素
construct调用中的模式会扩展出
std::forward<int>(10),  std::forward<char>(c)
通过在地调用中使用forward，我们保证如果用一个右值调用emplace_back，则construct也会得到一个右值。例如，在下面的调用中：
svec.emplace_back(s1  + s2); //使用移动构造函数
传递给emplace_back的实参是一个右值，它将以如下形式传递给construct
std::forward<string>(string("the end"))
forward<string>的结果类型是string&&，因此construct将得到一个右值引用实参。construct会继续将此实参传递给string的移动构造函数来创建新元素。

转发和可变参数模板
可变参数函数通常将它们的参数转发给其他函数。这种函数通常具有与我们的emplace_back函数一样的形式：
//fun有零个或多个参数，每个参数都是一个模板参数类型的右值引用
template<typename... Args>
void fun(Args&&... args)													//将Args扩展为一个右值引用的列表
{
	//work的实参既扩展Args又扩展args
	work(std::forward<Args>(args)...);
}
这里我们希望将fun的所有实参转发给另一个名为work的函数，假定由它完成函数的实际工作。类似emplace_back中对construct的调用，work调用中的扩展既扩展了模板参数包也
扩展了函数参数包。
由于fun的参数是右值引用，因此我们可以传递给它任意类型的实参；由于我们使用std::forward传递这些实参，因此它们的所有类型信息在调用work时都会得到保持。
*/