#include "UFCUnpack.h"

#ifdef UFC_HIERARCHICAL_COMPILE

#include "UFCUnpackHelperInfo.h"
#include "UFCStream.h"
#include "UFCStreamAux.h"
#include "UFCFilesystem.h"
#include "UFCStringUtilities.h"
#include "boost/iostreams/device/file_descriptor.hpp"
#include "boost/interprocess/mapped_region.hpp"
#include "boost/interprocess/file_mapping.hpp"
#define BOOST_IOSTREAMS_SOURCE
#include "boost/iostreams/filter/zlib.hpp"

#if UNG_DEBUGMODE
#include <iostream>
#include "UFCTimer.h"
#endif

namespace
{
	using namespace ung;

	/*!
	 * \remarks 解密
	 * \return 
	 * \param std::string& str
	*/
	void decrypt(std::string& str)
	{
		size_t strSize = str.size();

		char* strArray = new char[strSize];
		ufcContainerToMemory(str, strArray, strSize);
		str.clear();

		for (unsigned int i = 0; i < strSize; ++i)
		{
			if (i % 3 == 0)
			{
				//这里必须用无符号char类型
				unsigned char data = strArray[i];
				unsigned char ml5 = data << 5;
				unsigned char mr3 = data >> 3;
				strArray[i] = static_cast<unsigned char>(ml5 + mr3);
			}

			if (i % 2 == 0)
			{
				//这里必须用无符号char类型
				unsigned char data = strArray[i];
				unsigned char ml6 = data << 6;
				unsigned char mr2 = data >> 2;
				strArray[i] = static_cast<unsigned char>(ml6 + mr2);
			}
		}

		//遍历字符数组
		unsigned int mid = static_cast<unsigned int>(strSize / 2);
		for (unsigned int i = 0; i < mid; ++i)
		{
			std::swap(strArray[i], strArray[strSize - 1 - i]);
		}

		//解密后的名字块
		str.assign(strArray, strSize);

		//删除字符数组
		delete[]strArray;
	}

	/*!
	 * \remarks 规范化包全名
	 * \return 
	 * \param const char * packName
	*/
	String normalizePackName(const char* packName)
	{
		//文件系统
		auto& fsRef = Filesystem::getInstance();

		String zipFileName(packName);
		fsRef.processDirSeparator(zipFileName);
		BOOST_ASSERT(fsRef.isExists(zipFileName.data()) && fsRef.isRegularFile(zipFileName.data()));

		return std::move(zipFileName);
	}

	/*!
	 * \remarks 获取包的父路径
	 * \return 
	 * \param const char * packName
	*/
	String getPackParentPath(const char* packName)
	{
		//文件系统
		auto& fsRef = Filesystem::getInstance();

		String zipFileParentDir = fsRef.getParentPath(packName);
		fsRef.processDirSeparator(zipFileParentDir);

		return std::move(zipFileParentDir);
	}

	/*!
	 * \remarks 获取解压路径
	 * \return 
	 * \param String& packParentPath
	*/
	String getUnpackPath(String& packParentPath)
	{
		//找到最底层的位置
		//先删除掉zipFileParentDir最后的"/"
		StringUtilities::eraseRightInSensitive(packParentPath, String("/"));
		//然后找
		auto pos = StringUtilities::findRightInSensitive(packParentPath, String("/"));
		//再补上zipFileParentDir最后的"/"
		packParentPath += String("/");
		//给unzipDir多加一层（zipFileParentDir的最底层）
		String unzipDir = packParentPath + packParentPath.substr(pos + 1, packParentPath.size());

		return std::move(unzipDir);
	}

	/*!
	 * \remarks 获取给定文件的最终解压全路径
	 * \return 用来构造目的设备
	 * \param String const & fileFullName 文件全名
	 * \param String const & packParentDir 包的父路径
	 * \param String const & unpackDir 解压路径
	*/
	String getUnpackFileFullName(String const& fileFullName,String const& packParentDir,String const& unpackDir)
	{
		//文件系统
		auto& fsRef = Filesystem::getInstance();

		//当前文件的父路径
		String fileParent = fsRef.getParentPath(fileFullName.data());
		fsRef.processDirSeparator(fileParent);
		//从当前文件的父路径中删除掉“压缩文件的父路径”
		StringUtilities::eraseLeftInSensitive(fileParent, packParentDir);

		//当前文件的解压路径
		String sinkFileDir = unpackDir + fileParent;
		fsRef.processDirSeparator(sinkFileDir);
		//创建目录
		if (!fsRef.isExists(sinkFileDir.data()) || !fsRef.isDirectory(sinkFileDir.data()))
		{
			fsRef.createDir(sinkFileDir.data());
		}
		//当前文件的文件名
		String fileName = fsRef.getFileName(fileFullName.data());
		//当前文件的解压最终全名
		String sinkFileFullName = sinkFileDir + fileName;

		return std::move(sinkFileFullName);
	}

	/*!
	 * \remarks 获取32位无符号整数的字节数
	 * \return 
	*/
	unsigned int getUint32Bytes()
	{
		return sizeof(unsigned int);
	}

	/*!
	 * \remarks 获取64位整数的字节数
	 * \return 
	*/
	unsigned int getInt64Bytes()
	{
		return sizeof(int64);
	}

	/*!
	 * \remarks 获取尾部3个数据的字节数(名字块字节数，总段数，文件数)
	 * \return 
	*/
	unsigned int getTailInfoBytes()
	{
		return getUint32Bytes() * 3;
	}

	/*!
	 * \remarks 获取包文件尾部的名字字节数，总段数，文件数
	 * \return 
	 * \param const char* packFileFullName
	 * \param int64 packFileBytes
	*/
	std::tuple<unsigned int, unsigned int, unsigned int> getTailInfo(const char* packFileFullName, int64 packFileBytes)
	{
		using namespace boost::interprocess;

		//尾部数据的字节数
		unsigned int tailDataBytes = getTailInfoBytes();

		//对压缩文件的末尾数据的映射
		mapped_region region{ file_mapping(packFileFullName, read_only),read_only,packFileBytes - tailDataBytes,tailDataBytes };

		char* pAddress = reinterpret_cast<char*>(region.get_address());

		//名字块的字节数
		unsigned int nameBlockTotalBytes = *reinterpret_cast<unsigned int*>(pAddress);
		//总段数
		unsigned int sectionCount = *reinterpret_cast<unsigned int*>(pAddress + getUint32Bytes());
		//文件数量
		unsigned int fileCount = *reinterpret_cast<unsigned int*>(pAddress + getUint32Bytes() + getUint32Bytes());

		auto tp = std::make_tuple(nameBlockTotalBytes, sectionCount, fileCount);

		return std::move(tp);
	}

	/*!
	 * \remarks 获取一个item的字节数
	 * \return 
	*/
	unsigned int getOneItemBytes()
	{
		//文件名字字节数，原始大小，文件在section块中的第一个索引，段数，文件在压缩数据块中的偏移
		return getUint32Bytes() + getInt64Bytes() + getUint32Bytes() + getUint32Bytes() + getInt64Bytes();
	}

	/*!
	 * \remarks 获取所有items的字节数
	 * \return 
	 * \param unsigned int fileCount
	*/
	unsigned int getItemsBytes(unsigned int fileCount)
	{
		return getOneItemBytes() * fileCount;
	}

	/*!
	 * \remarks 获取解密后的名字块
	 * \return 
	 * \param const char* packFileFullName
	 * \param unsigned int nameBlockTotalBytes 名字块的总字节数
	*/
	String getNameBlock(const char* packFileFullName,unsigned int nameBlockTotalBytes)
	{
		using namespace boost::interprocess;

		//对压缩文件中的名字块区域进行映射
		mapped_region region{ file_mapping(packFileFullName, read_only),read_only,0,nameBlockTotalBytes };

		char* pAddress = reinterpret_cast<char*>(region.get_address());

		//名字块的字符串形式
		String nameBlockStr(pAddress, nameBlockTotalBytes);

		//解密
		decrypt(nameBlockStr);

		return std::move(nameBlockStr);
	}

	/*!
	 * \remarks 获取段区域的字节数
	 * \return 
	 * \param unsigned int sectionCount
	*/
	unsigned int getSectionsTotalBytes(unsigned int sectionCount)
	{
		return getUint32Bytes() * sectionCount;
	}

	/*!
	 * \remarks 获取每段的字节数，存储到容器中
	 * \return 
	 * \param const char* packFileFullName
	 * \param int64 packFileBytes 包总字节数
	 * \param unsigned int itemsTotalBytes 所有条目的总字节数
	 * \param unsigned int sectionCount 段数
	*/
	std::vector<unsigned int> getEverySectionBytes(const char* packFileFullName,int64 packFileBytes,
		unsigned int itemsTotalBytes,unsigned int sectionCount)
	{
		using namespace boost::interprocess;

		//尾部数据的字节数
		unsigned int tailDataBytes = getTailInfoBytes();

		//构造一个临时容器，用于获取每个段的字节数信息
		std::vector<unsigned int> sections;

		//段区域的总字节数
		unsigned int sectionsBlockSize = getSectionsTotalBytes(sectionCount);

		//对压缩文件中的存储所有段字节数的区域进行映射
		mapped_region region{ file_mapping(packFileFullName, read_only),read_only,
			packFileBytes - tailDataBytes - itemsTotalBytes - sectionsBlockSize,sectionsBlockSize };

		char* pAddress = reinterpret_cast<char*>(region.get_address());

		for (unsigned int i = 0; i < sectionCount; ++i)
		{
			unsigned int sectionBytes = *reinterpret_cast<unsigned int*>(pAddress);

			sections.push_back(sectionBytes);

			pAddress += getUint32Bytes();
		}

		BOOST_ASSERT(sections.size() == sectionCount);

		return std::move(sections);
	}

	/*!
	 * \remarks 计算当前文件分布于哪些映射段
	 * \return 
	 * \param std::vector<std::tuple<int64,unsigned int, unsigned int, unsigned int>> regionsInfo 0:这个映射距离压缩数据开头处的偏移,1:这个映射的字节数,2:这个映射所对应的起始段索引,3:下个映射所对应的结束段索引
	 * \param unsigned int sectionBegIndex 当前文件在“存储每段大小”区域的起始索引
	 * \param unsigned int sectionNum 当前文件的段数
	*/
	std::vector<unsigned int> calFileReleatedRegions(std::vector<std::tuple<int64,unsigned int, unsigned int, unsigned int>> regionsInfo,
		unsigned int sectionBegIndex, unsigned int sectionNum)
	{
		//用于存放包含该文件的所有映射段的索引
		std::vector<unsigned int> ret;

		//文件第一个section索引落于哪个map段
		unsigned int b{};
		//文件最后一个section索引落于哪个map段
		unsigned int e{};

		//对包的总映射数
		unsigned int regionsNum = regionsInfo.size();

		for (unsigned int i = 0; i < regionsNum; ++i)
		{
			//当前映射段
			auto currentRegion = regionsInfo[i];

			//当前映射段的起始索引
			unsigned int begIndex = std::get<2>(currentRegion);

			//当前映射段的结束索引
			unsigned int endIndex = std::get<3>(currentRegion);

			if (sectionBegIndex >= begIndex && sectionBegIndex < endIndex)
			{
				b = i;
			}

			if (sectionBegIndex + sectionNum > begIndex && sectionBegIndex + sectionNum <= endIndex)
			{
				e = i;

				break;
			}
		}

		for (unsigned int i = b; i <= e; ++i)
		{
			ret.push_back(i);
		}

		return std::move(ret);
	}
}//namespace

namespace ung
{
	void Unpack::unpackToDisk(const char* packName)
	{
		using namespace boost::iostreams;
		using namespace boost::interprocess;

		//文件系统
		auto& fsRef = Filesystem::getInstance();

		//对包的名字进行规范化
		String zipFileName = normalizePackName(packName);

		//获取包的父路径
		String zipFileParentDir = getPackParentPath(zipFileName.data());

		//获取解压路径
		String unzipDir = getUnpackPath(zipFileParentDir);

		//-----------------------------------------------------------------------------------------------------

		//得到压缩文件的字节数(unsigned int最大只能表达4G)
		int64 packFileBytes = fsRef.getFileSize(zipFileName.data());

		//-----------------------------------------------------------------------------------------------------

		//尾部数据的字节数
		unsigned int tailDataBytes = getTailInfoBytes();
		//获取包尾部的数据信息
		auto tailInfo = getTailInfo(zipFileName.data(),packFileBytes);
		//名字块的字节数
		unsigned int nameBlockTotalBytes = std::get<0>(tailInfo);
		//压缩包中的总段数
		unsigned int sectionCount = std::get<1>(tailInfo);
		//具体文件的数量
		unsigned int fileCount = std::get<2>(tailInfo);

		//每个条目的字节数
		unsigned int itemBytes = getOneItemBytes();
		//所有条目的字节数
		unsigned int itemsTotalBytes = getItemsBytes(fileCount);

		//所有段区域的总字节数
		unsigned int sectionsBytesBlockSize = getSectionsTotalBytes(sectionCount);

		//压缩数据的总字节数(这里不能使用unsigned int)
		int64 compressedBlockBytes = packFileBytes - tailDataBytes - itemsTotalBytes - sectionsBytesBlockSize -
			nameBlockTotalBytes;
		//压缩数据起始处在整个zip文件中的偏移字节数
		unsigned int compressedBlockStartOffset = nameBlockTotalBytes;

		//-----------------------------------------------------------------------------------------------------

		//名字块
		String nameBlockStr = getNameBlock(zipFileName.data(),nameBlockTotalBytes);
		//所有的名字
		std::vector<String> names;

		//-----------------------------------------------------------------------------------------------------

		//所有段的大小
		std::vector<unsigned int> sections = getEverySectionBytes(zipFileName.data(),packFileBytes,itemsTotalBytes,sectionCount);

#if UNG_DEBUGMODE
		int64 accumulateCompressedDataBytes{};
		for (auto const& elem : sections)
		{
			accumulateCompressedDataBytes += elem;
		}
		BOOST_ASSERT(accumulateCompressedDataBytes == compressedBlockBytes);
#endif

		//-----------------------------------------------------------------------------------------------------

		//条目(原始大小，文件在section块中的第一个索引，段数，文件在压缩数据中的偏移)
		std::vector<std::tuple<int64, unsigned int,unsigned int,int64>> items;
		items.reserve(fileCount);

		{
			//对压缩文件中的所有条目进行映射
			mapped_region region{ file_mapping(zipFileName.data(), read_only),read_only,
				packFileBytes - tailDataBytes - itemsTotalBytes,itemsTotalBytes };

			char* pAddress = reinterpret_cast<char*>(region.get_address());

			//监控名字的字节数
			unsigned int namesBytesPassed{};
			for (unsigned int i = 0; i < fileCount; ++i)
			{
				//名字字节数
				unsigned int nameBytes = *reinterpret_cast<unsigned int*>(pAddress);

				names.push_back(nameBlockStr.substr(namesBytesPassed,nameBytes));			//off,count
				namesBytesPassed += nameBytes;

				//原始文件字节数
				int64 originalBytes = *reinterpret_cast<int64*>(pAddress + getUint32Bytes());
				//文件在section块中的第一个索引
				unsigned int fileFirstIndexInSectionBlock = *reinterpret_cast<unsigned int*>(pAddress + getUint32Bytes() + getInt64Bytes());
				//段数
				unsigned int sectionNum = *reinterpret_cast<unsigned int*>(pAddress + getUint32Bytes() + getInt64Bytes() + getUint32Bytes());
				//在压缩数据块中的偏移
				int64 offsetInCompressedData = *reinterpret_cast<int64*>(pAddress + getUint32Bytes() + getInt64Bytes() + getUint32Bytes() + getUint32Bytes());

				auto item = std::make_tuple(originalBytes, fileFirstIndexInSectionBlock,sectionNum,offsetInCompressedData);

				items.push_back(item);

				pAddress += itemBytes;
			}
		}

		//-----------------------------------------------------------------------------------------------------

		/*
		对真正的压缩数据进行分段映射。
		每映射段字节数不超过100M。
		一个具体的文件，其压缩数据可能会跨越两个或多个映射段。
		*/
		const unsigned int regionMaxBytes = 100 * MEGABYTES;
		//0:这个映射距离压缩数据开头处的偏移,1:这个映射的实际字节数,2:这个映射所对应的起始段索引,3:这个map的结束段索引，下一个map的起始段索引
		using mapInfo_type = std::tuple<int64,unsigned int, unsigned int, unsigned int>;
		std::vector<mapInfo_type> compressedDataMappedInfo;

		{
			int64 offset{};
			unsigned int mapBytes{};
			unsigned int beg{};
			bool flag{ false };

			for (unsigned int i = 0; i < sectionCount;)
			{
				//当前section的大小
				unsigned int bytes = sections[i];

				if (!flag)
				{
					beg = i;
					flag = true;
				}

				//如果累加结果没有超过100M
				if ((mapBytes + bytes) <= regionMaxBytes)
				{
					//累加
					mapBytes += bytes;

					if (i == sectionCount - 1)
					{
						compressedDataMappedInfo.push_back(std::make_tuple(offset, mapBytes, beg, sectionCount));
					}
				}
				else
				{
					compressedDataMappedInfo.push_back(std::make_tuple(offset, mapBytes, beg, i));
					offset += mapBytes;
					mapBytes = 0;
					flag = false;
					
					continue;
				}

				++i;
			}
		}

		//-----------------------------------------------------------------------------------------------------

		//遍历所有的文件名
		//当前文件在“存储每段大小”的区域的开始处索引
		unsigned int sectionBegIndex{};
		for (unsigned int i = 0; i < fileCount; ++i)
		{
			//当前文件的名字
			String name = names[i];

#if UNG_DEBUGMODE
			std::cout << name << std::endl;
#endif

			//当前文件所对应的条目
			auto item = items[i];

			//当前文件的原始大小
			int64 orignalBytes = std::get<0>(item);

			//当前文件在section块中的第一个索引
			unsigned int firstIndexInSectionBlock = std::get<1>(item);

			//当前文件的段数
			unsigned int sectionNum = std::get<2>(item);

			//当前文件在压缩数据块的偏移
			int64 fileOffset = std::get<3>(item);
#if UNG_DEBUGMODE
			if (i == 0)
			{
				BOOST_ASSERT(fileOffset == 0);
			}
#endif

			//当前文件的解压最终全名
			String sinkFileFullName = getUnpackFileFullName(name, zipFileParentDir, unzipDir);

			//目的设备
			file_descriptor_sink out(sinkFileFullName);

			//计算当前文件都分布于哪些映射段
			auto locations = calFileReleatedRegions(compressedDataMappedInfo,sectionBegIndex,sectionNum);

			//跟踪当前文件的section索引
			unsigned int indexStep = 0;

			//遍历所分布的映射段
			for (unsigned int j = 0; j < locations.size(); ++j)
			{
				//当前映射段索引
				unsigned int currentRegionIndex = locations[j];

				//当前映射段信息
				auto currentRegionInfo = compressedDataMappedInfo[currentRegionIndex];

				//当前映射段距离压缩数据开头的偏移字节数
				int64 currentRegionOffset = std::get<0>(currentRegionInfo);

				//当前映射段的字节数
				unsigned int currentRegionBytes = std::get<1>(currentRegionInfo);

#if UNG_DEBUGMODE
				static uint64 totalMappedTimer{};
				SteadyClock_ms timer;
#endif

				/*
				实测，每次执行映射的时间几乎可以忽略不计。每次所消耗的时间，用毫秒无法度量，也就是说不到1毫秒。
				*/
				mapped_region region{ file_mapping(zipFileName.data(), read_only),read_only,
					compressedBlockStartOffset + currentRegionOffset, currentRegionBytes };
				char* source = reinterpret_cast<char*>(region.get_address());

#if UNG_DEBUGMODE
				totalMappedTimer += timer.elapsed();
				std::cout << "elapsed: " << totalMappedTimer << std::endl;
#endif

				/*
				如果当前文件横跨好几个映射段的话，那么只需要计算其在第一个映射段中的偏移。
				在第二和后续的映射段中，文件数据从映射段的开始处开始。
				*/
				unsigned int offset{};
				if (j == 0)
				{
					offset = fileOffset - currentRegionOffset;
				}

				//在这个映射段中，指向当前文件压缩数据的指针（这个pData所指向的内存地址，必须是两个步长的间隙）
				char* pData = source + offset;

				//当前映射段剩余的字节数
				unsigned int currentRegionLeftBytes = currentRegionBytes - offset;
				//遍历当前文件的每一个步长
				for (; indexStep < sectionNum; ++indexStep)
				{
					//这个步长的字节数
					unsigned int stepBytes = sections[indexStep + sectionBegIndex];

					//检查当前映射段剩余的字节数是否满足这个步长
					if (currentRegionLeftBytes >= stepBytes)
					{
						//输入流
						//此处，如果压缩数据和解压缩数据不对应的话，那么zlib会抛出异常
						filtering_istream in(zlib_decompressor() | array_source(pData, stepBytes));

						//拷贝
						ung::copy_not_close(in, out);

						//关闭输入流
						ung::close_one_now(in);

						pData += stepBytes;

						//更新当前映射段剩余的字节数
						currentRegionLeftBytes -= stepBytes;
					}
					else
					{
						//不满足这个步长，那么当前映射段剩余的字节数必须为0
						BOOST_ASSERT(currentRegionLeftBytes == 0);

						//跳出遍历步长的循环
						break;
					}
				}//end遍历每一个section
			}//end遍历每一个分布region

			//更新段数的索引
			sectionBegIndex += sectionNum;

			//此时indexStep必须等于sectionNum
			BOOST_ASSERT(indexStep == sectionNum);

			//关闭out
			close_one_now(out);
		}//end遍历每一个文件
	}

	//

	void Unpack::analysisPack(const char* packName,UnpackHelperInfo& helper)
	{
		using namespace boost::iostreams;
		using namespace boost::interprocess;

		//文件系统
		auto& fsRef = Filesystem::getInstance();

		//对包的名字进行规范化
		String zipFileName = normalizePackName(packName);

		//-----------------------------------------------------------------------------------------------------

		//得到压缩文件的字节数(unsigned int最大只能表达4G)
		int64 packFileBytes = fsRef.getFileSize(zipFileName.data());

		//-----------------------------------------------------------------------------------------------------

		//尾部数据的字节数
		unsigned int tailDataBytes = getTailInfoBytes();
		//获取包尾部的数据信息
		auto tailInfo = getTailInfo(zipFileName.data(),packFileBytes);
		//名字块的字节数
		unsigned int nameBlockTotalBytes = std::get<0>(tailInfo);
		//压缩包中的总段数
		unsigned int sectionCount = std::get<1>(tailInfo);
		//具体文件的数量
		unsigned int fileCount = std::get<2>(tailInfo);

		//每个条目的字节数
		unsigned int itemBytes = getOneItemBytes();
		//所有条目的字节数
		unsigned int itemsTotalBytes = getItemsBytes(fileCount);

		//所有段区域的总字节数
		unsigned int sectionsBytesBlockSize = getSectionsTotalBytes(sectionCount);

		//压缩数据的总字节数(这里不能使用unsigned int)
		int64 compressedBlockBytes = packFileBytes - tailDataBytes - itemsTotalBytes - sectionsBytesBlockSize -
			nameBlockTotalBytes;
		//压缩数据起始处在整个zip文件中的偏移字节数
		unsigned int compressedBlockStartOffset = nameBlockTotalBytes;

		//填充
		helper.mPackName = zipFileName;
		helper.mFileCount = fileCount;
		helper.mSectionCount = sectionCount;
		helper.mDataOffset = compressedBlockStartOffset;

		//-----------------------------------------------------------------------------------------------------

		//名字块
		String nameBlockStr = getNameBlock(zipFileName.data(),nameBlockTotalBytes);
		//所有的名字
		std::vector<String> names;

		//-----------------------------------------------------------------------------------------------------

		//所有段的大小
		std::vector<unsigned int> sections = getEverySectionBytes(zipFileName.data(),packFileBytes,itemsTotalBytes,sectionCount);

#if UNG_DEBUGMODE
		int64 accumulateCompressedDataBytes{};
		for (auto const& elem : sections)
		{
			accumulateCompressedDataBytes += elem;
		}
		BOOST_ASSERT(accumulateCompressedDataBytes == compressedBlockBytes);
#endif

		//-----------------------------------------------------------------------------------------------------

		//条目(原始大小，文件在section块中的第一个索引，段数，文件在压缩数据中的偏移)
		std::vector<std::tuple<int64, unsigned int,unsigned int,int64>> items;
		items.reserve(fileCount);

		{
			//对压缩文件中的所有条目进行映射
			mapped_region region{ file_mapping(zipFileName.data(), read_only),read_only,
				packFileBytes - tailDataBytes - itemsTotalBytes,itemsTotalBytes };

			char* pAddress = reinterpret_cast<char*>(region.get_address());

			//监控名字的字节数
			unsigned int namesBytesPassed{};
			for (unsigned int i = 0; i < fileCount; ++i)
			{
				//名字字节数
				unsigned int nameBytes = *reinterpret_cast<unsigned int*>(pAddress);

				names.push_back(nameBlockStr.substr(namesBytesPassed,nameBytes));			//off,count
				namesBytesPassed += nameBytes;

				//原始文件字节数
				int64 originalBytes = *reinterpret_cast<int64*>(pAddress + getUint32Bytes());
				//文件在section块中的第一个索引
				unsigned int fileFirstIndexInSectionBlock = *reinterpret_cast<unsigned int*>(pAddress + getUint32Bytes() + getInt64Bytes());
				//段数
				unsigned int sectionNum = *reinterpret_cast<unsigned int*>(pAddress + getUint32Bytes() + getInt64Bytes() + getUint32Bytes());
				//在压缩数据块中的偏移
				int64 offsetInCompressedData = *reinterpret_cast<int64*>(pAddress + getUint32Bytes() + getInt64Bytes() + getUint32Bytes() + getUint32Bytes());

				auto item = std::make_tuple(originalBytes, fileFirstIndexInSectionBlock,sectionNum,offsetInCompressedData);

				items.push_back(item);

				pAddress += itemBytes;
			}
		}

		//填充(存在copy开销)
		for (unsigned int i = 0; i < fileCount; ++i)
		{
			helper.mNames.push_back(names[i]);
			helper.mItems.push_back(items[i]);
		}

		for (unsigned int i = 0; i < sectionCount; ++i)
		{
			helper.mSections.push_back(sections[i]);
		}

		//-----------------------------------------------------------------------------------------------------

		/*
		对真正的压缩数据进行分段映射。
		每映射段字节数不超过100M。
		一个具体的文件，其压缩数据可能会跨越两个或多个映射段。
		*/
		const unsigned int regionMaxBytes = 100 * MEGABYTES;
		//0:这个映射距离压缩数据开头处的偏移,1:这个映射的实际字节数,2:这个映射所对应的起始段索引,3:这个map的结束段索引，下一个map的起始段索引
		using mapInfo_type = std::tuple<int64,unsigned int, unsigned int, unsigned int>;
		std::vector<mapInfo_type> compressedDataMappedInfo;

		{
			int64 offset{};
			unsigned int mapBytes{};
			unsigned int beg{};
			bool flag{ false };

			for (unsigned int i = 0; i < sectionCount;)
			{
				//当前section的大小
				unsigned int bytes = sections[i];

				if (!flag)
				{
					beg = i;
					flag = true;
				}

				//如果累加结果没有超过100M
				if ((mapBytes + bytes) <= regionMaxBytes)
				{
					//累加
					mapBytes += bytes;

					if (i == sectionCount - 1)
					{
						compressedDataMappedInfo.push_back(std::make_tuple(offset, mapBytes, beg, sectionCount));
					}
				}
				else
				{
					compressedDataMappedInfo.push_back(std::make_tuple(offset, mapBytes, beg, i));
					offset += mapBytes;
					mapBytes = 0;
					flag = false;
					
					continue;
				}

				++i;
			}
		}

		//填充
		for (unsigned int i = 0; i < compressedDataMappedInfo.size(); ++i)
		{
			helper.mMaps.push_back(compressedDataMappedInfo[i]);
		}		
	}

	std::pair<std::shared_ptr<char>, int64> Unpack::extractToMemory(UnpackHelperInfo const& helper, const char* fileName)
	{
		using namespace boost::iostreams;
		using namespace boost::interprocess;

		//获取包的名字
		String packName = helper.getPackName();

		//压缩数据在包中的偏移
		unsigned int dataOffset = helper.mDataOffset;

		//获取要提取的文件在包中的索引
		unsigned int fileIndex = helper.getFileIndex(fileName);

		//当前文件在“存储每段大小”的区域的开始处索引
		unsigned int sectionBegIndex{};
		//当前文件的名字
		String name = helper.mNames[fileIndex];

		//当前文件所对应的条目
		auto item = helper.mItems[fileIndex];

		//当前文件的原始大小
		int64 orignalBytes = std::get<0>(item);

		//分配内存空间(不要delete []这里分配的char数组)
		char* pData = new char[orignalBytes];

		//输出流
		array_sink as(pData,orignalBytes);
		stream<array_sink> out(as);

		//当前文件在section块中的第一个索引
		unsigned int firstIndexInSectionBlock = std::get<1>(item);

		//当前文件的段数
		unsigned int sectionNum = std::get<2>(item);

		//当前文件在压缩数据块的偏移
		int64 fileOffset = std::get<3>(item);

		//计算当前文件都分布于哪些映射段
		//复制来适应接口(存在开销，等统一整理容器的内存分配器时再修改)
		using mapInfo_type = std::tuple<int64, unsigned int, unsigned int, unsigned int>;
		std::vector<mapInfo_type> maps;
		for (unsigned int i = 0; i < helper.mMaps.size(); ++i)
		{
			maps.push_back(helper.mMaps[i]);
		}
		auto locations = calFileReleatedRegions(maps,sectionBegIndex,sectionNum);

		//跟踪当前文件的section索引
		unsigned int indexStep = 0;

		//遍历所分布的映射段
		for (unsigned int j = 0; j < locations.size(); ++j)
		{
			//当前映射段索引
			unsigned int currentRegionIndex = locations[j];

			//当前映射段信息
			auto currentRegionInfo = maps[currentRegionIndex];

			//当前映射段距离压缩数据开头的偏移字节数
			int64 currentRegionOffset = std::get<0>(currentRegionInfo);

			//当前映射段的字节数
			unsigned int currentRegionBytes = std::get<1>(currentRegionInfo);

			/*
			实测，每次执行映射的时间几乎可以忽略不计。每次所消耗的时间，用毫秒无法度量，也就是说不到1毫秒。
			*/
			mapped_region region{ file_mapping(packName.data(), read_only),read_only,
				dataOffset + currentRegionOffset, currentRegionBytes };
			char* source = reinterpret_cast<char*>(region.get_address());

			/*
			如果当前文件横跨好几个映射段的话，那么只需要计算其在第一个映射段中的偏移。
			在第二和后续的映射段中，文件数据从映射段的开始处开始。
			*/
			unsigned int offset{};
			if (j == 0)
			{
				offset = fileOffset - currentRegionOffset;
			}

			//在这个映射段中，指向当前文件压缩数据的指针（这个pData所指向的内存地址，必须是两个步长的间隙）
			char* pData = source + offset;

			//当前映射段剩余的字节数
			unsigned int currentRegionLeftBytes = currentRegionBytes - offset;
			//遍历当前文件的每一个步长
			for (; indexStep < sectionNum; ++indexStep)
			{
				//这个步长的字节数
				unsigned int stepBytes = helper.mSections[indexStep + sectionBegIndex];

				//检查当前映射段剩余的字节数是否满足这个步长
				if (currentRegionLeftBytes >= stepBytes)
				{
					//输入流
					//此处，如果压缩数据和解压缩数据不对应的话，那么zlib会抛出异常
					filtering_istream in(zlib_decompressor() | array_source(pData, stepBytes));

					//拷贝
					ung::copy_not_close(in, out);

					//关闭输入流
					ung::close_one_now(in);

					pData += stepBytes;

					//更新当前映射段剩余的字节数
					currentRegionLeftBytes -= stepBytes;
				}
				else
				{
					//不满足这个步长，那么当前映射段剩余的字节数必须为0
					BOOST_ASSERT(currentRegionLeftBytes == 0);

					//跳出遍历步长的循环
					break;
				}
			}//end遍历每一个section
		}//end遍历每一个分布region

		//更新段数的索引
		sectionBegIndex += sectionNum;

		//此时indexStep必须等于sectionNum
		BOOST_ASSERT(indexStep == sectionNum);

		//关闭out
		close_one_now(out);

		//要返回的内存空间
		std::shared_ptr<char> sharedDataPtr(pData,
																	[](char* p)
																	{
																		delete[] p;
																	}
																	);

		return make_pair(sharedDataPtr,orignalBytes);
	}
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE