#include "UPELineSegment.h"

#ifdef UPE_HIERARCHICAL_COMPILE

namespace ung
{
	LineSegment::LineSegment(Vector3 const & start, Vector3 const & end) :
		mStart(start),
		mEnd(end)
	{
	}

	Vector3 const & LineSegment::getStart() const
	{
		return mStart;
	}

	Vector3 const & LineSegment::getEnd() const
	{
		return mEnd;
	}

	Vector3 LineSegment::getDir() const
	{
		auto dir = mEnd - mStart;
		dir.normalize();

		return dir;
	}

	Vector3 LineSegment::getPoint(real_type t) const
	{
		//这里的方向向量，不要单位化
		auto point = mStart + t * (mEnd - mStart);

		return point;
	}
	real_type LineSegment::getLength() const
	{
		return (mEnd - mStart).length();
	}
	real_type LineSegment::getSquaredLength() const
	{
		return (mEnd - mStart).squaredLength();
	}
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE