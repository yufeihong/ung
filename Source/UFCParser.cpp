#include "UFCParser.h"

#ifdef UFC_HIERARCHICAL_COMPILE

#include "UFCException.h"
#include "UFCFilesystem.h"
#include "UFCStringUtilities.h"
#include "boost/interprocess/file_mapping.hpp"
#include <sstream>

namespace ung
{
	Parser::Parser(String const& fullName) :
		mFullName(fullName)
	{
		auto& fs = Filesystem::getInstance();

		const char* fullNameCStr = fullName.c_str();

		auto isExist = fs.isExists(fullNameCStr);
		if (!isExist)
		{
			UNG_EXCEPTION("The file to be parsed does not exist.");
		}

		//确保文件存在后(路径及文件名字正确)，再进行map
		mRegion = boost::interprocess::mapped_region(boost::interprocess::file_mapping(fullName.data(), boost::interprocess::read_only), boost::interprocess::read_only, 0, 0);

		auto validRet = fs.isRegularFile(fullNameCStr);
		BOOST_ASSERT(validRet && "not a file.");

		auto extRet = fs.hasExtension(fullNameCStr);
		BOOST_ASSERT(extRet && "does not have extension.");

		mExtension = fs.getFileExtension(fullNameCStr);

		mSource = reinterpret_cast<char*>(mRegion.get_address());
		BOOST_ASSERT(mSource);
		mSize = mRegion.get_size();
		BOOST_ASSERT(mSize > 0);

		mFileName = fs.getFileName(fullNameCStr);
	}

	Parser::~Parser()
	{
	}

	String const & Parser::getFileFullName() const
	{
		return mFullName;
	}

	String const & Parser::getFileName() const
	{
		return mFileName;
	}

	String const & Parser::getExtension() const
	{
		return mExtension;
	}

	usize const Parser::getSize() const
	{
		return mSize;
	}

	const char* Parser::getSource() const
	{
		return mSource;
	}

	//------------------------------------------------------------------

	PropertyTreeParser::PropertyTreeParser(String const& fileName) :
		Parser(fileName)
	{
		const char* content = getSource();

		String script(content, content + mSize);
		std::istringstream file(script);
		read(file);
	}

	PropertyTreeParser::~PropertyTreeParser()
	{
	}

	void PropertyTreeParser::read(std::istringstream& input)
	{
		using namespace boost::property_tree;

		if (StringUtilities::equalInSensitive(mExtension, "ini"))
		{
			read_ini(input, mTree);
		}
		else if (StringUtilities::equalInSensitive(mExtension, "info"))
		{
			read_info(input, mTree);
		}
		else if (StringUtilities::equalInSensitive(mExtension, "json"))
		{
			read_json(input, mTree);
		}
		else if (StringUtilities::equalInSensitive(mExtension, "xml"))
		{
			read_xml(input, mTree, xml_parser::trim_whitespace);
		}
		else
		{
			BOOST_ASSERT(false && "Unrecognized extension.");
		}
	}

	void PropertyTreeParser::get(String const& key, String& value)
	{
		value = mTree.get<String>(key);
	}

	void PropertyTreeParser::get(char const* key, String& value)
	{
		value = mTree.get<String>(key);
	}

	void PropertyTreeParser::get(char const* key, int& value)
	{
		value = mTree.get<int>(key);
	}

	void PropertyTreeParser::get(String const& key, STL_VECTOR(String)& vec)
	{
		for (auto& elem : mTree.get_child(key))
		{
			vec.push_back(elem.second.get_value<String>());
		}
	}

	void PropertyTreeParser::get_custom(String const& key, STL_VECTOR(String) const& keyVec,STL_VECTOR(STL_VECTOR(String))& values)
	{
		STL_VECTOR(boost::property_tree::ptree) ptVec;
		for (auto& elem : mTree.get_child(key))
		{
			ptVec.push_back(elem.second);
		}

		usize ptVecSize = ptVec.size();
		usize keyVecSize = keyVec.size();

		for (usize i = 0; i < ptVecSize; ++i)
		{
			STL_VECTOR(String) strVec;
			for (usize j = 0; j < keyVecSize; ++j)
			{
				strVec.push_back(ptVec[i].get<String>(keyVec[j]));
			}
			
			values.push_back(strVec);
		}

		BOOST_ASSERT(ptVecSize == values.size());
	}

	void PropertyTreeParser::get_custom(String const& key1, String const& key2, STL_VECTOR(STL_VECTOR(String))& values)
	{
		STL_VECTOR(boost::property_tree::ptree) ptVec;
		for (auto& elem : mTree.get_child(key1))
		{
			ptVec.push_back(elem.second);
		}

		usize ptVecSize = ptVec.size();

		for (usize i = 0; i < ptVecSize; ++i)
		{
			STL_VECTOR(String) temp;
			for (auto& elem : ptVec[i].get_child(key2))
			{
				temp.push_back(elem.second.get_value<String>());
			}

			values.push_back(temp);
		}
	}

	void PropertyTreeParser::get_custom(String const& key1, String const& key2, STL_VECTOR(String) const& keyVec, STL_VECTOR(STL_VECTOR(String))& values)
	{
		STL_VECTOR(boost::property_tree::ptree) ptVec1;
		for (auto& elem : mTree.get_child(key1))
		{
			ptVec1.push_back(elem.second);
		}

		usize ptVec1Size = ptVec1.size();

		for (usize i = 0; i < ptVec1Size; ++i)
		{
			STL_VECTOR(boost::property_tree::ptree) ptVec2;
			for (auto& elem : ptVec1[i].get_child(key2))
			{
				ptVec2.push_back(elem.second);
			}

			usize keyVecSize = keyVec.size();

			for (usize j = 0; j < keyVecSize; ++j)
			{
			}
		}
	}
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE