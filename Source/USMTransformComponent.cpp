#include "USMTransformComponent.h"

#ifdef USM_HIERARCHICAL_COMPILE

#include "USMObject.h"
#include "USMSceneNode.h"

using namespace tinyxml2;

namespace ung
{
	TransformComponent::TransformComponent() :
		ObjectComponent("TransformComponent")
	{
	}

	TransformComponent::~TransformComponent()
	{
	}

	void TransformComponent::init(XMLElement* pComponentElement)
	{
		BOOST_ASSERT(pComponentElement);

		//获取Position attribute中的值
		Vector3 position;
		XMLElement* pPositionElement = pComponentElement->FirstChildElement("Position");
		pPositionElement->QueryDoubleAttribute("x", position.getAddress());
		pPositionElement->QueryDoubleAttribute("y", position.getAddress() + 1);
		pPositionElement->QueryDoubleAttribute("z", position.getAddress() + 2);

		mLocal.setTranslate(position);
	}

	void TransformComponent::update()
	{
		if (mNeedUpdateFlag)
		{
			auto weakNode = mOwner->getAttachedNode();

			//world
			SQT world{};

			//如果对象已经关联到了某个场景节点
			if (!weakNode.expired())
			{
				auto strongNode = weakNode.lock();

				//获取node的movable属性对象
				auto& ma = strongNode->getMovableAttribute();

				world = ma.getToWorldSQT();
			}
			
			mWorld = world * mLocal;

			//在调整八叉树之前设置，否则，在八叉树代码中会引发异常
			mNeedUpdateFlag = false;

			//对象的变换发生了变化，那么必须调整对象在空间管理器树中的悬挂位置
			mOwner->adjustmentHangInSpatial();
		}
	}
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE