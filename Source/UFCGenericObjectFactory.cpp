/*
工厂类维护一个映射，此映射将类型名与创建对象的回调关联起来。然后就可以允许新的派生类通过一对新的方法调用来实现注册和注销。
运行时注册新类的能力，允许工厂方法模式为API创建可扩展的插件接口。
public:
	typedef Resource*(*createResourceCallback)();
	static void registerResource(String const& type,createResourceCallback cb);
	static void unregisterResource(String const& type);
	static Resource* createResource(String const& type);
private:
	using mCallbackMap = std::map<String, createResourceCallback>;
	static mCallbackMap mCallbacks;
*/

/*
工厂模式是一个创建型的设计模式.它允许创建对象时不指定要创建的对象的具体类型.
本质上,工厂方法是构造函数的一般化.

在C++中,构造函数有如下几个限制:
1:没有返回值.
2:命名限制.
3:静态绑定创建.在构造对象时,必须指定编译时能够确定的特定类名.
4:不允许虚构造函数.
相比之下,工厂模式绕开了以上所有限制.从基本层面来看,工厂方法仅是一个普通的方法调用,该调用返回类的实例.它们经常和继承一起使用,即
派生类能够重写工厂方法并返回派生类的实例.常见的做法是使用抽象基类(Abstract Base Class,ABC)实现工厂模式.
ABC是包含一个或多个纯虚成员函数的类,它不是实体类,不能使用new操作符进行实例化,而只能用作一个基类,并由派生类提供纯虚方法的实现.

工厂模式允许用户在运行时决定要创建哪个派生类,而不是在编译时要求用户使用正常的构造函数创建类实例.
各个具体的派生类的头文件仅包含在工厂的.cpp文件中.实际上,这些都是私有头文件,且不需要和API一起发布.因此,用户永远看不到不同派生类的
私有细节.工厂方法,提供了更强大的类构造语义并隐藏了子类的细节.
*/

/*
工厂方法模式是典型的解耦框架.高层模块只需要知道产品的抽象类,其他的实现类都不用关心.
符合迪米特法则,我不需要的就不要去交流.
符合依赖倒置原则,只依赖产品类的抽象.
符合里氏替换原则,使用产品子类替换产品父类,没问题.
*/

/*
对象工厂：
多态(polymorphism)得以提高二进制码的复用性和扩充性。
在steady mode之下你已经拥有了指向多态对象的pointer或reference，你可以对着它们调用成员函数，其
动态型别完全确知（虽然调用者可能并不清楚）。某些情况下，对象的产生需要同样的灵活性，也就是说我们
需要所谓的虚构造函数这种矛盾东西。是的，如果“待产物”的信息是动态的，无法直接被C++构造函数所用，你
就需要虚构造函数。
通常，多态对象是经由new操作符在自由空间中产生的：
class Base
{
};

class Derived : public Base
{
};

class AnotherDerived : public Base
{
};

//Create a Derived object and assign it to a pointer to Base
Base* pB = new Derived;
这里的问题在于，调用new操作符时出现了Derived这一确凿的型别名称。如果想产生AnotherDerived对象，你必须来到以上语句，以AnotherDerived替换Derived。
你无法让上述new操作符以更“多态性”的方式工作：你必须传给它一个型别，而该型别在编译期必须完全确知。

每一个对象的生成都是一段笨拙，静态绑定的死板代码。
面向对象技术总是企图打破“对具象型别（concrete type）的依存性”，然而，至少在C++中，“对象的生成”却将调用者绑死于最底层具象派生类。

当你着手生成一个对象时：
1，你想将这种确切信息留给另一物体（entity）。例如，你可能不直接调用new，而是调用某一具有更高级别的对象中的虚函数Create，从而让客户可以通过多态性
来改变行为。
2，你的确了解型别相关信息，但这种信息无法以C++表达。例如，你可能拥有一个字符串，其中包含“Derived”，但你却无法将这个字符串（而非型别名称本身）传给new。
上面这两点正是object factory所要解决的基本问题。

为什么需要Object Factory
在两种基本情况下我们需要object factory。
第一种情况是，程序库不仅需要“操纵”用户自定义对象，还需要“产生”它们。举个例子，想象你要开发一个“多窗口文档编辑器”框架（framework）。为了使它易于扩充，
你提供一个抽象类Document，用户可以由它派生出其他classes，例如TextDocument或HTMLDocument。这个框架的另一个组件可能是DocumentManager class，
它维护“所有已开启文档”的清单。这里要引入一条好规则：DocumentManager应该知道存在于程序中的每一份文档。因此，只要产生新文档，就必须被添加到DocumentManager
文档清单中，两个操作紧密联系。当两个操作如此紧密联系时，我们最好将它们放到同一个函数中，不要分开执行它们。
class DocumentManager
{
	...
public:
	Document* NewDocument();

protected:
	//工厂方法
	virtual Document* CreateDocument() = 0;

private:
	std::list<Document*> listOfDocs;
};

Document* DocumentManager::NewDocument()
{
	Document* pDoc = CreateDocument();
	listOfDocs.push_back(pDoc);
	...
	return pDoc;
}
成员函数CreateDocument()取代了对new的调用。NewDocument()不能使用new操作符，因为撰写DocumentManager时并不知道将来要产生什么具体文档。为了
使用这个框架，程序员必须继承DocumentManager并改写其中的虚（很可能是纯虚）函数CreateDocument()。将CreateDocument()称为factory method（工厂方法）。

由于derived class知道即将产生之文档的确切型别，因而可以直接调用new操作符。如果采用这种方法，框架本身便无需知道型别信息，只需和base class Document
打交道就好。改写动作非常简单，基本上只需包含一个new调用，像这样：
Document* GraphicDocumentManager::CreateDocument()
{
	return new GraphicDocument;
}

上述框架如欲开启前次保存于磁盘的文档，会带来“object factory必须存在”的第二个的情况。当你将一个对象保存于文件时，你必须以字符串，整数值或某种标识符保存
其实际型别。这种情况下型别相关信息虽然存在，但其存在形式不允许你直接产生C++对象。
这种情形反映出的一般概念是，“待产物”的型别相关信息被推迟至执行期，由终端用户输入或从存储器或网络得到。
从单纯的型别信息到实质对象的生成，最终再将“动态的”信息改装为“静态的”C++型别，这是建立object factory的一个重要议题。

“总而言之，就是你也不知道你都要产生什么，或者你知道却说不出来”。

Object Factories in C++:Classes和Objects
C++构造函数为什么这么死板？为什么没有办法藉由语言本身灵活地产生对象？
Base* pB = new Derived;
什么是class？什么是object？
上述语句的症结在于Derived---它是一个class名称，但我们却希望它产生一个实值（value），也就是一个object。
在C++中classes和objects是不同的东西。classes由程序员产生，objects由程序产生。你无法在执行期产生新的classes，你也无法在编译期产生objects。
classes并不具有第一级状态(first-class status):你无法拷贝一个class，将它保存于变量中，或是从某个函数中传回。

因为静态型别是优化的重要源泉。

C++中的type和value之间存在着裂缝：value拥有type所表示的属性，但type无法靠自身存活。如果想通过完全动态的方式来产生object，你就得有某种方法来表达和
传递纯粹的type，并能够根据请求，从type构造出value。这一点无法做到，所以你必须藉由某种方式将type表示为object---整数或字符串，等等，然后采用某种技巧拿
value换取正确的type，最后再运用那个type产生object。在静态型别语言中，这种“objec-type-object”交易行为对object factory而言是一种基石。

实现一个Object Factory
假设你正在编写一个简单的绘图程序，该程序允许使用者编辑简单的向量图形（包括线，圆，多边形等等）。如果采用典型的面向对象风格，你可以定义一个abstract class 
Shape，让所有图形都派生于它：
class Shape
{
public:
	virtual void Draw() const = 0;
	virtual void Rotate(real_type angle) = 0;
	virtual void Zoom(real_type zoomFactor) = 0;
	...
};
然后你可能会定义一个包含复杂图形的Drawing class，其中实质保存着一个集合（collection）（例如list,vector），其内所含都是pointer to Shape，并提供某些操作，
将复杂图形视为一个整体来处理。两个典型的操作是：1，将图形保存为文件，2，从先前保存的文件中取出图形。
class Drawing
{
public:
	void Save(std::ofstream& outFile);
	void Load(std::ifstream& inFile);
	...
};

void Drawing::Save(std::ofstream& outFile)
{
	...write drawing header

	for (each element in the drawing)
	{
		(current element)->Save(outFile);
	}
}
要求每一个从Shape派生的对象都在文件起始处保存一个整数标识符。每个对象都有自己独一无二的ID。那么，文件的读取将会像这样：
//a unique ID for each drawing object type
namespace DrawingType
{
	const int LINE = 1, POLYGON = 2, CIRCLE = 3;
};

void Drawing::Load(std::ifstream& inFile)
{
	//error handling omitted for simplicity
	while (inFile)
	{
		//read object type
		int drawingType;
		inFile >> drawingType;

		//create a new empty object
		Shape* pCurrentObject;
		switch (drawingType)
		{
			using namespace DrawingType;
		case LINE:
			pCurrentObject = new LINE;
			break;
		case POLYGON:
			pCurrentObject = new Polygon;
			break;
		case CIRCLE:
			pCurrentObject = new Circle;
			break;
		default:
			handle error-unknown object type
		}
		//read the object's contents by invoking a virtual fn
		pCurrentObject->Read(inFile);
		...add the object to the container
	}
}
这的确是一个object factory。它从文件中读取一个型别标识符，根据该符号产生相应的对象，并调用虚函数将该对象的内容从文件中读取出来。唯一的问题是，它违反
了面向对象的最重要规则：
1，它基于型别标记（type tag）执行了switch语句，因而带有switch语句的相应缺点，这正是面向对象程序竭力消除的东西。
2，它在一个源码文件中收集所有关于Shape派生类的相关信息。这同样也是我们应该竭力避免的。理由是，对于所有可能的图形，Drawing::Save的实作文件都因此必须
包含其头文件，这就造成了编译依存性和维护上的瓶颈。
3，它难以扩充。假设要在系统中增加一种新图形，例如Ellipse。那么，除了要产生这个class，还必须在命名空间DrawingType中增加一个独一无二的整数常量，并在
保存Ellipse对象时写入这个常量，还必须在Drawing::Load中为switch语句添加一个比对标记。

我们希望产生一个这样的object factory：它能完成我们的工作，但不存在以上缺点。首先要达到的一个实际目标是，拿掉switch语句，使Line的生成语句可以放在Line的
实作文件中---对Polygon或Circle情况也一样。

要想将代码片段集成起来并操控之，常用的一个手法是函数指针，此处“可定制的代码单元（unit of customizable code）”（也就是switch语句中的每一个入口）可以抽取
至某个带有如下形式的函数内：
Shape* CreateConcreteShape();
我们的“工厂”维护着一个由函数指针组成的集合（collection），ID和“用于产生相应对象”的函数指针之间必须对应。

现在可以开始设计ShapeFactory class了，这个class负责管理所有Shape派生对象的生成：
class ShapeFactory
{
public:
	typedef Shape* (*CreateShapeCallback)();

	//Returns 'true' if registration was successful
	bool RegisterShape(int id, CreateShapeCallback cb);

	//Returns 'true' if the id was registered before
	bool UnregisterShape(int id);

	Shape* CreateShape(int id);

private:
	typedef std::map<int, CreateShapeCallback> CallbackMap;
	CallbackMap callbacks;
};
这个工厂具有可伸缩性，每次你向系统添加一个新的“Shape派生类”时，不必修改它的代码。
每个新Shape都必须对工厂注册，也就是调用RegisterShape，并将“整数标识”和“生成函数的指针”传递给它。
多数情况下factories是singletons。
//Create an anonymous namespace to make the function invisible from other modules
namespace
{
	Shape* CreateLine()
	{
		return new Line;
	}

	const int LINE = 1;

	const bool registered = ShapeFactory::instance().RegisterShape(LINE,CreateLine);
}
ShapeFactory的成员函数将调用转发给callbacks。
bool ShapeFactory::RegisterShape(int id, CreateShapeCallback cb)
{
	//insert()成员函数会传回一个pair，其中包含一个指向刚安插进去元素的迭代器和一个bool。如果该元素的实值以前并不存在，那么bool为true，否则为false。
	return callbacks.insert(CallbackMap::value_type(id,cb)).second;
}

bool ShapeFactory::UnregisterShape(int id)
{
	//erase()会传回被删除的元素个数。
	return callbacks.erase(id) == 1;
}

Shape* ShapeFactory::CreateShape(int id)
{
	auto it = callbacks.find(id);
	if (it == callbacks.end())
	{
		throw std::runtime_error("Unknown Shape ID");
	}

	//invoke the creation function
	return (it->second)();
}

我们获得了一个动态体制，它要求每一种型别的对象都要对工厂进行注册。这就将职责从某个集中点转移到了它们所属的每一个concrete class（具象类）身上。

泛化（Generalization）
具体产品（Concrete product）,工厂以对象的形式交付产品。
抽象产品（Abstract product），工厂以多态方式运作，传回一个指向抽象产品的指针，不带有具体产品的型别信息。
产品型别标识符（Product type identifier），Microsoft's COM factory使用算法来为COM对象产生独一无二的128-bit标识符（globally unique identifiers,GUID）
产品生产者（Product creator），函数或仿函数专门用来生成某一类对象。我们通过函数指针来模塑（modeled）产品生产者。

以上列举的每一个概念几乎都可以转化为Factory template class的template参数。
工厂不必知道具体产品，否则对新增的每一个具体产品我们就得有不同的Factory型别---而我们此刻正竭力让Factory与具体型别分离。所以，只有面对不同的抽象产品，
我们才需要不同的Factory型别。
template <typename AbstractProduct,typename IdentifierType,typename ProductCreator>
class Factory
{
public:
	bool Register(const IdentifierType& id, ProductCreator creator)
	{
		return associations_.insert(AssocMap::value_type(id, creator)).second;
	}

	bool Unregister(const IdentifierType& id)
	{
		return associations_.erase(id) == 1;
	}

	AbstractProduct* CreateObject(const IdentifierType& id)
	{
		typename AssocMap::iterator i = associations_.find(id);
		if (i != associations_.end())
		{
			return (i->second)();
		}
		
		//handle error
	}

private:
	typedef std::map<IdentifierType, ProductCreator> AssocMap;
	AssocMap associations_;
};
我们的泛型工厂应该允许用户定制，也应该提供一套合理的缺省行为。所以，“错误处理”应该从成员函数CreateObject()中抽取出来，放进一个独立的FactoryError policy。
这个policy只定义一个函数：OnUnknownType。Factory会给这个函数一个合适的机会（和足够的消息），让它做出合理选择。
FactoryError policy的定义非常简单。它是一个template，带有两个参数：IdentifierType和AbstractProduct。如果FactoryErrorImpl是FactoryError的一份实作品，
那么下面表达式必然可用：
FactoryErrorImpl<IdentifierType,AbstractProduct> factoryErrorImpl;
IdentifierType id;
AbstractProduct* pProduct = factoryErrorImpl.OnUnknownType(id);
如果OnUnknownType()抛出异常，异常会从Factory中传播开来。

template<typename AbstractProduct,typename IdentifierType,typename ProductCreator,
																												template<typename, typename> class FactoryErrorPolicy>
class Factory : public FactoryErrorPolicy<IdentifierType, AbstractProduct>
{
public:
	AbstractProduct* CreateObject(const IdentifierType& id)
	{
		typename AssocMap::iterator i = associations_.find(id);
		if (i != associations_.end())
		{
			return (i->second)();
		}

		return OnUnknownType(id);
	}
};

template <typename IdentifierType, typename AbstractProductType>
class DefaultFactoryError
{
public:
	//这里没有必要特别取一个与众不同的名称，因为这个型别已经在DefaultFactoryError class template内部，外界不可得见
	class Exception : public std::exception
	{
	public:
		Exception(const IdentifierType& unknownID) :
			unknownID_(unknownID)
		{
		}

		virtual const char* what() const noexcept
		{
			return "Unknown object type passed to Factory";
		}

		const IdentifierType getID()
		{
			return unknownID_;
		}

	private:
		IdentifierType unknownID_;
	};//end class Exception

protected:
	static AbstractProductType* OnUnknownType(const IdentifierType& id)
	{
		throw Exception(id);
	}
};

有个非常有趣的template参数可以作为ProductCreator传递给Factory，那就是Functor<AbstractProductType*>。如果选择这个参数，你将获得很大的灵活性。你可以
通过“一般函数”或“成员函数”或“仿函数”，并对它们“绑定适当参数”，用以生成对象。胶合码（glue code）由Functor提供。

克隆工厂（Clone Factories）
大多数情况下，克隆C++对象是一件有益无害的事。这儿的目标与先前稍有不同：我们不再两手空空产生对象，我们有一个指向多态对象的指针，希望产生一些和它完全相同
的拷贝。但由于不知道这个多态对象的确切型别，所以无法确切知道将产生的是什么样的新对象。这正是问题所在。
克隆对象的常用手法是在base class中声明一个虚函数Clone，并让每一个derived class改写它。
class Shape
{
public:
	virtual Shape* Clone() const = 0;
};

class Line : public Shape
{
public:
	virtual Line* Clone() const
	{
		return new Line(*this);
	}
};
注意此处Line::Clone()传回的不是pointer to Shape。也就是说，我们运用了C++所谓的“协变式返回型别（covariant return type）”特性。基于这一特性，在改写的
虚函数中，你可以传回pointer to derived class，而不必一定传回pointer to base class。

假设你从Line派生出一个DottedLine，但忘记改写DottedLine::Clone()。就有可能会造成程序崩溃。你无法以C++告诉大家：“我定义了这个函数，我要求所有直接或
间接继承它的classes都必须改写该函数”。
一个办法：将Clone()声明为public non-virtual函数，并在其内部调用一个private virtual函数，例如DoClone()，然后检查动态型别是否相等：
class Shape
{
public:
	Shape* Clone() const																//non-virtual
	{
		//delegate to DoClone
		Shape* pClone = DoClone();

		//check for type equivalence
		assert(typeid(*pClone) == typeid(*this));

		return pClone;
	}

private:
	virtual Shape* DoClone() const = 0;										//private
};
唯一的缺点是，你不再能够使用协变式返回型别。

Clone Factory解法不使用虚调用（virtual call），而是采用一个“map查询动作”加上一个“经由函数指针的调用”。
在一个clone factory中，“型别标识符”和“产品”有着相同的型别。你接收“待复制物”并将它视为“型别标识符”，再将该“型别标识符”的拷贝（一个新对象）当作“输出”
传出去。clone factory的IdentifierType是一个pointer to AbstractProductType。真正的交易是，你传递一个pointer to clone factory，得到一个pointer to cloned
object。
clone factory得到一个pointer to AbstractProductType。它将typeid操作符施行于被指对象身上，得到一个reference to std::type_info对象。然后clone factory在
其private map中查询该对象（std::type_info的before()成员函数为“std::type_info对象集”引入了次序关系，使我们有机会运用map执行快速查找）。如果map中没有
找到相应元素，将有一个异常被跑出去。如果找到了相应元素，产品生产者（product creator）会被调用，并接受客户传入的一个pointer to AbstractProductType。

CloneFactory用的是TypeInfo，而不是std::type_info。TypeInfo是包装于pointer to std::type_info之上的一个class，用来定义合适的初始化操作，operator=,
operator==,operator<，这些操作和操作符对map都是必要的。
不再需要IdentifierType，因为标识符的型别是隐式的（implicit）。
template参数ProductCreator缺省为AbstractProductType* (*)(AbstractProductType*)。
IDToProductMap是AssocVector<TypeInfo,ProductCreator>。

template<typename AbstractProductType,typename ProductCreator = AbstractProductType* (*)(const AbstractProductType*),
																								template<typename, typename> class FactoryErrorPolicy = DefaultFactoryError>
class CloneFactory : public FactoryErrorPolicy<TypeInfo, AbstractProductType>
{
public:
	bool Register(const TypeInfo& ti, ProductCreator creator)
	{
		return associations_.insert(IdToProductMap::value_type(ti, creator)).second;
	}

	bool Unregister(const TypeInfo& id)
	{
		return associations_.erase(id) == 1;
	}

	AbstractProductType* CreateObject(const AbstractProductType* model)
	{
		if (model == 0)
		{
			return 0;
		}

		typename IdToProductMap::iterator i = associations_.find(typeid(*model));
		if (i != associations_.end())
		{
			return (i->second)(model);
		}

		return OnUnknownType(typeid(*model));
	}

private:
	typedef AssocVector<TypeInfo, ProductCreator> IdToProductMap;
	IdToProductMap associations_;
};
对于封闭的（也就是你无法修改的）class继承体系来说，如果要复制其中对象，CloneFactory class template是一个完整方案。
C++通过typeid和std::type_info提供执行期型别信息（RTTI）。如果没有RTTI，实作clone factory将会无比艰难。

factory天生具备全局性。
using ShapeFactory = Singleton<Factory<Shape,std::string>>;
低耦合性，虽然factory依然是对象生成的中心机构，但它无需收集继承体系中的所有静态型别相关信息。每个型别有责任将自己注册给factory。
克隆工厂，这是一种可以复制“多态对象”的工厂。
如果你想为某一继承体系提供对象工厂，那么AbstractProductType应指定为该继承体系中的base class。对于克隆工厂也是一样的。
*/