#include "URMVertexBuffer.h"

#ifdef URM_HIERARCHICAL_COMPILE

#include "URMShadowBuffer.h"

namespace ung
{
	VertexBuffer::VertexBuffer()
	{
	}

	VertexBuffer::VertexBuffer(uint32 vertexSize, uint32 numVertices, uint32 usage, bool useShadowBuffer) :
		VideoBuffer(usage,useShadowBuffer),
		mVertexSize(vertexSize),
		mVerticesNum(numVertices)
	{
		//Calculate the size of the vertices
		mSizeInBytes = vertexSize * numVertices;

		//Create a shadow buffer if required
		if (mUseShadowBuffer)
		{
			mShadowBuffer = UNG_NEW ShadowVertexBuffer(vertexSize,numVertices);
		}
	}

	VertexBuffer::~VertexBuffer()
	{
		if (mUseShadowBuffer)
		{
			UNG_DEL(mShadowBuffer);
		}
	}

	uint32 VertexBuffer::getVertexSize() const
	{
		return mVertexSize;
	}

	uint32 VertexBuffer::getVerticesNum() const
	{
		return mVerticesNum;
	}
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE