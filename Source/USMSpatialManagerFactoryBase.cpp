#include "USMSpatialManagerFactoryBase.h"

#ifdef USM_HIERARCHICAL_COMPILE

#pragma warning(push)

//返回局部变量的引用
#pragma warning(disable : 4172)

namespace ung
{
	String const & SpatialManagerFactoryBase::getIdentity() const
	{
		return EMPTY_STRING;
	}

	SceneTypeMask SpatialManagerFactoryBase::getMask() const
	{
		return SceneTypeMask();
	}

	ISpatialManager * SpatialManagerFactoryBase::createInstance(String const & instanceName)
	{
		return nullptr;
	}

	void SpatialManagerFactoryBase::destroyInstance(ISpatialManager * instancePtr)
	{
	}
}//namespace ung

#pragma warning(pop)

#endif//USM_HIERARCHICAL_COMPILE