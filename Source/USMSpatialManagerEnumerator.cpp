#include "USMSpatialManagerEnumerator.h"

#ifdef USM_HIERARCHICAL_COMPILE

#include "USMSpatialManagerFactoryBase.h"
#include "USMSpatialManagerBase.h"

namespace ung
{
	SpatialManagerEnumerator::SpatialManagerEnumerator()
	{
		UNG_LOG("Create spatial manager enumerator.")
	}

	SpatialManagerEnumerator::~SpatialManagerEnumerator()
	{
	}

	void SpatialManagerEnumerator::addFactory(ISpatialManagerFactory * factoryPtr)
	{
		BOOST_ASSERT(factoryPtr);

		mSpatialManagerFactorys[factoryPtr->getIdentity()] = factoryPtr;
	}

	void SpatialManagerEnumerator::removeFactory(ISpatialManagerFactory * factoryPtr)
	{
		auto ret = mSpatialManagerFactorys.erase(factoryPtr->getIdentity());

		BOOST_ASSERT(ret == 1);
	}

	ISpatialManager * SpatialManagerEnumerator::createSpatialManager(String const & factoryIdentity, String const & instanceName)
	{
		if (mSpatialManagers.find(instanceName) != mSpatialManagers.end())
		{
			UNG_EXCEPTION(instanceName + " has already been existed.");
		}

		auto ret = mSpatialManagerFactorys.find(factoryIdentity);

		if (ret == mSpatialManagerFactorys.end())
		{
			UNG_EXCEPTION(factoryIdentity + " has not been added to the enumerator.");
		}

		auto factory = ret->second;

		auto instance = factory->createInstance(instanceName);

		mSpatialManagers[instance->getManagerName()] = instance;

		return instance;
	}

	ISpatialManager * SpatialManagerEnumerator::createSpatialManager(SceneTypeMask mask, String const & instanceName)
	{
		if (mSpatialManagers.find(instanceName) != mSpatialManagers.end())
		{
			UNG_EXCEPTION(instanceName + " has already been existed.");
		}

		ISpatialManagerFactory* factory{};

		auto beg = mSpatialManagerFactorys.begin();
		auto end = mSpatialManagerFactorys.end();
		while (beg != end)
		{
			if (beg->second->getMask() & mask)
			{
				factory = beg->second;
			}

			++beg;
		}

		if (!factory)
		{
			UNG_EXCEPTION("Can not find the SpatialManagerFactory of the given mask.");
		}

		auto instance = factory->createInstance(instanceName);

		mSpatialManagers[instance->getManagerName()] = instance;

		return instance;
	}

	void SpatialManagerEnumerator::destroySpatialManager(ISpatialManager * spatialManagerPtr)
	{
		if (spatialManagerPtr)
		{
			auto eraseRet = mSpatialManagers.erase(spatialManagerPtr->getManagerName());
			BOOST_ASSERT(eraseRet == 1);

			spatialManagerPtr->getCreator()->destroyInstance(spatialManagerPtr);
		}
	}

	ISpatialManager * SpatialManagerEnumerator::getSpatialManager(String const & instanceName) const
	{
		auto ret = mSpatialManagers.find(instanceName);

		if (ret == mSpatialManagers.end())
		{
			return nullptr;
		}

		return ret->second;
	}

	bool SpatialManagerEnumerator::hasSpatialManager(String const & instanceName) const
	{
		if (getSpatialManager(instanceName))
		{
			return true;
		}
		
		return false;
	}

	bool SpatialManagerEnumerator::hasSpatialManager(ISpatialManagerFactory * factory) const
	{
		auto beg = mSpatialManagers.begin();
		auto end = mSpatialManagers.end();

		while (beg != end)
		{
			if (beg->second->getCreator() == factory)
			{
				return true;
			}

			++beg;
		}

		return false;
	}
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE