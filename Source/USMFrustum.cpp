#include "USMFrustum.h"

#ifdef USM_HIERARCHICAL_COMPILE

#include "UNGRoot.h"
#include "UIFIRenderSystem.h"

namespace ung
{
	Frustum::Frustum() :
		mAspect(4.0 / 3.0),
		mFOVy(Radian(Consts<real_type>::THIRDPI)),
		mNearDist(1.0),
		mFarDist(1000.0)
	{
		updateProjectionMatrix();
	}

	Frustum::Frustum(real_type aspect, Radian fovY, real_type nDis, real_type fDis) :
		mAspect(aspect),
		mFOVy(fovY),
		mNearDist(nDis),
		mFarDist(fDis)
	{
		updateProjectionMatrix();
	}

	Frustum::~Frustum()
	{
	}

	void Frustum::setFOVy(const Radian & fovy)
	{
		mFOVy = fovy;

		updateProjectionMatrix();
	}

	const Radian & Frustum::getFOVy() const
	{
		return mFOVy;
	}

	void Frustum::setNearClipDistance(real_type nearDist)
	{
		mNearDist = nearDist;

		updateProjectionMatrix();
	}

	real_type Frustum::getNearClipDistance() const
	{
		return mNearDist;
	}

	void Frustum::setFarClipDistance(real_type farDist)
	{
		mFarDist = farDist;

		updateProjectionMatrix();
	}

	real_type Frustum::getFarClipDistance() const
	{
		return mFarDist;
	}

	void Frustum::setAspectRatio(real_type ratio)
	{
		mAspect = ratio;

		updateProjectionMatrix();
	}

	real_type Frustum::getAspectRatio() const
	{
		return mAspect;
	}

	const Matrix4& Frustum::getProjectionMatrixOpenGL() const
	{
		BOOST_ASSERT(Root::getInstance().getCurrentRenderSystemType() == RenderSystemType::RST_GL);

		return mProjMat;
	}

	const Matrix4& Frustum::getProjectionMatrixD3D() const
	{
		BOOST_ASSERT(Root::getInstance().getCurrentRenderSystemType() == RenderSystemType::RST_D3D);

		return mProjMat;
	}

	real_type Frustum::getNearPlaneWidth() const
	{
		return mAspect * getNearPlaneHeight();
	}

	real_type Frustum::getNearPlaneHeight() const
	{
		return Math::calTan(mFOVy * 0.5) * mNearDist * 2.0;
	}

	real_type Frustum::getFarPlaneWidth() const
	{
		return mAspect * getFarPlaneHeight();
	}

	real_type Frustum::getFarPlaneHeight() const
	{
		return Math::calTan(mFOVy * 0.5) * mFarDist * 2.0;
	}

	void Frustum::updateProjectionMatrix()
	{
		auto renderSystemType = Root::getInstance().getCurrentRenderSystemType();

		switch (renderSystemType)
		{
		case RenderSystemType::RST_D3D:
			mProjMat = Matrix4::makePerspectiveD3D(mAspect, mFOVy.getRadian(), mNearDist, mFarDist);
		case RenderSystemType::RST_GL:
			mProjMat = Matrix4::makePerspectiveOpenGL(mAspect, mFOVy.getRadian(), mNearDist, mFarDist);
		}
	}
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE