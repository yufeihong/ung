#include "UPESphere.h"

#ifdef UPE_HIERARCHICAL_COMPILE

#include "UPEAABB.h"

namespace ung
{
	Sphere::Sphere() :
		mC(0.0, 0.0, 0.0),
		mR(0.0)
	{
	}

	Sphere::~Sphere()
	{
	}

	Sphere::Sphere(const Vector3& c, const real_type r) :
		mC(c),
		mR(r)
	{
	}

	Sphere::Sphere(const Sphere& s) :
		mC(s.mC),
		mR(s.mR)
	{
	}

	Sphere& Sphere::operator=(const Sphere& s)
	{
		if (this != &s)
		{
			mC = s.mC;
			mR = s.mR;
		}

		return *this;
	}

	void Sphere::setCenter(const Vector3& c)
	{
		mC = c;
	}

	void Sphere::setRadius(const real_type r)
	{
		mR = r;
	}

	const Vector3 Sphere::getCenter() const
	{
		return mC;
	}

	const real_type Sphere::getRadius() const
	{
		return mR;
	}

	void Sphere::merge(const Sphere& s)
	{
		Vector3 sc = s.getCenter();
		real_type sr = s.getRadius();

		Vector3 centerDiff = sc - mC;
		real_type centerDiffLengthSq = centerDiff.squaredLength();
		real_type radiusDiff = sr - mR;

		if (radiusDiff * radiusDiff >= centerDiffLengthSq)
		{
			if (radiusDiff <= 0.0)
			{
				return;
			}
			else
			{
				mC = sc;
				mR = sr;
				return;
			}
		}

		real_type centerDiffLength = Math::calSqrt(centerDiffLengthSq);
		real_type t = (centerDiffLength + radiusDiff) / (2.0 * centerDiffLength);
		mC = mC + centerDiff * t;
		mR = 0.5 * (centerDiffLength + mR + sr);
	}

	AABB Sphere::getAABB() const
	{
		return AABB(Vector3(-mR),Vector3(mR));
	}

	AABB Sphere::getWorldAABB() const
	{
		return AABB(Vector3(mC - mR),Vector3(mC + mR));
	}
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

 /*
 向量记法的球的隐式表示:
 |p - c| = r
 p是球表面上的任意一点.如果要让球内部的点p也满足上式,就必须将等号换为<=.
 上式也是2D圆的隐式表示.
 */