#include "URMPackResource.h"

#ifdef URM_HIERARCHICAL_COMPILE

namespace ung
{
	PackResource::PackResource(String const& packFullName) :
		mFullName(packFullName)
	{
		//���mHelperInfos
		Unpack::analysisPack(packFullName.data(),mHelperInfos);
	}

	PackResource::~PackResource()
	{
	}

	std::pair<std::shared_ptr<char>, int64> PackResource::extractData(const char* fileFullName)
	{		
		return Unpack::extractToMemory(mHelperInfos,fileFullName);
	}
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE