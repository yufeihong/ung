#include "UFCPluginManager.h"

#ifdef UFC_HIERARCHICAL_COMPILE

#include "UFCParser.h"
#include "UFCFilesystem.h"
#include "UFCDLL.h"
#include "UFCPlugin.h"
#include "UFCStringUtilities.h"
#include "UFCLogManager.h"

namespace ung
{
	typedef void(*DLL_START_PLUGIN)();
	typedef void(*DLL_STOP_PLUGIN)();

	PluginManager::PluginManager()
	{
		UNG_LOG("Create plugin manager.");
	}

	PluginManager::~PluginManager()
	{
		unloadAll();
	}

	void PluginManager::load(const String& pluginFile)
	{
		//文件系统
		auto& fs = Filesystem::getInstance();

		String thePluginFileName = pluginFile;
		//如果dll文件名中没有后缀的话，则添加上
		if (!StringUtilities::endWithInSensitive(thePluginFileName, ".dll"))
		{
			thePluginFileName += ".dll";
		}

		//检查dll文件是否存在
		auto isExist = fs.isExists(thePluginFileName.data());
		//如果不存在
		if (!isExist)
		{
			//在插件路径中再次检查
			thePluginFileName = global_plugins_path + thePluginFileName;
			isExist = fs.isExists(thePluginFileName.data());
			BOOST_ASSERT(isExist);
		}

		//创建DLL对象
		DLL* pDLL = UNG_NEW DLL(thePluginFileName);
		//把创建的DLL对象给保存到容器中
		mDLLs.push_back(pDLL);
		//load dll
		pDLL->load();

		//执行dllCreatePlugin()
		DLL_START_PLUGIN pFunc = (DLL_START_PLUGIN)pDLL->getSymbol("dllCreatePlugin");
		BOOST_ASSERT(pFunc && "Cannot find symbol dllCreatePlugin in library.");

		pFunc();
	}

	void PluginManager::loadAll(const String& pluginsFile)
	{
		//解析配置文件
		static String pluginPath("../../../../Resource/Config/");
		String pluginsConfigFileFullName = pluginPath + pluginsFile;
		PropertyTreeParser pars(pluginsConfigFileFullName);
		STL_VECTOR(String) pluginList;

		//把dll的名字给保存到容器中
		pars.get("PluginsConf.Plugins",pluginList);

		//遍历所有的dll名字
		for (auto it = pluginList.begin(); it != pluginList.end(); ++it)
		{
			load(*it);
		}
	}

	void PluginManager::unloadAll()
	{
		for (auto it = mDLLs.begin(); it != mDLLs.end(); ++it)
		{
			DLL_STOP_PLUGIN pFunc = (DLL_STOP_PLUGIN)(*it)->getSymbol("dllDestroyPlugin");
			BOOST_ASSERT(pFunc && "Cannot find symbol dllDestroyPlugin in library.");

			//在这个函数调用中，会调用PluginManager::uninstall
			pFunc();
			(*it)->unload();
			UNG_DEL(*it);
		}
		mDLLs.clear();
	}

	void PluginManager::install(Plugin* plugin)
	{
		auto it = std::find(mPlugins.begin(), mPlugins.end(), plugin);
		if (it == mPlugins.end())
		{
			mPlugins.push_back(plugin);
			plugin->install();
		}
	}

	void PluginManager::uninstall(Plugin* plugin)
	{
		auto it = std::find(mPlugins.begin(), mPlugins.end(), plugin);
		if (it != mPlugins.end())
		{
			plugin->uninstall();
			mPlugins.erase(it);
		}
	}
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE