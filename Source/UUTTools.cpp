#include "UUTTools.h"

#ifdef UUT_HIERARCHICAL_COMPILE

#include <direct.h>

namespace ung
{
	void uutSetAttribLocation(GLuint programHandle, std::initializer_list<const char*> lst)
	{
		GLuint index = 0;

		for (auto& attrib : lst)
		{
			glBindAttribLocation(programHandle,index++,attrib);
		}
	}

	bool uutIsExtSupported(const char* extension)
	{
		//向OpenGL查询当前实现支持多少个扩展。
		GLint nNumExtensions;
		glGetIntegerv(GL_NUM_EXTENSIONS, &nNumExtensions);

		for (GLint i = 0; i < nNumExtensions; i++)
		{
			//通过调用glGetStringi函数来获取特定扩展的名称。
			if (strcmp(extension, (const char*)glGetStringi(GL_EXTENSIONS, i)) == 0)
			{
				return true;
			}
		}

		return false;
	}

	void uutDrawLine(const Vector3 &from, const Vector3 &to, const Vector3 &color)
	{
		glBegin(GL_LINES);

		glColor3f(color.x, color.y, color.z);

		glVertex3f(from.x, from.y, from.z);
		glVertex3f(to.x, to.y, to.z);

		glEnd();
	}

	/*
	Find the window.  If active, set and return false.

	Checking for Multiple Instances of Your Game:
	If your game takes a moment to get around to(花足够的时间去做某事) creating a window, a 
	player might get a little impatient and double-click the game’s icon a few times. If you don’t 
	take the precaution(预防措施) of handling this problem, you’ll find that users can quickly create 
	a few dozen instances of your game, none of which will properly(适当地，正确地) initialize.
	You should create a splash screen to help minimize this problem, but it’s still a good idea to detect
	an existing instance of your game.
	*/
	bool uutIsOnlyInstance(const wchar_t* title)
	{
		/*
		The Windows CreateMutex() API is used to gate only one instance of your game to the
		window detection code.

		A mutex is a process synchronization mechanism and is common to any multitasking
		operating system. It is guaranteed to create one mutex with the identifier title for all processes
		running on the system. If it can’t be created, then another process has already created it.

		Only one game instance may have this mutex at a time.
		*/
		HANDLE handle = CreateMutex(NULL, TRUE, title);

		if (GetLastError() != ERROR_SUCCESS)
		{
			/*
			The FindWindow() API,you call it with your game’s title, which uniquely identifies your game.
			*/
			HWND hWnd = FindWindow(title, NULL);
			if (hWnd)
			{
				//An instance of your game is already running.
				ShowWindow(hWnd, SW_SHOWNORMAL);
				SetFocus(hWnd);
				SetForegroundWindow(hWnd);
				SetActiveWindow(hWnd);

				return false;
			}
		}

		return true;
	}

	bool uutCheckStorage(const unsigned __int64 diskSpaceNeeded)
	{
		//Check for enough free disk space on the current disk.
		/*
		int _getdrive():
		获取当前磁盘驱动器。
		返回当前值 (默认) 驱动器 (1=A，2=B，等等)。
		*/
		int const drive = _getdrive();

		/*
		struct _diskfree_t
		{
			//磁盘上的已用群集和可用群集的总数。
			unsigned total_clusters;
			//磁盘上未使用的群集数。
			unsigned avail_clusters;
			//每个群集中的扇区数
			unsigned sectors_per_cluster;
			//每个扇区的大小（以字节为单位）。
			unsigned bytes_per_sector;
		};
		*/
		struct _diskfree_t diskfree;

		/*
		unsigned _getdiskfree(unsigned drive,struct _diskfree_t* driveinfo):
		使用有关磁盘驱动器的信息填充 _diskfree_t 结构。
		*/
		unsigned ret = _getdiskfree(drive,&diskfree);
		BOOST_ASSERT(ret == 0);

		unsigned __int64 const neededClusters = diskSpaceNeeded / (diskfree.sectors_per_cluster * diskfree.bytes_per_sector);

		if (diskfree.avail_clusters < neededClusters)
		{
			return false;
		}

		return true;
	}

	bool uutCheckMemory(const unsigned __int64 physicalRAMNeeded, const unsigned __int64 virtualRAMNeeded)
	{
		/*
		MEMORYSTATUSEX reflects the state of memory at the time of the call. It also reflects the size of the paging file at that time. The operating system can enlarge the paging file up to the maximum size set by the administrator.

		typedef struct _MEMORYSTATUSEX
		{
			//The size of the structure, in bytes.You must set this member before calling GlobalMemoryStatusEx.
			DWORD dwLength;
			//A number between 0 and 100 that specifies the approximate percentage of physical memory that is in use (0 indicates no memory use and 100 indicates full memory use).
			DWORD dwMemoryLoad;
			//The amount of actual physical memory, in bytes.
			DWORDLONG ullTotalPhys;
			//The amount of physical memory currently available, in bytes. This is the amount of physical memory that can be immediately reused without having to write its contents to disk first. It is the sum of the size of the standby, free, and zero lists.
			DWORDLONG ullAvailPhys;
			//The current committed memory limit for the system or the current process, whichever is smaller, in bytes. To get the system-wide committed memory limit, call GetPerformanceInfo.
			DWORDLONG ullTotalPageFile;
			//The maximum amount of memory the current process can commit, in bytes. This value is equal to or smaller than the system-wide available commit value. To calculate the system-wide available commit value, call GetPerformanceInfo and subtract the value of CommitTotal from the value of CommitLimit.
			DWORDLONG ullAvailPageFile;
			//The size of the user-mode portion of the virtual address space of the calling process, in bytes. This value depends on the type of process, the type of processor, and the configuration of the operating system. For example, this value is approximately 2 GB for most 32-bit processes on an x86 processor and approximately 3 GB for 32-bit processes that are large address aware running on a system with 4-gigabyte tuning enabled.
			DWORDLONG ullTotalVirtual;
			//The amount of unreserved and uncommitted memory currently in the user-mode portion of the virtual address space of the calling process, in bytes.
			DWORDLONG ullAvailVirtual;
			//Reserved. This value is always 0.
			DWORDLONG ullAvailExtendedVirtual;
			} MEMORYSTATUSEX,*LPMEMORYSTATUSEX;
		*/
		MEMORYSTATUSEX status;
		status.dwLength = sizeof (status);

		/*
		BOOL WINAPI GlobalMemoryStatusEx(_Inout_ LPMEMORYSTATUSEX lpBuffer):
		Retrieves information about the system's current usage of both physical and virtual 
		memory.

		The information returned by the GlobalMemoryStatusEx function is volatile.There is no 
		guarantee that two sequential calls to this function will return the same information.
		*/
		auto ret = GlobalMemoryStatusEx(&status);
		BOOST_ASSERT(ret);

		if (status.ullTotalPhys < physicalRAMNeeded)
		{
			//Not enough physical memory.
			return false;
		}

		if (status.ullAvailVirtual < virtualRAMNeeded)
		{
			//Not enough virtual memory.
			return false;
		}

		/*
		//检查可以使用的最大连续内存空间。
		char* buff = new char[(unsigned int)virtualRAMNeeded];
		if (buff)
		{
			delete[] buff;
		}
		else
		{
			//Not enough contiguous available memory.
			return false;
		}
		*/

		return true;
	}

	bool uutCheckCPUSpeed(unsigned long cpuSpeedNeeded)
	{
		unsigned long BufSize = sizeof(unsigned long);
		unsigned long dwMHz = 0;
		unsigned long type = REG_DWORD;
		HKEY hKey;

		//Open the key where the proc speed is hidden:
		long lError = RegOpenKeyEx(HKEY_LOCAL_MACHINE,
										L"HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\0",
										0, KEY_READ, &hKey);

		if (lError == ERROR_SUCCESS)
		{
			//query the key:
			RegQueryValueEx(hKey, L"~MHz", NULL, &type, (LPBYTE)&dwMHz, &BufSize);
		}

		if (dwMHz < cpuSpeedNeeded)
		{
			return false;
		}

		return true;
	}
}//namespace ung

#endif//UUT_HIERARCHICAL_COMPILE