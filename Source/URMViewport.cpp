#include "URMViewport.h"

#ifdef URM_HIERARCHICAL_COMPILE

namespace ung
{
	Viewport::Viewport(int32 zOrder) :
		mCamera(nullptr),
		mRenderTarget(nullptr),
		mZOrder(zOrder)
	{
	}

	Viewport::~Viewport()
	{
		mCamera = nullptr;
		mRenderTarget = nullptr;
	}

	void Viewport::setZOrder(int32 zOrder)
	{
		mZOrder = zOrder;
	}

	int32 Viewport::getZOrder() const
	{
		return mZOrder;
	}

	void Viewport::attachRenderTarget(IRenderTarget * renderTarget)
	{
		BOOST_ASSERT(renderTarget);

		BOOST_ASSERT(!mRenderTarget);

		mRenderTarget = renderTarget;
	}

	void Viewport::detachRenderTarget()
	{
		BOOST_ASSERT(mRenderTarget);

		mRenderTarget = nullptr;
	}

	IRenderTarget* Viewport::getAttachedRenderTarget() const
	{
		BOOST_ASSERT(mRenderTarget);

		return mRenderTarget;
	}

	void Viewport::attachCamera(Camera* cam)
	{
		BOOST_ASSERT(cam);

		BOOST_ASSERT(!mCamera);

		mCamera = cam;
	}

	void Viewport::detachCamera()
	{
		BOOST_ASSERT(mCamera);

		mCamera = nullptr;
	}

	Camera* Viewport::getAttachedCamera() const
	{
		BOOST_ASSERT(mCamera);

		return mCamera;
	}
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

/*
A viewport is the meeting of a camera and a rendering surface,the camera renders the scene 
from a viewpoint,and places its results into some subset of a rendering target,which may be 
the whole surface or just a part of the surface.Each viewport has a single camera as source 
and a single target as destination.A camera only has 1 viewport, but a render target may have 
several.A viewport also has a Z-order,i.e. if there is more than one viewport on a single render 
target and they overlap, one must obscure the other in some predetermined way.
(一个viewport就是摄像机和渲染表面的一个meeting。一个viewport可能会覆盖整个渲染表面，也可能只
覆盖了渲染表面的一部分。每一个viewport都有一个唯一的摄像机作为其source，每一个viewport都有一个
渲染表面作为其destination。一个摄像机只有一个viewport，但是一个渲染表面可能会有多个viewport，
而且这多个viewport可能会重叠，所以viewport要具备一个Z order属性。)
注意：
1，viewport和摄像机是一对一的关系。
2，viewport把摄像机和渲染表面给联系了起来。
*/