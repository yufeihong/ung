#include "UPEQuadrilateral.h"

#ifdef UPE_HIERARCHICAL_COMPILE

#include "UPETriangle.h"

namespace ung
{
	Quadrilateral::Quadrilateral(Vector3 const & a, Vector3 const & b, Vector3 const & c, Vector3 const & d)
	{
		mVertices[0] = a;
		mVertices[1] = b;
		mVertices[2] = c;
		mVertices[3] = d;
	}

	Quadrilateral::Quadrilateral(Triangle const & triangle, Vector3 const & p)
	{
		auto v = triangle.getVertices();

		mVertices[0] = v[0];
		mVertices[1] = v[1];
		mVertices[2] = v[2];
		mVertices[3] = p;
	}

	std::array<Vector3, 4> const & Quadrilateral::getVertices() const
	{
		return mVertices;
	}
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE