/*
技术（Techniques）
编译期Assertion
表达式（expression）在编译期评估所得结果是个常数，这意味着你可以利用编译器（而非代码）来作检查。这个想法是传给编译器一个语言构造，如果是非零表达式便
合法，零表达式则非法。于是当你传入一个表达式而其值为零时，编译器会发出一个编译期错误的信息
大小为0的array是非法的：
#define STATIC_CHECK(expr) {char unnamed[(expr) ? 1 : 0];}
举例：
template<typename To,typename From>
To safe_reinterpret_cast(From from)
{
	STATIC_CHECK(sizeof(From) <= sizeof(To));

	return reinterpret_cast<To>(from);
}

模板偏特化
Partial template specialization让你在template的所有可能实体中特化出一组子集。
template<typename Window,typename Controller>
class Widget
{
	...generic implementation...
};

template<>
class Widget<ModalDialog,MyController>
{
	...specialized implementation...
};
有了这个Widget特化定义后，如果你定义Widget<ModalDialog,MyController>对象，编译器就使用上述定义，如果你定义其他泛型对象，编译器就使用原本的泛型定义。

然而有时候你也许想要针对任意Window并搭配一个特定的MyController来特化Widget。这时就需要模板偏特化机制：
template<typename Window>																		//Window仍是泛化
class Widget<Window,MyController>																//MyController是特化
{
	...partially specialized implementation...
};
通常在一个class template偏特化定义中，你只会特化某些template参数而留下其他泛化参数。当你在程序中具体实现上述class template时，编译器会试着找出最匹配的
定义。这个寻找过程十分复杂精细，允许你以富创意的方式来进行偏特化。例如，假设你有一个Button class template，它有一个template参数，那么，你不但可以拿Window
搭配特定MyController来特化Widget，还可以拿任意Button搭配特定MyController来偏特化Widget:
template<typename ButtonArg>
class Widget<Button<ButtonArg>,MyController>
{
	...further specialized implementation...
};

偏特化机制不能用在函数身上（不论成员函数或非成员函数）。
虽然你可以全特化class template中的成员函数，但你不能偏特化它们。

局部类
你可以在函数中定义class：
void Fun()
{
	class Local
	{
		...member variables...
		...member function definitions...
	};

	...code using Local...
}
local class不能定义static成员变量，也不能访问non-static局部变量。local class令人感兴趣的是，可以在template函数中被使用。定义于template函数内的local classes
可以运用函数的template参数。以下所列代码中有一个MakeAdapter template function，可以将某个接口转接为另一个接口。
class Interface
{
public:
	virtual void Fun() = 0;
	...
};

template<typename T,typename P>
Interface* MakeAdapter(const T& obj, const P& arg)
{
	class Local : public Interface
	{
	public:
		Local(const T& obj, const P& arg) :
			mObj(obj),
			mArg(arg)
		{
		}

		virtual void Fun()
		{
			mObj.Call(mArg);
		}

	private:
		T mObj;
		P mArg;
	};//end class Local

	return new Local(obj, arg);
}
事实证明，任何运用local classes的手法，都可以改用“函数外的template class”来完成。换言之，并非一定得local class不可。不过local classes可以简化实作并
提高符号的地域性。
外界不能继承一个隐藏于函数内的class。
可以运用local class产生所谓的“弹簧垫”函数（trampoline function）。

常整数映射为型别（Mapping Integral Consts to Types）
template <int v>
struct Int2Type
{
	enum { value = v };
};
Int2Type会根据引数所得的不同数值来产生不同型别。这是因为“不同的template具现体”本身便是“不同的型别”。因此Int2Type<0>不同于Int2Type<1>，以此类推。
用来产生型别的那个数值是一个枚举值。
当你想把常数视同型别时，便可采用上述Int2Type。这么一来便可根据编译期计算出来的结果选用不同的函数。实际上你可以运用一个常数达到静态分派（static dispatching）
功能。（运用Int2Type<true>和Int2Type<false>来对重载函数进行决议）
一般而言，符合下列两个条件便可使用Int2Type
1，有必要根据某个编译期常数调用一个或数个不同的函数。
2，有必要在编译期实施分派。

编译期间侦测可转换性和继承性
在泛型函数中，如果你确知某个class实作有某个接口，你便可以采用某个最佳算法。在编译期发现这样的关系，意味着不必使用dynamic_cast---它会耗损执行期效率。
如何测知任意型别T是否可以自动转换为型别U？
有个方案可以解决问题，并且只需仰赖sizeof。sizeof有着惊人的威力：你可以把sizeof用在任何表达式身上，不论后者有多复杂。sizeof会直接传回大小，不需托到
执行期才评估。这意味着sizeof可以感知重载（overloading），模板具现（template instantiation）。转换规则（conversion rules），或任何可发生于C++表达式
身上的机制。事实上sizeof背后隐藏了一个“用以推导表达式型别”的完整设施。最终sizeof会丢弃表达式并传回其大小。
“侦测转换能力”的想法是：合并运用sizeof和重载函数。我们提供两个重载函数：其中一个接受U（目标），另一个接受“任何其他型别”。我们以型别T的暂时对象来调用
这些重载函数，而“T是否可转化为U”正是我们想知道的。如果接受U的那个函数被调用，我们就知道T可转换为U，否则T便无法转化为U。为了知道哪一个函数被调用，我们
对这两个重载函数安排大小不同的返回型别，并以sizeof来区分其大小。型别本身无关紧要，重要的是其大小必须不同。
typedef char Small;
class Big
{
	char dummy[2];
};
接下来需要两个重载函数：
template<typename U>
Small Test(U);
但接下来，我该如何写出一个可接受任何其他对象的函数呢？因为template总是要求最佳匹配条件，因而遮蔽了转换动作。我需要一个“比自动转换稍差”的匹配，也就是说
我需要一个“唯有在自动转换缺席情况下”才会中选的转换。
所谓的“省略符比对”准则，那是最坏的情况了，于是我写出这样一个函数：
Big Test(...);
我们并不真正调用这个函数，它甚至没有被实作出来。sizeof并不对其引数求值。
现在我们传一个T对象给Test()，并将sizeof施行于其传回值身上：
const bool convExists = sizeof(Test(T())) == sizeof(Small);
就是这样。Test()会取得一个default构造对象T()，然后sizeof会取得这一表达式结果的大小，可能是sizeof(Small)或sizeof(Big)，取决于编译器是否找到转换方式。
这里还有一个小问题，万一T让自己的default构造函数为private，那么T()会编译失败。有一个简单的解法：以一个“稻草人函数（strawman function）”传回一个T
对象。记住，我们处于sizeof世界中，并不会真有任何表达式被求值（evaluated）。
T MakeT();																				//not implemented
const bool convExists = sizeof(Test(MakeT())) == sizeof(Small);
像MakeT()和Test()这样的函数，它们不只没做任何事情，甚至根本不真正存在。
template<typename T,typename U>
class Conversion
{
	typedef char Small;
	class Big
	{
		char dummy[2];
	};

	static Small Test(U);
	static Big Test(...);
	static T MakeT();

public:
	enum
	{
		exists = sizeof(Test(MakeT())) == sizeof(Small);
	};
};

int main()
{
	cout << Conversion<real_type, int>::exists << endl;
	cout << Conversion<char, char*>::exists << endl;
	cout << Conversion<size_t, vector<int>>::exists << endl;
}
输出100。注意：虽然std::vector实作出一个接受size_t引数的构造函数，但上述转换测试却传回0，因为该构造函数是explicit（explicit构造函数无法担任转换函数）。
*/

/*
模板元编程：
由泛型编程衍生出的模板元编程(template meta-programming)，简称元编程。这是一种对“类型”计算的程序。模板元编程本质上是泛型编程的一个子集，从广义
上来说，所有使用template的泛型代码都可以称作是元程序，因为泛型代码不是真正可编译执行的代码，它们只是定义了代码的产生规则，是用来生成代码的“模板”。
模板元编程的执行是在编译期，它把编译器变成了元程序的解释器，可以把C++的类型体系像面团一样捏来捏去，肆意打碎再任意组合起来，拥有近乎不可思议的“魔
力”。它操作的对象也不是普通的变量，因此不能使用运行时的C++关键字（如，if、else、for），可用的语法元素相当有限，最常用的是：
1：enum、static，用来定义编译期的整数常量。
2：typedef、using，最重要的元编程关键字，用于定义元数据。
3：template，模板元编程的起点，主要用于定义元函数。
4：“::”，域运算符，用于解析类型作用域获取计算结果（元数据）。

元数据：
元编程可操作的数据就称为“元数据”(meta data),也就是C++编译器在编译期可操作的数据，它是元编程的基础。
元数据都是不可变的，不能够就地修改，最常见的元数据是整数和C++的类型(type)。如果对元数据再进行细分归类，则元数据又可以分成整数元数据、值型元数据
（int、float等POD值类型）、函数元数据（函数类型）、类元数据（class、struct等用户自定义类型）等。
//元数据meta_data1,值为int
typedef int meta_data1;
//元数据meta_data2，值为vector<float>
typedef std::vector<float> meta_data2;
或
using meta_data2 = std::vector<float>;
实际上C++11中的using关键字还可以声明模板类别名，功能更强大，已经超越了typedef。

元函数：
元函数（meta function）是模板元编程中用于操作处理元数据的“构件”，可以在编译期被“调用”，因为其功能和形式类似运行时的函数而得名，是元编程中最重要的
构件。
元函数实际上表现为C++中的一个类或者类模板，它的通常形式是：
template<typename arg1,typename arg2,...>					//元函数参数列表
struct meta_function														//元函数名
{
	typedef some_define type;											//元函数返回的元数据
	static int const value = some_int;								//元函数返回的整数
};																					//使用分号结束元函数的定义
编写元函数就像是编写一个普通的运行时函数，但形式上却是一个类模板。
元函数也可以没有返回值（即不定义内部类型type），也可以有重载，也可以有缺省参数，也可以分为无参、单参、多参等类别，但元函数没有普通函数参数传值、传
引用的区别，也没有函数指针的概念，而且如果需要，元函数可以使用typedef/using关键字返回任意多个返回值，并且这些值没有顺序关系。可以将只返回::type的
元函数称为标准元函数，而返回多个元数据的元函数称为非标准元函数。
元函数，它的计算在编译的时候就已经完成了（即模板实例化），实际程序执行时没有计算动作而是直接使用结果。
谨记：元函数就是一个形式上很像函数的一个类模板，它用于计算（推导）类型。

元函数转发：
template<typename T1,typename T2>
struct Cat : Dog<T2,T1>													//元函数转发，使用struct的默认public继承
{
};
等价于如下写法：
template<typename T1,typename T2>
struct Cat																		//元函数，不使用转发
{
	typedef typename Dog<T2,T1>::type type;					//调用元函数计算
};
*/

/*
type_traits:
模板元编程库，具有类型特征提取的功能。
type_traits提供一组特征（traits）类---即元函数，可以在编译期确定类型（元数据）是否具有某些特征。
type_traits库提供了上百个元函数，可以按照返回类型和功能分类。

根据返回类型，type_traits库所提供的元函数可以分为以下两大类：
1：检查元数据属性的值元函数：以::value返回一个bool值或者一个整数。
2：操作元数据的标准元函数：对元数据进行计算，以::type返回一个新的元数据。
type_traits库中以is_和has_开头的元函数均属于值元函数，其它则属于标准元函数，但也有少数的例外。

根据元函数实现的功能，type_traits库所提供的元函数可分为以下七类：
1：检查元数据的类别：				均以is_开头，都是值元函数。
2：检查元数据的属性：				大部分以is_和has_开头，都是值元函数。
3：检查元函数之间的关系：			均以is_开头，都是值元函数。
4：检查操作符重载：					均以has_开头，都是值元函数。
5：转换元数据：						返回转换后的类型，都是标准元函数。
6：用指定的对齐方式组合类型：		包括type_with_alignment<T>和aligned_storage<T>两个元函数。
7：解析函数元数据：					是非标准元函数。

type_traits库提供15个值元函数，检查元数据T的基本类别（primary traits），被检查的元数据前可以用const、volatile关键字修饰，元函数用::value返回bool
类型的检查结果。
检查基本类型：
1：is_integral<T>:
检查T是否是bool,char,int,long等整型，这些整型前可以有signed，unsigned等关键字。
2：is_floating_point<T>:
检查T是否是float,real_type,long real_type等浮点型。
3：is_float<T>:
与is_floating_point<T>功能相同。
4：is_void<T>:
检查类型T是否为void类型。

检查其它类型：
1:is_array<T>:
检查T是否是一个原生数组（包括一维和多维数组）。
2：is_class<T>:
检查T是否是一个class或者struct。
3：is_enum<T>:
检查T是否是一个枚举类型。
4：is_union<T>:
检查T是否是一个联合类型。
5:is_complex<T>:
检查T是否是一个标准库的复数类型(std::complex<U>)。
6：is_pointer<T>:
检查T是否是一个指针或者函数指针类型，但不是成员指针。
7：is_function<T>:
检查T是否是一个函数类型，但不是函数指针或引用。
8：is_lvalue_reference<T>:
检查T是否是一个左值引用类型。
9：is_rvalue_reference<T>:
检查T是否是一个右值引用类型。

检查成员指针类型：
1：is_member_object_pointer<T>:
检查T是否是指向成员变量的指针。
2：is_member_function_pointer<T>:
检查T是否是一个成员函数指针。

在15个基本元函数之上，type_traits库又提供了七个检查复合类别（composite traits）的元函数，它们相当于多个基本类别的组合，使用::value返回bool类型的
检查结果。
1：is_reference<T>:
检查T是否是一个引用类型（左引用或右引用）。
2：is_arithmetic<T>:
检查T是否是算术类型，相当于is_integral<T> || is_floating_point<T>。
3：is_fundamental<T>:
检查T是否是基本类型，相当于is_arithmetic<T> || is_void<T>。
4:is_compound<T>:
检查T是否是复合类型，即非基本类型，相当于!is_fundamental<T>.
5:is_member_pointer<T>:
检查T是否是成员指针，包括指向数据成员和函数成员的指针，相当于is_member_object_pointer<T> || is_member_function_pointer<T>.
6:is_object<T>:
检查T是否是实体对象类型，即引用、void和函数之外的所有类型，相当于!is_reference<T> && !is_void<T> && !is_function<T>.
7:is_scalar<T>:
检查T是否是标量类型，即算术类型、枚举、指针和成员指针，相当于is_arithmetic<T> || is_enum<T> || is_pointer<T> || is_member_pointer<T>.

除了检查类别，type_traits库里还有更多的元函数用于获取元数据更细致的属性。这些元函数都是值元函数，以is_和has_开头的元函数使用::value返回bool类型
的检查结果，其它元函数使用::value返回整数。下面是13个元函数：
检查数组的属性：
1:rank<T>:
如果T是数组，那么返回数组的维数，否则返回0。
2：extent<T,N>:
如果T是数组，那么返回数组第N个维度（从0计数）的值，否则返回0。

检查基本的修饰词：
1:is_const<T>:
检查T是否被const修饰。
2：is_volatile<T>:
检查T是否被volatile修饰。
3:is_signed<T>:
检查T是否是有符号整数。
4:is_unsigned<T>:
检查T是否是无符号整数。

检查class的属性：
1:is_pod<T>:
检查T是否是一个POD类型。POD是术语Plain Old Data的缩写，但没有明确的定义，通常来说基本类型。
2:is_empty<T>:
检查T是否是一个空类。
3:is_abstract<T>:
检查T是否是一个抽象类（有纯虚函数）。
4:is_polymorphic<T>:
检查T是否是一个多态类（有虚函数）。

检查其它属性：
1:has_new_operator<T>:
检查T是否重载了operator new。
2:is_stateless<T>:
检查T是否是一个无状态类，即一个“平凡”的空类。
3:alignment_of<T>:
T在内存中字节对齐的倍数。

下面的16个元函数专门用于检查class的六大特殊成员函数：构造和析构、拷贝构造和拷贝赋值、移动构造和移动赋值。这些元函数都是值元函数。
1:has_nothrow_constructor<T>:
检查T是否有不抛出异常的构造函数。
2:has_nothrow_default_constructor<T>:
检查T是否有不抛出异常的缺省构造函数。
3:has_trivial_constructor<T>:
检查T是否有“平凡”的构造函数。“平凡”的构造、析构函数没有任何操作。“平凡”的拷贝构造函数、赋值函数类似于memcpy操作，因而可以被编译器优化。
4:has_trivial_default_constructor<T>:
检查T是否有“平凡”的缺省构造函数。
5:has_trivial_destructor<T>:
检查T是否有“平凡”的析构函数。
6:has_virtual_destructor<T>:
检查T是否有虚析构函数。
7:has_nothrow_copy<T>:
检查T是否有不抛出异常的拷贝构造函数。
8:has_nothrow_copy_constructor<T>:
同has_nothrow_copy<T>。
9:has_trivial_copy<T>:
检查T是否有“平凡”的拷贝构造函数。
10:has_trivial_copy_constructor<T>:
同has_trivial_copy<T>。
11:has_nothrow_assign<T>:
检查T是否有不抛出异常的赋值函数。
12:has_trivial_assign<T>:
检查T是否有“平凡”的赋值函数。
13:is_nothrow_move_constructible<T>:
检查T是否有不抛出异常的移动构造函数。
14:is_nothrow_move_assignable<T>:
检查T是否有不抛出异常的移动赋值操作符。
15:has_trivial_move_assign<T>:
检查T是否有“平凡”的移动赋值。
16:has_trivial_move_constructor<T>:
检查T是否有“平凡”的移动构造函数。

type_traits库提供了近40个值元函数检查元数据是否重载了某些操作符，使用::value返回bool类型的检查结果。
这些元函数位于头文件"boost/type_traits/has_operator.hpp"中。
这里仅列举几个较有代表性的：
1:has_greater<T>:
T是否重载了operator>。
2:has_less<T>:
T是否重载了operator<。
3:has_equal_to<T>:
T是否重载了operator==。
4:has_plus<T>:
T是否重载了operator+。
5:has_minus<T>:
T是否重载了operator-。
6:has_pre_increment<T>:
T是否重载了前置++。

type_traits库提供了4个元函数计算元数据之间的关系，它们都是两参值元函数，使用::value返回bool类型的检查结果。
1:is_same<T,U>:
检查T和U是否是相同的类型。
2:is_convertible<From,To>:
检查From是否可隐式转型为To类型。
3:is_base_of<Base,Derived>:
检查Base是否是Derived的基类，或者两者相同。
4:is_virtual_base_of<Base,Derived>:
检查Base是否是Derived的虚基类。

前面我们看到的元函数都是值元函数，它们返回bool值或者整数，下面的元函数将会真正开始对元数据（类型）进行计算，输入一个类型然后输出一个新的类型，
随意地处理C++的类型。
基本的元数据“加法”：
1:add_const<T>:
返回T const。
2:add_volatile<T>:
返回T volatile。
3:add_cv<T>:
返回T const volatile。
4:add_pointer<T>:
返回T*。
5:add_reference<T>:
返回T&。
6:add_lvalue_reference<T>:
对于对象或函数类型返回左值引用，通常是T&，否则返回T。
7:add_rvalue_reference<T>:
对于对象或函数类型返回右值引用，通常是T&&，否则返回T。

基本的元数据“减法”：
1:remove_const<T>:
移除T的顶层const修饰。
2:remove_volatile<T>:
移除T的顶层volatile修饰。
3:remove_cv<T>:
移除T的顶层const和volatile修饰。
4:remove_pointer<T>:
移除T的指针修饰(*)。
5:remove_reference<T>:
移除T的引用修饰(&或&&)。

处理算术类型：
下面的五个元函数用于处理算术类型元数据(is_arithmetic<T>::value == true)，但不能处理bool类型。
1:make_signed<T>:
返回T相应的有符号整数类型，cv修饰不变。
2:make_unsigned<T>:
返回T相应的无符号整数类型，cv修饰不变。
3:itegral_promotion<T>:
返回T的右值整型提升结果，cv修饰不变。
4:floating_point_promotion<T>:
返回T的右值浮点型提升结果，cv修饰不变。
5:promote<T>:
返回T的右值算术类型提升结果，相当于integral_promotion<T>或floating_point_promotion<T>。

处理数组类型：
1:remove_extent<T>:
移除数组的最顶层维度（降低一个维度）。
2:remove_bounds<T>:
同remove_extent<T>。
3:remove_all_extents<T>:
移除数组的所有维度（变为0维的普通类型）。

其它运算：
1:conditional<b,T,U>:
条件运算，类似?:操作，根据b的真假决定返回T或U，它等价于mpl::if_c<>。
2:common_type<T,...>:
求多个类型的共通类型，对类型的顺序有要求。
3:decay<T>:
先执行remove_reference<T>得到元数据U，如果U为数组类型，则返回remove_extent<U>*，如果U为函数类型，则结果为U*，否则返回U。

解析函数元数据：
解析函数元数据的元函数function_traits<>，是一个非标准元函数，能够返回多个值，包括函数的参数数量、参数类型和返回类型，支持解析最多10个参数的函数。
function_traits<T>要求输入的元数据（类型）必须满足is_function<T>::value == true，不能是函数指针或者引用。如果输入的不是函数类型，那么可以用
元函数remove_pointer<T>,remove_reference<T>来转换类型，否则会导致编译错误（即元程序执行失败）。
template<typename T>
struct function_traits
{
	static const std::size_t		arity;					//返回函数的参数数量
	typedef some_define			result_type;		//返回函数的返回值类型
	typedef some_define			argN_type;		//返回函数第N个参数的类型
};
*/