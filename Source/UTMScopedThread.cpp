#include "UTMScopedThread.h"

#ifdef UTM_HIERARCHICAL_COMPILE

namespace ung
{
	ScopedThread::ScopedThread() :
		mThread(std::thread())
	{
		BOOST_ASSERT(!mThread.joinable());
	}

	ScopedThread::ScopedThread(std::thread t) :
		mThread(std::move(t))
	{
		BOOST_ASSERT(mThread.joinable());

		mID = mThread.get_id();
	}

	ScopedThread::ScopedThread(ScopedThread&& st) noexcept :
		mThread(std::move(st.mThread))
	{
		BOOST_ASSERT(!st.mThread.joinable());

		BOOST_ASSERT(mThread.joinable());

		mID = mThread.get_id();
	}

	/*
	移动赋值运算符执行与析构函数和移动构造函数相同的工作。
	*/
	ScopedThread& ScopedThread::operator=(ScopedThread&& st) noexcept
	{
		if (this != &st)
		{
			BOOST_ASSERT(mThread.joinable());

			mThread.join();

			BOOST_ASSERT(!mThread.joinable());

			mThread = std::move(st.mThread);
			mID = mThread.get_id();
		}

		return *this;
	}

	ScopedThread::~ScopedThread()
	{
		//move后的源对象，会调用析构函数
		if (mThread.joinable())
		{
			mThread.join();
		}

		BOOST_ASSERT(!mThread.joinable());
	}

	std::thread::id ScopedThread::getID() const
	{
		return mID;
	}

	bool ScopedThread::operator==(ScopedThread const & st)
	{
		return mID == st.getID();
	}

	bool ScopedThread::operator!=(ScopedThread const & st)
	{
		return !operator==(st);
	}

	bool ScopedThread::operator<(ScopedThread const & st)
	{
		return mID < st.getID();
	}

	//----------------------------------------------------------------------------------

	std::size_t ScopedThreadHasher::operator()(const ScopedThread& st) const
	{
		return std::hash<std::thread::id>()(st.getID());
	}
}//namespace ung

#endif//UTM_HIERARCHICAL_COMPILE