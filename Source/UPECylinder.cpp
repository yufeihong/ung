#include "UPECylinder.h"

#ifdef UPE_HIERARCHICAL_COMPILE

namespace ung
{
	Cylinder::Cylinder(Vector3 const & topCenter, Vector3 const & bottomCenter, real_type radius) :
		mTopCenter(topCenter),
		mBottomCenter(bottomCenter),
		mRadius(radius)
	{
	}

	Vector3 const & Cylinder::getTopCenter() const
	{
		return mTopCenter;
	}

	Vector3 const & Cylinder::getBottomCenter() const
	{
		return mBottomCenter;
	}

	real_type Cylinder::getRadius() const
	{
		return mRadius;
	}
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE