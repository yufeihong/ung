#include "URMVideoBuffer.h"

#ifdef URM_HIERARCHICAL_COMPILE

namespace
{
	using namespace ung;

	void checkLockFlag(uint32 usage,bool useShadow,IVideoBuffer::LockOptions fl)
	{
		using type = IVideoBuffer::LockOptions;

		auto flag = static_cast<uint32>(fl);

		auto DISCARD = static_cast<uint32>(type::VB_LOCK_DISCARD);
		auto READ_ONLY = static_cast<uint32>(type::VB_LOCK_READ_ONLY);
		auto NO_OVERWRITE = static_cast<uint32>(type::VB_LOCK_NO_OVERWRITE);

#define HAVE_DISCARD							ungEnumCombinationHave(flag,DISCARD)
#define HAVE_READ_ONLY						ungEnumCombinationHave(flag,READ_ONLY)
#define HAVE_NO_OVERWRITE				ungEnumCombinationHave(flag,NO_OVERWRITE)

		/*
		如果有DISCARD或NO_OVERWRITE的话，那么usage中必须有VB_USAGE_DYNAMIC
		*/
		if (HAVE_DISCARD || HAVE_NO_OVERWRITE)
		{
			if (!ungEnumCombinationHave(usage,IVideoBuffer::Usage::VB_USAGE_DYNAMIC))
			{
				UNG_EXCEPTION("VB_LOCK_DISCARD must be used with VB_USAGE_DYNAMIC");
			}
		}

		/*
		如果有READ_ONLY的话，那么useShadow必须为true
		*/
		if (HAVE_READ_ONLY)
		{
			if (!useShadow)
			{
				UNG_EXCEPTION("VB_LOCK_READ_ONLY must be used with shadow memory.");
			}
		}

		/*
		如果有shadow memory的话，就没必要使用VB_LOCK_NO_OVERWRITE了
		*/
		if (useShadow)
		{
			if (HAVE_NO_OVERWRITE)
			{
				UNG_EXCEPTION("There is no need to use VB_LOCK_NO_OVERWRITE if there is a shadow memory.");
			}
		}
	}
}//namespace

namespace ung
{
	VideoBuffer::VideoBuffer() :
		mUsage(Usage::VB_USAGE_WRITE_ONLY),
		mUseShadowBuffer(false),
		mShadowUpdated(false),
		mIsLocked(false),
		mShadowBuffer(nullptr)
	{
	}

	VideoBuffer::VideoBuffer(uint32 usage, bool useShadowBuffer) :
		mUsage(usage),
		mUseShadowBuffer(useShadowBuffer),
		mShadowUpdated(false),
		mIsLocked(false),
		mShadowBuffer(nullptr)
	{
	}

	VideoBuffer::~VideoBuffer()
	{
	}

	void* VideoBuffer::lock(LockOptions options)
	{
		return lock(0, 0, options);
	}

	void* VideoBuffer::lock(uint32 offset, uint32 length, LockOptions options)
	{
		BOOST_ASSERT(!isLocked() && "Cannot lock this buffer,it is already locked!");

		checkLockFlag(mUsage, mUseShadowBuffer, options);

		void* ret{nullptr};

		//是否越界
		if ((offset + length) > mSizeInBytes)
		{
			UNG_EXCEPTION("Lock request out of bounds.");
		}
		else if (mUseShadowBuffer)
		{
			if (options != VB_LOCK_READ_ONLY)
			{
				/*
				不是read，那么就是写，而且因为使用了shadow，所以这里标记为“需要更新”，以便在unlock()
				的时候更新缓存。
				*/
				mShadowUpdated = true;
			}

			//操作系统内存
			ret = mShadowBuffer->lock(offset, length, options);
		}
		else
		{
			//lock缓存
			ret = lockImpl(offset, length, options);
			mIsLocked = true;
		}

		mLockStart = offset;
		mLockSize = length;

		return ret;
	}

	void VideoBuffer::unlock()
	{
		BOOST_ASSERT(isLocked() && "Cannot unlock this buffer,it is not locked!");

		//如果使用了shadow，并且系统内存已经处于locked的状态
		if (mUseShadowBuffer && mShadowBuffer->isLocked())
		{
			//操作系统内存
			mShadowBuffer->unlock();

			//从系统内存去更新缓存
			updateFromShadow();
		}
		else
		{
			//unlock缓存
			unlockImpl();

			mIsLocked = false;
		}
	}

	void VideoBuffer::readData(uint32 offset, uint32 length, void* pDest)
	{
	}

	void VideoBuffer::copyData(IVideoBuffer& srcBuffer, uint32 srcOffset, uint32 dstOffset, uint32 length, bool discardWholeBuffer)
	{
		const void* srcData = srcBuffer.lock(srcOffset, length, VB_LOCK_READ_ONLY);

		this->writeData(dstOffset, length, srcData, discardWholeBuffer);

		srcBuffer.unlock();
	}

	void VideoBuffer::copyData(IVideoBuffer& srcBuffer)
	{
		uint32 sz = std::min(getSizeInBytes(),srcBuffer.getSizeInBytes());

		copyData(srcBuffer,0,0,sz,true);
	}

	void VideoBuffer::updateFromShadow()
	{
		//如果使用了shadow，并且标记了需要更新
		if (mUseShadowBuffer && mShadowUpdated)
		{
			//lock源
			const void* srcData = mShadowBuffer->lockImpl(mLockStart, mLockSize,VB_LOCK_READ_ONLY);

			LockOptions lockOpt;
			if (mLockStart == 0 && mLockSize == mSizeInBytes)
			{
				lockOpt = VB_LOCK_DISCARD;
			}
			else
			{
				lockOpt = VB_LOCK_NORMAL;
			}

			//lock目标
			void* destData = lockImpl(mLockStart, mLockSize, lockOpt);

			//Copy shadow to real
			memcpy(destData, srcData, mLockSize);

			unlockImpl();

			mShadowBuffer->unlockImpl();

			mShadowUpdated = false;
		}
	}

	uint32 VideoBuffer::getSizeInBytes() const
	{
		return mSizeInBytes;
	}

	uint32 VideoBuffer::getUsage() const
	{
		return mUsage;
	}

	bool VideoBuffer::hasShadowBuffer() const
	{
		return mUseShadowBuffer;
	}

	bool VideoBuffer::isLocked() const
	{
		//如果shadow处于locked的状态，那么也认为当前缓存处于locked的状态
		return mIsLocked || (mUseShadowBuffer && mShadowBuffer->isLocked());
	}
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

/*
读取GPU/AGP buffers会影响到性能。

备份缓冲:
当我们创建一个WRITE_ONLY的缓冲区后,我们有时候需要从中读取数据,我们可以在创建缓冲区时传参数,这样我们在内存中就会创建一个备份的缓冲区.每当我们向显存中
写入一份数据的时候,UNG会自动的先将这份数据拷贝到内存缓冲区后,再将其更新到显存中的硬件缓冲区.
当然,这个技术会带来更多的开销,所以非必要时不要用它

缓存锁
当我们更新缓冲的时候,都应该先"锁"住它,以免它被修改.当然,之后记得解锁.
pBuffer->Lock(begin,length,lockType)一般来说,锁的范围越小越便捷快速.但是锁的类型lockType也可以对读取的效率产生影响.
*/