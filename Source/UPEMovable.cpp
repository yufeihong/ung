#include "UPEMovable.h"

#ifdef UPE_HIERARCHICAL_COMPILE

namespace ung
{
	Vector4 Movable::sInModelPosition(0.0, 0.0, 0.0, 1.0);
	Vector3 Movable::sInModelOrientation(0.0, 0.0, 1.0);
	real_type Movable::sScale(1.0);

	Movable::Movable() :
		mNeedUpdateFlag(true)
	{
	}

	Movable::~Movable()
	{
	}

	Movable::Movable(Movable const & other) :
		IMovable(other),
		mLocal(other.mLocal),
		mWorld(other.mWorld),
		mNeedUpdateFlag(true)
	{
	}
	
	Movable & Movable::operator=(Movable const & other)
	{
		if (this != &other)
		{
			IMovable::operator =(other);

			mLocal = other.mLocal;
			mWorld = other.mWorld;

			mNeedUpdateFlag = true;
		}

		return *this;
	}

	Movable::Movable(Movable && other) :
		IMovable(other),
		mLocal(other.mLocal),
		mWorld(other.mWorld),
		mNeedUpdateFlag(true)
	{
	}

	Movable & Movable::operator=(Movable && other)
	{
		if (this != &other)
		{
			Movable::operator =(other);

			mLocal = other.mLocal;
			mWorld = other.mWorld;

			mNeedUpdateFlag = true;
		}

		return *this;
	}

	void Movable::setNeedUpdateFlag(bool flag)
	{
		mNeedUpdateFlag = flag;
	}

	bool Movable::getNeedUpdateFlag() const
	{
		return mNeedUpdateFlag;
	}

	void Movable::setRotate(Vector3 const & axis, Radian r)
	{
		mLocal.setRotate(axis, r);

		mNeedUpdateFlag = true;
	}

	void Movable::setRotate(Vector3 const & axis, Degree d)
	{
		mLocal.setRotate(axis, d);

		mNeedUpdateFlag = true;
	}

	void Movable::setScale(real_type s)
	{
		mLocal.setScale(s);

		mNeedUpdateFlag = true;
	}

	void Movable::setTranslate(real_type x, real_type y, real_type z)
	{
		mLocal.setTranslate(x, y, z);

		mNeedUpdateFlag = true;
	}

	void Movable::setTranslate(Vector3 t)
	{
		mLocal.setTranslate(t);

		mNeedUpdateFlag = true;
	}

	void Movable::addUpRotate(Vector3 const & axis, Radian r)
	{
		mLocal.addUpRotate(axis, r);

		mNeedUpdateFlag = true;
	}

	void Movable::addUpRotate(Vector3 const & axis, Degree d)
	{
		mLocal.addUpRotate(axis, d);

		mNeedUpdateFlag = true;
	}

	void Movable::addUpScale(real_type s)
	{
		mLocal.addUpScale(s);

		mNeedUpdateFlag = true;
	}

	void Movable::addUpTranslate(real_type x, real_type y, real_type z)
	{
		mLocal.addUpTranslate(x, y, z);

		mNeedUpdateFlag = true;
	}

	void Movable::addUpTranslate(Vector3 t)
	{
		mLocal.addUpTranslate(t);

		mNeedUpdateFlag = true;
	}

	SQT const & Movable::getToParentSQT() const
	{
		return mLocal;
	}

	Matrix4 Movable::getToParentMatirx() const
	{
		Matrix4 temp{};

		mLocal.toAffineMatrix(temp);

		return temp;
	}

	SQT const & Movable::getToWorldSQT() const
	{
		BOOST_ASSERT(!mNeedUpdateFlag);

		return mWorld;
	}

	Matrix4 Movable::getToWorldMatrix() const
	{
		BOOST_ASSERT(!mNeedUpdateFlag);

		Matrix4 temp{};

		mWorld.toAffineMatrix(temp);

		return temp;
	}

	SQT Movable::getToWorldSQTInverse() const
	{
		BOOST_ASSERT(!mNeedUpdateFlag);

		return mWorld.inverse();
	}

	Matrix4 Movable::getToWorldMatrixInverse() const
	{
		BOOST_ASSERT(!mNeedUpdateFlag);

		Matrix4 temp{}, out{};

		mWorld.toAffineMatrix(temp);

		temp.inverse(out);

		return out;
	}

	Vector4 Movable::getInParentPosition()
	{
		return sInModelPosition * mLocal;
	}

	Vector3 Movable::getInParentOrientation()
	{
		return sInModelOrientation * mLocal;
	}

	real_type Movable::getInParentScale()
	{
		return sScale * mLocal.getScale();
	}

	Vector4 Movable::getInWorldPosition()
	{
		BOOST_ASSERT(!mNeedUpdateFlag);

		return sInModelPosition * mWorld;
	}

	Vector3 Movable::getInWorldOrientation()
	{
		BOOST_ASSERT(!mNeedUpdateFlag);

		return sInModelOrientation * mWorld;
	}

	real_type Movable::getInWorldScale()
	{
		BOOST_ASSERT(!mNeedUpdateFlag);

		return sScale * mWorld.getScale();
	}

	void Movable::update()
	{
	}

	void Movable::setWorld(SQT const & world)
	{
		mWorld = world;
	}
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE