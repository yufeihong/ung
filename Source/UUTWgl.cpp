#include "UUTWgl.h"

#ifdef UUT_HIERARCHICAL_COMPILE

#include "UUTTools.h"

namespace ung
{
	bool WGL::mRenderingFlag = true;
	ChangeSizeType WGL::mChangeSizeFunction;
	RenderType WGL::mRenderFunction;

	/*
	Since this method is a callback, it must be declared static.
	*/
	static LRESULT CALLBACK WndProc(HWND hWnd,UINT uMsg,WPARAM wParam,LPARAM lParam)
	{
		switch (uMsg)
		{
		case WM_ACTIVATE:
		case WM_SETFOCUS:
			WGL::getRenderFunction()();
			return 0;
		case WM_SIZE:
			WGL::getChangeSizeFunction()(LOWORD(lParam), HIWORD(lParam));
			WGL::getRenderFunction()();
			break;
		case WM_CLOSE:
			WGL::getRenderingFlag() = false;
			PostQuitMessage(0);
			return 0;
		default:
			break;
		}

		/*
		Pass All Unhandled Messages To DefWindowProc.

		It’s important to respond to messages being sent from the operating system, even if you 
		just pass them through to the default handler:
		*/
		return DefWindowProc(hWnd, uMsg, wParam, lParam);
	}

	WGL::WGL(uint32 width, uint32 height,wchar_t* title,bool fullScreen) :
		mWidth(width),
		mHeight(height),
		mFullScreen(fullScreen),
		mTitle(title),
		mMajor(4),
		mMinor(5)
	{
	}

	WGL::~WGL()
	{
	}

	void WGL::createWindow()
	{
#if UNG_DEBUGMODE
		BOOST_ASSERT(mMajor >= 3 && mMajor <= 4);
		if (mMajor == 3)
		{
			BOOST_ASSERT(mMinor >= 0 && mMinor <= 3);
		}
		else
		{
			BOOST_ASSERT(mMinor >= 0);
		}
#endif//UNG_DEBUGMODE

		int posX = 0;
		int posY = 0;
		int pixelFormats = -1;
		PIXELFORMATDESCRIPTOR pfDescriptor;

		DWORD extStyle;
		DWORD windowStyle;

		mHinstance = GetModuleHandle(NULL);

		WNDCLASS windowClass;
		windowClass.lpszClassName = mTitle;
		windowClass.lpfnWndProc = (WNDPROC)WndProc;
		windowClass.hInstance = mHinstance;
		windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
		windowClass.hIcon = LoadIcon(NULL, IDI_WINLOGO);
		windowClass.hbrBackground = NULL;
		windowClass.lpszMenuName = NULL;
		windowClass.style = CS_HREDRAW | CS_OWNDC | CS_VREDRAW;
		windowClass.cbClsExtra = 0;
		windowClass.cbWndExtra = 0;

		if (!RegisterClass(&windowClass))
		{
			exit(EXIT_FAILURE);
		}

		extStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
		windowStyle = WS_OVERLAPPEDWINDOW;
		ShowCursor(TRUE);

		RECT windowRect;
		windowRect.left = posX;
		windowRect.right = posX + mWidth;
		windowRect.top = posY;
		windowRect.bottom = posY + mHeight;

		//Setup window width and height
		AdjustWindowRectEx(&windowRect, windowStyle, FALSE, extStyle);

		//Adjust for adornments(装饰)
		int windowWidth = windowRect.right - windowRect.left;
		int windowHeight = windowRect.bottom - windowRect.top;

		//Create window
		mHwnd = CreateWindowEx(extStyle, mTitle, mTitle,
			windowStyle | WS_CLIPSIBLINGS | WS_CLIPCHILDREN,
			posX, posY,
			windowWidth,
			windowHeight,
			NULL, NULL,
			mHinstance,
			NULL);

		//now that we have a window, setup the pixel format descriptor
		mHdc = GetDC(mHwnd);

		//Set a dummy pixel format so that we can get access to wgl functions
		SetPixelFormat(mHdc, 1, &pfDescriptor);
		//Create OpenGL context and make it current
		mHglrc = wglCreateContext(mHdc);
		wglMakeCurrent(mHdc, mHglrc);

		if (mHdc == 0 || mHglrc == 0)
		{
			exit(EXIT_FAILURE);
		}

		GLenum err = glewInit();
		if (GLEW_OK != err)
		{
			exit(EXIT_FAILURE);
		}

		//Now that extensions are setup,delete window and start over picking a real format.
		wglMakeCurrent(NULL, NULL);
		wglDeleteContext(mHglrc);
		ReleaseDC(mHwnd, mHdc);
		DestroyWindow(mHwnd);

		if (mFullScreen)
		{
			//Prepare for a mode set to the requested resolution
			DEVMODE dm;
			memset(&dm, 0, sizeof(dm));
			dm.dmSize = sizeof(dm);
			dm.dmPelsWidth = mWidth;
			dm.dmPelsHeight = mHeight;
			dm.dmBitsPerPel = 32;
			dm.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

			long error = ChangeDisplaySettings(&dm, CDS_FULLSCREEN);

			if (error != DISP_CHANGE_SUCCESSFUL)
			{
				exit(EXIT_FAILURE);
			}
			else
			{
				//Mode set passed, setup the styles for fullscreen
				extStyle = WS_EX_APPWINDOW;
				windowStyle = WS_POPUP;
				ShowCursor(FALSE);
			}
		}

		AdjustWindowRectEx(&windowRect, windowStyle, FALSE, extStyle);

		//Create the window again
		mHwnd = CreateWindowEx(extStyle, mTitle, mTitle,
			windowStyle | WS_CLIPSIBLINGS | WS_CLIPCHILDREN,
			posX, posY,
			windowWidth, windowHeight,
			NULL, NULL,
			mHinstance,
			NULL);

		mHdc = GetDC(mHwnd);

		int pixFormatCount = 0;

		//Specify the important attributes we care about
		int pixAttribs[] = {
			WGL_SUPPORT_OPENGL_ARB,		1,
			WGL_DRAW_TO_WINDOW_ARB,	1,
			WGL_ACCELERATION_ARB,			WGL_FULL_ACCELERATION_ARB,
			WGL_COLOR_BITS_ARB,				24,
			WGL_DEPTH_BITS_ARB,				24,
			WGL_STENCIL_BITS_ARB,			8,
			WGL_DOUBLE_BUFFER_ARB,		GL_TRUE,
			WGL_PIXEL_TYPE_ARB,				WGL_TYPE_RGBA_ARB,
			WGL_SAMPLE_BUFFERS_ARB,		GL_TRUE,							//MSAA on
			WGL_SAMPLES_ARB,					8,										//8x MSAA

			0
		};

		//Ask OpenGL to find the most relevant format matching our attribs,Only get one format back.
		wglChoosePixelFormatARB(mHdc, &pixAttribs[0], NULL, 1, &pixelFormats, (UINT*)&pixFormatCount);

		if (pixelFormats == -1)
		{
			//WGL_SAMPLES_ARB
			pixAttribs[19] = 4;
			wglChoosePixelFormatARB(mHdc, &pixAttribs[0], NULL, 1, &pixelFormats, (UINT*)&pixFormatCount);
		}

		//有合适的像素格式
		if (pixelFormats != -1)
		{
#if UNG_DEBUGMODE
			//Check for MSAA
			int attrib[] = { WGL_SAMPLES_ARB };
			int samples = 0;
			wglGetPixelFormatAttribivARB(mHdc, pixelFormats, 0, 1, attrib, &samples);
#endif//UNG_DEBUGMODE

			//Got a format,now set it as the current one
			SetPixelFormat(mHdc, pixelFormats, &pfDescriptor);

			GLint attribs[] = {
					WGL_CONTEXT_MAJOR_VERSION_ARB,mMajor,
					WGL_CONTEXT_MINOR_VERSION_ARB,mMinor,
					0 };

			/*
			临时：
			int attribs[] =
	{
		WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
		WGL_CONTEXT_MINOR_VERSION_ARB, 1,
		WGL_CONTEXT_FLAGS_ARB, 0,
		0
	};

		if(wglewIsSupported("WGL_ARB_create_context") == 1)
		{
		m_hrc = wglCreateContextAttribsARB(pDC->m_hDC,0, attribs);
		wglMakeCurrent(NULL,NULL);
		wglDeleteContext(tempContext);
		wglMakeCurrent(pDC->m_hDC, m_hrc);
	}
	else
	{	//It's not possible to make a GL 3.x context. Use the old style context (GL 2.1 and before)
		m_hrc = tempContext;
	}

	Have you noticed something odd in this initialization? In order to create new OpenGL rendering context you have to call function wglCreateContextAttribsARB(), which is an OpenGL function and requires OpenGL to be active when it is called. How can we fulfill this when we are at the beginning of OpenGL rendering context creation? The only way is to create an old context, activate it, and while it is active create a new one. Very inconsistent, but we have to live with it!

	Checking For Extensions
This project did not use GL to check for extensions, however, if you want to check for extensions once your GL 3 context is created, you can chose one of these methods :

 const GLubyte *extensions_string=glGetString(GL_EXTENSIONS);
or

 int NumberOfExtensions;
 glGetIntegerv(GL_NUM_EXTENSIONS, &NumberOfExtensions);
 for(i=0; i<NumberOfExtensions; i++)
 {
   const GLubyte *one_string=glGetStringi(GL_EXTENSIONS, i);
 }
For GL 3.0, you can use either of those 2 methods. glGetString(GL_EXTENSIONS) has been around since GL 1.0. Yes, of course you need additional code for searching the long string returned by glGetString(GL_EXTENSIONS). That is why some people use a library function such as glewIsSupported from GLEW.

Starting with GL 3.1, you must use the second method.

glewInit();
	if (glewIsSupported("GL_VERSION_2_0"))
		printf("Ready for OpenGL 2.0\n");
	else
	{
		printf("OpenGL 2.0 not supported\n");
		exit(1);
	}
			*/

#define CREATE_CONTEXT										\
mHglrc = wglCreateContextAttribsARB(mHdc, 0, attribs);

#define REDUCE_VERSION										\
if (attribs[1] == 3)													\
{																			\
	if (attribs[3] > 0)												\
	{																		\
		--attribs[3];													\
	}																		\
}																			\
else if (attribs[1] == 4)											\
{																			\
	if (attribs[3] > 0)												\
	{																		\
		--attribs[3];													\
	}																		\
	else if (attribs[3] == 0)										\
	{																		\
		attribs[1] = 3;												\
		attribs[3] = 3;												\
	}																		\
}

			CREATE_CONTEXT;
			while (!mHglrc)
			{
				REDUCE_VERSION;
				CREATE_CONTEXT;
			}

			wglMakeCurrent(mHdc, mHglrc);
		}

		if (mHdc == 0 || mHglrc == 0)
		{
			exit(EXIT_FAILURE);
		}

		ShowWindow(mHwnd, SW_SHOW);
		SetForegroundWindow(mHwnd);
		SetFocus(mHwnd);

		//检查多个实例
		if (!uutIsOnlyInstance(mTitle))
		{
			return;
		}

		//检查应用程序运行时为了存储游戏，或者从DVD-ROM中缓存数据，或者其它的临时文件而所需的磁盘空间
		if (!uutCheckStorage())
		{
			return;
		}

		//检查应用程序对系统内存和虚拟内存的要求
		if (!uutCheckMemory())
		{
			return;
		}

		//检查应用程序所需的最小CPU速度
		if (!uutCheckCPUSpeed())
		{
			return;
		}

		/*
		使用GLEW快捷方式库，来自动获取驱动程序支持功能的扩展函数指针。
		BOOL vSyncRet = wglSwapIntervalEXT(1);
		BOOST_ASSERT(vSyncRet);
		*/
		//EXT前缀表明这个扩展是得到了众多实现的支持。
		bool supportedRet = uutIsExtSupported("WGL_EXT_swap_control");
		BOOST_ASSERT(supportedRet);
		typedef BOOL(APIENTRY *WGLSWAPINTERVALEXTTYPE)(int);
		WGLSWAPINTERVALEXTTYPE wglSwapIntervalEXT = nullptr;
		//Windows函数wglGetProcAddress返回一个指向OpenGL函数（扩展）名的指针。
		wglSwapIntervalEXT = (WGLSWAPINTERVALEXTTYPE)wglGetProcAddress("wglSwapIntervalEXT");
		BOOST_ASSERT(wglSwapIntervalEXT);
		wglSwapIntervalEXT(1);
	}

	void WGL::destroyWindow()
	{
		if (mFullScreen)
		{
			ChangeDisplaySettings(NULL, 0);
			ShowCursor(TRUE);
		}

		if (mHglrc)
		{
			wglMakeCurrent(NULL, NULL);
			wglDeleteContext(mHglrc);
			mHglrc = NULL;
		}

		if (mHdc)
		{
			ReleaseDC(mHwnd, mHdc);
			mHdc = NULL;
		}

		if (mHwnd)
		{
			DestroyWindow(mHwnd);
			mHwnd = NULL;
		}

		UnregisterClass(mTitle, mHinstance);
		mHinstance = NULL;
		ShowCursor(TRUE);
	}

	void WGL::setCallback(ChangeSizeType changeSize, RenderType render)
	{
		mChangeSizeFunction = changeSize;
		mRenderFunction = render;
	}

	void WGL::enterLoop()
	{
		while (mRenderingFlag)
		{
			MSG msg;

			/*
			The GetMessage() function will block execution until the application has at least one 
			message pending, and then it will run the inner block of the while loop. This in turn 
			calls the Windows procedure callback function you registered when creating the 
			window. If that function blocks the execution of GetMessage() by locking the 
			application in a loop, it won’t receive any messages. Have you ever clicked on a 
			Windows program and had it gray itself out, followed by a message saying 
			something like “this program is not responding”? What’s happening is that the 
			program is never getting back to the GetMessage() call.

			The problem here is that we can’t stop execution if there are no messages pending,
			nor can we ignore messages that come in. The solution here is the PeekMessage() 
			function, which is just like GetMessage() except that it doesn’t block execution.
			*/
			if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				if (msg.message == WM_QUIT)
				{
					mRenderingFlag = false;
				}
				else
				{
					TranslateMessage(&msg);
					DispatchMessage(&msg);
				}
			}
			else
			{
				mRenderFunction();
			}
		}
	}

	HWND WGL::getHwnd()
	{
		return mHwnd;
	}

	bool& WGL::getRenderingFlag()
	{
		return mRenderingFlag;
	}

	ChangeSizeType WGL::getChangeSizeFunction()
	{
		return mChangeSizeFunction;
	}
	
	RenderType WGL::getRenderFunction()
	{
		return mRenderFunction;
	}
}//namespace ung

#endif//UUT_HIERARCHICAL_COMPILE

/*
调用顺序：
WGL wglObject(800, 600, L"UNG Window");
wglObject.setCallback(ChangeSize, RenderScene);
wglObject.createWindow();
wglObject.enterLoop();
wglObject.destroyWindow();
*/

/*
示例：
WGL wglObject(800, 600, L"UNG Window");

void RenderScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	SwapBuffers(GetDC(wglObject.getHwnd()));
}

void ChangeSize(int nWidth, int nHeight)
{
	glViewport(0, 0, nWidth, nHeight);
}

//---------------------------------------------------------------

int CALLBACK wWinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPTSTR lpCmdLine,int nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	wglObject.setCallback(ChangeSize, RenderScene);
	wglObject.createWindow();
	wglObject.enterLoop();
	wglObject.destroyWindow();

	//---------------------------------------------------------------

	return 0;
}
*/

/*
A problem with working on a multitasking platform like Windows is that you sometimes have 
to yield resources to those applications. For example, games typically acquire exclusive access 
to system resources like the video card, which allows them to render in full screen at custom 
resolutions. If the user Alt-tabs, you will lose that exclusive control and need to be able to 
handle that situation.

Windows games—players have an annoying tendency to actually have other applications up,
like Firefox or something, while playing your game! Any kind of task switching, or user 
switching under XP or later, can cause DirectX to lose its devices.
*/