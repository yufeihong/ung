#include "UPERigidBody.h"

#ifdef UPE_HIERARCHICAL_COMPILE

namespace ung
{
	RigidBody::RigidBody(real_type mass, btMotionState* transformationPtr, btCollisionShape* collisionShapePtr) :
		btRigidBody(mass,transformationPtr,collisionShapePtr)
	{
	}

	RigidBody::~RigidBody()
	{
	}
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

/*
The collision object
Collision objects are the essential building blocks of our physics objects, since they
maintain the object's physical properties, and give us a hook from which to alter the
object's velocity, acceleration, apply a force upon it, and so on. When we query the
motion state for the transform, it actually comes to this object to obtain it.
The simplest and most commonly used type of collision object is a  btRigidBody .
Rigid bodies are physics objects that do not deform as a result of collisions, as
opposed to soft bodies which do. Rigid bodies are the easiest collision objects to
deal with because their behavior is consistent, and doesn't require extravagant
mathematics to handle basic collisions, unlike soft bodies which are far more
difficult and expensive to simulate.
Rigid bodies also require a  btRigidBodyConstructionInfo object to be passed
through their constructor. This object stores data of important physical properties
for the rigid body, such as mass, friction, restitution, and so on.
*/

/*
If we define the mass to be zero, we also set the local inertia to  (0,0,0) . This will prevent 
the object from rotating as a result of collisions. In addition, Bullet uses a mass of zero to 
imply that you really want it to have an infinite mass, and hence don't want the object to 
move as result of forces like gravity. This is useful for environmental objects that don't move,
such as a ground planes walls, and so on.

The center of mass (COM) of an object is an important property in Newtonian
physics and it's just as important in physics simulations. Bullet will assume that
the rigid body's COM is equal to its world position, unless told otherwise. Shifting
the COM to some other location can be achieved through the usage of compound
shapes.
*/

/*
Bullet uses a concept called Single Instruction, Multiple Data (SIMD) on some platforms,
which makes the ability to run the same instruction on multiple pieces of data very rapid. In 
order to use this, the objects must be aligned in memory in steps of 16 bytes, and an STL 
vector does not naturally do this.
However, there is a built-in object type in Bullet called btAlignedObjectArray, which functions 
similarly to an STL vector and is worth exploring if you wish to make use of performance 
enhancements like SIMD in the future. Iterating through a large list of game objects and 
performing updates on them is a perfect situation to apply this technique.
*/