#include "UFCConfig.h"

#ifdef UFC_HIERARCHICAL_COMPILE

#pragma comment(lib,"zdll.lib")

namespace
{
	//warning LNK4221: 此对象文件未定义任何之前未定义的公共符号，因此任何耗用此库的链接操作都不会使用此文件。
	int UFC_DUMMY_LNK_4221 = 0;
}//namespace

#endif//UFC_HIERARCHICAL_COMPILE

/*
命名空间定义：
一个命名空间的定义包含两部分：首先是关键字namespace，随后是命名空间的名字。在命名空间名字后面是一系列由花括号括起来的声明和定义。只要能出现在全局作用
域中的声明就能置于命名空间内，主要包括：类、变量(及其初始化操作)、函数(及其定义)、模板和其他命名空间：
namespace cplusplus_primer
{
	class Sales_data {};
	Sales_data operator+(const Sales_data&,const Sales_data&);
	class Query {};
	class Query_base {};
}//命名空间结束后无须分号，这一点与块类似
上面的代码定义了一个名为cplusplus_primer的命名空间，该命名空间包含四个成员：三个类和一个重载的 + 运算符。

和其他名字一样，命名空间的名字也必须在定义它的作用域内保持唯一。命名空间既可以定义在全局作用域内，也可以定义在其他命名空间中，但是不能定义在函数或类的
内部。

记住：命名空间作用域后面无须分号。

每个命名空间都是一个作用域
和其他作用域类似，命名空间中的每个名字都必须表示该空间内的唯一实体。因为不同命名空间的作用域不同，所以在不同命名空间内可以有相同名字的成员。

定义在某个命名空间中的名字可以被该命名空间内的其他成员直接访问，也可以被这些成员内嵌作用域中的任何单位访问。位于该命名空间之外的代码则必须明确指出所用
的名字属于哪个命名空间：
cplusplus_primer::Query q = cplusplus_primer::Query("hello");

命名空间可以是不连续的
命名空间可以定义在几个不同的部分，这一点与其他作用域不太一样。编写如下的命名空间定义：
namespace nsp
{
	//相关声明
}
可能是定义了一个名为nsp的新命名空间，也可能是为已经存在的命名空间添加一些新成员。如果之前没有名为nsp的命名空间定义，则上述代码创建一个新的命名空间；否
则，上述代码打开已经存在的命名空间定义并为其添加一些新成员的声明。

命名空间的定义可以不连续的特性使得我们可以将几个独立的接口和实现文件组成一个命名空间。此时，命名空间的组织方式类似于我们管理自定义类及函数的方式：
1：命名空间的一部分成员的作用是定义类，以及声明作为类接口的函数及对象，则这些成员应该置于头文件中，这些头文件将被包含在使用了这些成员的文件中。
2：命名空间的定义部分则置于另外的源文件中。

在程序中某些实体只能定义一次：如非内联函数、静态数据成员、变量等，命名空间中定义的名字也需要满足这一要求，我们可以通过上面的方式组织命名空间并达到目的。
这种接口和实现分离的机制确保我们所需的函数和其他名字只定义一次，而只要是用到这些实体的地方都能看到对于实体名字的声明：

定义多个类型不相关的命名空间应该使用单独的文件分别表示每个类型(或关联类型构成的集合)。

举例定义命名空间
通过使用上述接口与实现分离的机制，我们可以将cplusplus_primer库定义在几个不同的文件中。Sales_data类的声明及其函数将置于Sales_data.h头文件中，Query类
置于Query.h头文件中，以此类推。对应的实现文件将分别是Sales_data.cc和Query.cc：
//---- Sales_data.h----
//#includes应该出现在打开命名空间的操作之前
#include <string>
namespace cplusplus_primer
{
	class Sales_data {};
	Sales_data operator+(const Sales_data&,const Sales_data&);
	//Sales_data的其他接口函数的声明
}
//---- Sales_data.cc----
//确保#includes出现在打开命名空间的操作之前
#include "Sales_data.h"
namespace cplusplus_primer
{
	//Sales_data成员及重载运算符的定义
}

程序如果想使用我们定义的库，必须包含必要的头文件，这些头文件中的名字定义在命名空间cplusplus_primer内：
//---- user.cc----
//Sales_data.h头文件的名字位于命名空间cplusplus_primer中
#include "Sales_data.h"
int main()
{
	using cplusplus_primer::Sales_data;
	Sales_data trans1, trans2;
	//...
	return 0;
}

这种程序的组织方式提供了开发者和库用户所需的模块性。每个类仍组织在自己的接口和实现文件中，一个类的用户不必编译与其他类相关的名字。我们对用户隐藏了实现
细节，同时允许文件Sales_data.cc和user.cc被编译并链接成一个程序而不会产生任何编译时错误或链接时错误。库的开发者可以分别实现每一个类，相互之间没有干扰。

有一点需要注意，在通常情况下，我们不把#include放在命名空间内部。如果我们这么做了，隐含的意思是把头文件中所有的名字定义成该命名空间的成员。例如，如果
Sales_data.h在包含string头文件前就已经打开了命名空间cplusplus_primer，则程序将出错，因为这么做意味着我们试图将命名空间std嵌套在命名空间
cplusplus_primer中。

定义命名空间成员
假定作用域中存在合适的声明语句，则命名空间中的代码可以使用同一命名空间定义的名字的简写形式：
#include "Sales_data.h"
namespace cplusplus_primer														//重新打开命名空间cplusplus_primer
{
	//命名空间中定义的成员可以直接使用名字，此时无须前缀
	std::istream& operator>>(std::istream& in, Sales_data& s) {}
}
也可以在命名空间定义的外部定义该命名空间的成员。命名空间对于名字的声明必须在作用域内，同时该名字的定义需要明确指出其所属的命名空间：
//命名空间之外定义的成员必须使用含有前缀的名字
cplusplus_primer::Sales_data
cplusplus_primer::operator+(const Sales_data& lhs,const Sales_data& rhs)
{
	Sales_data ret(lhs);
	//...
}
和定义在类外部的类成员一样，一旦看到含有完整前缀的名字，我们就可以确定该名字位于命名空间的作用域内。在命名空间cplusplus_primer内部，我们可以直接使用该
命名空间的其他成员，比如在上面的代码中，可以直接使用Sales_data定义函数的形参。

尽管命名空间的成员可以定义在命名空间外部，但是这样的定义必须出现在所属命名空间的外层空间中。换句话说，我们可以在cplusplus_primer或全局作用域中定义
Sales_data operator++，但是不能在一个不相关的作用域中定义这个运算符。

模板特例化
模板特例化必须定义在原始模板所属的命名空间中。和其他命名空间名字类似，只要我们在命名空间中声明了特例化，就能在命名空间外部定义它了：
//我们必须将模板特例化声明成std的成员
namespace std
{
	template <>
	struct hash<Sales_data>;
}
//在std中添加了模板特例化的声明后，就可以在命名空间std的外部定义它了
template <>
struct std::hash<Sales_data>
{
	size_t operator()(const Sales_data& s) const
	{
		return hash<string>()(s.bookNo) ^ hash<unsigned>()(s.units_sold) ^ hash<real_type>()(s.revenue);
	}
	//other members as before
};

全局命名空间
全局作用域中定义的名字(即在所有类、函数及命名空间之外定义的名字)也就是定义在全局命名空间(global namespace)中。全局命名空间以隐式的方式声明，并且在所
有程序中都存在。全局作用域中定义的名字被隐式地添加到全局命名空间中。

作用域运算符同样可以用于全局作用域的成员，因为全局作用域是隐式的，所以它并没有名字。下面的形式
::member_name
表示全局命名空间中的一个成员。

嵌套的命名空间
嵌套的命名空间是指定义在其他命名空间中的命名空间：
namespace cplusplus_primer
{
	//第一个嵌套的命名空间：定义了库的Query部分
	namespace QueryLib
	{
		class Query {};
		Query operator&(const Query&, const Query&);
		//...
	}
	//第二个嵌套的命名空间：定义了库的Sales_data部分
	namespace Bookstore
	{
		class Quote {};
		class Disc_quote : public Quote {};
		//...
	}
}
上面的代码将命名空间cplusplus_primer分割为两个嵌套的命名空间，分别是QueryLib和Bookstore。

嵌套的命名空间同时是一个嵌套的作用域，它嵌套在外层命名空间的作用域中。嵌套的命名空间中的名字遵循的规则与往常类似：内层命名空间声明的名字将隐藏外层命名
空间声明的同名成员。在嵌套的命名空间中定义的名字只在内层命名空间中有效，外层命名空间中的代码要想访问它必须在名字前添加限定符。例如，在嵌套的命名空间
QueryLib中声明的类名是
cplusplus_primer::QueryLib::Query

内联命名空间
C++11：
C++11新标准引入了一种新的嵌套命名空间，称为内联命名空间(inline namespace)。和普通的嵌套命名空间不同，内联命名空间中的名字可以被外层命名空间直接使用。
也就是说，我们无须在内联命名空间的名字前添加表示该命名空间的前缀，通过外层命名空间的名字就可以直接访问它。

定义内联命名空间的方式是在关键字namespace前添加关键字inline：
inline namespace FifthEd
{
	//namespace for the code from the Primer Fifth Edition
}
namespace FifthEd																		//隐式内联
{
	class Query_base {};
	//other Query-related declarations
}
关键字inline必须出现在命名空间第一次定义的地方，后续再打开命名空间的时候可以写inline，也可以不写。

当应用程序的代码在一次发布和另一次发布之间发生了改变时，常常会用到内联命名空间。

未命名的命名空间
未命名的命名空间是指关键字namespace后紧跟花括号括起来的一系列声明语句。未命名的命名空间中定义的变量拥有静态生命周期：它们在第一次使用前创建，并且直到
程序结束才销毁。

一个未命名的命名空间可以在某个给定的文件内不连续，但是不能跨越多个文件。每个文件定义自己的未命名的命名空间，如果两个文件都含有未命名的命名空间，则这两个空间互相无关。在这两个未命名的命名空间中可以定义相同的名字，并且这些定义表示的是不同实体。如果一个头文件定义了未命名的命名空间，则该命名空间中定义的名字将在每个包含了该头文件的文件中对应不同实体。

记住：和其他命名空间不同，未命名的命名空间仅在特定的文件内部有效，其作用范围不会横跨多个不同的文件。

定义在未命名的命名空间中的名字可以直接使用，毕竟我们找不到什么命名空间的名字来限定它们；同样的，我们也不能对未命名的命名空间的成员使用作用域运算符。

未命名的命名空间中定义的名字的作用域与该命名空间所在的作用域相同。如果未命名的命名空间定义在文件的最外层作用域中，则该命名空间中的名字一定要与全局作用
域中的名字有所区别：
int i;																							//i的全局声明
namespace
{
	int i;
}
//二义性：i的定义即出现在全局作用域中，又出现在未嵌套的未命名的命名空间中
i = 10;

其他情况下，未命名的命名空间中的成员都属于正确的程序实体。和所有命名空间类似，一个未命名的命名空间也能嵌套在其他命名空间当中。此时，未命名的命名空间中
的成员可以通过外层命名空间的名字来访问：
namespace local
{
	namespace
	{
		int i;
	}
}
//正确，定义在嵌套的未命名的命名空间中的i与全局作用域中的i不同
local::i = 42;

未命名的命名空间取代文件中的静态声明
在标准C++引入命名空间的概念之前，程序需要将名字声明成static的以使得其对于整个文件有效。在文件中进行静态声明的做法是从C语言继承而来的。在C语言中，声明
为static的全局实体在其所在的文件外不可见。

在文件中进行静态声明的做法已经被C++标准取消了，现在的做法是使用未命名的命名空间。
*/