#include "URMPixelRectangle.h"

#ifdef URM_HIERARCHICAL_COMPILE

namespace ung
{
	PixelRectangle::PixelRectangle() :
		mStart(nullptr),
		mEntire(true),
		mMinX(-1),
		mMinY(-1),
		mMaxX(-1),
		mMaxY(-1)
	{
	}

	PixelRectangle::PixelRectangle(int32 minx, int32 miny, int32 maxx, int32 maxy) :
		mStart(nullptr),
		mEntire(false),
		mMinX(minx),
		mMinY(miny),
		mMaxX(maxx),
		mMaxY(maxy)
	{
		BOOST_ASSERT(mMinX > 0 && mMinY > 0 && mMaxX > 0 && mMaxY > 0);
	}

	PixelRectangle::~PixelRectangle()
	{
	}

	void PixelRectangle::setStart(void * pBits)
	{
		mStart = pBits;
	}

	void * PixelRectangle::getStart() const
	{
		BOOST_ASSERT(mStart);

		if (mEntire)
		{
			return mStart;
		}
		else
		{
			BOOST_ASSERT(mMinX >= 0 && mMinY >= 0 && mMaxX >= 0 && mMaxY >= 0);

			return mStart;
		}
	}

	int32 PixelRectangle::getMinX() const
	{
		return mMinX;
	}

	int32 PixelRectangle::getMinY() const
	{
		return mMinY;
	}

	int32 PixelRectangle::getMaxX() const
	{
		return mMaxX;
	}

	int32 PixelRectangle::getMaxY() const
	{
		return mMaxY;
	}

	void PixelRectangle::setMinX(int32 x)
	{
		BOOST_ASSERT(x >= 0);

		mMinX = x;
	}

	void PixelRectangle::setMinY(int32 y)
	{
		BOOST_ASSERT(y >= 0);

		mMinY = y;
	}

	void PixelRectangle::setMaxX(int32 x)
	{
		BOOST_ASSERT(x >= 0);

		mMaxX = x;
	}

	void PixelRectangle::setMaxY(int32 y)
	{
		BOOST_ASSERT(y >= 0);

		mMaxY = y;
	}

	bool PixelRectangle::isEntire() const
	{
		return mEntire;
	}
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE