#include "URMMeshParser.h"

#ifdef URM_HIERARCHICAL_COMPILE

#include "URMMeshParserHelper.h"
#include "URMMaterial.h"
#include "URMMesh.h"
#include "URMSubMesh.h"

//“ung::int32”: 将值强制为布尔值“true”或“false”(性能警告)
#pragma warning(disable : 4800)

namespace ung
{
	MeshParser::MeshParser(String const & fileName) :
		PropertyTreeParser(fileName)
	{
	}

	MeshParser::~MeshParser()
	{
	}

	uint32 MeshParser::getMaterialCount()
	{
		String materialCount;
		get("Scene.MaterialCount", materialCount);

		return LexicalCast::stringToInt(materialCount);
	}

	uint32 MeshParser::getMeshCount()
	{
		String meshCount;
		get("Scene.MeshCount", meshCount);

		return LexicalCast::stringToInt(meshCount);
	}

	void MeshParser::readMaterial(ufast index,std::shared_ptr<Material> materialPtr)
	{
		/*
		BOOST_ASSERT(materialPtr);

		using namespace boost::property_tree;

		if (mMaterialsVec.size() == 0)
		{
			for (auto& elem : mTree.get_child("Scene.Materials"))
			{
				mMaterialsVec.push_back(elem.second);
			}

			BOOST_ASSERT(mMaterialsVec.size() == getMaterialCount());
			BOOST_ASSERT(index < mMaterialsVec.size());
		}
		
		//Materials节点中的所有Material节点，找到index所要索引的那个Material节点
		for (ufast i = 0; i < mMaterialsVec.size(); ++i)
		{
			if (i == index)
			{
				ptree currentMaterial = mMaterialsVec[i];

				materialPtr->mName = currentMaterial.get<String>("Name");
				StringUtilities::toLower(materialPtr->mName);
				materialPtr->mAmbient = MeshParserHelper::stringToDVector3(currentMaterial.get<String>("Ambient"));
				materialPtr->mDiffuse = MeshParserHelper::stringToDVector3(currentMaterial.get<String>("Diffuse"));
				materialPtr->mSpecular = MeshParserHelper::stringToDVector3(currentMaterial.get<String>("Specular"));
				materialPtr->mShininess = LexicalCast::stringToDouble(currentMaterial.get<String>("Shininess"));
				materialPtr->mShininessStrength = LexicalCast::stringToDouble(currentMaterial.get<String>("ShininessStrength"));
				materialPtr->mTransparency = LexicalCast::stringToDouble(currentMaterial.get<String>("Transparency"));
				materialPtr->mBlendMode = currentMaterial.get<String>("BlendMode");
				materialPtr->mTwoSide = static_cast<bool>(LexicalCast::stringToInt(currentMaterial.get<String>("TwoSide")));
				materialPtr->mTextureCount = LexicalCast::stringToInt(currentMaterial.get<String>("TextureCount"));

				//遍历Material.Textures
				for (auto& elem : currentMaterial.get_child("Textures"))
				{
					String textureName = elem.second.get_value<String>();
					StringUtilities::toLower(textureName);
					materialPtr->mTextures.push_back(textureName);
				}
				BOOST_ASSERT(materialPtr->mTextures.size() == materialPtr->mTextureCount);

				break;
			}
		}
		*/
	}

	void MeshParser::readMesh(std::shared_ptr<Mesh> meshPtr)
	{
		//BOOST_ASSERT(meshPtr);

		//using namespace boost::property_tree;

		//ptree currentMesh = mTree.get_child("Scene.Mesh");

		//get("Scene.CoordSystem", meshPtr->mCoordSystem);
		//get("Scene.Format", meshPtr->mDataFormat);

		//meshPtr->mFaceCount = LexicalCast::stringToInt(currentMesh.get<String>("FaceCount"));
		//meshPtr->mVertexCount = LexicalCast::stringToInt(currentMesh.get<String>("VertexCount"));

		////遍历Mesh.Positions
		//for (auto& elem : currentMesh.get_child("Positions"))
		//{
		//	meshPtr->mPositions.push_back(MeshParserHelper::stringToDVector3(elem.second.get_value<String>()));
		//}
		//BOOST_ASSERT(meshPtr->mPositions.size() == meshPtr->mVertexCount);

		////遍历Mesh.Tangents
		//for (auto& elem : currentMesh.get_child("Tangents"))
		//{
		//	meshPtr->mTangents.push_back(MeshParserHelper::stringToDVector4(elem.second.get_value<String>()));
		//}
		//BOOST_ASSERT(meshPtr->mTangents.size() == meshPtr->mVertexCount);

		////遍历Mesh.Bitangents
		//for (auto& elem : currentMesh.get_child("Bitangents"))
		//{
		//	meshPtr->mBitangents.push_back(MeshParserHelper::stringToDVector3(elem.second.get_value<String>()));
		//}
		//BOOST_ASSERT(meshPtr->mBitangents.size() == meshPtr->mVertexCount);

		////遍历Mesh.Normals
		//for (auto& elem : currentMesh.get_child("Normals"))
		//{
		//	meshPtr->mNormals.push_back(MeshParserHelper::stringToDVector3(elem.second.get_value<String>()));
		//}
		//BOOST_ASSERT(meshPtr->mNormals.size() == meshPtr->mVertexCount);

		////遍历Mesh.UVs
		//for (auto& elem : currentMesh.get_child("UVs"))
		//{
		//	meshPtr->mUVs.push_back(MeshParserHelper::stringToDVector2(elem.second.get_value<String>()));
		//}
		//BOOST_ASSERT(meshPtr->mUVs.size() == meshPtr->mVertexCount);

		////当前Mesh.SubMeshs中的所有SubMesh节点
		//STL_VECTOR(ptree) subMeshsVec;
		//for (auto& elem : currentMesh.get_child("SubMeshs"))
		//{
		//	subMeshsVec.push_back(elem.second);
		//}
		//BOOST_ASSERT(subMeshsVec.size() == getMaterialCount());
		//meshPtr->mSubMeshCount = subMeshsVec.size();
		////遍历所有的SubMesh节点
		//for (ufast j = 0; j < subMeshsVec.size(); ++j)
		//{
		//	ptree currentSubMesh = subMeshsVec[j];

		//	//当前的SubMesh对象
		//	auto subMeshPtr = meshPtr->createSubMesh();

		//	subMeshPtr->mMaterialName = currentSubMesh.get<String>("MaterialName");
		//	StringUtilities::toLower(subMeshPtr->mMaterialName);
		//	subMeshPtr->mTrianglesMode = currentSubMesh.get<String>("TrianglesMode");
		//	subMeshPtr->mFaceCount = LexicalCast::stringToInt(currentSubMesh.get<String>("FaceCount"));
		//	subMeshPtr->mVertexCount = LexicalCast::stringToInt(currentSubMesh.get<String>("VertexCount"));
		//	subMeshPtr->mIndexCount = LexicalCast::stringToInt(currentSubMesh.get<String>("IndexCount"));

		//	//Indices
		//	for (auto& elem : currentSubMesh.get_child("Indices"))
		//	{
		//		subMeshPtr->mIndices.push_back(LexicalCast::stringToInt(elem.second.get_value<String>()));
		//	}
		//	BOOST_ASSERT(subMeshPtr->mIndices.size() == subMeshPtr->mIndexCount);
		//}

		//if (mMeshsVec.size() == 0)
		//{
		//	for (auto& elem : mTree.get_child("Scene.Meshs"))
		//	{
		//		mMeshsVec.push_back(elem.second);
		//	}

		//	BOOST_ASSERT(mMeshsVec.size() == getMeshCount());
		//	BOOST_ASSERT(index < mMeshsVec.size());
		//}

		//Meshs节点中的所有Mesh节点，找到index所要索引的那个Mesh节点
		//for (ufast i = 0; i < mMeshsVec.size(); ++i)
		//{
		//	if (i == index)
		//	{
		//		

		//		break;
		//	}
		//}
	}
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE