#include "USMSceneNode.h"

#ifdef USM_HIERARCHICAL_COMPILE

#include "USMObject.h"
#include "USMTransformComponent.h"

namespace ung
{
	SceneNode::SceneNode(WeakISceneNodePtr parentPtr, String const& name) :
		mParent(parentPtr),
		mName(name.empty() ? UniqueID().toString() : name),
		mObjects("SceneNode_mObjects")
	{
	}

	SceneNode::~SceneNode()
	{
		//std::cout << "销毁节点:" << mName << std::endl;
	}

	String const& SceneNode::getName() const
	{
		return mName;
	}

	WeakISceneNodePtr SceneNode::getParent() const
	{
		return mParent;
	}

	void SceneNode::preRender()
	{
		//矩阵压入
	}

	void SceneNode::render()
	{

	}

	void SceneNode::postRender()
	{
		//矩阵弹出
	}

	uint32 SceneNode::getChildrenCount()
	{
		return mChildren.size();
	}

	uint32 SceneNode::getFamilyCount()
	{
		uint32 familyCount{};

		auto lambdaExpression = [&familyCount](StrongSceneNodePtr n)
		{
			familyCount += n->mChildren.size();
		};

		preTraverseDepthFirst(lambdaExpression);

		familyCount += mChildren.size();

		return familyCount;
	}

	void SceneNode::detachFromParent()
	{
		if (mParent.expired())
		{
			return;
		}

		//父节点
		auto parent = mParent.lock();

		parent->removeChild(shared_from_this());

		//脱离父节点后，该节点以及其所有的子孙节点都需要更新
		mMovableAttribute.setNeedUpdateFlag(true);
		notifyDescendant();
	}

	void SceneNode::attachToParent(StrongISceneNodePtr parentPtr)
	{
		if (!mParent.expired())
		{
			return;
		}

		parentPtr->saveChild(shared_from_this());

		//关联到父节点后，该节点以及其子孙节点需要更新
		mMovableAttribute.setNeedUpdateFlag(true);
		notifyDescendant();
	}

	void SceneNode::preTraverseDepthFirst(std::function<void(StrongSceneNodePtr)> fn)
	{
		auto beg = mChildren.begin();
		auto end = mChildren.end();

		while (beg != end)
		{
			StrongISceneNodePtr iSceneNode = *beg;
			StrongSceneNodePtr sceneNode = std::static_pointer_cast<SceneNode>(iSceneNode);

			fn(sceneNode);
			sceneNode->preTraverseDepthFirst(fn);

			++beg;
		}
	}

	void SceneNode::postTraverseDepthFirst(std::function<void(StrongSceneNodePtr)> fn)
	{
		auto beg = mChildren.begin();
		auto end = mChildren.end();

		while (beg != end)
		{
			StrongSceneNodePtr sceneNode = std::static_pointer_cast<SceneNode>(*beg);

			sceneNode->postTraverseDepthFirst(fn);

			fn(sceneNode);

			++beg;
		}
	}

	void SceneNode::attachObject(StrongIObjectPtr objectPtr)
	{
		BOOST_ASSERT(objectPtr);

		//如果该对象已经与当前场景节点关联了
		if (objectPtr->getAttachedNode().lock() == shared_from_this())
		{
			return;
		}

		//如果该对象没有与任何场景节点关联
		if (!objectPtr->isAttachSceneNode())
		{
			//设置该对象所关联的场景节点
			objectPtr->setAttachedSceneNode(shared_from_this());

			//当前场景节点持有该对象
			mObjects.save(objectPtr->getName(), objectPtr);

			//关联到一个新的场景节点后，对象需要更新
			objectPtr->getTransformComponent()->setNeedUpdateFlag(true);

			return;
		}
		else
		{
			//如果该对象已经与另外一个场景节点相关联了，那么抛出异常
			UNG_EXCEPTION("This object has been attached to some node already.");
		}
	}

	void SceneNode::detachObject(StrongIObjectPtr objectPtr,bool remove)
	{
		BOOST_ASSERT(objectPtr);

		//给定参数必须是该场景节点所持有的对象
		BOOST_ASSERT(mObjects.getStrong(objectPtr->getName()));

		//放弃对节点指针的拥有权，并重新初始化为一个empty weak pointer
		objectPtr->setAttachedSceneNode(WeakSceneNodePtr{});

		//脱离一个场景节点后，对象需要更新
		objectPtr->getTransformComponent()->setNeedUpdateFlag(true);

		if (remove)
		{
			mObjects.remove(objectPtr->getName());
		}
	}

	uint32 SceneNode::getAttachedObjectCount()
	{
		return mObjects.size();
	}

	uint32 SceneNode::getAttachedAllObjectCount()
	{
		uint32 allObjectCount{};

		auto lambdaExpression = [&allObjectCount](StrongSceneNodePtr n)
		{
			allObjectCount += n->getObjectsSize();
		};

		preTraverseDepthFirst(lambdaExpression);

		allObjectCount += mObjects.size();

		return allObjectCount;
	}

	void SceneNode::update()
	{
		if (mMovableAttribute.getNeedUpdateFlag())
		{
			//计算world
			buildWorld();

			//通知所有的直接对象
			notifyObjects();

			//通知子孙节点
			notifyDescendant();

			mMovableAttribute.setNeedUpdateFlag(false);
		}

		//更新直接对象
		auto beg = mObjects.begin();
		auto end = mObjects.end();
		while (beg != end)
		{
			beg->second->update();

			++beg;
		}
	}

	Movable & SceneNode::getMovableAttribute()
	{
		return mMovableAttribute;
	}

	Movable const & SceneNode::getMovableAttribute() const
	{
		return mMovableAttribute;
	}

	void SceneNode::saveChild(StrongISceneNodePtr childPtr)
	{
		mChildren.push_back(childPtr);

		childPtr->setParent(shared_from_this());
	}

	void SceneNode::removeChild(StrongISceneNodePtr childPtr)
	{
		mChildren.remove(childPtr);

		//重置孩子节点的父节点为空
		childPtr->setParent(WeakSceneNodePtr{});
	}

	void SceneNode::setParent(WeakISceneNodePtr parentPtr)
	{
		mParent = parentPtr;
	}

	bool SceneNode::isRoot() const
	{
		if (mParent.expired())
		{
			return true;
		}

		return false;
	}

	uint32 SceneNode::getObjectsSize() const
	{
		return mObjects.size();
	}

	void SceneNode::notifyDescendant()
	{
		auto lambdaExpression = [](StrongSceneNodePtr n)
		{
			n->getMovableAttribute().setNeedUpdateFlag(true);
		};

		preTraverseDepthFirst(lambdaExpression);
	}

	void SceneNode::notifyObjects()
	{
		auto beg = mObjects.begin();
		auto end = mObjects.end();

		while (beg != end)
		{
			auto object = beg->second;

			object->getTransformComponent()->setNeedUpdateFlag(true);

			++beg;
		}
	}

	void SceneNode::buildWorld()
	{
		StrongISceneNodePtr parent = ungGetStrongPtr(mParent);

		if (parent)
		{
			auto world = parent->getMovableAttribute().getToWorldSQT() * 
				mMovableAttribute.getToParentSQT();
			
			mMovableAttribute.setWorld(world);
		}
	}
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE

/*
Each node has certain properties that affect how the node will draw, such as its material,its
geometric extents, what game actor it represents, and so on.

Every node in the hierarchy has a matrix that describes position and orientation relative to its
parent node.
*/

/*
Scene Graph Basics:
A scene graph is a dynamic data structure, similar to a multiway tree. Each node represents
an object in a 3D world or perhaps an instruction to the renderer. Every node can have zero
or more children nodes. The scene graph is traversed every frame to draw the visible world.
*/