/*
object_pool:
object_pool是用于类对象的内存池，它的功能与pool类似，“但会在析构时对所有已经分配的内存块调用析构函数，从而正确地释放资源”。“要求析构函数不能抛出异常”。
类摘要：
template<typename ElementType>
class object_pool : protected pool
{
public:
	object_pool();
	~object_pool();

	//分配内存
	element_type* malloc();													//并不调用类的构造函数，尽量少使用

	//归还内存
	void free(element_type* p);												//并不调用类的析构函数，尽量少使用

	bool is_from(element_type* p) const;

	//创建对象
	element_type* construct(...);											//最多支持3个参数，先调用malloc()分配内存，然后再在内存块上使用传入的参数调用类的构造函数，返回的是一个已经初始化的对象指针
	//销毁对象
	void destroy(element_type* p);											//先调用对象的析构函数，然后再用free()释放内存块
};
操作函数：
object_pool是pool的子类，但它使用的是保护继承，因此不能使用pool的接口。
object_pool的模板类型参数ElementType指定了object_pool要分配的元素类型，要求其析构函数不能抛出异常。一旦在模板中指定了类型，object_pool实例就不能
再用于分配其他类型的对象。
malloc()和free()函数分别分配和释放一块类型为ElementType*的内存块，但它们被调用时并不调用类的构造函数和析构函数，也就是说，操作的是一块原始内存块，
里面的值是未定义的，因此我们应当尽量少使用malloc()和free()。
object_pool的特殊之处是construct()和destroy()函数，这两个函数是object_pool的真正价值所在。construct()实际上是一组函数，有多个参数的重载形式（目前
最多支持3个参数，但可以扩展），它先调用malloc()分配内存，然后再在内存块上使用传入的参数调用类的构造函数，返回的是一个已经初始化的对象指针。destroy()
则先调用对象的析构函数，然后再用free()释放内存块。
这些函数都不会抛出异常，如果内存分配失败，将返回0。
用法：
struct demo_class
{
public:
	int a, b, c;
	demo_class(int x = 1, int y = 2, int z = 3) :
		a(x),
		b(y),
		c(z)
	{
	}
};

int main()
{
	object_pool<demo_class> pl;															//对象内存池

	//分配一个原始内存块
	demo_class* p = pl.malloc();
	assert(pl.is_from(p));

	//p指向的内存未经过初始化
	assert(p->a != 1 || p->b != 2 || p->c != 3);

	//构造一个对象，可以传递参数
	p = pl.construct(7, 8, 9);
	assert(p->a == 7);

	object_pool<string> pls;																	//定义一个分配string对象的内存池
	for (int i = 0; i < 10; ++i)
	{
		string* ps = pls.construct("hello object_pool");
		cout << *ps << endl;
	}

	return 0;
}//所有创建的对象在这里都被正确析构，释放内存

使用更多的构造参数：
默认情况下，在使用object_pool的construct()的时候我们只能最多使用3个参数来创建对象。大多数情况下这都是足够的，但有的时候我们可能会定义3个以上参数的
构造函数，此时construct()的默认重载形式就不能用了。
但construct()被设计为是可以扩展的，它基于宏预处理m4（通常UNIX系统自带，也有Windows的版本）实现了一个扩展机制，可以自动生成接受任意数量参数的
construct()函数。
pool库在目录boost/pool/detail下提供了一个名为pool_construct.m4和pool_construct_simple.m4的脚本，并同时提供可在UNIX和Windows下运行的同名sh和
bat可执行脚本文件。只需要简单地向批处理脚本传递一个整数的参数N，m4就会自动生成能够创建具有N个参数的construct()函数源代码。例如，在Linux下，执行
命令：./pool_construct_simple.sh 5;./pool_construct.sh 5将生成两个同名的.inc文件，里面包含了新的construct()函数定义，能够支持最多传递5个参数创建
对象。由于m4生成的是C++源代码，因此.inc文件也可以拷贝到其他操作系统的Boost库中使用。
扩展construct()函数时请慎重，数量过多的参数定义会导致程序的编译时间增加，所以最好只定义最合适最需要的参数数量，而不是一味地求多。
如果只是临时的需要增加construct()函数的参数数量，或者工作的系统上m4不可用，我们也可以简单地定义一个辅助模板函数。
下面的代码模仿construct()函数实现了一个可接受4个参数的创建函数：
template<typename P,typename T0,typename T1,typename T2,typename T3>
inline typename P::element_type* construct(P& p, const T0& a0, const T1& a1, const T2& a2, const T3& a3)
{
	typename P::element_type* mem = p.malloc();
	assert(mem);
	new (mem) P::element_type(a0, a1, a2, a3);

	return mem;
}
自由函数construct()接受5个参数，第一个是object_pool对象，其后是创建对象所需的4个参数，要创建的对象类型可以使用object_pool的内部类型定义element_type
来获得。函数中首先调用malloc()分配一块内存，然后调用定位new表达式创建对象。
struct demo_class
{
	demo_class(int, int, int, int)
	{
		cout << "demo_class ctor" << endl;
	}

	~demo_class()
	{
		cout << "demo_class dtor" << endl;
	}
};

object_pool<demo_class> pl;

//那么使用m4和自定义的construct()创建对象的代码就是：
demo_class* d1 = pl.construct(1, 2, 3, 4);													//使用m4扩展
demo_class* d2 = construct(pl, 1, 2, 3, 4);												//使用自定义扩展
*/

/*
#include "boost/pool/pool_alloc.hpp"
容器
*/