#include "URMTextureManager.h"

#ifdef URM_HIERARCHICAL_COMPILE

#include "URMImage.h"

namespace ung
{
	static String texturePath("../../../../Resource/Media/Materials/Textures/");

	TextureManager::TextureManager()
	{
	}

	TextureManager::~TextureManager()
	{
	}

	std::shared_ptr<Image> TextureManager::createTexture(String const& textureName)
	{
		BOOST_ASSERT(!textureName.empty());

		String name = texturePath + textureName;

		pointer tex(UNG_NEW_SMART Image(name));

		BOOST_ASSERT(tex);

		addTexture(textureName,tex);

		return tex;
	}

	void TextureManager::eraseTexture(String const & textureName)
	{
		String name = texturePath + textureName;

		removeTexture(name);
	}

	void TextureManager::eraseTexture(pointer texturePtr)
	{
		//removeTexture(texturePtr->getName());
	}

	void TextureManager::buildTexture(pointer texturePtr)
	{
		texturePtr->build();
	}

	TextureManager::const_pointer TextureManager::getTexture(String const & textureName) const
	{
		BOOST_ASSERT(!textureName.empty() && "texture name invalid.");

		String name = texturePath + textureName;

		auto findRet = mTextures.find(name);

		if (findRet == mTextures.end())
		{
			return nullptr;
		}

		return findRet->second;
	}

	TextureManager::pointer TextureManager::getTexture(String const & textureName)
	{
		BOOST_ASSERT(!textureName.empty() && "texture name invalid.");

		String name = texturePath + textureName;

		auto findRet = mTextures.find(name);

		if (findRet == mTextures.end())
		{
			return nullptr;
		}

		return findRet->second;
	}

	void TextureManager::addTexture(String const & textureName, pointer texturePtr)
	{
		BOOST_ASSERT(!textureName.empty());
		BOOST_ASSERT(texturePtr);

		String name = texturePath + textureName;

#if UNG_DEBUGMODE
		auto findRet = getTexture(name);
		BOOST_ASSERT(!findRet);
#endif

		mTextures[name] = texturePtr;
	}

	void TextureManager::removeTexture(String const& textureName)
	{
		BOOST_ASSERT(!textureName.empty());

		String name = texturePath + textureName;

#if UNG_DEBUGMODE
		auto findRet = getTexture(name);
		BOOST_ASSERT(findRet);
#endif

		auto eraseRet = mTextures.erase(name);

		BOOST_ASSERT(eraseRet == 1);
	}
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

/*
关于Manager：
1：是否有存在的必要？
有，因为需要有一个对象来统一，集中管理元素们。
2：Manager是否应该具有创建和销毁元素的接口？
应该，因为需要在创建和销毁元素的同时，来实时更新容器。
3：Manager是否应该再次包装元素的具体行为接口？
不应该，因为这样会产生大量的，不必要的冗余。
4：Manager应该提供一个通过某种查询来得到元素对象的接口，以便于在客户代码中去执行元素的具体行为。
*/

/*
标准容器：
在容器中存放的元素类型为某个类对象，那么当容器离开作用域销毁的时候，会调用这个对象的析构函数。(在容器离开作用域之前，手动调用clear()函数，效果一样)
在容器中存放的元素类型为指向某个类对象的普通指针，那么当容器离开作用域销毁的时候，不会调用普通指针所指向的对象的析构函数。(在容器离开作用域之前，手动调
用clear()函数，效果一样)，认为普通指针是scalar type，do nothing。
在容器中存放的元素类型为指向某个类对象的智能指针，那么当容器离开作用域销毁的时候，不会调用智能指针所指向的对象的析构函数，而当智能指针的计数为0的时候，
会调用其所指对象的析构函数。
*/

/*
ptr_vector在离开作用域时，会调用普通指针所指对象的析构函数。(在容器离开作用域之前，手动调用clear()函数，效果一样)release也同样会调用析构函数，原本是不会
删除指针，仅仅是释放了所有权，但是如果配合内存池的话，就直接删除指针了。
Debug:push_back()操作，ptr_vector比std::vector略慢
Release:push_back()操作，ptr_vector和std::vector基本持平（认为完全可以取代std::vector）
*/