#include "UPECone.h"

#ifdef UPE_HIERARCHICAL_COMPILE

namespace ung
{
	Cone::Cone(Vector3 const & top, Vector3 const & dir, real_type h, Radian halfAngle) :
		mTop(top),
		mDir(dir),
		mHeight(h),
		mHalfAngle(halfAngle)
	{
		if (!mDir.isUnitLength())
		{
			mDir.normalize();
		}
	}

	Cone::Cone(Vector3 const & top, Vector3 const & dir, real_type h, Degree halfAngle) :
		mTop(top),
		mDir(dir),
		mHeight(h),
		mHalfAngle(halfAngle.toRadian())
	{
		if (!mDir.isUnitLength())
		{
			mDir.normalize();
		}
	}

	Vector3 const & Cone::getTop() const
	{
		return mTop;
	}

	Vector3 const & Cone::getDir() const
	{
		return mDir;
	}

	real_type Cone::getHeight() const
	{
		return mHeight;
	}

	Radian Cone::getHalfAngle() const
	{
		return mHalfAngle;
	}

	real_type Cone::getRadius() const
	{
		return Math::calTan(mHalfAngle) * mHeight;
	}
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE