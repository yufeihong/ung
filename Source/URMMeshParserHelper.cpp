#include "URMMeshParserHelper.h"

#ifdef URM_HIERARCHICAL_COMPILE

namespace ung
{
#define STRING_DVector2(vec)		\
Vector2(LexicalCast::stringToDouble(vec[0]),LexicalCast::stringToDouble(vec[1]))

#define STRING_DVector3(vec)		\
Vector3(LexicalCast::stringToDouble(vec[0]),LexicalCast::stringToDouble(vec[1]),LexicalCast::stringToDouble(vec[2]))

#define STRING_DVector4(vec)		\
Vector4(LexicalCast::stringToDouble(vec[0]),LexicalCast::stringToDouble(vec[1]),LexicalCast::stringToDouble(vec[2]),LexicalCast::stringToDouble(vec[3]))

	Vector2 MeshParserHelper::stringToDVector2(String& stringValue,const char* pred)
	{
		STL_VECTOR(String) vecResult;

		StringUtilities::splitToVector(vecResult,stringValue,pred);

		return STRING_DVector2(vecResult);
	}

	Vector3 MeshParserHelper::stringToDVector3(String& stringValue, const char* pred)
	{
		STL_VECTOR(String) vecResult;

		StringUtilities::splitToVector(vecResult, stringValue, pred);

		return STRING_DVector3(vecResult);
	}

	Vector4 MeshParserHelper::stringToDVector4(String& stringValue, const char* pred)
	{
		STL_VECTOR(String) vecResult;

		StringUtilities::splitToVector(vecResult, stringValue, pred);

		return STRING_DVector4(vecResult);
	}
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE