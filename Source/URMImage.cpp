#include "URMImage.h"

#ifdef URM_HIERARCHICAL_COMPILE

#if UNG_DEBUGMODE
#include "UFCPerformance.h"
#include "UFCFilesystem.h"
#endif
/*
FreeImage is an Open Source library project for developers who would like to support popular graphics image formats like PNG, BMP, JPEG, TIFF 
and others as needed by today's multimedia applications. FreeImage is easy to use, fast, multithreading safe, compatible with all 32-bit or 64-bit 
versions of Windows, and cross-platform (works both with Linux and Mac OS X).
*/
#include "freeimage.h"
#include "boost/interprocess/mapped_region.hpp"
#include "boost/interprocess/file_mapping.hpp"

namespace ung
{
	//free image的FREE_IMAGE_FORMAT到Image::ImageFileType的映射
	static Image::ImageFileType freeImageFileTypeToFileType(FREE_IMAGE_FORMAT freeImageFileType)
	{
		Image::ImageFileType fileType = Image::IFT_UNKNOWN;

		switch (freeImageFileType)
		{
		case FIF_BMP:
			fileType = Image::IFT_BMP;
			break;
		case FIF_ICO:
			fileType = Image::IFT_ICO;
			break;
		case FIF_JPEG:
			fileType = Image::IFT_JPEG;
			break;
		case FIF_PNG:
			fileType = Image::IFT_PNG;
			break;
		case FIF_TARGA:
			fileType = Image::IFT_TGA;
			break;
		case FIF_TIFF:
			fileType = Image::IFT_TIFF;
			break;
		case FIF_PSD:
			fileType = Image::IFT_PSD;
			break;
		case FIF_XPM:
			fileType = Image::IFT_XPM;
			break;
		case FIF_DDS:
			fileType = Image::IFT_DDS;
			break;
		case FIF_GIF:
			fileType = Image::IFT_GIF;
			break;
		case FIF_HDR:
			fileType = Image::IFT_HDR;
			break;
		case FIF_RAW:
			fileType = Image::IFT_RAW;
			break;
		default:
			BOOST_ASSERT(0 && "unknown free image file type.");
			break;
		}//switch

		return fileType;
	}

	//Image::ImageFileType到free image的FREE_IMAGE_FORMAT的映射
	static FREE_IMAGE_FORMAT fileTypeToFreeImageFileType(Image::ImageFileType fileType)
	{
		FREE_IMAGE_FORMAT freeImageType = FIF_UNKNOWN;

		switch (fileType)
		{
		case Image::IFT_BMP:
			freeImageType = FIF_BMP;
			break;
		case Image::IFT_ICO:
			freeImageType = FIF_ICO;
			break;
		case Image::IFT_JPEG:
			freeImageType = FIF_JPEG;
			break;
		case Image::IFT_PNG:
			freeImageType = FIF_PNG;
			break;
		case Image::IFT_TGA:
			freeImageType = FIF_TARGA;
			break;
		case Image::IFT_TIFF:
			freeImageType = FIF_TIFF;
			break;
		case Image::IFT_PSD:
			freeImageType = FIF_PSD;
			break;
		case Image::IFT_XPM:
			freeImageType = FIF_XPM;
			break;
		case Image::IFT_DDS:
			freeImageType = FIF_DDS;
			break;
		case Image::IFT_GIF:
			freeImageType = FIF_GIF;
			break;
		case Image::IFT_HDR:
			freeImageType = FIF_HDR;
			break;
		case Image::IFT_RAW:
			freeImageType = FIF_RAW;
			break;
		default:
			BOOST_ASSERT(0 && "unknown file type.");
			break;
		}//switch

		return freeImageType;
	}

	//free image的FREE_IMAGE_TYPE到Image::PixelBitType的映射
	static Image::PixelBitType freeImageTypeToPixelBitType(FREE_IMAGE_TYPE freeImageType)
	{
		Image::PixelBitType pixelBitType = Image::PBT_UNKNOWN;

		switch (freeImageType)
		{
		case FIT_BITMAP:
			pixelBitType = Image::PBT_BITMAP;
			break;
		case FIT_UINT16:
			pixelBitType = Image::PBT_UINT16;
			break;
		case FIT_INT16:
			pixelBitType = Image::PBT_INT16;
			break;
		case FIT_UINT32:
			pixelBitType = Image::PBT_UINT32;
			break;
		case FIT_INT32:
			pixelBitType = Image::PBT_INT32;
			break;
		case FIT_FLOAT:
			pixelBitType = Image::PBT_FLOAT;
			break;
		case FIT_DOUBLE:
			pixelBitType = Image::PBT_DOUBLE;
			break;
		case FIT_COMPLEX:
			pixelBitType = Image::PBT_COMPLEX;
			break;
		case FIT_RGB16:
			pixelBitType = Image::PBT_RGB16;
			break;
		case FIT_RGBA16:
			pixelBitType = Image::PBT_RGBA16;
			break;
		case FIT_RGBF:
			pixelBitType = Image::PBT_RGBF;
			break;
		case FIT_RGBAF:
			pixelBitType = Image::PBT_RGBAF;
			break;
		default:
			BOOST_ASSERT(0 && "unknown free image type.");
			break;
		}//switch

		return pixelBitType;
	}

	//free image的FREE_IMAGE_COLOR_TYPE到Image::ColorType
	static Image::ColorType freeImageColorTypeToColorType(FREE_IMAGE_COLOR_TYPE freeImageColorType)
	{
		Image::ColorType colorType = Image::CT_UNKNOWN;

		switch (freeImageColorType)
		{
		case FIC_RGB:
			colorType = Image::CT_RGB;
			break;
		case FIC_RGBALPHA:
			colorType = Image::CT_RGBA;
			break;
		default:
			BOOST_ASSERT(0 && "unknown color type");
			break;
		}

		return colorType;
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------------

	class Image::Impl
	{
	public:
		Impl(/*String const& fullName*/) :
			//mFileName(fullName),
			mFileType(FIF_UNKNOWN),
			mBitMap(nullptr),
			mPixelBitType(FIT_UNKNOWN),
			mColorType(FIC_CMYK),
			mLoaded(false),
			mBPP(0),
			mWidthInPixels(0),
			mHeightInPixels(0),
			mWidthInBytes(0),
			mPitchInBytes(0),
			mIsTransparent(-1),
			mData(nullptr)
		{
#ifdef FREEIMAGE_LIB
			/*
			Initialises the library. When the load_local_plugins_only parameter is TRUE, FreeImage won’t make use of external plugins.
			When using the FreeImage DLL, this function is called automatically with the load_local_plugins_only parameter set to FALSE. When using 
			FreeImage as a static linked library, you must call this function exactly once at the start of your program.
			*/
			FreeImage_Initialise();
#endif

#if UNG_DEBUGMODE
			BOOL isLittleEndian = FreeImage_IsLittleEndian();
			BOOST_ASSERT(isLittleEndian);

			/*
			Returns a string containing the current version of the library.
			*/
			char const* version = FreeImage_GetVersion();
#endif
		}

		~Impl()
		{
			if (mBitMap)
			{
				mFileType = FIF_UNKNOWN;

				/*
				Deletes a previously loaded FIBITMAP from memory.
				*/
				FreeImage_Unload(mBitMap);
				mBitMap = nullptr;

				mPixelBitType = FIT_UNKNOWN;
				mColorType = FIC_CMYK;
				mLoaded = false;
				mBPP = 0;
				mWidthInPixels = 0;
				mHeightInPixels = 0;
				mWidthInBytes = 0;
				mPitchInBytes = 0;
				mIsTransparent = -1;
				mData = nullptr;
			}

#ifdef FREEIMAGE_LIB
			/*
			Deinitialises the library.
			When using the FreeImage DLL, this function is called automatically. When using FreeImage as a static linked library, you must call this function 
			exactly once at the end of your program to clean up allocated resources in the FreeImage library.
			*/
			FreeImage_DeInitialise();
#endif
		}

		//String mFileName;
		FREE_IMAGE_FORMAT mFileType;
		FIBITMAP* mBitMap;
		FREE_IMAGE_TYPE mPixelBitType;
		FREE_IMAGE_COLOR_TYPE mColorType;
		bool mLoaded;
		uint8 mBPP;
		usize mWidthInPixels;
		usize mHeightInPixels;
		usize mWidthInBytes;
		usize mPitchInBytes;
		int8 mIsTransparent;
		uint8* mData;
	};

	//---------------------------------------------------------------------------------------------------------------------------------------------------

	std::shared_ptr<Resource> Image::creatorCallback(String const& fullName)
	{
		std::shared_ptr<Resource> tex(UNG_NEW_SMART Image(fullName));

		BOOST_ASSERT(tex);

		return tex;
	}

	Image::Image(String const& fullName) :
		Resource(fullName,"Image")
	{
		std::lock_guard<std::recursive_mutex> lg(mRecursiveMutex);

		mImpl = std::make_shared<Impl>(/*fullName*/);
	}

	Image::Image(Image&& im) noexcept
	{
		std::lock(mRecursiveMutex, im.mRecursiveMutex);
		std::lock_guard<std::recursive_mutex> lg1(mRecursiveMutex,std::adopt_lock);
		std::lock_guard<std::recursive_mutex> lg2(im.mRecursiveMutex,std::adopt_lock);

		mImpl = std::move(im.mImpl);
	}

	Image& Image::operator=(Image&& im) noexcept
	{
		std::lock(mRecursiveMutex, im.mRecursiveMutex);
		std::lock_guard<std::recursive_mutex> lg1(mRecursiveMutex, std::adopt_lock);
		std::lock_guard<std::recursive_mutex> lg2(im.mRecursiveMutex, std::adopt_lock);

		if (this != &im)
		{
			mImpl = std::move(im.mImpl);
		}

		return *this;
	}

	Image::~Image()
	{
	}

	void Image::build(/*bool fromMemory*/)
	{
		bool fromMemory = true;
		auto bindLoad = std::bind(&Image::load,shared_from_this(), fromMemory);
		auto fut = ThreadPool::getInstance().submit(bindLoad);
		setFuture(fut);
	}

	Image::ImageFileType Image::getImageFileType()
	{
		std::lock_guard<std::recursive_mutex> lg(mRecursiveMutex);

		if (mImpl->mFileType == FIF_UNKNOWN)
		{
			mImpl->mFileType = FreeImage_GetFileType(getFullName().data());

			if (mImpl->mFileType == FIF_UNKNOWN)
			{
				/*
				The following functions retrieve the FREE_IMAGE_FORMAT from a bitmap by reading up to 16 bytes and analysing it.
				Note that for some bitmap types no FREE_IMAGE_FORMAT can be retrieved. This has to do with the bit-layout of the bitmap-types, which are
				sometimes not compatible with FreeImage’s file-type retrieval system. The unidentifiable formats are: CUT, MNG, PCD, TARGA and WBMP.
				However, these formats can be identified using the FreeImage_GetFIFFromFilename function.

				DLL_API FREE_IMAGE_FORMAT DLL_CALLCONV FreeImage_GetFileType(const char *filename, int size FI_DEFAULT(0));
				Orders FreeImage to analyze the bitmap signature. The function then returns one of the predefined FREE_IMAGE_FORMAT constants or a bitmap
				identification number registered by a plugin. The size parameter is currently not used and can be set to 0.

				Because not all formats can be identified by their header (some images don't have a header or one at the end of the file),FreeImage_GetFileType
				may return FIF_UNKNOWN whereas a plugin is available for the file being analysed. In this case,you can use FreeImage_GetFIFFromFilename to
				guess the file format from the file extension, but this last function is slower and less accurate.
				*/
				//no signature,try to guess the file format from the file extension
				mImpl->mFileType = FreeImage_GetFIFFromFilename(getFullName().data());

				BOOST_ASSERT(mImpl->mFileType != FIF_UNKNOWN);
			}
		}

		return freeImageFileTypeToFileType(mImpl->mFileType);
	}

	Image::PixelBitType Image::getPixelBitType()
	{
		isLoaded();

		if (mImpl->mPixelBitType == PBT_UNKNOWN)
		{
			/*
			Returns the data type of a bitmap.
			*/
			mImpl->mPixelBitType = FreeImage_GetImageType(mImpl->mBitMap);
		}
		
		return freeImageTypeToPixelBitType(mImpl->mPixelBitType);
	}

	Image::ColorType Image::getColorType()
	{
		isLoaded();

		if (mImpl->mColorType == FIC_CMYK)
		{
			/*
			Investigates the color type of the bitmap by reading the bitmap’s pixel bits and analysing them.
			FIC_RGB:High-color bitmap (16, 24 or 32 bit), RGB16 or RGBF.
			FIC_RGBALPHA:High-color bitmap with an alpha channel (32 bit bitmap, RGBA16 or RGBAF).
			FIC_CMYK:CMYK bitmap (32 bit only).
			*/
			mImpl->mColorType = FreeImage_GetColorType(mImpl->mBitMap);
		}

		return freeImageColorTypeToColorType(mImpl->mColorType);
	}

	bool Image::load(bool fromMemory)
	{
		if (fromMemory)
		{
			loadFromMemory();
		}
		else
		{
			loadFromFile();
		}

		return true;
	}

	void Image::setFuture(std::future<bool>& fut)
	{
		//只有主线程访问mNakedFuture，所以无需同步
		mNakedFuture = std::move(fut);
	}

	bool Image::isLoaded()
	{
		/*
		线程池中的线程，不会访问mImpl->mLoaded，这样，即便线程池中的线程正在执行task，而其它线程需要访问isLoaded()，读取到的mImpl->mLoaded值
		也为false。
		*/
		if (mImpl->mLoaded)
		{
			return mImpl->mLoaded;
		}

		if (mNakedFuture.valid())
		{
			//在这里，主线程可能会block。
			bool getRet = mNakedFuture.get();

			BOOST_ASSERT(getRet);

			//至此，线程池中的task已经结束，只有主线程访问，故，无需lock
			BOOST_ASSERT(!mImpl->mLoaded);
			mImpl->mLoaded = getRet;
		}

		return mImpl->mLoaded;
	}

	void Image::save(ImageFileType fileType,String const & fileName)
	{
		isLoaded();

		BOOST_ASSERT(!fileName.empty());

		/*
		This function saves a previously loaded FIBITMAP to a file. The first parameter defines the type of the bitmap to be saved. For example, when 
		FIF_BMP is passed, a BMP file is saved (an overview of possible FREE_IMAGE_FORMAT constants is available in Table 1).The second parameter 
		is the name of the bitmap to be saved. If the file already exists it is overwritten. Note that some bitmap save plugins have restrictions on the 
		bitmap types they can save. For example, the JPEG plugin can only save 24 bit and 8 bit greyscale bitmaps*.The last parameter is used to 
		change the behaviour or enable a feature in the bitmap plugin.Each plugin has its own set of parameters.
		*/
		FreeImage_Save(fileTypeToFreeImageFileType(fileType), mImpl->mBitMap,fileName.c_str());
	}

	uint8 Image::getBPP()
	{
		isLoaded();

		if (mImpl->mBPP == 0)
		{
			/*
			Returns the size of one pixel in the bitmap in bits. For example when each pixel takes 32-bits of space in the bitmap, this function returns 32.
			Possible bit depths are 1, 4, 8, 16, 24, 32 for standard bitmaps and 16-, 32-, 48-, 64-, 96- and 128-bit for non standard bitmaps.
			*/
			mImpl->mBPP = FreeImage_GetBPP(mImpl->mBitMap);
		}

		return mImpl->mBPP;
	}

	usize Image::getWidthInPixels()
	{
		isLoaded();

		if (mImpl->mWidthInPixels == 0)
		{
			//Returns the width of the bitmap in pixel units.
			mImpl->mWidthInPixels = FreeImage_GetWidth(mImpl->mBitMap);
		}

		return mImpl->mWidthInPixels;
	}

	usize Image::getHeightInPixels()
	{
		isLoaded();

		if (mImpl->mHeightInPixels == 0)
		{
			//Returns the height of the bitmap in pixel units.
			mImpl->mHeightInPixels = FreeImage_GetHeight(mImpl->mBitMap);
		}

		return mImpl->mHeightInPixels;
	}

	usize Image::getWidthInBytes()
	{
		isLoaded();

		if (mImpl->mWidthInBytes == 0)
		{
			//Returns the width of the bitmap in bytes.
			mImpl->mWidthInBytes = FreeImage_GetLine(mImpl->mBitMap);
		}

		return mImpl->mWidthInBytes;
	}

	uint8 Image::getPixelSizeInBytes()
	{
		return getWidthInBytes() / getWidthInPixels();
	}

	usize Image::getPitchInBytes()
	{
		isLoaded();

		if (mImpl->mPitchInBytes == 0)
		{
			/*
			Returns the width of the bitmap in bytes, rounded to the next 32-bit boundary, also known as pitch or stride or scan width.
			In FreeImage each scanline starts at a 32-bit boundary for performance reasons.

			When the source bitmap uses a 32-bit padding, you can calculate the pitch using the following formula:
			int pitch = ((((bpp * width) + 31) / 32) * 4);
			*/
			mImpl->mPitchInBytes = FreeImage_GetPitch(mImpl->mBitMap);

			BOOST_ASSERT(mImpl->mPitchInBytes == (getBPP() * getWidthInPixels() + 31) / 32 * 4);
		}

		return mImpl->mPitchInBytes;
	}

	bool Image::isTransparent()
	{
		isLoaded();

		if (mImpl->mIsTransparent == -1)
		{
			/*
			Returns TRUE when the transparency table is enabled (1-, 4- or 8-bit images) or when the input dib contains alpha values (32-bit images,
			RGBA16 or RGBAF images). Returns FALSE otherwise.
			*/
			mImpl->mIsTransparent = static_cast<int>(FreeImage_IsTransparent(mImpl->mBitMap));
		}

		return mImpl->mIsTransparent == 1;
	}

	uint8* Image::getData()
	{
		isLoaded();

		if (!mImpl->mData)
		{
			/*
			Returns a pointer to the data-bits of the bitmap.
			For a performance reason, the address returned by FreeImage_GetBits is aligned on a 16 bytes alignment boundary.
			*/
			mImpl->mData = static_cast<uint8*>(FreeImage_GetBits(mImpl->mBitMap));
		}

		return mImpl->mData;
	}

	void Image::flipHorizontal()
	{
		isLoaded();

		BOOL ret = FreeImage_FlipHorizontal(mImpl->mBitMap);

		BOOST_ASSERT(ret);
	}

	void Image::flipVertical()
	{
		isLoaded();

		BOOL ret = FreeImage_FlipVertical(mImpl->mBitMap);

		BOOST_ASSERT(ret);
	}

	void Image::loadFromFile()
	{
		std::lock_guard<std::recursive_mutex> lg(mRecursiveMutex);

		BOOST_ASSERT(!mImpl->mBitMap);

		if (mImpl->mFileType == FIF_UNKNOWN)
		{
			getImageFileType();
		}

		BOOST_ASSERT(FreeImage_FIFSupportsReading(mImpl->mFileType));

		mImpl->mBitMap = FreeImage_Load(mImpl->mFileType, getFullName().data());

		BOOST_ASSERT(mImpl->mBitMap);
	}

	void Image::loadFromMemory()
	{
		std::lock_guard<std::recursive_mutex> lg(mRecursiveMutex);

		BOOST_ASSERT(!mImpl->mBitMap);

		using namespace boost::interprocess;

		mapped_region region{ file_mapping(getFullName().data(), read_only),read_only };
		unsigned char* source = reinterpret_cast<unsigned char*>(region.get_address());
		size_t size = region.get_size();

		/*
		Open a memory stream. The function returns a pointer to the opened memory stream.When called with default arguments (0), this function 
		opens a memory stream for read / write access. The stream will support loading and saving of FIBITMAP in a memory file (managed internally 
		by FreeImage). It will also support seeking and telling in the memory file.This function can also be used to wrap a memory buffer provided by 
		the application driving FreeImage. A buffer containing image data is given as function arguments data (start of the buffer) and size_in_bytes 
		(buffer size in bytes). A memory buffer wrapped by FreeImage is read only. Images can be loaded but cannot be saved.
		*/
		//attach the binary data to a memory stream
		FIMEMORY* mem = FreeImage_OpenMemory(source,size);
		
		//get the file type
		mImpl->mFileType = FreeImage_GetFileTypeFromMemory(mem);
		
		BOOST_ASSERT(FreeImage_FIFSupportsReading(mImpl->mFileType));
		/*
		This function does for memory streams what FreeImage_Load does for file streams.FreeImage_LoadFromMemory decodes a bitmap, allocates 
		memory for it and then returns it as a FIBITMAP.
		*/
		//load an image from the memory stream，这个函数花费时间。
		mImpl->mBitMap = FreeImage_LoadFromMemory(mImpl->mFileType, mem);

		BOOST_ASSERT(mImpl->mBitMap);

		//always close the memory stream
		FreeImage_CloseMemory(mem);
	}
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE

/*
the bitmap doesn’t have a palette (i.e. when the pixel bit depth is greater than 8)
Only palletised bitmaps have a transparency table.High - color bitmaps store the transparency values directly in the bitmap bits.

grey = (0.2126 x R + 0.7152 x G + 0.0722 x B)
*/

/*
DLL_API FIBITMAP *DLL_CALLCONV FreeImage_ConvertFromRawBits(BYTE *bits, int width, int height, int pitch, unsigned bpp,
														unsigned red_mask, unsigned green_mask, unsigned blue_mask, BOOL topdown FI_DEFAULT(FALSE));
1 4 8 16 24 32,these numbers indicate the pixel depth of the input image that the function can operate on.
Converts a raw bitmap somewhere in memory to a FIBITMAP. The parameters in this function are used to describe the raw bitmap. The first 
parameter is a pointer to the start of the raw bits. The width and height parameter describe the size of the bitmap. The pitch defines the total 
width of a scanline in the source bitmap, including padding bytes that may be applied. The bpp parameter tells FreeImage what the bit depth of 
the bitmap is. The red_mask, green_mask and blue_mask parameters tell FreeImage the bit-layout of the color components in the bitmap. The 
last parameter, topdown, will store the bitmap top-left pixel first when it is TRUE or bottom-left pixel first when it is FALSE.
*/

/*
DLL_API FIBITMAP *DLL_CALLCONV FreeImage_ConvertFromRawBitsEx(BOOL copySource, BYTE* bits, FREE_IMAGE_TYPE type, int width,
			int height, int pitch, unsigned bpp, unsigned red_mask, unsigned green_mask, unsigned blue_mask, BOOL topdown FI_DEFAULT(FALSE));
Converts a raw bitmap somewhere in memory to a FIBITMAP. Conversion can be done without allocating an internal pixel buffer.
copySource:if FALSE, wrap the user's pixel buffer, otherwise, make a deep copy
bits:pointer to the start of the raw bits
type:image type
width:image width
height:image height
pitch:image pitch
bpp:image pixel's bit depth
red_mask:bit-layout of the red color components in the image
green_mask:bit-layout of the green color components in the image
blue_mask:bit-layout of the blue color components in the image
topdown:store the bitmap top-left pixel first when it is TRUE or bottom-left pixel first when it is FALSE
*/

/*
static void testBasicWrapper(BOOL copySource, BYTE *bits, FREE_IMAGE_TYPE type, int width, int height, int pitch, unsigned bpp)
{
	FIBITMAP *src = NULL;
	FIBITMAP *clone = NULL;
	FIBITMAP *dst = NULL;
	//allocate a wrapper
	src = FreeImage_ConvertFromRawBitsEx(copySource, bits, type, width, height, pitch,bpp, FI_RGBA_RED_MASK, FI_RGBA_GREEN_MASK,
																	FI_RGBA_BLUE_MASK, FALSE);
	assert(src != NULL);
	//test clone
	clone = FreeImage_Clone(src);
	assert(clone != NULL);
	//test in-place processing
	FreeImage_Invert(src);
	//test processing
	dst = FreeImage_ConvertToFloat(src);
	assert(dst != NULL);
	FreeImage_Unload(dst);
	FreeImage_Unload(clone);
	//unload the wrapper
	FreeImage_Unload(src);
}

static void testViewport(FIBITMAP *dib)
{
	FIBITMAP *src = NULL;
	//define a viewport as [vp_x, vp_y, vp_width, vp_height]
	//(assume the image is larger than the viewport)
	int vp_width = 300;
	int vp_height = 200;
	int vp_x = FreeImage_GetWidth(dib) / 2 - vp_width / 2;
	int vp_y = FreeImage_GetHeight(dib) / 2 - vp_height / 2;
	//point the viewport data
	unsigned bytes_per_pixel = FreeImage_GetLine(dib) / FreeImage_GetWidth(dib);
	BYTE *data = FreeImage_GetBits(dib) + vp_y * FreeImage_GetPitch(dib) + vp_x *
	bytes_per_pixel;
	//wrap a section (no copy)
	src = FreeImage_ConvertFromRawBitsEx(FALSE, data, FIT_BITMAP,vp_width, vp_height, FreeImage_GetPitch(dib), FreeImage_GetBPP(dib),
																	FI_RGBA_RED_MASK, FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK);
	//save the section (note that the image is inverted due to previous processing)
	FreeImage_Save(FIF_PNG, src, "viewport png");
	//unload the wrapper
	FreeImage_Unload(src);
}

//Main test functions
//----------------------------------------------------------
void testWrappedBuffer(const char *lpszPathName, int flags)
{
	FIBITMAP *dib = NULL;
	//simulate a user provided buffer
	//-------------------------------
	//load the dib
	FREE_IMAGE_FORMAT fif = FreeImage_GetFileType(lpszPathName);
	dib = FreeImage_Load(fif, lpszPathName, flags);
	assert(dib != NULL);
	//get data info
	FREE_IMAGE_TYPE type = FreeImage_GetImageType(dib);
	unsigned width = FreeImage_GetWidth(dib);
	unsigned height = FreeImage_GetHeight(dib);
	unsigned pitch = FreeImage_GetPitch(dib);
	unsigned bpp = FreeImage_GetBPP(dib);
	BYTE *bits = FreeImage_GetBits(dib);
	//test wrapped buffer manipulations
	//-------------------------------
	testBasicWrapper(TRUE, bits, type, width, height, pitch, bpp);
	testBasicWrapper(FALSE, bits, type, width, height, pitch, bpp);
	//test another use-case : viewport
	testViewport(dib);
	//unload the user provided buffer
	//-------------------------------
	FreeImage_Unload(dib);
}
*/

/*
Converts a FIBITMAP to a raw piece of memory.
DLL_API void DLL_CALLCONV FreeImage_ConvertToRawBits(BYTE *bits, FIBITMAP *dib, int pitch, unsigned bpp, unsigned red_mask,
																						unsigned green_mask, unsigned blue_mask, BOOL topdown FI_DEFAULT(FALSE));
topdown, will store the bitmap top-left pixel first when it is TRUE or bottom-left pixel first when it is FALSE.
1 4 8 16 24 32
*/

/*
//this code assumes there is a bitmap loaded and
//present in a variable called ‘dib’
//convert a bitmap to a 32-bit raw buffer (top-left pixel first)
//--------------------------------------------------------------
FIBITMAP *src = FreeImage_ConvertTo32Bits(dib);
FreeImage_Unload(dib);
//Allocate a raw buffer
int width = FreeImage_GetWidth(src);
int height = FreeImage_GetHeight(src);
int scan_width = FreeImage_GetPitch(src);
BYTE *bits = (BYTE*)malloc(height * scan_width);
//convert the bitmap to raw bits (top-left pixel first)
FreeImage_ConvertToRawBits(bits, src, scan_width, 32,FI_RGBA_RED_MASK, FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK,TRUE);
FreeImage_Unload(src);
//convert a 32-bit raw buffer (top-left pixel first) to a FIBITMAP
//----------------------------------------------------------------
FIBITMAP *dst = FreeImage_ConvertFromRawBits(bits, width, height, scan_width,32, FI_RGBA_RED_MASK, FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK, FALSE);
*/

/*
DLL_API FIBITMAP *DLL_CALLCONV FreeImage_Rotate(FIBITMAP *dib, double angle, const void *bkcolor FI_DEFAULT(NULL));
1 8 24 32 16UINT16 48RGB16 64RGBA16 32FLOAT 96RGBF 128RGBAF

This function rotates a standard image (1-, 8-bit greyscale or a 24-, 32-bit color), a RGB(A)16 or RGB(A)F image by means of 3 shears. The 
angle of rotation is specified by the angle parameter in degrees. Rotation occurs around the center of the image area. Rotated image retains size 
and aspect ratio of source image (destination image size is usually bigger), so that this function should be used when rotating an image by 90°,
180° or 270°.

For 1-bit images, rotation is limited to angles whose value is an integer multiple of 90° (e.g. –90, 90, 180, 270). A NULL value is returned for 
other angles.

When the angle value isn’t an integer multiple of 90°, the background is filled with the supplied bkcolor parameter. When bkcolor is NULL (default 
value), the background is filled with a black color. The data type of bkcolor depends on the image type.
*/
//perform a 90° rotation (CCW rotation)
//FIBITMAP* rotated = FreeImage_Rotate(memImage, 90);

/*
DLL_API FIBITMAP * DLL_CALLCONV FreeImage_Rescale(FIBITMAP *dib, int dst_width, int dst_height, FREE_IMAGE_FILTER filter);
1 4 8 16 24 32 16UINT16 48RGB16 64RGBA16 32FLOAT 96RGBF 128RGBAF

This function performs resampling (or scaling, zooming) of a greyscale or RGB(A) image to the desired destination width and height. A NULL 
value is returned when the bitdepth cannot be handled or when there’s not enough memory (this may happen with very large images).

Images whose image type is FIT_BITMAP are returned as 8-bit or 24-bit, or as 32-bit if they contain transparency. For example, 16-bit RGB 
bitmap are returned as 24-bit. Non transparent palettized and 4-bit bitmap are returned as 24-bit images. The algorithm tries to produce 
destination images with the smallest possible bit depth.
If you have transparency, you'll get a 32-bit image. If you have real colors, you'll get a 24-bit image. For all other cases, you'll get an 8-bit image 
with a linear color palette (which defaults to MINISBLACK; it is MINISWHITE only, if the source image was of type MINISWHITE).

The following filters can be used as resampling filters:
Filter flag									Description
FILTER_BOX								Box, pulse, Fourier window, 1st order (constant) B-Spline
FILTER_BILINEAR						Bilinear filter
FILTER_BSPLINE							4th order (cubic) B-Spline
FILTER_BICUBIC							Mitchell and Netravali's two-param cubic filter
FILTER_CATMULLROM					Catmull-Rom spline, Overhauser spline
FILTER_LANCZOS3						Lanczos-windowed sinc filter
*/

/*
DLL_API FIBITMAP *DLL_CALLCONV FreeImage_RescaleRect(FIBITMAP *dib, int dst_width, int dst_height, int left, int top, int right, int bottom,
																	FREE_IMAGE_FILTER filter FI_DEFAULT(FILTER_CATMULLROM), unsigned flags FI_DEFAULT(0));
1 4 8 16 24 32 16UINT16 48RGB16 64RGBA16 32FLOAT 96RGBF 128RGBAF

FreeImage_RescaleRect provides support for rescaling only a rectangular area of an image.
Basically, that's a much faster solution than coding:
FIBITMAP *dibTmp = FreeImage_Copy(dib, 10, 10, 140, 140);
FIBITMAP *dibDst = FreeImage_Rescale(dibTmp, 260, 260);
FreeImage_Unload(dibTmp);
Much faster and easier to write is:
FIBITMAP *dibDst = FreeImage_RescaleRect(dib, 260, 260, 10, 10, 140, 140);
Of course, the function does not rely on FreeImage_Copy but reads and rescales the bits inside the specified rectangle only.
Additionally, function FreeImage_RescaleRect takes a flags parameter. Currently, there are 3 flags/options defined and implemented:
RescaleRect flag										Description
FI_RESCALE_DEFAULT								Default options; none of the following other options apply.
FI_RESCALE_TRUE_COLOR						For non-transparent greyscale images, convert to 24-bit if src bitdepth <= 8(default is a 8-bit greyscale image).
FI_RESCALE_OMIT_METADATA					Do not copy metadata to the rescaled image.

About the FI_RESCALE_TRUE_COLOR option:
By using flag FI_RESCALE_TRUE_COLOR, one can ensure not getting a palletized 8-bit image but a true color result image (24- or 32-bit, depending 
on transparency), when rescaling an image with a bit depth smaller than or equal to 8 (src bpp <= 8), that consists of gray colors only (r=g=b).
By default, FreeImage_Rescale returns an image with the smallest possible bit depth (which is 8-bit for a grayscale image). However, it can be 
annoying to get either a true color image or a palletized image for a palletized source image, depending on the color-awareness of the source image 
only (especially, if the rescaled image needs further processing).
*/

/*
DLL_API FIBITMAP *DLL_CALLCONV FreeImage_GetThumbnail(FIBITMAP *dib);
Some image formats allow a thumbnail image to be embedded together with the output image file. When this thumbnail image is present in a file,
it is automatically loaded by FreeImage(whatever the loading flag, even when using the FIF_LOAD_NOPIXEL flag).Image formats that currently 
support thumbnail loading are JPEG (Exif or JFIF formats), PSD,EXR, TGA and TIFF.
FreeImage_GetThumbnail retrieves a link to the thumbnail that may be available with a dib.

DLL_API BOOL DLL_CALLCONV FreeImage_SetThumbnail(FIBITMAP *dib, FIBITMAP *thumbnail);
Attach a thumbnail image to a dib, so that it can be later stored together with the dib to an ouput image file format.
If input parameter thumbnail is NULL then the thumbnail is deleted from the dib.Image formats that currently support thumbnail saving are JPEG 
(JFIF formats), EXR, TGA and TIFF.

Thumbnail images are almost always standard bitmaps (e.g. images with a FIT_BITMAP image type). The JPEG format supports 8- or 24-bit 
thumbnails, while the EXR format only supports 32-bit thumbnails. The TGA format needs a thumbnail with the same bit depth as the image. The 
TIF format has no restriction regarding the thumbnail bit depth, but a standard bitmap type is recommended.

DLL_API FIBITMAP *DLL_CALLCONV FreeImage_MakeThumbnail(FIBITMAP *dib, int max_pixel_size, BOOL convert FI_DEFAULT(TRUE));
1 4 8 16 24 32 16UINT16 48RGB16 64RGBA16 32FLOAT 96RGBF 128RGBAF

Creates a thumbnail from a greyscale or RGB(A) image so that the output image fits inside a square of size max_pixel_size, keeping aspect ratio.

Downsampling is done using a bilinear filter (see FreeImage_Rescale). 16-bit RGB bitmap are returned as 24-bit. Palettized and 4-bit bitmap are 
returned as 8-bit or as 32-bit if they contain transparency.

When the convert parameter is set to TRUE, High Dynamic Range images (FIT_UINT16,FIT_RGB16, FIT_RGBA16, FIT_FLOAT) are transparently 
converted to standard images (i.e. 8-, 24 or 32-bit images), using one of the FreeImage_ConvertToXXX conversion function. As for RBG[A]F 
images, they are converted to 24-bit using the FreeImage_TmoDrago03 function with default options.
*/

//****************************************************************************************************************

/*
FreeImage uses the RGB(A) color model to represent color images in memory. A 8-bit greyscale image has a single channel, often called the black 
channel. A 24-bit image is made up of three 8-bit channels: one for each of the red, green and blue colors. For 32-bit images, a fourth 8-bit 
channel, called alpha channel, is used to create and store masks, which let you manipulate, isolate, and protect specific parts of an image. Unlike 
the others channels, the alpha channel doesn’t convey color information, in a physical sense.

Color manipulation functions used in FreeImage allow you to modify the histogram(直方图) of a specific channel. This transformation is known as 
a point operation, and may be used to adjust brightness, contrast(对比，反差) or gamma of an image, to perform image enhancement (e.g.
histogram equalization(直方图均衡化), non-linear contrast adjustment) or even to invert or threshold an image.

the following channels are defined in FreeImage:
Channel flag				Description
FICC_RGB					Function applies to red, green and blue channels
FICC_RED					Function applies to red channel only
FICC_GREEN				Function applies to green channel only
FICC_BLUE					Function applies to blue channel only
FICC_ALPHA				Function applies to alpha channel only
FICC_BLACK				Function applies to black channel
FICC_REAL					Complex images: function applies to the real part
FICC_IMAG					Complex images: function applies to the imaginary part
FICC_MAG					Complex images: function applies to the magnitude
FICC_PHASE				Complex images: function applies to the phase
*/

/*
ColorType colorType = getColorType();

if (colorType == CT_RGB || colorType == CT_RGBA)
{
	//In FreeImage, FIBITMAP are based on a coordinate system that is upside down relative to usual graphics conventions. Thus, the scanlines are
	//stored upside down,with the first scan in memory being the bottommost scan in the image.

	//FreeImage uses a BGR[A] pixel layout under a Little Endian processor (Windows, Linux) and uses a RGB[A] pixel layout under a Big Endian
	//processor (Mac OS X or any Big Endian Linux / Unix).

	//Returns a bit pattern describing the red color component of a pixel in a FIBITMAP, returns 0 otherwise.
	//16711680:0xFF0000
	//65280:0x00FF00
	//255:0x0000FF
	uint32 redMask = FreeImage_GetRedMask(mImpl->mBitMap);
	uint32 greenMask = FreeImage_GetGreenMask(mImpl->mBitMap);
	uint32 blueMask = FreeImage_GetBlueMask(mImpl->mBitMap);

	BOOST_ASSERT(redMask == 0xFF0000);
	BOOST_ASSERT(greenMask == 0x00FF00);
	BOOST_ASSERT(blueMask == 0x0000FF);
}
*/

/*
FreeImage_Allocate
1 4 8 16 24 32
DLL_API FIBITMAP *DLL_CALLCONV FreeImage_Allocate(int width, int height, int bpp,
										unsigned red_mask FI_DEFAULT(0), unsigned green_mask FI_DEFAULT(0), unsigned blue_mask FI_DEFAULT(0));
When red_mask is 0xFF000000 this means that the last 8 bits in one pixel are used for the color red. When green_mask is 0x000000FF, it means 
that the first 8 bits in a pixel are used for the color green.

FreeImage_Allocate is an alias for FreeImage_AllocateT and can be replaced by this call:
FreeImage_AllocateT(FIT_BITMAP, width, height, bpp, red_mask, green_mask,blue_mask);

FIBITMAP* createdTexture = FreeImage_Allocate(512, 512, 32, 0xFF000000,0x00FF0000,0x0000FF00);
if (createdTexture)
{
	FreeImage_Unload(createdTexture);
}
*/

/*
Bitmap Color Depth:
Different bitmap formats allocate a certain number of bits for red, green, blue, and alpha 
channels. Some formats are indexed, meaning that the pixel data is actually an index into a 
color table that stores the actual RGBA values. Here’s a list of the most common formats:
32-bit (8888 RGBA): The least compact way to store bitmaps, but retains the most information.

24-bit (888 RGB): This format is common for storing backgrounds that have too much color 
data to be represented in either 8-bit indexed or 16-bit formats and have no need for an alpha 
channel.

24-bit (565 RGB, 8 A): This format is great for making nice-looking bitmaps with a good 
alpha channel. Green gets an extra bit because the human eye is more sensitive to changes in 
green than red or blue.

16-bit (565 RGB): This compact format is used for storing bitmaps with more varieties of 
color and no alpha channel.

16-bit (555 RGB, 1 A): This compact format leaves one bit for translucency,which is 
essentially a chroma key.

8-bit indexed: A compact way to store bitmaps that have large areas of subtly shaded colors.
some of the indexes can be reserved for different levels of translucency.
*/

/*
Character textures for high-definition(高清晰度) console games can be as large as 2048 × 2048.
They also have multiple layered maps for specular and emissive effects that weigh in at 512 × 512 
or 1024 × 1024.
*/

/*
A textured object with a mip-map will look good no matter how far away the viewer is from 
the textured object. If you’ve ever seen a really cheap 3D game where the object textures 
flashed or scintillated(闪烁) all the time, it’s because the game didn’t use mip-mapped textures.
A mip-map precalculates(预先计算) the image of a texture at different distances.
The renderer will choose one or even blend more than one of these mip-maps to render the 
final pixels on the polygon.This creates a smooth textured effect, no matter how the viewpoint 
is moving.

A full mip-map for a texture takes 33 percent more space than the texture does by itself.
Games almost always pregenerate(预先生成) their mip-maps and store them in the resource 
file rather than generating them on the fly. There are two reasons for this. First, a good 
mip-map takes a long time to generate, and the second reason is that even a crappy mip-map 
takes longer to generate on the fly than it takes to load from disc.
*/