#include "USMPhysicsComponent.h"

#ifdef USM_HIERARCHICAL_COMPILE

using namespace tinyxml2;

namespace ung
{
	PhysicsComponent::PhysicsComponent() :
		ObjectComponent("PhysicsComponent")
	{
	}

	PhysicsComponent::~PhysicsComponent()
	{
	}

	void PhysicsComponent::init(XMLElement* pComponentElement)
	{
		BOOST_ASSERT(pComponentElement);

		XMLElement* pShapeElement = pComponentElement->FirstChildElement("Shape");
		if (pShapeElement)
		{
			mShape = pShapeElement->FirstChild()->Value();
		}
		const char* shape = pShapeElement->GetText();
		//string = pShape->FirstChild()->Value();
		//pPositionElement->QueryDoubleAttribute("y", position.getAddress() + 1);
		//pPositionElement->QueryDoubleAttribute("z", position.getAddress() + 2);

		//mLocal.setTranslate(position);
	}

	void PhysicsComponent::postInit()
	{
		if (mOwner)
		{
		}
	}

	void PhysicsComponent::update()
	{
	}
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE