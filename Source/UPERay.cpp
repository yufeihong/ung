#include "UPERay.h"

#ifdef UPE_HIERARCHICAL_COMPILE

#include "UPECollisionDetection.h"

namespace ung
{
	Ray::Ray()
	{
	}

	Ray::~Ray()
	{
	}

	Ray::Ray(const Vector3& o, const Vector3& d) :
		mOrigin(o),
		mDirection(d)
	{
		if (!mDirection.isUnitLength())
		{
			mDirection.normalize();
		}
	}

	Ray::Ray(const Ray& r)
	{
		mOrigin = r.mOrigin;
		mDirection = r.mDirection;
	}

	Ray& Ray::operator=(const Ray& r)
	{
		if (this != &r)
		{
			mOrigin = r.mOrigin;
			mDirection = r.mDirection;
		}

		return *this;
	}

	void Ray::setOrigin(const Vector3& o)
	{
		mOrigin = o;
	}

	const Vector3& Ray::getOrigin() const
	{
		return mOrigin;
	}

	void Ray::setDirection(const Vector3& d)
	{
		mDirection = d;
		if (!mDirection.isUnitLength())
		{
			mDirection.normalize();
		}
	}

	const Vector3& Ray::getDirection() const
	{		
		return mDirection;
	}

	Vector3 Ray::getPoint(real_type t) const
	{
		BOOST_ASSERT(mDirection.isUnitLength());

		return Vector3(mOrigin + t * mDirection);
	}

	Ray & Ray::operator*=(Matrix4 const & mat4)
	{
		mOrigin += mat4.getTrans();

		mDirection *= mat4;

		return *this;
	}

	Ray Ray::operator*(Matrix4 const & mat4)
	{
		Ray temp(mOrigin, mDirection);

		temp *= mat4;

		return temp;
	}
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

 /*
 射线的参数形式:
 x(t) = x0 + t * detx
 y(t) = y0 + t * dety
 z(t) = z0 + t * detz		参数t的范围为0到1.

 向量记法:
 p(t) = p0 + tu
 射线的起点p(0) = p0.这样,p0指定了射线的位置信息,同时增量向量u指定了它的长度和方向.射线的终点p(1) = p0 + u (注意u是向量)
 在一些相交性测试中,我们使用上式的一种变形:u为单位向量,参数t从0变化到l,l是射线的长度.
 */

 /*
 射线的参数定义为:
 pRay = p0 + t * dRay
 平面方程为:
 pPlane.n = dPlane

 解相交点的t值:
 (p0 + t * dRay).n = dPlane				其中(p0 + t * dRay)为射线上的某一点
 p0.n + (t * dRay).n = dPlane
 (t * dRay).n = dPlane - p0.n
 t = (dPlane - p0.n) / (dRay.n)
 如果射线和平面互相平行,分母为0,则它们之间没有交点.
 (我们仅讨论与平面的正面相交的情况.在这种情况下,仅当射线的方向和平面的法向量相反时才有交点,此时分母小于0)
 如果t超出了取值范围,说明射线和平面不相交.
 */

 /*
 图元表示方法:
 1:隐式表示
 通过定义一个布尔函数f(x,y,z),我们能够隐式表示一个图元.如果所指定的点在这个图元上,这个布尔函数就为真;对于其他的点,这个布尔函数为假.
 例如等式:x * x + y * y + z * z = 1			对中心在原点的单位球表面上的所有点为真.
 隐式表示法用于测试图元是否包含某点时非常有用.

 2:参数形式表示:
 定义如下两个关于t的函数:
 x(t) = cos(2PIt)
 y(t) = sin(2PIt)
 这里t被称作参数,并和所用的坐标系无关.当t从0变化到1时,点(x(t),y(t))的轨迹就是所要描述的形状.这组等式表示的是一个中心在原点的单位圆.
 尽管可以让t在我们想要的任意范围内变化,但是在大多数情况下,把参数的变换范围限制在0到1之间会比较方便一些.另一种常见的变换范围是从0到l,
 l是图元的"长度".
 如果函数只使用一个参数,就称这些函数为单变量的.单变量函数的轨迹是一条曲线.
 有时候函数可能有多于一个的参数,双变量函数接受两个参数,经常设为s和t.双变量函数的轨迹是一个曲面.

 3:"直接"形式表示
 例如,用两个端点来表示一个线段.用球心和半径来表示一个球.
 */