#include "UPEPlane.h"

#ifdef UPE_HIERARCHICAL_COMPILE

#include "UPETriangle.h"
#include "UPEAABB.h"
#include "UPECollisionDetection.h"

namespace ung
{
	Plane::Plane()
	{
	}

	Plane::~Plane()
	{
	}

	Plane::Plane(const Vector3& n, const real_type d) :
		mN(n),
		mD(d)
	{
		recalculate();
	}

	Plane::Plane(real_type nx, real_type ny, real_type nz, real_type d) :
		mN(nx, ny, nz),
		mD(d)
	{
		recalculate();
	}

	Plane::Plane(const Vector3& n, const Vector3& p) :
		mN(n),
		mD(n.dotProduct(p))
	{
		recalculate();
	}

	Plane::Plane(const Vector3& p0, const Vector3& p1, const Vector3& p2)
	{
		Vector3 e1 = p1 - p0;
		Vector3 e2 = p2 - p0;

		//检测是否共线
#if UNG_DEBUGMODE
		Vector3 e11 = e1.normalizeCopy();
		Vector3 e22 = e2.normalizeCopy();
		real_type dotRet = e11.dotProduct(e22);
		BOOST_ASSERT(!(Math::isEqual(dotRet, 1.0)));
#endif

		mN = e1.crossProductUnit(e2);
		mD = mN.dotProduct(p0);

		recalculate();
	}

	Plane::Plane(Triangle const & tri) :
		//委托构造函数
		Plane(tri.getVertices()[0],tri.getVertices()[1],tri.getVertices()[2])
	{
	}

	Plane::Plane(const Plane& p) :
		mN(p.mN),
		mD(p.mD)
	{
	}

	Plane& Plane::operator=(const Plane& p)
	{
		mN = p.mN;
		mD = p.mD;

		return *this;
	}

	bool Plane::operator==(const Plane& p) const
	{
		return (mN == p.mN && mD == p.mD);
	}

	bool Plane::operator!=(const Plane& p) const
	{
		return (mN != p.mN || mD != p.mD);
	}

	const Vector3 Plane::getN() const
	{
		return mN;
	}

	const real_type Plane::getD() const
	{
		return mD;
	}

	const real_type Plane::getSignedDistanceFromOriginToPlane() const
	{
		auto ret = mD * -1.0;

#if UNG_DEBUGMODE
		BOOST_ASSERT(mN.isUnitLength());
		auto t = mN.dotProduct(Vector3(0.0,0.0,0.0)) - mD;

		BOOST_ASSERT(Math::isEqual(ret,t));
#endif

		return ret;
	}

	Vector3 Plane::projectionVector(const Vector3& q) const
	{
		Matrix3 projMat = Matrix3::makeOrthogonal(mN);

		return q * projMat;
	}

	void Plane::transform(Matrix4 const & mat4)
	{
		/*
		We can transform a plane(n,d) by treating it as a 4D vector and multiplying it by the 
		inverse-transpose of the desired transformation matrix.
		Note that the plane's normal vector must be normalized first.
		*/

		BOOST_ASSERT(Math::isUnit(mN.squaredLength()));

		Matrix4 inverseTransposeMat{};
		//先计算逆矩阵，然后把计算得来的逆矩阵进行转置
		auto ret = mat4.getInverseTranspose(inverseTransposeMat);
		BOOST_ASSERT(ret);

		//推导时，所基于的计算d的方程不同，所以这里为-d
		Vector4 p(mN,-mD);
		p *= inverseTransposeMat;

		//重置平面的法线和d
		mN.x = p.x;
		mN.y = p.y;
		mN.z = p.z;
		mD = p.w;

		recalculate();
	}

	void Plane::recalculate()
	{
		real_type len2 = mN.squaredLength();
		if (Math::isUnit(len2))
		{
			return;
		}

		BOOST_ASSERT(!Math::isZero(len2));

		real_type invLen = Math::calInvSqrt(len2);

		mN.x *= invLen;
		mN.y *= invLen;
		mN.z *= invLen;
		mD *= invLen;
	}
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

/*
平面可用一个向量n和平面中一点p0来描述。其中向量n称为平面的法向量，与平面垂直。
平面是所有满足下式的点p的集合。
n · (p - p0) = 0
也就是说，p0为平面上的一点，如果向量(p - p0)与该平面的法向量垂直，则点p位于该平面上。
描述一个具体的平面时，由于法向量n和平面上一点p0都是确定的，因此可将上式写作更常用的形式：
n · p + d = 0
其中d = -n · p0
推导：n · (p - p0) = 0 ---> n · p - n · p0 = 0 ---> n · p + (-n · p0) = 0 ---> d = -n · p0
上面的推导，基于：A · (B + C) = A · B + A · C，其中A,B,C都是向量。
如果平面的法向量的模为1，则d = -n · p0给出了坐标原点到该平面最短的有符号距离（signed distance）。
将平面看成一个4D向量，记为(n,d)。
*/

 /*
 隐式定义:
 平面的隐式定义由所有满足平面方程的点p = (x,y,z)给出.
 ax + by + cz = d
 p.n = d
 第二种形式中,n = [a,b,c].一旦知道n,就能用任意已知的平面上的点来计算d.
 向量n也称作平面的法向量,因为它垂直于平面.
 让我们来验证一下,设p和q都在平面上,满足平面方程,将p和q带入有:
 p.n = d
 n.p = d
 n.q = d

 n.p = n.q
 n.p - n.q = 0
 n.(p - q) = 0
 最后一行点乘的几何意义就是n垂直于从q到p的向量.这对于平面上的任意p,q点都是成立的.因此,n垂直于平面上的任意向量.
 将n限制为单位长度并不会失去一般性,而且通常会给计算带来方便.
 另外,平面有正面和反面,一般来说,n的正方向就是平面的正面(Front Side).
 */