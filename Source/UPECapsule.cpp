#include "UPECapsule.h"

#ifdef UPE_HIERARCHICAL_COMPILE

namespace ung
{
	Capsule::Capsule(Vector3 const& start, Vector3 const& end, real_type radius) :
		mLineSegmentStart(start),
		mLineSegmentEnd(end),
		mRadius(radius)
	{
	}

	Vector3 const & Capsule::getStart() const
	{
		return mLineSegmentStart;
	}

	Vector3 const & Capsule::getEnd() const
	{
		return mLineSegmentEnd;
	}

	real_type Capsule::getRadius() const
	{
		return mRadius;
	}
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE