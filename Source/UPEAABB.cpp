#include "UPEAABB.h"

#ifdef UPE_HIERARCHICAL_COMPILE

#include "UPECollisionDetection.h"

namespace
{
	/*!
	 * \remarks 对一个给定的AABB进行旋转和平移变换
	 * \return 返回一个临时的变换后的AABB，不要再对这个AABB进行旋转
	 * \param ung::AABB const & aabb
	 * \param const ung::Matrix4 & matrix
	*/
	ung::AABB transform(ung::AABB const& aabb, const ung::Matrix4& matrix)
	{
		/*
		Transforming AABBs:
		Sometimes we need to transform an AABB from one coordinate space to another.
		For example, let’s say that we have the AABB in object space and we want to get an 
		AABB in world space.
		Of course, in theory, we could compute a world-space AABB of the object itself.However,
		we assume that the description of the object shape (perhaps a triangle mesh with a 
		thousand vertices) is more complicated than the AABB that we already have computed 
		in object space. So to get an AABB in world space, we will transform the object-space 
		AABB.
		To compute a new AABB,we must transform the eight corner points, and then form an 
		AABB from these eight transformed points.
		Let’s quickly review what happens when we transform a 3D point by a 3 × 3 matrix:
		(x′ y′ z′) = (x y z) |11 21 31|
								   |12 22 32|
								   |13 23 33|
		x′ = m11x + m21y + m31z,
		y′ = m12x + m22y + m32z,
		z′ = m13x + m23y + m33z.
		we wish to find the minimum value of m11x + m21y + m31z,where [x, y, z] is any of 
		the original eight corner points.Our job is to figure out which of these corner points 
		would have the smallest x value after transformation.The trick to minimizing the entire 
		sum is to minimize each of the three products individually.
		Let’s look at the first product,m11x. We must decide which of xmin or xmax to 
		substitute(替代) for x in order to minimize the product.Obviously, if m11 > 0, then the 
		smaller of the two, xmin, will result in the smaller product.Conversely(反过来),if m11 < 0,
		then xmax gives smaller product. Conveniently(方便地), whichever of xmin or xmax we
		use for computing x′ min, we use the other value for computing x′ max. We then apply 
		this process for each of the nine elements in the matrix.
		*/

		auto modelMin = aabb.getMin();
		auto modelMax = aabb.getMax();

		//取出矩阵的行
		auto r0 = matrix.getRow(0);
		auto r1 = matrix.getRow(1);
		auto r2 = matrix.getRow(2);

		//从矩阵中获取到平移向量
		ung::Vector3 translate;
		matrix.getTrans(translate);

		ung::Vector3 min{translate}, max{translate};

		if (r0.x > 0.0)
		{
			min.x += r0.x * modelMin.x;
			max.x += r0.x * modelMax.x;
		}
		else
		{
			min.x += r0.x * modelMax.x;
			max.x += r0.x * modelMin.x;
		}

		if (r0.y > 0.0)
		{
			min.y += r0.y * modelMin.x;
			max.y += r0.y * modelMax.x;
		}
		else
		{
			min.y += r0.y * modelMax.x;
			max.y += r0.y * modelMin.x;
		}

		if (r0.z > 0.0)
		{
			min.z += r0.z * modelMin.x;
			max.z += r0.z * modelMax.x;
		}
		else
		{
			min.z += r0.z * modelMax.x;
			max.z += r0.z * modelMin.x;
		}

		if (r1.x > 0.0)
		{
			min.x += r1.x * modelMin.y;
			max.x += r1.x * modelMax.y;
		}
		else
		{
			min.x += r1.x * modelMax.y;
			max.x += r1.x * modelMin.y;
		}

		if (r1.y > 0.0)
		{
			min.y += r1.y * modelMin.y;
			max.y += r1.y * modelMax.y;
		}
		else
		{
			min.y += r1.y * modelMax.y;
			max.y += r1.y * modelMin.y;
		}

		if (r1.z > 0.0)
		{
			min.z += r1.z * modelMin.y;
			max.z += r1.z * modelMax.y;
		}
		else
		{
			min.z += r1.z * modelMax.y;
			max.z += r1.z * modelMin.y;
		}

		if (r2.x > 0.0)
		{
			min.x += r2.x * modelMin.z;
			max.x += r2.x * modelMax.z;
		}
		else
		{
			min.x += r2.x * modelMax.z;
			max.x += r2.x * modelMin.z;
		}

		if (r2.y > 0.0)
		{
			min.y += r2.y * modelMin.z;
			max.y += r2.y * modelMax.z;
		}
		else
		{
			min.y += r2.y * modelMax.z;
			max.y += r2.y * modelMin.z;
		}

		if (r2.z > 0.0)
		{
			min.z += r2.z * modelMin.z;
			max.z += r2.z * modelMax.z;
		}
		else
		{
			min.z += r2.z * modelMax.z;
			max.z += r2.z * modelMin.z;
		}

		ung::AABB out{min,max};

#if UNG_DEBUGMODE
		out.mRotated = true;
#endif

		return out;
	}
}//namespace

namespace ung
{
	AABB::AABB()
	{
		setEmpty();
	}

	AABB::~AABB()
	{
	}

	AABB::AABB(const Vector3& min, const Vector3& max) :
		mMin(min),
		mMax(max)
	{
	}

	AABB::AABB(real_type minx, real_type miny, real_type minz, real_type maxx, real_type maxy, real_type maxz) :
		mMin(minx, miny, minz),
		mMax(maxx, maxy, maxz)
	{
	}

	AABB::AABB(const AABB& box) :
		mMin(box.mMin),
		mMax(box.mMax)
	{
	}

	AABB& AABB::operator=(const AABB& box)
	{
		mMin = box.mMin;
		mMax = box.mMax;

		return *this;
	}

	AABB::AABB(AABB && box) noexcept :
		mMin(box.mMin),
		mMax(box.mMax)
	{
	}

	AABB & AABB::operator=(AABB && box) noexcept
	{
		if (this != &box)
		{
			mMin = box.mMin;
			mMax = box.mMax;
		}

		return *this;
	}

	void AABB::setEmpty()
	{
		mMin.x = mMin.y = mMin.z = Math::POS_INFINITY;
		mMax.x = mMax.y = mMax.z = Math::NEG_INFINITY;
	}

	bool AABB::isEmpty() const
	{
		return mMin > mMax;
	}

	const Vector3& AABB::getMin() const
	{
		BOOST_ASSERT(!isEmpty());

		return mMin;
	}

	Vector3& AABB::getMin()
	{
		BOOST_ASSERT(!isEmpty());

		return mMin;
	}

	const Vector3& AABB::getMax() const
	{
		BOOST_ASSERT(!isEmpty());

		return mMax;
	}

	Vector3& AABB::getMax()
	{
		BOOST_ASSERT(!isEmpty());

		return mMax;
	}

	Vector3 AABB::getCenter() const
	{
		BOOST_ASSERT(!isEmpty());

		return (mMin + mMax) * 0.5;
	}

	Vector3 AABB::getSize() const
	{
		BOOST_ASSERT(!isEmpty());

		return mMax - mMin;
	}

	Vector3 AABB::getHalfSize() const
	{
		BOOST_ASSERT(!isEmpty());

		return (mMax - mMin) * 0.5;
	}

	real_type AABB::getRadius() const
	{
		BOOST_ASSERT(!isEmpty());

		return getHalfSize().length();
	}

	void AABB::setMin(const Vector3& min)
	{
		mMin = min;
	}

	void AABB::setMin(real_type minx, real_type miny, real_type minz)
	{
		mMin.x = minx;
		mMin.y = miny;
		mMin.z = minz;
	}

	void AABB::setMax(const Vector3& max)
	{
		mMax = max;
	}

	void AABB::setMax(real_type maxx, real_type maxy, real_type maxz)
	{
		mMax.x = maxx;
		mMax.y = maxy;
		mMax.z = maxz;
	}

	Vector3 AABB::getIndexedCorner(uint32 index) const
	{
		//0:+++,1:++-,2:+-+,3:+--,4:-++,5:-+-,6:--+,7:---

		BOOST_ASSERT(index >= 0 && index <= 7);

		auto center = getCenter();
		auto halfSize = getHalfSize();

		auto cx = center.x;
		auto cy = center.y;
		auto cz = center.z;

		auto hx = halfSize.x;
		auto hy = halfSize.y;
		auto hz = halfSize.z;

		Vector3 out{};

		switch (index)
		{
		case 0:
			out.x = cx + hx;
			out.y = cy + hy;
			out.z = cz + hz;
			break;
		case 1:
			out.x = cx + hx;
			out.y = cy + hy;
			out.z = cz - hz;
			break;
		case 2:
			out.x = cx + hx;
			out.y = cy - hy;
			out.z = cz + hz;
			break;
		case 3:
			out.x = cx + hx;
			out.y = cy - hy;
			out.z = cz - hz;
			break;

		case 4:
			out.x = cx - hx;
			out.y = cy + hy;
			out.z = cz + hz;
			break;
		case 5:
			out.x = cx - hx;
			out.y = cy + hy;
			out.z = cz - hz;
			break;
		case 6:
			out.x = cx - hx;
			out.y = cy - hy;
			out.z = cz + hz;
			break;
		case 7:
			out.x = cx - hx;
			out.y = cy - hy;
			out.z = cz - hz;
			break;
		}

		return out;
	}

	void AABB::scale(const Vector3& s)
	{
		BOOST_ASSERT(!isEmpty());

		mMin *= s;
		mMax *= s;
	}

	void AABB::merge(const AABB& obj)
	{
		mMin.setFloor(obj.mMin);
		mMax.setCeil(obj.mMax);
	}

	void AABB::merge(const Vector3& point)
	{
		mMin.setFloor(point);
		mMax.setCeil(point);
	}

	bool AABB::contain(const AABB& other) const
	{
		return mMin.x <= other.mMin.x && mMin.y <= other.mMin.y && mMin.z <= other.mMin.z &&
			mMax.x >= other.mMax.x && mMax.y >= other.mMax.y && mMax.z >= other.mMax.z;
	}

	bool AABB::operator==(const AABB& rhs) const
	{
		BOOST_ASSERT(!isEmpty() && !rhs.isEmpty());

		return this->mMin == rhs.mMin && this->mMax == rhs.mMax;
	}

	bool AABB::operator!= (const AABB& rhs) const
	{
		return !(*this == rhs);
	}

	//----------------------------------------------------------------------------------

	AABB operator*(AABB const& box, Matrix3 const& mat3)
	{
		BOOST_ASSERT(!box.mRotated);

		Matrix4 temp{mat3};
		
		return transform(box, temp);
	}

	AABB operator*(AABB const& box, Matrix4 const& mat4)
	{
		BOOST_ASSERT(!box.mRotated);

		return transform(box, mat4);
	}

	AABB operator*(AABB const& box, Quaternion const& rot)
	{
		BOOST_ASSERT(!box.mRotated);

		Matrix4 temp{ rot };

		return transform(box, temp);
	}

	std::ostream& operator<<(std::ostream& o, const AABB& aabb)
	{
		o << "AxisAlignedBox(min=" << aabb.mMin << ",max=" << aabb.mMax << ")";
		return o;
	}
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

 /*
 Axially Aligned Bounding Box
 AABB内的满足下列不等式:
 xmin <= x <= xmax
 ymin <= y <= ymax
 zmin <= z <= zmax
 特别重要的两个顶点为:
 Pmin = [xmin,ymin,zmin]
 Pmax = [xmax,ymax,zmax]
 中心点c为:
 c = (Pmin + Pmax) / 2
 "尺寸向量"s是从Pmin指向Pmax的向量,包含了矩形边界框的长,宽,高:
 s = Pmax - pmin
 还可以求出矩形边界框的"半径向量"r,它是从中心指向Pmax的向量:
 r = Pmax - c
   = s / 2
 明确地定义一个AABB只需要Pmin,Pmax,c,s,r这5个向量中的两个(除s和r不能配对外)
 */

 /*
 析构函数执行与构造函数相反的操作:构造函数初始化对象的非static数据成员,析构函数释放对象使用的资源,并销毁对象的非static数据成员.
 由于析构函数不接受参数,因此它不能被重载.对一个给定类,只会有唯一一个析构函数.

 如同构造函数有一个初始化部分和一个函数体,析构函数也有一个函数体和一个析构部分.在一个构造函数中,成员的初始化是在函数体执行之前完
 成的,且按照它们在类中出现的顺序进行初始化.在一个析构函数中,首先执行函数体,然后销毁成员.成员按初始化顺序的逆序销毁.

 通常,析构函数释放对象在生存期分配的所有资源.

 在一个析构函数中,不存在类似构造函数中初始化列表的东西来控制成员如何销毁,析构部分是隐式的.成员销毁时发生什么完全依赖于成员的类型.
 销毁类类型的成员需要执行成员自己的析构函数.内置类型没有析构函数,因此销毁内置类型成员什么也不需要做.
 隐式销毁一个内置指针类型的成员不会delete它所指向的对象.
 与普通指针不同,智能指针是类类型,所以具有析构函数.因此,与普通指针不同,智能指针成员在析构阶段会被自动销毁.

 什么时候会调用析构函数
 无论何时一个对象被销毁,就会自动调用其析构函数:
 1:变量在离开其作用域时被销毁.
 2:当一个对象被销毁时,其成员被销毁.
 3:容器(无论是标准库容器还是数组)被销毁时,其元素被销毁.
 4:对于动态分配的对象,当对指向它的指针应用delete运算符时被销毁.
 5:对于临时对象,当创建它的完整表达式结束时被销毁.

 当指向一个对象的引用或指针离开作用域时,析构函数不会执行.

 合成析构函数
 当一个类未定义自己的析构函数时,编译器会为它定义一个合成析构函数.
 合成析构函数不会delete一个指针数据成员.
 类似拷贝构造函数和拷贝赋值运算符,对于某些类,合成析构函数被用来阻止该类型的对象被销毁.如果不是这种情况,合成析构函数的函数体就为空.

 认识到析构函数体自身并不直接销毁成员是非常重要的.成员是在析构函数体之后隐含的析构阶段中被销毁的.在整个对象销毁过程中,析构函数体是
 作为成员销毁步骤之外的另一部分而进行的.

 有三个基本操作可以控制类的拷贝操作:拷贝构造函数、拷贝赋值运算符和析构函数.而且,在新标准下,一个类还可以定义一个移动构造函数和一个
 移动赋值运算符.
 需要析构函数的类也需要拷贝和赋值操作
 当我们决定一个类是否要定义它自己版本的拷贝控制成员时,一个基本原则是首先确定这个类是否需要一个析构函数.通常,对析构函数的需求要比对
 拷贝构造函数或赋值运算符的需求更为明显.如果这个类需要一个析构函数,我们几乎可以肯定它也需要一个拷贝构造函数和一个拷贝赋值运算符.

 如果我们为类定义一个析构函数,但是用合成版本的拷贝构造函数和拷贝赋值运算符,考虑会发生什么:
 这些函数简单拷贝指针成员,这意味着多个该类的对象可能指向相同的内存.

 值得注意的是,我们不能删除析构函数.如果析构函数被删除,就无法销毁此类型的对象了.
 对于一个删除了析构函数的类型,编译器将不允许定义该类型的变量或创建该类的临时对象.而且,如果一个类有某个成员的类型删除了析构函数,我
 们也不能定义该类的变量或临时对象.因为如果一个成员的析构函数时删除的,则该成员无法被销毁.而如果一个成员无法被销毁,则对象整体也就无
 法被销毁了.
 */

 /*
 通常,管理类外资源的类必须定义拷贝控制成员.这种类需要通过析构函数来释放对象所分配的资源.一旦一个类需要析构函数,那么它几乎肯定也需
 要一个拷贝构造函数和一个拷贝赋值运算符.
 为了定义这些成员,我们首先必须确定此类型对象的拷贝语义.一般来说,有两种选择:可以定义拷贝操作,使类的行为看起来像一个值或者像一个指
 针.
 类的行为像一个值,意味着它应该也有自己的状态.当我们拷贝一个像值的对象时,副本和原对象是完全独立的.改变副本不会对原对象有任何影响,
 反之亦然.
 行为像指针的类则共享状态.当我们拷贝一个这种类的对象时,副本和原对象使用相同的底层数据.改变副本也会改变原对象,反之亦然.
 在我们使用过的标准库类中,标准库容器和string类的行为像一个值.std::shared_ptr类提供类似指针的行为.IO类型和std::unique_ptr不允许拷贝或赋值,
 因此它们的行为既不像值也不像指针.unique:[ju'nik]

 为了提供类值的行为,对于类管理的资源,每个对象都应该拥有一份自己的拷贝.
 赋值运算符通常组合了析构函数和构造函数的操作.类似析构函数,赋值操作会销毁左侧运算对象的资源.类似拷贝构造函数,赋值操作会从右侧运
 算对象拷贝数据.但是,非常重要的一点是,这些操作是以正确的顺序执行的,即使将一个对象赋予它自身,也保证正确.而且,如果可能,我们编
 写的赋值运算符还应该是异常安全的---当异常发生时能将左侧运算对象置于一个有意义的状态.

 当你编写赋值运算符时,有两点需要记住:
 1:如果将一个对象赋予它自身,赋值运算符必须能正确工作.
 2:大多数赋值运算符组合了析构函数和拷贝构造函数的工作.

 当你编写一个赋值运算符时,一个好的模式是先将右侧运算对象拷贝到一个局部临时对象中.当拷贝完成后,销毁左侧运算对象的现有成员就是安全
 的了.一旦左侧运算对象的资源被销毁,就只剩下将数据从临时对象拷贝到左侧运算对象的成员中了.(如果先释放了对象所指向的内存空间,那么
 接下来如果是同一个对象在给自己赋值,那么我们就将从已释放的内存中拷贝数据!)所以说,对于一个赋值运算符来说,正确工作是非常重要的,即
 使是将一个对象赋予它自身,也要能正确工作.一个好的方法是在销毁左侧运算对象资源之前拷贝右侧运算对象.
 */

 /*
 可变数据成员(mutable data member):
 有时候,我们希望能修改类的某个数据成员,即使是在一个const成员函数内.那么可以通过在变量的声明中加入mutable关键字来做到这一点.
 一个可变数据成员永远不会是const,即使它是const对象的成员.因此,一个const成员函数可以改变一个可变成员的值.
 */
 /*
 与内置数组一样,标准库array的大小也是类型的一部分.当定义一个array时,除了指定元素类型,还要指定容器大小:
 array<int,42>//类型为:保存42个int的数组.
 array<string,10>//类型为:保存10个string的数组.

 array大小固定的特性也影响了它所定义的构造函数的行为.与其他容器不同,一个默认构造的array是非空的:它包含了与其大小一样多的元素.这些元
 素都被默认初始化,就像一个内置数组中的元素那样.如果我们对array进行列表初始化,初始值的数目必须等于或小于array的大小.如果初始值数目小
 于array的大小,则它们被用来初始化array中靠前的元素,所有剩余元素都会进行值初始化.在这两种情况下,如果元素类型是一个类类型,那么该类必
 须有一个默认构造函数,以使值初始化能够进行.

 值得注意的是:虽然我们不能对内置数组类型进行拷贝或对象赋值操作,但array并无此限制.

 与其他容器一样,array也要求初始值的类型必须与要创建的容器类型相同.此外,array还要求元素类型和大小也都一样,因为大小是array类型的一部
 分.

 std::array<int,3> A,作为类的数据成员的array中的元素都被默认初始化,即都为0.
 */