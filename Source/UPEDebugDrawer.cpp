#include "UPEDebugDrawer.h"

#ifdef UPE_HIERARCHICAL_COMPILE

#include "UPEUtilities.h"

namespace ung
{
	void DebugDrawer::setDebugMode(int debugMode)
	{
		mDebugMode = debugMode;
	}

	int DebugDrawer::getDebugMode() const
	{
		return mDebugMode;
	}

	void DebugDrawer::drawContactPoint(const btVector3 & pointOnB, const btVector3 & normalOnB, btScalar distance, int lifeTime, const btVector3 & color)
	{
		//draws a line between two contact points
		btVector3 const startPoint = pointOnB;
		btVector3 const endPoint = pointOnB + normalOnB * distance;

		drawLine(startPoint, endPoint, color);
	}

	void DebugDrawer::drawLine(const btVector3 & from, const btVector3 & to, const btVector3 & color)
	{
		uutDrawLine(TO_UNG_VECTOR3(from), TO_UNG_VECTOR3(to), TO_UNG_VECTOR3(color));
	}

	void DebugDrawer::reportErrorWarning(const char* warningString)
	{
	}

	void DebugDrawer::draw3dText(const btVector3 &location, const char* textString)
	{
	}

	void DebugDrawer::toggleDebugFlag(int flag)
	{
		//checks if a flag is set and enables/disables it
		if (mDebugMode & flag)
		{
			//flag is enabled, so disable it
			mDebugMode = mDebugMode & (~flag);
		}
		else
		{
			//flag is disabled, so enable it
			mDebugMode |= flag;
		}
	}
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

/*
Introducing activation states:
It may be apparent that the wireframe color of our boxes changes from white to
green a few moments after an object comes to rest. This is yet another optimization
that Bullet handles internally, which is only visualized through the debug mode,
but has a profound effect on CPU usage. Objects whose velocity is below a given
threshold for a given amount of time have their activation state set to deactivated.
Meanwhile, there are actually two dynamic bounding volume trees created when
you use a  btDbvtBroadphase object (as we did). One stores the active objects (the
active tree), and the other stores any static or deactivated objects (the deactive tree).
So, when an object is deactivated, it pushes them into the other tree.
This causes Bullet to skip over them when its time for the world to move objects
around, and since the broad phase object only compares the active tree against itself,
and the active tree against the deactive tree (more importantly, it doesn't compare the
deactive tree against the deactive tree) its impact on processing time is reduced even
further. Later, when an active object collides with the deactivated one, it is activated
once more, pushed back into the active tree, and Bullet performs the necessary
calculations until it decides to deactivate it once more. These activation/deactivation
states are typically referred to as putting the object to sleep, or waking it up.

Note that the ground plane is always drawn with a green wireframe (asleep) because Bullet 
knows that this object has an infinite mass, is static, is never a part of the active tree, and 
thus will never need to be moved.

This optimization has its drawbacks; sometimes an object may be moving slow
intentionally, but if it is moving too slowly, Bullet deactivates it. For example, we
might have an object that is very slowly teetering on an edge, which means it has
a very low angular velocity for a long time, at which point Bullet may assume that
it needs to put the object to sleep. This can cause some bizarre situations, where an
object looks like it should be falling over, but is in fact it is frozen in place at an angle
that would not be possible in the real world.
The typical workaround is to tweak the sleep threshold of the object, the
minimum values of linear and angular velocity, which Bullet considers too low.
This can be achieved by calling  setSleepingThresholds() on any rigid body.
As a last resort, we can force all the objects to remain activated, by calling the
setActivationState(DISABLE_DEACTIVATION) function on every new object,
but this will cost us some performance, since every object will now be a part of
the active tree, and hence will be checked every iteration.
*/