#include "UFCUtilities.h"

#ifdef UFC_HIERARCHICAL_COMPILE

#include <sstream>

#pragma warning(push)

//uint32强制转换为bool
#pragma warning(disable : 4800)

namespace ung
{
	uint8* UNG_STDCALL ungMallocAlignedBuffer(uint32 bytes)
	{
		auto reallyBytes = AllocatorHelper::roundUp(bytes);

		auto pAddress = (uint8*)AllocatorHelper::mallocAligned(reallyBytes);

		BOOST_ASSERT(pAddress);

		return pAddress;
	}

	void UNG_STDCALL ungFreeAlignedBuffer(uint8* pBuffer)
	{
		AllocatorHelper::freeAligned(pBuffer);
	}

	void UNG_STDCALL ungCheckFileFullName(String const& fileFullName)
	{
		//参数不能为空
		BOOST_ASSERT(!fileFullName.empty());

		//得到C字符串
		const char* fullName = fileFullName.data();

		//得到文件系统
		auto& fs = Filesystem::getInstance();

		//文件是否存在
		BOOST_ASSERT(fs.isExists(fullName));

		//是否是一个普通文件
		BOOST_ASSERT(fs.isRegularFile(fullName));
	}

	/*
	直接返回一个String对象的话：
	String UNG_STDCALL ungAddressToString(void* address);
	warning C4190: “ungAddressToString”有指定的 C 链接，但返回了与 C 不兼容的UDT。
	*/
	void UNG_STDCALL ungAddressToString(void* address,String& outString)
	{
		std::ostringstream oss;
		oss << address;

		//把地址转换为了字符串
		outString = oss.str();
	}

	void UNG_STDCALL ungSwapColorRB(uint32& color)
	{
		color = ((color & 0x00FF0000) >> 16) | ((color & 0x000000FF) << 16) | (color & 0xFF00FF00);
	}

	void UNG_STDCALL ungEnumCombinationJoin(uint32 & left, uint32 ev)
	{
		left |= ev;
	}

	void UNG_STDCALL ungEnumCombinationRemove(uint32 & left, uint32 ev)
	{
		left &= ~ev;
	}

	bool UNG_STDCALL ungEnumCombinationHave(uint32 & left, uint32 ev)
	{
		return left & ev;
	}
}//namespace ung

#pragma warning(pop)

#endif//UFC_HIERARCHICAL_COMPILE