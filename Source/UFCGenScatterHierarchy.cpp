/*
说明：
//mt为一个包含5个类型的类型列表，但是类型有重复
using mt = TYPELIST_5(char, short, int, char, int);
//nmt中没有了重复类型
using nmt = ung::utp::NoDuplicates<mt>::result;
//nmt的长度为3
len = ung::utp::Length<nmt>::value;
//通过索引来验证已经去掉了重复的类型
auto t1 = typeid(ung::utp::TypeAt<nmt,0>::result).name();
auto t2 = typeid(ung::utp::TypeAt<nmt,1>::result).name();
auto t3 = typeid(ung::utp::TypeAt<nmt,2>::result).name();

//一个包含3个类型的类型列表
using tl3 = ung::utp::MakeTypeList<char, short, int>::result;
//其长度为3
int len = ung::utp::Length<tl3>::value;
//定义一个Unit
template <class T>
struct Holder
{
	T mValue;
};
//theType类型，分别继承了3个Holder，这3个Holder的类型不同（因为不同的模板参数）
typedef GenScatterHierarchy<tl3,Holder> theType;
//验证theType是Holder<int>类型的子类
bool ret = UFC_SUPERSUBCLASS(Holder<int>, theType);
//定义一个theType类型的对象obj
theType obj;
//获取obj对象中的int部分
auto intC = static_cast<Holder<int>&>(obj);
auto t4 = typeid(theType).name();
auto t5 = typeid(intC).name();
//获取obj对象中的char部分
auto charC = Field<char>(obj);
auto t6 = typeid(charC).name();
//获取obj对象中的short部分
auto shortC = Field<1>(obj);
auto t7 = typeid(shortC).name();
//常量对象
const theType cObj;
auto charC2 = Field<char>(cObj);
auto t8 = typeid(charC2).name();

auto shortC2 = Field<1>(cObj);
auto t9 = typeid(shortC2).name();
*/