#include "UFCPack.h"

#ifdef UFC_HIERARCHICAL_COMPILE

#include "UFCFilesystem.h"
#include "UFCStream.h"
#include "UFCStreamAux.h"
#include "boost/interprocess/mapped_region.hpp"
#include "boost/interprocess/file_mapping.hpp"
#define BOOST_IOSTREAMS_SOURCE
#include "boost/iostreams/filter/zlib.hpp"

namespace
{
	/*!
	 * \remarks 加密
	 * \return 
	 * \param std::string& str
	*/
	void encryption(std::string& str)
	{
		size_t strSize = str.size();

		char* strArray = new char[strSize];
		ung::ufcContainerToMemory(str, strArray, strSize);
		str.clear();

		//遍历字符数组
		unsigned int mid = static_cast<unsigned int>(strSize / 2);
		for (unsigned int i = 0; i < mid; ++i)
		{
			std::swap(strArray[i], strArray[strSize - 1 - i]);
		}

		for (unsigned int i = 0; i < strSize; ++i)
		{
			if (i % 2 == 0)
			{
				//这里必须用无符号char类型
				unsigned char uc = strArray[i];
				unsigned char ml2 = uc << 2;
				unsigned char mr6 = uc >> 6;
				strArray[i] = static_cast<unsigned char>(ml2 + mr6);
			}

			if (i % 3 == 0)
			{
				//这里必须用无符号char类型
				unsigned char uc = strArray[i];
				unsigned char ml3 = uc << 3;
				unsigned char mr5 = uc >> 5;
				strArray[i] = static_cast<unsigned char>(ml3 + mr5);
			}
		}

		//加密后的名字块
		str.assign(strArray, strSize);

		//删除字符数组
		delete[]strArray;
	}
}//namespace

namespace ung
{
	Pack::Pack(String const& packDir,String const& packName) :
		mPackDir(packDir),
		mPackName(packName)
	{
	}

	Pack::~Pack()
	{
	}

	void Pack::packToDisk()
	{
		using namespace boost::iostreams;

		//文件系统
		auto& fsRef = Filesystem::getInstance();

		//处理路径
		fsRef.processDirSeparator(mPackDir);

		//压缩文件全名
		String zipFileName = mPackDir + mPackName;

		//目录名是否存在
		BOOST_ASSERT(fsRef.isExists(mPackDir.data()));
		//是否是一个目录
		BOOST_ASSERT(fsRef.isDirectory(mPackDir.data()));
		//目录不能为空
		BOOST_ASSERT(!fsRef.isEmpty(mPackDir.data()));

		//存储目录下的所有文件全名(递归)
		auto fileFullNamesPtr = fsRef.listFileFullNameRecursive(mPackDir.data());

		//处理文件全名中的路径分隔符
		//统计名字块的字节数
		unsigned int nameBlockBytes{};
		for (auto& fileFullName : *fileFullNamesPtr)
		{
			fsRef.processDirSeparator(fileFullName);

			nameBlockBytes += fileFullName.size();
		}

		//名字块
		String names;
		for (auto const& fileFullName : *fileFullNamesPtr)
		{
			names += fileFullName;
		}

		//文件输出设备(在递归获取给定目录下的所有文件后定义)
		file_descriptor_sink out(zipFileName);

		//把名字块插入到zip文件的头部
		nameBlockToStream(out, names);

		//存储条目
		std::vector<std::tuple<unsigned int, int64, unsigned int, unsigned int, int64>> items;

		//存储每段的大小(压缩后的段大小)
		std::vector<unsigned int> sections;

		//把真正的压缩数据插入到zip文件中
		compressedBlockToStream(out, fileFullNamesPtr, items,sections);

		//把所有段的大小插入到zip文件中
		unsigned int sectionCount{};				//总段数
		for (auto const& item : items)
		{
			sectionCount += std::get<3>(item);
		}
		BOOST_ASSERT(sections.size() == sectionCount);
		sectionsBlockToStream(out,sections);

		//把所有的条目插入到zip文件中
		itemsBlockToStream(out, items);

		//把名字块的字节数插入到zip文件中
		ui32ToStream(out, nameBlockBytes);

		//把总段数插入到zip文件中
		ui32ToStream(out, static_cast<unsigned int>(sections.size()));

		//把文件的数量值插入到zip文件的尾部
		ui32ToStream(out, static_cast<unsigned int>((*fileFullNamesPtr).size()));

		//关闭输出流
		close_one_now(out);
	}

	void Pack::nameBlockToStream(boost::iostreams::file_descriptor_sink& snk,String& names)
	{
		using namespace boost;
		using namespace boost::iostreams;

		//加密
		encryption(names);

		//把名字块插入到zip文件的头部
		copy_not_close(make_iterator_range(names), snk);
	}

	void Pack::compressedBlockToStream(boost::iostreams::file_descriptor_sink& snk,
		std::shared_ptr<STL_VECTOR(String)> fileFullNamesPtr,
		std::vector<std::tuple<unsigned int,int64,unsigned int,unsigned int,int64>>& items,
		std::vector<unsigned int>& sections)
	{
		using namespace boost;
		using namespace boost::iostreams;
		using namespace boost::interprocess;

		/*
		映射文件时的偏移类型。mapped_region构造函数中，其offset为__int64，是带符号类型。
		*/
		using type_offset = int64;

		//文件系统
		auto& fsRef = Filesystem::getInstance();

		//往容器中压缩的输出过滤流
		filtering_ostream outCon;
		/*
		这里使用vector，如果单个文件过大，会存在vector在内存中无法分配的情况，因为vector要求的是连续内存空间，不过在下面把
		步长设置为1M应该是没有问题的。
		*/
		std::vector<char> compressedCon;

		//跟踪当前压缩数据块中的偏移
		int64 offsetInCompressedDataBlock{};

		//遍历每个文件
		for (auto const& fileFullName : *fileFullNamesPtr)
		{
			//具体文件名字的字符数
			unsigned int fileFullNameLength = fileFullName.size();

			//原始文件大小
			int64 fileOriginalBytes = fsRef.getFileSize(fileFullName.data());
			int64 fileTotalBytes = fileOriginalBytes;

			//具体文件总共被压缩成了多少段数
			unsigned int fileSectionNum{};

			//压缩后的字节数
			int64 filecompressedBytes{};

			/*
			对文件进行分段映射，每段最长为100M
			这里每段映射所设置的长度和解压时对压缩数据进行分段映射所设置的长度没有关系，无需让二者相等。
			*/
			size_t mapStep = 100 * MEGABYTES;

			/*
			对压缩文件进行映射(map操作几乎不占有内存)

			boost文件映射，如果文件过大会抛出异常。
			实测：1000M没有问题。
			解决方案：
			pack时，如果单个文件大于1000M，则必须分段来映射，这样就没有问题。

			static std::size_t get_page_size();
			Returns the size of the page. This size is the minimum memory that will be used by the system when mapping a
			memory mappable source.

			pageSize为64k，如果通过智能指针来持有map后的结果，那么当持有多个的时候，会挂掉。
			实测pageSize累计最大只能为1M。而每一次映射，对pageSize的累计，跟所映射的文件大小(offset,size)有关系。

			file_mapping:
			A class that wraps a file-mapping that can be used to create mapped regions from the mapped files.

			mapped_region region{ file_mapping(fileFullName, read_only),read_only,
																									1,				//offset
																									1				//size
																									};
			*/

			type_offset mapOffset = 0;

			while (fileTotalBytes > 0)
			{
				//每段映射的实际长度
				size_t realMapLength{};

				if (fileTotalBytes > mapStep)
				{
					realMapLength = mapStep;
				}
				else
				{
					realMapLength = fileTotalBytes;
				}

				//更新剩余要映射的字节数(注意：这个值最小只能为0)
				fileTotalBytes -= realMapLength;
				BOOST_ASSERT(fileTotalBytes >= 0);

				//映射这一段
				mapped_region region{ file_mapping(fileFullName.data(), read_only),read_only,mapOffset,realMapLength };
				char* source = reinterpret_cast<char*>(region.get_address());
				//更新map offset
				mapOffset += realMapLength;

				/*
				这里之所以要对流拷贝进行分割，是因为boost的流copy，如果数据过大的话，copy操作会陷入死循环或者抛出异常。
				流拷贝的步长:1M
				说明：这里使用几M的步长，和解压没有关系。
				另，并不一定步长越大压缩速度越快
				*/
				size_t streamCopyStep = 1 * MEGABYTES;

				//第一段步长的头
				char* sectionBeg = source;

				while (realMapLength > 0)
				{
					//每段步长实际的长度
					size_t realLength{};
					if (realMapLength > streamCopyStep)
					{
						realLength = streamCopyStep;
					}
					else
					{
						realLength = realMapLength;
					}

					//构建源
					stream<array_source> in(sectionBeg, realLength);

					//构建在容器中压缩的输出过滤流
					outCon.push(zlib_compressor());
					outCon.push(boost::iostreams::back_inserter(compressedCon));
					copy(in, outCon);

					//压缩后文件大小
					unsigned int sectionCompressedSize = compressedCon.size() * sizeof(char);
					sections.push_back(sectionCompressedSize);

					//累加该文件压缩后的字节数
					filecompressedBytes += sectionCompressedSize;
					//累加压缩数据块中的偏移
					offsetInCompressedDataBlock += sectionCompressedSize;

					//把经过压缩的这段数据插入到zip文件中
					copy_not_close(make_iterator_range(compressedCon), snk);

					//更新段数
					++fileSectionNum;

					//清空容器中的压缩数据
					compressedCon.clear();

					//重置输出到容器中的过滤流
					outCon.reset();

					//下一个步长的开始位置
					sectionBeg += realLength;

					//从总大小中减去步长
					realMapLength -= realLength;
					BOOST_ASSERT(realMapLength >= 0);
				}//end 遍历“分片压缩”
			}//end 遍历每段map

			//文件在块中的第一个索引
			unsigned int fileFirstIndexInSectionBlock{sections.size() - fileSectionNum};

			//文件在压缩数据中的偏移
			int64 fileOffsetInCompressedDataBlock = offsetInCompressedDataBlock - filecompressedBytes;

			//保存条目
			items.push_back(std::make_tuple(fileFullNameLength, fileOriginalBytes,fileFirstIndexInSectionBlock,
				fileSectionNum,fileOffsetInCompressedDataBlock));
		}//end 遍历文件
	}

	void Pack::sectionsBlockToStream(boost::iostreams::file_descriptor_sink& snk,
		std::vector<unsigned int> const& sections)
	{
		//整个zip文件中的总段数
		unsigned int totalSections = sections.size();

		for (unsigned int i = 0; i < totalSections; ++i)
		{
			ui32ToStream(snk, sections[i]);
		}
	}

	void Pack::itemsBlockToStream(boost::iostreams::file_descriptor_sink& snk,
		std::vector<std::tuple<unsigned int, int64, unsigned int,unsigned int,int64>> const& items)
	{
		//把所有的条目插入到zip文件中
		for (auto const& item : items)
		{
			//文件全名的字符数
			ui32ToStream(snk, std::get<0>(item));
			//文件原始大小
			int64ToStream(snk, std::get<1>(item));
			//在section块中的第一个索引
			ui32ToStream(snk, std::get<2>(item));
			//文件的分段数
			ui32ToStream(snk, std::get<3>(item));
			//文件在压缩数据块中的偏移
			int64ToStream(snk, std::get<4>(item));
		}
	}

	void Pack::ui32ToStream(boost::iostreams::file_descriptor_sink& snk, unsigned int ui32)
	{
		using namespace boost::iostreams;

		//用于存放数据ui32的char数组
		char ar[sizeof(unsigned int)];

		//数组长度
		unsigned int len = sizeof(unsigned int);

		/*
		数值ui32的地址(指向其第一个字节地址)
		这里ui32值无需一直存留于内存中，只需要知道对应的具体值解析为比如4个字节后，每个字节的内容值就行
		了，这个“每字节具体内容值”，无论t的内存在哪里都是相同的，比如8这个int值，无论何时，将其解析为
		4个字节，每个字节里面的“char值”都是一样的。
		*/
		char* start = reinterpret_cast<char*>(&ui32);

		for (unsigned int i = 0; i < len; ++i)
		{
			//按字节，把其内容放入到每一个char数组的元素中
			ar[i] = *(start + i);
		}

		//char数组源设备
		basic_array_source<char> srcDev(ar, len);
		//输入流
		stream<basic_array_source<char>> in(srcDev);

		//拷贝，但是不关闭目的设备
		copy_not_close(in, snk);

		//关闭输入流
		close(in);
	}

	void Pack::int64ToStream(boost::iostreams::file_descriptor_sink& snk, int64 i64)
	{
		using namespace boost::iostreams;

		char ar[sizeof(int64)];

		unsigned int len = sizeof(int64);

		char* start = reinterpret_cast<char*>(&i64);

		for (unsigned int i = 0; i < len; ++i)
		{
			ar[i] = *(start + i);
		}

		basic_array_source<char> srcDev(ar, len);
		stream<basic_array_source<char>> in(srcDev);

		copy_not_close(in, snk);

		close(in);
	}
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

/*
Zip压缩文件内部结构:
-----------------------------
|	名字块						|
|									|
-----------------------------
|	压缩数据						|
|									|
|									|
|									|
-----------------------------
|	每段的大小					|
|									|
|									|
|									|
-----------------------------
|	条目							|
|									|
|									|
|									|
-----------------------------
|名字块的字节数,总段数,数量	|
-----------------------------
*/