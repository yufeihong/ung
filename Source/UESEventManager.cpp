#include "UESEventManager.h"

#ifdef UES_HIERARCHICAL_COMPILE

namespace ung
{
	EventManager::EventManager() :
		mActiveQueue(0)
	{
		UNG_LOG("Create event manager.");
	}

	EventManager::~EventManager()
	{
		mMap.clear();
	}

	void EventManager::addListener(delegate_type const& delegateFunctor, EventType const& et)
	{
		//Grabs or creates the event listener list.
		listener_list& listenerList = mMap[et];

		for (auto it = listenerList.begin(); it != listenerList.end(); ++it)
		{
			if (delegateFunctor == (*it))
			{
				//抛出异常
				BOOST_ASSERT(0 && "Attempting to double-register a delegate.");
			}
		}

		listenerList.push_back(delegateFunctor);
	}

	void EventManager::removeListener(delegate_type const& delegateFunctor, EventType const& et)
	{
		auto findIt = mMap.find(et);
		if (findIt != mMap.end())
		{
			listener_list& listeners = findIt->second;
			for (auto it = listeners.begin(); it != listeners.end(); ++it)
			{
				/*
				The FastDelegate classes all implement an overloaded == operator, so this works really well.
				*/
				if (delegateFunctor == (*it))
				{
					listeners.erase(it);
					break;
				}
			}
		}
	}

	void EventManager::triggerEvent(event_pointer const& evt) const
	{
		auto findIt = mMap.find(evt->getEventType());
		if (findIt != mMap.end())
		{
			const listener_list& eventListenerList = findIt->second;
			for (listener_list::const_iterator it = eventListenerList.begin(); it != eventListenerList.end(); ++it)
			{
				delegate_type listener = (*it);

				//调用委托函数
				listener(evt);
			}
		}
	}

	void EventManager::queueEven(event_pointer const& evt)
	{
		BOOST_ASSERT(evt);

		auto findIt = mMap.find(evt->getEventType());
		if (findIt != mMap.end())
		{
			mQueue[mActiveQueue].push_back(evt);
		}
	}

	void EventManager::update()
	{
		/*
		There are actually two queues. This is almost like double buffering in a renderer. Sometimes handling events creates 
		new events; in fact,it happens all the time. Colliding with an object might cause it to move and collide with another 
		object. If you always added events to a single queue, you might never run out of events to process.This problem is 
		handled easily with two queues: one for the events being actively processed and the other for new events.
		*/

		//Swap active queues and clear the new queue after the swap.
		int queueToProcess = mActiveQueue;
		mActiveQueue = (mActiveQueue + 1) % 2;
		mQueue[mActiveQueue].clear();

		//Process the queue
		while (!mQueue[queueToProcess].empty())
		{
			//pop the front of the queue
			event_pointer pEvent = mQueue[queueToProcess].front();
			mQueue[queueToProcess].pop_front();

			const EventType& eventType = pEvent->getEventType();

			//find all the delegate functions registered for this event
			auto findIt = mMap.find(eventType);
			if (findIt != mMap.end())
			{
				const listener_list& eventListeners = findIt->second;

				//call each listener
				for (auto it = eventListeners.begin(); it != eventListeners.end(); ++it)
				{
					delegate_type listener = (*it);

					listener(pEvent);
				}
			}
		}
	}
}//namespace ung

#endif//UES_HIERARCHICAL_COMPILE

/*
用法：
When you destroy an actor:
The first step is to define the event data by writing a new class that inherits from EventData.like:
//Sent when actors are destroyed
class EventDestroyActor : public EventData
{
public:
	static const EventType myEventType;

	explicit EventDestroyActor(ActorId id = INVALID_ACTOR_ID) :
		m_id(id)
	{
	}

	virtual const EventType& getEventType() const
	{
		return myEventType;
	}

	ActorId GetId(void) const
	{
		return m_id;
	}

private:
	ActorId m_id;
};

The next step is to define the delegate methods that need to handle this event.
Here’s what it might look like:
void RoleSystem::DestroyActorDelegate(IEventDataPtr pEventData)
{
	//Cast the base event pointer to the actual event data we need
	shared_ptr<EventDestroyActor> pCastEventData = std::static_pointer_cast<EventDestroyActor>(pEventData);

	//Remove the actor from the map of roles.Assume the role map is defined as follows:std::map<ActorId, RoleData> m_roleMap;
	m_roleMap.erase(pCastEventData->GetActorId());
}

Somewhere in the initialization of the role system,you also need to register the delegate:
bool RoleSystem::init(void)
{
	//Create the delegate function object
	delegate_type delegateFunc = MakeDelegate(this,&RoleSystem::DestroyActorDelegate);

	//Register the delegate with the event manager
	EventManager::getInstance()->addListener(delegateFunc,EventDestroyActor::myEventType);
}

You also need to remember to remove it in the destructor:
RoleSystem::~RoleSystem(void)
{
	//Create a delegate function object.This will have the same value as the one previously registered in VInit().
	//Another way to do this would be to cache the delegate object. This is a memory vs performance trade-off.Since the 
	//performance gain would only be during shut-down, memory is the better way to go here.
	delegate_type delegateFunc = MakeDelegate(this, &RoleSystem::DestroyActorDelegate);

	//Remove the delegate from the event manager
	EventManager::getInstance()->removeListener(delegateFunc,EventDestroyActor::myEventType);
}
That’s all you need to do in order to register events.Here’s the code to send the event:
//Instantiate the event.
shared_ptr<EventDestroyActor> pDestroyEvent(new EventDestroyActor(pActor->GetId());
//Queue the event.
EventManager::getInstance()->queueEven(pDestroyEvent);
//Or, if you prefer, force the event to resolve immediately with triggerEvent()
EventManager::getInstance()->triggerEvent(pDestroyEvent);

And there you have it, a simple event system.
*/

/*
Game Event Management:
Instead of calling each system every time an actor is destroyed, the game logic could create a 
game event and send it into a system that knows how to distribute the event to any subsystem 
that wants to listen. One side effect of this solution is that it cleans up the relationship 
between game subsystems. Mostly, they don’t care about anything but themselves and the 
event management system.

A system such as an audio system already knows what events it should listen to. In this case,
it would listen to “object collided” or “object destroyed.” On the other hand, there might be 
tons of other messages that the audio system could safely ignore, such as an event that 
signals the end of an animation.

In a well-designed game, each subsystem should be responsible for subscribing(订阅) to and 
handling game events as they pass through the system. The game event system is global to 
the application and therefore makes a good candidate to sit in the application layer. It 
manages all communications going on between the game logic and game views. If the game 
logic moves or destroys an actor, an event is sent, and all the game views will receive it. If a 
game view wants to send a command to the game logic, it does so through the event system.
The game event system is the glue that holds the entire game logic and game view 
architecture together.

The game event system is organized into three basic parts:
1,Events and event data.
2,Event handler delegates.
3,Event Manager.
Events and event data are generated by authoritative systems when an action of any 
significance occurs, and they are sent into the Event Manager, sometimes also called a 
listener registry. The Event Manager matches each event with all the subsystems that have 
subscribed to the event and calls each event listener delegate function in turn so it can handle 
the event in its own way.

Events and Event Data:
A classic problem in computer games is how to define types of data or objects.The easiest 
way to define different types of elements, such as event types, is to put them all into a single 
enumeration like this:
Enum EventType
{
	Event_Object_Moved,
	Event_Object_Created,
	Event_Object Destroyed,
	Event_Guard_Picked_Nose,
	//and on and on….
};
With this type of solution, each subsystem in your game would likely need this enumeration 
because each probably generates one or more of these events.In coding this approach,you 
would need to have every system #include this enumeration.Then,every time you add to it or 
change it, your entire game would need to be recompiled,clearly a bad thing.

An event encapsulates the event type, the event data, and the time the event occurred.

The implementation of EventManager manages two sets of objects:event data and listener 
delegates. As events are processed by the system, the Event Manager matches them up with 
subscribed listener delegate functions and calls each one with events they care about.

There are two ways to send events:by queue and by trigger.
By queue means the event will sit in line with other events until the game processes 
EventManager::update().
By trigger means the event will be sent immediately—almost like calling each delegate 
function directly from your calling code.

There are two event queues here so that delegate methods can safely queue up new events.
You can imagine an infinite loop where two events queue up each other.
*/

/*
Event和Process的区别：
A game event is something that has happened in the most recent frame, such as an actor has 
been destroyed or moved.
A process is something that takes more than one frame to process, such as an animation or 
monitoring a sound effect.

Events are the main tool used for communicating to other systems.Using the event system 
presented in this chapter, you can design complex systems that are nice and decoupled from 
each other while still allowing them to talk to one another.This decoupling allows these 
systems to grow and change organically(有机地) without affecting any of the other systems 
they are attached to, as long as they still send and respond to the same events as before.
*/

/*
Shows an example of the kind of game events you might send in just about any game:

Game Events										Description
ActorMove											A game object has moved.
ActorCollision										A collision has occurred.
AICharacterState									Character has changed states.
PlayerState											Player has changed states.
PlayerDeath										Player is dead.
GameOver											Player death animation is over.
ActorCreated										A new game object is created.
ActorDestroy										A game object is destroyed.

PreLoadLevel										A new level is about to be loaded.
LoadedLevel										A new level is finished loading.
EnterTriggerVolume								A character entered a trigger volume.
ExitTriggerVolume								A character exited a trigger volume.
PlayerTeleported									The player has been teleported.

GraphicsStarted									The graphics system is ready.
PhysicsStarted										The physics system is ready.
EventSystemStarted								The event system is ready.
SoundSystemStarted							The sound system is ready.
ResourceCacheStarted							The resource system is ready.
NetworkStarted									The network system is ready.
HumanViewAttached							A human view has been attached.
GameLogicStarted								The game logic system is ready.
GamePaused										The game is paused.
GameResumedResumed						The game is resumed.
PreSave												The game is about to be saved.
PostSave											The game has been saved.

AnimationStarted									An animation has begun.
AnimationLooped									An animation has looped.
AnimationEnded									An animation has ended.
SoundEffectStarted								A new sound effect has started.
SoundEffectLooped								A sound effect has looped back to the beginning.
SoundEffectEnded								A sound effect has completed.
VideoStarted										A cinematic has started.
VideoEnded										A cinematic has ended.
*/

/*
The fast delegate library uses raw pointers on the inside and binds the object pointer you give it to the function pointer.
That means that if you destroy the object without removing the delegate, you’ll get a crash when an event that delegate is 
set to receive is fired. You should always remember to clean up after yourself and remove any delegate listeners in the 
destructor of your listener class.
*/