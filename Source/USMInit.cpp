#include "USMInit.h"

#ifdef USM_HIERARCHICAL_COMPILE

#include "USMSpatialManagerEnumerator.h"

namespace ung
{
	void UNG_STDCALL usmInit()
	{
		UNG_LOG_START("Initialize USM.");

		UNG_LOG_START("Load UOT's dll.");

#if UNG_DEBUGMODE
		PluginManager::getInstance().load("UOT_d.dll");
#else
		PluginManager::getInstance().load("UOT.dll");
#endif

		UNG_LOG_END("Load UOCT's dll.");

		//------------------------------------------------------

		UNG_LOG_START("Load UD9's dll.");

#if UNG_DEBUGMODE
		PluginManager::getInstance().load("UD9_d.dll");
#else
		PluginManager::getInstance().load("UD9.dll");
#endif

		UNG_LOG_END("Load UD9's dll.");

		//------------------------------------------------------

		UNG_LOG_START("Load UG3's dll.");

#if UNG_DEBUGMODE
		PluginManager::getInstance().load("UG3_d.dll");
#else
		PluginManager::getInstance().load("UG3.dll");
#endif

		UNG_LOG_END("Load UG3's dll.");

		//------------------------------------------------------

		UNG_LOG_END("Initialize USM.");
	}
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE