#include "URMVertexDeclaration.h"

#ifdef URM_HIERARCHICAL_COMPILE

namespace ung
{
	VertexDeclaration::VertexDeclaration() :
		mAddVertexElementFlag(false)
	{
	}

	VertexDeclaration::~VertexDeclaration()
	{
		//这里无需对mElementList调用clear()函数
	}

	void VertexDeclaration::addElement(VertexElement* ve)
	{
		BOOST_ASSERT(!mAddVertexElementFlag);

		mElementList.push_back(ve);
	}

	void VertexDeclaration::addElement(uint8 stream, uint8 offset, VertexElementSemantic semantic, VertexElementType theType, uint8 index)
	{
		BOOST_ASSERT(!mAddVertexElementFlag);

		mElementList.push_back(UNG_NEW VertexElement(stream, offset, semantic, theType, index));
	}

	void VertexDeclaration::removeElement(VertexElementSemantic semantic, uint8 index)
	{
		auto its = mElementList.getIterators();

		while (its.first != its.second)
		{
			//指针容器在内部已经做过一次节引用操作了
			if (its.first->getSemantic() == semantic && its.first->getUsageIndex() == index)
			{
				mElementList.erase(its.first);
				break;
			}

			++its.first;
		}
	}

	void VertexDeclaration::removeAllElements()
	{
		mElementList.clear();
	}

	uint32 VertexDeclaration::getElementCount() const
	{
		BOOST_ASSERT(mAddVertexElementFlag);

		return mElementList.size();
	}

	void VertexDeclaration::modifyElement(uint8 elem_index, uint8 source, uint8 offset, VertexElementSemantic semantic, VertexElementType theType, uint8 index)
	{
		BOOST_ASSERT(elem_index < getElementCount() && "Index out of bounds");

		auto it = mElementList.begin();

		/*
		Increments an iterator by a specified number of positions.
		template<class InputIterator, class Distance>
		void advance(InputIterator& InIt,Distance Off);
		The range advanced through must be nonsingular,where the iterators must be 
		dereferenceable or past the end.
		If the InputIterator satisfies the requirements for a bidirectional iterator type, then Off 
		may be negative. If InputIterator is an input or forward iterator type, Off must be 
		nonnegative.
		The advance function has constant complexity when InputIterator satisfies the 
		requirements for a random-access iterator; otherwise, it has linear complexity and so is 
		potentially expensive.
		InIt:
		The iterator that is to be incremented and that must satisfy the requirements for an input 
		iterator.
		Off:
		An integral type that is convertible to the iterator's difference type and that specifies the 
		number of increments the position of the iterator is to be advanced.
		*/
		std::advance(it, elem_index);

		/*
		replace():替换迭代器所指位置的指针，然后以auto_type返回当前位置的原指针。

		这里的oldPointer会自动释放。
		即便没有oldPointer变量去持有replace()函数的返回值，也不会造成内存泄露，还是会自动释放。
		*/
		VertexElementList::auto_type oldPointer = mElementList.replace(it,UNG_NEW VertexElement(source, offset, semantic,theType, index));
	}
	
	VertexDeclaration::borrow_type VertexDeclaration::findElementBySemantic(VertexElementSemantic sem, uint8 index)
	{
		auto its = mElementList.getIterators();

		auto it = its.second;

		while (its.first != its.second)
		{
			if (its.first->getSemantic() == sem && its.first->getUsageIndex() == index)
			{
				it = its.first;
			}

			++its.first;
		}

		return mElementList.borrow(it);
	}

	void VertexDeclaration::sortElements()
	{
		/*
		注意参数的写法，用于Boost库指针容器时。
		*/
		auto lambdaExpress = [](VertexElement const& e1, VertexElement const& e2)
		{
			auto leftStream = e1.getStream();
			auto leftSemantic = e1.getSemantic();

			auto rightStream = e2.getStream();
			auto rightSemantic = e2.getSemantic();

			//Sort by stream first
			if (leftStream < rightStream)
			{
				return true;
			}
			else if (leftStream == rightStream)
			{
				//Use ordering of semantics to sort
				if (leftSemantic < rightSemantic)
				{
					return true;
				}
				else if (leftSemantic == rightSemantic)
				{
					//Use index to sort
					if (e1.getUsageIndex() < e2.getUsageIndex())
					{
						return true;
					}
				}
			}

			return false;
		};

		mElementList.sort(lambdaExpress);
	}

	uint32 VertexDeclaration::getVertexSize(uint8 stream)
	{
		uint32 sz = 0;

		auto its = mElementList.getIterators();

		while (its.first != its.second)
		{
			//找到对应的stream(vertex buffer index)
			if (its.first->getStream() == stream)
			{
				sz += its.first->getSize();
			}

			++its.first;
		}

		return sz;
	}

	bool VertexDeclaration::operator==(const VertexDeclaration& rightInst) const
	{
		if (getElementCount() != rightInst.getElementCount())
		{
			return false;
		}

		auto its = mElementList.getIterators();
		auto rightIts = rightInst.mElementList.getIterators();

		while (its.first != its.second && rightIts.first != rightIts.second)
		{
			if ((*its.first) != (*rightIts.first))
			{
				return false;
			}

			++its.first;
			++rightIts.first;
		}

		return true;
	}

	bool VertexDeclaration::operator!=(const VertexDeclaration& rightInst) const
	{
		return !(*this == rightInst);
	}
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE