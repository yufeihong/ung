#include "USMSpatialNodeBase.h"

#ifdef USM_HIERARCHICAL_COMPILE

namespace ung
{
	ISpatialNode * SpatialNodeBase::getParent() const
	{
		return nullptr;
	}

	int32 SpatialNodeBase::getDepth() const
	{
		return int32();
	}

	void SpatialNodeBase::attachObject(StrongIObjectPtr objectPtr)
	{
	}

	void SpatialNodeBase::detachObject(StrongIObjectPtr objectPtr)
	{
	}

	ISpatialNode * SpatialNodeBase::createChild()
	{
		return nullptr;
	}

	void SpatialNodeBase::createChildren()
	{
	}
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE