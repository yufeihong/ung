/*
我们可以通过set的迭代器改变set的元素值吗？
不行，因为set元素值就是其键值，关系到set元素的排列规则。如果任意改变set元素值，会严重破坏set组织。set iterators是一种constant iterators(相对于mutable iterators)。

set拥有与list相同的某些性质：当客户端对它进行元素新增操作（insert）或删除操作（erase）时，操作之前的所有迭代器，在操作完成之后都依然有效。当然，被删除的那个元素
的迭代器必然是个例外。
*/

/*
identity:
identity是一个最简单的键提取器，它不做任何“提取”动作，直接使用元素本身作为键，相当于标准容器的键类型（key type）。只要元素类型不是const，那么它就是可写的键提取器。
identity位于头文件"boost/multi_index/identity.hpp"，其类摘要如下：
template<typename Type>
struct identity : mpl::if_c<is_const<Type>::value,detail::const_identity_base<Type>,detail::non_const_identity_base<Type>>::type
{};
identity使用了元函数if_c<>，根据类型Type是否被const修饰分别转交给const_identity_base和non_const_identity_base处理，这两个实现类的具体代码差异很小，下面列出
const_identity_base的主要代码：
template<typename Type>
struct const_identity_base
{
	typedef Type result_type;

	//操作元素类型本身
	Type& operator()(Type& x) const
	{
		return x;
	}

	//操作元素类型的reference_wrapper包装
	Type& operator()(const reference_wrapper<Type>& x) const
	{
		return x.get();
	}

	template<typename ChainedPtr>
	typename disable_if<is_convertible<const ChainedPtr&, Type&>, Type&>::type operator()(const ChainedPtr& x) const
	{
		return operator()(*x);
	}
};
const_identity_base的前两个operator()，它们直接返回变量自身。
最后一个重载形式用于处理链式指针，它使用了元函数disable_if<>，当模板参数ChainedPtr是指针类型时编译器递归生成解引用的operator()，这样在运行时就可以连续调用直至
获得最终的Type&类型。

离开多索引容器的范围，identity就是一个普通的函数对象，像是对类型做了一层薄薄的包装，例如：
assert((is_same<string, identity<string>::result_type>::value));
assert(identity<int>()(10) == 10);
assert(identity<string>()("abc") == "abc");

int* p = new int(100);														//指针
int** pp = &p;																//指针的指针（链式指针）
assert(identity<int>()(pp) == 100);									//从链式指针中获得键
*/