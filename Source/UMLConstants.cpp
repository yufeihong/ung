#include "UMLConsts.h"

#ifdef UML_HIERARCHICAL_COMPILE

//#include "boost/math/constants/constants.hpp"

////double->float
//#pragma warning(disable:4305)

namespace ung
{
//#define CONSTANTS_STATICMEMBER_DEFINE template<typename T> const T Consts<T>
//
//	CONSTANTS_STATICMEMBER_DEFINE::SIXTHPI = boost::math::constants::sixth_pi<type>();
//	CONSTANTS_STATICMEMBER_DEFINE::THIRDPI = boost::math::constants::third_pi<type>();
//	CONSTANTS_STATICMEMBER_DEFINE::HALFPI = boost::math::constants::half_pi<type>();
//	CONSTANTS_STATICMEMBER_DEFINE::TWOTHIRDSPI = boost::math::constants::two_thirds_pi<type>();
//	CONSTANTS_STATICMEMBER_DEFINE::THREEQUARTERSPI = boost::math::constants::three_quarters_pi<type>();
//	CONSTANTS_STATICMEMBER_DEFINE::PI = boost::math::constants::pi<type>();
//	CONSTANTS_STATICMEMBER_DEFINE::TWOPI = boost::math::constants::two_pi<type>();
//
//	CONSTANTS_STATICMEMBER_DEFINE::THIRD = boost::math::constants::third<type>();
//	CONSTANTS_STATICMEMBER_DEFINE::TWOTHIRDS = boost::math::constants::two_thirds<type>();
//	CONSTANTS_STATICMEMBER_DEFINE::THREEQUARTERS = boost::math::constants::three_quarters<type>();
//	CONSTANTS_STATICMEMBER_DEFINE::ROOTTWO = boost::math::constants::root_two<type>();
//	CONSTANTS_STATICMEMBER_DEFINE::ROOTTHREE = boost::math::constants::root_three<type>();
//	CONSTANTS_STATICMEMBER_DEFINE::RADIAN = boost::math::constants::radian<type>();
//	CONSTANTS_STATICMEMBER_DEFINE::DEGREE = boost::math::constants::degree<type>();
//
//	CONSTANTS_STATICMEMBER_DEFINE::EPSILON = std::numeric_limits<type>::epsilon();
//	/*
//	e:表示10
//	a * 10 ^ b,表示为a * eb
//	0.000001
//	*/
//	CONSTANTS_STATICMEMBER_DEFINE::ZERO = 1e-006;
}//namespace ung

namespace
{
	//warning LNK4221: 此对象文件未定义任何之前未定义的公共符号，因此任何耗用此库的链接操作都不会使用此文件。
	int UML_DUMMY_LNK_4221 = 0;
}//namespace

#endif//UML_HIERARCHICAL_COMPILE