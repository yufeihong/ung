#include "UPEUtilities.h"

#ifdef UPE_HIERARCHICAL_COMPILE

#include "UPETriangle.h"
#include "UPEPlane.h"
#include "UPESphere.h"
#include "UPEAABB.h"
#include "UPECylinder.h"
#include "UPERay.h"
#include "UPEPlaneBoundedVolume.h"

namespace ung
{
	Vector3 convert(btVector3 const & v3)
	{
		Vector3 tempV3(v3.getX(),v3.getY(),v3.getZ());

		return std::move(tempV3);
	}

	int32 UNG_STDCALL ungPointSideOfLine(Vector2 const & point, Vector2 const & line)
	{
		//列为主
		Matrix3 Orient2D{0.0,0.0,1.0,
								line.x,line.y,1.0,
								point.x,point.y,1.0 };

		auto det = Orient2D.determinant();

		return det;
	}

	int32 UNG_STDCALL ungPointSideOfCircle(Vector2 const & point, Vector2 const & c1, Vector2 const & c2, Vector2 const & c3)
	{
		//列为主
		Matrix4 Circle2D = { c1.x,c1.y,c1.x * c1.x + c1.y * c1.y,1.0,
						c2.x,c2.y,c2.x * c2.x + c2.y * c2.y,1.0,
						c3.x,c3.y,c3.x * c3.x + c3.y * c3.y,1.0,
						point.x,point.y,point.x * point.x + point.y * point.y,1.0 };

		auto det = Circle2D.determinant();

		return det;
	}

	real_type UNG_STDCALL ungPointToStraightLineDistance(Vector3 const & start, Vector3 const & end, Vector3 const & point)
	{
		Vector3 v{ end - start };

		return v.crossProduct(point - start).length();
	}

	real_type UNG_STDCALL ungPointToLineSegmentSquaredDistance(Vector3 const & start, Vector3 const & end, Vector3 const & point)
	{
		/*
		The squared distance between a point C and a segment AB can be directly computed
		without explicitly computing the point D on AB closest to C.
		A,B为线段的两端点，C为空间中的一点
		When AC · AB ≤ 0, A is closest to C and the squared distance is given by AC · AC.
		When AC · AB ≥ AB · AB, B is closest to C and the squared distance is BC · BC.
		In the remaining case, 0 < AC · AB < AB · AB,the squared distance is given by CD · CD,
		where
		D = A + (AC · AB / AB · AB) * AB
		勾股定理：
		D = AC · AC - (AC · AB) ^ 2 / AB · AB ,
		*/
		
		Vector3 ab = end - start, ac = point - start, bc = point - end;

		float e = ac.dotProduct(ab);
		//Handle cases where c projects outside ab
		if (e <= 0.0)
		{
			return ac.dotProduct(ac);
		}

		float f = ab.dotProduct(ab);
		if (e >= f)
		{
			return bc.dotProduct(bc);
		}

		//Handle cases where c projects onto ab
		return ac.dotProduct(ac) - e * e / f;
	}

	/*
	任意点到平面的有符号距离.
	设想一个平面和一个不在平面上的点q.
	平面上存在一个点p,它到q的距离最短.很明显,从p到q的向量垂直于平面,且形式为an.
	设n为单位向量,那么p到q的距离(也就是q到平面的距离)就是a了.(如果q在平面的反面,这个距离为负.)令人
	惊奇的是,不用知道p的位置就能计算出a.
	p + an = q
	(p + an).n = q.n
	p.n + (an).n = q.n
	d + a = q.n
	a = q.n - d
	*/
	/*
	n · p = d
	其中d = n · p0			//p0位平面上的一点
	上式可以用来考察某点相对于平面的位置关系。例如，给定平面(n,d)，我们可求出一个特定点p与该平面的关系。
	若t = 0，则点p位于平面上。
	若t > 0，则点p位于平面的前方。
	若t < 0，则点p位于平面的后方。
	如果平面的法向量n的模为1，则n · p - d就等于该平面到点p的最短有符号距离。
	*/
	real_type UNG_STDCALL ungSignedDistanceFromPointToPlane(Plane const & plane, Vector3 const & point)
	{
		/*
		空间中的一点到平面的正交投影点的推导过程：
		Given a plane π, defined by a point P(平面上的一点) and a normal n, all points X on the 
		plane satisfy the equation n · (X - P) = 0 (that is, the vector from P to X(平面上的任意一点) 
		is perpendicular to n).
		Now let Q be an arbitrary point in space. The closest point R on the plane to Q is the
		orthogonal projection of Q onto the plane, obtained by moving Q perpendicularly
		(with respect to n) toward the plane. That is, R = Q - tn for some value of t.
		Inserting this expression for R into the plane equation and solving for t gives:
		n · ((Q - tn) - P) = 0 <-> (inserting R for X in plane equation)
		n · Q - t(n · n) - n · P = 0 <-> (expanding dot product)
		n · (Q - P) = t(n · n) <-> (gathering similar terms and moving t expression to RHS)
		t = n · (Q - P)/(n · n) (dividing both sides by n · n)
		至此，空间中的点到平面的有符号距离t已经推导出来了。
		下面是对投影点的推导：
		Substituting this expression for t in R = Q - tn gives the projection point R as
		R = Q - (n · (Q - P)/(n · n))n.
		When n is of unit length, t simplifies to t = n · (Q - P), giving R as simply
		R = Q - (n · (Q - P))n. From this equation it is easy to see that for an arbitrary point
		Q, t = n · (Q - P) corresponds to the signed distance of Q from the plane in units of
		the length of n. If t is positive, Q is in front of the plane (and if negative, Q is behind
		the plane).
		*/

		auto n = plane.getN();
		auto d = plane.getD();
		auto len2 = n.squaredLength();

		//参数所给定的点到该平面的有符号距离
		real_type t{};

		//法线为单位长度
		if (Math::isUnit(len2))
		{
			t = n.dotProduct(point) - d;				//d = n.平面上的一点
		}
		else
		{
			t = (n.dotProduct(point) - d) / len2;
		}

		return t;
	}

	real_type UNG_STDCALL ungPointToAABBSquaredDistance(AABB const & box, Vector3 const & point)
	{
		/*
		对于顶点P，当获取包围盒B上的最近点Q并计算二者距离时，无须显式计算最近点Q。
		*/

		auto min = box.getMin();
		auto max = box.getMax();
		
		float sqDist = 0.0;

		//For each axis count any excess(过量的) distance outside box extents
		if (point.x < min.x)
		{
			sqDist += (min.x - point.x) * (min.x - point.x);
		}

		if (point.x > max.x)
		{
			sqDist += (point.x - max.x) * (point.x - max.x);
		}

		if (point.y < min.y)
		{
			sqDist += (min.y - point.y) * (min.y - point.y);
		}

		if (point.y > max.y)
		{
			sqDist += (point.y - max.y) * (point.y - max.y);
		}

		if (point.z < min.z)
		{
			sqDist += (min.z - point.z) * (min.z - point.z);
		}

		if (point.z > max.z)
		{
			sqDist += (point.z - max.z) * (point.z - max.z);
		}

		return sqDist;
	}

	bool UNG_STDCALL ungIsThreePointsCollinear(Vector3 const & p1, Vector3 const & p2, Vector3 const & p3)
	{
		auto ret = (p2 - p1).crossProduct(p3 - p1);

		if (Math::isZero(ret.squaredLength()))
		{
			return true;
		}

		return false;
	}

	bool UNG_STDCALL ungIsPointOutsideOfPlane(Vector3 const & a, Vector3 const & b, Vector3 const & c, Vector3 const & point)
	{
#if UNG_DEBUGMODE
		//确保用于构建平面的3个点不共线
		BOOST_ASSERT(!ungIsThreePointsCollinear(a, b, c));
#endif

		//计算标量三重积
		return ungScalarTripleProduct(point - a, b - a, c - a) >= 0.0;
	}

	bool UNG_STDCALL ungIsPointOutsideOfTriangle(Triangle const & triangle, Vector3 const & point)
	{
		auto a = triangle.getVertices()[0];
		auto b = triangle.getVertices()[1];
		auto c = triangle.getVertices()[2];

		//计算标量三重积
		auto scalarTripleProduct = ungScalarTripleProduct(point - a, b - a, c - a);

		return scalarTripleProduct >= 0.0;
	}

	int32 UNG_STDCALL ungPointSideOfTriangle(Triangle const & triangle, Vector3 const& point)
	{
		auto vertices = triangle.getVertices();

		//列为主
		Matrix4 Orient3D{ vertices[0].x,vertices[0].y,vertices[0].z,1.0,
			vertices[1].x,vertices[1].y,vertices[1].z,1.0,
			vertices[2].x,vertices[2].y,vertices[2].z,1.0,
			point.x,point.y,point.z,1.0 };

		auto det = Orient3D.determinant();

#if UNG_DEBUGMODE
		//使用标量三重积来进行验证
		if (det < 0.0)
		{
			//点位于负法线半空间
			BOOST_ASSERT(ungIsPointOutsideOfTriangle(triangle, point));
		}
#endif

		return det;
	}

	bool UNG_STDCALL ungIsPointInTriangle(Triangle const & triangle, Vector3 const & point)
	{
		/*
		质心坐标具有投影不变性。
		这一特征与前述的坐标计算方法相比更具高效性：
		即无须使用顶点的3D坐标来计算面积，可以将顶点投影至xy，xz或yz平面从而简化计算过程。
		为了防止退化现象，投影总是作用于最大投影面，三角形法线分量的最大绝对值给出了投影时的“舍弃”分量。
		*/
		auto lambdaExp =  [](real_type x1, real_type y1, real_type x2, real_type y2, real_type x3, real_type y3)
		{
			return (x1 - x2)*(y2 - y3) - (x2 - x3)*(y1 - y2);
		};

		auto vertices = triangle.getVertices();

		Vector3 a{vertices[0]}, b{vertices[1]}, c{vertices[2]};

		//Unnormalized triangle normal
		Vector3 m = (b - a).crossProduct(c - a);
		//Nominators and one-over-denominator for u and v ratios
		real_type nu, nv, ood;
		//Absolute components for determining projection plane
		real_type x = Math::calAbs(m.x), y = Math::calAbs(m.y), z = Math::calAbs(m.z);

		//Compute areas in plane of largest projection
		if (x >= y && x >= z)
		{
			//x is largest, project to the yz plane
			nu = lambdaExp(point.y, point.z, b.y, b.z, c.y, c.z);		//Area of PBC in yz plane
			nv = lambdaExp(point.y, point.z, c.y, c.z, a.y, a.z);		//Area of PCA in yz plane
			ood = 1.0 / m.x;														//1/(2*area of ABC in yz plane)
		}
		else if (y >= x && y >= z)
		{
			//y is largest, project to the xz plane
			nu = lambdaExp(point.x, point.z, b.x, b.z, c.x, c.z);
			nv = lambdaExp(point.x, point.z, c.x, c.z, a.x, a.z);
			ood = 1.0 / -m.y;
		}
		else
		{
			//z is largest, project to the xy plane
			nu = lambdaExp(point.x, point.y, b.x, b.y, c.x, c.y);
			nv = lambdaExp(point.x, point.y, c.x, c.y, a.x, a.y);
			ood = 1.0 / m.z;
		}

		real_type u, v, w;
		u = nu * ood;
		v = nv * ood;
		w = 1.0 - u - v;

		auto ret = (v >= 0.0 && w >= 0.0 && (v + w) <= 1.0);

#if UNG_DEBUGMODE
		/*
		验证(采用另一种算法来判断点是否在三角形内)。
		针对三角形ABC及其平面内一点P进行相应的平移操作，使得顶点P位于原点。
		当且仅当三角形PAB,PBC,PCA皆呈现顺时针排列或逆时针排列时，顶点P位于三角形ABC的内部。
		当前，由于视顶点P为原点，因此上述方法等价于计算：
		u = B × C, v = C × A, w = A × B是否指向同一方向，即测试：
		是否u · v ≥ 0且u · w ≥ 0
		根据拉格朗日恒等式将叉积操作转化为点积操作。
		*/

		bool verifyRet{ true };

		//Translate point and triangle so that point lies at origin
		a -= point;
		b -= point;
		c -= point;
		auto ab = ungVector3DotProduct(a, b);
		auto ac = ungVector3DotProduct(a, c);
		auto bc = ungVector3DotProduct(b, c);
		auto cc = ungVector3DotProduct(c, c);

		//Make sure plane normals for pab and pbc point in the same direction
		if (bc * ac - cc * ab < 0.0f)
		{
			verifyRet = false;
			//return verifyRet;
		}

		//Make sure plane normals for pab and pca point in the same direction
		auto bb = ungVector3DotProduct(b, b);
		if (ab * bc - ac * bb < 0.0f)
		{
			verifyRet = false;
			//return verifyRet;
		}

		//Otherwise P must be in (or on) the triangle
		//verifyRet = true;
		//return verifyRet;
		BOOST_ASSERT(verifyRet == ret);
#endif

		return ret;
	}

	bool UNG_STDCALL ungIsPointInPolygon(STL_VECTOR(Vector3) const & polygon, Vector3 const & point)
	{
		//如果不共面的话，那么点一定不会位于多边形的内部
		auto isCoplanar = ungPointSideOfTriangle(Triangle(polygon[0],polygon[1],polygon[2]),point);

		if (!isCoplanar)
		{
			return false;
		}

		/*
		算法消耗的时间为：O(log n)。
		假设n边凸多边形顶点为V0,V1, . . . , Vn-1，通过测试顶点P与方向直线V0,Vk,k = n/2的位置关系，
		可将多边形的测试范围缩至一半。该过程重复执行并相应地调整k值，直至发现顶点P位于多边形的外部
		或位于有向直线V0Vk~V0Vk+1之间。对于后者，若顶点P位于有向直线VkVk+1的左侧，则其位于
		多边形的内部。
		*/

		//多边形的法线
		auto pN = (polygon[1] - polygon[0]).crossProduct(polygon[2] - polygon[0]);

		auto isTriangleCCW = [&pN](Vector3 const& p0,Vector3 const& p1,Vector3 const& p2)
		{
			auto n = (p1 - p0).crossProduct(p2 - p0);

			return n.dotProduct(pN) > 0.0;
		};

		/*
		Do binary search over polygon vertices to find the fan triangle(v[0],v[low],v[high]) the 
		point p lies within the near sides of.
		*/
		uint32 low = 0,high = polygon.size();

		do
		{
			int mid = (low + high) / 2;
			if (isTriangleCCW(polygon[0],polygon[mid],point))
			{
				low = mid;
			}
			else
			{
				high = mid;
			}
		}while (low + 1 < high);

		//If point outside last (or first) edge, then it is not inside the n-gon
		if (low == 0 || high == polygon.size())
		{
			return false;
		}

		//point is inside the polygon if it is left of the directed edge from v[low] to v[high]
		return isTriangleCCW(polygon[low],polygon[high],point);
	}

	bool UNG_STDCALL ungIsPointInAABB(AABB const & box, Vector3 const & point)
	{
		auto min = box.getMin();
		auto max = box.getMax();

		return min <= point && max >= point;
	}

	bool UNG_STDCALL ungIsPointInCylinder(Cylinder const & cylinder, Vector3 const & point)
	{
		auto top = cylinder.getTopCenter();
		auto bottom = cylinder.getBottomCenter();
		auto radius = cylinder.getRadius();

		//计算point到top和bottom所定义的“直线”上的最近点
		Vector3 out{};
		ungClosestPointOnStraightLineToPoint(top, bottom, point, out);

		//判断out是否在top和bottom所定义的“线段”上
		if ((out - top).length() + (out - bottom).length() > (top - bottom).length())
		{
			return false;
		}

		//计算point到圆柱体中心线段的距离的平方
		auto sd = ungPointToLineSegmentSquaredDistance(top, bottom, point);

		if (sd > radius * radius)
		{
			return false;
		}

		return true;
	}

	uint32 UNG_STDCALL ungPointSideOfCylinder(Cylinder const & cylinder, Vector3 const & point)
	{
		if (ungIsPointInCylinder(cylinder, point))
		{
			return 0;
		}

		auto a = cylinder.getTopCenter();
		auto b = cylinder.getBottomCenter();

		auto d = b - a;
		auto pa = point - a;
		auto pb = point - b;

		if (pa.dotProduct(d) < 0.0)
		{
			return 2;
		}

		if (pb.dotProduct(-d) < 0.0)
		{
			return 3;
		}

		return 1;
	}

	bool UNG_STDCALL ungIsTwoTrianglescoplanar(Triangle const & triangle1, Triangle const & triangle2)
	{
		//三角形1的顶点
		auto v1 = triangle1.getVertices();
		//三角形2的顶点
		auto v2 = triangle2.getVertices();

		//计算三角形2的3个顶点是否均位于三角形1的面上
		auto value1 = ungPointSideOfTriangle(triangle1, v2[0]);
		auto value2 = ungPointSideOfTriangle(triangle1, v2[1]);
		auto value3 = ungPointSideOfTriangle(triangle1, v2[2]);

		if (Math::isZero(value1) && Math::isZero(value2) && Math::isZero(value3))
		{
			return true;
		}

		return false;
	}

	bool UNG_STDCALL ungIsTriangleContainedInTriangle(Triangle const & bigTriangle, Triangle const & smallTriangle)
	{
		auto vSmall = smallTriangle.getVertices();

		auto inside0 = ungIsPointInTriangle(bigTriangle, vSmall[0]);
		auto inside1 = ungIsPointInTriangle(bigTriangle, vSmall[1]);
		auto inside2 = ungIsPointInTriangle(bigTriangle, vSmall[2]);

		if (inside0 && inside1 && inside2)
		{
			return true;
		}

		return false;
	}

	real_type UNG_STDCALL ungIsTriangleFullyInsideHalfSpaceOfPlane(Plane const & plane, Triangle const & triangle)
	{
		auto v = triangle.getVertices();

		//计算三角形的三个顶点到plane的有符号距离
		real_type dis0 = ungSignedDistanceFromPointToPlane(plane, v[0]);
		real_type dis1 = ungSignedDistanceFromPointToPlane(plane, v[1]);
		real_type dis2 = ungSignedDistanceFromPointToPlane(plane, v[2]);

		if (dis0 > 0.0 && dis1 > 0.0 && dis2 > 0.0)
		{
			return 1.0;
		}

		if (dis0 < 0.0 && dis1 < 0.0 && dis2 < 0.0)
		{
			return -1.0;
		}

		return 0.0;
	}

	bool UNG_STDCALL ungIsSphereFullyInsideNegativeHalfSpaceOfPlane(Plane const & plane, Sphere const & sphere)
	{
		auto dist = ungSignedDistanceFromPointToPlane(plane, sphere.getCenter());
		auto radius = sphere.getRadius();

		return dist < -radius;
	}

	bool UNG_STDCALL ungIsSphereFullyInsideAABB(Sphere const & sphere, AABB const & box)
	{
		//包裹球体的AABB
		auto sphereWorldAABB = sphere.getWorldAABB();

		return box.contain(sphereWorldAABB);
	}

	bool UNG_STDCALL ungIsAABBFullyInsideNegativeHalfSpaceOfPlane(Plane const & plane, AABB const & box)
	{
		for (uint32 i = 0; i < 8; ++i)
		{
			//角
			auto corner = box.getIndexedCorner(i);

			//角到平面的有符号距离(负：背面，正：正面，0：共面)
			auto signedDis = ungSignedDistanceFromPointToPlane(plane, corner);

			//这里当为0时，没有return false
			if (signedDis > 0.0)
			{
				return false;
			}
		}

		return true;
	}

	bool UNG_STDCALL ungIsAABBFullyInsidePositiveHalfSpaceOfPlane(Plane const & plane, AABB const & box)
	{
		for (uint32 i = 0; i < 8; ++i)
		{
			//角
			auto corner = box.getIndexedCorner(i);

			//角到平面的有符号距离(负：背面，正：正面，0：共面)
			auto signedDis = ungSignedDistanceFromPointToPlane(plane, corner);

			//这里当为0时，没有return false
			if (signedDis < 0.0)
			{
				return false;
			}
		}

		return true;
	}

	bool UNG_STDCALL ungIsAABBFullyInsideSphere(AABB const & box, Sphere const & sphere)
	{
		//球心
		auto center = sphere.getCenter();
		//半径
		auto radius = sphere.getRadius();
		//半径的平方
		auto sqRadius = radius * radius;

		//遍历AABB的8个顶点
		for (uint32 i = 0; i < 8; ++i)
		{
			//角
			auto corner = box.getIndexedCorner(i);

			if (corner.squaredDistance(center) > sqRadius)
			{
				return false;
			}
		}

		return true;
	}

	bool UNG_STDCALL ungIsAABBFullyInsidePlaneBoundedVolume(AABB const & box, PlaneBoundedVolume const & pbv)
	{
		auto its = pbv.getIterator();
		auto beg = its.first;
		auto end = its.second;

		while (beg != end)
		{
			//平面
			auto& plane = *(*beg);

			if (!ungIsAABBFullyInsideNegativeHalfSpaceOfPlane(plane,box))
			{
				return false;
			}

			++beg;
		}

		return true;
	}

	void UNG_STDCALL ungClosestPointOnStraightLineToPoint(Vector3 const & start, Vector3 const & end, Vector3 const & point, Vector3 & out)
	{
		auto P = point - start;
		auto Q = end - start;

		real_type t = P.dotProduct(Q) / Q.squaredLength();

		out = start + t * Q;
	}

	void UNG_STDCALL ungClosestPointOnLineSegmentToPoint(Vector3 const & start, Vector3 const & end, Vector3 const & point, Vector3 & out)
	{
		/*
		Let AB be a line segment specified by the endpoints A and B. Given an arbitrary point C, 
		the problem is to determine the point D on AB closest to C.projecting C onto the extended 
		line through AB provides the solution. If the projection point P lies within the segment, P 
		itself is the correct answer. If P lies outside the segment, it is instead the segment 
		endpoint closest to C that is the closest point.
		直线AB上的一点可参数化为：
		P(t) = A + t(B - A)，注意：这里t取值为[0,1]，那么就不要求(B - A)为单位向量。
		t = (C - A) · n / |B - A|, where n = (B - A) / |B - A| is a unit vector in the direction of AB.
		*/

		/*
		向量P在向量Q上的投影向量为：
		(P · Q) * Q / (|Q| ^ 2)
		对应的t为：
		(P · Q) / (|Q| ^ 2)
		这里没有乘的Q，对应P(t) = A + t(B - A)中的(B - A)
		*/

		/*
		auto P = point - start;
		auto Q = end - start;

		real_type t = P.dotProduct(Q) / Q.squaredLength();

		if (t < 0.0)
		{
			t = 0.0;
		}

		if (t > 1.0)
		{
			t = 1.0;
		}

		out = start + t * Q;
		*/

		auto P = point - start;
		auto Q = end - start;

		real_type t = P.dotProduct(Q);

		if (t < 0.0)
		{
			t = 0.0;
			out = start;
		}
		else
		{
			real_type denom = Q.squaredLength();

			if (t > denom)
			{
				t = 1.0;
				out = end;
			}
			else
			{
				t = t / denom;
				out = start + t * Q;
			}
		}
	}

	void UNG_STDCALL ungClosestPointOnRayToPoint(Ray const & ray, Vector3 const & point, Vector3 & out)
	{
		//射线的起点
		auto start = ray.getOrigin();
		//射线的方向(单位向量)
		auto Q = ray.getDirection();

		auto P = point - start;

		real_type t = P.dotProduct(Q);

		if (t < 0.0)
		{
			t = 0.0;
		}

		out = start + t * Q;
	}

	void UNG_STDCALL ungClosestPointOnPlaneToPoint(Plane const & plane, Vector3 const & point, Vector3& out)
	{
		auto n = plane.getN();

		real_type signedDis = ungSignedDistanceFromPointToPlane(plane, point);

		//投影点：R = Q - (n · (Q - P)/(n · n))n
		out = point - signedDis * n;
	}

	void UNG_STDCALL ungClosestPointOnTriangleToPoint(Triangle const & triangle, Vector3 const & point, Vector3& out)
	{
		//三角形的顶点
		auto vertices = triangle.getVertices();
		auto a = vertices[0];
		auto b = vertices[1];
		auto c = vertices[2];

		auto p = point;

#if UNG_DEBUGMODE
		//蛮力算法，用于验证。
		//结果
		Vector3 ret{};
		Vector3 projOnPlane{};
		//根据三角形来构建一个平面
		Plane plane(triangle);
		//找到空间中任意一点在该平面上的正交投影
		ungClosestPointOnPlaneToPoint(plane, p, projOnPlane);
		//现在投影点和三角形处于同一个平面中了
		//判断投影点位于三角形内还是外
		bool in = ungIsPointInTriangle(triangle, projOnPlane);
		//如果是内，那么投影点就是三角形上距离空间中任意一点的最近点
		if (in)
		{
			ret = projOnPlane;
		}
		else
		{
			//如果是外，那么找出投影点距离三角形的哪一条边的距离最短
			//ab:0
			auto dis_ab = ungPointToLineSegmentSquaredDistance(a, b, projOnPlane);
			//ac:1
			auto dis_ac = ungPointToLineSegmentSquaredDistance(a, c, projOnPlane);
			//bc:2
			auto dis_bc = ungPointToLineSegmentSquaredDistance(b, c, projOnPlane);

			int32 flag = -1;
			if (dis_ab < dis_ac)
			{
				flag = 0;

				if (dis_ab > dis_bc)
				{
					flag = 2;
				}
			}
			else
			{
				flag = 1;

				if (dis_ac > dis_bc)
				{
					flag = 2;
				}
			}
			//现在问题就转化为寻找线段上的距离投影点最近的点了
			switch (flag)
			{
			case 0:
				ungClosestPointOnLineSegmentToPoint(a, b, projOnPlane, ret);
				break;
			case 1:
				ungClosestPointOnLineSegmentToPoint(a, c, projOnPlane, ret);
				break;
			case 2:
				ungClosestPointOnLineSegmentToPoint(b, c, projOnPlane, ret);
				break;
			}
			/*
			至此，就得到了通过蛮力算法计算的三角形上距离空间任意一点最近的点ret了。
			在下面的BOOST_ASSERT中进行验证。
			*/
		}
#endif

		//--------------------------------------------------------------------

		//Check if P in vertex region outside A
		auto ab = b - a;
		auto ac = c - a;
		auto ap = p - a;
		real_type d1 = ungVector3DotProduct(ab, ap);
		real_type d2 = ungVector3DotProduct(ac, ap);
		if (d1 <= 0.0 && d2 <= 0.0)
		{
			//barycentric coordinates (1,0,0)
			out = a;
#if UNG_DEBUGMODE
			BOOST_ASSERT(out == ret);
#endif
			return;
		}

		//Check if P in vertex region outside B
		auto bp = p - b;
		real_type d3 = ungVector3DotProduct(ab, bp);
		real_type d4 = ungVector3DotProduct(ac, bp);
		if (d3 >= 0.0 && d4 <= d3)
		{
			//barycentric coordinates (0,1,0)
			out = b;
#if UNG_DEBUGMODE
			BOOST_ASSERT(out == ret);
#endif
			return;
		}

		//Check if P in edge region of AB, if so return projection of P onto AB
		real_type vc = d1 * d4 - d3 * d2;
		if (vc <= 0.0 && d1 >= 0.0 && d3 <= 0.0)
		{
			real_type v = d1 / (d1 - d3);

			//barycentric coordinates (1-v,v,0)
			out = a + v * ab;
#if UNG_DEBUGMODE
			BOOST_ASSERT(out == ret);
#endif
			return;
		}

		//Check if P in vertex region outside C
		auto cp = p - c;
		real_type d5 = ungVector3DotProduct(ab, cp);
		real_type d6 = ungVector3DotProduct(ac, cp);
		if (d6 >= 0.0 && d5 <= d6)
		{
			//barycentric coordinates (0,0,1)
			out = c;
#if UNG_DEBUGMODE
			BOOST_ASSERT(out == ret);
#endif
			return;
		}

		//Check if P in edge region of AC, if so return projection of P onto AC
		real_type vb = d5*d2 - d1*d6;
		if (vb <= 0.0 && d2 >= 0.0 && d6 <= 0.0)
		{
			real_type w = d2 / (d2 - d6);

			//barycentric coordinates (1-w,0,w)
			out = a + w * ac;
#if UNG_DEBUGMODE
			BOOST_ASSERT(out == ret);
#endif
			return;
		}

		//Check if P in edge region of BC, if so return projection of P onto BC
		real_type va = d3*d6 - d5*d4;
		if (va <= 0.0 && (d4 - d3) >= 0.0 && (d5 - d6) >= 0.0)
		{
			real_type w = (d4 - d3) / ((d4 - d3) + (d5 - d6));

			//barycentric coordinates (0,1-w,w)
			out = b + w * (c - b);
#if UNG_DEBUGMODE
			BOOST_ASSERT(out == ret);
#endif
			return;
		}

		//P inside face region. Compute Q through its barycentric coordinates (u,v,w)
		real_type denom = 1.0 / (va + vb + vc);
		real_type v = vb * denom;
		real_type w = vc * denom;

		//= u*a + v*b + w*c, u = va * denom = 1.0f - v - w
		out = a + ab * v + ac * w;
#if UNG_DEBUGMODE
		BOOST_ASSERT(out == ret);
#endif
		return;
	}

	void UNG_STDCALL ungClosestPointOnAABBToPoint(AABB const & box, Vector3 const & point, Vector3 & out)
	{
		auto min = box.getMin();
		auto max = box.getMax();

		out = point;

		if (point.x < min.x)
		{
			out.x = min.x;
		}
		else if (point.x > max.x)
		{
			out.x = max.x;
		}

		if (point.y < min.y)
		{
			out.y = min.y;
		}
		else if (point.y > max.y)
		{
			out.y = max.y;
		}

		if (point.z < min.z)
		{
			out.z = min.z;
		}
		else if (point.z > max.z)
		{
			out.z = max.z;
		}
	}

	void UNG_STDCALL ungClosestPointOnTetrahedronToPoint(Vector3 const & p0, Vector3 const & p1, Vector3 const & p2, Vector3 const & p3, Vector3 const & point, Vector3 & out)
	{
		//如果point位于四面体的内部
		real_type u{}, v{}, w{}, x{};
		if (ungTetrahedronBarycentric(p0, p1, p2, p3, point, u, v, w, x))
		{
			out = point;
			return;
		}

		//因为可能要改变顶点winding，所以这里拷贝
		auto a = p0;
		auto b = p1;
		auto c = p2;
		auto d = p3;

		//找到四面体的中心点
		Vector3 center = 0.5 * (a + b + c + d);

		auto closestPointOnTriangleToPoint = [&center](Vector3 const& a, Vector3& b, Vector3& c, Vector3 const& p)
		{
			//四面体的中心点是否位于由v0,v1,v2所构成的三角形的负法线空间
			auto isOutSide = ungIsPointOutsideOfTriangle(Triangle(a, b, c), center);
			//如果不是，那么交换v1,v2
			if (!isOutSide)
			{
				b.swap(c);
			}

			Vector3 out{};
			Triangle t(a, b, c);
			ungClosestPointOnTriangleToPoint(t, p, out);

			return out;
		};

		real_type bestSqDist = Math::POS_INFINITY;

		Vector3 q{};
		real_type sqDist{};

		q = closestPointOnTriangleToPoint(a, b, c, point);
		sqDist = (q - point).squaredLength();
		if (sqDist < bestSqDist)
		{
			bestSqDist = sqDist;
			out = q;
		}

		q = closestPointOnTriangleToPoint(a, c, d, point);
		sqDist = (q - point).squaredLength();
		if (sqDist < bestSqDist)
		{
			bestSqDist = sqDist;
			out = q;
		}

		q = closestPointOnTriangleToPoint(a, d, b, point);
		sqDist = (q - point).squaredLength();
		if (sqDist < bestSqDist)
		{
			bestSqDist = sqDist;
			out = q;
		}

		q = closestPointOnTriangleToPoint(b, d, c, point);
		sqDist = (q - point).squaredLength();
		if (sqDist < bestSqDist)
		{
			bestSqDist = sqDist;
			out = q;
		}
	}

	void UNG_STDCALL ungClosestPointBetweenTwoStraightLines(Vector3 const & p1, Vector3 const & q1, Vector3 const & p2, Vector3 const & q2, Vector3& out1, Vector3& out2)
	{
		/*
		Let the lines L1 and L2 be specified parametrically by the points P1 and Q1 and P2 and Q2:
		L1(s) = P1 + sd1, d1 = Q1 - P1
		L2(t) = P2 + td2, d2 = Q2 - P2
		For some pair of values for s and t, L1(s) and L2(t) correspond to the closest points
		on the lines, and v(s, t) = L1(s) - L2(t) describes a vector between them.
		The points are at their closest when v is of minimum length. The key realization is
		that this happens when v is perpendicular to both L1 and L2.
		The problem is now finding values for s and t satisfying these two perpendicularity
		constraints:
		d1 · v(s, t) = 0
		d2 · v(s, t) = 0.
		Substituting the parametric equation for v(s, t) gives:
		d1 · (L1(s) - L2(t)) = d1 · ((P1 - P2) + sd1 - td2) = 0
		d2 · (L1(s) - L2(t)) = d2 · ((P1 - P2) + sd1 - td2) = 0.
		This can be expressed as the 2 × 2 system of linear equations
		(d1 · d1)s - (d1 · d2)t = -(d1 · r)
		(d2 · d1)s - (d2 · d2)t = -(d2 · r),
		where r = P1 - P2.
		Written symbolically, in matrix notation, this corresponds to
		a		-b			s					-c
									=
		b		-e			t					-f
		where a = d1 · d1, b = d1 · d2, c = d1 · r, e = d2 · d2, and f = d2 · r
		Cramer’s rule to give:
		s = (bf - ce) / d
		t = (af - bc) / d,
		where d = ae - b2
		*/

		//如果两条直线平行
		auto ret = (q1 - p1).isParallel(q2 - p2);
		if (ret)
		{
			out1 = p1;
			ungClosestPointOnStraightLineToPoint(p2,q2,out1,out2);

			return;
		}

		auto d1 = q1 - p1;
		auto d2 = q2 - p2;
		auto r = p1 - p2;

		auto a = ungVector3DotProduct(d1, d1);
		auto b = ungVector3DotProduct(d1, d2);
		auto c = ungVector3DotProduct(d1, r);
		auto e = ungVector3DotProduct(d2, d2);
		auto f = ungVector3DotProduct(d2, r);
		auto d = a * e - b * b;

		real_type s{}, t{};
		s = (b * f - c * e) / d;
		t = (a * f - b * c) / d;

		out1 = p1 + s * d1;
		out2 = p2 + t * d2;

#if UNG_DEBUGMODE
		//验证
		Vector3 out11, out22;
		ungClosestPointOnStraightLineToPoint(p2, q2, out1, out22);
		BOOST_ASSERT(out22 == out2);
		ungClosestPointOnStraightLineToPoint(p1, q1, out2, out11);
		BOOST_ASSERT(out11 == out1);
#endif
	}

	void UNG_STDCALL ungClosestPointBetweenTwoLineSegments(Vector3 const & start1, Vector3 const & end1, Vector3 const & start2, Vector3 const & end2, Vector3 & out1, Vector3 & out2)
	{
		/*
		若直线L1和L2间的最近点恰好位于对应线段上，则可以应用相关的直线最近点算法。
		而对于直线L1和L2间最近点位于线段外部延长线的情况，一种常见的错误是将外部最近点截取为距其
		最近的线段端点。
		S1(s) = P1 + sd1, d1 = Q1 - P1, 0 ≤ s ≤ 1
		S2(t) = P2 + td2, d2 = Q2 - P2, 0 ≤ t ≤ 1
		在某些情况下，若直线间某一最近点位于相关线段的外部延长线上，该点是可以截取至相应线段的最近
		端点处的，比如L1上的最近点截取为线段S1的端点Q1。于是，计算L2上距Q1的最近点R且该点位于
		线段S2上，则线段间的最近点为Q1和R。
		若直线间的最近点皆位于各自线段的外部延长线上，则上述截取操作需要重复计算两次。L1上的最近点
		截取为线段S1的端点Q1，计算L2上距Q1的最近点R。由于R位于线段S2的外部延长线上，将其截取为
		线段的最近端点Q2，再次计算L1上距Q2的最近点S且该点位于线段S1上，则线段间的最近点为Q2和S。
		给定直线上L2一点S2(t) = P2 + td2，则L1上最近点L1(s)为：
		s = (S2(t) - P1) · d1 / d1 · d1 = (P2 + td2 - P1) · d1 / d1 · d1
		类似的，给定直线S1上一点S1(s) = P1 + sd1，直线L2上最近点L2(t)为：
		t = (S1(s) - P2) · d2 / d2 · d2 = (P1 + sd1 - P2) · d2 / d2 · d2

		s = (bt - c) / a
		t = (bs + f ) / e
		with a = d1 · d1, b = d1 · d2, c = d1 · r, e = d2 · d2, and f = d2 · r
		*/

		real_type s{}, t{};

		auto d1 = end1 - start1;					//Direction vector of segment S1
		auto d2 = end2 - start2;					//Direction vector of segment S2

		auto r = start1 - start2;
		auto a = ungVector3DotProduct(d1, d1);				//Squared length of segment S1, always nonnegative
		auto e = ungVector3DotProduct(d2, d2);				//Squared length of segment S2, always nonnegative
		auto f = ungVector3DotProduct(d2, r);

		auto clamp = [](real_type n, real_type min, real_type max)
		{
			if (n < min)
			{
				return min;
			}

			if (n > max)
			{
				return max;
			}

			return n;
		};

		//Check if either or both segments degenerate into points(退化成点)
		if (Math::isZero(a) && Math::isZero(e))
		{
			//Both segments degenerate into points
			s = t = 0.0;
			out1 = start1;
			out2 = start2;
		}

		if (Math::isZero(a))
		{
			//First segment degenerates into a point
			s = 0.0;
			//s = 0 => t = (b * s + f) / e = f / e
			t = f / e;
			t = clamp(t, 0.0, 1.0);
		}
		else
		{
			real_type c = ungVector3DotProduct(d1, r);

			if (Math::isZero(e))
			{
				//Second segment degenerates into a point
				t = 0.0;
				//t = 0 => s = (b * t - c) / a = -c / a
				s = clamp(-c / a, 0.0, 1.0);
			}
			else
			{
				//The general nondegenerate(非退化) case starts here
				real_type b = ungVector3DotProduct(d1, d2);
				real_type denom = a * e - b * b;

				//If segments not parallel, compute closest point on L1 to L2 and clamp to segment S1. Else pick arbitrary s (here 0)
				if (denom != 0.0)
				{
					//直线算法
					s = clamp((b * f - c * e) / denom, 0.0, 1.0);
				}
				else
				{
					s = 0.0;
				}

				/*
				Compute point on L2 closest to S1(s) using 
				t = Dot((P1 + D1*s) - P2,D2) / Dot(D2,D2) = (b * s + f) / e
				*/
				//线段算法
				t = (b * s + f) / e;
				/*
				If t in [0,1] done.
				Else clamp t,recompute s for the new value of t using 
				s = Dot((P2 + D2*t) - P1,D1) / Dot(D1,D1)= (t * b - c) / a
				and clamp s to [0, 1]
				*/
				//线段算法
				if (t < 0.0)
				{
					t = 0.0;
					s = clamp(-c / a, 0.0, 1.0);
				}
				else if (t > 1.0)
				{
					t = 1.0;
					s = clamp((b - c) / a, 0.0, 1.0);
				}
			}
		}

		out1 = start1 + d1 * s;
		out2 = start2 + d2 * t;
	}

	bool UNG_STDCALL ungTriangleBarycentric(Triangle const& triangle, Vector3  const& point, real_type& u, real_type& v, real_type& w)
	{
		/*
		三元组(u,v,w)对应着顶点的质心坐标。对于三角形ABC，顶点A，B，C的质心坐标分别为(1, 0, 0),
		(0, 1, 0), 和 (0, 0, 1)。一般地，当且仅当0 ≤ u, v, w ≤ 1，或当且仅当0 ≤ v ≤ 1, 0 ≤ w ≤ 1, 和
		v + w ≤ 1时，具有质心坐标(u,v,w)的顶点将位于三角形的内部（或边上）。质心坐标实际上按照
		P = uA + vB + wC的方式参数化了平面，且是P = A + v(B - A) + w(C - A)的另一种表达形式：
		P = A + v(B - A) + w(C - A) = (1 - v - w)A + vB + wC（该式可以推导出来）
		为了求解质心坐标，将表达式写作v v0 + w v1 = v2
		其中v0 = B - A, v1 = C - A, v2 = P - A
		则，通过两边乘以v0,v1构造一个2x2线性方程组：
		(v v0 + w v1) · v0 = v2 · v0
		(v v0 + w v1) · v1 = v2 · v1
		由于点积是一个线性操作符，上述表达式等价于：
		v (v0 · v0) + w (v1 · v0) = v2 · v0
		v (v0 · v1) + w (v1 · v1) = v2 · v1
		该方程组可以应用克莱姆法则。
		*/

		auto vertices = triangle.getVertices();

		Vector3 v0 = vertices[1] - vertices[0], v1 = vertices[2] - vertices[0], v2 = point - vertices[0];
		auto d00 = v0.dotProduct(v0);
		auto d01 = v0.dotProduct(v1);
		auto d11 = v1.dotProduct(v1);
		auto d20 = v2.dotProduct(v0);
		auto d21 = v2.dotProduct(v1);

		//系数矩阵
		Matrix2 coefficientMatrix{d00,d01,d01,d11};
		//常数向量
		Vector2 constantVector{d20,d21};
		//待求解的向量
		Vector2 out;
		auto ret = ungSolvingLinearEquations2(coefficientMatrix,constantVector,out);
		BOOST_ASSERT(ret);

		v = out.x;
		w = out.y;
		//上面的推导过程，在形成线性方程时，u是被替换了的，也就是说，线性方程的解为v和w。
		u = 1.0 - v - w;

		return u >= 0.0 && u <= 1.0 &&
			v >= 0.0 && v <= 1.0 &&
			w >= 0.0 && w <= 1.0;
	}

	bool UNG_STDCALL ungTetrahedronBarycentric(Vector3 const & a, Vector3 const & b, Vector3 const & c, Vector3 const & d, Vector3 const & point, real_type & u, real_type & v, real_type & w, real_type & x)
	{
		/*
		通过求解下列线性方程组可以得到质心坐标。
		(bx - ax)v + (cx - ax) w + (dx - ax) x = px - ax
		(by - ay)v + (cy - ay) w + (dy - ay) x = py - ay
		(bz - az)v + (cz - az) w + (dz - az) x = pz - az
		*/

		//系数矩阵
		Matrix3 coefficientMatrix{b.x - a.x,c.x - a.x,d.x - a.x,
							b.y - a.y,c.y - a.y,d.y - a.y,
							b.z - a.z,c.z - a.z,d.z - a.z};
		
		//常数向量
		Vector3 constantVector{point.x - a.x,point.y - a.y,point.z - a.z};

		//待求解向量
		Vector3 out{};

		//解线性方程组
		auto ret = ungSolvingLinearEquations3(coefficientMatrix,constantVector,out);

		BOOST_ASSERT_MSG(ret,"The linear equations specified by the parameters have no solution.");

		v = out.x;
		w = out.y;
		x = out.z;
		u = 1.0 - v - w - x;

		return u >= 0.0 && u <= 1.0 &&
			v >= 0.0 && v <= 1.0 &&
			w >= 0.0 && w <= 1.0 &&
			x >= 0.0 && x <= 1.0;
	}

	int32 UNG_STDCALL ungSignedVolumeOfTetrahedron(Vector3 const & v1, Vector3 const & v2, Vector3 const & v3, Vector3 const & v4)
	{
		//参数顺序会影响正负号

		//列为主
		Matrix4 Orient3D{v1.x,v1.y,v1.z,1.0,
						v2.x,v2.y,v2.z,1.0,
						v3.x,v3.y,v3.z,1.0,
						v4.x,v4.y,v4.z,1.0};

		auto det = Orient3D.determinant();

		return det / 6.0;
	}

	bool UNG_STDCALL ungIsQuadConvex(Vector3 const & a, Vector3 const & b, Vector3 const & c, Vector3 const & d)
	{
		auto bda = (d - b).crossProduct(a - b);
		auto bdc = (d - b).crossProduct(c - b);

		if (bda.dotProduct(bdc) >= 0.0)
		{
			return false;
		}

		auto acd = (c - a).crossProduct(d - a);
		auto acb = (c - a).crossProduct(b - a);

		return (acd.dotProduct(acb) < 0.0);
	}

	bool UNG_STDCALL ungIntersectionLineOfTwoPlanes(Plane const & plane1, Plane const & plane2, Vector3 & p0, Vector3 & lineDir)
	{
		auto n1 = plane1.getN();
		auto n2 = plane2.getN();

		/*
		计算交线。
		交线为两个平面的公共边，则必定垂直于两个平面的面法线。
		*/
		lineDir = n1.crossProduct(n2);

		//叉积长度为0.0，n1和n2两个向量是平行的
		if (Math::isZero(lineDir.squaredLength()))
		{
			//两平面平行，不可能相交
			return false;
		}

		//常数向量
		Vector2 cn{ plane1.getD(),plane2.getD() };
		//待求解向量
		Vector2 so{};

		bool ret = false;
		for (int32 i = 0; i < 3; ++i)
		{
			//系数矩阵
			Matrix2 co{};
			if (i == 0)
			{
				co.setRow(0, Vector2(n1.y,n1.z));
				co.setRow(1, Vector2(n2.y,n2.z));
				ret = ungSolvingLinearEquations2(co, cn, so);
				if (ret)
				{
					p0.x = 0.0;
					p0.y = so.x;
					p0.z = so.y;

					break;
				}
			}
			else if (i == 1)
			{
				co.setRow(0, Vector2(n1.x,n1.z));
				co.setRow(1, Vector2(n2.x,n2.z));
				ret = ungSolvingLinearEquations2(co, cn, so);
				if (ret)
				{
					p0.x = so.x;
					p0.y = 0.0;
					p0.z = so.y;

					break;
				}
			}
			else
			{
				co.setRow(0, Vector2(n1.x,n1.y));
				co.setRow(1, Vector2(n2.x,n2.y));
				ret = ungSolvingLinearEquations2(co, cn, so);
				if (ret)
				{
					p0.x = so.x;
					p0.y = so.y;
					p0.z = 0.0;

					break;
				}
			}
		}

		BOOST_ASSERT(ret);

		/*
		相交线方程为:
		L(t) = p0 + t * lineDir
		*/

		return true;
	}
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE