/*
所有的流都可以看成是数据滑槽。有缓冲流和非缓冲流。
所有输入流都有一个关联的来源，所有输出流都有一个关联的目标。
如果编写的是一个库，那么绝对不要假定存在cout，cin，cerr或clog，因为不知道该库会应用在控制台程序还是GUI程序。

流不仅包含数据，还包含一个称为当前位置（current position）的数据。当前位置指的是流将要进行下一次读或写操作的位置。

流这个概念可以应用于任何接受数据或产生数据的对象。
在C++中，流可以使用3个公共的来源和目标。

字符串流是将流隐喻应用于字符串类型。使用字符串流时，可以像处理其他任何流一样处理字符数据。
就字符串流的大部分功能而言，只不过是为string类的很多方法能够完成的功能提供了便利的语法。使用流式语法为优化提供了机会，而且比直接使用string
类方便得多。

放入数据滑槽的内容并非仅限于数据，C++流还能识别操作算子（manipulator），操作算子是能够修改流行为的对象，而不是流能够操作的数据。
*/

/*
可以通过字符串流将流语义用于string。通过这种方式，可得到一个内存内的流（in memory stream），来表示文本数据。
字符串流也非常适于解析文本，因为流内建了标记化的功能。
相对于标准C++ string，字符串流的主要优点是除了数据之外，这个对象还知道从哪里进行下一次的读或写操作，这个位置也称为当前位置。根据字符串流
的特定实现，可能还会有性能优势。例如，如果需要将大量字符串串联在一起，使用字符串流的效率可能高于反复调用string对象的+=运算符。
*/

/*
文件就是一些字节。
string对象本身实际上并没有包含字符串，而是包含一个指向其中存储了字符串的内存单元的指针。因此，复制string的话，复制的不是
真正的数据，而是字符串的存储地址。当再次运行该程序时，该地址将毫无意义。
*/

/*
binary	0010 0000		32		0x20			以二进制方式进行IO(不要替换特殊字符)。(相对于文本模式)
														binary flag使得stream能够封锁特殊字符或特殊字符序列(例如end-of-line或end-of-file)的转换。某些
														操作系统(例如Windows或OS/2)的文本文件，每一行结束时以两个字符(CR和LF)为记号。正常模式下(未
														设置binary)进行读或写，newline(换行)字符会被上述两个字符替换，反之亦然。如果处于二进制模式(设
														置了binary)，就不会进行这样的转换。
														to read a file as a binary stream, rather than as a text stream.

in			0000 0001		1		0x1			以读方式打开。(从开头开始读取)
														to permit(允许) extraction(抽取) from a stream.
														(in，来源文件必须存在)
														(in | out，读和写，最初位置在起点，文件必须存在)
														(in | out | trunc，先清空，再读/写，有必要才创建)
														(in | app，在尾端更新，有必要才创建)
														(in | out | app，在尾端更新，有必要才创建)

out		0000 0010		2		0x2			以写方式打开。(从开头开始写入，覆盖已有的数据)隐含了trunc，若想保留以out模式打开的文件内容，必
														须同时指定app或者同时指定in。
														(out，清空而后涂写，有必要才创建)
														(out | trunc，清空而后涂写，有必要才创建)
														(out | app，追加，有必要才创建)
														to permit insertion to a stream.
app		0000 1000		8		0x8			打开文件，每次写操作前均定位到文件末尾。trunc没有被设定，就可以设定app，设定app隐含了out。
														to seek to the end of a stream before each insertion.
														(app，追加，有必要才创建)
trunc		0001 0000		16		0x10			打开文件，截断文件。(删除任何已有数据)只有当out被设定时，才可以设置trunc。trucn | app是不允许
														存在的。
														to delete contents of an existing file when its controlling object is created.
ate		0000 0100		4		0x4			打开文件，打开文件后令读/写位置定位到文件末尾。
														to seek to the end of a stream when its controlling object is first created.
*/

/*
随机IO本质上是依赖于系统的。

虽然标准库为所有流类型都定义了seek和tell函数，但它们是否会做有意义的事情依赖于流绑定到哪个设备。在大多数系统中，绑定到cin、cout、cerr和clog
的流不支持随机访问---毕竟，当我们向cout直接输出数据时，类似向回跳十个位置这种操作时没有意义的。对这些流我们可以调用seek和tell函数，但在运行
时会出错，将流置于一个无效状态。

seek和tell函数
为了支持随机访问，IO类型维护一个标记来确定下一个读写操作要在哪里进行。它们还提供了两个函数：一个函数通过将标记seek到一个给定位置来重定位
它；另一个函数tell我们标记的当前位置。标准库实际上定义了两对seek和tell函数，如下表所示。一对用于输入流，另一对用于输出流。输入和输出版本的
差别在于名字的后缀是g还是p。g版本表示我们正在“获得”(读取)数据，而p版本表示我们正在“放置”(写入)数据。

seek和tell函数：
tellg(),tellp()
返回一个输入流中(tellg)或输出流中(tellp)标记的当前位置。

seekg(pos),seekp(pos)
在一个输入流或输出流中将标记重定位到给定的绝对地址。pos通常是前一个tellg或tellp返回的值。

seekp(off,from),seekg(off,from)
在一个输入流或输出流中将标记定位到from之前或之后off个字符，from可以是下列值之一：
beg，偏移量相对于流开始位置
cur，偏移量相对于流当前位置
end，偏移量相对于流结尾位置

从逻辑上讲，我们只能对istream和派生自istream的类型ifstream和istringstream使用g版本，同样只能对ostream和派生自ostream的类型ofstream和
ostringstream使用p版本。一个iostream、fstream或stringstream既能读又能写关联的流，因此对这些类型的对象既能使用g版本又能使用p版本。
*/

/*
对于可读又可写的stream，不能在读/写动作之间任意转换其读/写属性。一旦开始读或写，如果一定需要转换，怎么办呢？必须进行一个seek动作，到达
当前位置，再转换读/写属性。唯一例外是，如果已经读到end-of-file，可立即接着写入字符。
*/

/*
C++11，file stream提供了rvalue和move语义。提供了一个move构造函数，一个move assignment操作符以及swap()。
现在可以使用临时创建的stream对象。例如：
string s("Hello");
fstream("test.txt") << s << endl;
fstream("test.txt", ios::app) << "World" << endl;
可以传一个file stream作为函数实参，或让某个函数返回一个file stream。（按值传递）
*/

/*
如果stream对象由多个文件共享，那么处理完一个文件后，必须调用clear()清除被设于文件尾端的state flag。open()并不会清除任何state flag。
*/

/*
指向file stream缓冲区的指针：
fs.rdbuf();
*/

/*
basic_istream::get
Reads one or more characters from the input stream.

int_type get();
basic_istream<Elem, Tr>& get(Elem& _Ch);
basic_istream<Elem, Tr>& get(Elem *_Str,streamsize _Count);
basic_istream<Elem, Tr>& get(Elem *_Str,streamsize _Count,Elem _Delim);
basic_istream<Elem, Tr>& get(basic_streambuf<Elem, Tr>& _Strbuf);
basic_istream<Elem, Tr>& get(basic_streambuf<Elem, Tr>& _Strbuf,Elem _Delim);

Parameters 
_Count				The number of characters to read from strbuf.
_Delim				The character that should terminate the read if it is encountered before _Count.
_Str					A string in which to write.
_Ch					A character to get.
_Strbuf				A buffer in which to write.

Return Value 
The parameterless form of get returns the element read as an integer or end of file. The remaining forms return the stream (*this).

Remarks 
The first of these unformatted input functions extracts an element, if possible, as if by returning rdbuf->sbumpc. Otherwise, it returns 
traits_type::eof. If the function extracts no element, it calls setstate(failbit).
The second function extracts the int_type element meta the same way. If meta compares equal to traits_type::eof, the function calls 
setstate(failbit). Otherwise, it stores traits_type::to_char_type(meta) in _Ch. The function returns *this.
The third function returns get(_Str, _Count, widen('\n')).
The fourth function extracts up to _Count - 1 elements and stores them in the array beginning at _Str. It always stores char_type after 
any extracted elements it stores. In order of testing, extraction stops:
At end of file.
After the function extracts an element that compares equal to _Delim, in which case the element is put back to the controlled sequence.
After the function extracts _Count - 1 elements.
If the function extracts no elements, it calls setstate(failbit). In any case, it returns *this.
The fifth function returns get(strbuf, widen('\n')).
The sixth function extracts elements and inserts them in strbuf. Extraction stops on end-of-file or on an element that compares equal 
to _Delim, which is not extracted. It also stops, without extracting the element in question, if an insertion fails or throws an exception (
which is caught but not rethrown). If the function extracts no elements, it calls setstate(failbit). In any case, the function returns *this.
*/

/*
The Boost Iostreams Library

宗旨：
Boost iostreams库有3个目标：

1：很容易的去创建标准C++ stream和stream buffer来访问新的Source设备和Sink设备。
2：提供一个框架，用来定义Filter并且把它们给连接到标准stream和stream buffer上。
3：提供一些可以直接使用的Filter，Source设备和Sink设备。

比如，Boost的iostream库能够用于创建访问TCP连接的流，或者作为加密和数据压缩的框架。库包含一些组件，可以用来访问memory-mapped files,使
用操作系统的file descriptor访问文件，代码转换（宽窄字符之间转换），使用regular expression来过滤文本，转换换行符，压缩和解压缩zlib,gzip,
bzip2格式。

概览：
Boost iostreams库的核心是一些concepts，通过模板设置来把这些concepts的models给转换为C++标准库的stream和stream buffer。

Concepts：
库的基本构建模块是一些关于Source设备，Sink设备，输入过滤器，输出过滤器的概念。
Source设备，用来提供对字符序列的读访问。
Sink设备，用来提供对字符序列的写访问。
输入过滤器，用来对从Source设备读取的字符进行过滤。
输出过滤器，用来对写入Sink设备的字符进行过滤。

流概念:用来连接设备和过滤器,让数据得以流动.
stream,stream_buffer:搭配设备使用，实现类似标准输入输出流的功能。
filtering_stream,filtering_streambuf:增强的流，里面含有多个设备组成的链，数据流过链上的设备完成
过滤处理。

通用stream和stream buffer:
类模板stream_buffer和stream实现了把I/O操作委托给容器设备的标准stream buffer和stream。我们可以通过成员函数open,is_open,close来访问设
备。设备提供的接口类似于标准的file-based stream和stream buffer。

Filtered stream和stream buffer:
对于过滤，Boost iostreams库提供了filtering_streambuf和filtering_stream模板。filtering_streambuf和filtering_stream的实例包含了过滤器和
设备的chains，访问接口类似于std::stack。
*/

/*
C++中将输入输出视为"流"(stream),即数据在其中"流动"的序列,数据处理即在流动中完成操作.
流处理的数据通常是字符类型(char或wchar_t),但也可以使用模板参数指定处理任意的类型.
boost.iostreams库建立在标准库的IO流框架基础之上,它定义了device,source,sink,filter等新的流处理概念,几乎可以用流
来处理任何数据.
总的来说,iostreams库包含如下的组成部分:
设备(device):包括流的起点source(源设备)和流的终点sink(接受设备),两者统称为设备.
过滤器(filter):包括读取时处理数据的输入过滤器和写入时处理数据的输出过滤器.
流(stream):用来连接设备和过滤器,让数据得以流动.

标准库中流框架：
ios_base是流的基类,它的子类basic_ios<>使用模板参数规定了流处理的数据类型和特性(traits),并依赖basic_streambuf<>
完成实际的读写操作.
basic_istream<>和basic_ostream<>分别从basic_ios<>虚继承,定义了输入输出流,最后的basic_iostream<>实现了既可
读又可写的双向流.
我们常用的cin,std::cout的类型分别是std::istream和std::ostream,而这两个类型则分别是basic_istream<>和basic_ostream<>的模板特化
形式,即:
typedef basic_istream<char, char_traits<char>> std::istream;
typedef basic_ostream<char, char_traits<char>> std::ostream;
*/

/*
示例演示“输入输出流的末端都是流而不是设备”：
char ar[]{"abc"};
//定义一个字符数组输入流
stream<array_source>sa(ar);

//一个计数过滤器
counter ct;

//定义一个空的输入过滤流
filtering_istream in;
assert(in.empty());

//加入计数过滤器，使用ref包装
in.push(ref(ct));
//链不完整
assert(!in.is_complete());
//加入输入流(在输入过滤流里面添加字符数组输入流)
in.push(sa);
//链完整
assert(in.is_complete());

//输出过滤流直接连接标准输出，没有添加过滤器
copy(in,filtering_ostream(cout));
*/

/*
流处理函数：
get():
从源设备或流中获取一个字符。
put():
向接收设备或流中写入一个字符。
read():
从源设备或流中获取多个字符。
write():
向接收设备或流中写入多个字符。
seek():
随机访问设备或流。
flush():
刷新设备或整个流，清空缓冲区。
close():
关闭设备或流。
*/

/*
定制设备：
iostreams库不仅仅是一个工具库，它更是一个框架。

定制源设备：
编写设备首先要满足设备的概念，具有char_type和category内部类型定义，对于源设备来说其模式必须为input。
device类在头文件"boost/iostreams/concepts.hpp"中定义了若干特化表示一些设备子概念，其中就包括为了
方便用户扩展而定义的source：
typedef device<input> source;				//char源设备
只要我们自定义的设备public继承source，那么设备就会自动满足源设备的概念。
源设备被用于输入，因此它需要实现一个如下形式的read()成员函数：
std::streamsize read(char_type* s,std::streamszie n);
read()函数读取最多n个字符到缓冲区s中，然后返回读取的字符数表示读取成功，返回EOF(-1)表示已经读取完毕。

source是char设备，如果需要其他类型的设备，就需要从device直接特化：
typedef device<input,unsigned char> unsigned_char_source;

定制接收设备：
typedef device<output> sink;				//char接收设备
std::streamsize write(const char_type* s,std::streamsize n);
write()从缓冲区s中读取最多n个字符，然后写入某个地方，返回写入的字符数。
*/

/*
定制过滤器：
编写过滤器必须符合filter的概念，它位于头文件"boost/iostreams/concepts.hpp"。
根据模式的不同，过滤器可以分为：输入过滤器，输出过滤器，两用过滤器，可定位过滤器。
根据字符的不同，过滤器又可以分为：单字符过滤器，多字符过滤器。

过滤器辅助类：
aggregate_filter:
两用过滤器，一次性接收所有的数据。
basic_line_filter:
两用过滤器，每次提取一行数据。
basic_stdio_filter:
两用过滤器，用于适配标准输入输出流。
symmetric_filter:
两用过滤器，带有内部缓冲区。

我们需要实现其纯虚函数do_filter()，在该函数中去编写适当的逻辑代码。
*/

/*
The device boost::iostreams::mapped_file, which loads a file partially or completely into 
memory. The stream boost::iostreams::stream can be connected to a device like 
boost::iostreams::mapped_file to use the familiar stream operators operator<< and 
operator>> to read and write data.

Devices:
A device is nothing more than a class with the member function read() or write(). A device 
can be connected with a stream so you can read and write formatted data rather than using 
the read() and write() member functions directly.

A device of type boost::iostreams::back_insert_device.This device can be used to write data 
to any container that provides the member function insert(). The device calls this member 
function to forward data to the container.

IOStreams also provides the device boost::iostreams::mapped_file_source to load a file 
partially or completely into memory. boost::iostreams::mapped_file_source defines a 
member function data() to receive a pointer to the respective memory area. That way, data 
can be randomly accessed in a file without having to read the file sequentially.

Devices boost::iostreams::file_descriptor_source and boost::iostreams::file_descriptor_sink.
These devices support read and write operations on platform-specific objects. On Windows 
these objects are handles, and on POSIX operating systems they are file descriptors.

The filter and the device are connected with the stream boost::iostreams::filtering_ostream. This class provides a member function push(), which the filter and the device are passed to.

The filter(s) must be passed before the device; the order is important. You can pass one or 
more filters, but once a device has been passed, the stream is complete, and you must not 
call push() again.

Please note that you must not use a stream that isn’t connected with a device.
*/

/*
2.1.1. Overview: Devices, stream_buffer and stream:
Writing a new stream or stream buffer class using the Boost Iostreams library is easy:
you simply write a class modeling the Device concept, then use that class as the template 
argument to stream or stream_buffer:

#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/stream_buffer.hpp>

namespace io = boost::iostreams;

class my_device {};

typedef io::stream<my_device> my_stream;
typedef io::stream_buffer<my_device> my_streambuf;

Here io::stream_buffer<my_device> is a derived class of std::basic_streambuf, and 
io::stream<my_device> is a derived class of std::basic_istream, std::basic_ostream or 
std::basic_iostream depending on the mode of my_device, i.e., depending on which of the 
fundamental i / o operations read, write and seek it supports.

The template io::stream is provided as a convenience.It's always possible to avoid io::stream 
and simply use io::stream_buffer together with one of the standard library stream templates.
E.g.,

#include <ostream>
#include <boost/iostreams/device/file.hpp>
#include <boost/iostreams/stream.hpp>

namespace io = boost::iostreams;

int main()
{
	io::stream_buffer<io::file_sink> buf("log.txt");
	std::ostream out(&buf);
	//out writes to log.txt
}
In this example, the ostream out uses the stream_buffer buf as its underlying data sink, so 
that data written to out goes to the file log.txt.The same effect could be achieved by default 
constructing out and telling it to use stream buffer buf by invoking out.rdbuf(&buf).

Another way to define a new stream or stream buffer class using the Boost Iostreams library 
is to derive from filtering_stream or filtering_streambuf.

The next three items will demonstrate how to write Devices for accessing STL - compatible 
containers.The source code for the examples can be found in the header 
<libs / iostreams / example / container_device.hpp>
*/