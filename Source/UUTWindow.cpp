#include "UUTWindow.h"

#ifdef UUT_HIERARCHICAL_COMPILE

#include "UUTTools.h"

#if UNG_DEBUGMODE
#define UUT_INFO																			\
const GLubyte* openGLVendor = glGetString(GL_VENDOR);				\
BOOST_ASSERT(openGLVendor);														\
const GLubyte* openGLVersion = glGetString(GL_VERSION);				\
BOOST_ASSERT(openGLVersion);													\
int openGLVersionMajor, openGLVersionMinor;									\
glGetIntegerv(GL_MAJOR_VERSION, &openGLVersionMajor);				\
glGetIntegerv(GL_MINOR_VERSION, &openGLVersionMinor);				\
BOOST_ASSERT(openGLVersionMajor >= 3);									\
BOOST_ASSERT(openGLVersionMinor >= 0);									\
String glslVersion = String(reinterpret_cast<const char*>(glGetString(GL_SHADING_LANGUAGE_VERSION))).substr(0, 4);
#else
#define UUT_INFO
#endif//UNG_DEBUGMODE

namespace ung
{
	WindowWrapper::~WindowWrapper()
	{
	}

	void WindowWrapper::createWindow(int argc, char ** argv, int width, int height, const char * title, int major, int minor)
	{
	}

	void WindowWrapper::setCallback(changeSizeType changeSize, renderType render, destroyWindowType destroyWindow)
	{
	}

	void WindowWrapper::enterLoop()
	{
	}

	void WindowWrapper::createWindow(GLFWwindow*& window, int width, int height, const char* title, int major, int minor)
	{
	}

	//-----------------------------------------------------------------------------------------------

	void FreeglutWrapper::createWindow(int argc, char** argv, int width, int height, const char* title, int major, int minor)
	{
		glutInit(&argc, argv);
		glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH | GLUT_STENCIL | GLUT_MULTISAMPLE);
		glutInitWindowSize(width, height);
		glutInitContextVersion(major, minor);
		glutInitContextProfile(GLUT_CORE_PROFILE);
		glutCreateWindow(title);

		GLEW_INIT;

		UUT_INFO;
	}

	void FreeglutWrapper::setCallback(changeSizeType changeSize, renderType render, destroyWindowType destroyWindow)
	{
		glutReshapeFunc(changeSize);
		glutDisplayFunc(render);
		glutCloseFunc(destroyWindow);
	}

	void FreeglutWrapper::enterLoop()
	{
		glutMainLoop();
	}

	//-----------------------------------------------------------------------------------------------

	void GlfwWrapper::createWindow(GLFWwindow*& window, int width, int height, const char* title, int major, int minor)
	{
		if (!glfwInit())
		{
			exit(EXIT_FAILURE);
		}

		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, major);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, minor);
		window = glfwCreateWindow(width, height, title, NULL, NULL);
		if (!window)
		{
			glfwTerminate();
			exit(EXIT_FAILURE);
		}

		glfwMakeContextCurrent(window);

		glfwSwapInterval(1);

		GLEW_INIT;

		UUT_INFO;
	}
}//namespace ung

#endif//UUT_HIERARCHICAL_COMPILE

/*
调用说明：
std::shared_ptr<WindowWrapperFactory> freeglutFactoryPtr = std::make_shared<FreeglutFactory>();
WindowWrapper* windowWrapperPtr = freeglutFactoryPtr->create<WindowWrapper>();
windowWrapperPtr->createWindow(argc, argv,512, 512,"Triangle",3, 1);
windowWrapperPtr->setCallback(reshape,display,destroyWindow);
//一些初始化工作
windowWrapperPtr->enterLoop();
*/

/*
示例：
#pragma comment(lib,"LuaPlus_53_d.lib")

//=====================================

#define BUFFER_OFFSET(x)  ((const void*) (x))

GLuint  vao1;
GLuint  buffer1;
GLuint textureID;

#pragma warning(disable:4838)
#pragma warning(disable:4305)

GLfloat vertices[] =
{
	-1.0f, -1.0f,
	1.0f, -1.0f,
	-1.0f,  1.0f,
	1.0, -1.0,
	1.0,  1.0,
	-1.0,  1.0
};

GLfloat coords[] =
{
	0.0f,0.0f,
	1.0f,0.0f,
	0.0f,1.0f,
	1.0f,0.0f,
	1.0f,1.0f,
	0.0f,1.0f
};

//hooks
void display()
{
	glClearColor(0.0, 1.0, 0.0, 1.0);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	glDrawArrays(GL_TRIANGLES, 0, 6);

	glutSwapBuffers();
}

void reshape(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, width, height);
}

void destroyWindow()
{
	ung::ThreadPool::getInstance().closePool();
}

void buildTexture()
{
	//纹理
	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_2D, textureID);

	//创建纹理
	String textureName{ "liang.jpg" };
	auto resPtr = ResourceFactory::getInstance().createResource("Image", textureName);
	resPtr->build();

	auto texturePtr = std::dynamic_pointer_cast<Image>(ResourcePool::getInstance().take(resPtr->getFullName()));
	BOOST_ASSERT(texturePtr);

	auto textureData = texturePtr->getData();
	GLsizei width = texturePtr->getWidthInPixels();
	GLsizei height = texturePtr->getHeightInPixels();
	uint8 bits = texturePtr->getBPP();
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR,
		GL_UNSIGNED_BYTE, textureData);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
}

void init()
{
	using namespace ung;

	auto programPtr = ShaderManager::getInstance().createProgram("triangles");
	GLuint programHandle = programPtr->getProgram();
	uutSetAttribLocation(programHandle, { "vPosition","vTexCoords" });
	ShaderManager::getInstance().linkProgram(programPtr);
	ShaderManager::getInstance().useProgram(programPtr);
	GLint iTextureUniform = glGetUniformLocation(programHandle, "colorMap");
	glUniform1i(iTextureUniform, 0);

	glGenVertexArrays(1, &vao1);
	glBindVertexArray(vao1);

	glGenBuffers(1, &buffer1);
	glBindBuffer(GL_ARRAY_BUFFER, buffer1);

	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices) + sizeof(coords), NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(vertices), sizeof(coords), coords);

	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
	glEnableVertexAttribArray(0);

	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeof(vertices)));
	glEnableVertexAttribArray(1);

	//初始化URM库
	urmInit();
}

WindowWrapper* windowWrapperPtr = nullptr;

void buildWindow(int argc,char** argv)
{
	std::shared_ptr<WindowWrapperFactory> freeglutFactoryPtr = std::make_shared<FreeglutFactory>();
	windowWrapperPtr = freeglutFactoryPtr->create<WindowWrapper>();
	windowWrapperPtr->createWindow(argc, argv, 512, 512, "Triangle", 3, 1);
	windowWrapperPtr->setCallback(reshape, display, destroyWindow);
}

void loop()
{
	windowWrapperPtr->enterLoop();
}

//-------------------------------------------------------------------------------------

#include "boost/interprocess/mapped_region.hpp"
#include "boost/interprocess/file_mapping.hpp"
#include <functional>
#include "boost/function.hpp"
#include "LuaPlus.h"
using namespace LuaPlus;

int main(int argc,char** argv)
{
	//LuaStateOwner state;
	LuaState* pLuaState = LuaState::Create();
	int iret = pLuaState->DoFile("test.lua");
	int mytest = pLuaState->GetGlobal("health").GetInteger();

	LuaState::Destroy(pLuaState);
	pLuaState = nullptr;
	//--------------------------------------------------------------------

	buildWindow(argc,argv);

	//--------------------------------------------------------------------

	init();

	buildTexture();

	//--------------------------------------------------------------------

	//--------------------------------------------------------------------

	loop();

	return 0;
}
*/

/*
示例：
static void error_callback(int error, const char* description)
{
	std::cerr << "Error:" << description << std::endl;
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GLFW_TRUE);
}

int main()
{
	using namespace ung;
	GLFWwindow* window = nullptr;
	uutGlfwWindow(window,640, 480, "My Title",3,1);

	while (!glfwWindowShouldClose(window))
	{
		float ratio;
		int width, height;
		glfwGetFramebufferSize(window, &width, &height);

		ratio = width / (float) height;
		glViewport(0, 0, width, height);

		glClear(GL_COLOR_BUFFER_BIT);

		glfwSwapBuffers(window);

		glfwPollEvents();
	}

	glfwDestroyWindow(window);

	glfwTerminate();

	exit(EXIT_SUCCESS);
}
*/