#include "USMProcess.h"

#ifdef USM_HIERARCHICAL_COMPILE

namespace ung
{
	Process::Process() :
		mState(INITIALIZED)
	{
	}

	Process::~Process()
	{
		if (mChild)
		{
			mChild->onAbort();
		}
	}

	void Process::pause()
	{
		//当前状态必须为RUNNING
		if (mState != RUNNING)
		{
			//抛出异常
		}

		mState = PAUSED;
	}

	void Process::unpause()
	{
		//当前状态必须为PAUSED
		if (mState != PAUSED)
		{
			//抛出异常
		}

		mState = RUNNING;
	}

	void Process::succeed()
	{
		//当前状态必须为RUNNING或PAUSED
		if (mState != RUNNING && mState != PAUSED)
		{
			//抛出异常
		}

		//设置状态为结束
		mState = SUCCEEDED;
	}

	bool Process::isPaused() const
	{
		return mState == PAUSED;
	}

	bool Process::isAlive() const
	{
		return (mState == RUNNING || mState == PAUSED);
	}

	bool Process::isDead() const
	{
		return (mState == SUCCEEDED || mState == ABORTED);
	}

	void Process::attachChild(StrongProcessPtr pProcess)
	{
		//如果当前child为空
		if (!mChild)
		{
			mChild = pProcess;
		}
		else
		{
			//让参数child为当前child的child
			mChild->attachChild(pProcess);
		}
	}

	Process::StrongProcessPtr Process::removeChild()
	{
		if (mChild)
		{
			StrongProcessPtr pChild = mChild;
			mChild.reset();

			return pChild;
		}

		return StrongProcessPtr();
	}

	Process::StrongProcessPtr Process::getChild()
	{
		return mChild;
	}

	Process::State Process::getState() const
	{
		return mState;
	}

	void Process::onInit()
	{
		mState = RUNNING;
	}

	void Process::onSuccess()
	{
	}

	void Process::onAbort()
	{
	}

	void Process::setState(State newState)
	{
		mState = newState;
	}
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE

/*
Data-Driven Processes:
If you plan on using processes as the core of your game, you should have a data format that 
lets you define chains of processes and dependencies. At Super-Ego Games, we used XML to 
define our process chains and how they all fit together. It allowed us to set up complex game 
logic without having to touch a single line of code.

More Uses of Process Derivatives:
Every updatable game object(每个可更新的游戏对象) can inherit from Process.
User interface objects such as buttons, edit boxes, or menus can inherit from Process.
Audio objects such as sound effects, speech, or music make great use of this design because 
of the dependency and timing features.
*/