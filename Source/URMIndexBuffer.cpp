#include "URMIndexBuffer.h"

#ifdef URM_HIERARCHICAL_COMPILE

#include "URMShadowBuffer.h"

namespace ung
{
	IndexBuffer::IndexBuffer() :
		mIndexType(IndexType::IT_16BIT),
		mNumIndexes(0)
	{
	}

	IndexBuffer::IndexBuffer(IndexType idxType, uint32 numIndexes, uint32 usage, bool useShadowBuffer) :
		VideoBuffer(usage, useShadowBuffer),
		mIndexType(idxType),
		mNumIndexes(numIndexes)
	{
		switch (mIndexType)
		{
		case IndexType::IT_16BIT:
			mIndexSize = sizeof(uint16);
			break;
		case IndexType::IT_32BIT:
			mIndexSize = sizeof(uint32);
			break;

		default:
			UNG_EXCEPTION("Invalid index type.");
		}

		mSizeInBytes = mIndexSize * mNumIndexes;

		if (mUseShadowBuffer)
		{
			mShadowBuffer = UNG_NEW ShadowVertexBuffer(mIndexSize,mNumIndexes);
		}
	}

	IndexBuffer::~IndexBuffer()
	{
		if (mUseShadowBuffer)
		{
			UNG_DEL(mShadowBuffer);
		}
	}

	IndexType IndexBuffer::getType() const
	{
		return mIndexType;
	}

	uint32 IndexBuffer::getNumIndexes() const
	{
		return mNumIndexes;
	}

	uint32 IndexBuffer::getIndexSize() const
	{
		return mIndexSize;
	}
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE