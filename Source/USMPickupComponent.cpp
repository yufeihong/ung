#include "USMPickupComponent.h"

#ifdef USM_HIERARCHICAL_COMPILE

namespace ung
{
	PickupComponent::PickupComponent(String const& name) :
		ObjectComponent(name)
	{
	}

	PickupComponent::~PickupComponent()
	{
	}
}//namespace ung

#endif//USM_HIERARCHICAL_COMPILE