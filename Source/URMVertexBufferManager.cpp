#include "URMVertexBufferManager.h"

#ifdef URM_HIERARCHICAL_COMPILE

#include "URMVertexElement.h"
#include "URMVertexDeclaration.h"

namespace ung
{
	VertexBufferManager::VertexBufferManager()
	{
	}

	VertexBufferManager::~VertexBufferManager()
	{
	}

	VertexElement* VertexBufferManager::createElement(uint8 stream, uint8 offset, VertexElementSemantic semantic, VertexElementType theType, uint8 index)
	{
		return UNG_NEW VertexElement(stream, offset, semantic, theType, index);
	}

	void VertexBufferManager::setDeclaration(VertexDeclaration* pDecl)
	{
		BOOST_ASSERT(pDecl);

		pDecl->set();
	}
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE