#include "URMInit.h"

#ifdef URM_HIERARCHICAL_COMPILE

#include "URMResourceFactory.h"
#include "URMImage.h"
#include "URMMaterial.h"
#include "URMMesh.h"

namespace ung
{
	void UNG_STDCALL urmInit()
	{
		UNG_LOG_START("Initialize URM.");

		/*
		在工厂方法中，对资源进行注册。
		*/
		ResourceFactory::getInstance().registerCreator("Image",Image::creatorCallback);
		ResourceFactory::getInstance().registerCreator("Material",Material::creatorCallback);
		ResourceFactory::getInstance().registerCreator("Mesh",Mesh::creatorCallback);

		UNG_LOG_END("Initialize URM.");
	}
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE