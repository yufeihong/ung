/*
Rounding Functions:
Rounding(四舍五入) Truncation and Integer Conversion:
#include <boost/math/special_functions/round.hpp>
template <class T>
T round(const T& v);

template <class T, class Policy>
T round(const T& v, const Policy&);

template <class T>
int iround(const T& v);

template <class T, class Policy>
int iround(const T& v, const Policy&);

template <class T>
long lround(const T& v);

template <class T, class Policy>
long lround(const T& v, const Policy&);

template <class T>
long long llround(const T& v);

template <class T, class Policy>
long long llround(const T& v, const Policy&);
These functions return the closest integer to the argument v.
Halfway cases are rounded away from zero, regardless of the current rounding direction.
If the argument v is either non-finite or else outside the range of the result type, then returns the result of rounding_error: by 
default this throws an instance of boost::math::rounding_error.

Truncation Functions:
#include <boost/math/special_functions/trunc.hpp>
template <class T>
T trunc(const T& v);

template <class T, class Policy>
T trunc(const T& v, const Policy&);

template <class T>
int itrunc(const T& v);

template <class T, class Policy>
int itrunc(const T& v, const Policy&);

template <class T>
long ltrunc(const T& v);

template <class T, class Policy>
long ltrunc(const T& v, const Policy&);

template <class T>
long long lltrunc(const T& v);

template <class T, class Policy>
long long lltrunc(const T& v, const Policy&);
The trunc functions round their argument to the integer value, nearest to but no larger in magnitude than the argument.
For example itrunc(3.7) would return 3 and ltrunc(-4.6) would return -4.
If the argument v is either non-finite or else outside the range of the result type, then returns the result of rounding_error: by 
default this throws an instance of boost::math::rounding_error.

Integer and Fractional Part(小数部分) Splitting:
#include <boost/math/special_functions/modf.hpp>
template <class T>
T modf(const T& v, T* ipart);

template <class T, class Policy>
T modf(const T& v, T* ipart, const Policy&);

template <class T>
T modf(const T& v, int* ipart);

template <class T, class Policy>
T modf(const T& v, int* ipart, const Policy&);

template <class T>
T modf(const T& v, long* ipart);

template <class T, class Policy>
T modf(const T& v, long* ipart, const Policy&);

template <class T>
T modf(const T& v, long long* ipart);

template <class T, class Policy>
T modf(const T& v, long long* ipart, const Policy&);
The modf functions store the integer part of v in *ipart and return the fractional part of v. The sign of the integer and fractional parts 
are the same as the sign of v.
If the argument v is either non-finite or else outside the range of the result type, then returns the result of rounding_error: by 
default this throws an instance of boost::math::rounding_error.

Floating-Point Classification(分类): Infinities and NaNs:
Synopsis
#include <boost/math/special_functions/fpclassify.hpp>
#define FP_ZERO								//implementation specific value
#define FP_NORMAL							//implementation specific value
#define FP_INFINITE							//implementation specific value
#define FP_NAN								//implementation specific value
#define FP_SUBNORMAL					//implementation specific value

template <class T>
int fpclassify(T t);

template <class T>
bool isfinite(T z);									//Neither infinity nor NaN.

template <class T>
bool isinf(T t);										//Infinity (+ or -).

template <class T>
bool isnan(T t);										//NaN.

template <class T>
bool isnormal(T t);								//isfinite and not denormalised.
Description:
These functions provide the same functionality as the macros with the same name in C99, indeed(的确) if the C99 macros are 
available, then these functions are implemented in terms of them, otherwise they rely on std::numeric_limits<> to function.
Note that the definition of these functions does not suppress(压制) the definition of these names as macros by math.h on those 
platforms that already provide these as macros. That mean that the following have differing meanings:
using namespace boost::math;

//This might call a global macro if defined,but might not work if the type of z is unsupported by the std lib macro:
isnan(z);

//This calls the Boost version,(found via the "using namespace boost::math" declaration),it works for any type that has 
//numeric_limits support for type z:
(isnan)(z);

//As above but with explicit namespace qualification.
(boost::math::isnan)(z);

//This will cause a compiler error if isnan is a native macro:
boost::math::isnan(z);
//So always use instead:
(boost::math::isnan)(z);

//You can also add a using statement,globally to a .cpp file, or to a local function in a .hpp file.
using boost::math::isnan;
//So you can write the shorter and less cluttered
(isnan)(z)
//But, as above, if isnan is a native macro, this causes a compiler error,because the macro always 'gets' the name first, unless 
//enclosed in () brackets.

template <class T>
int fpclassify(T t);
Returns an integer value that classifies the value t:
FP_ZERO
If t is zero.

FP_NORMAL
If t is a non-zero, non-denormalised finite value.

FP_INFINITE
If t is plus or minus infinity.

FP_NAN
If t is a NaN.

FP_SUBNORMAL
If t is a denormalised number.

template <class T>
bool isfinite(T z);
Returns true only if z is not an infinity or a NaN.

template <class T>
bool isinf(T t);
Returns true only if z is plus or minus infinity.

template <class T>
bool isnan(T t);
Returns true only if z is a NaN.

template <class T>
bool isnormal(T t);
Returns true only if z is a normal number (not zero, infinite, NaN, or denormalised).

Sign Manipulation Functions:
Synopsis:
#include <boost/math/special_functions/sign.hpp>
namespace boost
{
	namespace math
	{
		template<class T>
		int signbit(T x);

		template <class T>
		int sign (const T& z);

		template <class T, class U>
		T copysign (const T& x, const U& y);

		template <class T>
		calculated-result-type changesign (const T& z);
	}//namespace math
}//namespaces boost
Description:
template<class T>
int signbit(T x);
Returns a non-zero value if the sign bit is set in variable x,otherwise 0.Important:The return value from this function is zero or 
not-zero and not zero or one(零或者非零和非零或者一).
One of the bits in the binary representation of a floating-point number gives the sign, and the remaining bits give the absolute 
value. That bit is known as the sign bit. The sign bit is set = 1 for negative numbers, and is not set = 0 for positive numbers.

template <class T>
int sign (const T& z);
Returns 1 if x > 0, -1 if x < 0, and 0 if x is zero.

template <class T, class U>
calculated-result-type copysign (const T& x, const U& y);
Sets the sign of x to be the same as the sign of y.

template <class T>
T changesign (const T& z);
Returns a floating-point number with a binary representation where the signbit is the opposite of the sign bit in x, and where the 
other bits are the same as in x.
This function is widely available, but not specified in any standards.Rationale: Not specified by TR1, but changesign(x) is both 
easier to read and more efficient than copysign(x, signbit(x) ? 1.0 : -1.0);For finite values, this function has the same effect as 
simple negation, the assignment z = -z, but for nonfinite values, infinities and NaNs, the changesign(x) function may be the only 
portable way to ensure that the sign bit is changed.

Examples:
signbit(3.5)							is zero (or false)
signbit(-7.1)							is 1 (or true)
copysign(4.2, 7.9)					is 4.2
copysign(3.5 -1.4)				is -3.5
copysign(-4.2, 1.0)				is 4.2
copysign(-8.6, -3.3)				is -8.6
changesign(6.9)					is -6.9
changesign(-1.8)					is 1.8

Floating-Point Representation Distance (ULP), and Finding Adjacent Floating-Point Values:
Unit of Least Precision or Unit in the Last Place is the gap between two different, but as close as possible, floating-point numbers.
Most decimal values, for example 0.1, cannot be exactly represented as floating-point values, but will be stored as the closest 
representable floating-point.
Functions are provided for finding adjacent greater and lesser floating-point values, and estimating the number of gaps between 
any two floating-point values.
Finding the Next Representable Value in a Specific Direction (nextafter):
Synopsis:
#include <boost/math/special_functions/next.hpp>
namespace boost
{
	namespace math
	{
		template <class FPT>
		FPT nextafter(FPT val, FPT direction);
	}//namespaces math
}//namespaces boost
Tip:
Nearly always, you just want the next or prior representable value, so instead use float_next or float_prior below.
Examples - nextafter
The two representations using a 32-bit float either side of unity are:
The nearest (exact) representation of 1.F is				1.00000000
nextafter(1.F, 999) is												1.00000012
nextafter(1/f, -999) is												0.99999994

The nearest (not exact) representation of 0.1F is		0.100000001
nextafter(0.1F, 10) is												0.100000009
nextafter(0.1F, 10) is												0.099999994

Finding the Next Greater Representable Value (float_next):
Synopsis:
#include <boost/math/special_functions/next.hpp>
namespace boost
{
	namespace math
	{
		template <class FPT>
		FPT float_next(FPT val);
	}//namespaces math
}//namespaces boost
Description - float_next:
Returns the next representable value which is greater than x. If x is non-finite then returns the result of a domain_error. If there 
is no such value greater than x then returns an overflow_error.
Has the same effect as:
nextafter(val,(std::numeric_limits<FPT>::max)());

Finding the Next Smaller Representable Value (float_prior):
Synopsis:
#include <boost/math/special_functions/next.hpp>
namespace boost
{
	namespace math
	{
		template <class FPT>
		FPT float_prior(FPT val);
	}//namespaces math
}//namespaces boost
Description - float_prior:
Returns the next representable value which is less than x. If x is non-finite then returns the result of a domain_error. If there is 
no such value less than x then returns an overflow_error.
Has the same effect as:
nextafter(val, -(std::numeric_limits<FPT>::max)());//Note most negative value -max.

Calculating the Representation Distance Between Two floating-point Values (ULP) float_distance
Function float_distance finds the number of gaps/bits/ULP between any two floating-point values. If the significands(尾数) of 
floating-point numbers are viewed as integers, then their difference is the number of ULP/gaps/bits different.
Synopsis:
#include <boost/math/special_functions/next.hpp>
namespace boost
{
	namespace math
	{
		template <class FPT>
		FPT float_distance(FPT a, FPT b);
	}//namespaces math
}//namespaces boost
Description - float_distance:
Returns the distance between a and b: the result is always a signed integer value (stored in floating-point type FPT) representing 
the number of distinct representations between a and b.
Note that:
float_distance(a, a)							always returns 0.
float_distance(float_next(a), a)			always returns -1.
float_distance(float_prior(a), a)		always returns 1.
The function float_distance is equivalent to calculating the number of ULP (Units in the Last Place) between a and b except that it 
returns a signed value indicating whether a > b or not.
If the distance is too great then it may not be able to be represented as an exact integer by type FPT, but in practice this is 
unlikely to be a issue.

Advancing a floating-point Value by a Specific Representation Distance (ULP) float_advance:
Function float_advance advances a floating-point number by a specified number of ULP.
Synopsis:
#include <boost/math/special_functions/next.hpp>
namespace boost
{
	namespace math
	{
		template <class FPT>
		FPT float_advance(FPT val, int distance);
	}//namespaces math
}//namespaces boost
Description - float_advance:
Returns a floating-point number r such that float_distance(val, r) == distance.

Obtaining(获取) the Size of a Unit In the Last Place - ULP:
Function ulp gives the size of a unit-in-the-last-place for a specified floating-point value.
Synopsis:
#include <boost/math/special_functions/ulp.hpp>
namespace boost
{
	namespace math
	{
		template <class FPT>
		FPT ulp(const FPT& x);
		template <class FPT, class Policy>
		FPT ulp(const FPT& x, const Policy&);
	}//namespaces math
}//namespaces boost
Description - ulp:
Returns one unit in the last place of x.
Corner cases are handled as followes:
If the argument is a NaN, then raises a domain_error.
If the argument is an infinity, then raises an overflow_error.
If the argument is zero then returns the smallest representable value: for example for type double this would be either 
std::numeric_limits<double>::min() or std::numeric_limits<double>::denorm_min() depending whether denormals are 
supported (which have the values 2.2250738585072014e-308 and 4.9406564584124654e-324 respectively).
If the result is too small to represent, then returns the smallest representable value.
Always returns a positive value such that ulp(x) == ulp(-x).
The function is asymetrical(不对称), which is to say, given u = ulp(x) if x > 0 then x + u is the next floating-point value, 
but x - u is not necessarily the previous value. Similarly, if x < 0 then x - u is the previous floating-point value, but x + u is not 
necessarily the next value. The corner cases occur at power of 2 boundaries.
When the argument becomes very small, it may be that there is no floating-point value that represents one ULP. Whether this is 
the case or not depends not only on whether the hardware may sometimes support denormals (as signalled by 
std::numeric_limits<FPT>::has_denorm), but also whether these are currently enabled at runtime (for example on SSE hardware,
the DAZ or FTZ flags will disable denormal support). In this situation, the ulp function may return a value that is many orders of 
magnitude too large.
In light of the issues above, we recomend that:
To move between adjacent floating-point values always use float_next, float_prior or nextafter (std::nextafter is another 
candidate, but our experience is that this also often breaks depending which optimizations and hardware flags are in effect).
To move several floating-point values away use float_advance.
To calculate the edit distance between two floats use Boost.MathImpl float_distance.
There is none the less, one important use case for this function:
If it is known that the true result of some function is xt and the calculated result is xc, then the error measured in ulp is simply 
fabs(xt - xc) / ulp(xt).
*/

/*
Floating-point Comparison:
Comparison of floating-point values has always been a source of endless difficulty and confusion.
Unlike integral values that are exact, all floating-point operations will potentially produce an inexact result that will be rounded to 
the nearest available binary representation. Even apparently inocuous(无害的) operations such as assigning 0.1 to a double 
produces an inexact result (as this decimal number has no exact binary representation).
Floating-point computations also involve rounding so that some 'computational noise' is added, and hence results are also not 
exact (although repeatable, at least under identical platforms and compile options).
Boost provides a number of ways to compare floating-point values to see if they are tolerably(还算) close enough to each other, but 
first we must decide what kind of comparison we require:
1,Absolute difference/error: the absolute difference between two values a and b is simply fabs(a-b).
2,The edit distance between the two values: i.e. how many (binary) floating-point values are between two values a and b? This is 
provided by the function Boost.MathImpl float_distance, but is probably only useful when you know that the distance should be very 
small. This function is somewhat(有些) difficult to compute, and doesn't scale to values that are very far apart. In other words, 
use with care.
3,The relative distance/error between two values. This is quick and easy to compute, and is generally(通常) the method of choice 
when checking that your results are "tolerably close" to one another. However, it is not as exact as the edit distance when 
dealing with small differences, and due to(由于) the way floating-point values are encoded(编码) can "wobble"(摇晃) by a factor of 
2 compared to the "true" edit distance. This is the method documented below: if float_distance is a surgeon's scalpel(一个外科医生
的手术刀), then relative_difference is more like a Swiss army knife: both have important but different use cases.
Relative Comparison of Floating-point Values:
#include <boost/math/special_functions/relative_difference.hpp>

template <class T, class U>
calculated-result-type relative_distance(T a, U b);

template <class T, class U>
calculated-result-type epsilon_distance(T a, U b);

The function relative_distance returns the relative distance/error E between two values as defined by:
E = fabs((a - b) / min(a,b))
The function epsilon_difference is a convenience function that returns relative_distance(a, b) / eps where eps is the machine 
epsilon for the result type.
The following special cases are handled as follows:
If either of a or b is a NaN, then returns the largest representable value for T: for example for type double, this is 
std::numeric_limits<double>::max() which is the same as DBL_MAX or 1.7976931348623157e+308.
If a and b differ in sign then returns the largest representable value for T.
If both a and b are both infinities (of the same sign), then returns zero.
If just one of a and b is an infinity, then returns the largest representable value for T.
If both a and b are zero then returns zero.
If just one of a or b is a zero or a denormalized value, then it is treated as if it were the smallest (non-denormalized) value 
representable in T for the purposes of the above calculation.
When comparing values that are quite close or approximately equal, we could use either float_distance or 
relative_difference/epsilon_difference, for example with type float, these two values are adjacent to each other:
std::cout.precision(std::numeric_limits<float>::max_digits10);
std::cout << std::boolalpha << std::showpoint << std::endl;

float a = 1;
float b = 1 + std::numeric_limits<float>::epsilon();
std::cout << "a = " << a << std::endl;
std::cout << "b = " << b << std::endl;
std::cout << "float_distance = " << float_distance(a, b) << std::endl;
std::cout << "relative_difference = " << relative_difference(a, b) << std::endl;
std::cout << "epsilon_difference = " << epsilon_difference(a, b) << std::endl;

Which produces the output:
a = 1.00000000
b = 1.00000012
float_distance = 1.00000000
relative_difference = 1.19209290e-007
epsilon_difference = 1.00000000

In the example above, it just so happens that the edit distance as measured by float_distance, and the difference measured in 
units of epsilon were equal. However, due to the way floating point values are represented, that is not always the case:
a = 2.0f / 3.0f;													//2/3 inexactly represented as a float
b = float_next(float_next(float_next(a)));			//3 floating point values above a
std::cout << "a = " << a << std::endl;
std::cout << "b = " << b << std::endl;
std::cout << "float_distance = " << float_distance(a, b) << std::endl;
std::cout << "relative_difference = " << relative_difference(a, b) << std::endl;
std::cout << "epsilon_difference = " << epsilon_difference(a, b) << std::endl;
Which produces the output:
a = 0.666666687
b = 0.666666865
float_distance = 3.00000000								//(位距离)
relative_difference = 2.68220901e-007
epsilon_difference = 2.25000000
There is another important difference between float_distance and the relative_difference/epsilon_difference functions in that 
float_distance returns a signed result that reflects which argument is larger in magnitude, where as 
relative_difference/epsilon_difference simply return an unsigned value that represents how far apart(相隔多远) the values are. For 
example if we swap the order of the arguments:
std::cout << "float_distance = " << float_distance(b, a) << std::endl;
std::cout << "relative_difference = " << relative_difference(b, a) << std::endl;
std::cout << "epsilon_difference = " << epsilon_difference(b, a) << std::endl;
The output is now:
float_distance = -3.00000000
relative_difference = 2.68220901e-007
epsilon_difference = 2.25000000

Zeros are always treated as equal, as are infinities as long as they have the same sign:
a = 0;
b = -0;				//signed zero
std::cout << "relative_difference = " << relative_difference(a, b) << std::endl;
a = b = std::numeric_limits<float>::infinity();
std::cout << "relative_difference = " << relative_difference(a, b) << std::endl;
std::cout << "relative_difference = " << relative_difference(a, -b) << std::endl;
Which produces the output:
relative_difference = 0.000000000
relative_difference = 0.000000000
relative_difference = 3.40282347e+038
Note that finite values are always infinitely far away from infinities even if those finite values are very large:
a = (std::numeric_limits<float>::max)();
b = std::numeric_limits<float>::infinity();
std::cout << "a = " << a << std::endl;
std::cout << "b = " << b << std::endl;
std::cout << "relative_difference = " << relative_difference(a, b) << std::endl;
std::cout << "epsilon_difference = " << epsilon_difference(a, b) << std::endl;
Which produces the output:
a = 3.40282347e+038
b = 1.#INF0000
relative_difference = 3.40282347e+038
epsilon_difference = 3.40282347e+038

Finally, all denormalized values and zeros are treated as being effectively equal:
a = std::numeric_limits<float>::denorm_min();
b = a * 2;
std::cout << "a = " << a << std::endl;
std::cout << "b = " << b << std::endl;
std::cout << "float_distance = " << float_distance(a, b) << std::endl;
std::cout << "relative_difference = " << relative_difference(a, b) << std::endl;
std::cout << "epsilon_difference = " << epsilon_difference(a, b) << std::endl;
a = 0;
std::cout << "a = " << a << std::endl;
std::cout << "b = " << b << std::endl;
std::cout << "float_distance = " << float_distance(a, b) << std::endl;
std::cout << "relative_difference = " << relative_difference(a, b) << std::endl;
std::cout << "epsilon_difference = " << epsilon_difference(a, b) << std::endl;
Which produces the output:
a = 1.40129846e-045
b = 2.80259693e-045
float_distance = 1.00000000								//b = a * 2(位距离)
relative_difference = 0.000000000
epsilon_difference = 0.000000000
a = 0.000000000
b = 2.80259693e-045
float_distance = 2.00000000
relative_difference = 0.000000000
epsilon_difference = 0.000000000

Handling Absolute Errors
Imagine we're testing the following function:
double myspecial(double x)
{
	return sin(x) - sin(4 * x);
}
This function has multiple roots, some of which are quite predicable in that both sin(x) and sin(4x) are zero together. Others 
occur because the values returned from those two functions precisely cancel out. At such points the relative difference between 
the true value of the function and the actual value returned may be arbitrarily large due to cancellation error.
In such a case, testing the function above by requiring that the values returned by relative_error or epsilon_error are below some 
threshold is pointless: the best we can do is to verify that the absolute difference between the true and calculated values is below 
some threshold.
Of course, determining what that threshold should be is often tricky, but a good starting point would be machine epsilon multiplied
by the largest of the values being summed. In the example above, the largest value returned by sin(whatever) is 1, so simply 
using machine epsilon as the target for maximum absolute difference might be a good start (though in practice we may need a 
slightly higher value - some trial and error will be necessary).
*/

/*
Specified-width floating-point typedefs:
Overview:
The header <boost/cstdfloat.hpp> provides optional standardized floating-point typedefs having specified widths. These are 
useful for writing portable code because they should behave identically on all platforms. These typedefs are the floating-point 
analog(类似) of specified-width integers in <cstdint> and stdint.h.
The typedefs include float16_t, float32_t, float64_t, float80_t, float128_t, their corresponding least and fast types, and the 
corresponding maximum-width type. The typedefs are based on underlying built-in types such as float, double, or long double, or 
based on other compiler-specific non-standardized types such as __float128. The underlying types of these typedefs must 
conform with the corresponding specifications of binary16, binary32, binary64, and binary128 in IEEE_floating_point floating-point 
format.
The 128-bit floating-point type (of great interest(极大兴趣) in scientific and numeric programming(科学和数字编程)) is not required 
in the Boost header, and may not be supplied for all platforms/compilers, because compiler support for a 128-bit floating-point 
type is not mandated(要求,命令) by either the C standard or the C++ standard.
Note:
<boost/cstdfloat.hpp> cannot synthesize or create a typedef if the underlying type is not provided by the compiler. For example, 
if a compiler does not have an underlying floating-point type with 128 bits (highly sought-after in scientific and numeric 
programming), then float128_t and its corresponding least and fast types are not provided by <boost/cstdfloat.hpp>.
Warning:
If <boost/cstdfloat.hpp> uses a compiler-specific non-standardized type (not derived from float, double, or long double) for one 
or more of its floating-point typedefs, then there is no guarantee that specializations of numeric_limits<> will be available for 
these types. Typically, specializations of numeric_limits<> will only be available for these types if the compiler itself supports 
corresponding specializations for the underlying type(s), exceptions are GCC's __float128 type and Intel's _Quad type which are 
explicitly supported via our own code.
Tip:
For best results, <boost/cstdfloat.hpp> should be #included before other headers that define generic code making use of 
standard library functions defined in <cmath>.
This is because <boost/cstdfloat.hpp> may define overloads of standard library functions where a non-standard type (i.e. other 
than float, double, or long double) is used for one of the specified width types. If generic code (for example in another Boost.MathImpl
header) calls a standard library function, then the correct overload will only be found if these overloads are defined prior to the 
point of use. See implementation for more details.
For this reason, making #include <boost/cstdfloat.hpp> the first include is usually best.
*/