/*
可以配合内存池使用。
*/

/*
checked_delete库是对C++关键字delete的增强,可以在编译期保证delete或delete[]删除的是一个指向"完整类"(complete type)的指针,避免在
运行时发生未定义行为.
checked_delete并不完全等同于delete关键字,它在delete操作之外增加了对不完整类型的检查.所谓不完整类型(incomplete type)是指仅有声明
而没有定义的类,它通常见于类的前向声明.对一个不完整类执行delete操作会导致析构函数未被执行,引发未定义行为.例如:
class demo_class;																	//前向声明,只有声明而没有具体定义

void do_delete(demo_class* p)
{
	delete p;
}

class demo_class																		//这里是类的完整定义
{
	...
};

int main()
{
	demo_class* p = new demo_class();
	do_delete(p);																		//将导致未定义行为,不会调用析构函数,因为demo_class是一个前向声明的不完整类,此时编译器还不知道它的析构函数.

	return 0;
}
使用checked_delete可以避免这种问题,如果要删除的指针指向的不是一个完整类型,将引发编译错误.

typedef char type_must_be_complete[sizeof(T) ? 1:-1];
(void) sizeof(type_must_be_complete);
通过typedef定义了一个数组类型,其大小由要被删除的类型T确定.如果T是一个完整类型,那么sizeof(T)表达式结果是一个正整数,数组大小为1,如果
T是一个不完整类型,那么sizeof(T)表达式结果是0,数组大小为-1.但C++中数组的定义是不允许为负数的,所以会引发一个编译错误.
*/

/*
delete之后重置指针值
当我们delete一个指针后，指针值就变为无效了。虽然指针已经无效，但在很多机器上指针仍然保存着(已经释放了的)动态内存的地址。在delete之后，指针就变成了空悬
指针(dangling pointer)，即，指向一块曾经保存数据对象但现在已经无效的内存的指针。
未初始化指针的所有缺点空悬指针也都有。有一种方法可以避免空悬指针的问题：在指针即将要离开其作用域之前释放掉它所关联的内存。这样，在指针关联的内存被释放掉
之后，就没有机会继续使用指针了。如果我们需要保留指针，可以在delete之后将nullptr赋予指针，这样就清楚地指出指针不指向任何对象。这只是提供了有限的保护，动态
内存的一个基本问题是可能有多个指针指向相同的内存。在delete内存之后重置指针的方法只对这个指针有效，对其他仍指向（已释放的）内存的指针是没有作用的。
*/

/*
基于Policy的Class设计
具备复杂功能的policy-based class由许多小型classes（称为policies）组成。每一个这样的小型class都只负责单纯如行为或结构的某一方面（behavioral or structural aspect）。
一个policy会针对特定主题建立一个接口。

软件设计的多样性
所谓软件设计就是解域空间（solution space）中的一道选择题。

一个Smart Pointer，这种class可被用于单线程或多线程之中，可以运用不同的ownership策略，可以在安全与速度之间协调，可以支持或不支持“自动转为内部指针”，以上
这些特性都可以被自由组合起来，而所谓解答，就是最适合你的应用程序的那个方案。

程序库设计者必须将各种常见情况加以分类融合，并给出一个开放架构，如此一来，应用程序员便能根据个别情况组合出他所需要的功能。

Templates带来曙光
templates是一种很适合“组合各种行为”的机制，主要因为它们是“依赖使用者提供的型别信息”并且“在编译期产生”的代码。

和一般的class不同，class templates可以以不同的方式定制。如果想要针对特定情况来设计class，你可以在你的class template中特化其成员函数来因应。
对于带有多个参数的class templates，你可以采用partial template specialization(偏特化)。它可以让你根据部分参数来特化一个class template。但是无法特化数据成员。
可以对“单一template参数”的class template特化其成员函数，却无法对着“多个template参数”的class template特化其个别成员函数。
class template的作者可以对每个成员函数提供一份缺省实作品，却不能对同一个成员函数提供多份缺省实作品。

Policies和Policy Classes
Policies和Policy Classes有助于我们设计出安全，有效率且具高度弹性的“设计元素”。所谓policy，乃用来定义一个class或class template的接口，该接口由下列项目
之一或全部组成：
内隐型别定义，成员函数和成员变量。
Policies也被用于traits，不同的是后者比较重视行为而非型别。
举个例子：让我们定义一个Policy用以生成对象：Creator Policy是一个带有型别T的class template，它必须提供一个名为Create的函数给外界使用，此函数不接受引数，
传回一个pointer to T。就语义而言，每当Create()被调用就必须传回一个指针，指向新生的T对象。至于对象的精确生成模式，留给policy实作品作为回旋余地。

让我们来定义一个可实作出Creator Policy的class：
template<typename T>
struct OpNewCreator
{
	static T* Create()
	{
		return new T;
	}
};

template<typename T>
struct MallocCreator
{
	static T* Create()
	{
		void* p = std::malloc(sizeof(T));
		if (!p)
		{
			return nullptr;
		}

		return new(p) T;
	}
};

template<typename T>
struct PrototypeCreator
{
	PrototypeCreator(T* pObj = nullptr) :
		pPrototype(pObj)
	{
	}

	T* GetPrototype()
	{
		return pPrototype;
	}

	void SetPrototype(T* pObj)
	{
		pPrototype = pObj;
	}

	T* Create()
	{
		return pPrototype ? pPrototype->Clone() : nullptr;
	}

private:
	T* pPrototype;
};
任何一个policy都可以有无限多份实作品。实作出policy者便称为policy classes，这个东西并不意图被单独使用，它们主要用于继承或被内含于其他classes。

现在我们看看如何设计一个class得以利用Creator policy，它以复合或继承的方式使用先前所定义的三个classes之一：
//Library code
template<typename CreationPolicy>
class WidgetManager : public CreationPolicy
{
...
};
如果一个class采用一个或多个policies，我们称其为hosts。hosts负责把policies提供的结构和行为组成一个更复杂的结构和行为。
当客户端将WidgetManager template具现化时，必须传进一个他所期望的policy：
//Application code
typedef WidgetManager<OpNewCreator<Widget>> MyWidgetMgr;
让我们来分析，无论何时，当一个MyWidgetMgr对象需要产生一个Widget对象时，它便调用它的policy子对象OpNewCreator<Widget>所提供的Create()。选择
“生成策略”是WidgetManager使用者的权利。藉由这样的设计，可以让WidgetManager使用者自行装配他所需要的机能。
这便是policy-based class的设计主旨。
运用template template参数：
//Library code
template<template<typename Created>class CreationPolicy>
class WidgetManager : public CreationPolicy<Widget>
{
...
};
Created是CreationPolicy的参数，CreationPolicy是WidgetManager的参数。
Widget已经写入上述程序库中，所以使用时不需要再传一次参数给policy。
Created是CreationPolicy的形参，可以省略。
template<template<typename>class CreationPolicy>
class WidgetManager : public CreationPolicy<Widget>
{
...
};
//Application code
typedef WidgetManager<OpNewCreator> MyWidgetMgr;
搭配policy class使用“template template参数”，并不单纯只为了方便。有时候这种用法不可或缺，以便host class可藉由templates产生不同型别的对象。
举个例子，假设WidgetManager想要以相同的生成策略产生一个Gadget对象，代码如下：
//Library code
template<template<typename>class CreationPolicy>
class WidgetManager : public CreationPolicy<Widget>
{
	...
	void doSomething()
	{
		Gadget* pW = CreationPolicy<Gadget>().Create();
	}
};
WidgetManager就像一个小型的“代码生成引擎”，你可以自行设定代码的产生方式。

policies不适用于动态连接和二进位接口。

Policy Classes的析构函数
大部分情况下host class会以“public继承”方式从某些policies派生而来。因此，使用者可以将一个host class自动转为一个policy class（向上转型），并于稍后
delete该指针。除非policy class定义了一个虚析构函数，否则delete一个指向policy class的指针，会产生不可预期的结果。
然而如果为policy定义了一个虚析构函数，会妨碍policy的静态连接特性，也会影响效率。许多policies并无任何数据成员，纯粹只规范行为。第一个虚函数被加入后
会为对象大小带来额外开销（因为引入一份vptr），所以虚析构函数应该尽可能避免。
policies应该采用一个轻便而有效率的解法---定义一个non-virtual protected析构函数：
template<typename T>
struct OpNewCreator
{
	static T* Create()
	{
		return new T;
	}

protected：
	~OpNewCreator()
	{
	}
};
由于析构函数属于protected层级，所以只有派生而得的classes才可以摧毁这个policy对象。这样一来外界就不可能delete一个指向policy class的指针。而由于析构函数
并非虚函数，所以不会有大小或速度上的额外开销。（也就是说，如果你敢把指向子类的指针存入到policy class，那么就压根不让你delete，你要delete的话，就得自己
另想办法）。

将一个Class分解为一堆Policies
建立policy-based class design的最困难部分，便是如何将class正确分解为policies。一个准则就是，将参与class行为的设计鉴别出来并命名之。任何事情只要能以一种
以上的方法解决，都应该被分析出来，并从class中移出来称为policy。
过于泛化的host class会产生缺点，它会有过多的template参数。实际上，4-6个以上的template参数会显得笨拙。
当你将class分解为policies时，找到正交分解很重要。正交分解会产生一些彼此完全独立的policies。你很容易发现一个非正交分解---如果各式各样的policies需要知道
彼此，那就是了。

Policies机制由templates和多重继承组成。一个class如果使用了policies，我们称其为host class，那是一个拥有多个template参数（通常是“template template参数”）
的class template，每一个参数代表一个policy。host class的所有机能都来自policies，运作起来就像是一个聚合了数个policies的容器。
*/