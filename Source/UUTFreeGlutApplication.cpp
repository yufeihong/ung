#include "UUTFreeGlutApplication.h"

#ifdef UUT_HIERARCHICAL_COMPILE

namespace ung
{
	FreeglutApplication::FreeglutApplication()
	{
	}

	FreeglutApplication::~FreeglutApplication()
	{
		//当freeglutGo函数返回后，会调用这里。
	}

	void FreeglutApplication::initialize()
	{
	}

	void FreeglutApplication::keyboard(unsigned char key, int x, int y)
	{
	}

	void FreeglutApplication::keyboardUp(unsigned char key, int x, int y)
	{
	}

	void FreeglutApplication::special(int key, int x, int y)
	{
	}

	void FreeglutApplication::specialUp(int key, int x, int y)
	{
	}

	void FreeglutApplication::reshape(int newW, int mewH)
	{
		if (mewH == 0)
		{
			mewH = 1;
		}

		/*
		The  glViewport () function should be called each time when FreeGLUT calls the
		Reshape() function, which is called every time when the window size changes,
		passing us the new height and width. It's also called once when the application is
		first launched.
		*/
		glViewport(0, 0, newW, mewH);
	}

	void FreeglutApplication::idle()
	{
		/*
		The command to swap these buffers in FreeGLUT is glutSwapBuffers(), and the
		command to clear the backbuffer is glClear().In order to clear the color value
		of every pixel in the current buffer, we pass the value GL_COLOR_BUFFER_BIT into
		glClear().Both of these functions must be called inside the Idle() function,
		which is automatically and repeatedly called by FreeGLUT whenever it's not busy
		processing its own events. This is the moment where we should process our own
		background tasks such as physics processing, object management, responding to
		game events, and even rendering itself.
		*/
	}

	void FreeglutApplication::mouse(int button, int state, int x, int y)
	{
	}

	void FreeglutApplication::passiveMotion(int x, int y)
	{
	}

	void FreeglutApplication::motion(int x, int y)
	{
	}

	void FreeglutApplication::display()
	{
	}

	int UNG_STDCALL freeglutGo(int argc, char **argv, int width, int height, const char* title, FreeglutApplication* appPtr)
	{
		globalApplicationPtr = appPtr;

		glutInit(&argc, argv);
		glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
		glutInitWindowPosition(0, 0);
		glutInitWindowSize(width, height);
		glutCreateWindow(title);
		glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);

		/*
		Keyboard() is called whenever FreeGLUT detects that a generic keyboard key has been pressed, such as letters, numbers, and so on.
		*/
		glutKeyboardFunc(keyboardCallback);
		glutKeyboardUpFunc(keyboardUpCallback);

		/*
		Special() function is called for special keys such as the arrow keys, Home, End, Page Up/Page Down, and so on.
		*/
		glutSpecialFunc(specialCallback);
		glutSpecialUpFunc(specialUpCallback);
		glutReshapeFunc(reshapeCallback);
		glutIdleFunc(idleCallback);
		glutMouseFunc(mouseCallback);
		glutPassiveMotionFunc(motionCallback);
		glutMotionFunc(motionCallback);
		glutDisplayFunc(displayCallback);

		//初始化应用程序
		globalApplicationPtr->initialize();

		globalApplicationPtr->idle();

		glutMainLoop();

		return 0;
	}

	//-----------------------------------------------------------------------------------------

#define RADIANS_PER_DEGREE 0.01745329
#define CAMERA_STEP_SIZE 5.0

	BaseFreeglutApplication::BaseFreeglutApplication() :
		mScreenWidth(0),
		mScreenHeight(0),
		mCameraPosition(0.0, 30.0, 30.0),
		mCameraTarget(0.0, 0.0, 0.0),
		mUpVector(0.0, 1.0, 0.0),
		mNearPlane(1.0),
		mFarPlane(1000.0),
		mCameraDistance(15.0),
		mCameraPitch(20.0),
		mCameraYaw(0.0)
	{
	}

	BaseFreeglutApplication::~BaseFreeglutApplication()
	{
	}

	void BaseFreeglutApplication::keyboard(unsigned char key, int x, int y)
	{
		switch (key)
		{
			//'z' zooms in
			case 'z': zoomCamera(+CAMERA_STEP_SIZE);
				break;

			//'x' zoom out
			case 'x': zoomCamera(-CAMERA_STEP_SIZE);
				break;
		}
	}

	void BaseFreeglutApplication::special(int key, int x, int y)
	{
		switch (key)
		{
			//the arrow keys rotate the camera up/down/left/right
			case GLUT_KEY_LEFT:
				rotateCamera(mCameraYaw, +CAMERA_STEP_SIZE);
				break;

			case GLUT_KEY_RIGHT:
				rotateCamera(mCameraYaw, -CAMERA_STEP_SIZE);
				break;

			case GLUT_KEY_UP:
				rotateCamera(mCameraPitch, +CAMERA_STEP_SIZE);
				break;

			case GLUT_KEY_DOWN:
				rotateCamera(mCameraPitch, -CAMERA_STEP_SIZE);
				break;
		}
	}

	void BaseFreeglutApplication::reshape(int nW, int nH)
	{
		FreeglutApplication::reshape(nW, nH);

		mScreenWidth = nW;
		mScreenHeight = nH;

		updateCamera();
	}

	void BaseFreeglutApplication::initialize()
	{
		//OpenGL's built-in lighting system
		GLfloat ambient[] = { 0.5f, 0.5f, 0.5f, 1.0f };
		GLfloat diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		/*
		Specular lighting represents the shininess of an object by highlighting
		certain areas with a brighter color depending on the angle of the camera with the
		light source. Because the camera also gets involved, the effect itself changes as the
		camera moves.
		*/
		GLfloat specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };

		GLfloat light0Position[] = { 10.0f, 10.0f, 10.0f, 0.0f };

		/*
		The glLightfv() function is used to specify the properties of a given light. The
		first parameter is used to select which light to edit, the second is used to determine
		which property to edit, and the third is used to specify the new value. The first two
		parameters must be an enumerator corresponding to a specific value.
		For instance, the options for the first parameter can be GL_LIGHT0, GL_LIGHT1,
		GL_LIGHT2, and so on. Meanwhile, the second parameter could be GL_AMBIENT ,
		GL_DIFFUSE, or GL_SPECULAR to define which lighting property of the given light to
		modify, or even GL_POSITION to define its position.
		*/
		glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
		glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
		glLightfv(GL_LIGHT0, GL_SPECULAR, specular);
		glLightfv(GL_LIGHT0, GL_POSITION, light0Position);

		//enables lighting
		glEnable(GL_LIGHTING);

		//enables the 0th light
		glEnable(GL_LIGHT0);

		//colors materials when lighting is enabled
		glEnable(GL_COLOR_MATERIAL);

		/*
		The glMaterialfv and glMateriali functions specify material parameters for the
		lighting system. Specifically(明确地), we will be using them to define the strength of our
		specular lighting effect. The first parameter for both functions can either be  GL_FRONT,
		GL_BACK,or both combined to define if the setting should affect front or back faces.

		The first call sets the color of the specular effect through GL_SPECULAR.
		The second sets the shininess (GL_SHININESS) to a value of  15.Larger values produce 
		weaker shine effects, and vice versa.
		*/
		glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
		glMateriali(GL_FRONT, GL_SHININESS, 15);

		//enable smooth shading
		glShadeModel(GL_SMOOTH);

		//enable depth testing to be 'less than'
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LESS);

		glClearColor(0.0, 0.0, 0.0, 1.0);
	}


	//
	//临时
	#include "BulletDynamics/Dynamics/btDynamicsWorld.h"

	//

	void BaseFreeglutApplication::updateCamera()
	{
		if (mScreenWidth == 0 && mScreenHeight == 0)
		{
			return;
		}

		//select the projection matrix
		glMatrixMode(GL_PROJECTION);

		//set it to the matrix-equivalent of 1
		glLoadIdentity();

		//determine the aspect ratio of the screen
		float aspectRatio = mScreenWidth / (float)mScreenHeight;

		//create a viewing frustum based on the aspect ratio and the boundaries of the camera
		/*
		The glFrustum function multiplies the currently selected matrix by a projection
		matrix defined by the parameters fed into it. This generates our perspective effect
		and when applied, creates the illusion of depth. It accepts six values describing the left,
		right, bottom, top, near, and far clipping planes of the camera's frustum: essentially the six 
		sides of a 3D trapezoid (or trapezoidal prism in technical terms).
		*/
		//left,right,bottom,top,zNear,zFar
		glFrustum(-aspectRatio * mNearPlane, aspectRatio * mNearPlane, -mNearPlane, mNearPlane, mNearPlane, mFarPlane);
		//the projection matrix is now set

		//-------------------------------------------------------------------------------------------

		//select the view matrix
		glMatrixMode(GL_MODELVIEW);

		//set it to 1
		glLoadIdentity();

		//Our values represent the angles in degrees, but 3D math typically demands angular values are in radians.
		real_type pitch = mCameraPitch * RADIANS_PER_DEGREE;
		real_type yaw = mCameraYaw * RADIANS_PER_DEGREE;

		//Create a quaternion defining the angular rotation around the up vector
		btVector3 bullet_mUpVector(mUpVector.x,mUpVector.y,mUpVector.z);
		btQuaternion rotation(bullet_mUpVector,yaw);

		//set the camera's position to 0,0,0, then move the 'z' 
		//position to the current value of mCameraDistance.
		btVector3 cameraPosition(0, 0, 0);
		cameraPosition[2] = -mCameraDistance;

		//Create a Vec3 to represent the camera position and scale it up if its value is too small.
		btVector3 forward(cameraPosition[0], cameraPosition[1], cameraPosition[2]);
		if (forward.length2() < SIMD_EPSILON)
		{
			forward.setValue(1.f, 0.f, 0.f);
		}

		//figure out the 'right' vector by using the cross 
		//product on the 'forward' and 'up' vectors
		btVector3 right = bullet_mUpVector.cross(forward);

		//Create a quaternion that represents the camera's roll
		btQuaternion roll(right, -pitch);

		//Turn the rotation (around the Y-axis) and roll (around the forward axis) into transformation matrices and apply them to the camera position.This gives us the final position
		cameraPosition = btMatrix3x3(rotation) * btMatrix3x3(roll) * cameraPosition;

		//Save our new position in the member variable,and shift it relative to the target position (so that we orbit it)
		//mCameraPosition[0] = cameraPosition.getX();
		//mCameraPosition[1] = cameraPosition.getY();
		//mCameraPosition[2] = cameraPosition.getZ();
		//mCameraPosition += mCameraTarget;

		//create a view matrix based on the camera's position and where it's looking
		/*
		The gluLookAt function multiplies the currently selected matrix by a view matrix
		generated from nine  double s (essentially three vectors), which represents the eye
		position, the point at which the camera is looking at, and a vector that represents
		which direction is up. The up vector is used to assist in defining the camera's
		rotation. To use common angular rotation vernacular, if we only define a position
		and target, that gives us the pitch and yaw we need, but there's still the question of
		the roll, so we use the up vector to help us calculate it.
		*/
		gluLookAt(mCameraPosition.x, mCameraPosition.y, mCameraPosition.z,
			mCameraTarget.x, mCameraTarget.y, mCameraTarget.z,
			mUpVector.x, mUpVector.y, mUpVector.z);
		//the view matrix is now set
	}

	void BaseFreeglutApplication::rotateCamera(real_type& angle, real_type value)
	{
		angle -= value;
		//Keep the value within bounds
		if (angle < 0)
		{
			angle += 360;
		}

		if (angle >= 360)
		{
			angle -= 360;
		}

		//Update the camera since we changed the angular value
		updateCamera();
	}

	void BaseFreeglutApplication::zoomCamera(real_type distance)
	{
		//Change the distance value
		mCameraDistance -= distance;
		//Prevent it from zooming in too far
		if (mCameraDistance < 0.1f)
		{
			mCameraDistance = 0.1f;
		}

		//Update the camera since we changed the zoom distance
		updateCamera();
	}
}//namespace ung

#endif//UUT_HIERARCHICAL_COMPILE

/*
调用说明：
int main(int argc, char** argv)
{
	//myApp继承自BaseFreeglutApplication
	myApp demo;

	return freeglutGo(argc, argv, 1024, 768, "title", &demo);
}
*/