#include "UPECollisionBox.h"

#ifdef UPE_HIERARCHICAL_COMPILE

namespace ung
{
	CollisionBox::CollisionBox(Vector3 const & boxHalfExtents) :
		btBoxShape({boxHalfExtents.x,boxHalfExtents.y,boxHalfExtents.z})
	{
	}

	CollisionBox::CollisionBox(real_type halfW, real_type halfH, real_type halfZ) :
		btBoxShape({halfW,halfH,halfZ})
	{
	}

	CollisionBox::~CollisionBox()
	{
	}
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

/*
The collision shape:
The collision shape represents the volume of an object in space, be it a box, a sphere,a cylinder, or some other more complex shape.
In addition, a newbie(新手) physics programmer would typically start out by using spheres
to represent the bounding volumes for objects to generate their very first broad phase
system. But, they will later graduate to using AABBs (similar to those described
previously) as they find that the spheres are not very good at representing long,
thin objects, and the mathematics aren't quite as efficient as AABB overlap checks.
Even though AABBs are technically boxes, they don't rotate (since the AA stands
for Axis-aligned), making the overlap math very simple—even simpler than
comparing two spheres for overlap.
*/

/*
btScalar is a simple float by default, but could also be a double if #define BT_USE_DOUBLE_PRECISION is placed somewhere in the code.
*/

/*
Bullet和OpenGL一样，都使用右手坐标系。
*/