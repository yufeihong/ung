 /*
 适配器模式:
 适配器设计模式将一个类的接口转换为一个兼容的但不相同的接口.
 与代理模式的相似之处是,适配器设计模式也是一个单一组件包装器,但适配器类和原始类的接口可以不相同.
 适配器可以用"组合"或者"继承"来实现.这两种分别称为对象适配器和类适配器.
 */

/*
 限制模板类的实例化:
 如果希望模板类仅用于某些已知的类型,就可以使用这种技术.这种技术,在头文件中没有方法定义,末尾也没有#include语句.但是需要在项目中添加一个
 真正的.cpp文件,它包含方法定义.
 为了使这个方法能运行,需要给允许客户使用的类型显式实例化模板.有了这些显式的实例化,就不允许客户代码给其他类型使用这个类模板了.
 为了向用户指出可以使用哪些模板特化(即那些已经显式实例化的模板特化),可以在公有头文件的末尾添加一些类型定义(typedef).
 */

/*
1秒为1000毫秒(millisecond,ms)

duration:持续时间,表示两个时间点之间的间隔.通过模板化的duration类表示.
duration类保存了滴答(tick)数和一个滴答的周期(tick period).
滴答周期指的是两个滴答之间的秒数,是一个编译时ratio常量,也就是说可以是一秒的分数.
template <typename Rep,typename Period = ratio<1>>
class duration
{
	...
}
duration<long,ratio<60> d1(10);		//10 minutes
duration<long,ratio<1> d2(14);		//14 seconds

clock类由time_point和duration组成.
标准定义了3个clock.
1:system_clock,表示来自系统实时时钟的真实时间(wall clock).
2:steady_clock,是一个保证其time_point绝不递减的时钟.system_clock无法做出这个保证,因为系统时钟可以随时调整.
3:high_resolution_clock,这个时钟的滴答周期达到了最小值.high_resolution_clock可能就是steady_clock或system_clock的别名,具体取决于编译器.
每个clock都有一个静态的now()方法,用于把当前时间用作time_point.

计算一段代码执行所消耗的时间:
变量start和end的类型为system_clock::time_point,diff的类型为duration.
//Get the start time
auto start = system_clock::now();

//Execute code that you want to time
real_type d = 0;
for(int i = 0; i < 1000000; ++i)
{
	d += sqrt(sin(i) * cos(i));
}

//Get the end time and calculate the difference
auto end = system_clock::now();
auto diff = end - start;
//Convert the difference into milliseconds and print on the console
cout << duration<real_type,milli>(diff).count() << "ms" << endl;

注意:如果在系统上获得非常小的毫秒差值,那么这些值不会很准确,因为尽管计时器的精度为毫秒级,但是在大部分操作系统上,这个计时器更新的频率不高,
例如每10毫秒或15毫秒更新一次.

time_point类表示的是时间中的一个点.
每个time_point都关联一个clock,创建time_point时,指定clock作为模板参数:
time_point<steady_clock> tp;
每个clock都直到各自的time_point类型
*/

/*
serialization库把存档的格式与类型的序列化完全分离开来,任意的数据类型都可以采用任意格式的存档保存,带来了极大的灵活性.
serialization库有三个基本概念:
存档:archive
可序列化:serializable
序列化:serialize和反序列化:unserialize

存档:
在serialization库里表现为一系列的字节,它对应任意的C++对象,可以持久化保存并在某个时刻恢复成C++对象.
有纯文本格式,XML格式,二进制格式.
有输出存档,输入存档.

可序列化:
所有的C++基本类型都是可序列化的.
标准字符串string和wstring是可序列化的.
自定义类需要一个成员函数.
可序列化类型的数组也是可序列化的.
可序列化类型的指针和引用也是可序列化的.
*/

/*
boost::serialization::access是一个辅助类,声明了一系列的静态成员函数间接调用自定义类的serialize(),存档通过它来完成对自定义
类的序列化.为了让access可以调用serialize(),我们需要使用友元的方式授予访问权限.
friend class boost::serialization::access;

/*
侵入式可序列化
使用辅助类access后,serialize()最好被设置为private,表示仅供类本身使用.
用法:
std::stringstream ss;

Vector2 v_save(3.2, 4.8);
binary_oarchive(ss) << v_save;

Vector2 v_load;
binary_iarchive(ss) >> v_load;

assert(v_save == v_load);
std::cout << v_load;

-------------------------------------

template<typename Archive>				//可以是输入存档或者输出存档
void serialize(Archive &ar, const uint32 version)
{
//基于已有类型的序列化代码
ar & x;
ar & y;
}
*/