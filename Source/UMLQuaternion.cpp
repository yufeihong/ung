//#include "boost/math/quaternion.hpp"

/*
若要平滑地把摄像机在几秒内从某起始定向A旋转到目标定向B，便需在期间找出A和B之间的许多中间旋转。若以矩阵表示A和B的定向，要计算
这些中间值是很困难的。

单位长度的四元数(即，所有符合a * a + b * b + c * c + d * d = 1的四元数)能代表三维旋转。

把单位四元数视为三维旋转：
单位四元数可以视觉化为三维矢量加上第四维的标量坐标。矢量部分(v)是旋转的单位轴乘以旋转半角的正弦，而标量部分(s)是旋转半角的余弦。
那么单位四元数可以写成：
q = [v s] = [a * sin(θ / 2) cos(θ / 2)]
其中a为旋转轴方向的单位矢量，θ为旋转角度。
旋转方向使用右手法则，也就是说，若使右手拇指朝向旋转轴的方向，正旋转角则是其余4只手指弯曲的方向。
单位四元数和轴角(axis-angle)旋转表达方式很相似。但是，四元数在数学上比轴角更方便。

四元数运算：
必须谨记：两个四元数相加的和并不能代表三维旋转，因为该四元数并不是单位长度的。

用于四元数上的最重要运算之一就是乘法。给定两个四元数p和q，分别代表旋转P和Q，则pq代表两旋转的合成旋转(即旋转Q之后再旋转P)。
四元数的乘法有好几种，其中和三维旋转相关的乘法称为格拉斯曼积(Grassmann product)。定义如下：
pq之积为：
pq = [(ps * qv + qs * pv + pv x qv) (ps * qs - pv · qv)]
矢量部分的结果为四元数的x,y,z分量，标量部分为w分量。

逆四元数和原四元数的乘积会变成标量1(即，q * q(-1) = 0i + 0j + 0k + 1)。
四元数[0 0 0 1]代表零旋转(从sin0 = 0代表前3个分量，并且cos0 = 1代表第4个分量，即可得)。
要计算逆四元数，先要定义一个称为共轭(conjugate)的量。共轭通常写成q(*)，定义如下：
q(*) = [-v s]
换句话说，共轭是矢量部分求反(negation)，标量部分不变。
有了这个共轭的定义，逆四元数q(-1)的定义如下：
q(-1) = (q(*)) / (|q|^2)
由于我们使用的四元数都是用于代表三维旋转的，这些四元数都是单位长度的(即，|q| = 1)，因此，在这种情况下，共轭和逆四元数是相等
的。
这意味着，计算逆四元数比计算3x3逆矩阵快得多。

四元数积(pq)的共轭，等于求各个四元数的共轭后，以相反的次序相乘：
(pq)(*) = q(*)p(*)
类似的，四元数积的逆等于求各个四元数的逆后，以相反的次序相乘：
(pq)(-1) = q(-1)p(-1)
这种相反的次序计算，同样适用于矩阵积的转置和逆。

用四元数旋转矢量：
首先要把矢量重写为四元数的形式。矢量是涉及基矢量i,j,k的和，四元数是涉及基矢量i,j,k以及第4个标量项的和。因此，把矢量写成四元数，
并把标量项s设为0。
要用四元数q旋转矢量v，须用q前乘以矢量v(v的对应四元数形式)，再后乘以逆四元数q(-1)：
v1 = rotate(q,v) = q  * v * q(-1)
因为旋转用的四元数都是单位长度的，所以使用共轭也是等同的：
v1 = rotate(q,v) = q * v * q(*)
只要从四元数形式的v1提取矢量部分，就能得到旋转后的矢量v2。

四元数的串接：
和矩阵一样，四元数可以通过相乘来串接旋转。
例如，考虑3个四元数q1,q2,q3分别表示不同的旋转，并对应其等价的矩阵R1,R2,R3。我们希望首先进行旋转1，接着旋转2，最后旋转3。
求合成旋转矩阵R和其旋转矢量v，等式如下：
R = R1 * R2 * R3
v2 = v * R1 * R2 * R3 = v * R
相似地，求合成旋转四元数q和其旋转矢量v(以四元数形式表示的v)，等式如下：
q = q3 * q2 * q1
v1 = q3 * q2 * q1 * v * q1(-1) * q2(-1) * q3(-1) = q * v * q(-1)
注意：四元数的相乘次序和进行旋转的次序必须是相反的。
没有求逆的四元数从右至左阅读，逆四元数从左至右阅读。

等价的四元数和矩阵：
任何三维旋转都可以从3x3矩阵表达方式R和四元数表达方式q之间自由转换。
若设q = [v s] = [x y z w]，则可用如下方式求R：
行为主：
R = 
1 - 2 * y * y - 2 * z * z			2 * x * y + 2 * z * w				2 * x * z - 2 * y * w
2 * x * y - 2 * z * w				1 - 2 * x * x - 2 * z * z			2 * y * z + 2 * x * w
2 * x * z + 2 * y * w				2 * y * z - 2 * x * w				1 - 2 * x * x - 2 * y * y

旋转性的线性插值：
套用四维矢量的线性插值(LERP)至四元数。
给定两个分别代表旋转A和旋转B的四元数qa和qb，可找出自旋转A至旋转B之间β百分点的中间旋转Qlerp。
Qlerp = LERP(qa,qb,β) = ((1.0 - β) * qa + β * qb) / ((1.0 - β) * qa + β * qb)
注意：插值后的四元数需要再归一。这是因为LERP运算一般来说并不保持矢量长度。

球面线性插值：
LERP是沿着超球的弦进行插值的，而不是在超球面上插值。这样会导致当β以恒定速度改变时，旋转动画并非
以恒定角速度进行。旋转在两端看似较慢，但在动画中间就会较快。
解决此问题的方法是，采用LERP运算的变体---球面线性插值(spherical linear interpolation)，简称SLERP。
SLERP使用正弦和余弦在四维超球面的大圆上进行插值，而不是沿弦上插值。当β以常数速率变化，插值结果
便会以常数角速率变化。
SLERP公式和LERP公式相似，但其加权值以wp和wq取代(1 - β)和β。wp和wq使用到两个四元数之夹角的
正弦:
SLERP(p,q,β) = wp * p + wq * q
其中：
wp = (sin((1 - β) * θ)) / sinθ
wq = sin(β * θ) / sinθ
两个单位四元数之间的夹角，可以使用四维点积求得。
*/

/*
Quaternions:
One of the most important aspects of quaternions is that they provide an efficient way to parameterize rotations in R^3 (the 
usual three-dimensional space) and R^4.
A quaternion is simply a quadruple(由四部份组成的) of real numbers (α,β,γ,δ), which we can write in the form q = α + βi + γj + δk.
An addition and a multiplication is defined on the set of quaternions, which generalize their real and complex counterparts. The 
main novelty(新颖，新奇) here is that the multiplication is not commutative(不可交换) (i.e. there are quaternions x and y such that 
xy ≠ yx).A good way of remembering things is by using the formula i*i = j*j = k*k = -1.
*/

/*
"方向"和"方位":
向量有方向,但没有方位.区别在于,当一个向量指向特定方向时,可以让向量自转,但向量(或者说它的方向)却不会有任何变化,因为向量的属性只有
大小,而没有厚度和宽度.然而,当一个物体朝向特定的方向时,让它自转,我们就会发现物体的方位改变了.从技术角度讲,这就说明在3D中,只要用
两个数字(例如,极坐标),就能用参数表示一个方向(direction).但是,要确定一个方位(orientation),却至少需要三个数字.

角位移:
我们知道不能用绝对坐标来描述物体的位置,要描述物体的位置,必须把物体放置于特定的参考系中.描述位置实际上就是描述相对于给定参考点(通常是
坐标系的原点)的位移.同样,描述物体方位时,也不能使用绝对量.与位置只是相对已知点的位移一样,方位是通过于相对已知方位(通常称作单位方位
或源方位)的旋转来描述的.旋转的量称作角位移.

我们用矩阵和四元数来表示"角位移",用欧拉角来表示"方位".

欧拉证明了一个旋转序列等价于单个旋转.因此,3D中的任意角位移都能表示为绕单一轴的单一旋转(这里的轴是一般意义上的旋转轴,不要和笛卡尔
坐标轴混淆).当一个方位用这种形式来描述时,称作轴-角描述法.
在Mat3中,我们导出了使向量绕任意轴旋转的矩阵.像以前那样,设n为旋转轴(单位长度),θ为绕轴旋转的量.把四元数解释为角位移的轴-角对
方式.然而,n和θ不是直接存储在四元数的四个数中,而是如下这样:
q = [cos(θ / 2),sin(θ / 2) * n.x,sin(θ / 2) * n.y,sin(θ / 2) * n.z]
*/

/*
使用单位四元数来表达旋转.

四元数是用来表示旋转的另一种数学形式(旋转矩阵的另一种描述方式).其优点:
1:存储空间更少.
2:四元数之间的连接运算需要的算术运算更少.
3:在产生平滑的三维动画时,用四元数更容易进行修改.

(虚数i -> 复数a + bi -> 超复数(四元数))
四元数是指有一个实部和三个虚部的复数.

可以这样表示四元数:q = q0 + q1 * i + q2 * j + q3 * k,或者q = q0 +qv
i = <1,0,0>,j = <0,1,0>,k = <0,0,1>,且q0,q1,q2,q3都是实数.i,j,k都是虚数,它们组成四元数q的向量基.
虚数基<i,j,k>,可以将其视为坐标系中三个互相垂直的单位向量,用于定义<i,j,k>空间中的点.
注意:
i * i = j * j = k * k = -1 = i * j * k.
i * j = -j * i = k;
j * k = -k * j = i
k * i = -i * k = j
*/

/*
四元数乘法(叉乘):根据复数解释来乘.
(w1 + x1i + y1j + z1k) * (w2 + x2i + y2j + z2k)
 =	(w1 * w2 -	x1 * x2 -	y1 * y2 -	z1 * z2) +
	(w1 * x2 +	x1 * w2 +	y1 * z2 -	z1 * y2)i +
	(w1 * y2 -		x1 * z2 +	y1 * w2 +	z1 * x2)j +
	(w1 * z2 +	x1 * y2 -	y1 * x2 +	z1 * w2)k
四元数叉乘满足结合律,但不满足交换律:
(ab)c = a(bc)
ab != ba
四元数乘积的模等于模的乘积.这个结论非常重要,因为它保证了两个单位四元数相乘的结果还是单位四元数.
四元数乘积的逆等于各个四元数的逆以相反的顺序相乘.

把一个标准的Vec3(x,y,z)给扩展到四元数空间,通过定义四元数p = [0,(x,y,z)]即可.当然,一般情况下,这个p不会是单位四元数.设q为
旋转四元数形式[cos(θ / 2),sin(θ / 2) * n],n为旋转轴,单位向量,θ为旋转角,那么就会发现一个神奇的等式:
p1 = q * p * q(-1),p1为p转换后的结果,q(-1)表示四元数q的逆.
再看,将p用一个四元数a旋转,然后再用另一个四元数b旋转:
p1 = b(apa(-1))b(-1)
	= (ba)p(a(-1)b(-1))
	= (ba)p(ba)(-1)
我们发现,先进行a旋转再进行b旋转,等价于执行乘积ba代表的单一旋转.
*/

/*
四元数的模
几何意义:
|q| = |[w,v]|
	= sqrt(cos(θ / 2)的平方 + (sin(θ / 2) * n)的平方)			//n为单位向量
	= sqrt(cos(θ / 2)的平方 + sin(θ / 2)的平方)
因为sinx的平方 + cosx的平方 = 1,所以
	= sqrt(1)
	= 1
所以,我们能看到,一个绕单位向量(轴)一定角度的四元数,其模为1.原则上,我们仅使用符合这个规则的"单位四元数".
*/

/*
四元数的共轭
通过让四元数的向量部分变负来获得.
q * q(*) = 1 = q(*) * q
*/

/*
四元数的逆
四元数的逆,定义为四元数的共轭除以它的模.
一个四元数q乘以它的逆,即可得到单位四元数[1,0].
因为我们只使用单位四元数,所以四元数的逆和共轭是相等的.
因为四元数和其共轭代表相反的角位移.很容易验证这种说法,使向量v变负,也就是是旋转轴反向,它颠倒了我们所认为的旋转正方向.因此四元数
绕轴旋转θ角,而其共轭沿相反的方向旋转相同的角度.
*/

/*
两个四元数的"差".
"差"被定义为一个方位到另一个方位的角位移.换句话说,给定方位a和b,能够计算从a旋转到b的角位移d.
ad = b
a(-1)(ad) = a(-1)b
[1,0]d = a(-1)b
d = a(-1)b
*/

/*
四元数的对数
q = cos(A) + sin(A) * (x * i + y * j + z * k)其中x,y,z为单位长度向量的分量
log(q) = A * (x * i + y * j + z * k)
如果sin(A)接近于0,那么
log(q) = sin(A) * (x * i + y * j + z * k)因为sin(A)/A limit 1.
*/

/*
四元数的指数
q = A * (x * i + y * j + z * k)其中x,y,z为单位长度向量的分量
exp(q) = cos(A) + sin(A) * (x * i + y * j + z * k)
如果sin(A)接近于0,那么
exp(q) = cos(A) + A * (x * i + y * j + z * k)因为sin(A)/A limit 1.
*/

/*
Matrix to Quater

我们知道w = cos(θ / 2),所以w^2 = (cos(θ / 2))^2,又因为2 * (cos(θ / 2))^2 = 1 + cosθ,另cosθ = (trace(R) - 1) / 2,
所以w^2 = (trace(R) + 1) / 4,或者|w| = sqrt(trace(R) + 1) / 2
如果trace(R) > 0,那么|w| > 1 /2,所以这里我们取w = sqrt(trace(R) + 1) / 2.
从R - R^t = 2 * sinθ * S可以得到(r21 - r12,r02 - r20,r10 - r01) = 2 * sinθ * (w0,w1,w2)这里的w0,w1,w2表示旋转轴.
通过比对Mat3中绕任意轴旋转矩阵和从四元数来构建矩阵,可知:
2xw = w0 * sinθ,2yw = w1 * sinθ,2zw = w2 * sinθ.这里的x,y,z为Quater的成员变量.
注意要区分旋转轴和Quater本身的成员变量.
这样就可以得到:
x = (r21 - r12) / (4w)
y = (r02 - r20) / (4w)
z = (r10 - r01) / (4w)

如果trace(R) <= 0,那么|w| <= 1 /2,这个时候如果r00是对角线上最大的,那么x在magnitude上,就比y和z大.
4 * x * x = r00 - r11 - r22 + 1,这样就可以得到x = sqrt(r00 - r11 - r22 + 1) / 2,w = (r21 - r12) / (4x),
y = (r10 + r01) / (4x),z + (r20 + r02) / (4x)
r11最大和r22最大同理.

------------------------------------------------------------------------------------------------------------------------------------

trace > 0:6A + 5M + 1D + 1F + 1C
trace <= 0:6A + 5M + 1D + 1F + 3C
*/