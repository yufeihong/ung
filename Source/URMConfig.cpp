#include "URMConfig.h"

#ifdef URM_HIERARCHICAL_COMPILE

#pragma comment(lib,"freeimage.lib")

namespace
{
	//warning LNK4221: 此对象文件未定义任何之前未定义的公共符号，因此任何耗用此库的链接操作都不会使用此文件。
	int URM_DUMMY_LNK_4221 = 0;
}//namespace

#endif//URM_HIERARCHICAL_COMPILE