/*
boost.pool库基于简单分隔存储思想实现了一个快速，紧凑的内存池库。
它类似于一个小型的垃圾回收机制，在需要大量地分配/释放小对象时很有效率，而且完全不需要考虑delete。
pool库包含四个组成部分：
1：最简单的pool
2：分配类实例的object_pool
3：单件内存池singleton_pool
4：可用于标准库的pool_alloc
*/

/*
pool：
pool是最简单也最容易使用的内存池类，可以返回一个简单数据类型（POD）的内存指针。
它位于名字空间boost，头文件：boost/pool/pool.hpp
类摘要：
template<typename UserAllocator = ...>
class pool
{
public:
	explicit pool(size_type requested_size);											//requested_size，指示每次pool分配内存块的大小（而不是pool内存池的大小）
	~pool();

	size_type get_requested_size() const;											//分配块大小

	//分配内存
	void* malloc();																			//从内存池中任意分配一个内存块
	void* ordered_malloc();																//分配的同时合并空闲块链表
	void* ordered_malloc(size_type n);												//连续分配n块的内存
	bool is_from(void* chunk) const;

	//归还内存，手工释放之前分配的内存块，内存池会自动管理内存分配，不应该调用
	void free(void* chunk);
	void ordered_free(void* chunk);
	void free(void* chunks, size_type n);
	void ordered_free(void* chunks, size_type n);

	//释放内存
	bool release_memory();																//释放所有未被分配的内存，但已分配的内存块不受影响
	bool purge_memory();																	//强制释放pool持有的所有内存，不管内存块是否被使用(不应该手工调用)，purge:清除，清洗
};
操作函数：
pool的模板类型参数UserAllocator是一个用户定义的内存分配器，通常可以直接使用默认的default_user_allocator_new_delete。
pool的构造函数接受一个size_type类型的整数requested_size，指示每次pool分配内存块的大小（而不是pool内存池的大小），这个值可以用get_requested_size()
获得。
pool会根据需要自动地向系统申请或归还使用的内存，在析构时，pool将自动释放它所持有的所有内存块。
malloc()和ordered_malloc()的行为很类似C中的全局函数malloc()，用void*指针返回从内存池中分配的内存块，大小为构造函数中指定的requested_size。如果
内存分配失败，函数会返回0，不会抛出异常。malloc()从内存池中任意分配一个内存块，而ordered_malloc()则在分配的同时合并空闲块链表。ordered_malloc()带
参数的形式还可以连续分配n块的内存。分配后的内存块可以用is_from()函数测试是否是从这个内存池分配出去的。
与malloc()对应的一组函数是free()，用来手工释放之前分配的内存块，这些内存块必须是从这个内存池分配出去的（is_from(chunk) == true）。一般情况内存池会
自动管理内存分配，不应该调用free()函数，除非你认为内存池的空间已经不足，必须释放已经分配的内存。
release_memory()让内存池释放所有未被分配的内存，但已分配的内存块不受影响。purge_memory()则强制释放pool持有的所有内存，不管内存块是否被使用。实际
上，pool的析构函数就是调用的purge_memory()。这两个函数一般情况下也不应该由程序员手工调用。

用法：
pool很容易使用，可以像C中的malloc()一样分配内存，然后随意使用。除非有特殊要求，否则不必对分配的内存调用free()释放，pool会很好地管理内存。例如：
{
	pool<> pl(sizeof(int));																//一个内存池，可分配int
	int* p = static_cast<int*>(pl.malloc());
	assert(pl.is_from(p));
}//内存池对象析构，所有分配的内存在这里都被释放
因为pool在分配内存失败的时候不会抛出异常，所以实际编写的代码应该检查返回的指针，以防止空指针错误，不过通常这种情况极少出现。

pool只能作为普通数据类型的内存池，不能应用于复杂的类对象，因为它只分配内存，不调用构造函数。
*/

/*
singleton_pool:
singleton_pool与pool的接口完全一致，可以分配简单数据类型（POD）的内存指针，但它是一个单件（保证在main()函数运行之前就创建单件），并提供线程安全。
类摘要：
template<typename Tag,unsigned RequestedSize>
class singleton_pool
{
public:
	static bool is_from(void* p);
	...
};
singleton_pool主要有两个模板类型参数（其余的可以使用缺省值）。第一个Tag仅仅是用于标记不同的单件，可以是空类，甚至是声明。第二个参数等同于pool构造函数
中的整数requested_size，指示pool分配内存块的大小。
singleton_pool的接口与pool完全一致，但成员函数均是静态的，因此不需要声明singleton_pool的实例（因为使用了单件模式，用户也无法创建singleton_pool的实例），
直接用域操作符::来调用静态成员函数。因为singleton_pool是单件，所以它的生命周期与整个程序同样长，除非手动调用release_memory()或purge_memory()，
否则singleton_pool不会自动释放所占用的内存。除了这两点，singleton_pool的用法与pool完全相同。
struct pool_tag{};																				//仅仅用于标记的空类
typedef singleton_pool<pool_tag, sizeof(int)> spl;									//内存池定义

int main()
{
	int* p = (int*)spl::malloc();																//分配一个整数内存块
	assert(spl::is_from(p));
	spl::release_memory();																	//释放所有未被分配的内存

	return 0;
}
//spl的内存直到程序结束才完全释放，而不是退出作用域

用于标记的类pool_tag可以再进行简化，直接在模板参数列表中声明tag类，例如：
typedef singleton_pool<struct pool_tag,sizeof(int)> spl;
*/