#include "UPETransformationHook.h"

#ifdef UPE_HIERARCHICAL_COMPILE

namespace ung
{
	UngExport TransformationHook::TransformationHook(const btTransform &transform) :
		btDefaultMotionState(transform)
	{
	}

	UngExport TransformationHook::~TransformationHook()
	{
	}

	UngExport void TransformationHook::grabWorldTransform(real_type* transform)
	{
		btTransform trans;
		getWorldTransform(trans);
		trans.getOpenGLMatrix(transform);
	}

	UngExport void TransformationHook::grabWorldTransform(Matrix4& transform)
	{
		std::array<real_type, 16> holderRowMajor;
		grabWorldTransform(holderRowMajor.data());
		
		Matrix4 tempM4(holderRowMajor.data(),16);

		//需要显式调用std::move()函数。
		transform = std::move(tempM4);
	}
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

/*
The motion state:
The motion state's job is to catalogue(登记分类) the object's current position and orientation.
This lets us to use it as a hook to grab the object's transformation matrix (also known
as a transform). We can then pass the object's transform into our rendering system in
order to update the graphical object to match that of the physics system.
This is an incredibly(难以置信的) important point that cannot be ignored, forgotten, or otherwise
misplaced; Bullet does not know, nor does it care, how we render its objects.Our physics and graphics
engines are completely isolated from one another, and they have different ways of representing the same object.
(我们的物理和图形引擎是完全分离的，它们有不同的方式表示同一个对象。)
Having no dependency between our graphics and physics is ideal(是理想的) because it means
that we could completely replace the physics engine without having to touch the
graphics engine, and vice versa. It also means that we can have invisible physics
objects (such as force fields), or graphical objects that don't need a physical presence
(such as particle effects).

It is not uncommon(屡见不鲜) for a game engine to separate these components entirely with
three different libraries, resulting in the three different sets of Vec3/Matrix4x4/Quater
classes in the lowest levels; one set for the physics, one set for the graphics, and one set for
general game logic.
*/