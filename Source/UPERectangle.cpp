#include "UPERectangle.h"

#ifdef UPE_HIERARCHICAL_COMPILE

namespace ung
{
	Rectangle::Rectangle(Vector2 const & min, Vector2 const & max) :
		mMin(min),
		mMax(max)
	{
	}

	Rectangle::Rectangle(Vector2 const & min, real_type width, real_type height) :
		Rectangle(min,min + Vector2(width,height))
	{
	}

	Rectangle::Rectangle(real_type minx, real_type miny, real_type maxx, real_type maxy) :
		mMin(minx,miny),
		mMax(maxx,maxy)
	{
	}
}

#endif//UPE_HIERARCHICAL_COMPILE