#include "UPETriangle.h"

#ifdef UPE_HIERARCHICAL_COMPILE

namespace ung
{
	Triangle::Triangle(Vector3 const& v1,Vector3 const& v2,Vector3 const& v3)
	{
		mVertices[0] = v1;
		mVertices[1] = v2;
		mVertices[2] = v3;
	}

	Triangle::~Triangle()
	{
	}

	std::array<Vector3, 3> const& Triangle::getVertices() const
	{
		return mVertices;
	}
}//namespace ung

#endif//UPE_HIERARCHICAL_COMPILE

/*
Triangle ABC are given by T(u, v, w) = uA + vB + wC,where (u, v, w) are the barycentric 
coordinates of the point such that u + v + w = 1.T is inside ABC if and only if its barycentric 
coordinates satisfy 0 �� u, v, w �� 1.Alternatively, this may also be written as
T(v, w) = A + v(B - A) + w(C - A), with u = 1 - v - w.T is now inside ABC if v �� 0,w �� 0, and 
v + w �� 1.
*/