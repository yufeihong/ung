/*
所谓STL容器，即是将最常被运用的一些数据结构(data structures)实现出来。

标准的STL关联式容器分为set(集合)和map(映射表)两大类，以及这两大类的衍生体multiset(多键集合)和multimap(多键映射表)。这些容器的底层机制均以RB-tree(红黑树)
完成。RB-tree也是一个独立容器，但并不开放给外界使用。

关联式容器：hash table(散列表)，以及以此hash table为底层机制而完成的hash_set(散列集合)，hash_map(散列映射表)，hash_multiset(散列多键集合)，hash_multimap
(散列多键映射表)。

heap内含一个vector，priority-queue内含一个heap，stack和queue都内含一个deque，set/map/multiset/multimap都内含一个RB-tree，hash_x都内含一个hasttable。

关联式容器(associative containers)
当元素被插入到关联式容器中时，容器内部结构(可能是RB-tree，也可能是hashtable)便依照其键值大小，以某种特定规则将这个元素放置于适当位置。关联式容器没有所谓头尾
(只有最大元素和最小元素)，所以不会有所谓push_back(),push_front(),pop_back(),pop_front(),begin(),end()这样的操作行为。
一般而言，关联式容器的内部结构是一个balanced binary tree(平衡二叉树)，以便获得良好的搜寻效率。平衡二叉树有多种类型，包括AVL-tree,RB-tree,AA-tree，其中最被
广泛运用于STL的是RB-tree(红黑树)。
set的键值就是实值。map的键值可以和实值分开，并形成一种映射关系，所以map被称为映射表，或称为字典(dictionary)。

树
树由节点（nodes）和边（edges）构成。整棵树有一个最上端节点，称为根节点（root）。每个节点可以拥有具方向性的边（directed edges），用来和其他节点相连。相连节点
之中，在上者称为父节点（parent），在下者称为子节点（child）。无子节点者称为叶子节点（leaf）。子节点可以存在多个，如果最多只允许两个子节点，即所谓二叉树（binary 
tree）。不同的节点如果拥有相同的父节点，则彼此互为兄弟节点（siblings）。根节点至任何节点之间有唯一路径（path），路径所经过的边数，称为路径长度（length）。根节点
至任一节点的路径长度，即为该节点的深度（depth）。根节点的深度永远是0。某节点至其最深子节点（叶子节点）的路径长度，称为该节点的高度（height）。整棵树的高度，便
以根节点的高度来代表。节点A->B之间如果存在（唯一）一条路径，那么A称为B的祖代（ancestor），B称为A的子代（descendant）。任何节点的大小（size）是指其所有子代
（包括自己）的节点总数。

二叉搜索树（binary search tree）
所谓二叉树（binary tree），其意义是：“任何节点最多只允许有两个子节点”。这两个子节点称为左子节点和右子节点。如果以递归方式来定义二叉树，我们可以说：“一个二叉树
如果不为空，便是由一个根节点和左右两个子树构成，左右子树都可能为空”。
所谓二叉搜索树（binary search tree），可提供对数时间（logarithmic time）的元素插入和访问。二叉搜索树的节点放置规则是：任何节点的键值一定大于其左子树中的每一个
节点的键值，并小于其右子树中的每一个节点的键值。因此，从根节点一直往左走，直至无左路可走，即得到最小元素。从根节点一直往右走，直至无右路可走，即得到最大元素。
插入新元素时，可以从根节点开始，遇到键值较大者就向左，遇到键值较小者就向右，一直到尾端，即为插入点。
欲删除旧节点A，情况可分为两种。如果A只有一个子节点，我们就直接将A的子节点连至A的父节点，并将A删除。如果A有两个子节点，我们就以右子树内的最小节点取代A。注意，
右子树的最小节点极易获得：从右子节点开始（视为右子树的根节点），一直向左走至底即是。

平衡二叉搜索树（balanced binary search tree）
也许因为输入值不够随机，也许因为经过某些插入或删除操作，二叉搜索树可能会失去平衡，造成搜寻效率低落的情况。
所谓树形平衡与否，并没有一个绝对的测量标准。“平衡”的大致意义是：没有任何一个节点过深（深度过大）。不同的平衡条件，造就出不同的效率表现，以及不同的实现复杂度。
有数种特殊结构如AVL-tree,RB-tree,AA-tree，均可实现出平衡二叉搜索树，它们都比一般的（无法绝对维持平衡的）二叉搜索树复杂，因此，插入节点和删除节点的平均时间也
比较长，但是它们可以避免极难应付的最坏（高度不平衡）情况，而且由于它们总是保持某种程度的平衡，所以元素的访问（搜寻）时间平均而言也就比较少。一般而言其搜寻时间
可节省25%左右。

AVL tree(Adelson-Velskii-Landis tree)
AVL tree是一个“加上了额外平衡条件”的二叉搜索树。其平衡条件的建立是为了确保整棵树的深度为O(logN)。直观上的最佳平衡条件是每个节点的左右子树有着相同的高度，但这
未免太过严苛，我们很难插入新元素而又保持这样的平衡条件。AVL tree于是退而求其次，要求任何节点的左右子树高度相差最多1。这是一个较弱的条件，但仍能够保证“对数深度
（logarithmic depth）”平衡状态。

RB-tree（红黑树）
所谓RB-tree，不仅是一个二叉搜索树，而且必须满足一下规则：
1：每个节点不是红色就是黑色。
2：根节点为黑色。
3：如果节点为红，其子节点必须为黑。
4：任一节点至nullptr（视为黑，树尾端）的任何路径，所含之黑节点数必须相同。
根据规则4，新增节点必须为红。
根据规则3，新增节点之父节点必须为黑。
当新节点根据二叉搜索树的规则到达其插入点，却未能符合上述条件时，就必须调整颜色并旋转树形。
经验告诉我们，RB-tree的搜寻平均效率和AVL-tree几乎相等。
插入节点
假设新节点为X，其父节点为P，祖父节点为G，伯父节点（父节点之兄弟节点）为S，曾祖父节点为GG。
根据二叉搜索树的规则，新节点X必为叶子节点。
根据规则4，X必为红。
若P也为红（这就违反了规则3，必须调整树形），则G必为黑（因为原本RB-tree必须遵循规则3）。于是根据X的插入位置及外围节点（S和GG）的颜色，有了以下四种考虑：
1：S为黑且X为外侧插入。对此情况，我们先对P，G做一次单旋转，并更改P，G颜色，即可重新满足红黑树的规则3。
2：S为黑且X为内侧插入。对此情况，我们必须先对P，X做一次单循转并更改G，X颜色，再将结果对G做一次单循转，即可再次满足规则3。
3：S为红且X为外侧插入。对此情况，先对P，G做一次单旋转，并改变X的颜色。此时如果GG为黑，一切搞定。但如果GG为红，则问题比较大，见状况4。
4：S为红且X为外侧插入。对此情况，先对P，G做一次单旋转，并改变X的颜色。此时如果GG为红，还得持续往上做，直到不再有父子连续为红的情况。
为了避免状况4“父子节点皆为红色”的情况持续向RB-tree的上层结构发展，形成处理时效上的瓶颈，我们可以施行一个由上而下的程序：假设新增节点为A，那么就沿着A的路径，
只要看到有某节点X的两个子节点皆为红色，就把X改为红色，并把两个子节点改为黑色。但是如果X的父节点P也是红色（注意，此时S绝不可能为红），就得像状况1一样地做
一次单循转并改变颜色，或是像状况2一样地做一次双旋转并改变颜色。在此之后，新节点的插入就很单纯了：要么直接插入，要么插入后（若X为红）再做一次旋转（单双皆可能）
即可。
*/

/*
一棵树是N个节点和N-1条边的集合，其中的一个节点叫做根。因为，每条边都将某个节点连接到它的父亲，而除去根节点外每个节点都有一个父亲。
从每一个节点到它自己有一条长为0的路径。
在一棵树中从根节点到每个节点恰好存在一条路径。
对任意节点ni，ni的深度为从根节点到ni的唯一路径的长。因此，根节点的深度为0。
ni的高是从ni到一片树叶的最长路径的长。因此所有的树叶的高都是0。
一棵树的高等于它的根节点的高。
一棵树的深度等于它的最深的树叶的深度，该深度总是等于这棵树的高。
如果存在从n1到n2的一条路径，那么n1是n2的一位祖先（ancestor）而n2是n1的一个后裔（descendant）。如果n1 != n2，那么n1是n2的一位真祖先（proper ancestor）而
n2是n1的一个真后裔（proper descendant）。

二叉树是一棵树，其中每个节点都不能有多于两个的儿子。
二叉树实际上就是图（graph）。
使二叉树成为二叉查找树的性质是，对于树中的每个节点X，它的左子树中所有关键字值小于X的关键字值，而它的右子树中所有关键字值大于X的关键字值。
*/

/*
一个元素的度（degree of an element）是指其孩子的个数。叶子节点的度为0。
一棵树的度（degree of a tree）是其元素的度的最大值。

二叉树和树的根本区别是：
1：二叉树的每个元素都恰好有两棵子树（其中一个或两个可能为空）。而树的每个元素可有任意数量的子树。
2：在二叉树中，每个元素的子树都是有序的，也就是说，有左子树和右子树之分。而树的子树是无序的。

一棵二叉树的高度为h，h>=0，它最少有h个元素，最多有2^h - 1个元素。
一棵二叉树有n个元素，n>0，它的高度最大为n，最小为log2(n+1)。
设完全二叉树的一元素其编号为i，1<=i<=n。有以下关系成立：
1：如果i=1，则该元素为二叉树的根。若i>1，则其父节点的编号为i/2。
2：如果2i>n，则该元素无左孩子。否则，其左孩子的编号为2i。
3：如果2i+1>n，则该元素无右孩子。否则，其右孩子的编号为2i+1。

有4种遍历二叉树的常用方法：
1：前序遍历
template<typename T>
void preOrder(binaryTreeNode<T> *t)
{
	//前序遍历二叉树*t

	if(t)
	{
		visit(t);
		preOrder(t->leftChild);
		preOrder(t->rightChild);
	}
}
2：中序遍历
template<typename T>
void preOrder(binaryTreeNode<T> *t)
{
	//中序遍历二叉树*t

	if(t)
	{
		preOrder(t->leftChild);
		visit(t);
		preOrder(t->rightChild);
	}
}
3：后序遍历
template<typename T>
void preOrder(binaryTreeNode<T> *t)
{
	//后序遍历二叉树*t

	if(t)
	{
		preOrder(t->leftChild);
		preOrder(t->rightChild);
		visit(t);
	}
}

template<typename T>
void visit(binaryTreeNode<T> *x)
{
	//访问节点*x，仅输出element域
	cout << x->element << ' ';
}
4：层次遍历

二叉搜索树是一棵二叉树，可能为空。一棵非空的二叉搜索树满足以下特征：
1：每个元素有一个关键字，并且任意两个元素的关键字都不同。因此，所有的关键字都是唯一的。
2：在根节点的左子树中，元素的关键字（如果有的话）都小于根节点的关键字。
3：在根节点的右子树中，元素的关键字（如果有的话）都大于根节点的关键字。
4：根节点的左，右子树也都是二叉搜索树。

AVL树和红黑树都使用“旋转”来保持平衡。AVL树对每个插入操作最多需要一次旋转，对每个删除操作最多需要O(logn)次旋转。而红黑树对每个插入和删除操作，都只需要
一次旋转。
*/

/*
函数调用运算符
如果类重载了函数调用运算符，则我们可以像使用函数一样使用该类的对象。因为这样的类同时也能存储状态，所以与普通函数相比它们更加灵活。
举例：下面这个名为absInt的struct含有一个调用运算符，该运算符负责返回其参数的绝对值：
struct absInt
{
	int operator()(int val) const
	{
		return val < 0 ? -val : val;
	}
};
这个类只定义了一种操作：函数调用运算符，它负责接受一个int类型的实参，然后返回该实参的绝对值。
我们使用调用运算符的方式是令一个absInt对象作用于一个实参列表，这一过程看起来非常像调用函数的过程：
int i = -42;
absInt absObj;													//含有函数调用运算符的对象
int ui = absObj(i);												//将i传递给absObj.operator()
即使absObj只是一个对象而非函数，我们也能“调用”该对象。调用对象实际上是在运行重载的调用运算符。
函数调用运算符必须是成员函数。一个类可以定义多个不同版本的调用运算符，相互之间应该在参数数量或类型上有所区别。
如果类定义了调用运算符，则该类的对象称作函数对象(function object)。因为可以调用这种对象，所以我们说这些对象的“行为像函数一样”。

含有状态的函数对象类
和其他类一样，函数对象类除了operator()之外也可以包含其他成员。函数对象类通常含有一些数据成员，这些成员被用于定制调用运算符中的操作。
举例：定义一个打印string实参内容的类。默认情况下，我们的类会将内容写到cout中，每个string之间以空格隔开。同时也允许类的用户提供其他可写入的流及其他分隔符。我们将
该类定义如下：
class PrintString
{
public:
	PrintString(ostream &o = cout, char c = ' ') :
		os(o),
		sep(c)
	{
	}

	void operator()(const string &s) const
	{
		os << s << sep;
	}

private:
	ostream &os;														//用于写入的目的流
	char sep;															//用于将不同输出隔开的字符
};
我们的类有一个构造函数，它接受一个输出流的引用以及一个用于分隔的字符，这两个形参的默认实参分别是cout和空格。之后的函数调用运算符使用这些成员协助其打印给定的
string。
当定义PrintString的对象时，对于分隔符及输出流既可以使用默认值也可以提供我们自己的值：
PrintString printer;													//使用默认值，打印到cout
printer(s);																//在cout中打印s，后面跟一个空格
PrintString errors(cerr, '\n');
errors(s);																//在cerr中打印s，后面跟一个换行符
函数对象常常作为泛型算法的实参。例如，可以使用标准库for_each算法和我们自己的PrintString类来打印容器的内容：
for_each(vs.begin(),  vs.end(), PrintString(cerr, '\n'));
for_each的第三个实参是类型PrintString的一个临时对象，其中我们用cerr和换行符初始化了该对象。当程序调用for_each时，将会把vs中的每个元素依次打印到cerr中，元素之间
以换行符分隔。

lambda是函数对象
当我们编写了一个lambda后，编译器将该表达式翻译成一个未命名类的未命名对象。在lambda表达式产生的类中含有一个重载的函数调用运算符。例如下面我们传递给stable_sort
作为其最后一个实参的lambda表达式来说：
//根据单词的长度对其进行排序，对于长度相同的单词按照字母表顺序排序
stable_sort(words.begin(), words.end(),
														[](const string &a, const string &b)
														{
															return a.size() < b.size();
														}
														);
其行为类似于下面这个类的一个未命名对象：
class ShorterString
{
public:
	bool operator()(const string &s1, const string &s2) const
	{
		return s1.size() < s2.size();
	}
};
产生的类只有一个函数调用运算符成员，它负责接受两个string并比较它们的长度，它的形参列表和函数体与lambda表达式完全一样。默认情况下lambda不能改变它捕获的变量。因
此在默认情况下，由lambda产生的类当中的函数调用运算符是一个const成员函数。如果lambda被声明为可变的，则调用运算符就不是const的了。
用这个类替代lambda表达式后，我们可以重写并重新调用stable_sort：
stable_sort(words.begin(),  words.end(), ShorterString());
第三个实参是新构建的ShorterString对象，当stable_sort内部的代码每次比较两个string时就会“调用”这一对象，此时该对象将调用运算符的函数体，判断第一个string的大小小于
第二个时返回true。

表示lambda及相应捕获行为的类
当一个lambda表达式通过引用捕获变量时，将由程序负责确保lambda执行时引用所引的对象确实存在。因此，编译器可以直接使用该引用而无须在lambda产生的类中将其存储为数
据成员。
相反，通过值捕获的变量被拷贝到lambda中。因此，这种lambda产生的类必须为每个值捕获的变量建立对应的数据成员，同时创建构造函数，令其使用捕获的变量的值来初始化数据
成员。举例：有一个lambda，它的作用是找到第一个长度不小于给定string对象：
//获得第一个指向满足条件元素的迭代器，该元素满足size() is >= sz
auto wc = find_if(words.begin(), words.end(),
																[sz](const string &a)
																{
																	return a.size() >= sz;
																});
该lambda表达式产生的类将形如：
class SizeComp
{
public:
	SizeComp(size_t n) :
		sz(n)
		{
		}																	//该形参对应捕获的变量

	//该调用运算符的返回类型、形参和函数体都与lambda一致
	bool operator()(const string &s) const
	{
		return s.size() >= sz;
	}

private:
	size_t sz;															//该数据成员对应通过值捕获的变量
};
和我们的ShorterString类不同，上面这个类含有一个数据成员以及一个用于初始化该成员的构造函数。这个合成的类不含有默认构造函数，因此要想使用这个类必须提供一个实参：
//获得第一个指向满足条件元素的迭代器，该元素满足size() is >= sz
auto wc = find_if(words.begin(), words.end(), SizeComp(sz));
lambda表达式产生的类不含默认构造函数、赋值运算符及默认析构函数。它是否含有默认的拷贝/移动构造函数则通常要视捕获的数据成员类型而定。

标准库定义的函数对象
标准库定义了一组表示算术运算符、关系运算符和逻辑运算符的类，每个类分别定义了一个执行命名操作的调用运算符。例如plus类定义了一个函数调用运算符用于对一对运算对象执
行+的操作。modulus类定义了一个调用运算符执行二元的%操作。equal_to类执行==，等等。
这些类都被定义成模板的形式，我们可以为其指定具体的应用类型，这里的类型即调用运算符的形参类型。例如，plus<string>令string加法运算符作用于string对象。plus<int>的
运算对象是int，plus<Sales_data>对Sales_data对象执行加法运算，以此类推：
plus<int> intAdd;													//可执行int加法的函数对
negate<int> intNegate;											//可对int值取反的函数对象
//使用intAdd::operator(int, int)求10和20的和
int sum = intAdd(10, 20);										//等价于sum = 30
sum = intNegate(intAdd(10, 20));								//等价于sum = 30
//使用intNegate::operator(int)生成-10，然后将-10作为intAdd::operator(int, int)的第二个参数
sum = intAdd(10, intNegate(10));								//sum = 0

下面列出的标准库函数对象都定义在functional头文件中：
算术：
plus<Type>,mimus<Type>,multiplies<Type>,divides<Type>,modulus<Type>,negate<Type>
关系：
equal_to<Type>,not_equal_to<Type>,greater<Type>,greater_equal<Type>,less<Type>,less_equal<Type>
逻辑：
logical_and<Type>,logical_or<Type>,logical_not<Type>

在算法中使用标准库函数对象
表示运算符的函数对象类常用来替换算法中的默认运算符。如我们所知，在默认情况下排序算法使用operator<将序列按照升序排列。如果要执行降序排列的话，我们可以传入一个
greater类型的对象。该类将产生一个调用运算符并负责执行待排序类型的大于运算。例如，如果svec是一个vector<string>，
//传入一个临时的函数对象用于执行两个string对象的>比较运算
sort(svec.begin(), svec.end(), greater<string>());
则上面的语句将按照降序对svec进行排序。第三个实参是greater<string>类型的一个未命名的对象，因此当sort比较元素时，不再是使用默认的<运算符，而是调用给定的greater
函数对象。该对象负责在string元素之间执行>比较运算。
需要特别注意的是，标准库规定其函数对象对于指针同样适用。我们知道比较两个无关指针将产生未定的行为，然而我们可能会希望通过比较指针的内存地址来sort指针的vector。直
接这么做将产生未定义的行为，因此我们可以使用一个标准库函数对象来实现该目的：
vector<string *> nameTable;									//vector of pointers
//错误，nameTable中的指针彼此之间没有关系，所以<将产生未定义的行为
sort(nameTable.begin(), nameTable.end(),
[](string *a, string *b) { return a < b; });
//正确，标准库规定指针的less是定义良好的
sort(nameTable.begin(), nameTable.end(), less<string*>());

可调用对象与function
C++语言中有几种可调用的对象：函数、函数指针、lambda表达式、bind创建的对象以及重载了函数调用运算符的类。
和其他对象一样，可调用的对象也有类型。例如，每个lambda有它自己唯一的(未命名)类类型；函数及函数指针的类型则由其返回值类型和实参类型决定，等等。
然而，两个不同类型的可调用对象却可能共享同一种调用形式(call signature)。调用形式指明了调用返回的类型以及传递给调用的实参类型。一种调用形式对应一个函数类型，例如：
int(int,int)
是一个函数类型，它接受两个int，返回一个int。
不同类型可能具有相同的调用形式
对于几个可调用对象共享同一种调用形式的情况，有时我们会希望把它们看成具有相同的类型。例如，考虑下列不同类型的可调用对象：
//普通函数
int add(int i, int j) { return i + j; }
//lambda，其产生一个未命名的函数对象类
auto mod = [](int i, int j) { return i % j; };
//函数对象类
struct div
{
	int operator()(int denominator, int divisor)
	{
		return denominator / divisor;
	}
};
上面这些可调用对象分别对其参数执行了不同的算术运算，尽管它们的类型各不相同，但是共享同一种调用形式：
int(int,int)

举例：使用这些可调用对象构建一个简单的桌面计算器。
为了实现这一目的，需要定义一个函数表(function table)用于存储指向这些可调用对象的“指针”。当程序需要执行某个特定的操作时，从表中查找该调用的函数。
在C++语言中，函数表很容易通过map来实现。对于此例来说，我们使用一个表示运算符符号的string对象作为关键字，使用实现运算符的函数作为值。当我们需要求给定运算符的值
时，先通过运算符索引map，然后调用找到的那个元素。
假定我们的所有函数都相互独立，并且只处理关于int的二元运算，则map可以定义成如下的形式：
//构建从运算符到函数指针的映射关系，其中函数接受两个int、返回一个int
map<string, int(*)(int,int)> binops;
我们可以按照下面的形式将add的指针添加到binops中：
//正确，add是一个指向正确类型函数的指针
binops.insert({"+", add});													//{"+", add} is a pair
但是我们不能将mod或者divide存入binops：
binops.insert({"%",  mod});													//错误，mod不是一个函数指针
问题在于mod是个lambda表达式，而每个lambda有它自己的类类型，该类型与存储在binops中的值的类型不匹配。

标准库function类型
C++11：
我们可以使用一个名为function的新的标准库类型解决上述问题，function定义在头文件functional中。
function的操作：
function<T> f;
f是一个用来存储可调用对象的空function。这些可调用对象的调用形式应该与函数类型T相同。

function<T> f(nullptr);
显式地构造一个空function。

function<T> f(obj);
在f中存储可调用对象obj的副本。

f
将f作为条件：当f含有一个可调用对象时为真，否则为假。

f(args)
调用f中的对象，参数是args。

定义为function<T>的成员的类型：
result_type
该function类型的可调用对象返回的类型。

argument_type,first_argument_type,second_argument_type
当T有一个或两个实参时定义的类型。如果T只有一个实参，则argument_type是该类型的同义词；如果T有两个实参，则first_argument_type和second_argument_type分别代表
两个实参的类型。

function是一个模板，和我们使用过的其他模板一样，当创建一个具体的function类型时我们必须提供额外的信息。在此例中，所谓额外的信息是指该function类型能够表示的对象的
调用形式。参考其他模板，我们在一对尖括号内指定类型：
function<int(int,  int)>
在这里我们声明了一个function类型，它可以表示接受两个int、返回一个int的可调用对象。因此，我们可以用这个新声明的类型表示任意一种桌面计算器用到的类型：
function<int(int,  int)> f1 = add;										//函数指针
function<int(int, int)> f2 = div();										//函数对象类的对象
function<int(int, int)> f3 = [](int i, int j) { return i * j; };	//lambda
cout << f1(4,2) << endl;													//prints 6
cout << f2(4,2) << endl;													//prints 2
cout << f3(4,2) << endl;													//prints 8
使用这个function类型我们可以重新定义map：
//列举了可调用对象与二元运算符对应关系的表格
//所有可调用对象都必须接受两个int、返回一个int
//其中的元素可以是函数指针、函数对象或者lambda
map<string, function<int(int, int)>> binops;
我们能把所有可调用对象，包括函数指针、lambda或者函数对象在内，都添加到这个map中：
map<string,  function<int(int, int)>> binops =
{
	{"+", add},																//函数指针
	{"-", std::minus<int>()},											//标准库函数对象
	{"/",  div()},																//用户定义的函数对象
	{"*", [](int i, int j) { return i * j; }},								//未命名的lambda
	{"%", mod}																//命名了的lambda对象
};
我们的map中包含5个元素，尽管其中的可调用对象的类型各不相同，我们仍然能够把所有这些类型都存储在同一个function<int (int,int)>类型中。

一如往常，当我们索引map时将得到关联值的一个引用。如果我们索引binops，将得到function对象的引用。function类型重载了调用运算符，该运算符接受它自己的实参然后将其
传递给存好的可调用对象：
binops["+"](10,  5);														//调用add(10, 5)
binops["-"](10, 5);															//使用minus<int>对象的调用运算符
binops["/"](10, 5);															//使用divide对象的调用运算符
binops["*"](10, 5);															//调用lambda函数对象
binops["%"](10, 5);														//调用lambda函数对象
我们依次调用了binops中存储的每个操作。在第一个调用中，我们获得的元素存放着一个指向add函数的指针，因此调用binops["+"](10,  5)实际上是使用该指针调用add，并传入
10和5。在接下来的调用中，binops["-"]返回一个存放着std::minus<int>类型对象的function，我们将执行该对象的调用运算符。
重载的函数与function
我们不能(直接)将重载函数的名字存入function类型的对象中：
int add(int i, int j) { return i + j; }
Sales_data add(const Sales_data&, const Sales_data&);
map<string, function<int(int, int)>> binops;
binops.insert( {"+", add} );												//error: which add?
解决上述二义性问题的一条途径是存储函数指针而非函数的名字：
int (*fp)(int,int) = add;													//指针所指的add是接受两个int的版本
binops.insert( {"+", fp} );												//正确，fp指向一个正确的add版本
同样，我们也能使用lambda来消除二义性：
//正确，使用lambda来指定我们希望使用的add版本
binops.insert( {"+", [](int a, int b) {return add(a, b);} });
lambda内部的函数调用传入了两个int，因此该调用只能匹配接受两个int的add版本，而这也正是执行lambda时真正调用的函数。
新版本标准库中的function类与旧版本中的unary_function和binary_function没有关联，后两个类已经被更通用的bind函数替代了。

标准库bind函数
定义在头文件functional中。
可以将bind函数看做一个通用的函数适配器，它接受一个可调用对象，生成一个新的可调用对象来“适应”原对象的参数列表。
调用bind的一般形式为：
auto newCallable = bind(callable,arg_list);
其中，newCallable本身是一个可调用对象，arg_list是一个逗号分隔的参数列表，对应给定的callable的参数。即，当我们调用newCallable时，newCallable会调用callable，并传递给它arg_list中的参数。
arg_list中的参数可能包含形如_n的名字，其中n是一个整数。这些参数是“占位符”，表示newCallable的参数，它们占据了传递给newCallable的参数的“位置”。数值n表示生成的可调用对象中参数的位置：_1为newCallable的第一个参数，_2为第二个参数，以此类推。

bool check_size(const string &s,string::size_type sz)
{
return s.size() >= sz;
}

作为一个简单的例子，我们将使用bind生成一个调用check_size的对象，如下所示，它用一个定值作为其大小参数来调用check_size：
//check6是一个可调用对象，接受一个string类型的参数，并用此string和值6来调用check_size
auto check6 = bind(check_size,std::placeholders::_1,6);
此bind调用只有一个占位符，表示check6只接受单一参数。占位符出现在arg_list的第一个位置，表示check6的此参数对应check_size的第一个参数。此参数是一个const string&。因此，调用check6必须传递给它一个string类型的参数，check6会将此参数传递给check_size。
string s = "hello";
bool b1 = check6(s);														//check6(s)会调用check_size(s,6)
使用bind，我们可以将原来基于lambda的find_if调用：
auto wc = find_if(words.begin(),words.end(),
																[](string const& s)
																{
																	return s.size() >= sz;
																}
																);
替换为如下使用check_size的版本：
auto wc = find_if(words.begin(),words.end(),
																bind(check_size,std::placeholders::_1,sz));
此bind调用生成一个可调用对象，将check_size的第二个参数绑定到sz的值。当find_if对words中的string调用这个对象时，这些对象会调用check_size，将给定的string和sz传递
给它。

bind的参数
我们可以用bind修正参数的值。更一般的，可以用bind绑定给定可调用对象中的参数或重新安排其顺序。例如，假定f是一个可调用对象，它有5个参数，则下面对bind的调用：
//g是一个有两个参数的可调用对象
auto g = bind(f,a,b,_2,c,_1);
生成一个新的可调用对象，它有两个参数，分别用占位符_2和_1表示。这个新的可调用对象将它自己的参数作为第三个和第五个参数传递给f。f的第一个，第二个和第四个参数分别被
绑定到给定的值a，b和c上。
传递给g的参数按位置绑定到占位符。即，第一个参数绑定到_1，第二个参数绑定到_2。因此，当我们调用g时，其第一个参数将被传递给f作为最后一个参数，第二个参数将被传递给
f作为第三个参数。实际上，这个bind调用会将
g(_1,_2)
映射为
f(a,b,_2,c,_1)
即，对g的调用会调用f，用g的参数代替占位符，在加上绑定的参数a，b和c。

绑定引用参数
默认情况下，bind的那些不是占位符的参数被拷贝到bind返回的可调用对象中。但是，与lambda类似，有时对有些绑定的参数我们希望以引用方式传递，或是要绑定参数的类型无法
拷贝。
如果我们希望传递给bind一个对象而又不拷贝它，就必须使用标准库ref函数：
for_each(words.begin(),words.end(),bind(print,ref(os),_1,''));
函数ref返回一个对象，包含给定的引用，此对象是可以拷贝的。标准库中还有一个cref函数，生成一个保存const引用的类。
与bind一样，函数ref和cref也定义在头文件functional中。
*/