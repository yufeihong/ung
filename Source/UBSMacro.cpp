/*
层次依赖顺序：
UBS
UTM
UMM
UFC
UES
UML
UUT
UPE
URM
URS
USM
*/

/*
导入和导出函数和类的可移植方式：
有两种类型的库：运行时（也称为共享或动态加载）库和静态库。
不同的平台有不同的方法来描述哪些符号必须从共享库导出。
使用Boost以一个可移植的方式管理符号的可见性。
异常和使用dynamic_cast强制转换的类，必须用BOOST_SYMBOL_VISIBLE声明。
作为开发人员，你必须告诉编译器，我们现在导出这些方法给用户。
用户必须告诉编译器，他要从库中导入方法。
*/

/*
导出符号：
除了语言级别的访问控制特性(公有、私有和受保护)外，还有两个相关的概念允许暴露API中的符号，它们位于物理文件层次，分别是：
1：外部链接
2：导出可见性
外部链接这个术语是指一个编译单元中的一个符号可以被其他编译单元访问，而导出是只在库文件(比如DLL)中可见的符号。
只有外部链接符号才可以导出。

外部链接：
这是决定客户是否可以访问共享库中符号的第一步。特别地，.cpp文件中的全局(文件作用域)自由函数和变量会有外部链接，除非采取措施进行阻止。
比如下列代码出现在一个cpp文件中：
const int INTERNAL_CONST = 42;
String Filename = "file.txt";
void FreeFunction()
{
	std::cout << "Free function called" << std::endl;
}
尽管已经将这些函数和变量的使用限制在一个.cpp文件之内，但是客户仍可以轻松地在自己的程序中访问这些符号(暂时忽略符号导出问题)。这样他们就能够
不通过公有API直接调用全局函数并修改全局状态，从而破坏封装性。比如：
extern void FreeFunction();
extern const int INTERNAL_CONST;
extern String Filename;

//调用模块内部的函数
FreeFunction();
//访问模块内部定义的常量
std::cout << "Constant = " << INTERNAL_CONST << std::endl;
//修改模块内部的全局状态
Filename = "different.txt";
对于这种外部链接泄露问题，有以下几种解决方法：
1：静态声明。使用static关键字声明函数和变量，它将函数或变量指定为内部链接，从而不能在其所在的编译单元之外访问。
2：匿名命名空间。把文件作用域内的函数和变量封装进匿名命名空间。这是一种更好的办法，因为它能避免污染全局命名空间。可以向下面这样实现：
namespace
{
	const int INTERNAL_CONST = 42;
	String Filename = "file.txt";
	void FreeFunction()
	{
		std::cout << "Free function called" << std::endl;
	}
}

对那些具有外部链接的符号，还需要考虑导出符号这一概念，它决定一个符号在共享库中是否可见。
大多数编译器为类和函数提供了修饰符，允许显式指定一个符号是否会出现在库文件的导出符号表中。
Microsoft Visual Studio：DLL中的符号默认是不可访问的。必须显式导出DLL中的函数、类和变量，从而允许客户访问它们。可以在符号前使用__declspec
修饰符。例如，当构建DLL时，可以通过指定__declspec(dllexport)导出符号。然后客户在自己的程序中指定__declspec(dllimport)才能访问相同的符号。
*/

/*
可以导出的：
普通的-成员函数：
1，构造函数
2，析构函数
3，成员函数
4，静态成员函数(只需在函数声明时，在static关键字前面加上UngExport就可以了)

虚-成员函数：
1，析构函数
2，成员函数

关系：
继承

模板：
不用导出，在主程序中实例化即可

运行时链接的时候，就不需要#pragma .lib了，也就是说，不需要导入库了。

警告：
STL会产生4251警告，屏蔽。

不用导出的：
子类

更新：
可以只编译产生出新的dll，而主程序不变，但是主程序的运行中却能把新的dll给表现出来。

1，使用DLL。
2，DLL显式运行时链接。
*/