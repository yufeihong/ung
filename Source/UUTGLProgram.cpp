#include "UUTGLProgram.h"

#ifdef UUT_HIERARCHICAL_COMPILE

#include "UUTGLShader.h"

namespace ung
{
	GLProgram::GLProgram() :
		mProgram(glCreateProgram())
	{
	}

	GLProgram::~GLProgram()
	{
		detachAll();

		glDeleteProgram(mProgram);
	}

	void GLProgram::attach(std::shared_ptr<GLShader> shader)
	{
		BOOST_ASSERT(glIsProgram(mProgram));
		GLuint shaderObject = shader->getShaderObject();
		glAttachShader(mProgram, shaderObject);
	}

	void GLProgram::detach(std::shared_ptr<GLShader> shader)
	{
		for (auto& element : mShaders)
		{
			if (element == shader)
			{
				glDetachShader(mProgram, element->getShaderObject());
			}
		}
	}

	void GLProgram::detachAll()
	{
		for (auto& shader : mShaders)
		{
			glDetachShader(mProgram, shader->getShaderObject());
		}
	}

	void GLProgram::link()
	{
		glLinkProgram(mProgram);

		GLint linked;
		glGetProgramiv(mProgram, GL_LINK_STATUS, &linked);
		BOOST_ASSERT(linked);
	}

	void GLProgram::use()
	{
		glUseProgram(mProgram);
	}

	GLuint const GLProgram::getProgram() const
	{
		return mProgram;
	}

	void GLProgram::insert(std::shared_ptr<GLShader> shader)
	{
		mShaders.push_back(shader);
	}

	void GLProgram::erase(std::shared_ptr<GLShader> shader)
	{
		mShaders.remove(shader);
	}

	void GLProgram::eraseAll()
	{
		mShaders.clear();
	}
}//namespace ung

#endif//UUT_HIERARCHICAL_COMPILE