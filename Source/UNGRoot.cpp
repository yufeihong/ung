#include "UNGRoot.h"

#ifdef UNG_HIERARCHICAL_COMPILE

#include "UIFIRenderSystem.h"

#include "UNGStatistics.h"

namespace ung
{
	Root::Root() :
		mCurrentRenderSystem(nullptr),
		mElapsedTime(0.0)
	{
		UNG_LOG_START("Start UNG.");

		/*
		不能给Root定义一个成员变量，比如mBigDogManager，然后在Root构造函数的初始化列表中对
		mBigDogManager进行初始化，比如：
		mBigDogManager(BigDog::getInstance())，这样会导致Root对象先于其它的单件对象进行了
		析构行为。
		*/
		
		/*
		不能在这里调用init()，否则客户代码还没有创建特定空间管理器的SceneManager呢，就先操作
		插件了，操作插件不要紧，但是如果插件代码中使用了比如：
		Root::getInstance().getSpatialManagerEnumerator().addFactory(...);
		这个时候，Root::getInstance()函数会进入未定义行为。
		*/

		/*
		不能在这里进行单件的析构登记，这样会导致Root对象先于其它的单间对象进行了析构行为。
		*/
	}

	Root::~Root()
	{
		UNG_PRINT_ONE_LINE("==========析构Root==========");

		//Root一定会先于LogManager之前析构的。
		UNG_LOG_END("Shut down UNG.");
	}

	void Root::init()
	{
		/*
		按照下面的顺序来进行析构登记(先进后出)。
		必须确保SpatialManagerEnumerator在PluginManager之后析构。
		*/
		BigDog::getInstance();
		Platform::getInstance();
		Filesystem::getInstance();
		SpatialManagerEnumerator::getInstance();
		PluginManager::getInstance();
		EventManager::getInstance();
		Statistics::getInstance();

		//--------------------------------------

		UNG_LOG_START("Initialize UBS.");
		ubsInit();
		UNG_LOG_END("Initialize UBS.");

		urmInit();

		usmInit();

		//临时
		//mCurrentRenderSystem = mRenderSystems["plugin_UD9"];
		mCurrentRenderSystem = mRenderSystems["plugin_UG3"];
	}

	void Root::run()
	{
		BOOST_ASSERT(mCurrentRenderSystem);
		BOOST_ASSERT(mCurrentRenderSystem->isInitialized());

		//构造一个毫秒Timer来计算上一帧耗去的时间
		SteadyClock_ms elapsedTimer;

#if UNG_PLATFORM == UNG_PLATFORM_WIN32
		/*
		PeekMessage和GetMessage的区别：
		二者都是到消息队列中抓取消息，如果抓不到，程序中的主执行线程会被系统挂起，当系统再次回来照顾
		这个线程时，发现消息队列中仍然是空的，这时候两个API的行为就不同了：
		GetMessage，会过门不入，操作系统会再去照顾其他人。
		PeekMessage，会取回控制权，使程序得以执行一段时间。

		Windows为当前运行的每个Windows程序维护了一个消息队列。
		当发生输入事件后，Windows将事件转化为一个消息，并将它放入程序的消息队列中。

		从消息队列中取出消息：
		Windows Message Loop (NULL means check all HWNDs belonging to this context).

		PM_REMOVE:
		消息被PeekMessage()处理后，将其从消息队列中删除。
		*/
		MSG msg;
		SecureZeroMemory(&msg, sizeof(MSG));
		while (msg.message != WM_QUIT)
		{
			if(PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
			{
				//将msg结构传递给Windows，进行一些键盘转换
				TranslateMessage(&msg);
				/*
				再将msg结构传递给Windows。
				通过Windows USER模块，把消息分派给窗口过程。
				然后，Windows将该消息发送给适当的窗口，让它们进行处理。
				DispatchMessage() calls windowProc,which handles messages in the queue.
				*/
				DispatchMessage(&msg);
			}
#endif
			else
			{
				//上一帧所消耗的时间(单位:毫秒)
				mElapsedTime = elapsedTimer.elapsed();
				//重新开始这一帧的计时
				elapsedTimer.restart();

				update(mElapsedTime);

				render(mElapsedTime);
			}
		}//end while
	}

	void Root::update(real_type dt)
	{
		//更新统计数据
		getStatistics().update(dt);

		//更新渲染器
		mCurrentRenderSystem->update(dt);
	}

	void Root::render(real_type dt)
	{
		//渲染一帧
		mCurrentRenderSystem->renderOneFrame(dt);
	}

	void Root::close()
	{
		//关闭渲染器
		mCurrentRenderSystem->shutdown();

		/*
		销毁所有的场景管理器。
		通过让Root持有场景管理器的强引用，可以很清晰的控制场景管理器是析构时间。
		*/
		auto beg = mSceneManagers.begin();
		auto end = mSceneManagers.end();

		while (beg != end)
		{
			auto sm = beg->second;

			destroySceneManager(sm->getName());

			++beg;
		}

		/*
		先前的reset会使元素的析构函数在while循环遍历完成退出之后，而在clear函数被调用之前之前被调用，
		此时的clear，只是清空一些empty Strong指针。
		*/
		mSceneManagers.clear();
	}

	void Root::setElapsedTime(real_type dt)
	{
		mElapsedTime = dt;
	}

	real_type Root::getElapsedTime()
	{
		return mElapsedTime;
	}

	void Root::destroySceneManager(String const& name)
	{
		auto findRet = mSceneManagers.find(name);
		BOOST_ASSERT(findRet != mSceneManagers.end());

		auto& sm = findRet->second;

		//销毁该场景管理器的场景图的根节点
		auto root = sm->getSceneRootNode();
		/*
		这里之所以把根节点放在这里进行销毁，而不是放在场景管理器的析构函数中进行销毁，是因为如果在
		析构函数中进行销毁的话，那个时候场景管理器的智能指针的所有强引用已经释放了，弱引用也过期了，
		而在销毁场景节点的过程中，是要使用一个有效的场景管理器指针的。
		*/
		sm->destroySceneNode(root);

		//自定义智能指针deleter
		sm.reset();
	}

	StrongISceneManagerPtr Root::getSceneManager(String const& name) const
	{
		auto findRet = mSceneManagers.find(name);
		if (findRet == mSceneManagers.end())
		{
			UNG_EXCEPTION("The name specified by the parameter error.");
		}

		return findRet->second;
	}

	void Root::addRenderSystem(String const & name, IRenderSystem * rs)
	{
		auto findRet = mRenderSystems.find(name);
		if (findRet != mRenderSystems.end())
		{
			UNG_EXCEPTION("This render system has been added.");
		}

		mRenderSystems[name] = rs;
	}

	void Root::removeRenderSystem(String const & name)
	{
		auto findRet = mRenderSystems.find(name);
		if (findRet == mRenderSystems.end())
		{
			UNG_EXCEPTION("Can not find this render system.");
		}

		auto eraseRet = mRenderSystems.erase(name);
		BOOST_ASSERT(eraseRet == 1);
	}

	IRenderSystem* Root::getRenderSystem(String const & name) const
	{
		auto findRet = mRenderSystems.find(name);
		if (findRet == mRenderSystems.end())
		{
			return nullptr;
		}

		return findRet->second;
	}

	IRenderSystem* Root::getCurrentRenderSystem() const
	{
		BOOST_ASSERT(mCurrentRenderSystem);

		/*
		初始化渲染系统
		放在这里初始化，是因为单元测试其它内容时，由于shutdown没有在卸载插件时调用，故，会表现出
		一些内存没有释放。
		*/
		if (!mCurrentRenderSystem->isInitialized())
		{
			mCurrentRenderSystem->initialize();
		}

		return mCurrentRenderSystem;
	}

	RenderSystemType const& Root::getCurrentRenderSystemType() const
	{
		BOOST_ASSERT(mCurrentRenderSystem);

		static RenderSystemType type;

		type = mCurrentRenderSystem->getType();

		return type;
	}

	LogManager& Root::getLogManager()
	{
		return LogManager::getInstance();
	}

#if UNG_DEBUGMODE
	BigDog& Root::getBigDogManager()
	{
		return BigDog::getInstance();
	}
#endif

	Platform& Root::getPlatformManager()
	{
		return Platform::getInstance();
	}

	Filesystem& Root::getFilesystemManager()
	{
		return Filesystem::getInstance();
	}

	PluginManager& Root::getPluginManager()
	{
		return PluginManager::getInstance();
	}

	EventManager& Root::getEventManager()
	{
		return EventManager::getInstance();
	}

	SpatialManagerEnumerator& Root::getSpatialManagerEnumerator()
	{
		return SpatialManagerEnumerator::getInstance();
	}

	Statistics& Root::getStatistics()
	{
		return Statistics::getInstance();
	}
}//namespace ung

#endif//UNG_HIERARCHICAL_COMPILE

/*
boost::any要求value type必须可拷贝，且其析构函数不能抛出异常。
*/

/*
关于智能指针的自定义析构器(删除器)：
元素通过内存池分配后，给予其智能指针一个自定义的deleter，然后将智能指针存放入未使用内存池分配策
略的容器中，当容器clear时，会崩溃，解决方法是：在clear之前，先reset智能指针，让对象先释放。
元素通过内存池分配后，给予其智能指针一个自定义的deleter，然后将智能指针存放入使用了内存池分配策
略的容器中，当容器clear时，不会崩溃，但是会造成内存池异常，具体表现为内存池剩余空间加上空闲列表的
剩余空间居然比总共从内存条中拿走的空间多出来了48个字节。解决方法是：在clear之前，先reset智能指针，
让对象先释放。
如果没有给予其智能指针一个自定义的deleter的话，那么使用定制分配器的容器就不需要先reset了，直接clear
就行。但是使用默认分配器的容器还是需要先reset。
这样会产生一个问题，也就是但凡对象是从内存池分配的，无论其智能指针是否自定义了一个deleter，将来如果
要统一切换为使用默认分配器的容器的话，都需要先reset。目前能想到的解决方法是对容器进行一层包装，在
包装内的clear之前，先reset。
*/

/*
单件构造顺序：
LogManager
Root
BigDog
Platform
Filesystem
SpatialManagerEnumerator
PluginManager
EventManager
场景管理器
析构顺序刚好相反。
*/