#include "URMUtilities.h"

#ifdef URM_HIERARCHICAL_COMPILE

#include "UNGRoot.h"
#include "UIFIRenderSystem.h"

namespace ung
{
	VertexElementType UNG_STDCALL ungProcessVertexElementType(VertexElementType vet)
	{
		/*
		Refine(提炼) color type to a specific type
		根据渲染系统(无法获取渲染系统信息的时候根据平台)来得到一个最适合的.
		*/
		if (vet == VertexElementType::VET_COLOR)
		{
			auto pRS = Root::getInstance().getCurrentRenderSystem();

			if (pRS)
			{
				return pRS->getNativeColorType();
			}
			else
			{
#if UNG_PLATFORM == UNG_PLATFORM_WIN32
				return VertexElementType::VET_COLOR_ARGB;		//prefer D3D format on windows
#else
				return VertexElementType::VET_COLOR_ABGR;		//prefer GL format on everything else
#endif
			}
		}

		return vet;
	}
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE