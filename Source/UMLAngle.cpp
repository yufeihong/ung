#include "UMLAngle.h"

#ifdef UML_HIERARCHICAL_COMPILE

#include "UMLMath.h"
#include "UMLTypedef.h"

namespace ung
{
	Degree::Degree(real_type d) :
		mDegree(d)
	{
	}

	real_type Degree::getDegree() const
	{
		return mDegree;
	}

	Radian Degree::toRadian() const
	{
		return Radian(Math::degreeToRadian(mDegree));
	}

	Degree Degree::operator-() const
	{
		return Degree(-mDegree);
	}

	Degree Degree::operator+(real_type d) const
	{
		return Degree(mDegree + d);
	}

	Degree & Degree::operator+=(real_type d)
	{
		mDegree += d;

		return *this;
	}

	Degree Degree::operator-(real_type d) const
	{
		return Degree(mDegree - d);
	}

	Degree & Degree::operator-=(real_type d)
	{
		mDegree -= d;

		return *this;
	}

	Degree Degree::operator*(real_type d) const
	{
		return Degree(mDegree * d);
	}

	Degree & Degree::operator*=(real_type d)
	{
		mDegree *= d;

		return *this;
	}

	Degree Degree::operator+(Degree d)
	{
		return Degree(mDegree + d.getDegree());
	}

	Degree& Degree::operator+=(Degree d)
	{
		mDegree += d.getDegree();

		return *this;
	}

	Degree Degree::operator-(Degree d)
	{
		return Degree(mDegree - d.getDegree());
	}

	Degree& Degree::operator-=(Degree d)
	{
		mDegree -= d.getDegree();

		return *this;
	}

	bool Degree::operator==(real_type d)
	{
		return Math::isEqual(mDegree, d);
	}

	bool Degree::operator!=(real_type d)
	{
		return !operator==(d);
	}

	bool Degree::operator==(Degree d)
	{
		return Math::isEqual(mDegree,d.getDegree());
	}

	bool Degree::operator!=(Degree d)
	{
		return !operator==(d);
	}

	bool Degree::operator>(real_type d)
	{
		return mDegree > d;
	}

	bool Degree::operator<(real_type d)
	{
		return mDegree < d;
	}

	bool Degree::operator>=(real_type d)
	{
		return mDegree >= d;
	}

	bool Degree::operator<=(real_type d)
	{
		return mDegree <= d;
	}

	bool Degree::operator>(Degree d)
	{
		return mDegree > d.mDegree;
	}

	bool Degree::operator<(Degree d)
	{
		return mDegree < d.mDegree;
	}

	bool Degree::operator>=(Degree d)
	{
		return mDegree >= d.mDegree;
	}

	bool Degree::operator<=(Degree d)
	{
		return mDegree <= d.mDegree;
	}

	Degree operator+(real_type d1, Degree d2)
	{
		return d2 + d1;
	}

	Degree operator-(real_type d1, Degree d2)
	{
		return d2 - d1;
	}

	Degree operator*(real_type d1, Degree d2)
	{
		return d2 * d1;
	}

	bool operator==(real_type d1, Degree d2)
	{
		return d2 == d1;
	}

	bool operator!=(real_type d1, Degree d2)
	{
		return !operator==(d1, d2);
	}

	bool operator>(real_type d1, Degree d2)
	{
		return d2 > d1;
	}

	bool operator<(real_type d1, Degree d2)
	{
		return d2 < d1;
	}

	bool operator>=(real_type d1, Degree d2)
	{
		return d2 >= d1;
	}

	bool operator<=(real_type d1, Degree d2)
	{
		return d2 <= d1;
	}

	//---------------------------------------------------------

	Radian::Radian(real_type r) :
		mRadian(r)
	{
	}

	real_type Radian::getRadian() const
	{
		return mRadian;
	}

	Degree Radian::toDegree() const
	{
		return Degree(Math::radianToDegree(mRadian));
	}

	Radian Radian::operator-() const
	{
		return Radian(-mRadian);
	}

	Radian Radian::operator+(real_type r) const
	{
		return Radian(mRadian + r);
	}

	Radian& Radian::operator+=(real_type r)
	{
		mRadian += r;

		return *this;
	}

	Radian Radian::operator-(real_type r) const
	{
		return Radian(mRadian - r);
	}

	Radian& Radian::operator-=(real_type r)
	{
		mRadian -= r;

		return *this;
	}

	Radian Radian::operator*(real_type r) const
	{
		return Radian(mRadian * r);
	}

	Radian& Radian::operator*=(real_type r)
	{
		mRadian *= r;

		return *this;
	}

	Radian Radian::operator+(Radian r)
	{
		return Radian(mRadian + r.getRadian());
	}

	Radian& Radian::operator+=(Radian r)
	{
		mRadian += r.getRadian();

		return *this;
	}

	Radian Radian::operator-(Radian r)
	{
		return Radian(mRadian - r.getRadian());
	}

	Radian& Radian::operator-=(Radian r)
	{
		mRadian -= r.getRadian();

		return *this;
	}

	bool Radian::operator==(real_type r)
	{
		return Math::isEqual(mRadian,r);
	}

	bool Radian::operator!=(real_type r)
	{
		return !operator==(r);
	}

	bool Radian::operator==(Radian r)
	{
		return Math::isEqual(mRadian,r.getRadian());
	}

	bool Radian::operator!=(Radian r)
	{
		return !operator==(r);
	}

	bool Radian::operator>(real_type d)
	{
		return mRadian > d;
	}

	bool Radian::operator<(real_type d)
	{
		return mRadian < d;
	}

	bool Radian::operator>=(real_type d)
	{
		return mRadian >= d;
	}

	bool Radian::operator<=(real_type d)
	{
		return mRadian <= d;
	}

	bool Radian::operator>(Radian d)
	{
		return mRadian > d.mRadian;
	}

	bool Radian::operator<(Radian d)
	{
		return mRadian < d.mRadian;
	}

	bool Radian::operator>=(Radian d)
	{
		return mRadian >= d.mRadian;
	}

	bool Radian::operator<=(Radian d)
	{
		return mRadian <= d.mRadian;
	}

	Radian operator+(real_type r1, Radian r2)
	{
		return r2 + r1;
	}

	Radian operator-(real_type r1, Radian r2)
	{
		return r2 - r1;
	}

	Radian operator*(real_type r1, Radian r2)
	{
		return r2 * r1;
	}

	bool operator==(real_type r1, Radian r2)
	{
		return r2 == r1;
	}

	bool operator!=(real_type r1, Radian r2)
	{
		return !operator==(r1, r2);
	}

	bool operator>(real_type d1, Radian d2)
	{
		return d2 > d1;
	}

	bool operator<(real_type d1, Radian d2)
	{
		return d2 < d1;
	}

	bool operator>=(real_type d1, Radian d2)
	{
		return d2 >= d1;
	}

	bool operator<=(real_type d1, Radian d2)
	{
		return d2 <= d1;
	}
}//namespace ung

#endif//UML_HIERARCHICAL_COMPILE