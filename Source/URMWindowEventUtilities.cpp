#include "URMWindowEventUtilities.h"

#ifdef URM_HIERARCHICAL_COMPILE

#include "UIFIRenderSystem.h"
#include "UIFIWindow.h"

//
#include "UNGRoot.h"

namespace ung
{
	/*
	如果程序有消息需要处理，则先处理消息，然后再渲染图形。如果没有消息处理，则一直不停地渲染图形。
	*/
	LRESULT WindowEventUtilities::windowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		//当前渲染系统
		auto renderSystem = Root::getInstance().getCurrentRenderSystem();

		/*
		程序通常不直接调用窗口过程。
		窗口过程通常由Windows本身调用。
		通过调用SendMessage()，程序能够直接调用自己的窗口过程。
		*/

		/*
		Windows调用过程函数，将第一个参数设为窗口句柄，第二个参数设为WM_CREATE。
		通过过程在WM_CREATE处理期间进行一次窗口初始化。

		The WM_CREATE message is sent after your window is already created.
		*/
		if (msg == WM_CREATE)
		{
			/*
			LONG_PTRSetWindowLongPtr(HWND hWnd,int nIndex,LONG_PTR dwNewLong);
			改变指定窗口的属性。函数也将指定的一个值设置在窗口的额外存储空间的指定偏移位置。
			hWnd：窗口句柄，间接给出窗口所属的类。
			nlndex：指定将设定的大于等于0的偏移值。有效值的范围从0到额外类的存储空间的字节数减去一
			个整型的大小(-sizeof(int))。要设置其他任何值，可以指定下面值之一：
			GWL_EXSTYLE				设定一个新的扩展风格。更多信息，请见CreateWindowEx。
			GWL_STYLE					设定一个新的窗口风格。
			GWL_WNDPROC				为窗口过程设置一个新的地址。
			GWL_HINSTANCE			设置一个新的应用程序实例句柄。
			GWL_ID							设置一个新的窗口标识符。
			GWL_USERDATA				设置与该窗口相关的用户数据。这些用户数据可以在程序创建该窗口时被
												使用。用户数据的初始值为0。
			当hWnd参数标识了一个对话框时，也可使用下列值：
			DWL_DLGPROC				设置对话框过程的新地址。
			DWL_MSGRESULT			设置对话框中的消息处理程序的返回值。
			DWL_USER					设置的应用程序所私有的新的额外信息，例如句柄或指针。
			dwNewLong：指定的替换值。
			如果函数成功，则返回所指定的偏移量的前一个值。
			如果函数失败，则返回0。若想获得更多的错误信息，请调用GetLastError函数。
			注意：可以存在这样一种情况，就是如果如果函数成功，且所返回的指定的偏移量的前一个值恰好为0，
			这时函数的返回值也为0。这种情况下，如果我们想知道SetWindowLongPtr()函数到底运行成功了
			没有，可以通过如下的方法得知：
			1，先调用SetLastError(0)，清除最后的错误信息。
			2，调用SetWindowLongPtr()。这时如果SetWindowLongPtr()函数的返回值为0。
			3，再调用GetLastError()，获取最后的错误信息。如果GetLastError()的返回值为非0的话，则
			SetWindowLongPtr()函数运行失败。
			*/
#if UNG_DEBUGMODE
			SetLastError(0);
#endif
			//在函数CreateWindowEx()的调用中，最后一个参数传递了this
			auto setRet = SetWindowLongPtr(hWnd,GWLP_USERDATA,
				(LONG_PTR)(((LPCREATESTRUCT)lParam)->lpCreateParams));

#if UNG_DEBUGMODE
			//setRet非0的话，说明成功
			if (setRet == 0)
			{
				auto ret = GetLastError();
				//ret为0的话，说明成功
				BOOST_ASSERT(ret == 0);
			}
#endif
			return 0;
		}

		/*
		获取窗口instance。
		LONG_PTR GetWindowLongPtr(HWND hWnd,int nIndex);
		在指定的窗口中获取信息。也可以在指定window内存偏移量的情况下获取值。
		除了set中的，还可以GWLP_HWNDPARENT：如果只有一个父窗口，获取父窗口的句柄 。
		如果函数执行成功，将返回读取的值。
		如果执行失败，将返回零，要获取更多扩展信息，请调用GetLastError.
		如果SetWindowLong or SetWindowLongPtr没有被事先调用，就在扩展窗口或类内存中调用
		GetWindowLongPtr函数，将返回零
		*/
		IWindow* window = (IWindow*)GetWindowLongPtr(hWnd, GWLP_USERDATA);
		if (!window)
		{
			/*
			LRESULT DefWindowProc(HWND hWnd,UINT Msg,WPARAM wParam,LPARAM IParam);
			调用缺省的窗口过程来为应用程序没有处理的任何窗口消息提供缺省的处理。该函数确保每一个消息
			得到处理。
			这个函数是默认的窗口处理函数，我们可以把不关心的消息都丢给它来处理。这个函数在处理关闭窗
			口消息WM_CLOSE时，是调用DestroyWindow函数关闭窗口并且发WM_DESTROY消息给应用程
			序；而它对WM_DESTROY这个消息是不处理的。我们在应用程序中对这个消息的处理是发出
			WM_QUIT消息。因此WM_CLOSE、WM_DESTROY、WM_QUIT这三个消息是先后产生的。
			当DefWindowProc处理WM_DESTROY消息时，它不自动调用PostQuitMessage。
			*/
			return DefWindowProc(hWnd,msg,wParam,lParam);
		}

		static bool paused{ false };

		//Is the application in a minimized or maximized state?
		static bool minOrMaxed{ false };

		RECT clientRect = {0, 0, 0, 0};

		switch (msg)
		{
		case WM_CREATE:
			/*
			窗口过程在处理消息时，必须返回0。
			*/
			return 0;

		/*
		WM_ACTIVE is sent when the window is activated or deactivated.
		*/
		case WM_ACTIVATE:
			if (LOWORD(wParam) == WA_INACTIVE)
			{
				//暂停
				paused = true;
			}
			else
			{
				//继续
				paused = false;
			}
			return 0;

		/*
		不要处理这个消息。
		At the very least, a window procedure should process the WM_PAINT message to draw 
		itself.
		case WM_PAINT:
			//Paint the window's client area. 
			return 0;
		*/

		/*
		WM_SIZE is sent when the user resizes the window.
		*/
		case WM_SIZE:
			renderSystem->setBackBufferWH(LOWORD(lParam),HIWORD(lParam));

			if (wParam == SIZE_MINIMIZED)
			{
				paused = true;
				minOrMaxed = true;

				//mined
				window->setMinMaxState(false);
			}
			/*
			响应：max来最大化窗口。
			*/
			else if (wParam == SIZE_MAXIMIZED)
			{
				paused = false;
				minOrMaxed = true;

				renderSystem->onSizeChanged();

				//maxed
				window->setMinMaxState(true);
			}
			/*
			Restored is any resize that is not a minimize or maximize.
			For example,restoring the window to its default size after a minimize or maximize,or 
			from dragging the resize bars.

			响应：从max还原到原来窗口的大小
			*/
			else if (wParam == SIZE_RESTORED)
			{
				paused = false;

				/*
				Are we restoring from a mimimized or maximized state,and are in windowed mode?
				Do not execute this code if we are restoring to full screen mode.
				*/
				if (minOrMaxed && renderSystem->getWindowed())
				{
					renderSystem->onSizeChanged();

					//maxed->normal和mined->normal
					window->setMinMaxState(boost::logic::indeterminate);
				}
				else
				{
					/*
					No,which implies the user is resizing by dragging the resize bars.However,we do 
					not reset the device here because as the user continuously drags the resize bars,
					a stream of WM_SIZE messages is sent to the window,and it would be pointless 
					(and slow) to reset for each WM_SIZE message received from dragging the resize 
					bars.So instead,we reset after the user is done resizing the window and releases 
					the resize bars,which sends a WM_EXITSIZEMOVE message.
					*/
				}
				minOrMaxed = false;
			}
			return 0;

		/*
		WM_EXITSIZEMOVE is sent when the user releases the resize bars.
		Here we reset everything based on the new window dimensions.
		*/
		case WM_EXITSIZEMOVE:
			GetClientRect(window->getHandle(), &clientRect);

			renderSystem->setBackBufferWH(clientRect.right, clientRect.bottom);

			renderSystem->onSizeChanged();
			return 0;

		/*
		The WM_DESTROY message is sent when your window is about to be destroyed.
		The DestroyWindow function takes care of destroying any child windows of the window 
		being destroyed.
		*/
		case WM_DESTROY:
			window->destroy();
			/*
			PostQuitMessage()在消息队列中插入一个WM_QUIT消息。通知应用程序退出系统。
			*/
			PostQuitMessage(0);
			return 0;

		case WM_KEYDOWN:
			if (wParam == VK_ESCAPE)
			{
				DestroyWindow(hWnd);
			}

			//PaUp
			if (wParam == VK_PRIOR)
			{
				//在窗口模式和全屏模式之间切换
				renderSystem->switchBetweenWindowedAndFullScreen();
			}
			return 0;
		}

		/*
		Application can modify the message parameters before passing the message to
		DefWindowProc, or it can continue with the default processing after performing its own
		operations.

		从DefWindowProc()返回的值必须由窗口过程返回。

		当用户按下菜单中的“关闭”命令时，系统发出WM_CLOSE消息。
		通常程序的窗口过程不拦截此消息，由DefWindowProc()来处理。
		DefWindowProc()收到WM_CLOSE后，调用DestroyWindow()，DestroyWindow()本身又发出
		WM_DESTROY消息。

		BOOL WINAPI DestroyWindow(HWND hWnd);
		hWnd:A handle to the window to be destroyed.
		If the function succeeds, the return value is nonzero.
		If the function fails, the return value is zero.
		Destroys the specified window. The function sends WM_DESTROY and WM_NCDESTROY 
		messages to the window to deactivate it and remove the keyboard focus from it. The 
		function also destroys the window's menu, flushes the thread message queue, destroys 
		timers, removes clipboard ownership, and breaks the clipboard viewer chain (if the window 
		is at the top of the viewer chain).
		If the specified window is a parent or owner window, DestroyWindow automatically 
		destroys the associated child or owned windows when it destroys the parent or owner 
		window. The function first destroys child or owned windows, and then it destroys the 
		parent or owner window.
		DestroyWindow also destroys modeless dialog boxes created by the CreateDialog function.
		A thread cannot use DestroyWindow to destroy a window created by a different thread.
		If the window being destroyed is a child window that does not have the WS_EX_NOPARENTNOTIFY 
		style, a WM_PARENTNOTIFY message is sent to the parent.
		You can use the DestroyWindow function to destroy a window. Typically, an application 
		sends the WM_CLOSE message before destroying a window, giving the window the 
		opportunity to prompt the user for confirmation before the window is destroyed. A window 
		that includes a window menu automatically receives the WM_CLOSE message when the 
		user clicks Close from the window menu. If the user confirms that the window should be 
		destroyed, the application calls DestroyWindow. The system sends the WM_DESTROY 
		message to the window after removing it from the screen. In response to WM_DESTROY,
		the window saves its data and frees any resources it allocated. A main window concludes 
		its processing of WM_DESTROY by calling the PostQuitMessage function to quit the 
		application.
		*/
		return DefWindowProc(hWnd, msg, wParam, lParam);
	}

	void WindowEventUtilities::messagePump()
	{
#if UNG_PLATFORM == UNG_PLATFORM_WIN32
		/*
		PeekMessage和GetMessage的区别：
		二者都是到消息队列中抓取消息，如果抓不到，程序中的主执行线程会被系统挂起，当系统再次回来照顾
		这个线程时，发现消息队列中仍然是空的，这时候两个API的行为就不同了：
		GetMessage，会过门不入，操作系统会再去照顾其他人。
		PeekMessage，会取回控制权，使程序得以执行一段时间。

		Windows为当前运行的每个Windows程序维护了一个消息队列。
		当发生输入事件后，Windows将事件转化为一个消息，并将它放入程序的消息队列中。

		从消息队列中取出消息：
		Windows Message Loop (NULL means check all HWNDs belonging to this context).

		PM_REMOVE:
		消息被PeekMessage()处理后，将其从消息队列中删除。
		*/
		MSG msg;
		while (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
		{
			//将msg结构传递给Windows，进行一些键盘转换
			TranslateMessage(&msg);
			/*
			再将msg结构传递给Windows。
			通过Windows USER模块，把消息分派给窗口过程。
			然后，Windows将该消息发送给适当的窗口，让它们进行处理。
			*/
			DispatchMessage(&msg);
		}
#endif
	}
}//namespace ung

#endif//URM_HIERARCHICAL_COMPILE