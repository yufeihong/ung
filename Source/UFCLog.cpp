#include "../../API/Include/UFCLogPrecompiled.h"

#include "UFCLog.h"

#ifdef UFC_HIERARCHICAL_COMPILE

#include <fstream>

namespace logging = boost::log;
namespace src = boost::log::sources;
namespace sinks = boost::log::sinks;
namespace keywords = boost::log::keywords;
namespace expr = boost::log::expressions;
namespace attrs = boost::log::attributes;

namespace ung
{
	class LogImpl::Impl
	{
	public:
		src::severity_logger_mt< LOG_LEVEL_TYPE > mServertyMtLog;
	};

	std::ostream& operator<< (std::ostream& strm, LOG_LEVEL_TYPE level)
	{
		static const char* strings[] =
		{
			"normal",
			"notification",
			"warning",
			"error",
			"critical"
		};

		if (static_cast<size_t>(level) < sizeof(strings) / sizeof(*strings))
		{
			strm << strings[level];
		}
		else
		{
			strm << static_cast<int>(level);
		}

		return strm;
	}

	struct severity_tag;

	logging::formatting_ostream& operator<<(logging::formatting_ostream& strm,
																	logging::to_log_manip< LOG_LEVEL_TYPE, severity_tag > const& manip)
	{
		static const char* strings[] =
		{
			"NORM",
			"NTFY",
			"WARN",
			"ERRR",
			"CRIT"
		};

		LOG_LEVEL_TYPE level = manip.get();
		if (static_cast<size_t>(level) < sizeof(strings) / sizeof(*strings))
		{
			strm << strings[level];
		}
		else
		{
			strm << static_cast<int>(level);
		}

		return strm;
	}

	BOOST_LOG_ATTRIBUTE_KEYWORD(line_id, "LineID", unsigned int)
	BOOST_LOG_ATTRIBUTE_KEYWORD(severity, "Severity", LOG_LEVEL_TYPE)
	BOOST_LOG_ATTRIBUTE_KEYWORD(tag_attr, "Tag", String)

	LogImpl::LogImpl() : 
		mImplPtr(std::make_shared<Impl>())
	{
		boost::shared_ptr< logging::core > corePtr = logging::core::get();

		boost::shared_ptr< sinks::text_file_backend > ungBackendPtr = 
			boost::make_shared< sinks::text_file_backend >(keywords::file_name = "Ung_%2N.log",
																					keywords::rotation_size = 1 * 1024 * 1024);
		typedef sinks::synchronous_sink< sinks::text_file_backend > sink_file;													//synchronous：同步
		boost::shared_ptr< sink_file > ungSinkPtr(new sink_file(ungBackendPtr));
#if UNG_DEBUGMODE
		ungSinkPtr->set_filter(severity >= LLT_NORMAL);
#else
		ungSinkPtr->set_filter(severity >= LLT_WARNING);
#endif
		logging::formatter ungFmt = expr::stream
															<< std::setw(6) << std::setfill('0') << expr::attr< unsigned int >("LineID") << std::setfill(' ')
															<< ","
															<< expr::format_date_time< boost::posix_time::ptime >("TimeStamp", "%Y-%m-%d %H:%M:%S")
															<< ": <" << expr::attr< LOG_LEVEL_TYPE, severity_tag >("Severity") << ">:"
															<< expr::smessage;

		ungSinkPtr->set_formatter(ungFmt);
		corePtr->add_sink(ungSinkPtr);

		boost::shared_ptr< sinks::text_ostream_backend > exceptionBackendPtr = boost::make_shared< sinks::text_ostream_backend >();
		exceptionBackendPtr->add_stream(boost::shared_ptr< std::ostream >(new std::ofstream("Exception.log")));
		typedef sinks::synchronous_sink< sinks::text_ostream_backend > sink_ostream;
		boost::shared_ptr< sink_ostream > exceptionSinkPtr(new sink_ostream(exceptionBackendPtr));
		exceptionBackendPtr->auto_flush(true);
		exceptionSinkPtr->set_filter(expr::has_attr(tag_attr) && tag_attr == "IMPORTANT_MESSAGE");
		logging::formatter exceptionFmt = expr::stream
															<< std::setw(6) << std::setfill('0') << line_id << std::setfill(' ')
															<< ","
															<< expr::format_date_time< boost::posix_time::ptime >("TimeStamp", "%Y-%m-%d %H:%M:%S")
															<< ":"
															<< expr::smessage;

		exceptionSinkPtr->set_formatter(exceptionFmt);
		corePtr->add_sink(exceptionSinkPtr);

		logging::add_common_attributes();
	}

	LogImpl::~LogImpl()
	{
		/*
		防止当使用file sinks的情况下,进程termination的时候,程序crash.
		在main()返回之前,从core中移除和销毁所有的sinks.
		*/
		logging::core::get()->remove_all_sinks();
	}

	void LogImpl::logRecord(String& mes,LOG_LEVEL_TYPE level)
	{
		BOOST_LOG_SEV(mImplPtr->mServertyMtLog, level) << mes;
	}

	void LogImpl::logRecord(const char* mes,LOG_LEVEL_TYPE level)
	{
		BOOST_LOG_SEV(mImplPtr->mServertyMtLog, level) << mes;
	}

	void LogImpl::logException(const char* mes,LOG_LEVEL_TYPE level)
	{
		mImplPtr->mServertyMtLog.add_attribute("Tag", attrs::constant< String >("IMPORTANT_MESSAGE"));
		BOOST_LOG_SEV(mImplPtr->mServertyMtLog,level) << mes;
	}

	void LogImpl::logException(String& mes,LOG_LEVEL_TYPE level)
	{
		//BOOST_LOG_NAMED_SCOPE("Exception");

		mImplPtr->mServertyMtLog.add_attribute("Tag", attrs::constant< String >("IMPORTANT_MESSAGE"));
		BOOST_LOG_SEV(mImplPtr->mServertyMtLog,level) << mes;
	}
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE

/*
枚举类型(enumeration)使我们可以将一组整型常量组织在一起.和类一样,每个枚举类型定义了一种新的类型.枚举属于字面值常量类型.
C++包含两种枚举:限定作用域的和不限定作用域的.C++11新标准引入了限定作用域的枚举类型(scoped enumeration).

定义限定作用域的枚举类型的一般形式是:首先是关键字enum class(或者等价地使用enum struct),随后是枚举类型名字以及用花括号括起来的以逗号分隔的枚
举成员(enumerator)列表,最后是一个分号:
enum class open_modes {input, output, append};
我们定义了一个名为open_modes的枚举类型,它包含三个枚举成员:input, output和append.

定义不限定作用域的枚举类型(unscoped enumeration)时省略掉关键字class(或struct),枚举类型的名字是可选的:
enum color {red, yellow, green};  //不限定作用域的枚举类型
//未命名的、不限定作用域的枚举类型
enum {floatPrec = 6, doublePrec = 10, double_doublePrec = 10};
如果enum是未命名的,则我们只能在定义该enum时定义它的对象.和类的定义类似,我们需要在enum定义的右侧花括号和最后的分号之间提供逗号分隔的声明
列表.

枚举成员
在限定作用域的枚举类型中,枚举成员的名字遵循常规的作用域准则,并且在枚举类型的作用域外是不可访问的.与之相反,在不限定作用域的枚举类型中,枚举
成员的作用域与枚举类型本身的作用域相同:
enum color {red, yellow, green};  //不限定作用域的枚举类型
enum stoplight {red, yellow, green};  //错误,重复定义了枚举成员
enum class peppers {red, yellow, green}; //正确,枚举成员被隐藏了
color eyes = green; //正确,不限定作用域的枚举类型的枚举成员位于有效的作用域中
peppers p = green;  //错误,peppers的枚举成员不在有效的作用域中
//color::green在有效的作用域中,但是类型错误
color hair = color::red;  //正确,允许显式地访问枚举成员
peppers p2 = peppers::red; //正确,使用peppers的red
默认情况下,枚举值从0开始,依次加1.不过我们也能为一个或几个枚举成员指定专门的值:
enum class intTypes {
charTyp = 8, shortTyp = 16, intTyp = 16,
longTyp = 32, long_longTyp = 64
};
由枚举成员intTyp和shortTyp可知,枚举值不一定唯一.如果我们没有显式地提供初始值,则当前枚举成员的值等于之前枚举成员的值加1.

枚举成员是const,因此在初始化枚举成员时提供的初始值必须是常量表达式.也就是说,每个枚举成员本身就是一条常量表达式,我们可以在任何需要常量表达
式的地方使用枚举成员.例如,我们可以定义枚举类型的constexpr变量:
constexpr intTypes charbits = intTypes::charTyp;
类似的,我们也可以将一个enum作为switch语句的表达式,而将枚举值作为case标签.出于同样的原因,我们还能将枚举类型作为一个非类型模板形参使用;或
者在类的定义中初始化枚举类型的静态数据成员.

和类一样,枚举也定义新的类型
只要enum有名字,我们就能定义并初始化该类型的成员.要想初始化enum对象或者为enum对象赋值,必须使用该类型的一个枚举成员或者该类型的另一个对象:
open_modes om = 2;  //错误,2不属于类型open_modes
om = open_modes::input; //正确,input是open_modes的一个枚举成员
一个不限定作用域的枚举类型的对象或枚举成员自动地转换成整形.因此,我们可以在任何需要整型值的地方使用它们:
int i = color::red;  //正确,不限定作用域的枚举类型的枚举成员隐式地转换成int
int j = peppers::red; //错误,限定作用域的枚举类型不会进行隐式转换

指定enum的大小
C++11:
尽管每个enum都定义了唯一的类型,但实际上enum是由某种整数类型表示的.在C++11新标准中,我们可以在enum的名字后加上冒号以及我们想在该enum中
使用的类型:
enum intValues : unsigned long long {
charTyp = 255, shortTyp = 65535, intTyp = 65535,
longTyp = 4294967295UL,
long_longTyp = 18446744073709551615ULL
};
如果我们没有指定enum的潜在类型,则默认情况下限定作用域的enum成员类型是int.对于不限定作用域的枚举类型来说,其枚举成员不存在默认类型,我们只
知道成员的潜在类型足够大,肯定能够容纳枚举值.如果我们指定了枚举成员的潜在类型(包括对限定作用域的enum的隐式指定),则一旦某个枚举成员的值超出
了该类型所能容纳的范围,将引发程序错误.

指定enum潜在类型的能力使得我们可以控制不同实现环境中使用的类型,我们将可以确保在一种实现环境中编译通过的程序所生成的代码与其他实现环境中生成
的代码一致.

枚举类型的前置声明
C++11:
在C++11新标准中,我们可以提前声明enum.enum的前置声明(无论隐式地还是显式地)必须指定其成员的大小:
//不限定作用域的枚举类型intValues的前置声明
enum intValues : unsigned long long; //不限定作用域的,必须指定成员类型
enum class open_modes;  //限定作用域的枚举类型可以使用默认成员类型int.
因为不限定作用域的enum未指定成员的默认大小,因此每个声明必须指定成员的大小.对于限定作用域的enum来说,我们可以不指定其成员的大小,这个值被隐
式地定义成int.

和其他声明语句一样,enum的声明和定义必须匹配,这意味着在该enum的所有声明和定义中成员的大小必须一致.而且,我们不能在同一个上下文中先声明一个
不限定作用域的enum名字,然后再声明一个同名的限定作用域的enum:
//错误:所有的声明和定义必须对该enum是限定作用域的还是不限定作用域的保持一致
enum class intValues;
enum intValues;  //错误,intValues已经被声明成限定作用域的enum
enum intValues : long; //错误,intValues已经被声明成int

形参匹配与枚举类型
要想初始化一个enum对象,必须使用该enum类型的另一个对象或者它的一个枚举成员.因此,即使某个整型值恰好与枚举成员的值相等,它也不能作为函数的
enum实参使用:
//不限定作用域的枚举类型,潜在类型因机器而异
enum Tokens {INLINE = 128, VIRTUAL = 129};
void ff(Tokens);
void ff(int);
int main()
{
	Tokens curTok = INLINE;
	ff(128);  //精确匹配ff(int)
	ff(INLINE); //精确匹配ff(Tokens)
	ff(curTok); //精确匹配ff(Tokens)
	return 0;
}

尽管我们不能直接将整型值传给enum形参,但是可以将一个不限定作用域的枚举类型的对象或枚举成员传给整形形参.此时,enum的值提升成int或更大的整形,
实际提升的结果由枚举类型的潜在类型决定:
void newf(unsigned char);
void newf(int);
unsigned char uc = VIRTUAL;
newf(VIRTUAL);  //调用newf(int)
newf(uc);  //调用newf(unsigned char)
枚举类型Tokens只有两个枚举成员,其中较大的那个值是129.该枚举类型可以用unsigned char来表示,因此很多编译器使用unsigned char作为Tokens的潜
在类型.不管Tokens的潜在类型到底是什么,它的对象和枚举成员都提升成int.尤其是,枚举成员永远不会提升成unsigned char,即使枚举值可以
用unsigned char存储也是如此.
*/

 /*
 大多数类应该定义默认构造函数、拷贝构造函数和拷贝赋值运算符,无论是隐式地还是显式地.
 虽然大多数类应该定义(而且通常也的确定义了)拷贝构造函数和拷贝赋值运算符,但对某些类来说,这些操作没有合理的意义.在此情况下,定义类
 时必须采用某种机制阻止拷贝或赋值.例如,std::iostream类阻止了拷贝,以避免多个对象写入或读取相同的IO缓冲.为了阻止拷贝,看起来可能应该不
 定义拷贝控制成员.但是,这种策略是无效的:如果我们的类未定义这些操作,编译器为它生成合成的版本.

 在新标准下,我们可以通过将拷贝构造函数和拷贝赋值运算符定义为删除的函数(deleted function)来阻止拷贝.删除的函数是这样一种函数:我们虽
 然声明了它们,但不能以任何方式使用它们.在函数的参数列表后面加上=delete来指出我们希望将它定义为删除的.

 =delete通知编译器(以及我们代码的读者),我们不希望定义这些成员.
 与=default不同,=delete必须出现在函数第一次声明的时候,这个差异与这些声明的含义在逻辑上是吻合的.一个默认的成员只影响为这个成员而
 生成的代码,因此=default直到编译器生成代码时才需要.而另一方面,编译器需要知道一个函数是删除的,以便禁止试图使用它的操作.
 与=default的另一个不同之处是,我们可以对任何函数指定=delete(我们只能对编译器可以合成的默认构造函数或拷贝控制成员使用=default).虽
 然删除函数的主要用途是禁止拷贝控制成员,但当我们希望引导函数匹配过程时,删除函数有时也是有用的.

 析构函数不能是删除的成员
 值得注意的是,我们不能删除析构函数.如果析构函数被删除,就无法销毁此类型的对象了.
 对于一个删除了析构函数的类型,编译器将不允许定义该类型的变量或创建该类的临时对象.而且,如果一个类有某个成员的类型删除了析构函数,我
 们也不能定义该类的变量或临时对象.因为如果一个成员的析构函数时删除的,则该成员无法被销毁.而如果一个成员无法被销毁,则对象整体也就无
 法被销毁了.

 对于删除了析构函数的类型,虽然我们不能定义这种类型的变量或成员,但可以动态分配这种类型的对象.但是,不能释放这些对象:
 struct NoDtor
 {
	NoDtor() = default;																					//使用合成默认构造函数
	~NoDtor() = delete;																				//我们不能销毁NoDtor类型的对象
 };
 NoDtor nd;																								//错误,NoDtor的析构函数时删除的
 NoDtor *p = new NoDtor();																			//正确,但我们不能delete p
 delete p;																									//错误,NoDtor的析构函数是删除的
 对于析构函数已删除的类型,不能定义该类型的变量或释放指向该类型动态分配对象的指针.
 */