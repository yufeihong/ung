#include "UMMBigDog.h"

#ifdef UMM_HIERARCHICAL_COMPILE

#include <sstream>

namespace ung
{
	BigDog::size_type BigDog::sLevelFirstMallocTimes{};
	BigDog::size_type BigDog::sLevelFirstHeapSize{};
	BigDog::size_type BigDog::sLevelSecondMallocTimes{};
	BigDog::size_type BigDog::sLevelSecondHeapSize{};
	BigDog::size_type BigDog::sRecordTimesArray2[UMM_FREELISTS_COUNT]{};
	BigDog::size_type BigDog::sRecordCellNum[UMM_FREELISTS_COUNT]{};
	BigDog::size_type BigDog::sPoolRemaining{};
	BigDog::statistics_type BigDog::mStatistics;

	BigDog::BigDog()
	{
	}

	BigDog::~BigDog()
	{
		reportWholePool();
	}

	void BigDog::doNew(void* address,size_type size,std::string const& info)
	{
		std::string theInfo(info);
		std::ostringstream oss;
		oss << size;
		theInfo += "$";
		theInfo += oss.str();
		mStatistics.insert({address,theInfo});
	}

	void BigDog::doDelete(void* address)
	{
#if UNG_DEBUGMODE
		auto eraseRet = mStatistics.erase(address);

		/*
		这里抛出异常的原因是，类已经从MemoryPool<>继承了，但是当进行new操作的时候，使用的是
		GLOBAL_NEW而不是UNG_NEW。
		*/
		BOOST_ASSERT(eraseRet == 1);
#endif
	}

	//-------------------------------------------------------------------------------------

	void BigDog::addLevelFirstMallocTimes()
	{
		++sLevelFirstMallocTimes;
	}

	void BigDog::addLevelFirstHeapSize(size_type size,bool flag)
	{
		if (flag)
		{
			sLevelFirstHeapSize += size;
		}
		else
		{
			sLevelFirstHeapSize -= size;
		}
	}

	void BigDog::addLevelSecondMallocTimes()
	{
		++sLevelSecondMallocTimes;
	}

	void BigDog::addLevelSecondHeapSize(size_type size)
	{
		sLevelSecondHeapSize += size;
	}

	void BigDog::addFreeListsTouched(size_type freelistIndex)
	{
		++sRecordTimesArray2[freelistIndex];
	}

	void BigDog::addFreeListsCellNum(size_type freelistIndex, size_type num)
	{
		sRecordCellNum[freelistIndex] += num;
	}

	void BigDog::substractFreeListsCellNum(size_type freelistIndex)
	{
		//必须有可减的才行。
		BOOST_ASSERT(sRecordCellNum[freelistIndex] > 0);

		--sRecordCellNum[freelistIndex];
	}

	void BigDog::setPoolRemaining(size_type size)
	{
		sPoolRemaining = size;
	}

	void BigDog::reportWholePool()
	{
		using namespace std;

		cout << endl;

		//cout << "-----第一级-----" << endl;
		//cout << "总共从内存条中成功获取空间的总次数:" << sLevelFirstMallocTimes << endl;
		//sLevelFirstMallocTimes（这个变量中不包含已经归还给堆的）
		if (sLevelFirstHeapSize == 0)
		{
			//cout << "=====第一级无泄露=====" << endl;
		}
		else
		{
			cout << "!!!!!!!!!!第一级泄露了:" << sLevelFirstHeapSize << "B" << endl;
		}

		//-------------------------------------------------------------------------------------------
		
		//计算Free Lists的剩余总字节数
		size_type freelistsLeftBytes{};
		for (size_type i = 0; i < UMM_FREELISTS_COUNT; ++i)
		{
			freelistsLeftBytes += sRecordCellNum[i] * ((i + 1) * UMM_BYTESALIGNED);
		}

		//以免无符号整数的回绕
		int32 secondLeak{};
		if ((sPoolRemaining + freelistsLeftBytes) == sLevelSecondHeapSize)
		{
			//cout << "=====第二级无泄露=====" << endl;
		}
		else
		{
			secondLeak = sLevelSecondHeapSize - (sPoolRemaining + freelistsLeftBytes);
			cout << "!!!!!!!!!!第二级泄露了:" << secondLeak << "B" << endl;
			BOOST_ASSERT_MSG(secondLeak > 0,"给予了智能指针定制的deleter，却没有在容器clear之前进行reset。");
		}

		//-------------------------------------------------------------------------------------------

		//new操作的总泄露量
		size_type newTotalLeak{};

		if (mStatistics.empty())
		{
			//cout << "=====new操作无内存泄露=====" << endl << endl;
		}
		else
		{
			cout << "new操作内存泄露明细:" << endl;
			cout << "共计:" << mStatistics.size() << "个对象未释放。" << endl;

			//明细编号
			size_type num{};

			for (auto const& item : mStatistics)
			{
				auto const& address = item.first;
				auto const& info = item.second;

				//获取size(+1，从$之后)
				string sizeStr = info.substr(info.find('$') + 1);
				auto leakBytes = stoi(sizeStr);
				newTotalLeak += leakBytes;

				ostringstream addr;
				addr << address;

				cout << "第" << num++ << "个:" << endl;

				cout << "地址:" << addr.str()
					<< endl
					<< "信息:" << info
					<< endl << endl;
			}
		}

		//总计泄露量
		auto theTotalLeakBytes = sLevelFirstHeapSize + secondLeak;
		if (theTotalLeakBytes > 0)
		{
			cout << "!!!!!!!!!!总计泄露量:" << theTotalLeakBytes << "B" << endl;
		}

		if (newTotalLeak > 0)
		{
			cout << "!!!!!!!!!!new操作共计泄露了:" << newTotalLeak << "B" << endl;
		}

		int32 conTotalLeak = theTotalLeakBytes - newTotalLeak;
		if (conTotalLeak > 0)
		{
			cout << "!!!!!!!!!!容器分配操作共计泄露了:" << conTotalLeak << "B" << endl;
		}
		else
		{
			//cout << "=====容器分配无内存泄露=====" << endl;
		}
	}
}//namespace ung

#endif//UMM_HIERARCHICAL_COMPILE