#include "UNGStatistics.h"

#ifdef UNG_HIERARCHICAL_COMPILE

namespace ung
{
	Statistics::Statistics() :
		mFPS(0.0)
	{
	}

	Statistics::~Statistics()
	{
	}

	void Statistics::update(real_type dt)
	{
		_updateFPS(dt);
	}

	real_type Statistics::getFPS() const
	{
		return mFPS;
	}

	void Statistics::_updateFPS(real_type dt)
	{
		static real_type numFrames = 0.0;
		static real_type timeElapsed = 0.0;

		//Increment the frame count.
		numFrames += 1.0;

		//Accumulate how much time has passed.
		timeElapsed += dt;

		//1����
		if (timeElapsed >= 1000.0)
		{
			mFPS = numFrames;

			numFrames = 0.0;
			timeElapsed = 0.0;
		}
	}
}//namespace ung

#endif//UNG_HIERARCHICAL_COMPILE