#include "UFCUnpackHelperInfo.h"

#ifdef UFC_HIERARCHICAL_COMPILE

#include "UFCFilesystem.h"

#pragma warning(disable:4715)

namespace ung
{
	UnpackHelperInfo::~UnpackHelperInfo()
	{
		mMaps.clear();
		mItems.clear();
		mSections.clear();
		mNames.clear();
	}

	String const& UnpackHelperInfo::getPackName() const
	{
		return mPackName;
	}

	unsigned int UnpackHelperInfo::getFileIndex(const char* fileName) const
	{
		String name(fileName);
		BOOST_ASSERT(!name.empty());

		//文件系统
		auto& fsRef = Filesystem::getInstance();

		//规范化文件名
		fsRef.processDirSeparator(name);

		for (unsigned int i = 0; i < mFileCount; ++i)
		{
			if (mNames[i] == name)
			{
				return i;
			}
		}

		//抛出异常
		BOOST_ASSERT(0 && "file name is not found in this pack.");
	}
}//namespace ung

#endif//UFC_HIERARCHICAL_COMPILE